FROM php:7.1-apache

# Set working directory
WORKDIR "/var/www/html"

# Copy directory and it's content to working directory  
COPY . /var/www/html

RUN chmod -R 777 /var/www/html

# Exposing port
EXPOSE 5000