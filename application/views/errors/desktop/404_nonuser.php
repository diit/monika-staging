<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.ico">
    <title>Page Not Found - Monika App</title>
    <meta name="description" content="Software otomatisasi yang membatu mengelola pemasaran freelancer dan ahensi dalam membuat hingga melacak proposal dan faktur untuk memenangkan klien dalam waktu singkat">
    <meta name="author" content="Monika">
    <meta name="keyword" content="Proposal, Invoice, Penawaran, Lacak Proposal, Tracking, Closing, Proposal Goal">

    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/font-awesome-5/css/all.css">
    <link rel="stylesheet/less" href="<?= base_url() ?>assets/css/style.less">
    <link rel="stylesheet/less" href="<?= base_url() ?>assets/css/style_desktop.less">
    <link rel="stylesheet/less" href="<?= base_url() ?>assets/css/style_desktop_akses.less">

    <script src="<?= base_url() ?>assets/js/bootstrap-4/js/jquery-3.3.1.slim.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap-4/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap-4/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/js/less.min.js" ></script>

    <!-- analytics script -->
    <?php $this->load->view('template/global_analytics'); ?>
    <!-- end of analytics script -->

</head>
<body>

    <div class="row">
    <div class="col-sm-6 col-md-7" id="welcome" style="overflow:hidden;position: fixed;">
        <div id="konten">
            <h3>Menangkan klien dalam waktu singkat</h3>
            <ul class="fa-ul">
                <li><span class="fa-li" ><i class="fas fa-check"></i></span><span class="bold">Mudah</span> membuatnya</li>
                <li><span class="fa-li" ><i class="fas fa-check"></i></span><span class="bold">Banyak template</span> proposal sesuai bidang</li>
                <li><span class="fa-li" ><i class="fas fa-check"></i></span><span class="bold">Aman,</span> data hingga pembayaran lebih aman</li>
                <li><span class="fa-li" ><i class="fas fa-check"></i></span><span class="bold">On-time,</span> terima pembayaran tepat waktu</li>
            </ul>

            <div id="medsos">
                <a href="https://www.facebook.com/usemonika" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                        <path id="fb" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z    M67.521,24.89l-6.76,0.003c-5.301,0-6.326,2.519-6.326,6.215v8.15h12.641L67.07,52.023H54.436v32.758H41.251V52.023H30.229V39.258   h11.022v-9.414c0-10.925,6.675-16.875,16.42-16.875l9.851,0.015V24.89L67.521,24.89z" fill="#FFFFFF"/>
                    </svg>
                </a>
                <a href="https://www.instagram.com/usemonika/" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                        <path id="in" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z    M30.562,81.966h-13.74V37.758h13.74V81.966z M23.695,31.715c-4.404,0-7.969-3.57-7.969-7.968c0.001-4.394,3.565-7.964,7.969-7.964   c4.392,0,7.962,3.57,7.962,7.964C31.657,28.146,28.086,31.715,23.695,31.715z M82.023,81.966H68.294V60.467   c0-5.127-0.095-11.721-7.142-11.721c-7.146,0-8.245,5.584-8.245,11.35v21.869H39.179V37.758h13.178v6.041h0.185   c1.835-3.476,6.315-7.14,13-7.14c13.913,0,16.481,9.156,16.481,21.059V81.966z" fill="#FFFFFF"/>
                    </svg>
                </a>
                <a href="https://www.linkedin.com/company/monikasales/" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                        <ellipse id="ig" cx="48.944" cy="48.719" rx="14.465" ry="14.017" fill="#FFFFFF"/>
                        <path id="ig" d="M71.333,49.501c0,11.981-10.024,21.692-22.389,21.692s-22.389-9.711-22.389-21.692c0-2.147,0.324-4.221,0.924-6.18h-6.616    v30.427c0,1.576,1.288,2.863,2.863,2.863h50.159c1.576,0,2.865-1.287,2.865-2.863V43.321h-6.341    C71.008,45.28,71.333,47.354,71.333,49.501z" fill="#FFFFFF"/>
                        <path id="ig" d="M65.332,35.11h8.141c1.784,0,3.242-1.458,3.242-3.242v-7.762c0-1.785-1.458-3.243-3.242-3.243h-8.141    c-1.785,0-3.243,1.458-3.243,3.243v7.762C62.088,33.651,63.547,35.11,65.332,35.11z" fill="#FFFFFF"/>
                        <path id="ig" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z     M75.645,84.891H22.106c-5.087,0-9.246-3.765-9.246-9.244V22.105c0-5.481,4.159-9.245,9.246-9.245h53.539    c5.086,0,9.246,3.764,9.246,9.245v53.542C84.891,81.126,80.73,84.891,75.645,84.891z" fill="#FFFFFF"/>
                    </svg>
                </a>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-md-5 text-center" id="form-login">
        <div class="row justify-content-center">
            <div class="col-sm-11 col-md-8">
                <img src="<?= base_url() ?>assets/img/monika-logo-mobile.png" alt="monika-logo" id="brand">
                <img src="<?= base_url() ?>assets/img/404.png" alt="not-found-monika" style="width:100%">
                &nbsp;
                <h4 class="text-center">Page Not Found</h4>

                <a href="<?= site_url('login') ?>" class="btn btn-primary" style="margin-right:0; margin-top:40px">login sekarang</a>
            </div>
        </div>
    </div>


    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="798a792f-1ac9-4623-ae90-3bf3c539b6fa";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

</body>
</html>
