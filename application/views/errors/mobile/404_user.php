<div id="content" class="text-center">
    <img src="<?= base_url() ?>assets/img/404.png" style="width:200px">
    <h3 style="margin-top:30px">Page Not Found</h3>
    <a href="<?= site_url('home') ?>" class="btn btn-primary" style="margin-right:0; margin-top:20px">back to dashboard</a>
</div>
