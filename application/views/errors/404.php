<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Monika">
    <meta name="keyword" content="Proposal, Invoice, Penawaran, Lacak Proposal, Tracking, Closing, Proposal Goal">
    <title>MONIKA - Manajemen Dokumen</title>
    <link rel="shortcut icon" href="assets/img/favicon.ico">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">

    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lineicons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/easyWizard.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/monika.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/content-tools.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style-dimention.css">
    <link href="<?php echo base_url(); ?>assets/css/spectrum.css" rel="stylesheet">
    <script src="<?php echo base_url()?>assets/js/jquery3.2.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap3.3.7.min.js"></script>


    <!-- Hotjar Tracking Code for https://app.monika.id -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:751855,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

      <style>
          .btn-aksi {
              margin: 40px 15px 20px 0;
          }
      </style>

  </head>

  <body>
      <?php if(empty($this->session->userdata('team_id'))) { ?>
        <div class="row" style="margin-top:5%">
            <div class="container">
                <div class="col-xs-12 text-center">
                    <h1 style="font-size:10em">404</h1>
                    <h1 style="font-weight:bold">Page not found</h1>
                    <h3>Check the url address</h3>
                </div>
            </div>
        </div>
      <?php }
       else {
      ?>

  <section id="container" >
      <!--header start-->
      <?php $this->load->view('template/navbar'); ?>
      <!--header end-->
      <!-- Start Sidebar -->
        <?php $this->load->view('template/sidebar');  ?>
      <!-- End Sidebar -->
      <!-- Start Content -->
        <section id="main-content">
            <section class="wrapper">
                <div class="row mt">
                    <div class="container">
                        <div class="col-xs-12 text-center">
                            <h1 style="font-size:10em">404</h1>
                            <h1 style="font-weight:bold">Page not found</h1>
                            <h3>Check the url address</h3>
                        </div>
                    </div>
                </div>
            </section>
      </section>
      <!-- End Content -->
  </section>


    <!-- Intro JS -->
    <script type="text/javascript" src="<?php echo base_url();?>node_modules/sweetalert/dist/sweetalert.min.js"> </script>
    <script src="<?php echo base_url(); ?>assets/js/monika.js"></script>
    <script src="<?php echo base_url(); ?>assets/content-tools.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/common-scripts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/spectrum.js"></script>

    <script type="application/javascript">
        $(document).ready(function () {
          <?php if ($content == 'home.php'&& $this->session->userdata('role') == 1 && $tutorial == 0 && ($this->session->userdata('user_category') != NULL || $this->session->userdata('user_about') != NULL)) {?>
              var intro = introJs();
              intro.setOptions({
                exitOnOverlayClick: false,
                nextLabel: 'Selanjutnya',
                prevLabel: 'Sebelumnya',
                doneLabel: 'Selesai',
                disableInteraction: true,
                steps: [
                  {
                    intro: "Terima kasih atas waktu untuk mengisi informasi usaha Anda. Sekarang saatnya Anda membuat proposal pertama."
                  },
                  {
                    element: '#duplicatepro',
                    intro: "Bila Anda sering membuat proposal berulang-ulang dengan isi yang sama atau dengan beberapa perubahan, gunakan dulikasi proposal yang akan sangat menghemat waktu Anda"
                  },
                  {
                    element: '#createpro',
                    intro: "Untuk membuat proposal dengan pengisian baru silahkan klik tombol Buat baru"
                  },
                  {
                    element: '#editprop',
                    intro: "Atau gunakan contoh template yang sudah ada di sini untuk pembuatan yang lebih cepat"
                  },
                  {
                    element: '#propstat',
                    intro: "Anda bisa melacak status proposal di sini. Pastikan Anda mengetahui sejauh mana klien memahami proposal yang dibaca melalui prediksi berdasarkan lamanya waktu membaca proposal per bagian"
                  }
                ]
               });
                // $('.introjs-prevbutton').text('Sebelumnya');
                intro.start();

                intro.oncomplete(function() {
                      $.ajax({
                        type: "POST",
                        data:{
                          tutorial: 1,
                          id: '<?=$this->session->userdata('user_id')?>'
                        },
                        url: '<?php echo base_url()."user/settutorial"; ?>',
                      }).done(function(res){
                        window.location='<?=base_url()."home"?>';
                      });
                });

                intro.onexit(function() {
                      $.ajax({
                        type: "POST",
                        data:{
                          tutorial: 1,
                          id: '<?=$this->session->userdata('user_id')?>'
                        },
                        url: '<?php echo base_url()."user/settutorial"; ?>',
                      }).done(function(res){
                        window.location='<?=base_url()."home"?>';
                      });
                });
          <?php } ?>


              $('[data-toggle="tooltip"]').tooltip();



              $('.btn-editkategori').on('click',function(){
                $('#category_edit').modal('show');
                $('#category_edit').modal({backdrop: 'static',keyboard: false});
              });

              $(document).on('click','.btn-save-business-detail',function(){
                        var ini = $('.btn-save-about');
                        var aboutme = $('#aboutme_edit').val();
                        var kategori = $('input[name="userCat_edit"]:checked').val();
                        var skills=[];
                        $('[name="skill"]').each(function(){
                          if($(this).val() != '')
                          {
                            skills.push($(this).val());
                          }
                        });

                        var portfolio=[];
                        $('[name="portfolio"]').each(function(){
                          if($(this).val() != '')
                          {
                            portfolio.push($(this).val());
                          }
                        });

                        if(kategori==undefined)
                        {
                          swal('Mohon pilih kategori usaha Anda');
                        }
                        else if(aboutme == '')
                        {
                          swal('Mohon isi tentang Anda dan usaha yang dijalankan');
                        }
                        else if(skills.length <= 0 || skills[0] == '')
                        {
                          swal('Mohon isi keahlian Anda');
                        }
                        else if(portfolio.length <= 0 || portfolio[0] == '')
                        {
                          swal('Mohon isi pengalaman projek yang pernah Anda kerjaan');
                        }
                        else
                        {
                          ini.attr('disable',true);
                          ini.html('Menyimpan...');
                          $.ajax({
                            type:"POST",
                            url: "<?php echo base_url()?>home/simpan_business_detail",
                            data: {
                                kategori: kategori,
                                about: aboutme,
                                skill: skills,
                                portfolio: portfolio
                                },
                            success: function(data){
                              ini.attr('disable',false);
                              ini.html('<i class="fa fa-check" aria-hidden="true"></i>Simpan');
                              if(data == 1)
                              {
                                $('#category_edit').modal('hide');
                                swal('Berhasil','Detail usaha berhasil disimpan','success')
                                .then((value) => {
                                  location.reload();
                                });
                              }
                              else
                              {
                                swal('Ada kesalahan');
                              }

                            }, error:function(error){
                              swal('Ada kesalahan nih');
                            }
                          });
                        }
                      });

              <?php if($this->session->userdata('role') == 1 && $tutorial == 0 && ($this->session->userdata('user_category') == NULL || $this->session->userdata('user_about') == NULL)){ ?>
                      //var kategori;
                      //var aboutme;
                      $('#category_edit').modal('show');
                      $('#category_edit').modal({backdrop: 'static', keyboard: false});
                <?php } ?>

              $('.concat').on('click',function() {
                $('.concat').removeClass('concat_active');
                $('.check_active').css('display','none');
                var catid = $(this).attr('catid');
                $('#cat_'+catid).prop("checked", true);
                $(this).addClass('concat_active');
                $('#check_'+catid).css('display','block');
              });


            var cs = <?php echo $cs; ?>;
            $(document).on('click','.addskill',function(){
              var divaddskill = $('<div id="skill'+ parseInt(cs+1) +'"></div>');
              var inputskill = $('<input style="float: left;width: 90%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="skill" id="inputskill'+ parseInt(cs+1) +'" type="text" class="form-control skill" placeholder="Skill '+ parseInt(cs+1) +'" maxlength="50">');
              var btndeleteskill = $('<button skillid="'+ parseInt(cs+1) +'" style="margin-top: 5px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm deleteskill"><i class="fa fa-trash"></i></button>');
              var latesskill = $('#inputskill'+cs).val();
              if(latesskill == '')
              {
                swal("Isikan keahlian pada isian terakhir");
              }
              else
              {
                cs++;
                $('.listskill').append(divaddskill.append(inputskill).append(btndeleteskill));
                inputskill.select();
              }
            });

            $(document).on('click','.deleteskill',function(){
              var skillid = $(this).attr('skillid');
              $('#skill'+skillid).remove();
              cs--;
            });


            var th = <?php echo $th; ?>;
            $(document).on('click','.addportfolio',function(){
              var divaddportfolio = $('<div id="portfolio'+ parseInt(th+1) +'"></div>');
              var inputportfolio = $('<input style="float: left;width: 90%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="portfolio" id="inputportfolio'+ parseInt(th+1) +'" type="text" class="form-control portfolio" placeholder="Portfolio '+ parseInt(th+1) +'" maxlength="70">');
              var btndeleteportfolio = $('<button portfolioid="'+ parseInt(th+1) +'" style="margin-top: 5px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm deleteportfolio"><i class="fa fa-trash"></i></button>');
              var latestportfolio = $('#inputportfolio'+th).val();
              if(latestportfolio == '')
              {
                swal("Isikan portfolio / pengalaman pada isian terakhir");
              }
              else
              {
                th++;
                $('.listportfolio').append(divaddportfolio.append(inputportfolio).append(btndeleteportfolio));
                inputportfolio.select();
              }
            });

            $(document).on('click','.deleteportfolio',function(){
              var portfolioid = $(this).attr('portfolioid');
              $('#portfolio'+portfolioid).remove();
              th--;
            });


        });

    </script>

      <?php } ?>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5a619ebb4b401e45400c3764/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->

  </body>
</html>
