<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.ico">
    <title><?= $page ?> - Monika App</title>
    <meta name="description" content="Software otomatisasi yang membatu mengelola pemasaran freelancer dan ahensi dalam membuat hingga melacak proposal dan faktur untuk memenangkan klien dalam waktu singkat">
    <meta name="author" content="Monika">
    <meta name="keyword" content="Proposal, Invoice, Penawaran, Lacak Proposal, Tracking, Closing, Proposal Goal">

    <?php $this->load->view('template/mobile/header'); ?>
</head>
<body>

    <?php $this->load->view('template/mobile/navmenu'); ?>

    <?php $this->load->view('template/mobile/headbar'); ?>

    <?php if((($this->uri->segment(1) == 'proposal') || ($this->uri->segment(1) == 'invoice') || ($this->uri->segment(1) == 'home') || ($this->uri->segment(1) == 'arsip') || ($this->uri->segment(1) == 'client')) && (empty($this->uri->segment(2))))
    {
        $this->load->view('template/mobile/history');
    } ?>

    <!-- content -->
    <?php $this->load->view($content); ?>
    <!-- end of content -->

    <?php $this->load->view('template/mobile/footer'); ?>
    <!-- page's footer -->
    <?php $this->load->view($t_foot); ?>
    <!-- end of page's footer -->

</body>
</html>
