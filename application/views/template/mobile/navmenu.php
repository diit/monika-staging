<div class="fixed-bottom" id="nav-menu">
    <div class="row text-center">
        <div class="col-3"><a href="<?= site_url('home') ?>"><i class="fas fa-th-large icon<?php echo ($this->uri->segment(1) == 'home') ? '-active':''; ?>"></i></a></div>
        <div class="col-3"><a href="<?= site_url('proposal') ?>"><i class="far fa-file-alt icon<?php echo ($this->uri->segment(1) == 'proposal') ? '-active':''; ?>"></i></a></div>
        <div class="col-3"><a href="<?= site_url('invoice') ?>"><i class="fab fa-wpforms icon<?php echo ($this->uri->segment(1) == 'invoice') ? '-active':''; ?>"></i></a></div>
        <div class="col-3"><a href="<?= site_url('user') ?>"><i class="far fa-user icon<?php echo ($this->uri->segment(1) == 'user') ? '-active':''; ?>"></i></a></div>
    </div>
</div>
