<div class="static-top" id="head-bar">
    <!-- icon history -->
    <?php if((($this->uri->segment(1) == 'proposal') || ($this->uri->segment(1) == 'invoice') || ($this->uri->segment(1) == 'home') || ($this->uri->segment(1) == 'arsip') || ($this->uri->segment(1) == 'klien')) && (empty($this->uri->segment(2))))
    { ?>
    <div class="pull-right" id="log">
        <span><i class="far fa-bell icon view-history"></i></span>
    </div>
    <?php } ?>
    <!-- end of icon history -->

    <!-- search box -->
    <?php if((($this->uri->segment(1) == 'proposal') || ($this->uri->segment(1) == 'invoice') || ($this->uri->segment(1) == 'klien') || ($this->uri->segment(1) == 'arsip')) && (empty($this->uri->segment(2)))) { ?>
    <div class="pull-left">
        <span><i class="fas fa-search grey"></i></span>
        <span class="grey"><?= $search ?></span>
    </div>
    <?php } ?>
    <!-- end of search box -->

    <!-- monika logo -->
    <?php if(($this->uri->segment(1) == 'home') || ($this->uri->segment(1) == 'user')) { ?>
    <img src="<?= base_url() ?>assets/img/monika-logo-mobile.png" alt="monika-logo" id="brand">
    <?php } ?>
    <!-- end of monika logo -->

    <!-- link back -->
    <?php if(!empty($branch)) { ?>
    <div id="back">
        <a href="<?= $back ?>"><i class="fas fa-arrow-left"></i></a>
        <span id="branch"><?= $branch ?></span>
    </div>
    <?php } ?>
    <!-- end of link back -->
</div>
