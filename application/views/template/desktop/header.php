<link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-4/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/font-awesome-5/css/all.css">
<link rel="stylesheet/less" href="<?= base_url() ?>assets/css/style.less">
<link rel="stylesheet/less" href="<?= base_url() ?>assets/css/style_desktop.less">

<!-- page's header -->
<?php $this->load->view($t_head); ?>
<!-- end of page's header -->

<script src="<?= base_url() ?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-4/js/popper.min.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-4/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/less.min.js" ></script>

<!-- analytics script -->
<?php $this->load->view('template/global_analytics'); ?>
<!-- end of analytics script -->
