<div id="nav" class="fixed-top">
    <div class="row">
        <div class="col-md-8">
            <?php
            if ((($this->uri->segment(1) == 'home') || ($this->uri->segment(1) == 'proposal') || ($this->uri->segment(1) == 'invoice') || ($this->uri->segment(1) == 'klien')) && (empty($this->uri->segment(2)))){
            ?>
                <span><i class="fas fa-search grey"></i></span>
                <span class="grey"><?= $search ?></span>
            <?php }
            elseif ($this->uri->segment(1) == 'arsip') {
            ?>
                <span><i class="fas fa-search grey"></i></span>
                <span class="grey"><?= $search1 ?> <?= $search2 ?></span>
            <?php
            }
            else { ?>
                <span class="grey text-little" id="list-link"><?= $branch ?></span>
            <?php } ?>

            <!-- reamain plan's day -->
            <?php if($this->monikalib->remainTrialDay()!=false){ ?>
                <a href="<?=site_url('plan')?>" class="btn btn-grey btn-small text-small bold"><?=$this->monikalib->remainTrialDay()?> hari trial plan agency</a>
            <?php } ?>
            <!-- end of reamain plan's day -->
        </div>
        <div class="col-md-4 text-right">
            <span id="statusSave"></span>
            <span>
                <a href="#" class="dropdown-toggle dropdown" data-toggle="dropdown"><i class="fas fa-th-list icon"></i></a>
                <div class="dropdown-menu dropdown-menu-right doc-list">
                    <?php
                    //proposal<=0 & invoice<=0
                    if (count($this->monikalib->getTotalProposalByTeam())<=0 && count($this->monikalib->getTotalInvoiceByTeam())<=0) { ?>
                        <a class="dropdown-item"><span class="docs"><i class="fas fa-exclamation"></i> Belum ada dokumen yang dibuat</span></a>
                    <?php }
                    //end of proposal<=0 & invoice<=0

                    else { ?>
                    <!-- proposal -->
                    <div>
                        <?php
                        //proposal<0
                        if (count($this->monikalib->getTotalProposalByTeam())<=0) {
                        ?>
                            <a class="dropdown-item"><span class="docs"><i class="fas fa-exclamation"></i> Belum ada proposal yang dibuat</span></a>
                        <?php }
                        //end of proposal<0

                        //proposal>0
                        elseif (count($this->monikalib->getTotalProposalByTeam())>0) { ?>
                            <a class="dropdown-item"><span class="pull-right"><?= count($this->monikalib->detailProposalTeam()['baca']) ?></span><span class="docs"><i class="fas fa-eye"></i> Proposal dibaca</span></a>
                            <a class="dropdown-item"><span class="pull-right"><?= count($this->monikalib->detailProposalTeam()['setuju']) ?></span><span class="docs"><i class="fas fa-check"></i> Proposal disetujui</span></a>
                        <?php }
                        //end of proposal>0 ?>
                    </div>
                    <!-- end of proposal -->

                    <div class="dropdown-divider"></div>

                    <!-- invoice -->
                    <div>
                        <?php
                        //invoice<=0
                        if (count($this->monikalib->getTotalInvoiceByTeam())<=0) {
                        ?>
                            <a class="dropdown-item"><span class="docs"><i class="fas fa-exclamation"></i> Belum ada invoice yang dibuat</span></a>
                        <?php }
                        //end of invoice<0

                        //invoice>0
                        elseif (count($this->monikalib->getTotalInvoice())>0) {
                        ?>
                            <a class="dropdown-item"><span class="pull-right"><?= count($this->monikalib->detailInvoiceTeam()['baca']) ?></span><span class="docs"><i class="fas fa-eye"></i> Invoice dibaca</span></a>
                            <a class="dropdown-item"><span class="pull-right"><?= count($this->monikalib->detailInvoiceTeam()['setuju']) ?></span><span class="docs"><i class="fas fa-check"></i> Invoice terbayar</span></a>
                        <?php }
                        //end of invoice>0 ?>
                    </div>
                    <!-- end of invoice -->
                    <?php } ?>
                </div>
            </span>
            <span>
                <a class="dropdown-toggle dropdown text-normal" href="#" data-toggle="dropdown"><?= $this->session->userdata('user_name') ?> <i class="fas fa-angle-down"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="<?= site_url('user') ?>">Profil dan Tim</a>
                    <a class="dropdown-item" href="<?= site_url('info') ?>">Bantuan</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?= site_url('user/logout') ?>">Logout</a>
                </div>
            </span>
            <span><i id="view-history" class="far fa-bell icon icon-click"></i></span>
        </div>
    </div>
</div>
