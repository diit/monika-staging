<div id="history">
    <p class="title">HISTORY DOCUMENT</p>
    <?php if(!empty($log_document)){ ?>
    <ul class="no-point">
        <?php
            foreach ($log_document as $d) {
              $random = md5(mt_rand(1,10000));
              $first = substr($random,0,5);
              $last = substr($random,5,10);
              $urlrand = $first.$d->id.$last;
              switch ($d->action) {
                case 'upload':
                  $act = 'mengunggah foto';
                  break;
                case 'remove':
                  $act = 'menghapus';
                  break;
                case 'edit':
                  $act = 'mengubah';
                  break;
                case 'add':
                  $act = 'menambahkan section';
                  break;
                case 'download':
                  $act = 'download PDF';
                  break;
                case 'kirim':
                  $act = 'mengirim';
                  break;
                case 'share link':
                  $act = 'membagikan link';
                  break;
                case 'permission':
                    $act = 'mengubah hak akses';
                    break;
                case 'update_client':
                    $act = 'mengganti klien';
                    break;
                default:
                  $act = $d->action;
                  break;
              }
          ?>
        <li>
            <p class="riwayat">
                Anda <?= $act ?> <?=  str_replace('_', ' ', $d->section); ?> <a href='<?php echo ($d->section == 'invoice') ? site_url('invoice/detail/'.$urlrand) : site_url('proposal/viewproposal/'.$urlrand) ?>'><?= $d->judul ?></a>.
            </p>
            <p class="date-time">
                <?= $this->monikalib->format_date_indonesia($d->logtime).' '.date('H:i:s',strtotime($d->logtime)); ?>
            </p>
        </li>
        <?php }
        }
        else {
        ?>
        <p class="text-center"><i class="far fa-comment-alt"></i></p>
        <p class="text-center">Belum ada history dokumen dan aktifitas user</p>
        <?php } ?>
    </ul>
</div>
