<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.ico">
    <title><?= $page ?></title>
    <meta name="description" content="Software otomatisasi yang membatu mengelola pemasaran freelancer dan ahensi dalam membuat hingga melacak proposal dan faktur untuk memenangkan klien dalam waktu singkat">
    <meta name="author" content="Monika">
    <meta name="keyword" content="Proposal, Invoice, Penawaran, Lacak Proposal, Tracking, Closing, Proposal Goal">

    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/font-awesome-5/css/all.css">
    <link rel="stylesheet/less" href="<?= base_url() ?>assets/css/style.less">
    <link rel="stylesheet/less" href="<?= base_url() ?>assets/css/style_desktop.less">

    <script src="<?= base_url() ?>assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap-4/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap-4/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/js/less.min.js" ></script>

</head>
<body>
    <!-- content -->
    <?php $this->load->view($content); ?>
    <!-- end of content -->
</body>
</html>
