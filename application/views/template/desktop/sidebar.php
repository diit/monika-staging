<div id="sidebar">
    <img src="<?php echo base_url(); ?>assets/img/monika-brand.png" alt="monika-logo" id="brand">
    <div id="menu" class="text-center">
        <ul class="no-point">
            <a <?php echo ($this->uri->segment(1) == 'home') ? 'class="active"':''; ?> href="<?= site_url('home') ?>"><li><i class="fas fa-th-large"></i></li></a>
            <a <?php echo ($this->uri->segment(1) == 'proposal') ? 'class="active"':''; ?> href="<?= site_url('proposal') ?>"><li><i class="far fa-file-alt"></i></li></a>
            <a <?php echo ($this->uri->segment(1) == 'invoice') ? 'class="active"':''; ?> href="<?= site_url('invoice') ?>"><li><i class="fab fa-wpforms"></i></li></a>
            <a <?php echo ($this->uri->segment(1) == 'klien') ? 'class="active"':''; ?> href="<?= site_url('klien') ?>"><li><i class="far fa-address-book"></i></li></a>
            <a <?php echo ($this->uri->segment(1) == 'arsip') ? 'class="active"':''; ?> href="<?= site_url('arsip') ?>"><li><i class="far fa-file-archive"></i></li></a>
            <a <?php echo ($this->uri->segment(1) == 'plan') ? 'class="active"':''; ?> href="<?= site_url('plan') ?>"><li><i class="far fa-square"></i></li></a>
            <a href="<?= site_url('info') ?>" class="pull-bottom <?php echo ($this->uri->segment(1) == 'info') ? 'active-noborder':''; ?>"><li><i class="fas fa-exclamation-circle"></i></li></a>
        </ul>
    </div>
</div>
