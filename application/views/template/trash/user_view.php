<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Monika">
    <meta name="keyword" content="Proposal, Invoice, Penawaran, Lacak Proposal, Tracking, Closing, Proposal Goal">
    <title><?= $page ?></title>
    <link rel="shortcut icon" href="<?php echo site_url('assets/img/favicon.ico'); ?>">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">

    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lineicons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/content-tools.min.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/monika.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-dimention.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/introjs/introjs.css" rel="stylesheet">
    <script src="<?php echo base_url()?>assets/js/jquery3.2.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap3.3.7.min.js"></script>
    <script src="<?php echo base_url() ?>assets/introjs/intro.js"></script>


  </head>

  <body>

  <section id="container" >
      <!--header start-->
      <header class="header black-bg" style="min-height: 3px;border-bottom: none;">
        </header>
      <!--header end-->
  </section>


  <!-- Start Content -->
  <?php $this->load->view($content); ?>
  <!-- End Content -->
    <script class="include" type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/common-scripts.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/gritter-conf.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>node_modules/sweetalert/dist/sweetalert.min.js"> </script>
    <script src="<?php echo base_url(); ?>assets/content-tools.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/editor.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/screentime.js"></script>

  </body>
</html>
