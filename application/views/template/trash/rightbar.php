<aside>
    <div class="hidden-xs" id="rightbar">
        <?php
            if($detail_proposal)
            {
        ?>
        <div class="toggle-editor">
            <div class="toggle-editor-button">
                <i class="fa fa-angle-double-right fa-2x" aria-hidden="true"></i>
            </div>
        </div>
        <div style="overflow-y: scroll;overflow-x: hidden;max-height: 95%;">
            <div class="row">
                <div class="col-md-12">
                    <h4 style="text-align: center;font-weight: 800;">Editor</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div style="padding: 0 15px;border-bottom: 1px solid rgb(216, 218, 220);">
                        <h4>Header</h4>
                            <div style="margin: 5px;">
                              <label>Font Style</label>
                              <select style="display: inline-block;width: auto; padding: 4px 7px;height: 30px;" class="form-control" id="fontstyle">
                                <option value="Avenir" <?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['fontstyle'] == NULL){ echo 'selected style="font-family: \'Avenir\';"'; }else if($detail_proposal['style']['fontstyle'] == 'Avenir'){ echo 'selected style="font-family: \'Avenir\';"'; }}else{echo 'selected style="font-family: \'Avenir\';"';} ?> >Avenir</option>
                                <option value="Arial" <?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['fontstyle'] == 'Arial'){ echo 'selected style="font-family: Arial;"'; }else{ echo 'style="font-family: Arial;"'; }}else{echo 'style="font-family: Arial;"';} ?>>Arial</option>
                                <option value="Verdana" <?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['fontstyle'] == 'Verdana'){ echo 'selected style="font-family: Verdana;"'; }else{ echo 'style="font-family: Verdana;"'; }}else{echo 'style="font-family: Verdana;"';} ?>>Verdana</option>
                                <option value="Open Sans" <?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['fontstyle'] == 'Open Sans'){ echo 'selected style="font-family: Open Sans;"'; }else{ echo 'style="font-family: Open Sans;"'; }}else{echo 'style="font-family: Open Sans;"';} ?>>Open Sans</option>
                              </select>
                            </div>
                            <div style="margin: 5px;">
                              <label>Header Font Color</label>
                              <input type="text" class="headertextpallete">
                            </div>
                            <div style="margin: 5px;">
                              <label>Header Color</label>
                              <input type="text" class="headerpallete">
                            </div>
                            <!--
                            <div style="margin: 5px;">
                              <label>Layout</label>
                              <div class="row">
                                  <div class="col-md-6">
                                      <img class="img-responsive nav-layout" src="<?php // echo base_url(); ?>assets/img/header-center-style.svg">
                                  </div>
                                  <div class="col-md-6">
                                      <img class="img-responsive nav-layout" src="<?php // echo base_url(); ?>assets/img/header-left-style.svg">
                                  </div>
                              </div>
                            </div>
                            -->

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div style="padding: 15px;border-bottom: 1px solid rgb(216, 218, 220);">
                        <h4>Content</h4>
                            <div style="margin: 5px;">
                              <label>Layout</label>
                              <div class="row">
                                  <div class="col-md-6">
                                      <img class="img-responsive nav-layout layout-wide" src="<?php echo base_url(); ?>assets/img/section-one-column.svg">
                                  </div>
                                  <div class="col-md-6">
                                      <img class="img-responsive nav-layout layout-split" src="<?php echo base_url(); ?>assets/img/section-two-column.svg">
                                  </div>
                              </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            }
        ?>
    </div>
</aside>
