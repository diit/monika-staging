<aside>
    <div id="sidebar"  class="nav-collapse">

        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <li class="mt">
                <a <?php echo ($this->uri->segment(1) == 'home') ? 'class="active"':''; ?>  href="<?php echo site_url('home'); ?>">
                    <i class="fa fa-tachometer" aria-hidden="true"></i>
                    <p style="font-size: 12px;" class="pointbtn5">Dashboard</p>
                    <span class="pointbtn4">Dashboard</span>
                </a>
            </li>
            <li>
                <a <?php echo ($this->uri->segment(1) == 'proposal') ? 'class="active"':''; ?>  href="<?php echo site_url('proposal'); ?>">
                    <i><img src="<?php echo base_url()."/assets/images/dokumen.svg"; ?>"></i>
                    <p style="font-size: 12px;" class="pointbtn5">Proposal</p>
                    <span class="pointbtn4">Proposal</span>
                </a>
            </li>
            <li>
                <a <?php echo ($this->uri->segment(1) == 'invoice') ? 'class="active"':''; ?>  href="<?php echo site_url('invoice'); ?>">
                    <i class="fa fa-list-alt"></i>
                    <p style="font-size: 12px;" class="pointbtn5">Invoice</p>
                    <span class="pointbtn4">Invoice</span>
                </a>
            </li>
            <li>
                <a <?php echo ($this->uri->segment(1) == 'klien') ? 'class="active"':''; ?>  href="<?php echo site_url('klien'); ?>">
                    <i class="fa fa-users"></i>
                    <p style="font-size: 12px;" class="pointbtn5">Klien</p>
                    <span class="pointbtn4">Klien</span>
                </a>
            </li>
            <li>
                <a <?php echo ($this->uri->segment(1) == 'arsip') ? 'class="active"':''; ?> href="<?php echo site_url('arsip'); ?>">
                    <i class="fa fa-archive"></i>
                    <p style="font-size: 12px;" class="pointbtn5">Arsip</p>
                    <span class="pointbtn4">Arsip</span>
                </a>
            </li>

            <li>
                <a <?php echo ($this->uri->segment(1) == 'plan') ? 'class="active"':''; ?> href="<?php echo ($this->monikalib->getUserPlan() == NULL) ? site_url('plan') : site_url('plan/status'); ?>">
                    <i class="fa fa-tag"></i>
                    <p style="font-size: 12px;" class="pointbtn5">Plan</p>
                    <span class="pointbtn4">Plan</span>
                </a>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
