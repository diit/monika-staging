<section id="container">
    <header class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars"></div>
        </div>

        <!--logo start-->
        <a href="<?php echo site_url('home'); ?>" class="logo pointbtn">
            <img width="100" src="<?php echo base_url(); ?>assets/img/monika_logo_white.png">
        </a>
        <!--logo end-->
				<p id="notifsave"></p>
        <div class="top-menu">
            <div class="nav pull-right top-menu">
                <ul class="nav top-menu">
                    <!-- settings start -->
                    <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
                    <li style="margin-right: 20px;" class="hidden-lg hidden-md hidden-sm">
                        <?php echo (14 - $this->session->userdata('trial_day') == 0) ? '' : '<a data-toggle="modal" data-target="#modalTrial" class="btn btn-info btn-sm" style="font-size: 12px; border:none">Sisa trial '.(int)(14 - $this->session->userdata('trial_day')).' hari lagi </a>' ?>
                    </li>
                      <li style="margin-right: 20px;" class="hidden-xs">
                          <span style="color: #ffffff; font-size: 12px;"><?php echo (14 - $this->session->userdata('trial_day') == 0) ? 'Kesempatan terakhir hari ini untuk coba GRATIS Plan Agency <a class="btn btn-info" style="padding: 5px;font-size: 11px;" href="'.site_url('plan').'">UPGRADE</a>':'Masa coba GRATIS Plan Agency tersisa '. (int) (14 - $this->session->userdata('trial_day')).' hari lagi <a class="btn btn-info" style="padding: 5px;font-size: 11px;" href="'.site_url('plan').'">BENEFIT</a>'; ?></span>
                      </li>
                    <?php } ?>
                    <li id="statpro" class="dropdown" style="margin-right:20px">
                        <a data-toggle="dropdown" class="dropdown-toggle tooltips" href="#" data-placement="bottom" data-original-title="Status Dokumen">
                            <i class="fa fa-tasks"></i>
                            <span class="badge bg-theme">
                              <?php
                              if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1)
                              {
                                if ($this->monikalib->getTotalProposalByTeam()<=0 && $this->monikalib->getTotalInvoiceByTeam()<=0) {
                                      echo 0;
                                } else {
                                      echo ((int) $this->monikalib->getTotalProposalByTeam() + (int) $this->monikalib->getTotalInvoiceByTeam());
                                }
                              }
                              else
                              {
                                if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
                                {
                                  if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
                                  {
                                    if ($this->monikalib->getTotalProposalByTeam()<=0 && $this->monikalib->getTotalInvoiceByTeam()<=0) {
                                      echo 0;
                                    } else {
                                      echo ((int) $this->monikalib->getTotalProposalByTeam() + (int) $this->monikalib->getTotalInvoiceByTeam());
                                    }
                                  }
                                  else
                                  {
                                    if ($this->monikalib->getTotalProposal()<=0 && $this->monikalib->getTotalInvoice()<=0) {
                                      echo 0;
                                    } else {
                                      echo ((int) $this->monikalib->getTotalProposal() + (int) $this->monikalib->getTotalInvoice());
                                    }
                                  }
                                }
                                else
                                {
                                   if ($this->monikalib->getTotalProposal()<=0 && $this->monikalib->getTotalInvoice()<=0) {
                                      echo 0;
                                    } else {
                                      echo ((int) $this->monikalib->getTotalProposal() + (int) $this->monikalib->getTotalInvoice());
                                    }
                                }
                              }
                              ?>
                            </span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right extended tasks-bar">
                            <div class="notify-arrow-prop notify-arrow-green"></div>
                            <li>
                                <p class="green">
                                  <?php
                                    if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1)
                                    {
                                      if ($this->monikalib->getTotalProposalByTeam()<=0 && $this->monikalib->getTotalInvoiceByTeam()<=0) {
                                            echo 'Anda dan tim belum membuat proposal / invoice';
                                      }else{
                                            echo 'Anda memiliki '.((int)$this->monikalib->getTotalProposalByTeam() + (int) $this->monikalib->getTotalInvoiceByTeam()).' dokumen';
                                      }
                                    }
                                    else
                                    {
                                      if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
                                      {
                                        if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
                                        {
                                          if ($this->monikalib->getTotalProposalByTeam()<=0 && $this->monikalib->getTotalInvoiceByTeam()<=0) {
                                            echo 'Anda dan tim belum membuat proposal / invoice';
                                          }else{
                                            echo 'Anda memiliki '.((int)$this->monikalib->getTotalProposalByTeam() + (int) $this->monikalib->getTotalInvoiceByTeam()).' dokumen';
                                          }
                                        }
                                        else
                                        {
                                          if ($this->monikalib->getTotalProposal()<=0 && $this->monikalib->getTotalInvoice()<=0) {
                                            echo 'Anda belum membuat proposal / invoice';
                                          } else {
                                            echo 'Anda memiliki '.((int) $this->monikalib->getTotalProposal() + (int)$this->monikalib->getTotalInvoice()).' dokumen';
                                          }
                                        }
                                      }
                                      else
                                      {
                                        if ($this->monikalib->getTotalProposal()<=0 && $this->monikalib->getTotalInvoice()<=0) {
                                            echo 'Anda belum membuat proposal / invoice';
                                          } else {
                                            echo 'Anda memiliki '.((int)$this->monikalib->getTotalProposal() + (int)$this->monikalib->getTotalInvoice()).' dokumen';
                                          }
                                      }
                                    }
                                  ?>
                                </p>
                            </li>
                            <li>
                                <a>
                                    <div class="task-info">
                                        <div class="row">
                                            <div class="col-xs-8"><i class="fa fa-eye"></i> Proposal dibaca</div>
                                            <div class="col-xs-4 text-right">
                                              <?php
                                                if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1)
                                                {
                                                  echo $this->monikalib->detailProposalTeam()['baca'];
                                                }
                                                else
                                                {
                                                  if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
                                                  {
                                                    if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
                                                    {
                                                      echo $this->monikalib->detailProposalTeam()['baca'];
                                                    }
                                                    else
                                                    {
                                                      echo $this->monikalib->detailProposal()['baca'];
                                                    }
                                                  }
                                                  else
                                                  {
                                                    echo $this->monikalib->detailProposal()['baca'];
                                                  }
                                                }
                                              ?>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <div class="task-info">
                                        <div class="row">
                                            <div class="col-xs-8"><i class="fa fa-check"></i> Proposal disetujui</div>
                                            <div class="col-xs-4 text-right">
                                              <?php
                                                if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1)
                                                {
                                                  echo $this->monikalib->detailProposalTeam()['setuju'];
                                                }
                                                else
                                                {
                                                  if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
                                                  {
                                                    if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
                                                    {
                                                      echo $this->monikalib->detailProposalTeam()['setuju'];
                                                    }
                                                    else
                                                    {
                                                      echo $this->monikalib->detailProposal()['setuju'];
                                                    }
                                                  }
                                                  else
                                                  {
                                                    echo $this->monikalib->detailProposal()['setuju'];
                                                  }
                                                }
                                              ?>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <div class="task-info">
                                        <div class="row">
                                            <div class="col-xs-8"><i class="fa fa-eye"></i> Invoice dibaca</div>
                                            <div class="col-xs-4 text-right">
                                              <?php
                                                if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1)
                                                {
                                                  echo $this->monikalib->detailInvoiceTeam()['baca'];
                                                }
                                                else
                                                {
                                                  if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
                                                  {
                                                    if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
                                                    {
                                                      echo $this->monikalib->detailInvoiceTeam()['baca'];
                                                    }
                                                    else
                                                    {
                                                      echo $this->monikalib->detailInvoice()['baca'];
                                                    }
                                                  }
                                                  else
                                                  {
                                                    echo $this->monikalib->detailInvoice()['baca'];
                                                  }
                                                }
                                              ?>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <div class="task-info">
                                        <div class="row">
                                            <div class="col-xs-8"><i class="fa fa-check"></i> Proposal disetujui</div>
                                            <div class="col-xs-4 text-right">
                                              <?php
                                                if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1)
                                                {
                                                  echo $this->monikalib->detailInvoiceTeam()['setuju'];
                                                }
                                                else
                                                {
                                                  if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
                                                  {
                                                    if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
                                                    {
                                                      echo $this->monikalib->detailInvoiceTeam()['setuju'];
                                                    }
                                                    else
                                                    {
                                                      echo $this->monikalib->detailInvoice()['setuju'];
                                                    }
                                                  }
                                                  else
                                                  {
                                                    echo $this->monikalib->detailInvoice()['setuju'];
                                                  }
                                                }
                                              ?>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <!--<li class="external">
                                <a href="<?php // echo site_url('home');?>">Lihat Semua proposal</a>
                            </li>-->
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle hidden-xs" href="#" style="border:none; font-size:16px">
                            <?php echo $this->session->userdata('user_name') ?> <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        </a>
                        <a data-toggle="dropdown" class="dropdown-toggle hidden-sm hidden-md hidden-lg" href="#" style="border:none; font-size:16px">
                            <i class="fa fa-user"></i> <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-akun">
                            <li><a href="<?php echo site_url('user/setting'); ?>" class="btn-editprofil" uid="<?=$this->session->userdata('user_id')?>">Profil & Tim</a></li>
                            <?php if($this->session->userdata('role') == 1){ ?>
                            <li><a href="#" class="btn-editkategori" uid="<?=$this->session->userdata('user_id')?>">Tentang Usaha</a></li>
                            <?php } ?>
                            <li><a href="<?php echo site_url('syarat-ketentuan'); ?>">Syarat dan Ketentuan</a></li>
                            <li><a href="<?php echo site_url('kebijakan-privasi'); ?>">Kebijakan Privasi</a></li>
                            <li><a href="<?php echo site_url('faq'); ?>">Pertanyaan Umum</a></li>
                            <li><a href="<?php echo site_url('cara-membuat-proposal'); ?>">Bantuan</a></li>
                            <li style="margin-top:10px; padding-top:10px; border-top:1px solid #fff"><a href="<?php echo base_url('user/logout');?>">Keluar</a></li>
                        </ul>
                    </li>
                    <!-- settings end -->
                </ul>
            </div>
        </div>
    </header>
</section>

<!-- Modal -->
<div class="modal fade" id="modalTrial" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body text-center">
        <p style="font-weight:bold">Masa coba GRATIS Plan Agency tersisa <?php echo (int) (14 - $this->session->userdata('trial_day')); ?> hari lagi</p>
        <a class="btn btn-info btn-sm" href="<?= site_url('plan') ?>">BENEFIT</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
