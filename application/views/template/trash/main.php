<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Software otomatisasi yang membatu mengelola pemasaran freelancer dan ahensi dalam membuat hingga melacak proposal dan faktur untuk memenangkan klien dalam waktu singkat">
    <meta name="author" content="Monika">
    <meta name="keyword" content="Proposal, Invoice, Penawaran, Lacak Proposal, Tracking, Closing, Proposal Goal">

    <?php $this->load->view('template/header'); ?>
    <?php $this->load->view($t_head); ?>
    <!-- insert $title & view on$t_head -->
</head>
<body>
    <?php $this->load->view('template/navbar'); ?>

    <?php $this->load->view('template/sidebar'); ?>

    <?php $this->load->view($content); ?>

    <?php
        if($content == 'proposal/view_proposal.php')
        {
            if($detail_proposal && $sign < 2)
            {
                $this->load->view('template/rightbar');
            }
        }
    ?>

    <!-- Modal Tentang Usaha -->
    <div class="modal" id="category_edit" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true"<?php echo ($this->session->userdata('role') == 1 && $tutorial == 0 && ($this->session->userdata('user_category') == NULL || $this->session->userdata('user_about') == NULL))?'data-backdrop="static" data-keyboard="false"':''; ?> >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center" id="" style="font-weight:bold"><?php echo ($this->session->userdata('role') == 1 && $tutorial == 0 && ($this->session->userdata('user_category') == NULL || $this->session->userdata('user_about') == NULL))?'Selamat Datang di Monika':'Tentang Usaha'; ?></h4>
                </div>
                <div class="modal-body pilihCat">
                <?php if($this->session->userdata('user_category') == NULL){ ?>
                    <div class="col-xs-12">
                        <div class="alert alert-success" role="alert" style="font-size: 16px;">
                        Untuk mempermudah proses selanjutnya, ada baiknya kesediaan Anda untuk mengisi informasi yang berkaitan dengan usaha. Monika.id selalu berusaha membuat proses promosi dan penjualan Anda semakin menghemat waktu.
                        </div>
                    </div>
                    <?php } ?>

                    <div class="wizard">
                        <div class="wizard-inner">
                        <div class="<?php echo ($this->session->userdata('role') == 1 && $tutorial == 0 && ($this->session->userdata('user_category') == NULL || $this->session->userdata('user_about') == NULL))?'connecting-line-intro':'connecting-line'; ?>"></div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#tentang-usaha-1" data-toggle="tab" aria-controls="tentang-usaha-1" role="tab" title="Ketegori usaha">
                                    <span class="round-tab"><i class="glyphicon glyphicon-briefcase"></i></span>
                                </a>
                            </li>

                            <li role="presentation" class="disabled">
                                <a href="#tentang-usaha-2" data-toggle="tab" aria-controls="tentang-usaha-2" role="tab" title="Deskripsi usaha">
                                    <span class="round-tab"><i class="glyphicon glyphicon-pencil"></i></span>
                                </a>
                            </li>
                            <!--
                            <li role="presentation" class="disabled">
                                <a href="#tentang-usaha-3" data-toggle="tab" aria-controls="tentang-usaha-3" role="tab" title="Skill">
                                    <span class="round-tab"><i class="glyphicon glyphicon-tasks"></i></span>
                                </a>
                            </li>
                            -->
                            <li role="presentation" class="disabled">
                                <a href="#tentang-usaha-4" data-toggle="tab" aria-controls="tentang-usaha-4" role="tab" title="Portfolio">
                                    <span class="round-tab"><i class="glyphicon glyphicon-book"></i></span>
                                </a>
                            </li>
                        </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane active" role="tabpanel" id="tentang-usaha-1">
                                <h4 style="font-weight: bold;margin-left: 18px;">Pilih kategori usaha Anda</h4>
                                <?php foreach ($this->monikalib->getCategory() as $lc){ ?>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="concat <?php echo $this->session->userdata('user_category') == $lc->id ? 'concat_active':''; ?>" catid="<?php echo $lc->id; ?>">
                                        <label>
                                        <input type="radio" name="userCat_edit" id="cat_<?php echo $lc->id; ?>" value="<?php echo $lc->id; ?>" <?php echo $this->session->userdata('user_category') == $lc->id ? 'checked':''; ?>>
                                        <i class="fa <?php echo $lc->icon_fa;?>" aria-hidden="true"></i> <?php echo $lc->kategori; ?>
                                        </label>
                                        <i style="position: absolute;right: 23px;top: 35%; <?php echo $this->session->userdata('user_category') == $lc->id ?'display: block;':'display: none;'; ?>" id="check_<?php echo $lc->id; ?>" class="fa fa-check check_active"></i>
                                    </div>
                                </div>
                                <?php } ?>
                                <ul class="list-inline pull-right btn-aksi" style="margin-top:40px">
                                    <li><button type="button" class="btn btn-primary next-step">Simpan dan selanjutnya</button></li>
                                </ul>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="tentang-usaha-2">
                                <h4 style="font-weight: bold;margin-left: 18px;">Jelaskan secara rinci tentang Anda dan usaha yang dijalankan </h4>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="aboutme_edit"><?php
                                        $about = $this->session->userdata('user_about');
                                        if($about == NULL) $about = '';
                                        echo trim($about);
                                        ?></textarea>
                                    </div>
                                </div>

                                <ul class="list-inline pull-right btn-aksi" style="margin-top:40px">
                                    <li><button type="button" class="btn btn-default prev-step">Sebelumnya</button></li>
                                    <li><button type="button" class="btn btn-primary next-step">Simpan dan selanjutnya</button></li>
                                </ul>
                            </div>
                            <!--
                            <div class="tab-pane" role="tabpanel" id="tentang-usaha-3">
                                <h4 style="font-weight: bold;margin-left: 18px;">Keahlian</h4>
                                <div class="col-md-12 listskill">
                                    <?php // if($this->session->userdata('userskill') != NULL && count($this->session->userdata('userskill')) > 0){ ?>
                                    <?php // $cs = count($this->session->userdata('userskill'))-1; foreach ($this->session->userdata('userskill') as $key => $userskill){ ?>
                                    <div id="skill<?php // echo $key; ?>">
                                        <input style="float: left;width: 90%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" id="inputskill<?php // echo $key; ?>" name="skill" type="text" class="form-control skill" value="<?php // echo $userskill; ?>" placeholder="Skill<?php // echo $key+1; ?>" maxlength="50">
                                        <?php // if($key==0){ ?>
                                        <button style="margin-top: 3px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm addskill"><i class="fa fa-plus"></i></button>
                                        <?php // }else{ ?>
                                        <button skillid="<?php // echo $key; ?>" style="margin-top: 5px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm deleteskill"><i class="fa fa-trash"></i></button>
                                        <?php // } ?>
                                    </div>
                                    <?php // } ?>
                                    <?php // }else{ $cs = 3; ?>
                                    <div id="skill1">
                                        <input style="float: left;width: 90%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="skill" type="text" id="inputskill1" class="form-control skill" placeholder="Skill 1" maxlength="50">
                                        <button style="margin-top: 3px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm addskill"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div id="skill2">
                                        <input style="float: left;width: 90%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="skill" type="text" id="inputskill2" class="form-control skill" placeholder="Skill 2" maxlength="50">
                                        <button skillid="2" style="margin-top: 5px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm deleteskill"><i class="fa fa-trash"></i></button>
                                    </div>
                                    <div id="skill3">
                                        <input style="float: left;width: 90%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="skill" type="text" id="inputskill3" class="form-control skill" placeholder="Skill 3" maxlength="50">
                                        <button skillid="3" style="margin-top: 5px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm deleteskill"><i class="fa fa-trash"></i></button>
                                    </div>
                                    <?php // } ?>
                                </div>

                                <ul class="list-inline pull-right btn-aksi" style="margin-top:40px">
                                    <li><button type="button" class="btn btn-default prev-step">Sebelumnya</button></li>
                                    <li><button type="button" class="btn btn-primary btn-info-full next-step">Simpan dan selanjutnya</button></li>
                                </ul>
                            </div>
                            -->
                            <div class="tab-pane" role="tabpanel" id="tentang-usaha-4">
                                <h4 style="font-weight: bold;margin-left: 18px;">Portfolio / Pengalaman</h4>
                                <div class="col-md-12 listportfolio">
                                    <?php if($this->session->userdata('userportfolio') != NULL && count($this->session->userdata('userportfolio')) > 0){ ?>
                                    <?php $th = count($this->session->userdata('userportfolio'))-1; foreach ($this->session->userdata('userportfolio') as $key => $userportfolio){ ?>
                                    <div id="portfolio<?php echo $key; ?>">
                                        <input style="float: left;width: 90%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="portfolio" value="<?php echo $userportfolio; ?>" type="text" id="inputportfolio<?php echo $key; ?>" class="form-control portfolio" placeholder="Portfolio / pengalaman <?php echo $key+1; ?>" maxlength="50">
                                        <?php if($key==0){ ?>
                                        <button style="margin-top: 3px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm addportfolio"><i class="fa fa-plus"></i></button>
                                        <?php }else{ ?>
                                        <button portfolioid="<?php echo $key; ?>" style="margin-top: 5px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm deleteportfolio"><i class="fa fa-trash"></i></button>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <?php }else{ $th = 3; ?>
                                    <div id="portfolio1">
                                        <input style="float: left;width: 90%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="portfolio" type="text" id="inputportfolio1" class="form-control portfolio" placeholder="Portfolio / pengalaman 1" maxlength="70">
                                        <button portfolioid="1" style="margin-top: 3px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm addportfolio"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div id="portfolio2">
                                        <input style="float: left;width: 90%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="portfolio" type="text" id="inputportfolio2" class="form-control portfolio" placeholder="Portfolio / pengalaman 2" maxlength="70">
                                        <button portfolioid="2" style="margin-top: 5px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm deleteportfolio"><i class="fa fa-trash"></i></button>
                                    </div>
                                    <div id="portfolio3">
                                        <input style="float: left;width: 90%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="portfolio" type="text" id="inputportfolio3" class="form-control portfolio" placeholder="Portfolio / pengalaman 3" maxlength="70">
                                        <button portfolioid="3" style="margin-top: 5px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm deleteportfolio"><i class="fa fa-trash"></i></button>
                                    </div>
                                    <?php } ?>
                                </div>

                                <ul class="list-inline pull-right btn-aksi" style="margin-top:40px">
                                    <li><button type="button" class="btn btn-default prev-step">Sebelumnya</button></li>
                                    <li><button class="btn btn-info btn-save-business-detail" id="simpan_business_detail"><i class="fa fa-check" aria-hidden="true"></i>Simpan</button></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="border-top:none">
                </div>
            </div>
        </div>
    </div>
<!-- End of Modal Tentang Usaha -->

    <?php $this->load->view('template/footer'); ?>

    <?php $this->load->view($t_foot); ?>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();

        $('.btn-editkategori').on('click',function(){
            $('#category_edit').modal('show');
            $('#category_edit').modal({backdrop: 'static',keyboard: false});
        });

        $(document).on('click','.btn-save-business-detail',function(){
            var ini = $('.btn-save-about');
            var aboutme = $('#aboutme_edit').val();
            var kategori = $('input[name="userCat_edit"]:checked').val();
            /*
            var skills=[];
            $('[name="skill"]').each(function(){
                if($(this).val() != '')
                {
                    skills.push($(this).val());
                }
            });
            */

            var portfolio=[];
            $('[name="portfolio"]').each(function(){
                if($(this).val() != '')
                {
                    portfolio.push($(this).val());
                }
            });

            if(kategori==undefined)
            {
                swal('Mohon pilih kategori usaha Anda');
            }
            else if(aboutme == '')
            {
                swal('Mohon isi tentang Anda dan usaha yang dijalankan');
            }
            /*
            else if(skills.length <= 0 || skills[0] == '')
            {
                swal('Mohon isi keahlian Anda');
            }
            */
            else if(portfolio.length <= 0 || portfolio[0] == '')
            {
                swal('Mohon isi pengalaman projek yang pernah Anda kerjaan');
            }
            else
            {
                ini.attr('disable',true);
                ini.html('Menyimpan...');
                $('#simpan_business_detail').prop('disabled',true).html('Menyimpan...');
                $.ajax({
                    type:"POST",
                    url: "<?php echo base_url()?>home/simpan_business_detail",
                    data: {
                        kategori: kategori,
                        about: aboutme,
                        //skill: skills,
                        portfolio: portfolio
                    },
                    success: function(data){
                        ini.attr('disable',false);
                        ini.html('<i class="fa fa-check" aria-hidden="true"></i>Simpan');
                        if(data == 1)
                        {
                            $('#category_edit').modal('hide');
                            swal('Berhasil','Detail usaha berhasil disimpan','success')
                            .then((value) => {
                              location.reload();
                            });
                        }
                        else
                        {
                            swal('Ada kesalahan');
                        }
                    }, error:function(error){
                        swal('Ada kesalahan nih');
                    }
                });
            }
        });

        <?php if($this->session->userdata('role') == 1 && $tutorial == 0 && ($this->session->userdata('user_category') == NULL || $this->session->userdata('user_about') == NULL)){ ?>
            //var kategori;
            //var aboutme;
            $('#category_edit').modal('show');
            $('#category_edit').modal({backdrop: 'static', keyboard: false});
        <?php } ?>

        $('.concat').on('click',function() {
            $('.concat').removeClass('concat_active');
            $('.check_active').css('display','none');
            var catid = $(this).attr('catid');
            $('#cat_'+catid).prop("checked", true);
            $(this).addClass('concat_active');
            $('#check_'+catid).css('display','block');
        });

        /*
        var cs = <?php // echo $cs; ?>;
        $(document).on('click','.addskill',function(){
            var divaddskill = $('<div id="skill'+ parseInt(cs+1) +'"></div>');
            var inputskill = $('<input style="float: left;width: 90%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="skill" id="inputskill'+ parseInt(cs+1) +'" type="text" class="form-control skill" placeholder="Skill '+ parseInt(cs+1) +'" maxlength="50">');
            var btndeleteskill = $('<button skillid="'+ parseInt(cs+1) +'" style="margin-top: 5px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm deleteskill"><i class="fa fa-trash"></i></button>');
            var latesskill = $('#inputskill'+cs).val();
            if(latesskill == '')
            {
                swal("Isikan keahlian pada isian terakhir");
            }
            else
            {
                cs++;
                $('.listskill').append(divaddskill.append(inputskill).append(btndeleteskill));
                inputskill.select();
            }
        });

        $(document).on('click','.deleteskill',function(){
            var skillid = $(this).attr('skillid');
            $('#skill'+skillid).remove();
            cs--;
        });
        */

        var th = <?php echo $th; ?>;
        $(document).on('click','.addportfolio',function(){
            var divaddportfolio = $('<div id="portfolio'+ parseInt(th+1) +'"></div>');
            var inputportfolio = $('<input style="float: left;width: 90%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="portfolio" id="inputportfolio'+ parseInt(th+1) +'" type="text" class="form-control portfolio" placeholder="Portfolio '+ parseInt(th+1) +'" maxlength="70">');
            var btndeleteportfolio = $('<button portfolioid="'+ parseInt(th+1) +'" style="margin-top: 5px;padding: 5px 10px;font-size: 12px;border-radius: 3px;" type="button" class="btn btn-default btn-sm deleteportfolio"><i class="fa fa-trash"></i></button>');
            var latestportfolio = $('#inputportfolio'+th).val();
            if(latestportfolio == '')
            {
                swal("Isikan portfolio / pengalaman pada isian terakhir");
            }
            else
            {
                th++;
                $('.listportfolio').append(divaddportfolio.append(inputportfolio).append(btndeleteportfolio));
                inputportfolio.select();
            }
        });

        $(document).on('click','.deleteportfolio',function(){
            var portfolioid = $(this).attr('portfolioid');
            $('#portfolio'+portfolioid).remove();
            th--;
        });
    });
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a619ebb4b401e45400c3764/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
    <!-- insert view on $t_foot -->
</body>
</html>
