<title>MONIKA - <?= $page ?></title>
<link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.ico">

<link href="<?= base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lineicons/style.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/easyWizard.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/content-tools.min.css">
<link href="<?= base_url() ?>assets/css/spectrum.css" rel="stylesheet">
<!-- custom style -->
<link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/monika.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/style-responsive.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style-dimention.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/introjs/introjs.css">

<!-- main js -->
<script src="<?= base_url() ?>assets/js/jquery3.2.min.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap3.3.7.min.js"></script>

<!-- Hotjar Tracking Code for https://app.monika.id -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:751855,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115539143-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115539143-1');
</script>

<!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1971319419754936');
    fbq('track', 'PageView');
  </script>

  <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1971319419754936&ev=PageView&noscript=1"
  /></noscript>
<!-- End Facebook Pixel Code -->
