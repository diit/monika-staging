<?php
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
$pdf= new FPDF('P','mm','A4'); //L For Landscape / P For Portrait
$pdf->AddPage();
$pdf->AliasNbPages();
$pdf->AddFont('Avenir','','avenir.php');
$pdf->AddFont('Verdana','','verdana.php');
$pdf->AddFont('OpenSans','','opensans-regular.php');
$pdf->AddFont('Pecita','','pecita.php');
$pdf->SetTitle('Invoice');

//die(var_dump($get_style));
//Header
$pdf->SetFont('Avenir','',25);
$pdf->Ln(20);
$pdf->Cell(5);
$pdf->Cell(10,5,'Invoice',0,0,'L');

if ((!empty($invoice->number))){
    $pdf->SetFont('Avenir','',12);
    $pdf->Ln(10);
    $pdf->Cell(5);
    $pdf->Cell(10,5,'# '.$invoice->number,0,0,'L');
}

if (!empty($get_style->logoimageurl)){
    $gambar = $get_style->logoimageurl;
    $pdf->Image($gambar,145,30,40,0);
} else{
    $pdf->Cell(0,0,'',0,0,'L');
}

$pdf->SetFont('Avenir','',10);
$pdf->Ln(25);
$pdf->Cell(5);
$pdf->Cell(10,5,'DARI',0,0,'L');

$pdf->SetFont('Avenir','',12);
$pdf->Ln(9);
$pdf->Cell(5);
$pdf->Cell(10,5,$invoice->user_name,0,0,'L');

if ((!empty($invoice->due_invoice))){
    $pdf->SetFont('Avenir','',10);
    $pdf->Ln(15);
    $pdf->Cell(5);
    $pdf->Cell(130,5,'KEPADA',0,0,'L');
    $pdf->SetFont('Avenir','',10);
    $pdf->Cell(49,5,'Batas akhir pembayaran',0,0,'R');

    $pdf->SetFont('Avenir','',12);
    $pdf->Ln(9);
    $pdf->Cell(5);
    $pdf->Cell(130,5,$invoice->nama_pic,0,0,'L');
    $tgl = $this->monikalib->format_date_indonesia($invoice->due_invoice);
    $pdf->Cell(49,5,$tgl,0,0,'R');
}

$pdf->SetFont('Avenir','',12);
$pdf->Ln(7);
$pdf->Cell(5);
$pdf->Cell(10,5,(empty($invoice->perusahaan))? '' : $invoice->perusahaan,0,'L');
$pdf->Ln(7);
$pdf->Cell(5);
$pdf->Cell(10,5,(empty($invoice->alamat_usaha))? '' : $invoice->alamat_usaha,0,'L');
$pdf->Ln(7);
$pdf->Cell(5);
$pdf->Cell(10,5,(empty($invoice->kota))? '' : $invoice->kota,0,'L');

//item invoice
$pdf->SetFont('Avenir','',10);
$pdf->Ln(20);
$pdf->Cell(5);
$pdf->Cell(130,10,'ITEM DESKRIPSI','T,B',0,'C');
$pdf->Cell(49,10,'BIAYA','T,B',0,'C');

$pdf->SetFont('Avenir','',10);
$pdf->Ln(2);
$sub_tot = 0;
foreach($item as $d){
    $pdf->Ln(10);
    $pdf->Cell(5);
    $pdf->Cell(130,10,$d->item,0,0,'L');
    $pdf->Cell(49,10,'Rp '.number_format($d->harga,"0",",","."),0,0,'R');
    $sub_tot = $sub_tot+($d->harga);
}

$pdf->SetFont('Avenir','',12);
$pdf->Ln(12);
$pdf->Cell(5);
$pdf->Cell(130,10,'SUB TOTAL','T',0,'R');
$pdf->Cell(49,10,'Rp '.number_format($sub_tot,"0",",","."),'T',0,'R');

$pdf->Ln(10);
$pdf->Cell(5);
$tax_price = ($invoice->tax/100)*$sub_tot;
$pdf->Cell(130,6,'TAX '.number_format($invoice->tax,"0",",",".").'%',0,0,'R');
$pdf->Cell(49,6,'Rp '.number_format($tax_price,"0",",","."),0,0,'R');

$pdf->Ln(5);
$pdf->Cell(5);
$pdf->Cell(130,10,'TOTAL',0,0,'R');
 $tot = $sub_tot + $tax_price;
$pdf->Cell(49,10,'Rp '.number_format($tot,"0",",","."),0,0,'R');

if (!empty($invoice->note)){
    $pdf->SetFont('Avenir','',12);
    $pdf->Ln(30);
    $pdf->Cell(5);
    $pdf->Cell(10,5,'Catatan :',0,'L');
    $pdf->Ln(6);
    $pdf->Cell(5);
    $pdf->MultiCell(0,5,$invoice->note,0,'L');
}

$pdf->Output('I','Invoice - '.$invoice->judul.'.pdf');





?>
