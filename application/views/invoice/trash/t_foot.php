<script src="<?= base_url() ?>assets/js/bootstrap-tagsinput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
$(document).ready(function(){
    $('#cariInvoice').keyup(function(){
    var query = $(this).val();
      $.ajax({
        url:"<?php echo base_url();?>invoice",
        method:"post",
        data:{query:query},
        success:function(data){
          $('.list_invoice').remove();
          $('#hasilCariInvoice').html(data);
        }
      });
    });
});
</script>
