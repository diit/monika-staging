<section id="main-content">
  <section class="wrapper">
    <div class="row mt">
        <div class="container">
            <div class="col-xs-12">
                <?php if($flag == 0) { ?>
                  <h3 style="margin-top:10px">INVOICE</h3>
                  <div class="col-md-12 col-sm-4 mb">
                    <div class="empty-panel">
                      <h1 class="mt"><i class="fa fa-list-alt fa-3x"></i></h1>
                        <div class="darkblue-header">
                            <h2>Data invoice kosong.</h2>
                            <h5>Anda bisa mulai dengan klik generate invoice dari proposal di menu Dashboard. </h5>
                            <h5>Atau klik tombol "Buat Invoice Baru" di bawah. </h5>
                            <a href="<?= site_url('invoice/baru') ?>" class="btn btn-theme03">
                                <i class="fa fa-plus" aria-hidden="true"></i> Buat Invoice Baru
                            </a>
                        </div>
                    </div>
                  </div>
                <?php } else { ?>
                  <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
                    <a style="float:right" href="<?= site_url('invoice/baru') ?>" class="btn btn-theme03">
                      <i class="fa fa-plus" aria-hidden="true"></i> Buat Invoice
                    </a>
                  <?php }else{ ?>
                      <?php if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1){ ?>
                        <a style="float:right" href="<?= site_url('invoice/baru') ?>" class="btn btn-theme03">
                          <i class="fa fa-plus" aria-hidden="true"></i> Buat Invoice
                        </a>
                      <?php }else{ ?>
                        <?php if((int) $this->monikalib->getTotalProposal() + (int) $this->monikalib->getTotalInvoice() >= 2 + $this->monikalib->getTotalReferral()){ ?>
                          <button class="btn btn-theme03 pull-right" id="reachlimit">
                            <i class="fa fa-plus" aria-hidden="true"></i> Buat Invoice
                          </button>
                        <?php }else{ ?>
                          <a style="float:right" href="<?= site_url('invoice/baru') ?>" class="btn btn-theme03">
                            <i class="fa fa-plus" aria-hidden="true"></i> Buat Invoice
                          </a>
                        <?php } ?>
                      <?php } ?>
                  <?php } ?>
                      <h3 style="margin-left:0px; margin-top:10px">INVOICE</h3>
                       <!--Kolom pencarian-->
                        <div class="form-group" style="margin:20px 0 20px 0">
                          <input type="text" class="form-control" id="cariInvoice" placeholder="Cari Projek atau Nama PIC klien anda">
                        </div>
                        <!--Kolom pencarian-->
                        <div id="hasilCariInvoice">

                        </div>
                      <?php foreach($listInvoice as $d) { ?>
                        <div class="col-xs-12 mb-short list_invoice" id="div_list_invoice_<?= $d->id ?>">
                         <!-- WHITE PANEL - TOP USER -->
                         <div  style="height:auto;text-align:left;" class="row white-panel pn">
                             <div class="row">
                                 <div class="container" style="width:100%">
                                     <?php
                                        $random = md5(mt_rand(1,10000));
                                        $first = substr($random,0,5);
                                        $last = substr($random,5,10);
                                        $urlrand = $first.$d->id.$last;
                                     ?>
                                     <a href="<?php echo site_url('invoice/lihat/'.$urlrand); ?>">
                                     <div class="col-xs-8 white-header">
                                         <h5><b><?= $d->judul ?></b></h5>
                                     </div>
                                     </a>
                                     <?php if($d->status == 0) { ?>
                                     <div class="col-xs-3 col-xs-offset-1" id="proposal_status">
                                         <div class="label label-warning text-right prop-stat">Draft / Belum Dibaca</div>
                                     </div>
                                     <?php }else if($d->status == 1) { ?>
                                     <div class="col-xs-3 col-xs-offset-1" id="proposal_status">
                                         <div class="label label-primary text-right prop-stat">Sudah Dibaca</div>
                                     </div>
                                     <?php }else if($d->status == 2) { ?>
                                     <div class="col-xs-3 col-xs-offset-1" id="proposal_status">
                                         <div class="label label-primary text-right prop-stat">LUNAS</div>
                                     </div>
                                     <?php } ?>
                                  </div>
                             </div>
                           <div class="row">
                             <div class="col-md-4 col-sm-2 col-xs-12">
                               <p class="mt"><b>Nama PIC</b></p>
                               <p class="pcontent"><?= $d->nama_pic ?></p>
                             </div>
                             <div class="col-md-2 col-sm-2 col-xs-12">
                               <p class="mt"><b>Tanggal Kirim</b></p>
                               <p class="pcontent"><?= ($d->send_invoice=='')? 'Not Set' : $this->monikalib->format_date_indonesia($d->send_invoice) ?></p>
                             </div>
                             <div class="col-md-2 col-sm-2 col-xs-12">
                               <p class="mt"><b>Deadline Pembayaran</b></p>
                               <p class="pcontent"><?= ($d->due_invoice=='')? 'Not Set' : $this->monikalib->format_date_indonesia($d->due_invoice) ?></p>
                             </div>
                             <div class="col-md-2 col-sm-2 col-xs-12">
                                 <?php
                                  $CI =& get_instance();
                                  $CI->load->model('invoice_model');
                                  $result = $CI->invoice_model->getItem($d->id);
                                  $total = 0;
                                  foreach($result  as $row){
                                      $total = $total + $row->harga;
                                  }
                                  $tagihan = ((($d->tax)/100)*$total)+$total;
                                 ?>
                               <p class="mt"><b>Total Tagihan</b></p>
                               <p class="pcontent">Rp <?= number_format($tagihan,'0',',','.') ?></p>
                              </div>
                               <div class="col-md-2 col-sm-6 col-xs-12">
                                  <!-- Tombol Aksi -->
                                    <span class="btn-group group-action">
                                        <a href="<?php echo site_url('invoice/lihat/'.$urlrand); ?>" class="btn btn-default edit-invoice tooltips" data-placement="top" data-original-title="Edit">
                                            <i class="fa fa-fw fa-pencil"></i>
                                        </a>
                                        <a class="btn btn-default arsip-in tooltips" data-placement="top" data-original-title="Arsipkan" iid="<?= $d->id ?>">
                                            <i class="fa fa-fw fa-archive"></i>
                                        </a>
                                   </span>
                                  <!-- /Tombol Aksi -->
                               </div>
                           </div>
                         </div>
                            <span style="font-size:10px;position: absolute;right: 10px;bottom: 5px;">Dibuat pada <?php echo $this->monikalib->format_date_indonesia($d->created_at); ?></span>
                      </div>
                  <?php } ?>
                <?php } ?>
            </div>
        </div>
        <div class="modal" id="modal_referral" role="dialog" aria-labelledby="" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                     <div class="modal-content">
                         <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title text-center" id="" style="font-weight:bold">Akses Masih Terbatas</h4>
                         </div>
                         <div class="modal-body">
                          <p>Anda masih menggunakan Plan Personal dengan limit hanya 2 proposal dan 2 invoice. Untuk menambah limit proposal, undang kerabat Anda untuk bergabung:</p>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="label-control">Masukkan alamat email (bisa input lebih dari 1 email)</label>
                                    <input type="text" class="form-control" name="friend_email" data-role="tagsinput" id="friend_email">
                                  </div>
                              </div>
                              <div class="col-md-12">
                                <button type="button" class="btn btn-primary btn-refer-to-friend"><i class="fa fa-send" aria-hidden="true"></i>
                                  Kirim
                                </button>
                                <span>atau</span>
                                <a href="<?php echo site_url('plan'); ?>" class="btn btn-success btn-priority-access">
                                  Upgrade Plan
                                </a>
                              </div>
                            </div>
                         </div>
                      </div>
                  </div>
                </div>
    </div><!--/ row -->
  </section><!-- /wrapper -->
</section><!-- /MAIN CONTENT -->

<script>
  $(document).ready(function(){
    $('.arsip-in').on('click',function(){
        var invoice_id = $(this).attr('iid');
        swal({
          title: "Apakah Anda yakin?",
          text: "Invoice masih bisa diakses pada menu Arsip.",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
                type: "POST",
                url: '<?=base_url()?>invoice/arsip',
                data: {
                    'id_in' : invoice_id
                  },
                success: function(data)
                {
                  $('#div_list_invoice_'+invoice_id).remove();
                  swal("Archived","Invoice berhasil diarsipkan","success");
                  location.href = "<?= site_url('invoice') ?>";
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                  swal("Oops","Invoice gagal diarsipkan. Silahkan mencoba kembali.","error");
                }
            });
          }
        });
      });

    $('#reachlimit').on('click',function(){
      $('#modal_referral').modal('show');
    });

    $('.btn-refer-to-friend').on('click',function(){
        if($('#friend_email').val() == '')
        {
          swal("Oops","Email belum diisi","error");
        }
        else
        {
                  var em = $('#friend_email').tagsinput('items');

                  if(em.length > 0)
                  {
                   for(var j=0;j<em.length;j++)
                   {
                      if(!ValidateEmail(em[j]))
                      {
                        swal("Oops","Pastikan format email benar","error");
                        return false;
                      }
                   }
                  }

                  swal({
                    title: "Undangan akan dikirim",
                    text: "Apakah Anda yakin?",
                    icon: "info",
                    buttons: true,
                    dangerMode: true,
                  })
                  .then((kirim) => {
                    if (kirim) {
                      $('.btn-refer-to-friend').attr('disabled',true).html('Mengirim...');
                      $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>home/refer_to_friend",
                        data: {
                            'user_id' : <?php echo $this->session->userdata('user_id');?>,
                            'refer_email' : $('#friend_email').val()
                            },
                        success: function(data){
                          $('.btn-refer-to-friend').attr('disabled',false).html('<i class="fa fa-send" aria-hidden="true"></i> Kirim');
                          if(data == 1)
                          {
                            swal('Ajakan berhasil dikirim');
                            $('#friend_email').val('');
                            $('#friend_email').tagsinput('removeAll');
                            $('#modal_referral').modal('hide');
                          }
                          else if(data == 0)
                          {
                            swal("Info","Ajakan sudah pernah dikirim via email. Coba ajak teman lainnya.","info");
                            $('#friend_email').val('');
                            $('#friend_email').tagsinput('removeAll');
                          }
                          else
                          {
                            swal('Ada kesalahan');
                          }

                        }, error:function(error){
                          swal('Ada kesalahan');
                        }
                      });
                    }
                  });
        }
      });
  });
</script>
