<style>
    input:valid {
        border-color: #ccc;
    }
        .row .col-sm-4 .form_date input[readonly]{
        background-color: #fff;
        cursor: pointer;
    }
    #item {
        margin-top: 60px;
        line-height: 2;
    }
    #item tr td {
        vertical-align: top;
        padding: 5px;
    }
    .th-item {
        border-bottom: 1px solid #ccc;
    }
    #grand-total {
        font-size: 1.5em;
        font-weight: bold;
    }
</style>

<section id="main-content">
    <section class="wrapper">
        <div class="row mt" style="margin-top:35px">
            <div class="showback" style="padding:40px 40px 40px 40px">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Invoice</h3>
                    </div>
                </div>
                <div class="row" style="margin-bottom:20px">
                    <div class="col-sm-6">
                        <div class="input-group">
                            <div class="input-group-addon">#</div>
                            <input type="text" class="form-control" id="noInvoice" style="z-index:unset" placeholder="nomor invoice" name="no_invoice" value="">
                        </div>
                        <span style="font-style:italic; color:grey">kosongkan jika tidak ingin menampilkan nomor invoice</span><br><br>
                        <textarea class="form-control" id="titleInvoice" placeholder="nama invoice / nama proyek" style="font-weight:bold"></textarea>
                        <span style="font-style:italic; color:grey">kosongkan jika tidak ingin menampilkan nama proyek</span><br><br>
                    </div>
                    <div class="col-sm-3 col-sm-offset-2">
                        <div class="pull-right text-center">
                            <img src="" id="show-image" class="img img-responsive" style="width:100%"><br>
                                    <?php if($team_logo && $team_logo->team_logo != NULL){ ?>
                                        <button class="btn btn-default btn-get-team-logo">Gunakan Logo Usaha</button>
                                        <input type="hidden" value="" id="logo-proposal" name="logo_proposal">
                                    <?php }else{ ?>
                                        <a href="<?php echo site_url('user/setting'); ?>" class="btn btn-default">Gunakan Logo Usaha</a>
                                    <?php } ?>
                        </div>
                    </div>
                </div><!-- endRow -->
                <div class="row" style="padding-top:5px; border-top: 1px solid #ccc">
                    <div class="col-sm-6">
                        <h4>DARI</h4>
                        <input type="text" class="form-control" value="<?= $this->session->userdata('user_name') ?>" placeholder="pengirim invoice" readonly>&nbsp;
                        <h4>KEPADA</h4>
                        <div id="client_select">
                            <select class="js-example-basic-single" name="select_duplicate_client" id="select_duplicate_client" style="width:100%">
                                        <option value=" " disabled selected>Pilih dari daftar client</option>
                                        <option value="baru">Klien Baru</option>
                                        <?php foreach ($user_client as $row) {?>
                                        <option value="<?php echo $row->id_client;?>"><?php echo $row->nama_pic;?> | <?php echo $row->perusahaan;?></option>
                                        <?php } ?>
                            </select>
                        </div>
                        <p style="margin: 10px;font-size: 13px;">tambah atau edit klien di <a href="<?= base_url()?>klien">menu klien</a></p>
                        <input type="text" class="form-control" id="namaKlien" value="" placeholder="penerima invoice">&nbsp;
                        <input type="text" class="form-control" id="namaPerusahaan" value="" placeholder="nama perusahaan (optional)">&nbsp;
                        <input type="text" class="form-control" id="emailKlien" value="" placeholder="Email klien">&nbsp;
                        <textarea class="form-control" rows="2" placeholder="alamat usaha klien (optional)" id="alamatPerusahaan"></textarea>&nbsp;
                        <input type="text" class="form-control" value="" placeholder="kota (optional)" id="kotaPerusahaan" >&nbsp;
                        <input class="form-control" placeholder="No Hp klien" id="noTelpKlien" value="">
                        <input hidden="hidden" id="id_client" value="">
                    </div>
                    <div class="col-sm-4 col-sm-offset-2">
                        <div class="input-group date form_date" data-date="" data-date-format="dd-M-yyyy" data-link-field="dtp_input2" data-link-format="yyy-mm-dd" style="margin-top:150px">
                            <div class="input-group-addon">Kirim</div>
                            <input id="sendInvoice" class="form-control" type="text" value="" readonly style="z-index:unset" placeholder="tanggal kirim">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>&nbsp;
                        <div class="input-group date form_date" data-date="" data-date-format="dd-M-yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                            <div class="input-group-addon">Due</div>
                            <input id="dueInvoice" class="form-control" type="text" value="" readonly style="z-index:unset" placeholder="tanggal deadline">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>&nbsp;
                    </div>
                </div><!-- endRow -->
                <div class="row">
                    <table id="item">
                        <tr class="th-item">
                            <th class="text-center" style="width:75%">ITEM DESCRIPTION</th>
                            <th class="text-center" style="width:25%">BIAYA</th>
                            <th>&nbsp;</th>
                        </tr>
                        <?php $c_item = 3; ?>
                        <tr id="item-0">
                            <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>
                            <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                        </tr>
                        <tr id="item-1">
                            <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>
                            <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                        </tr>
                        <tr id="item-2">
                            <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>
                            <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                        </tr>

                        <tr>
                            <td colspan="3">
                                <button type="button" class="btn btn-default" id="btnAddItem">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah item
                                </button>
                            </td>
                        </tr>
                        <tr style="border-bottom:1px solid #ccc">
                            <td colspan="3"></td>
                        </tr>
                        <tr style="font-size:1.2em; font-weight:bold">
                            <td class="text-right">SUB TOTAL</td>
                            <td class="text-right" id="getSubtotal">Rp </td>
                            <td></td>
                        </tr>
                        <tr style="font-size:1.2em; font-weight:bold">
                            <td class="text-right">
                                <div class="input-group pull-right" style="width:130px">
                                    <div class="input-group-addon">Tax</div>
                                    <input type='text' onblur="sum();" class="form-control sepNumber" style="z-index:unset" name="tax" id="tax" value="0" />
                                    <div class="input-group-addon">%</div>
                                </div>
                            </td>
                            <td class="text-right" id="getTaxPrice">Rp </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                        <tr id="grand-total">
                            <td style="padding:10px; " class="text-right">TOTAL</td>
                            <td class="text-right" style="padding:10px; background-color:#eee" id="getTotPrice">Rp </td>
                            <td></td>
                        </tr>
                    </table>

                    <div class="col-sm-8 col-xs-12" style="margin-top:60px">
                        <h4>CATATAN</h4>
                        <textarea rows="3" type="text" class="form-control" id="catatan" placeholder="Tambahkan catatan untuk penerima jika ada" name="invoice_note"></textarea>
                    </div>

                    <div class="col-xs-12">
                        <button id="createInvoice" class="btn pull-right">Simpan</button>
                    </div>
                </div><!-- endRow -->
            </div>
        </div>
    </section>
</section>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.id.js"></script>
<script src="http://www.decorplanit.com/plugin/autoNumeric-1.9.18.js"></script>

<script>
    $('.form_date').datetimepicker({
        language:  'en',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

    $(function($) {
        $('.sepNumber').autoNumeric('init', {  lZero: 'deny', aSep: '.', aDec: ',' , mDec: 0 });
    });

    function sum()
        {
            var tax = parseInt($('#tax').val());
            var newsubTot = 0;
            $(".biaya").each(function() {
                var str = this.value.trim().replace(/\./g,'');  // .trim() may need a shim
                if (str) {   // don't send blank values to `parseInt`
                    newsubTot += parseInt(str, 10);
                }
            });

            //console.log(newsubTot);

                var taxPrice = parseInt((tax/100) * newsubTot);
                var totPrice = parseInt(newsubTot + taxPrice);

                $('#getSubtotal').html('Rp '+parseFloat(newsubTot).toLocaleString('id'));
                //$('#getTax').html(tax);
                $('#getTaxPrice').html('Rp '+parseFloat(taxPrice).toLocaleString('id'));
                $('#getTotPrice').html('Rp '+parseFloat(totPrice).toLocaleString('id'));
        }

    function ValidateEmail(email) {
        var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              return expr.test(email);
    };


    $(document).ready(function()
    {
        $('.js-example-basic-single').select2({
            dropdownParent: $("#client_select")
        });

        $('#select_duplicate_client').on('change',function(){
            if($(this).val() !== 'baru'){
                    var client_id = $(this).val();
                    //pilihClientLama(client_id);
                    $.ajax({
                        type:"GET",
                        url: "<?php echo base_url()?>client/pilihclient",
                        data: {id:client_id},
                        datatype: 'json',
                        success: function(data){
                            var datanya = JSON.parse(data);
                            document.getElementById("id_client").value = client_id;
                            document.getElementById("namaKlien").value = datanya.nama;
                            document.getElementById("namaPerusahaan").value = datanya.perusahaan;
                            document.getElementById("emailKlien").value = datanya.email;
                            document.getElementById("alamatPerusahaan").value = datanya.alamat;
                            document.getElementById("kotaPerusahaan").value = datanya.kota;
                            document.getElementById("noTelpKlien").value = datanya.telp;
                            if (document.getElementById("emailKlien").value !== ''){
                                document.getElementById("emailKlien").setAttribute('readonly', 'true');
                            }else{
                                $('#emailKlien').removeAttr("readonly");
                            }
                            document.getElementById("namaKlien").setAttribute('readonly', 'true');
                            document.getElementById("namaPerusahaan").setAttribute('readonly', 'true');
                            //$('#klienlama').modal('hide');
                        }, error:function(error){
                            swal('error;' +eval(error));
                        }
                        });

                }else if($(this).val() == 'baru'){
                    $('#namaKlien').removeAttr("readonly");
                    $('#namaPerusahaan').removeAttr("readonly");
                    $('#emailKlien').removeAttr("readonly");
                    $('#namaKlien').val("");
                    $('#id_client').val("");
                    $('#namaPerusahaan').val("");
                    $('#emailKlien').val("");
                    $('#alamatPerusahaan').val("");
                    $('#kotaPerusahaan').val("");
                    $('#noTelpKlien').val("");
                }
            });

            var c_item = <?= $c_item ?>;
        $('#btnAddItem').on('click',function(){
            var newitem = '';
            newitem += '<tr id="item-'+c_item+'">';
            newitem += '<td><textarea rows="1" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>';
            newitem += '<td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);" onblur="sum();" value=""></td>';
            newitem += '<td align="right"><button type="button" item-id="'+c_item+'" class="btn btn-default deleteItem"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>';
            newitem += '</tr>';
            $(newitem).insertAfter($('#item-'+ parseInt(c_item - 1)))
            c_item++;
        });

        $(document).on('click','.deleteItem',function(){
            if(c_item > 1)
            {
                $('#item-'+ $(this).attr('item-id')).remove();
            }
        });

        $(document).on('click','#createInvoice', function(){
            var number = $('#noInvoice').val();
            var title = $('#titleInvoice').val();
            var note = $('#catatan').val();
            var tax = $('#tax').val();
            var sendInvoice = $('#sendInvoice').val();
            var dueInvoice = $('#dueInvoice').val();
            // var idKlien = $('#select_duplicate_client').val();
            var idKlien = $('#id_client').val();
            var namaKlien = $('#namaKlien').val();
            var namaUsaha = $('#namaPerusahaan').val();
            var addKlien = $('#alamatPerusahaan').val();
            var kotaKlien = $('#kotaPerusahaan').val();
            var email = $('#emailKlien').val();
            var telpKlien = $('#telpKlien').val();
            var logo_team = $('#logo-proposal').val();
            var notelp = $('#noTelpKlien').val();

            if(namaKlien == '')
            {
                swal("Oops","Nama klien belum diisi","error");
            }
            else if(email == '')
            {
                swal("Oops","Email klien belum diisi","error");
            }
            else if(!ValidateEmail(email))
            {
                swal("Oops","Pastikan format email klien sudah benar","error");
            }
            else
            {
                var item = [];
                $('[name="item"]').each(function(){
                    if($(this).val() != '')
                    {
                        item.push($(this).val());
                    }
                });

                var biaya=[];
                $('[name="biaya"]').each(function(){
                    if($(this).val() != '')
                    {
                        biaya.push($(this).val().trim().replace(/\./g,''));
                    }
                });

                if(item.length > 0)
                {
                    $.ajax({
                        type: "POST",
                        url: "<?=base_url()?>invoice/store",
                        data: {
                            'no_in' : number,
                            'judul_in' : title,
                            'id_klien' : idKlien,
                            'note_in' : note,
                            'tax_in' : tax,
                            'send_in' : sendInvoice,
                            'due_in' : dueInvoice,
                            'add_klien' : addKlien,
                            'kota_klien' : kotaKlien,
                            'biaya' : biaya,
                            'item' : item,
                            'namapic' : namaKlien,
                            'email' : email,
                            'telp' : notelp,
                            'perusahaan' : namaUsaha,
                        },
                        success: function(data)
                        {
                            if(data !== 0)
                            {
//                                swal(data);
                                 swal("Invoice berhasil dibuat");
                                 location.href = '<?php echo site_url('invoice'); ?>';
                            }
                            else
                            {
                                swal("Oops","Ada kesalahan. Silahkan mencoba kembali.","error");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            swal("Oops","Invoice gagal diedit. Silahkan mencoba kembali.","error");
                        }
                    });
                }
                else
                {
                    swal("Oops","Pastikan Anda sudah memasukan item.","error");
                }
            }
        });

        $(document).on('click','.btn-save-team-logo-image',function(){
                    if($('#imglg').val() == '')
                    {
                      swal("Oops","Pilih gambar yang akan ditambahkan","error");
                    }
                    else
                    {
                      $('#form_upload_team_logo_new_proposal').submit();
                    }
                  });

        <?php if($team_logo && $team_logo->team_logo !== NULL){ ?>
                    $(document).on('click','.btn-get-team-logo',function(){
                        swal({
                            title: "Gunakan Logo Usaha",
                            text: "Anda akan menggunakan logo usaha pada invoice ini. Lanjutkan?",
                            icon: "info",
                            dangerMode: true,
                            buttons: {
                                cancel: true,
                                confirm: true
                            }
                        })
                        .then((kirim) => {
                            if (kirim){
                                $('#logo-proposal').val('<?php echo $team_logo->team_logo; ?>');
                                $('#show-image').attr('src','<?php echo $team_logo->team_logo; ?>');
                            }
                        });
                    });
        <?php } ?>
    });
</script>
