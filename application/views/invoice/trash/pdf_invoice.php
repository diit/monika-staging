<style>
    .space tr td{
        padding: 10px 0 10px 0;
    }
    h4, p{
        margin: 0px;
    }
    .blank{
        color: #fff;
    }
    .bold{
        font-weight: bold;
    }
    p{
        line-height: 1.5;
    }
    table{
        border-collapse: collapse;
    }
</style>

<h1>Invoice</h1>
<table class="space">
    <tr>
        <?php if(!empty($invoice->number)) { ?>
        <td valign="top">#<?= $invoice->number ?></td>
        <?php } else { ?>
        <td class="blank">.</td>
        <?php } ?>

        <td class="blank">logo</td>
    </tr>
</table>
<table class="space" style="margin-top:40px">
    <tr>
        <td style="width:570px">
            <p style="padding-bottom:10px">DARI</p>
            <p class="bold"><?= $invoice->user_name ?></p>
        </td>
        <td class="blank">.</td>
    </tr>
    <tr>
        <td>
            <p style="padding-bottom:10px">KEPADA</p>
            <p class="bold"><?= $invoice->nama_pic ?></p>
            <?= (empty($invoice->perusahaan))? '' : "<p>".$invoice->perusahaan."</p>" ?>
            <?= (empty($invoice->alamat_usaha))? '' : "<p>".$invoice->alamat_usaha."</p>" ?>
            <?= (empty($invoice->kota))? '' : "<p>".$invoice->kota."</p>" ?>
        </td>
        <?php if(!empty($invoice->send_invoice)) { ?>
        <td style="text-align:right"><p>Tgl akhir pembayaran</p><p class="bold"><?= date("d-M-Y", strtotime($invoice->send_invoice)) ?></p></td>
        <?php } else { ?>
        <td class="blank">.</td>
        <?php } ?>
    </tr>
</table>

<table style="margin-top:40px">
    <tr>
        <td style="width:500px; text-align:center; padding:10px 0 5px 0; border-top: 0.5px solid #ccc; border-bottom: 0.5px solid #ccc;"><p class="bold">ITEM DESKRIPSI</p></td>
        <td style="width:130px; text-align:center; padding:10px 0 5px 0; border-top: 0.5px solid #ccc; border-bottom: 0.5px solid #ccc;"><p class="bold">BIAYA</p></td>
    </tr>
    <tr>
        <td colspan="2" class="blank">.</td>
    </tr>
    <?php
        $sub_tot = 0;
        foreach($item as $d) {
    ?>
    <tr>
        <td style="width:500px; padding:0 10px 10px 10px"><p><?= $d->item ?></p></td>
        <td style="width:130px; text-align:right; padding:0 10px 10px 10px"><p>Rp <?= number_format($d->harga,"0",",",".") ?></p></td>
    </tr>
    <?php
        $sub_tot = $sub_tot+($d->harga);
        }
    ?>
    <tr style="padding-top:20px">
        <td style="width:500px; text-align:right; border-top: 0.5px solid #ccc; padding:0 60px 0 0"><p class="bold">SUB TOTAL</p></td>
        <td style="width:130px; text-align:right; border-top: 0.5px solid #ccc; padding:0 10px 0 0"><p class="bold">Rp <?= number_format($sub_tot,"0",",",".") ?></p></td>
    </tr>
    <tr style="padding-top:20px">
        <td style="text-align:right; padding:0 60px 5px 0"><p class="bold">TAX <?= number_format($invoice->tax,"0",",",".") ?> %</p></td>
        <?php $tax_price = ($invoice->tax/100)*$sub_tot; ?>
        <td style="text-align:right; padding-right:10px"><p class="bold">Rp <?= number_format($tax_price,"0",",",".") ?></p></td>
    </tr>
    <tr style="padding-top:20px">
        <td style="text-align:right; padding:0 60px 5px 0"><p class="bold">TOTAL</p></td>
        <?php $tot = $sub_tot + $tax_price; ?>
        <td style="text-align:right; padding-right:10px"><p class="bold">Rp <?= number_format($tot,"0",",",".") ?></p></td>
    </tr>
</table>

<?php if(!empty($invoice->note)) { ?>
<table style="margin-top:100px">
    <tr>
        <td><p>Catatan:</p> <p><?= $invoice->note ?></p></td>
    </tr>
</table>
<?php } ?>
