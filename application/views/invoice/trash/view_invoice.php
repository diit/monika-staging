<style>
    input:valid {
        border-color: #ccc;
    }
    .row .col-sm-4 .form_date input[readonly]{
        background-color: #fff;
        cursor: pointer;
    }
    #item {
        margin-top: 60px;
        line-height: 2;
    }
    #item tr td {
        vertical-align: top;
        padding: 5px;
    }
    .th-item {
        border-bottom: 1px solid #ccc;
    }
    #grand-total {
        font-size: 1.5em;
        font-weight: bold;
    }

    .button-label {
      display: inline-block;
      padding: 0 20px;
      margin: 0.5em;
      cursor: pointer;
      color: #292929;
      border-radius: 0.25em;
      background: #efefef;
      transition: 0.3s;
      user-select: none;
      &:hover {
        background: darken(#efefef, 10%);
        color: darken(#292929, 10%);
      }
      &:active {
        transform: translateY(2px);
      }
    }

    #btn-lunas:checked + .button-label {
      background: #337ab7;
      color: #efefef;
      &:hover {
        background: darken(#337ab7, 5%);
        color: darken(#efefef, 5%);
      }
    }

    #btn-pending:checked + .button-label {
      background: #f0ad4e;
      color: #efefef;
      &:hover {
        background: darken(#f0ad4e, 5%);
        color: darken(#efefef, 5%);
      }
    }

    .hidden {
      display: none;
    }
</style>

<section id="main-content">
    <section class="wrapper">
        <div class="floating-proposal-panel">
          <div style="display: flex;margin-left: auto;-webkit-box-pack: end;justify-content: flex-end;">
            <div class="" style="color: rgb(149, 155, 163);box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 15px;transition: all 0.3s;">
                <h2 class="hidden-xs" style="margin-bottom:20px">PREVIEW</h2>
                <h4 class="hidden-lg hidden-md hidden-sm">PREVIEW</h4>
          </div>
          <div id="link_invoice" class="hidden-xs" style="color: rgb(149, 155, 163);box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;width:250px;padding: 0px 15px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
            <input style="width: 100%;height:30px;border: none;" id="link" type="text" name="" value="<?= $link ?>">
          </div>
          <div id="copy_link_btn" style="color: rgb(149, 155, 163);cursor: pointer;box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 20px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
            <span class="hidden-xs hidden-sm"><i class="fa fa-link"></i>  Copy URL</span>
              <i class="fa fa-link hidden-md hidden-lg"></i>
          </div>
          <!-- hidden 25 April, adit -->
          <div id="download_pdf" style="color: rgb(149, 155, 163);cursor: pointer;box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 20px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
              <span class="hidden-xs"><i class="fa fa-cloud-download"></i>  DOWNLOAD PDF</span>
              <i class="fa fa-cloud-download hidden-lg hidden-md hidden-sm"></i>
          </div>

            <div id="status_invoice" class="tooltips" data-placement="bottom" data-original-title="Ubah status" style="color: rgb(149, 155, 163);cursor: pointer;box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 20px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
                <?php if($invoice->status == 0 || $invoice->status == 1){ ?>
                <span><i class="glyphicon glyphicon-briefcase"></i> BELUM DIBAYAR</span>
                <?php } else if($invoice->status == 2){ ?>
                <span><i class="glyphicon glyphicon-briefcase"></i> LUNAS</span>
                <?php } ?>
            </div>

            <div id="send_invoice" style="background-color:#5ee2d4;color: #fff;cursor: pointer;box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 20px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
              <span><i class="fa fa-envelope"></i> KIRIM</span>
            </div>

        </div>
      </div>
        <div class="row mt" style="margin-top:75px">
            <div class="showback" style="padding:40px 40px 40px 40px">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Invoice</h3>
                    </div>
                </div>
                <div class="row" style="margin-bottom:20px">
                    <div class="col-sm-6">
                        <div class="input-group">
                            <div class="input-group-addon">#</div>
                            <input type="text" class="form-control" id="noInvoice" style="z-index:unset" placeholder="nomor invoice" name="no_invoice" value="<?= $invoice->number ?>">
                        </div>
                        <span style="font-style:italic; color:grey">kosongkan jika tidak ingin menampilkan nomor invoice</span><br><br>
                        <textarea class="form-control" id="titleInvoice" placeholder="nama invoice / nama proyek" style="font-weight:bold"><?= $invoice->judul ?></textarea>
                        <span style="font-style:italic; color:grey">kosongkan jika tidak ingin menampilkan nama proyek</span><br><br>
                        <?php
                            $random = md5(mt_rand(1,10000));
                            $first = substr($random,0,5);
                            $last = substr($random,5,10);
                            $urlrand = $first.$invoice->id_proposal.$last;
                         ?>
                        <?= (!empty($invoice->id_proposal)) && $invoice->id_proposal !=1 ? '<p><a href="'.site_url('proposal/lihat/'.$urlrand).'">Lihat Proposal terkait</a></p>':''; ?>
                    </div>
                    <div class="col-sm-3 col-sm-offset-2">
                        <div class="pull-right text-center">
                                <?php if($get_style && ($get_style->logoimageurl != NULL || $get_style->logoimageurl != '')){ ?>
                                    <img src="<?=$get_style->logoimageurl; ?>" style="width:200px;">
                                <?php }else{ ?>
                                    <?php if($team_logo && $team_logo->team_logo != NULL){ ?>
                                        <button class="btn btn-default btn-get-team-logo">Gunakan Logo Usaha</button><br><p style="margin: 5px;font-size: 12px;">atau</p>
                                    <?php } ?>
                                <?php } ?>
                                <div style="margin:10px auto;">
                                                <?php echo form_open_multipart('invoice/upload_image_logo',array('id' => 'form_upload_team_logo'));?>
                                                <div class="input-group" style="width:200px;margin: auto;">
                                                    <span class="input-group-btn">
                                                        <input type="hidden" name="invoiceid" value="<?php echo $invoice->id; ?>">
                                                        <span class="btn btn-default btn-file">
                                                            <i class="fa fa-image"></i> <input type="file" name="file" id="imglg">
                                                        </span>
                                                    </span>
                                                    <input type="text" class="form-control" readonly>
                                                    <span class="input-group-btn">
                                                      <button class="btn btn-save-team-logo-image" disabled="true" type="button">Upload</button>
                                                    </span>
                                                </div>
                                                </form>
                                </div>
                        </div>
                    </div>
                </div><!-- endRow -->
                <div class="row" style="padding-top:5px; border-top: 1px solid #ccc">
                    <div class="col-sm-6">
                        <h5>DARI</h5>
                        <input class="form-control" value="<?= $invoice->user_name ?>" readonly>&nbsp;
                        <h5>KEPADA</h5>
                        <div id="client_select">
                            <select class="js-example-basic-single" name="select_duplicate_client" id="select_duplicate_client" style="width:100%">
                                        <option value="0" disabled  selected>Pilih dari daftar client</option>
                                        <option value="baru">Tambah Klien Baru</option>
                                        <?php foreach ($user_client as $row) {?>
                                        <option value="<?php echo $row->id_client;?>"><?php echo $row->nama_pic;?> | <?php echo $row->perusahaan;?></option>
                                        <?php } ?>
                            </select>
                        </div>
                        <p style="margin: 10px;font-size: 13px;">atau isi klien baru</p>
                        <input class="form-control" id="namaKlien" placeholder="Nama Klien Anda" value="<?= $invoice->nama_pic ?>" readonly="true">&nbsp;
                        <input class="form-control" id="namaPerusahaan" value="<?= ($invoice->perusahaan=='')? '' : $invoice->perusahaan ?>" placeholder="nama perusahaan" readonly="true">&nbsp;
                        <textarea class="form-control" style="resize: none;" rows="2" placeholder="Alamat usaha klien" id="alamatPerusahaan"><?= ($invoice->alamat_usaha=='')? '' : $invoice->alamat_usaha ?></textarea>&nbsp;
                        <input class="form-control" value="<?= ($invoice->kota=='')? '' : $invoice->kota ?>" placeholder="Kota" id="kotaPerusahaan" >&nbsp;
                        <input class="form-control" placeholder="email klien" id="emailKlien" value="<?= $invoice->email ?>" readonly="true">&nbsp;
                        <input class="form-control" placeholder="No Hp klien" id="noTelpKlien" value="<?= $invoice->telephone ?>">
                        <input hidden="hidden" id="id_client" value="<?= $invoice->id_client ?>">
                    </div>
                    <div class="col-sm-4 col-sm-offset-2">
                        <div class="input-group date form_date" data-date="" data-date-format="dd-M-yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd" style="margin-top:150px">
                            <div class="input-group-addon">Kirim</div>
                            <input id="sendInvoice" class="form-control" type="text" value="<?= ($invoice->send_invoice=='' || $invoice->send_invoice == '0000-00-00 00:00:00' || $invoice->send_invoice == NULL)? date("d-M-Y") : date("d-M-Y", strtotime($invoice->send_invoice)); ?>" readonly style="z-index:unset" placeholder="tanggal kirim">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>&nbsp;
                        <div class="input-group date form_date" data-date="" data-date-format="dd-M-yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                            <div class="input-group-addon">Due</div>
                            <input id="dueInvoice" class="form-control" type="text" value="<?= ($invoice->due_invoice=='' || $invoice->send_invoice == '0000-00-00 00:00:00' || $invoice->send_invoice == NULL)? date("d-M-Y") : date("d-M-Y", strtotime($invoice->due_invoice)) ?>" readonly style="z-index:unset" placeholder="tanggal deadline">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>&nbsp;
                    </div>
                </div><!-- endRow -->
                <div class="row">
                    <table id="item">
                        <tr class="th-item">
                            <th class="text-center" style="width:75%">ITEM DESCRIPTION</th>
                            <th class="text-center" style="width:25%">BIAYA</th>
                            <th>&nbsp;</th>
                        </tr>
                        <?php
                            $sub_tot = 0;
                            if(count($item) > 0){
                                $c_item = count($item);
                                foreach($item as $id=>$d) {
                                    if($d->item != NULL && $d->harga > 0){
                        ?>
                        <tr id="item-<?= $id ?>">
                            <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"><?= $d->item ?></textarea></td>
                            <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="<?= $d->harga ?>" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                            <td align="right">
                                <button type="button" item-id="<?= $id ?>" class="btn btn-default deleteItem">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
                        <?php
                                        $sub_tot = $sub_tot+($d->harga);
                                    }
                                }
                            }else{
                                $c_item = 3;
                        ?>
                        <tr id="item-0">
                            <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>
                            <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                            <td align="right">
                                <button type="button" class="btn btn-default">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
                        <tr id="item-1">
                            <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>
                            <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                            <td align="right">
                                <button type="button" class="btn btn-default">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
                        <tr id="item-2">
                            <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>
                            <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                            <td align="right">
                                <button type="button" class="btn btn-default">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
                        <?php
                                $sub_tot = 0;
                            }
                        ?>
                        <tr>
                            <td colspan="3">
                                <button type="button" class="btn btn-default" id="btnAddItem">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah item
                                </button>
                            </td>
                        </tr>
                        <tr style="border-bottom:1px solid #ccc">
                            <td colspan="3"></td>
                        </tr>
                        <input hidden="hidden" type="text" value="<?= $sub_tot ?>" id="subTotal">
                        <tr style="font-size:1.2em; font-weight:bold">
                            <td class="text-right"><h5>SUB TOTAL</h5></td>
                            <td class="text-right" id="getSubtotal"><h5>Rp <?= number_format($sub_tot,0,",",".") ?></h5></td>
                            <td></td>
                        </tr>
                        <tr style="font-size:1.2em; font-weight:bold">
                            <?php $tax = (($invoice->tax)/100)*$sub_tot;  ?>
                            <td class="text-right">
                                <div class="input-group pull-right" style="width:130px">
                                    <div class="input-group-addon">Tax</div>
                                    <input type='text' onblur="sum();" class="form-control sepNumber" style="z-index:unset" name="tax" id="tax" value="<?= number_format($invoice->tax,0,",",".") ?>" /> <!--onkeyup="this.value=this.value.replace(/[^\d]/,.'')"-->
                                    <div class="input-group-addon">%</div>
                                </div>
                            </td>
                            <td class="text-right" id="getTaxPrice"><h5>Rp <?= number_format($tax,0,",",".") ?></h5></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                        <tr id="grand-total">
                            <?php $tot = $sub_tot + $tax; ?>
                            <td style="padding:10px; " class="text-right"><h5 class="bold">TOTAL</h5></td>
                            <td class="text-right" style="padding:10px; background-color:#eee" id="getTotPrice"><h5 class="bold">Rp <?= number_format($tot,0,",",".") ?></h5></td>
                            <td></td>
                        </tr>
                    </table>

                    <div class="col-sm-8 col-xs-12" style="margin-top:60px">
                        <h5>CATATAN</h5>
                        <textarea rows="3" type="text" class="form-control" id="catatan" placeholder="Tambahkan catatan untuk penerima jika ada" name="invoice_note"><?= $invoice->note ?></textarea>
                    </div>

                    <div class="col-xs-12">
                        <button id="saveInvoice" class="btn btn-theme03 pull-right" style="margin-top:50px" iid="<?= $invoice->id ?>" pid="<?= $invoice->id_proposal ?>" kid="<?= $invoice->id_client ?>">Simpan</button>
                    </div>
                </div><!-- endRow -->
            </div>
        </div>
    </section>
</section>

<div class="modal" id="modal_send_invoice" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog modal-md">
         <div class="modal-content">
             <div class="modal-header">
               <h4 class="modal-title text-center" id="" style="font-weight:bold">Kirim Invoice</h4>
             </div>
             <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="label-control">Nama Pengirim</label>
                            <input type="text" class="form-control" value="<?php echo $invoice->user_name; ?>" id="send_invoice_sender_name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="label-control">Email Pengirim</label>
                            <input type="text" class="form-control" value="<?php echo $invoice->user_email; ?>" id="send_invoice_sender_email">
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                        <label class="label-control">Email Klien</label>
                        <input type="hidden" value="<?php echo $invoice->nama_pic; ?>" id="send_invoice_client_name">
                        <input type="text" class="form-control" value="<?= $invoice->email ?>" data-role="tagsinput" id="send_invoice_client_email">
                        <span style="font-size: 10px; font-style: italic;">Masukan banyak email menggunakan tanda koma atau tekan enter</span>
                      </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                        <label class="label-control">CC</label>
                        <input type="text" class="form-control" value="" data-role="tagsinput" id="send_invoice_client_cc">
                        <span style="font-size: 10px; font-style: italic;">Masukan banyak email cc menggunakan tanda koma atau tekan enter</span>
                      </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                        <label class="label-control">Subjek</label>
                        <input type="text" class="form-control" value="Invoice<?= ($invoice->judul)=='' ? '' : ' - '.$invoice->judul ?>" id="send_invoice_client_email_subject">
                      </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                        <label class="label-control">Pesan</label>
                        <textarea rows="8" class="form-control" id="send_invoice_client_email_message">Dengan Hormat,

Berikut kami lampirkan Invoice untuk [...]

Klik di sini > <?php echo $link; ?>

Bila ada pertanyaan silahkan langsung tanyakan ke saya.

Terima kasih,

<?php echo $invoice->user_name; ?>

<?php echo $invoice->team_name; ?></textarea>
                      </div>
                  </div>
                </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-info" id="btn-send-invoice"><i class="fa fa-send" aria-hidden="true"></i>
                      Kirim
                    </button>
                 </div>
          </div>
      </div>
    </div>
</div>

<div class="modal" id="modal_status_invoice" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog modal-md">
         <div class="modal-content">
             <div class="modal-header">
               <h4 class="modal-title text-center" id="" style="font-weight:bold">Status Invoice</h4>
             </div>
             <div class="modal-body text-center">
                 <input type="radio" name="statPay" id="btn-pending" class="hidden radio-label" value="1" <?= $invoice->status == 0 || $invoice->status == 1 ? 'checked' : '' ?> >
                 <label for="btn-pending" class="button-label"><h4>Belum terbayar</h4></label>

                 <input type="radio" name="statPay" id="btn-lunas" class="hidden radio-label" value="2" <?= $invoice->status == 2 ? 'checked' : '' ?> >
                 <label for="btn-lunas" class="button-label"><h4>Lunas</h4></label>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-info" id="btn-change-status">Ubah status pembayaran</button>
             </div>
          </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.id.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-tagsinput.js"></script>
<script src="http://www.decorplanit.com/plugin/autoNumeric-1.9.18.js"></script>

<script>
    $('.form_date').datetimepicker({
        language:  'en',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

    $(function($) {
        $('.sepNumber').autoNumeric('init', {  lZero: 'deny', aSep: '.', aDec: ',' , mDec: 0 });
    });


    /*$('input').keyup(function(){
        var tax = Number($('#tax').val());
        var subTot = Number($('#subTotal').val());
        console.log(subTot);
        var taxPrice = (tax/100) * subTot;
        var totPrice = subTot + taxPrice;

        $('#getTax').html(tax);
        $('#getTaxPrice').html('Rp '+parseFloat(taxPrice).toLocaleString('id'));
        $('#getTotPrice').html('Rp '+parseFloat(totPrice).toLocaleString('id'));
    });*/

    function sum()
        {
            var tax = parseInt($('#tax').val());
            var newsubTot = 0;
            $(".biaya").each(function() {
                var str = this.value.trim().replace(/\./g,'');  // .trim() may need a shim
                if (str) {   // don't send blank values to `parseInt`
                    newsubTot += parseInt(str, 10);
                }
            });

            //console.log(newsubTot);

                var taxPrice = parseInt((tax/100) * newsubTot);
                var totPrice = parseInt(newsubTot + taxPrice);

                $('#getSubtotal').html('Rp '+parseFloat(newsubTot).toLocaleString('id'));
                //$('#getTax').html(tax);
                $('#getTaxPrice').html('Rp '+parseFloat(taxPrice).toLocaleString('id'));
                $('#getTotPrice').html('Rp '+parseFloat(totPrice).toLocaleString('id'));
        }

    $(document).ready(function()
    {
            $('.js-example-basic-single').select2({
              dropdownParent: $("#client_select")
            });

            $('#select_duplicate_client').on('change',function(){
                if($(this).val() !== 'baru'){
                    var client_id = $(this).val();
                    //pilihClientLama(client_id);
                    $.ajax({
                        type:"GET",
                        url: "<?php echo base_url()?>client/pilihclient",
                        data: {id:client_id},
                        datatype: 'json',
                        success: function(data){
                            var datanya = JSON.parse(data);
                            document.getElementById("id_client").value = datanya.id;
                            document.getElementById("namaKlien").value = datanya.nama;
                            document.getElementById("namaPerusahaan").value = datanya.perusahaan;
                            document.getElementById("emailKlien").value = datanya.email;
                            document.getElementById("alamatPerusahaan").value = datanya.alamat;
                            document.getElementById("kotaPerusahaan").value = datanya.kota;
                            document.getElementById("noTelpKlien").value = datanya.telp;
                            document.getElementById("namaKlien").setAttribute('readonly', 'true');
                            document.getElementById("namaPerusahaan").setAttribute('readonly', 'true');
                            document.getElementById("emailKlien").setAttribute('readonly', 'true');
                            //$('#klienlama').modal('hide');
                        }, error:function(error){
                            swal('error;' +eval(error));
                        }
                        });

                }else if($(this).val() == 'baru'){
                    $('#namaKlien').removeAttr("readonly");
                    $('#namaPerusahaan').removeAttr("readonly");
                    $('#emailKlien').removeAttr("readonly");
                    $('#namaKlien').val("");
                    $('#id_client').val("");
                    $('#namaPerusahaan').val("");
                    $('#emailKlien').val("");
                    $('#alamatPerusahaan').val("");
                    $('#kotaPerusahaan').val("");
                    $('#noTelpKlien').val("");
                }
            });

        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if( input.length ) {
                    input.val(log);
                    $('.btn-save-team-logo-image').attr('disabled',false).addClass('btn-success');
                } else {
                    if( log ) alert(log);
                }

        });

        var c_item = <?= $c_item ?>;
        $(document).on('click','#btnAddItem',function(){
            var newitem = '';
            newitem += '<tr id="item-'+c_item+'">';
            newitem += '<td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>';
            newitem += '<td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);" onblur="sum();" value=""></td>';
            newitem += '<td align="right"><button type="button" item-id="'+c_item+'" class="btn btn-default deleteItem"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>';
            newitem += '</tr>';
            $(newitem).insertAfter($('#item-'+ parseInt(c_item - 1)))
            c_item++;
        });

        $(document).on('click','.deleteItem',function(){
            if(c_item > 1)
            {
                $('#item-'+ $(this).attr('item-id')).remove();
            }
        });

        $(document).on('click','#saveInvoice', function(){
            var idInvoice = $(this).attr('iid');
            var number = $('#noInvoice').val();
            var title = $('#titleInvoice').val();
            var idProposal = $(this).attr('pid');
            // var idKlien = $(this).attr('kid');
            // var idKlien = $('#select_duplicate_client').val();
<<<<<<< HEAD
            // var projek = $('#id_client').val();
            var idKlien = $('#id_client').val();
=======
//            var idKlien = $('#id_client').val();
>>>>>>> origin/percobaan
            var note = $('#catatan').val();
            var tax = $('#tax').val();
            var sendInvoice = $('#sendInvoice').val();
            var dueInvoice = $('#dueInvoice').val();
            var namaKlien = $('#namaKlien').val();
            var namaUsaha = $('#namaPerusahaan').val();
            var addKlien = $('#alamatPerusahaan').val();
            var kotaKlien = $('#kotaPerusahaan').val();
            var emailKlien = $('#emailKlien').val();
            var noTelp = $('#noTelpKlien').val();

            var item = [];
            $('[name="item"]').each(function(){
                if($(this).val() != '')
                {
                    item.push($(this).val());
                }
            });
            var biaya=[];
            $('[name="biaya"]').each(function(){
                if($(this).val() != '')
                {
                    biaya.push($(this).val().trim().replace(/\./g,''));
                }
            });

            if(item.length > 0)
            {

                $.ajax({
                    type: "POST",
                    url: "<?=base_url()?>invoice/update",
                    data: {
                        'id_in' : idInvoice,
                        'no_in' : number,
                        'judul_in' : title,
                        'id_prop' : idProposal,
                        'id_klien' : idKlien,
                        'note_in' : note,
                        'tax_in' : tax,
                        'send_in' : sendInvoice,
                        'due_in' : dueInvoice,
                        'projek' : title,
                        'add_klien' : addKlien,
                        'kota_klien' : kotaKlien,
                        'biaya' : biaya,
                        'item' : item,
                        'namapic' : namaKlien,
                        'email' : emailKlien,
                        'telp' : noTelp,
                        'perusahaan' : namaUsaha
                    },
                    success: function(data)
                    {
                        if(data == 1)
                        {
                            swal("Invoice berhasil diedit");
                            location.href = '<?php echo site_url('invoice'); ?>';
                        }
                        else
                        {
                            swal("Oops","Ada kesalahan. Silahkan mencoba kembali.","error");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        swal("Oops","Invoice gagal diedit. Silahkan mencoba kembali.","error");
                    }
                });
            }
            else
            {
                swal("Oops","Pastikan Anda sudah memasukan item invoice.","error");
            }

        });

        $('#link').on('click',function(){
            $(this).select();
        });

        $('#copy_link_btn').on('click',function(){
            var copyText = document.getElementById("link");
            copyText.select();
            document.execCommand("Copy");
            $.ajax({
              success : function(data){
                swal("Perhatian","Kami tidak merekomendasikan Anda mengirimkan invoice ini menggunakan cara selain menggunakan tombol KIRIM di kanan atas. Bila Anda tetap menginginkan, silahkan bagikan link URL ini.","warning");
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                swal("Oops","Link gagal disalin","error");
              }
            });
        });

        $('#download_pdf').click( function(e) {
            <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
                e.preventDefault();
                <?php
                    $random = md5(mt_rand(1,10000));
                    $first = substr($random,0,5);
                    $last = substr($random,5,10);
                    $urlrand = $first.$invoice->id.$last;
                 ?>
                window.open( "<?php echo site_url('invoice/download/'.$urlrand);?>","_blank");
                return false;
            <?php }else{ ?>
                <?php if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1 && $plan->id != 1 && $plan->id != 2) { ?>
                  e.preventDefault();
                    <?php
                        $random = md5(mt_rand(1,10000));
                        $first = substr($random,0,5);
                        $last = substr($random,5,10);
                        $urlrand = $first.$invoice->id.$last;
                     ?>
                    window.open("<?php echo site_url('invoice/download/'.$urlrand);?>","_blank");
                  return false;
                <?php }else{ ?>
                  swal("Oops","Ubah plan menjadi \"Agency\" untuk bisa download PDF Proposal.","info", {
                    buttons: {
                      cancel:true,
                      upgrade:{
                        text: "Ubah Plan",
                        value: "upgrade",
                      }
                    },
                  })
                  .then((value) => {
                    switch (value) {

                      case "upgrade":
                        window.location = "<?php echo site_url('plan'); ?>";
                        break;

                      default:
                        break;
                    }
                  });
                <?php } ?>
            <?php } ?>
        });

        $('#send_invoice').on('click',function(){
            <?php if($this->session->userdata('user_status') == 1){ ?>
                $('#modal_send_invoice').modal('show');
            <?php }else{ ?>
                swal("Aktifkan Akun Anda","Untuk mengirim invoice, silahkan aktifkan akun Anda terlebih dahulu melalui link yang sudah kami kirimkan ke email Anda. Bila Anda tidak menerima email, silahkan hubungi Customer Service","info");
            <?php } ?>
        });

        $('#btn-send-invoice').on('click',function(){
            if($('#send_invoice_sender_name').val() == '')
            {
                swal("Oops","Silahkan isi nama Anda / pengirim invoice yang diinginkan","error");
            }
            else if($('#send_invoice_sender_email').val() == '')
            {
                swal("Oops","Silahkan isi email Anda / pengirim invoice yang diinginkan","error");
            }
            else if(!ValidateEmail($('#send_invoice_sender_email').val()))
            {
                swal("Oops","Pastikan format email pengirim sudah benar","error");
            }
            else if($('#send_invoice_client_email').tagsinput('items').length <= 0)
            {
              swal("Oops","Email klien belum diisi","error");
            }
            else if($('#send_invoice_client_email_subject').val() == '')
            {
              swal("Oops","Subject email belum diisi","error");
            }
            else if($('#send_invoice_client_email_message').val() == '')
            {
              swal("Oops","Isi email masih kosong","error");
            }
            else
            {
    //            swal('invoice berhasil dikirim');
    //            $('#modal_send_invoice').modal('hide');
                var em = $('#send_invoice_client_email').tagsinput('items');
                  var cc = $('#send_invoice_client_cc').tagsinput('items');

                  if(em.length > 0)
                  {
                   for(var j=0;j<em.length;j++)
                   {
                      if(!ValidateEmail(em[j]))
                      {
                        swal("Oops","Pastikan format email benar","error");
                        return false;
                      }
                   }
                  }

                  if(cc.length > 0)
                  {
                   for(var i=0;i<cc.length;i++)
                   {
                      if(!ValidateEmail(cc[i]))
                      {
                        swal("Oops","Pastikan format email di cc benar","error");
                        return false;
                      }
                   }
                  }

                  swal({
                    title: "Invoice akan dikirim",
                    text: "Apakah Anda yakin?",
                    icon: "info",
                    buttons: true,
                    dangerMode: true,
                  })
                  .then((kirim) => {
                    if (kirim) {
                      $('#btn-send-invoice').attr('disabled',true).html('Mengirim...');
                      $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>invoice/sendInvoice",
                        data: {
                            'sender_name' : $('#send_invoice_sender_name').val(),
                            'sender_email' : $('#send_invoice_sender_email').val(),
                            'client_email' : $('#send_invoice_client_email').val(),
                            'client_cc' : $('#send_invoice_client_cc').val(),
                            'client_name' : $('#send_invoice_client_name').val(),
                            'email_message' : $('#send_invoice_client_email_message').val(),
                            'email_subject' : $('#send_invoice_client_email_subject').val()
                            },
                        success: function(data){
                          $('#btn-send-invoice').attr('disabled',false).html('<i class="fa fa-send" aria-hidden="true"></i> Kirim');
                          if(data == 1)
                          {
                            swal('Invoice berhasil dikirim');
                            $('#modal_send_invoice').modal('hide');
                          }
                          else
                          {
                              swal('Ada kesalahan');
                          }

                        }, error:function(error){
                          swal('Ada kesalahan');
                        }
                      });
                    }
                  });
            }
      });

        $(document).on('click','.btn-save-team-logo-image',function(){
                    if($('#imglg').val() == '')
                    {
                      swal("Oops","Pilih gambar yang akan ditambahkan","error");
                    }
                    else
                    {
                      $('#form_upload_team_logo').submit();
                    }
                  });

        <?php if($team_logo && $team_logo->team_logo != NULL){ ?>
                    $(document).on('click','.btn-get-team-logo',function(){
                      swal({
                          title: "Gunakan Logo Usaha",
                          text: "Anda akan menggunakan logo usaha pada invoice ini. Lanjutkan?",
                          icon: "info",
                          buttons: true,
                          dangerMode: true,
                        })
                        .then((kirim) => {
                            if(kirim){
                                $.ajax({
                                method: "POST",
                                url : "<?php echo base_url();?>invoice/get_team_logo",
                                data : {
                                    id_invoice:<?php echo $invoice->id; ?>,
                                    team_logo:'<?php echo $team_logo->team_logo; ?>'
                                },
                                success : function(data){
                                    if(data == 1)
                                    {
                                    swal('Berhasil menyimpan logo');
                                    location.reload();
                                    }
                                    else
                                    {
                                    swal("Oops","Ada kesalahan. Silahkan coba kembali.","error");
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown)
                                {
                                    swal("Oops","Ada kesalahan. Silahkan coba kembali.","error");
                                }
                                });

                            }
                      });
                    });
        <?php } ?>

        $('#status_invoice').on('click',function(){
            $('#modal_status_invoice').modal('show');
        });

        $('#btn-change-status').on('click', function(){
            var status = $("input[name='statPay']:checked").val();

            $.ajax({
                method: "POST",
                url: "<?= base_url() ?>invoice/changeStatusPayment",
                data: {
                    statPay : status,
                    id_in : <?= $invoice->id ?>
                },
                success : function(data){
                    swal('Status pembayaran invoice berhasil diedit');
                    location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    swal("Oops","Ada kesalahan. Silahkan coba kembali.","error");
                }
            });
        });
    });
</script>
