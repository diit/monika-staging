<style>
    .bold{
        font-weight: bold;
    }
    p{
        line-height: 1;
    }
    h4{
        margin-bottom: 20px;
    }
    #item > tbody > tr > td{
        padding: 20px 25px 10px 25px;
    }
    #price > tbody > tr > td{
        border: none;
    }
</style>
<!-- start panel -->
<div style="top: 0;" class="floating-proposal-panel">
    <div class="proposal-menu-panel-right">
        <h3 class="hidden-xs">INVOICE</h3>
        <h4 class="hidden-lg hidden-md hidden-sm" style="margin-top:22px">INVOICE</h4>
    </div>
    <div style="display: flex;margin-left: auto;-webkit-box-pack: end;justify-content: flex-end;">
        <!-- hidden 25 April, adit -->
        <?php // if((!empty($plan)) && ($plan->status == 1) && ($plan->plan_id != 1) && ($plan->plan_id != 2)) { ?>
            <!--
            <div id="download_invoice" style="color: rgb(149, 155, 163);cursor: pointer;box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 20px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
                <span class="hidden-xs"><i class="fa fa-cloud-download"></i>  DOWNLOAD PDF</span>
                <i class="fa fa-cloud-download hidden-lg hidden-md hidden-sm"></i>
            </div>
            -->
        <?php // } ?>
    </div>
</div>
<!-- end of panel -->

<section id="main-content">
      <section class="wrapper">
        <div class="row mt">
            <div class="container">
                <div class="col-xs-12">
                    <div class="showback" id="all" style="padding:40px 20px 120px 40px">
                        <div class="row" style="margin:0; border-bottom:1px solid #ccc; padding-bottom:20px">
                            <div class="col-sm-6" <?php echo ($get_style && ($get_style->logoimageurl != NULL || $get_style->logoimageurl != ''))?'style="padding-top:35px;";':''; ?>>
                                <h4>Invoice <?= $invoice->number == NULL || $invoice->number == '' ? '':'#'.$invoice->number; ?></h4>
                                <h5 style="font-weight:bold"><?= $invoice->judul ?></h5>
                            </div>
                            <?php if($get_style && ($get_style->logoimageurl != NULL || $get_style->logoimageurl != '')){ ?>
                                <div class="col-sm-3 col-sm-offset-2">
                                    <div class="pull-right text-center">
                                        <img src="<?=$get_style->logoimageurl; ?>" style="width:200px;">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="row" style="margin:0; padding:40px 0 20px 0">
                            <div class="col-md-6">
                                <h5>DARI</h5>
                                <p class="bold"><?= $invoice->team_name ?></p>
                                <h5>KEPADA</h5>
                                <p class="bold"><?= ($invoice->perusahaan)=='' ? $invoice->nama_pic : $invoice->perusahaan; ?></p>
                                <?= ($invoice->perusahaan)=='' ? '' : $invoice->nama_pic; ?>
                                <?= ($invoice->alamat_usaha)=='' ? '' : '<p>'.$invoice->alamat_usaha.'</p>' ?>
                                <?= ($invoice->kota)=='' ? '' : '<p>'.$invoice->kota.'</p>' ?>
                            </div>
                            <div class="col-md-6 text-right">
                                <?= ($invoice->send_invoice)=='' ? '' : '<h5>Batas akhir pembayaran</h5> <h5 class="bold">'.$this->monikalib->format_date_indonesia($invoice->send_invoice).'</h5>' ?>
                            </div>
                        </div>

                        <div class="row" style="margin-top:20px">
                            <div class="col-xs-12">
                                <table class="table table-striped" id="item">
                                    <tr>
                                        <th class="text-center" style="width:70%; padding-top:15px"><h5 class="bold">DESKRIPSI ITEM</h5></th>
                                        <th class="text-center" style="width:30%; padding-top:15px"><h5 class="bold">BIAYA</h5></th>
                                    </tr>
                                    <?php
                                        $sub_tot = 0;
                                        if(count($item) > 0){
                                            $c_item = count($item);
                                            foreach($item as $id=>$d) {
                                                if($d->item != NULL && $d->harga > 0){
                                    ?>
                                    <tr>
                                        <td><h5><?= $d->item ?></h5></td>
                                        <td class="text-right"><?= ($d->harga)=='' ? '' : '<h5> Rp '.number_format($d->harga,"0",",",".").'</h5>' ?></td>
                                    </tr>
                                    <?php
                                                    $sub_tot = $sub_tot+($d->harga);
                                                }
                                            }
                                        }
                                    ?>
                                    <tr style="border-bottom:1px solid #ccc">
                                        <td colspan="5"></td>
                                    </tr>
                                </table>
                                <table class="table" id="price">
                                    <tr style="font-weight:bold">
                                        <td class="text-right" style="width:65%"><h5>SUB TOTAL</h5></td>
                                        <td class="text-right" style="width:35%"><h5>Rp <?= number_format($sub_tot,0,",",".") ?></h5></td>
                                    </tr>
                                    <tr style="font-weight:bold">
                                        <?php $tax = (($invoice->tax)/100)*$sub_tot;  ?>
                                        <td class="text-right"><h5>TAX <?= number_format($invoice->tax,0) ?> %</h5></td>
                                        <td class="text-right" id="getTaxPrice"><h5>Rp <?= number_format($tax,0,",",".") ?></h5></td>
                                    </tr>
                                    <tr>
                                        <?php $tot = $sub_tot + $tax; ?>
                                        <td class="text-right"><h5 class="bold">TOTAL</h5></td>
                                        <td class="text-right"><h5 class="bold">Rp <?= number_format($tot,0,",",".") ?></h5></td>
                                    </tr>
                                </table>

                                <div class="col-sm-8 col-xs-12" style="margin-top:60px">
                                    <?= ($invoice->note)=='' ? '' : '<h5>CATATAN</h5><p>'.$invoice->note.'</p>' ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="visibility: hidden;" class="floatButton" >
                      <div class="row btn-fab" >
                        <div id="console">
                          <p>--sementara ditampilkan dulu<br>untuk Johan--</p>
                          <ul>
                                <li data-time="0" data-field="all" tot-wak="0">all: <span>0</span>s</li>
                          </ul>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
    </section>
</section>

<script src="<?php echo base_url(); ?>assets/js/screentime.js"></script>
<script>
    $('#download_invoice').click( function(e) {
          e.preventDefault();
            <?php
                $random = md5(mt_rand(1,10000));
                $first = substr($random,0,5);
                $last = substr($random,5,10);
                $urlrand = $first.$invoice->id.$last;
             ?>
          window.open("<?php echo site_url('invoice/userdownload/'.$urlrand);?>","_blank");
          return false;
    });

    $(document).ready(function(){
        var varscreen;



          var dataSection = [
            { selector: '#all',
              name: 'all'
            }
          ];


          var time = 0;
          var counter = 0;
          //console.log(varscreen);
          $.screentime({
            fields: dataSection,
            reportInterval: 1,
            percentOnScreen: "65%",
            callback: function(data) {

              $.each(data, function(key, val) {
                var $elem = $('#console li[data-field="' + key + '"]');
                var current = parseInt($elem.data('time'), 10);
                $elem.data('time', current + val);
                $elem.find('span').html(current += val);
                $elem.attr('tot-wak',current);
                //varscreen[key] = current;
              });

              var cumi;
              time++;
              if(time%10 == 0)
              {
                counter++;
                varscreen = [];
                $('li[tot-wak]').each(function(){
                  cumi = {};
                  cumi[$(this).attr('data-field')] = parseInt($(this).attr('tot-wak'));
                  varscreen.push(cumi);
                });

                $.ajax({
                  url : "<?php echo site_url('userview/tracking_invoice')?>",
                  data : {
                    invoice_id : <?php echo $invoice->id; ?>,
                    invoice_track : varscreen,
                    tracking_count : counter
                  },
                  type: "POST",
                  success: function(data)
                  {
                  }
                });
              }
            }
          });
    });
</script>
