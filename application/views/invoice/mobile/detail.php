<div id="content">
    <!-- menu -->
    <div class="box toolbar-box">
        <button class="btn btn-sm btn-outline-primary pull-right" data-toggle="modal" data-target="#modal-kirim-invoice"><i class="far fa-envelope"></i> kirim</button>

        <button class="btn btn-sm btn-outline-secondary pull-right" <?= (($this->monikalib->currentUserPlan()==0) || ($this->monikalib->getUserPlan()==1)) ? 'id="notice-plan"' : 'id="download-invoice"' ?>><i class="far fa-arrow-alt-circle-down"></i> download</button>

        <button class="btn btn-sm btn-outline-secondary pull-right" id="copy-share-link"><i class="far fa-clone"></i> share url</button>
        <p class="pull-left" style="color:transparent">.</p>
    </div>
    <!-- end of menu -->

    <!-- invoice content -->
    <div class="box top-space" id="invoice-content">
        <p class="grey"><?= (empty($invoice->number))? '':'#'.$invoice->number; ?></p>
        <p class="bold"><?= $invoice->judul ?></p>
        <div class="border-bottom"></div>
        &nbsp;
        <p><span class="text-little">DARI</span><br><?= $invoice->team_name ?></p>
        <p><span class="text-little">KEPADA</span><br><?= $invoice->nama_pic ?><br><?=$invoice->perusahaan?></p>
        <p class="text-little"><?=$invoice->alamat_usaha?><br><?=$invoice->kota?></p>
        <p class="text-little">Batas akhir pembayaran<br><span class="bold"><?=$this->monikalib->format_date_indonesia($invoice->due_invoice)?></span></p>

        &nbsp;
        <table class="table border-bottom">
            <thead>
                <tr>
                    <th style="width:65%" class="text-center">DESKRIPSI ITEM</th>
                    <th class="text-center">BIAYA</th>
                </tr>
            </thead>
            <tbody class="text-little">
                <?php
                    $sub_tot = 0;
                    if(count($item) > 0){
                        $c_item = count($item);
                        foreach($item as $id=>$d) {
                            if($d->item != NULL && $d->harga > 0){
                ?>
                <tr>
                    <td><?= $d->item ?></td>
                    <td class="text-right"><?php echo (empty($d->harga))? '' : 'Rp '.number_format($d->harga,"0",",",".") ?></td>
                </tr>
                <?php
                                $sub_tot = $sub_tot+($d->harga);
                            }
                        }
                    }
                ?>
            </tbody>
        </table>
        <div class="row">
            <?php
                $tax = (($invoice->tax)/100)*$sub_tot;
                $tot = $sub_tot + $tax;
            ?>
            <div class="col-6 text-right">
                <p class="text-little">SUB TOTAL</p>
                <p class="text-little">Tax <?= number_format($invoice->tax,0) ?>%</p>
                <p class="bold">TOTAL</p>
            </div>
            <div class="col-6 text-right" style="padding-right:28px">
                <p class="text-little">Rp <?=number_format($sub_tot,0,",",".")?></p>
                <p class="text-little">Rp <?= number_format($tax,0,",",".") ?></p>
                <p class="bold">Rp <?= number_format($tot,0,",",".") ?></p>
            </div>
        </div>
        &nbsp;
        <?php if(!empty($invoice->note)){ ?>
        <div class="row">
            <div class="col-7">
                <p class="text-little"><?=$invoice->note?></p>
            </div>
        </div>
        <?php } ?>
    </div>
    <!-- end of invoice content -->
</div>

<!-- modal kirim  -->
<div class="modal fade modal-kirim" id="modal-kirim-invoice" tabindex="-1" role="dialog" aria-labelledby="modalKirimProposal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header text-center">
            <h5 class="modal-title">Kirim Invoice</h5>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label for="">Nama Pengirim</label>
                    <input class="form-control" type="text" placeholder="nama pengirim" value="<?= $invoice->user_name?>" id="send_invoice_sender_name" readonly>
                </div>
                <div class="col-md-6">
                    <label for="">Email Pengirim</label>
                    <input class="form-control" type="email" placeholder="email pengirim" value="<?= $invoice->user_email?>" id="send_invoice_sender_email" readonly>
                </div>
            </div> <br>
            <div class="row">
                <div class="col-md-12">
                    <input hidden value="<?=$invoice->nama_pic?>" id="send_invoice_client_name">
                    <div class="form-group">
                        <label for="">Email Klien</label>
                        <input class="form-control" type="text" data-role="tagsinput" value="<?= $invoice->email?>" id="send_invoice_client_email">
                        <div id="email-client-msg"></div>
                    </div>
                    <div class="form-group">
                        <label for="">CC</label>
                        <input class="form-control" type="email" data-role="tagsinput" placeholder="email klien" id="send_invoice_client_cc">
                    </div>
                    <div class="form-group">
                        <label for="">Subjek</label>
                        <input class="form-control" type="text" placeholder="Judul Proposal" value="<?= $invoice->judul ?>" id="send_invoice_client_email_subject">
                        <div id="subject-msg"></div>
                    </div>
                    <div class="form-group">
                        <label for="">Pesan</label>
                        <textarea class="form-control" name="pesan-proposal" cols="30" rows="10" id="send_invoice_client_email_message">Dengan Hormat,
Berikut Proposal [...] yang bisa Anda baca.
Klik di sini > <?= $link ?>
Bila ada pertanyaan silahkan langsung tanyakan ke saya dengan balas email ini.

Terima kasih,
<?= $invoice->user_name?>
                        </textarea>
                        <div id="message-msg"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">batal</button>
            <?php
                $random = md5(mt_rand(1,10000));
                $first = substr($random,0,5);
                $last = substr($random,5,10);
                $id_in = $first.$invoice->id.$last;
            ?>
            <button class="btn btn-primary" type="button" id="btn-kirim-invoice" idi="<?= $id_in ?>">kirim</button>
        </div>
    </div>
  </div>
</div>
<!-- end of modal kirim  -->
