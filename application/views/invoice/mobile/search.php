<?php
$status_send = <<<EOD
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 299.997 299.997" style="enable-background:new 0 0 299.997 299.997;" xml:space="preserve" width="24px" height="24px" data-toggle="tooltip" data-placement="top" title="Terkirim">
    <path d="M149.996,0C67.157,0,0.001,67.158,0.001,149.997c0,82.837,67.156,150,149.995,150s150-67.163,150-150C299.996,67.158,232.835,0,149.996,0z M145.294,100.159h111.864c0.763,0,1.502,0.091,2.225,0.223l-62.648,36.017l-52.964-36.087C144.275,100.25,144.77,100.159,145.294,100.159z M60.572,185.31v-15.558h49.921l-0.609,5.047l-0.112,0.931v0.936c0,3.019,0.501,5.921,1.398,8.644H60.572z M112.293,154.842h-68.58V139.28h70.465L112.293,154.842z M117.571,111.218l-0.965,7.965H28.585v-15.562h90.384C118.17,106.028,117.677,108.573,117.571,111.218z M249.375,188.852H137.513c-3.348,0-6.378-1.351-8.58-3.538c0,0,0,0,0-0.003c-0.539-0.537-1.027-1.128-1.46-1.758c-0.01-0.016-0.029-0.031-0.039-0.047c-0.402-0.594-0.737-1.232-1.032-1.891c-0.029-0.065-0.073-0.122-0.104-0.189c-0.265-0.622-0.451-1.284-0.609-1.956c-0.029-0.117-0.083-0.223-0.106-0.34c-0.163-0.799-0.249-1.621-0.249-2.464l4.145-34.259l0.379-3.13l3.258-26.94c0-0.77,0.093-1.515,0.231-2.243c0.016-0.078,0.008-0.163,0.026-0.241c0.01,0.005,0.018,0.013,0.029,0.021c0.35-1.662,1.012-3.206,1.958-4.547v5.88l57.741,39.132c0.078,0.054,0.163,0.083,0.244,0.13c0.083,0.052,0.169,0.093,0.257,0.14c0.456,0.233,0.923,0.42,1.401,0.545c0.052,0.013,0.099,0.021,0.15,0.031c0.524,0.124,1.056,0.2,1.582,0.2h0.005c0.005,0,0.008,0,0.01,0c0.527,0,1.058-0.075,1.582-0.2c0.052-0.01,0.099-0.018,0.15-0.031c0.477-0.124,0.944-0.311,1.401-0.545c0.086-0.047,0.171-0.088,0.257-0.14c0.08-0.047,0.163-0.075,0.244-0.13l68.792-39.716c0.08,0.565,0.171,1.128,0.171,1.717l-7.781,64.329C261.559,183.4,256.105,188.852,249.375,188.852z" fill="#3cbdb1" />
</svg>
EOD;

$status_read = <<<EOD
<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" data-toggle="tooltip" data-placement="top" title="Dibaca">
    <path d="m512 256c0 141.386719-114.613281 256-256 256s-256-114.613281-256-256 114.613281-256 256-256 256 114.613281 256 256zm0 0" fill="#ec9d21"/><g fill="#fff"><path d="m256 379.960938c-40.882812 0-81.003906-11.132813-116.027344-32.199219-33.941406-20.417969-62.304687-49.589844-82.023437-84.363281-2.601563-4.589844-2.601563-10.207032 0-14.796876 19.71875-34.773437 48.082031-63.945312 82.023437-84.363281 35.023438-21.0625 75.144532-32.199219 116.027344-32.199219s81.003906 11.136719 116.027344 32.199219c33.941406 20.417969 62.304687 49.589844 82.023437 84.363281 2.601563 4.589844 2.601563 10.207032 0 14.796876-19.71875 34.773437-48.082031 63.945312-82.023437 84.363281-35.023438 21.066406-75.144532 32.199219-116.027344 32.199219zm-167.546875-123.960938c36.328125 58.175781 99.757813 93.960938 167.546875 93.960938s131.21875-35.785157 167.546875-93.960938c-36.328125-58.175781-99.757813-93.960938-167.546875-93.960938s-131.21875 35.785157-167.546875 93.960938zm0 0"/><path d="m256 319.332031c-34.921875 0-63.332031-28.410156-63.332031-63.332031s28.410156-63.332031 63.332031-63.332031 63.332031 28.410156 63.332031 63.332031-28.410156 63.332031-63.332031 63.332031zm0-96.664062c-18.378906 0-33.332031 14.953125-33.332031 33.332031s14.953125 33.332031 33.332031 33.332031 33.332031-14.953125 33.332031-33.332031-14.953125-33.332031-33.332031-33.332031zm0 0"/></g>
</svg>
EOD;

$status_accept = <<<EOD
<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" data-toggle="tooltip" data-placement="top" title="Disetujui">
    <path d="m512 256c0 141.386719-114.613281 256-256 256s-256-114.613281-256-256 114.613281-256 256-256 256 114.613281 256 256zm0 0" fill="#a2cb39"/><path d="m175 395.246094c-4.035156 0-7.902344-1.628906-10.726562-4.511719l-81-82.832031c-5.789063-5.921875-5.683594-15.417969.238281-21.210938 5.921875-5.792968 15.417969-5.6875 21.210937.238282l70.273438 71.859374 232.277344-237.523437c5.792968-5.921875 15.289062-6.027344 21.210937-.234375 5.925781 5.789062 6.03125 15.289062.238281 21.210938l-243 248.492187c-2.820312 2.882813-6.6875 4.511719-10.722656 4.511719zm0 0" fill="#fff"/>
</svg>
EOD;
?>

<?php
    foreach($result as $d) {
        $random = md5(mt_rand(1,10000));
        $first = substr($random,0,5);
        $last = substr($random,5,10);
        $in_id = $first.$d->id.$last;
?>
<li id="div_list_invoice_<?= $in_id ?>">
    <a href="<?php echo site_url('invoice/lihat/'.$in_id); ?>"><p class="title-list-view"><?= $d->judul ?></p></a>
    <p class="mini-text grey">Dibuat <?= $this->monikalib->format_date_indonesia($d->created_at) ?></p>
    <p class="header">Nama PIC</p>
    <p class="text-thin"><?= $d->nama_pic ?></p>
    <div class="row">
        <div class="col">
            <p class="header">Kirim</p>
            <p class="text-thin"><?= ($d->send_invoice=='')? 'Not Set' : $this->monikalib->format_date_indonesia($d->send_invoice) ?></p>
        </div>
        <div class="col">
            <p class="header">Deadline</p>
            <p class="text-thin"><?= ($d->due_invoice=='')? 'Not Set' : $this->monikalib->format_date_indonesia($d->due_invoice) ?></p>
        </div>
    </div>
    <?php
        $CI =& get_instance();
        $CI->load->model('invoice_model');
        $result = $CI->invoice_model->getItem($d->id);
        $total = 0;
        foreach($result  as $row){
            $total = $total + $row->harga;
        }
        $tagihan = ((($d->tax)/100)*$total)+$total;
    ?>
    <div class="row">
        <div class="col">
            <p class="header">Total Tagihan</p>
            <p class="text-thin">Rp <?= number_format($tagihan,'0',',','.') ?></p><br>
        </div>
        <div class="col">
            <p class="header">Dipersiapkan Oleh</p>
            <p class="text-thin"><?= ($d->id_user == $this->session->userdata('user_id')) ? 'Anda' : $d->user_name ?></p>
        </div>
    </div>
    <div class="row space-bottom">
        <div class="col">
            <button class="btn btn-sm btn-outline-primary btn-mobile arsip-in" iid="<?= $in_id ?>"><i class="far fa-trash-alt"></i></button>
        </div>
        <div class="col">
            <p class="pull-right" id="doc-stat">
            <?php
            if ($d->status == 1) {
                echo $status_send; echo $status_read;
            }
            elseif ($d->status == 2) {
                echo $status_send; echo $status_read; echo $status_accept;
            }
            ?>
        </p>
        </div>
    </div>
</li>
<?php } ?>

<script>
    $(document).ready(function() {
        $('.arsip-in').on('click',function(){
            var invoice_id = $(this).attr('iid');
            swal({
              title: "Apakah Anda yakin?",
              text: "Invoice masih bisa diakses pada menu Arsip.",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $.ajax({
                    type: "POST",
                    url: '<?=base_url()?>invoice/arsip',
                    data: {
                        'id_in' : invoice_id
                      },
                    success: function(data)
                    {
                      $('#div_list_invoice_'+invoice_id).remove();
                      swal("Archived","Invoice berhasil diarsipkan","success");
                      location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      swal("Oops","Invoice gagal diarsipkan. Silahkan mencoba kembali.","error");
                    }
                });
              }
            });
        });
    });
</script>
