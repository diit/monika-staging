<div id="user-view-content">
    <!-- menu -->
    <div class="box toolbar-box">
        <button class="btn btn-sm btn-outline-secondary pull-right" <?= ($this->monikalib->currentUserPlan()==0) ? 'id="notice-plan"' : 'id="download-invoice"' ?>><i class="far fa-arrow-alt-circle-down"></i> download</button>
        <p class="pull-left">INVOICE</p>
    </div>
    <!-- end of menu -->

    <!-- invoice content -->
    <div class="box top-space" id="invoice-box">
        <p class="grey"><?= (empty($invoice->number))? '':'#'.$invoice->number; ?></p>
        <p class="bold"><?= $invoice->judul ?></p>
        <div class="border-bottom"></div>
        &nbsp;
        <p><span class="text-little">DARI</span><br><?= $invoice->team_name ?></p>
        <p><span class="text-little">KEPADA</span><br><?= $invoice->nama_pic ?><br><?=$invoice->perusahaan?></p>
        <p class="text-little"><?=$invoice->alamat_usaha?><br><?=$invoice->kota?></p>
        <?php if((!empty($invoice->due_invoice)) && ($invoice->due_invoice != '0000-00-00 00:00:00')){ ?>
        <p class="text-little">Batas akhir pembayaran<br><span class="bold"><?=$this->monikalib->format_date_indonesia($invoice->due_invoice)?></span></p>
        <?php } ?>

        &nbsp;
        <table class="table border-bottom">
            <thead>
                <tr>
                    <th style="width:65%" class="text-center">DESKRIPSI ITEM</th>
                    <th class="text-center">BIAYA</th>
                </tr>
            </thead>
            <tbody class="text-little">
                <?php
                    $sub_tot = 0;
                    if(count($item) > 0){
                        $c_item = count($item);
                        foreach($item as $id=>$d) {
                            if($d->item != NULL && $d->harga > 0){
                ?>
                <tr>
                    <td><?= $d->item ?></td>
                    <td class="text-right"><?php echo (empty($d->harga))? '' : 'Rp '.number_format($d->harga,"0",",",".") ?></td>
                </tr>
                <?php
                                $sub_tot = $sub_tot+($d->harga);
                            }
                        }
                    }
                ?>
            </tbody>
        </table>
        <div class="row">
            <?php
                $tax = (($invoice->tax)/100)*$sub_tot;
                $tot = $sub_tot + $tax;
            ?>
            <div class="col-6 text-right">
                <p class="text-little">SUB TOTAL</p>
                <p class="text-little">Tax <?= number_format($invoice->tax,0) ?>%</p>
                <p class="bold">TOTAL</p>
            </div>
            <div class="col-6 text-right" style="padding-right:28px">
                <p class="text-little">Rp <?=number_format($sub_tot,0,",",".")?></p>
                <p class="text-little">Rp <?= number_format($tax,0,",",".") ?></p>
                <p class="bold">Rp <?= number_format($tot,0,",",".") ?></p>
            </div>
        </div>
        &nbsp;
        <?php if(!empty($invoice->note)){ ?>
        <div class="row">
            <div class="col-7">
                <p class="text-little"><?=$invoice->note?></p>
            </div>
        </div>
        <?php } ?>
    </div>
    <!-- end of invoice content -->

    <div id="console" style="display:none">
        <ul>
            <li data-time="0" data-field="all" total-time=""><span class="total">0</span>s</li>
        </ul>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/screentime.js"></script>
<script src="<?= base_url() ?>node_modules/sweetalert/dist/sweetalert.min.js"></script>
<script>
$(document).ready(function(){
    $('#download-invoice').click(function(){
        <?php
            $random = md5(mt_rand(1,10000));
            $first = substr($random,0,5);
            $last = substr($random,5,10);
            $urlrand = $first.$invoice->id.$last;
        ?>
        location.href = ("<?php echo site_url('userview/downloadPdfInvoice/'.$urlrand);?>");
    });

    // screen time
    var areascreen = [
        {
            selector:'#invoice-box',
            name:'all'
        }
    ];
    var time = 0;
    var counter = 0;
    $.screentime({
        fields: areascreen,
        reportInterval: 1,
        callback: function(data) {
            $.each(data, function(key, val) {
                var $elem = $('#console li[data-field="' + key + '"]'); //pilih element untuk store value per section
                var current = parseInt($elem.data('time'), 10); // initial variabel
                $elem.data('time', current + val); // store value pada atribute data-time
                // $elem.find('span.total').html(current += val); // menampilkan total waktu pada element span di dalam element li yang dipilih diawal
                $elem.attr('total-time',current);
            });
            time++;
            if(time%5 == 0)
            {
                counter++;
                invoice_time = [];
                var section_time = $('#console li');
                $(section_time).each(function(){
                    section = {};
                    section[$(this).data('field')] = $(this).attr('total-time');
                    invoice_time.push(section);
                });
                // console.log(invoice_time);
                $.ajax({
                    type: "POST",
                    url : "<?php echo site_url('userview/tracking_invoice')?>",
                    data : {
                        invoice_id : <?php echo $invoice->id; ?>,
                        invoice_track : invoice_time,
                        tracking_count : counter
                    },
                    success: function(data)
                    {
                        // console.log('Berhasil');
                    }
                });
            }
        }
    });

    $('#notice-plan').click(function(){
        swal('Download PDF Invoice','Invoice belum bisa di download, karena pengirim menggunakan plan Personal. Silahkan hubungi pengirim untuk upgrade plan.');
    });
});
</script>
