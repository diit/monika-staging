<script src="<?= base_url() ?>node_modules/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-tagsinput.js"></script>

<script>
    $(document).ready(function() {
        // SCRIPT INDEX
        <?php if (($this->uri->segment(1) == 'invoice') && (empty($this->uri->segment(2)))) { ?>
        $('#history').hide();
        $('.view-history').click(function() {
            $('#history').toggle();
        });

        $('.arsip-in').on('click',function(){
            var invoice_id = $(this).attr('iid');
            swal({
              title: "Apakah Anda yakin?",
              text: "Invoice masih bisa diakses pada menu Arsip.",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $.ajax({
                    type: "POST",
                    url: '<?=base_url()?>invoice/arsip',
                    data: {
                        'id_in' : invoice_id
                      },
                    success: function(data)
                    {
                      $('#div_list_invoice_'+invoice_id).remove();
                      swal("Archived","Invoice berhasil diarsipkan","success");
                      location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      swal("Oops","Invoice gagal diarsipkan. Silahkan mencoba kembali.","error");
                    }
                });
              }
            });
        });

        $('#cari-invoice').keyup(function(){
          var query = $(this).val();
            $.ajax({
              url:"<?php echo base_url();?>invoice/invoice",
              method:"post",
              data:{query:query},
              success:function(data){
                $('#list-invoice').remove();
                $('#list-cari-invoice').html(data);
              }
            });
        });

        $('#add').click(function(){
            swal('Buat invoice melalui desktop view di PC atau Laptop.');
        });
        <?php } ?>
        // END OF SCRIPT INDEX

        //SCRIPT DETAIL
        <?php if ($this->uri->segment(2) == 'detail') { ?>
        <?php
            $random = md5(mt_rand(1,10000));
            $first = substr($random,0,5);
            $last = substr($random,5,10);
            $urlrand = $first.$invoice->id.$last;
        ?>

        $('#download-invoice').click(function(){
            location.href = ("<?= site_url('userview/downloadPdfInvoice/'.$urlrand) ?>");
        });

        $('#notice-plan').on('click',function(){
            swal('Download Invoice','Upgrade plan ke Startup atau Agency untuk download invoice');
        });

        function copyToClipboard(text) {
            var selected = false;
            var el = document.createElement('textarea');
            el.value = text;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            if (document.getSelection().rangeCount > 0) {
                selected = document.getSelection().getRangeAt(0)
            }
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
            if (selected) {
                document.getSelection().removeAllRanges();
                document.getSelection().addRange(selected);
            }
        };

        $('#copy-share-link').on('click',function(){
            copyToClipboard('<?=$link?>');
            $.ajax({
              success : function(data){
                swal("Perhatian","Kami tidak merekomendasikan Anda mengirimkan invoice ini menggunakan cara selain menggunakan tombol KIRIM di kanan atas. Bila Anda tetap menginginkan, silahkan bagikan link URL ini.","warning");
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                swal("Oops","Link gagal disalin","error");
              }
            });
        });

        $(document).on('click','#btn-kirim-invoice', function(){
            var client_email = $('#send_invoice_client_email').val();
            var subject = $('#send_invoice_client_email_subject').val();
            var message = $('#send_invoice_client_email_message').val();

            if (client_email == '') {
                $('#email-client-msg').empty().append("<div class='alert alert-fit alert-danger text-little'>Email klien wajib diisi (minimal 1)</div>");
            }
            else if (subject == '') {
                $('#subject-msg').empty().append("<div class='alert alert-fit alert-warning text-little'>Kami menyarankan untuk mengisi subject email Anda</div>")
            }
            else if (message == '') {
                $('#message-msg').empty().append("<div class='alert alert-fit alert-danger text-little'>Pesan email wajib diisi</div>");
            }
            else {
                $('#btn-kirim-invoice').attr('disabled',true).html('Mengirim...');
                $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>invoice/sendInvoice",
                data: {
                    'sender_name' : $('#send_invoice_sender_name').val(),
                    'sender_email' : $('#send_invoice_sender_email').val(),
                    'client_email' : $('#send_invoice_client_email').val(),
                    'client_name' : $('#send_invoice_client_name').val(),
                    'client_cc' : $('#send_invoice_client_cc').val(),
                    'email_message' : $('#send_invoice_client_email_message').text(),
                    'email_subject' : $('#send_invoice_client_email_subject').val(),
                    'id' : $(this).attr('idi')
                    },
                success: function(data){
                    $('#btn-kirim-invoice').attr('disabled',false).html('<i class="fa fa-send" aria-hidden="true"></i> Terkirim');
                    $('#modal-kirim-invoice').modal('hide');
                    swal('Invoice Terkirim','Invoice berhasil dikirim ke email klien Anda','success');
                }, error:function(error){
                    swal('Ada kesalahan');
                }
                });
            }
        });
        <?php } ?>
        //END OF SCRIPT DETAIL
    });


</script>
