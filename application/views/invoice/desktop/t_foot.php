<script src="<?= base_url() ?>node_modules/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?= base_url() ?>assets/js/select2.min.js"></script>
<script src="<?= base_url() ?>assets/js/gijgo-datepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-tagsinput.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-toggle.min.js"></script>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        //SCRIPT INDEX
        <?php if(($this->uri->segment(1) == 'invoice') && (empty($this->uri->segment(2)))) { ?>
        $('.arsip-in').on('click',function(){
            var invoice_id = $(this).attr('iid');
            swal({
              title: "Apakah Anda yakin?",
              text: "Invoice masih bisa diakses pada menu Arsip.",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $.ajax({
                    type: "POST",
                    url: '<?=base_url()?>invoice/arsip',
                    data: {
                        'id_in' : invoice_id
                      },
                    success: function(data)
                    {
                      $('#div_list_invoice_'+invoice_id).remove();
                      swal("Archived","Invoice berhasil diarsipkan","success");
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      swal("Oops","Invoice gagal diarsipkan. Silahkan mencoba kembali.","error");
                    }
                });
              }
            });
        });

        $('#cari-invoice').keyup(function(){
          var query = $(this).val();
            $.ajax({
              url:"<?php echo base_url();?>invoice/invoice",
              method:"post",
              data:{query:query},
              success:function(data){
                $('#list-invoice').remove();
                $('#list-cari-invoice').html(data);
              }
            });
        });
        <?php } ?>
        //END OF SCRIPT INDEX

        //SCRIPT CREATE, DETAIL
        <?php if(($this->uri->segment(2) == 'create') || ($this->uri->segment(2) == 'detail')) { ?>
        var invoice_id = $('#saveInvoice').attr('iid');

        $('.basic-single').select2({
            dropdownParent: $("#client_select")
        });

        $('.datepicker-sent').datepicker({
            uiLibrary: 'bootstrap4'
        });
        $('.datepicker-last').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('#select_duplicate_client').on('change',function(){
            if($(this).val() !== 'baru'){
                var client_id = $(this).val();
                $.ajax({
                    type:"GET",
                    url: "<?php echo base_url()?>client/pilihclient",
                    data: {id:client_id},
                    datatype: 'json',
                    success: function(data){
                        var datanya = JSON.parse(data);
                        document.getElementById("id_client").value = client_id;
                        document.getElementById("namaKlien").value = datanya.nama;
                        document.getElementById("namaPerusahaan").value = datanya.perusahaan;
                        document.getElementById("emailKlien").value = datanya.email;
                        document.getElementById("alamatPerusahaan").value = datanya.alamat;
                        document.getElementById("kotaPerusahaan").value = datanya.kota;
                        document.getElementById("noTelpKlien").value = datanya.telp;
                        if (document.getElementById("emailKlien").value !== ''){
                            document.getElementById("emailKlien").setAttribute('readonly', 'true');
                        }else{
                            $('#emailKlien').removeAttr("readonly");
                        }
                        document.getElementById("namaKlien").setAttribute('readonly', 'true');
                        document.getElementById("namaPerusahaan").setAttribute('readonly', 'true');
                    }, error:function(error){
                        swal('error;' +eval(error));
                    }
                    });

            }else if($(this).val() == 'baru'){
                $('#namaKlien').removeAttr("readonly");
                $('#namaPerusahaan').removeAttr("readonly");
                $('#emailKlien').removeAttr("readonly");
                $('#namaKlien').val("");
                $('#id_client').val("");
                $('#namaPerusahaan').val("");
                $('#emailKlien').val("");
                $('#alamatPerusahaan').val("");
                $('#kotaPerusahaan').val("");
                $('#noTelpKlien').val("");
            }
        });
        <?php } ?>
        //END OF SCRIPT CREATE, DETAIL

        //SCRIPT CREATE
        <?php if($this->uri->segment(2) == 'create') { ?>
        function ValidateEmail(email) {
            var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return expr.test(email);
        };

        $(document).on('click','#createInvoice', function(){
            var number = $('#noInvoice').val();
            var title = $('#titleInvoice').val();
            var note = $('#catatan').val();
            var tax = $('#tax').val();
            var sendInvoice = $('#sendInvoice').val();
            var dueInvoice = $('#dueInvoice').val();
            // var idKlien = $('#select_duplicate_client').val();
            var idKlien = $('#id_client').val();
            var namaKlien = $('#namaKlien').val();
            var namaUsaha = $('#namaPerusahaan').val();
            var addKlien = $('#alamatPerusahaan').val();
            var kotaKlien = $('#kotaPerusahaan').val();
            var email = $('#emailKlien').val();
            var telpKlien = $('#telpKlien').val();
            var logo_team = $('#logo-proposal').val();
            var notelp = $('#noTelpKlien').val();
            var share = $('#share-team').val();
            if(share=='true'){
                var share = 1;
            }
            else{
                var share = 0;
            }

            if(namaKlien == '')
            {
                swal("Oops","Nama klien belum diisi","error");
            }
            else if(email == '')
            {
                swal("Oops","Email klien belum diisi","error");
            }
            else if(!ValidateEmail(email))
            {
                swal("Oops","Pastikan format email klien sudah benar","error");
            }
            else
            {
                var item = [];
                $('[name="item"]').each(function(){
                    if($(this).val() != '')
                    {
                        item.push($(this).val());
                    }
                });

                var biaya=[];
                $('[name="biaya"]').each(function(){
                    if($(this).val() != '')
                    {
                        biaya.push($(this).val().trim().replace(/\./g,''));
                    }
                });

                if(item.length > 0)
                {
                    if(title==''){
                        swal('Oops','Judul invoice tidak boleh kosong','error');
                    }
                    else{
                        $.ajax({
                            type: "POST",
                            url: "<?=base_url()?>invoice/store",
                            data: {
                                'no_in' : number,
                                'judul_in' : title,
                                'id_klien' : idKlien,
                                'note_in' : note,
                                'tax_in' : tax,
                                'send_in' : sendInvoice,
                                'due_in' : dueInvoice,
                                'add_klien' : addKlien,
                                'kota_klien' : kotaKlien,
                                'biaya' : biaya,
                                'item' : item,
                                'namapic' : namaKlien,
                                'email' : email,
                                'telp' : notelp,
                                'perusahaan' : namaUsaha,
                                'share' : share
                            },
                            success: function(data)
                            {
                                if(data !== 0)
                                {
                                    swal("Invoice Tersimpan","Invoice berhasil dibuat","success");
                                    location.href = '<?php echo site_url('invoice'); ?>';
                                }
                                else
                                {
                                    swal("Oops","Ada kesalahan. Silahkan mencoba kembali.","error");
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown)
                            {
                                swal("Oops","Invoice gagal dibuat. Silahkan mencoba kembali.","error");
                            }
                        });
                    }
                }
                else
                {
                    swal("Oops","Pastikan Anda sudah memasukan item.","error");
                }
            }
        });

        $(function() {
            $('#toggle-event').change(function() {
                $('#share-team').val($(this).prop('checked'));
            });
        });
        <?php } ?>
        //END OF SCRIPT CREATE

        //SCRIPT DETAIL
        <?php if($this->uri->segment(2) == 'detail') { ?>
        $('#share-link').on('click',function(){
            $(this).select();
        });

        $('#download-invoice').on('click',function(){
            location.href = '<?= base_url('invoice/download/')?>'+invoice_id;
        });

        $(document).on('click','#saveInvoice', function(){
            var idInvoice = $(this).attr('iid');
            var number = $('#noInvoice').val();
            var title = $('#titleInvoice').val();
            var idProposal = $(this).attr('pid');
            // var idKlien = $('#select_duplicate_client').val();
            var idKlien = $('#id_client').val();
            var note = $('#catatan').val();
            var tax = $('#tax').val();
            var sendInvoice = $('#sendInvoice').val();
            var dueInvoice = $('#dueInvoice').val();
            var namaKlien = $('#namaKlien').val();
            var namaUsaha = $('#namaPerusahaan').val();
            var addKlien = $('#alamatPerusahaan').val();
            var kotaKlien = $('#kotaPerusahaan').val();
            var emailKlien = $('#emailKlien').val();
            var noTelp = $('#noTelpKlien').val();
            var share = $('#share-team').val();
            if(share=='true'){
                var share = 1;
            }
            else{
                var share = 0;
            }

            var item = [];
            $('[name="item"]').each(function(){
                if($(this).val() != '')
                {
                    item.push($(this).val());
                }
            });
            var biaya=[];
            $('[name="biaya"]').each(function(){
                if($(this).val() != '')
                {
                    biaya.push($(this).val().trim().replace(/\./g,''));
                }
            });

            if(item.length > 0)
            {
                if(title==''){
                    swal('Oops','Judul invoice tidak boleh kosong','error');
                }
                else{
                    $.ajax({
                        type: "POST",
                        url: "<?=base_url()?>invoice/update",
                        data: {
                            'id_in' : idInvoice,
                            'no_in' : number,
                            'judul_in' : title,
                            'id_prop' : idProposal,
                            'id_klien' : idKlien,
                            'note_in' : note,
                            'tax_in' : tax,
                            'send_in' : sendInvoice,
                            'due_in' : dueInvoice,
                            'projek' : title,
                            'add_klien' : addKlien,
                            'kota_klien' : kotaKlien,
                            'biaya' : biaya,
                            'item' : item,
                            'namapic' : namaKlien,
                            'email' : emailKlien,
                            'telp' : noTelp,
                            'perusahaan' : namaUsaha,
                            'share' : share
                        },
                        success: function(data)
                        {
                            if(data == 1)
                            {
                                swal("Tersimpan","Invoice berhasil diedit","success");
                                location.reload();
                            }
                            else
                            {
                                swal("Oops","Ada kesalahan. Silahkan mencoba kembali.","error");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            swal("Oops","Invoice gagal diedit. Silahkan mencoba kembali.","error");
                        }
                    });
                }
            }
            else
            {
                swal("Oops","Pastikan Anda sudah memasukan item invoice.","error");
            }

        });

        $('#copy-share-link').on('click',function(){
            var copyText = document.getElementById("share-link");
            swal({
                title: 'Perhatian',
                text: 'Kami tidak merekomendasikan Anda mengirimkan invoice ini menggunakan cara selain menggunakan tombol KIRIM di kanan atas. Bila Anda tetap menginginkan, silahkan bagikan link URL ini.'
            })
            .then((willShare)=>{
                if (willShare){
                    copyText.select();
                    document.execCommand("Copy");
                    $.ajax({
                        method: "POST",
                        url: "<?=base_url()?>invoice/log_copy_url",
                        data: { invoice: invoice_id },
                        success: function(data){
                            swal('Link Copied','Link url invoice berhasil tersalin','success');
                        },error: function (jqXHR, textStatus, errorThrown){
                            swal('Coping Failed','Link url invoice gagal disalin','danger');
                        }
                    });
                }
            });
        });

        $(document).on('click','#btn-kirim-invoice', function(){
            var client_email = $('#send_invoice_client_email').val();
            var subject = $('#send_invoice_client_email_subject').val();
            var message = $('#send_invoice_client_email_message').val();

            if (client_email == '') {
                $('#email-client-msg').empty().append("<div class='alert alert-fit alert-danger text-little'>Email klien wajib diisi (minimal 1)</div>");
            }
            else if (subject == '') {
                $('#subject-msg').empty().append("<div class='alert alert-fit alert-warning text-little'>Kami menyarankan untuk mengisi subject email Anda</div>");
            }
            else if (message == '') {
                $('#message-msg').empty().append("<div class='alert alert-fit alert-danger text-little'>Pesan email wajib diisi</div>");
            }
            else {
                $('#btn-kirim-invoice').attr('disabled',true).html('Mengirim...');
                $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>invoice/sendInvoice",
                data: {
                    'sender_name' : $('#send_invoice_sender_name').val(),
                    'sender_email' : $('#send_invoice_sender_email').val(),
                    'client_email' : $('#send_invoice_client_email').val(),
                    'client_name' : $('#send_invoice_client_name').val(),
                    'client_cc' : $('#send_invoice_client_cc').val(),
                    'email_message' : $('#send_invoice_client_email_message').text(),
                    'email_subject' : $('#send_invoice_client_email_subject').val(),
                    'id' : $(this).attr('idi')
                    },
                success: function(data){
                    $('#btn-kirim-invoice').attr('disabled',false).html('<i class="fa fa-send" aria-hidden="true"></i> Terkirim');
                    $('#modal-kirim-invoice').modal('hide');
                    swal('Invoice Terkirim','Invoice berhasil dikirim ke email klien Anda','success');
                }, error:function(error){
                    swal('Ada kesalahan');
                }
                });
            }
        });

        $(function() {
            $('#toggle-event').change(function() {
                var share = $(this).prop('checked');
                $.ajax({
                    type: "POST",
                    url: "<?=base_url()?>invoice/updatePermissionDocument/"+invoice_id,
                    data: {share : share},
                    datatype: 'json',
                    success: function(response){
                        swal('Update Permission','Hak akses invoice berhasil diupdate','success');
                    }, error:function(error){
                        swal('Oops','Hak akses invoice gagal diupdate','error');
                    }
                });
            });
        });
        <?php } ?>
        //END OF SCRIPT DETAIL
    });
</script>
