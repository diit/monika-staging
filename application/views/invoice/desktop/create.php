<div id="content">
    <div class="row justify-content-center">
        <div class="col-sm-11 col-md-9 box" id="invoice-content">
            <div class="row">
                <div class="col-sm-6">
                    <h3>Invoice</h3>
                    &nbsp;
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-hashtag"></i></div>
                        </div>
                        <input type="text" class="form-control" placeholder="Nomor invoice" name="no_invoice" id="noInvoice">
                    </div>
                    <p class="info-form">*kosongkan jika tidak ada nomor invoice</p>
                    &nbsp;
                    <textarea class="form-control" rows="2" placeholder="Nama invoice/project" id="titleInvoice"></textarea>
                    <p class="info-form">*judul invoice wajib diisi</p>
                </div>
                <div class="col-sm-5 offset-sm-1">
                    <form class="form-inline">
                        <p style="margin:0 10px 0 0">Share invoice to team</p>&nbsp;
                        <input type="checkbox" data-toggle="toggle" data-on="share" data-off="private" id="toggle-event" data-onstyle="primary" data-size="small">
                    </form>
                    <input id="share-team" value="false" type="hidden">
                    <!-- photo -->
                </div>
            </div>
            &nbsp;
            <div class="line-separator"></div>
            &nbsp;
            <div class="row">
                <div class="col-sm-6">
                    <p>DARI</p>
                    <input type="text" class="form-control" placeholder="Pengirim" readonly value="<?= $this->session->userdata('user_name') ?>">
                    &nbsp;
                    <p>KEPADA</p>
                    <div id="client_select">
                        <select class="basic-single form-control" name="select_duplicate_client" id="select_duplicate_client" style="width:100%">
                                <option value="0" disabled selected>Pilih dari daftar client</option>
                                <option value="baru">Klien Baru</option>
                                <?php
                                    foreach ($user_client as $row) {
                                        $random = md5(mt_rand(1,10000));
                                        $first = substr($random,0,5);
                                        $last = substr($random,5,10);
                                        $id_klien = $first.$row->id_client.$last;
                                ?>
                                <option value="<?= $id_klien ?>"><?= $row->nama_pic ?> | <?= $row->perusahaan ?></option>
                                <?php } ?>
                        </select>
                    </div>
                    <p class="info-form">*edit klien di <a href="<?= site_url('client/klien') ?>">menu klien</a></p>
                    &nbsp;
                    <div id="form-client">
                        <input type="text" class="form-control" id="namaKlien" value="" placeholder="penerima invoice" readonly>
                        <input type="text" class="form-control" id="namaPerusahaan" value="" placeholder="nama perusahaan (optional)" readonly>
                        <input type="text" class="form-control" id="emailKlien" value="" placeholder="Email klien" readonly>
                        <textarea class="form-control" rows="2" placeholder="alamat usaha klien (optional)" id="alamatPerusahaan"></textarea>
                        <input type="text" class="form-control" value="" placeholder="kota (optional)" id="kotaPerusahaan" >
                        <input class="form-control" placeholder="No Hp klien" id="noTelpKlien" value="">
                        <input hidden="hidden" id="id_client" value="">
                    </div>
                </div>
                <div class="col-sm-4 offset-sm-2">
                    <div class="input-group" style="margin-top:25%">
<!--                        <input id="sendInvoice" class="form-control datepicker-sent" value="" readonly placeholder="tanggal kirim">-->
                    </div>
                    <label>BATAS AKHIR PEMBAYARAN</label>&nbsp;
                    <input id="dueInvoice" class="form-control datepicker-last" value="" readonly placeholder="tanggal deadline">
                </div>
            </div>
            &nbsp;
            <table id="item">
                <tr class="th-item">
                    <th class="text-center" style="width:75%">ITEM DESCRIPTION</th>
                    <th class="text-center" style="width:25%">BIAYA</th>
                    <th>&nbsp;</th>
                </tr>
                <?php $c_item = 3; ?>
                <tr id="item-0">
                    <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>
                    <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                </tr>
                <tr id="item-1">
                    <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>
                    <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                </tr>
                <tr id="item-2">
                    <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>
                    <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <button type="button" class="btn btn-sm btn-outline-grey" id="btnAddItem"><i class="fas fa-plus"></i> Tambah item</button>
                    </td>
                </tr>
                <tr style="border-bottom:1px solid #ccc">
                    <td colspan="3"></td>
                </tr>
                <tr style="font-size:1.2em; font-weight:bold">
                    <td class="text-right">SUB TOTAL</td>
                    <td class="text-right" id="getSubtotal">Rp </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <div class="input-group pull-right" style="width:150px">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Tax</div>
                            </div>
                            <input type="text" class="form-control text-right" placeholder="0" onblur="sum();" name="tax" id="tax" value="0">
                            <div class="input-group-prepend">
                                <div class="input-group-text">%</div>
                            </div>
                        </div>
                    </td>
                    <td class="text-right" id="getTaxPrice">Rp </td>
                    <td></td>
                </tr>
                <tr id="grand-total">
                    <td style="padding:10px;" class="text-right">TOTAL</td>
                    <td class="text-right" style="padding:10px; background-color:#eee" id="getTotPrice">Rp </td>
                    <td></td>
                </tr>
            </table>

            <div class="row">
                <div class="col-sm-8 col-md-7">
                    <p>CATATAN</p>
                    <textarea class="form-control" rows="3" id="catatan" placeholder="Tambahkan catatan untuk penerima jika ada" name="invoice_note"></textarea>
                </div>
            </div>
            <br><br>
            <button class="btn btn-primary pull-right" id="createInvoice">simpan</button>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        var c_item = <?= $c_item ?>;
        $('#btnAddItem').on('click',function(){
            var newitem = '';
            newitem += '<tr id="item-'+c_item+'">';
            newitem += '<td><textarea name="item" rows="1" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>';
            newitem += '<td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);" onblur="sum();" value=""></td>';
            newitem += '<td align="right"><button type="button" item-id="'+c_item+'" class="btn btn-default deleteItem"><i class="far fa-trash-alt"></i></button></td>';
            newitem += '</tr>';
            $(newitem).insertAfter($('#item-'+ parseInt(c_item - 1)))
            c_item++;
        });

        $(document).on('click','.deleteItem',function(){
            if(c_item > 1)
            {
                $('#item-'+ $(this).attr('item-id')).remove();
            }
        });
    });

    function sum()
    {
        var tax = parseInt($('#tax').val());
        var newsubTot = 0;
        $(".biaya").each(function() {
            var str = this.value.trim().replace(/\./g,'');  // .trim() may need a shim
            if (str) {   // don't send blank values to `parseInt`
                newsubTot += parseInt(str, 10);
            }
        });

        var taxPrice = parseInt((tax/100) * newsubTot);
        var totPrice = parseInt(newsubTot + taxPrice);

        $('#getSubtotal').html('Rp '+parseFloat(newsubTot).toLocaleString('id'));
        $('#getTaxPrice').html('Rp '+parseFloat(taxPrice).toLocaleString('id'));
        $('#getTotPrice').html('Rp '+parseFloat(totPrice).toLocaleString('id'));
    }
</script>
