<div id="content">
    <div class="row justify-content-center">
        <!-- invoice menu -->
        <div class="col-sm-12">
            <div class="box toolbar-box">
                <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-kirim-invoice"><i class="far fa-envelope"></i> kirim</button>

                <button class="btn btn-outline-secondary pull-right" <?= (($this->monikalib->currentUserPlan()==0) || ($this->monikalib->getUserPlan()==1)) ? 'data-toggle="tooltip" data-placement="bottom" title="Upgrade plan untuk download proposal"' : 'id="download-invoice"' ?>><i class="far fa-arrow-alt-circle-down"></i> download</button>

                <button class="btn btn-outline-secondary pull-right" id="copy-share-link"><i class="far fa-clone"></i> copy url share</button>
                <input class="form-control no-border pull-right text-little" value="<?= $link ?>" style="width:250px" id="share-link">
                <h3 class="pull-left">PREVIEW</h3>
            </div>
        </div>
        <!-- end of invoice menu -->

        <div class="col-sm-11 col-md-9 box top-space" id="invoice-content">
            <div class="row">
                <div class="col-sm-6">
                    <h3>Invoice</h3>
                    &nbsp;
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-hashtag"></i></div>
                        </div>
                        <input type="text" class="form-control" placeholder="Nomor invoice" name="no_invoice" id="noInvoice" value="<?= $invoice->number ?>">
                    </div>
                    <p class="info-form">*kosongkan jika tidak ada nomor invoice</p>
                    &nbsp;
                    <textarea class="form-control" rows="2" placeholder="Nama invoice/project" id="titleInvoice"><?= $invoice->judul ?></textarea>
                    <p class="info-form">*judul invoice wajib diisi</p>
                </div>
                <div class="col-sm-5 offset-sm-1">
                    <?php if ($invoice->id_user == $this->session->userdata('user_id')) { ?>
                    <form class="form-inline">
                        <p style="margin:0 10px 0 0">Share invoice to team</p>&nbsp;
                        <input type="checkbox" data-toggle="toggle" data-on="share" data-off="private" id="toggle-event" data-onstyle="primary" data-size="small" <?= ($invoice->share_team == 1) ? 'checked' : '' ?>>
                    </form>
                    <input id="share-team" value="<?= ($invoice->share_team == 1) ? 'true' : 'false' ?>" type="hidden">
                    <?php } ?>
                    <!-- photo -->
                </div>
            </div>
            &nbsp;
            <div class="line-separator"></div>
            &nbsp;
            <div class="row">
                <div class="col-sm-6">
                    <p>DARI</p>
                    <input type="text" class="form-control" placeholder="Pengirim" readonly value="<?= $invoice->user_name ?>">
                    &nbsp;
                    <p>KEPADA</p>
                    <div id="client_select">
                        <select class="basic-single form-control" name="select_duplicate_client" id="select_duplicate_client">
                            <option value="0" disabled selected>Pilih dari daftar client</option>
                            <option value="baru">Klien Baru</option>
                            <?php
                                foreach ($user_client as $row) {
                                    $random = md5(mt_rand(1,10000));
                                    $first = substr($random,0,5);
                                    $last = substr($random,5,10);
                                    $id_klien = $first.$row->id_client.$last;
                            ?>
                            <option value="<?= $id_klien ?>"><?= $row->nama_pic ?> | <?= $row->perusahaan ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <p class="info-form">*edit klien di <a href="<?= site_url('client/klien') ?>">menu klien</a></p>
                    &nbsp;
                    <div id="form-client">
                        <input type="text" class="form-control" id="namaKlien" value="<?= $invoice->nama_pic ?>" placeholder="penerima invoice" readonly>
                        <input type="text" class="form-control" id="namaPerusahaan" value="<?= $invoice->perusahaan ?>" placeholder="nama perusahaan (optional)" readonly>
                        <input type="text" class="form-control" id="emailKlien" value="<?= $invoice->email ?>" placeholder="Email klien" readonly>
                        <textarea class="form-control" rows="2" placeholder="alamat usaha klien (optional)" id="alamatPerusahaan"><?= $invoice->alamat_usaha ?></textarea>
                        <input type="text" class="form-control" value="<?= $invoice->kota ?>" placeholder="kota (optional)" id="kotaPerusahaan" >
                        <input class="form-control" placeholder="No Hp klien" id="noTelpKlien" value="<?= $invoice->telephone ?>">
                        <?php
                            $random = md5(mt_rand(1,10000));
                            $first = substr($random,0,5);
                            $last = substr($random,5,10);
                            $id_klien = $first.$invoice->id_client.$last;
                        ?>
                        <input hidden="hidden" id="id_client" value="<?= $id_klien ?>">
                    </div>
                </div>

                <div class="col-sm-4 offset-sm-2">
                    <div class="input-group" style="margin-top:25%">
                        <!-- send_invoice -->
                    </div>
                    <label>BATAS AKHIR PEMBAYARAN</label>&nbsp;
                    <input id="dueInvoice" class="form-control datepicker-last" value="<?= (empty($invoice->due_invoice)) || ($invoice->due_invoice == '0000-00-00 00:00:00')? '' : date("d-M-Y", strtotime($invoice->due_invoice)); ?>" readonly placeholder="tanggal deadline">
                </div>
            </div>
            &nbsp;
            <table id="item">
                <tr class="th-item">
                    <th class="text-center" style="width:75%">ITEM DESCRIPTION</th>
                    <th class="text-center" style="width:25%">BIAYA</th>
                    <th>&nbsp;</th>
                </tr>
                <?php
                    $sub_tot = 0;
                    if(count($item) > 0){
                        $c_item = count($item);
                        foreach($item as $id=>$d) {
                            if($d->item != NULL && $d->harga > 0){
                ?>
                <tr id="item-<?= $id ?>">
                    <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"><?= $d->item ?></textarea></td>
                    <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="<?= $d->harga ?>" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                    <td align="right">
                        <button type="button" item-id="<?= $id ?>" class="btn btn-default deleteItem" data-toggle="tooltip" data-placement="right" title="Delete item"><i class="far fa-trash-alt"></i></button>
                    </td>
                </tr>
                <?php
                                $sub_tot = $sub_tot+($d->harga);
                            }
                        }
                    }else{
                        $c_item = 3;
                ?>
                <tr id="item-0">
                    <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>
                    <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                    <td align="right">
                        <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="right" title="Delete item"><i class="far fa-trash-alt"></i></button>
                    </td>
                </tr>
                <tr id="item-1">
                    <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>
                    <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                    <td align="right">
                        <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="right" title="Delete item"><i class="far fa-trash-alt"></i></button>
                    </td>
                </tr>
                <tr id="item-2">
                    <td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>
                    <td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onblur="sum();" value="" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>
                    <td align="right">
                        <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="right" title="Delete item"><i class="far fa-trash-alt"></i></button>
                    </td>
                </tr>
                <?php
                        $sub_tot = 0;
                    }
                ?>
                <tr>
                    <td colspan="3">
                        <button type="button" class="btn btn-sm btn-default" id="btnAddItem"><i class="fas fa-plus"></i> Tambah item</button>
                    </td>
                </tr>
                <tr style="border-bottom:1px solid #ccc">
                    <td colspan="3"></td>
                </tr>
                <input hidden="hidden" type="text" value="<?= $sub_tot ?>" id="subTotal">
                <tr style="font-size:1.2em; font-weight:bold">
                    <td class="text-right">SUB TOTAL</td>
                    <td class="text-right" id="getSubtotal">Rp <?= number_format($sub_tot,0,",",".") ?></td>
                    <td></td>
                </tr>
                <tr style="font-size:1.2em; font-weight:bold">
                    <?php $tax = (($invoice->tax)/100)*$sub_tot;  ?>
                    <td>
                        <div class="input-group pull-right" style="width:150px">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Tax</div>
                            </div>
                            <input type="text" class="form-control text-right" placeholder="0" onblur="sum();" name="tax" id="tax" value="<?= number_format($invoice->tax,0,",",".") ?>">
                            <div class="input-group-prepend">
                                <div class="input-group-text">%</div>
                            </div>
                        </div>
                    </td>
                    <td class="text-right" id="getTaxPrice"><h5>Rp <?= number_format($tax,0,",",".") ?></h5></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr id="grand-total">
                    <?php $tot = $sub_tot + $tax; ?>
                    <td style="padding:10px;" class="text-right">TOTAL</td>
                    <td class="text-right" style="padding:10px; background-color:#eee" id="getTotPrice">Rp <?= number_format($tot,0,",",".") ?></td>
                    <td></td>
                </tr>
            </table>

            <div class="row">
                <div class="col-sm-8 col-md-7">
                    <p>CATATAN</p>
                    <textarea class="form-control" rows="3" id="catatan" placeholder="Tambahkan catatan untuk penerima jika ada" name="invoice_note"><?php echo preg_replace("/[^A-Za-z0-9 ']/", "",preg_replace("/\r|\n/","",$invoice->note)); ?></textarea>
                </div>
            </div>
            <br><br>
            <?php
                $random = md5(mt_rand(1,10000));
                $first = substr($random,0,5);
                $last = substr($random,5,10);

                $id_in = $first.$invoice->id.$last;
                $id_prop = $first.$invoice->id_proposal.$last;
                $id_client = $first.$invoice->id_client.$last;
            ?>
            <button class="btn btn-primary pull-right" id="saveInvoice" iid="<?= $id_in ?>" pid="<?= $id_prop ?>" kid="<?= $id_client ?>">simpan</button>
        </div>
    </div>
</div>

<!-- modal kirim  -->
<div class="modal fade modal-kirim" id="modal-kirim-invoice" tabindex="-1" role="dialog" aria-labelledby="modalKirimProposal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header text-center">
            <h5 class="modal-title">Kirim Invoice</h5>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label for="">Nama Pengirim</label>
                    <input class="form-control" type="text" placeholder="nama pengirim" value="<?= $invoice->user_name?>" id="send_invoice_sender_name" readonly>
                </div>
                <div class="col-md-6">
                    <label for="">Email Pengirim</label>
                    <input class="form-control" type="email" placeholder="email pengirim" value="<?= $invoice->user_email?>" id="send_invoice_sender_email" readonly>
                </div>
            </div> <br>
            <div class="row">
                <div class="col-md-12">
                    <input hidden value="<?=$invoice->nama_pic?>" id="send_invoice_client_name">
                    <div class="form-group">
                        <label for="">Email Klien</label>
                        <input class="form-control" type="text" data-role="tagsinput" value="<?= $invoice->email?>" id="send_invoice_client_email">
                        <div id="email-client-msg"></div>
                    </div>
                    <div class="form-group">
                        <label for="">CC</label>
                        <input class="form-control" type="email" data-role="tagsinput" placeholder="email klien" id="send_invoice_client_cc">
                    </div>
                    <div class="form-group">
                        <label for="">Subjek</label>
                        <input class="form-control" type="text" placeholder="Judul Proposal" value="<?= $invoice->judul ?>" id="send_invoice_client_email_subject">
                        <div id="subject-msg"></div>
                    </div>
                    <div class="form-group">
                        <label for="">Pesan</label>
                        <textarea class="form-control" name="pesan-proposal" cols="30" rows="10" id="send_invoice_client_email_message">Dengan Hormat,
Berikut Proposal [...] yang bisa Anda baca.
Klik di sini > <?= $link ?>
Bila ada pertanyaan silahkan langsung tanyakan ke saya dengan balas email ini.

Terima kasih,
<?= $invoice->user_name?>
                        </textarea>
                        <div id="message-msg"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">batal</button>
            <button class="btn btn-primary" type="button" id="btn-kirim-invoice" idi="<?= $id_in ?>">kirim</button>
        </div>
    </div>
  </div>
</div>
<!-- end of modal kirim  -->

<script>
    $(document).ready(function(){
        var c_item = <?= $c_item ?>;
        $('#btnAddItem').on('click',function(){
            var newitem = '';
            newitem += '<tr id="item-'+c_item+'">';
            newitem += '<td><textarea rows="1" name="item" placeholder="Nama item dan deskripsi" class="form-control"></textarea></td>';
            newitem += '<td><input type="text" name="biaya" placeholder="0" class="form-control text-right biaya sepNumber" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);" onblur="sum();" value=""></td>';
            newitem += '<td align="right"><button type="button" item-id="'+c_item+'" class="btn btn-default deleteItem" data-toggle="tooltip" data-placement="right" title="Arsipkan"><i class="far fa-trash-alt"></i></button></td>';
            newitem += '</tr>';
            $(newitem).insertAfter($('#item-'+ parseInt(c_item - 1)))
            c_item++;
        });

        $(document).on('click','.deleteItem',function(){
            if(c_item > 1)
            {
                $('#item-'+ $(this).attr('item-id')).remove();
            }
        });
    });

    function sum()
    {
        var tax = parseInt($('#tax').val());
        var newsubTot = 0;
        $(".biaya").each(function() {
            var str = this.value.trim().replace(/\./g,'');  // .trim() may need a shim
            if (str) {   // don't send blank values to `parseInt`
                newsubTot += parseInt(str, 10);
            }
        });

        var taxPrice = parseInt((tax/100) * newsubTot);
        var totPrice = parseInt(newsubTot + taxPrice);

        $('#getSubtotal').html('Rp '+parseFloat(newsubTot).toLocaleString('id'));
        $('#getTaxPrice').html('Rp '+parseFloat(taxPrice).toLocaleString('id'));
        $('#getTotPrice').html('Rp '+parseFloat(totPrice).toLocaleString('id'));
    }
</script>
