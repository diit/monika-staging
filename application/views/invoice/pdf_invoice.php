<?php
ob_start();
$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('Invoice - '. $invoice->judul);
// $pdf->SetHeaderMargin(0);
$pdf->SetTopMargin(18);
$pdf->SetLeftMargin(16);
$pdf->SetRightMargin(16);
// $pdf->setFooterMargin(10);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('Monika');
$pdf->SetDisplayMode('real', 'default');
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$url = base_url();
$pdf->AddPage();
// $pdf->SetFont("avenirltstd", '', 12);
$tgl = $this->monikalib->format_date_indonesia($invoice->due_invoice);
$pdf->Ln(10);
$pdf->SetFont("avenirltstd", '', 20);
$pdf->Cell(115,0,'Invoice',0,1,'L');
$pdf->SetFont("avenirltstd", '', 12);
$pdf->Cell(40,0,'#'.$invoice->number,0,0,'L');

if (!empty($get_style->logoimageurl)){
    // $html1 = '<img src="'.$get_style->logoimageurl.'" alt="" width="150" border="0">';
    $pdf->Image($get_style->logoimageurl, 150, 30, 35,'', 'png','', '', true, 300, '', false, false, 0, false, false, false);
}
// $a = $pdf->getX();
// $b = $pdf->getY();
// $pdf->writeHTMLCell(150,30, 145,'', $html1, 0,1,0,'C',true);

//pengirim
$pdf->Ln(20);
$pdf->Cell(115,10,'DARI',0,1,'L');
$pdf->Cell(60,0,$invoice->user_name,0,1,'L');

// Penerima
$pdf->Ln(10);
$pdf->Cell(115,10,'Kepada',0,0,'L');
$pdf->Cell(60,10,'Batas Akhir Pembayaran',0,1,'R');
$pdf->Cell(115,8,$invoice->nama_pic,0,0,'L');
$pdf->Cell(60,8,$tgl,0,1,'R');

if(!empty($invoice->perusahaan)){
    $pdf->Cell(100,8,$invoice->perusahaan,0,1,'L');
}
if(!empty($invoice->alamat_usaha)){
    $pdf->Cell(100,8,$invoice->alamat_usaha,0,1,'L');
}
if(!empty($invoice->perusahaan)){
    $pdf->Cell(100,8,$invoice->kota,0,1,'L');
}

//Table of items
$pdf->Ln(15);
$pdf->SetFont("avenirltstd", '', 12);
$pdf->setCellPaddings(0, 4, 0, 4);
$pdf->Cell(115,10,'ITEM DESKRIPSI','TB',0,'C');
$pdf->Cell(60,0,'HARGA','TB',0,'C');
//Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=0, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M')
// Cell()
$pdf->SetFont("avenirltstd", '', 11);
$pdf->setCellPaddings(5, 4, 0, 4);
$pdf->Ln(5);
$sub_tot = 0;
foreach($item as $d){
    $pdf->Ln(10);
    $pdf->Cell(115,10,$d->item,0,0,'L');
    $pdf->Cell(50,0,'Rp '.number_format($d->harga,"0",",","."),0,0,'R');
    $sub_tot = $sub_tot+($d->harga);
}

$pdf->SetFont("avenirltstd", '', 12);
// Subtotal
$pdf->Ln(15);
$pdf->Cell(175,15,' ','T',0,'R');
$pdf->Ln(0);
$pdf->Cell(115,10,'SUB TOTAL',0,0,'R');
$pdf->Cell(50,0,'Rp '.number_format($sub_tot,"0",",","."),0,0,'R');

// Pajak
$pdf->Ln(10);
$tax_price = ($invoice->tax/100)*$sub_tot;
$pdf->Cell(115,10,'TAX '.number_format($invoice->tax,"0",",",".").'%',0,0,'R');
$pdf->Cell(50,0,'Rp '.number_format($tax_price,"0",",","."),0,0,'R');

// Total harga
$tot = $sub_tot + $tax_price;
$pdf->Ln(10);
$pdf->Cell(115,10,'TOTAL',0,0,'R');
$pdf->Cell(50,0,'Rp '.number_format($tot,"0",",","."),0,1,'R');

if(!empty($invoice->note)){
    $pdf->Ln(15);
    $pdf->Cell(25,5,'CATATAN',0,0,'L');
    $pdf->Ln(8);
    $y = $pdf->getY();
    $x = $pdf->getX();
    $pdf->MultiCell(175, 5, $invoice->note, 0, 'L', 0, 0, $x,$y);
    // $pdf->Cell(100,10,$invoice->note,0,0,'L');
}

$pdf->lastPage();

$pdf->Output('Invoice - '.$invoice->judul.'.pdf', 'I');
