<!DOCTYPE html>
<html lang="id">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Software pendukung dan pengelolaan penjualan untuk freelancer dan agency dalam mengirim proposal hingga mendapatkan pembayaran tepat waktu.">
    <meta name="author" content="Monika">
    <meta name="keyword" content="Proposal, Invoice, Penawaran, Escrow, Lacak Proposal, Tracking, Closing, Proposal Goal">

    <title>Monika - Register</title>
    <link rel="shortcut icon" href="assets/img/favicon.ico">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-social.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/monika.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">
    <!-- Validasi form -->
    <style>
        .error {
            border:2px solid red;
        }
        .btn-social > :first-child{
              font-size: 1.2em;
          }
    </style>
    <!-- Hotjar Tracking Code for https://app.monika.id -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:751855,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115539143-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-115539143-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1971319419754936');
      fbq('track', 'PageView');
    </script>

    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=1971319419754936&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
  </head>

  <body>
	  <div id="login-page" class="login-page">
	  	<div class="container" style="margin-top:5%">
	  		<div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-6 hidden-xs" style="padding-top:100px">
	  			<div style="color:#fff;">
	  				<img width="150" src="<?php echo base_url(); ?>assets/img/monika_logo.png" alt="monika-logo">
	  				<h3 style="margin-top:20px;">Menangkan klien dalam waktu singkat</h3>
	  				<p style="font-size:16px;line-height:40px;"><i class="fa fa-check" aria-hidden="true"></i> <span style="color:#46bfb2;">Mudah</span> membuatnya</p>
            <p style="font-size:16px;line-height:40px;"><i class="fa fa-check" aria-hidden="true"></i> <span style="color:#46bfb2;">Banyak template</span> proposal sesuai bidang</p>
            <p style="font-size:16px;line-height:40px;"><i class="fa fa-check" aria-hidden="true"></i> <span style="color:#46bfb2;">Aman</span>, data hingga pembayaran lebih aman</p>
            <p style="font-size:16px;line-height:40px;"><i class="fa fa-check" aria-hidden="true"></i> <span style="color:#46bfb2;font-style: italic;">On-time</span>, terima pembayaran tepat waktu</p>
	  			</div>
	  		</div>
	  		<div class="col-sm-6 col-xs-12">
		  		<?php
	                  $success_msg = $this->session->flashdata('success_msg');
	                  $error_msg = $this->session->flashdata('error_msg');
	                ?>
			      <form name="registerForm" id="registerForm" class="form-login" method="post" action="<?php echo isset($inv)? base_url('user/register_user/?inv='.$inv) :base_url('user/register_user'); ?>">
			        <h2 class="form-login-heading">FREE TRIAL</h2>
			        <div class="login-wrap">
			        	<input type="text" id="username" class="form-control" name="user_name" placeholder="Nama Lengkap" title="Harap isi nama dengan benar" autocomplete="off" autofocus required>
			            <br>
			            <input type="text" id="nama-usaha" class="form-control" name="user_corp" placeholder="Nama Usaha" autocomplete="off">
                  <p style="text-align:right; color: #fff;font-size: 10px;">Kosongkan bila perorangan</p>
			            <input type="email" id="mail" class="form-control" name="user_email" placeholder="Email" autocomplete="off" required>
                  <br/>
                  <div class="alert alert-danger" style="display: none;" id="email-alert"></div>
			            <input type="text" id="mobile" class="form-control" name="user_mobile" placeholder="Nomor Handphone" title="Minimal 10 karakter. Hanya angka."
                  pattern="^[0-9]{10,13}$" autocomplete="off" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);" required >
                  <br/>
                  <div class="alert alert-danger" style="display: none;" id="phone-alert"></div>
			            <input type="password" id="password" class="form-control" name="user_password" placeholder="Password" title="Wajib diisi" autocomplete="off" required>
			            <br>
                  <?php
                  if($this->session->flashdata('status_regis')==1)
                  {
                    echo "<div class='alert alert-danger'><p>Ada kesalahan, silahkan <span id='clickchat' style='font-weight:800;text-decoration:underline;'>klik di sini</span> untuk berbicara dengan Customer Service kami.</p></div>";
                  }
                  else if($this->session->flashdata('status_regis')==2)
                  {
                    echo "<div class='alert alert-danger'><p>Email sudah digunakan, silahkan gunakan email lain.</p></div>";
                  }
                  ?>

                    <?php if($success_msg){ ?>
                    <div class="alert alert-success">
                          <?php echo $success_msg; ?>
                        </div>
                    <?php }if($error_msg){ ?>
                        <div class="alert alert-danger">
                          <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>

                    <button class="btn btn-theme btn-block" id="btnsubmit" type="button"><i class="fa fa-lock"></i> Daftar</button>

<!--
                    <h4 class="text-center">atau</h4>
                    <a class="btn btn-block btn-social btn-google" href="<?php //echo $loginURL; ?>">
                        <span class="fa fa-google"></span> Register with Google
                    </a>
-->

                    <p style="color:#4f4f4f;font-size:11px; margin-top:20px">Dengan klik <strong>Register</strong>, Anda telah menyetujui <a href="<?php echo site_url('syarat-ketentuan') ?>" target="_blank">Syarat dan Ketentuan</a> serta <a href="<?php echo site_url('kebijakan-privasi') ?>" target="_blank">Kebijakan Privasi</a> Monika.id</p>

                    <hr>

                    <div class="registration">
                        Sudah memiliki akun?<br/>
                        <a class="" href="<?php echo site_url('login'); ?>">
                            Login di sini
                        </a>
                    </div>

			        </div>
			      </form>
			</div>
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("<?php echo base_url(); ?>assets/img/login-bg_after.jpg", {speed: 500});
    </script>
    <script src="<?php echo base_url();?>node_modules/sweetalert/dist/sweetalert.min.js"> </script>

    <script>
      function ValidateEmail(email) {
          //var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
          var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return expr.test(email);
      };

      $('.form-control').on('keypress',function (e) {
        if (e.which == 13) {
          $('#btnsubmit').trigger('click');
        }
      });

      $(document).ready(function(){
        $('#clickchat').on('click',function(){
          //$('#harga').val($(this).attr('data-pricing'));
          Tawk_API.toggle();
          //$('html,body').animate({scrollTop: $('.register').offset().top - 50 },'slow');
        });

        //$('#phone-alert').hide();
        $('#mail').change(function(){
          if($('#mail').val()==''){
            $('#email-alert').hide();
            $('#btnsubmit').prop('disabled',false);
          }else{
            x = $('#mail').val();
            $.ajax({
              type:"POST",
              url: "<?php echo base_url()?>user/checkEmail",
              data: {email:x},
              dataType: 'json',
              success: function(data){
                if(data.status == "false"){
                  $('#email-alert').html('Email sudah terdaftar');
                  $('#email-alert').attr('class','alert alert-danger');
                  $('#email-alert').show();
                  $('#mail').focus();
                  $('#btnsubmit').prop('disabled',true);
                }else if(data.status=="true"){
                  $('#email-alert').html('Email valid');
                  $('#email-alert').attr('class','alert alert-success');
                  $('#email-alert').hide();
                  $('#btnsubmit').prop('disabled',false);
                }
              }
            });
          }
        });

        $('#mobile').change(function(){
          if($('#mobile').val()==''){
            $('#phone-alert').hide();
            $('#btnsubmit').prop('disabled',false);
          }else{
            y = $('#mobile').val();
            $.ajax({
              type:"POST",
              url: "<?php echo base_url()?>user/checkPhone",
              data: {phone:y},
              dataType: 'json',
              success: function(data){
                if(data.status == "false"){
                  $('#phone-alert').html('Nomor Handphone sudah terdaftar');
                  $('#phone-alert').attr('class','alert alert-danger');
                  $('#phone-alert').show();
                  $('#mobile').focus();
                  $('#btnsubmit').prop('disabled',true);
                }else if(data.status=="true"){
                  $('#phone-alert').html('Phone valid');
                  $('#phone-alert').attr('class','alert alert-success');
                  $('#phone-alert').hide();
                  $('#btnsubmit').prop('disabled',false);
                }
              }
            });
          }
        });

        var format = /[$%^&*()+\=\[\]{};'"\\|<>\/?]/;

        $('#btnsubmit').on('click',function(){
          if($('#username').val() == '')
          {
            swal("Oops","Isi nama Anda","error");
          }
          /*
          else if($('#nama-usaha').val() == '')
          {
            swal("Oops","Isi nama usaha atau tempat Anda bekerja. Bila self-employed, isikan nama Anda","error");
          }
          */
          else if($('#mail').val() == '')
          {
            swal("Oops","Email wajib diisi","error");
          }
          else if(!ValidateEmail($('#mail').val()))
          {
            swal("Oops","Pastikan format email benar","error");
          }
          else if($('#password').val() == '')
          {
            swal("Oops","Password wajib diisi","error");
          }
          else
          {
            if(format.test($('#username').val()))
            {
              swal("Oops","Mohon tidak menginput special character!","error");
            }
            else if(format.test($('#mail').val()))
            {
              swal("Oops","Mohon tidak menginput special character!","error");
            }
            /*
            else if(format.test($('#nama-usaha').val()))
            {
              swal("Oops","Mohon tidak menginput special character!","error");
            }
            */
            else if(format.test($('#password').val()))
            {
              swal("Oops","Mohon tidak menginput special character!","error");
            }
            else
            {
              $('#btnsubmit').prop('disabled',true).html('Memproses...');
              fbq('track', 'CompleteRegistration');
              $('#registerForm').submit();
            }
          }
        });

      });

    </script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5a619ebb4b401e45400c3764/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
  </body>
</html>
