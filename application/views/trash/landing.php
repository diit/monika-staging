<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Monika - Buat Proposal dalam Hitungan Menit</title>

    <link href="<?php echo base_url(); ?>assets/bootstrap4/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/monika.css" rel="stylesheet">

    <script src="<?php echo base_url()?>assets/bootstrap4/js/jquery-3.2.1.slim.min.js"></script>
    <script src="<?php echo base_url()?>assets/bootstrap4/js/popper.min.js"></script>
    <script src="<?php echo base_url()?>assets/bootstrap4/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="landingPage">
    <div class="container-fluid" id="navLP">
      <nav class="navbar navbar-expand-lg navbar-light">
          <a class="navbar-brand" href="#">
            <img src="<?php echo base_url('')?>/assets/img/monika_logo_white.png" width="160" height="50" alt="" id="logoMonika">
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuMonika" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse justify-content-end" id="menuMonika">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="#">Fitur</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#sec5LP">Harga</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">Panduan</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">FAQ</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">Blog</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">Masuk</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">Daftar</a>
              </li>
            </ul>
          </div>
      </nav>
    </div>
    <section>
      <div class="row" id="sec1LP">
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
          <h1>Buat Proposal dan Invoice dalam Hitungan Menit</h1>
          <div class="btnGroup">
            <a href="#" class="btn" id="coba">Coba Sekarang</a>
            <a href="#" class="btn" id="pelajari">Pelajari</a>
          </div>
        </div>
      </div>
    </section>
    <section id="sec2LP">
          <div class="row" id="sec1_5">
            <div class="col-12  text-center">
              <h1>Kenapa Monika?</h1>
            </div>
          </div>
          <div class="row">
              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 whyUs">
                <img src="<?php echo base_url('')?>assets/img/cepat.png" alt="">
                <!-- <div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div> -->
                <h3>Cepat</h3>
                <p>Buat penawaran dengan berbagai
                  template desain yang sangat profesional dan lakukan kustomisasi.
                  Kirimkan langsung kepada klien
                  tanpa harus diprint atau lampirkan
                  ke email. Anda akan mendapatkan
                  respon cepat dari klien! </p>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 whyUs">
                  <img src="<?php echo base_url('')?>assets/img/tracking.png" alt="">
                  <!-- <div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div> -->
                  <h3>Lacak Progress</h3>
                  <p>Selalu ingat perkembangan terakhir,
                    follow-up ke klien dari semua project dalam satu tempat. Sistem akan
                    rutin mengirimkan notifikasi pengingat kepada Anda. </p>
                  </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 whyUs">
                  <img src="<?php echo base_url('')?>assets/img/disukai.png" alt="">
                  <!-- <div>Icons made by <a href="https://www.flaticon.com/authors/becris" title="Becris">Becris</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div> -->
                  <h3>Disukai Client</h3>
                  <p>Desain, isi dan kemudahan meng akses menjadi penentu tersendiri bagi klien untuk merespon penawaran hingga tagihan dari Anda.
                    Menangkan klien dengan segera! </p>
                </div>
          </div>
          <div class="row" id="tutorialLP">
            <div class="col-12 text-center">
              <a href="" class="btn">Tutorial</a>
            </div>
          </div>
    </section>
    <section id="sec3LP">
      <div class="row">
        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
          <img src="<?php echo base_url('')?>assets/img/laptop.png" alt="">
        </div>
        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
          <h3>Sangat mudah membuat banyak proposal dan melacaknya</h3>
          <p>Monika memudahkan freelancer, profesional dan
            agency dalam membuat, mengingatkan dan
            mengirim proposal penawaran hingga invoice
            kepada klien dengan berbagai pilihan desain
            template yang menarik perhatian. </p>
          <a href="" class="btn">Preview Proposal</a>
        </div>
      </div>
    </section>
    <section id="sec4LP">
      <div class="row">
        <div class="col-12 text-center">
          <h3>Onboard new hires with confidence and clarity</h3>
          <p>Create a fantastic onboarding experience for every new hire with a personalized on-
            boarding page that details the tasks, tips, contact info, and files which every new hire
            will need before starting work at your company. Learn more about Kin's onboarding feature.</p>
          <div class="preProp">
              <div class="content-prop">
                <!-- <img src="http://res.cloudinary.com/monika-id/image/upload/c_scale,w_1125/v1514348384/pv_proposal_w1zeml.jpg" alt="" id="gambarProp"> -->
                <img src="<?php echo base_url('')?>assets/img/pvproposal_re3.jpg" alt="">
              </div>
          </div>
          <a class="btn" href="#">Coba Sekarang</a>
        </div>
      </div>
    </section>
    <section id="sec5LP">
      <div class="row">
        <div class="col-12 text-center">
            <h3>Mana benefit yang sesuai kebutuhan anda?</h3>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 pricingLP">
            <div class="atas-pricing" >
              <div class="headerPricing">
                <h2>Personal</h2>
                <p><span class="pricing1">Rp</span><span class="pricing2">100</span><span class="pricing3">rb</span></p>
                <p>per bulan</p>
              </div>
              <table class="table table-striped">
                  <tbody>
                    <tr>
                      <td>1 User</td>
                    </tr>
                    <tr>
                      <td>8 Dokumen</td>
                    </tr>
                    <tr>
                      <td>PDF Download</td>
                    </tr>
                    <tr>
                      <td>-</td>
                    </tr>
                    <tr>
                      <td>-</td>
                    </tr>
                    <tr>
                      <td>-</td>
                    </tr>
                    <tr>
                      <td><a href="#">Pilih Ini</a></td>
                    </tr>
                  </tbody>
              </table>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 pricingLP">
            <div class="atas-pricing" >
              <div class="headerPricing">
                <h2>Agency</h2>
                <p><span class="pricing1">Rp</span><span class="pricing2">250</span><span class="pricing3">rb</span></p>
                <p>per bulan</p>
              </div>
              <table class="table table-striped">
                  <tbody>
                    <tr>
                      <td>Hingga 5 User</td>
                    </tr>
                    <tr>
                      <td>Tanpa Batas Dokumen</td>
                    </tr>
                    <tr>
                      <td>PDF Download</td>
                    </tr>
                    <tr>
                      <td style="color:var(--main-color)">In Proposal Chat*</td>
                    </tr>
                    <tr>
                      <td>Pilihan Template</td>
                    </tr>
                    <tr>
                      <td>Managerial Account</td>
                    </tr>
                    <tr>
                      <td><a href="#">Pilih Ini</a></td>
                    </tr>
                  </tbody>
              </table>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 pricingLP">
            <div class="atas-pricing" >
              <div class="headerPricing">
                <h2>Enterprise</h2>
                <p style="margin-bottom: 25px;margin-top: 32px;"><span style="font-size:35pt;">Price by Call</span></p>
                <p>per bulan</p>
              </div>
              <table class="table table-striped">
                  <tbody>
                    <tr style="font-size: 16pt;">
                      <td colspan="7" style="padding-top: 33%;padding-bottom: 33%;">Untuk Perusahaan yang membutuhkan fitur dan Template diluar Fitur Reguler</td>
                    </tr>
                    <tr>
                      <td><a href="#">Pilih Ini</a></td>
                    </tr>
                  </tbody>
              </table>
            </div>
        </div>
        <p id="inchatLP">*) InProposal Chat adalah fitur dimana pelanggan dapat berbicara langsung pada penerima dokumen saat penerima membaca proposal penawaran</p>
      </div>
    </section>
    <section id="sec6LP">
      <div class="row" id="headertesti">
        <div class="col-12 text-center">
            <h2>Apa yang dikatakan pengguna Kami</h2>
        </div>
      </div>
      <div id="slider-testimoni" class="carousel slide row justify-content-center" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item row active">
                <div class="row justify-content-center">
                    <div class="col-5">
                      <div class="testi">
                        <div class="header-testi">
                          <h4>Nama User</h4>
                          <label for="">Jabatan User</label>
                        </div>
                        <img src="<?php echo base_url('')?>assets/img/testi/user1.jpg" alt="">
                        <p>So much of what we already do relies on individual team members being self-starting and self-reliant, that Kin was a natural fit for us. It not only reinforces that ethos, but makes it easy for individual team members to be self-sufficient</p>
                      </div>
                    </div>
                    <div class="col-5">
                      <div class="testi">
                        <div class="header-testi">
                          <h4>Nama User</h4>
                          <label for="">Jabatan User</label>
                        </div>
                        <img src="<?php echo base_url('')?>assets/img/testi/user2.jpg" alt="">
                        <p>So much of what we already do relies on individual team members being self-starting and self-reliant, that Kin was a natural fit for us. It not only reinforces that ethos, but makes it easy for individual team members to be self-sufficient</p>
                      </div>
                    </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="row justify-content-center">
                    <div class="col-5">
                      <div class="testi">
                        <div class="header-testi">
                          <h4>Nama User 3</h4>
                          <label for="">Jabatan User 3</label>
                        </div>
                        <img src="<?php echo base_url('')?>assets/img/testi/user1.jpg" alt="">
                        <p>So much of what we already do relies on individual team members being self-starting and self-reliant, that Kin was a natural fit for us. It not only reinforces that ethos, but makes it easy for individual team members to be self-sufficient</p>
                      </div>
                    </div>
                    <div class="col-5">
                      <div class="testi">
                        <div class="header-testi">
                          <h4>Nama User 4</h4>
                          <label for="">Jabatan User 4</label>
                        </div>
                        <img src="<?php echo base_url('')?>assets/img/testi/user2.jpg" alt="">
                        <p>So much of what we already do relies on individual team members being self-starting and self-reliant, that Kin was a natural fit for us. It not only reinforces that ethos, but makes it easy for individual team members to be self-sufficient</p>
                      </div>
                    </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="row justify-content-center">
                    <div class="col-5">
                      <div class="testi">
                        <div class="header-testi">
                          <h4>Nama User 5</h4>
                          <label for="">Jabatan User 5</label>
                        </div>
                        <img src="<?php echo base_url('')?>assets/img/testi/user1.jpg" alt="">
                        <p>So much of what we already do relies on individual team members being self-starting and self-reliant, that Kin was a natural fit for us. It not only reinforces that ethos, but makes it easy for individual team members to be self-sufficient</p>
                      </div>
                    </div>
                    <div class="col-5">
                      <div class="testi">
                        <div class="header-testi">
                          <h4>Nama User 6</h4>
                          <label for="">Jabatan User 6</label>
                        </div>
                        <img src="<?php echo base_url('')?>assets/img/testi/user2.jpg" alt="">
                        <p>So much of what we already do relies on individual team members being self-starting and self-reliant, that Kin was a natural fit for us. It not only reinforces that ethos, but makes it easy for individual team members to be self-sufficient</p>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#slider-testimoni" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#slider-testimoni" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
      </div>
    </section>
    <section id="sec7LP">
        <div class="row justify-content-center" id="headerFAQ">
            <div class="col-10 text-center">
                <h2>Pertanyaan Umum (FAQ)</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-10">
              <div id="accordion" role="tablist">
                  <div class="card">
                    <div class="card-header" role="tab" id="faq1">
                      <h5 class="mb-0">
                        <a data-toggle="collapse" href="#isi-faq1" aria-expanded="true" aria-controls="isi-faq1">
                          Apa itu Monika?
                        </a>
                      </h5>
                    </div>
                    <div id="isi-faq1" class="collapse" role="tabpanel" aria-labelledby="faq1" data-parent="#accordion">
                      <div class="card-body">
                        <p>Monika adalah aplikasi untuk membuat proposal (quotation / penawaran), kontrak dan invoice berbasis online.</p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" role="tab" id="faq2">
                      <h5 class="mb-0">
                        <a class="collapsed" data-toggle="collapse" href="#isi-faq2" aria-expanded="false" aria-controls="isi-faq2">
                          Mengapa Monika?
                        </a>
                      </h5>
                    </div>
                    <div id="isi-faq2" class="collapse" role="tabpanel" aria-labelledby="faq2" data-parent="#accordion">
                      <div class="card-body">
                        <p>Aplikasi monika sangat simpel. Anda hanya perlu persiapkan data klien dan menambahkan sedikit informasi terkait untuk proposal, kontrak atau invoice. Tidak perlu berganti perangkat atau aplikasi dokumen, semua diakses melalui satu aplikasi begitu juga klien Anda.</p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" role="tab" id="faq3">
                      <h5 class="mb-0">
                        <a class="collapsed" data-toggle="collapse" href="#isi-faq3" aria-expanded="false" aria-controls="isi-faq3">
                          Bagaimana Cara Menggunakannya?
                        </a>
                      </h5>
                    </div>
                    <div id="isi-faq3" class="collapse" role="tabpanel" aria-labelledby="faq3" data-parent="#accordion">
                      <div class="card-body">
                        <h5>Sangatlah mudah.</h5>
                        <ul>
                          <li>Pertama buat akun terlebih dahulu di <a href="<?php echo base_url('');?>/login">https://monika.id/login</a></li>
                          <li>Pilih dokumen mana yang akan Anda buat</li>
                          <li>Masukkan data klien (atau Anda juga bisa memasukkan daftar klien yang banyak terlebih dahulu)</li>
                          <li>Masukkan informasi terkait tujuan Anda</li>
                          <li>Bagikan link dokumen melalui whatsapp, email atau SMS.</li>
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div class="card">
                    <div class="card-header" role="tab" id="faq4">
                      <h5 class="mb-0">
                        <a class="collapsed" data-toggle="collapse" href="#isi-faq4" aria-expanded="false" aria-controls="isi-faq4">
                          Apakah Perlu Mencetak Dokumen untuk Menandatangani?
                        </a>
                      </h5>
                    </div>
                    <div id="isi-faq4" class="collapse" role="tabpanel" aria-labelledby="faq4" data-parent="#accordion">
                      <div class="card-body">
                        <p>Tidak perlu. Cukup berikan link dokumen kepada klien Anda, arahkan untuk mengklik "Tandatangani". Client atau Anda bisa menandatangani dokumen langsung dari smartphone, tablet ataupun laptop. Anda tetap bisa mengunduh versi PDF dari dokumen.</p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" role="tab" id="faq5">
                      <h5 class="mb-0">
                        <a class="collapsed" data-toggle="collapse" href="#isi-faq5" aria-expanded="false" aria-controls="isi-faq5">
                          Apakah Bisa Mencobanya Secara Gratis?
                        </a>
                      </h5>
                    </div>
                    <div id="isi-faq5" class="collapse" role="tabpanel" aria-labelledby="faq5" data-parent="#accordion">
                      <div class="card-body">
                        <p>Anda bisa gunakan secara Gratis dengan batasan 2 dokumen. Selebihnya Anda bisa memilih paket sesuai kebutuhan mulai dari Rp 100.000,- untuk pengguna personal (freelancer).</p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" role="tab" id="faq6">
                      <h5 class="mb-0">
                        <a class="collapsed" data-toggle="collapse" href="#isi-faq6" aria-expanded="false" aria-controls="isi-faq6">
                          Saya tidak pintar mendesain tetapi saya ingin proposal saya terlihat menarik untuk klien. Apakah bisa?
                        </a>
                      </h5>
                    </div>
                    <div id="isi-faq6" class="collapse" role="tabpanel" aria-labelledby="faq6" data-parent="#accordion">
                      <div class="card-body">
                        <p>Mudah, di Monika.id tersedia template yang menarik bagi klien sesuai dengan kebutuhan. Tidak perlu khawatir tentang tampilan, warna, dan detail desain lainnya, karena tim kami sudah menyiapkan yang terbaik untuk Anda gunakan. Anda cukup menulis proposal dan desain otomatis akan menangani masalah Anda.
                          Jika Anda merasa bosan dengan proposal yang ada, Anda boleh menghubungi tim kami dan siap membantu Anda.</p>
                      </div>
                    </div>
                  </div>

                  <div class="card">
                    <div class="card-header" role="tab" id="faq7">
                      <h5 class="mb-0">
                        <a class="collapsed" data-toggle="collapse" href="#isi-faq7" aria-expanded="false" aria-controls="isi-faq7">
                          Apakah proposal di Monika.id bisa diubah?
                        </a>
                      </h5>
                    </div>
                    <div id="isi-faq7" class="collapse" role="tabpanel" aria-labelledby="faq7" data-parent="#accordion">
                      <div class="card-body">
                        <p><b>YA</b>, setiap proposal di Monika.id yang dibuat bisa diubah 100%. Anda cukup mengubah dan menghapus bagian yang dirasa perlu untuk diubah.</p>
                      </div>
                    </div>
                  </div>

                  <div class="card">
                    <div class="card-header" role="tab" id="faq8">
                      <h5 class="mb-0">
                        <a class="collapsed" data-toggle="collapse" href="#isi-faq8" aria-expanded="false" aria-controls="isi-faq8">
                          Apakah logo Monika.id akan muncul di dalam proposal yang dikirim ke klien kami?
                        </a>
                      </h5>
                    </div>
                    <div id="isi-faq8" class="collapse" role="tabpanel" aria-labelledby="faq8" data-parent="#accordion">
                      <div class="card-body">
                        <p><b>Tidak</b>, semua pelanggan berbayar mendapatkan pilihan, jadi tidak ada watermark monika.id di dalam proposal yang akan digunakan ke klien.</p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" role="tab" id="faq9">
                      <h5 class="mb-0">
                        <a class="collapsed" data-toggle="collapse" href="#isi-faq9" aria-expanded="false" aria-controls="isi-faq9">
                          Apakah Monika juga memberikan layanan untuk Perusahaan besar?
                        </a>
                      </h5>
                    </div>
                    <div id="isi-faq9" class="collapse" role="tabpanel" aria-labelledby="faq9" data-parent="#accordion">
                      <div class="card-body">
                        <p>Kami juga menawarkan fitur untuk Enterprise , dimana Anda bisa menikmati layanan bebas tak berbatas dan bisa menambahkan tim di akun Anda. Hubungi tim kami, detail bisa dilihat di bawah.</p>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    </section>
    <footer id="footerLP">
      <div class="row justify-content-between">
          <div class="col-7">
            <ul>
              <li><a href="#" style="margin-left:20px;">Cara Kerja</a></li>
              <li><a href="#">Kebijakan Pribadi</a></li>
              <li><a href="#" style="float:left;">Tentang Monika</a></li>
              <li><p>Copyright &copy; <a href="<?php echo base_url('')?>" style="color:var(--main-color)">Monika</a> 2017</p></li>
            </ul>
          </div>
          <div class="col-4 text-right">
              <p>DS Siliwangi Space 3rd Floor Room 303, <br>
                Siliwangi Plaza E6, Jl. Jend. Sudirman No.187 <br>
                Semarang </p>
          </div>
      </div>

    </footer>

  </body>
  <script type="text/javascript">
    $(document).ready(function() {
      var menu = $("#navLP");
      var menuLink = $(".nav-link");
      var pos = menu.position();
    	$(window).scroll(function() {
    		var windowpos = $(window).scrollTop();
    		if (windowpos >= pos.top & windowpos > 50) {
          $("#logoMonika").attr("src","<?php echo base_url('')?>assets/img/monika_logo.png");
          menu.css({"background-color":"#fff"});
    			menu.css({"box-shadow":"0 8px 16px 0 rgba(0,0,0,0.2)"});
          menuLink.css({"color":"#000"});
    		} else {
          $("#logoMonika").attr("src","<?php echo base_url('')?>assets/img/monika_logo_white.png");
          menu.css({"background-color":"transparent"});
          menu.css({"box-shadow":"none"});
          menuLink.css({"color":"#fff"});
    		}
    	});
      $('#slider-testimoni').carousel({
        interval: 1000
      });
    });
  </script>

</html>
