<style>
    h1{
        padding-left: 45%;
        padding-top: 10%;
    }
    .list-quest{
        background-color: #fff;
        padding: 0.5% 2% 0.5% 2%;
        margin-bottom: 20px;
        box-shadow: 3px 3px 4px transparent;
        transition: all 0.3s ease;
    }
    .list-quest:hover{
        box-shadow: 3px 3px 4px #b1b1b1;
    }
    .nama-project-quest {
        padding-bottom: 0px;
        margin-bottom: 0px;
    }
    .list-quest .nama-projek-quest .col-md-12 a h4{
        margin-bottom: 5px;
        color: #000;
        font-weight: bold;
    }
</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row mt">
					<div class="container">
            <div class="col-xs-12">
                  <h3 style="margin-top:10px; margin-bottom:20px;">QUESTIONNAIRE</h3>
                        <?php
                            if (isset($allQuest) == 1){
                                foreach($allQuest as $quest){
                                    $random = md5(mt_rand(1,10000));
                                    $first = substr($random,0,5);
                                    $last = substr($random,5,10);
                                    $urlrand = $first.$quest->id.$last;
                                ?>
                                 <div class="list-quest">
                                      <div class="row nama-projek-quest">
                                            <div class="col-md-12">
                                                <a href="<?= base_url('quest_detail/'.$urlrand)?>"><h4><?php echo $quest->nama_projek;?></h4></a>
                                            </div>
                                      </div>
                                      <div class="row detail-projek-quest">
                                          <div class="col-md-6">
                                                <h5>Klien</h5>
                                                <p>Nama Klien</p>
                                          </div>
                                          <div class="col-md-3">
                                                <h5>Dipersiapkan Oleh</h5>
                                                <p>Anda</p>
                                          </div>
                                          <div class="col-md-3 button-action-quest">
                                                <span class="btn-group group-action" style="padding:5px">
                                                      <a class="btn btn-default btn-edit editprop tooltips" data-placement="top" data-original-title="Edit" id="editprop" name="btn-edit" href=''>
                                                          <i class="fa fa-fw s fa-pencil" aria-hidden="true"></i>
                                                      </a>
                                                      <button class="btn btn-default dupliprop tooltips" data-placement="top" data-original-title="Duplikat">
                                                          <i class="fa fa-fw fa-clone" aria-hidden="true"></i>
                                                      </button>
                                                      <button class="btn btn-default btn-delete archprop tooltips" data-placement="top" data-original-title="Arsipkan">
                                                          <i class="fa fa-fw fa-archive" aria-hidden="true"></i>
                                                      </button>
                                                 </span>
                                          </div>
                                      </div>
                                 </div>
                          <?php }
                            }else{?>
                                  <div class="col-md-12 col-sm-4 mb">
                                    <div class="empty-panel">
                                      <h1 class="mt"><i class="fa fa-list-alt fa-3x"></i></h1>
                                        <div class="darkblue-header text-center">
                                            <h2>Data questionnaire kosong.</h2>
                                            <h5>Dengan klik tombol "Buat Questionnaire Baru" di bawah.</h5>
                                            <a href="<?= site_url('quest_baru/'.$urlrand) ?>" class="btn btn-theme03">
                                                <i class="fa fa-plus" aria-hidden="true"></i> Buat Questionnaire Baru
                                            </a>
                                        </div>
                                    </div>
                                  </div>
                      <?php }?>
            </div>
        	</div>
        </div>
    </section>
</section>
