<style>
    input:valid {
        border-color: #ccc;
    }
    .quest .row{
        padding-left: 7%;
        padding-right: 7%;
    }
    .header-quest{
        padding: 10% 0% 10% 5%;
        background-color: #46bfb2;
        margin-left:0px;
        margin-right:0px;
        color: #fff
    }
    .header-quest .col-xs-6 h1{
        margin-top: 10px;
    }
    .header-quest .col-xs-6 h3{
        margin-top: 0px;
    }
    .desc-quest{
        padding-top: 40px;
        padding-bottom: 10px;
    }
    .sec-quest{
        margin-top: 20px;
        padding-top: 10px;
    }
    .sec-quest .form-group label{
        font-size: 12pt;
        margin-bottom: 10px;
    }
    .sec-quest .form-group textarea{
        resize: none;
        overflow: hidden;
        border: none;
        box-shadow: none;
        border-bottom: 1px solid #cecece;
        border-radius: 0px;
        padding: 0px;
    }
    .quest_items .row, .add-sec-quest{
        margin-left: 0px;
        margin-right: 0px;
    }
    .quest_items .row:hover{
        background-color: #ececec;
    }
    .quest_items .row .form-group
    .quest_items .row .form-group textarea::placeholder{
        color: var(--main-color);
        font-weight: bold;
    }
    .add-sec-quest{
        margin-top: 20px;
        margin-bottom: 40px;
    }
    .add-sec-quest .form-group{
        margin-bottom: 20px;
    }
    .add-sec-quest .form-group a{
        text-align: left;
        width: 100%;
        color: var(--main-color);
		transition: all 0.3s ease;
    }
    .add-sec-quest .form-group a:hover{
        cursor: pointer;
        background: #46bfb2;
        color: #fff;
    }
    .add-sec-quest .form-group a i{
        margin-right: 5px;
    }
    .add-sec-quest .form-group a h5{
        font-size: 12pt;
    }
    .quest_items .sec-quest .form-group label{
        position: relative;
    }
    .quest_items .sec-quest .form-group label input{
        background-color: transparent;
        border: 0;
        padding: 3px 0px;
        cursor: text;
        box-sizing: border-box;
        transition: width 0.25s;
    }
    .quest_items .sec-quest .form-group label input:focus{
        outline: none;
    }
    .title-quest ~ .focus-border{
        position: absolute;
        bottom: 0;
        left: 50%;
        width: 0;
        height: 2px;
        background-color: var(--main-color);
        transition: 0.4s;
    }
    .title-quest:focus ~ .focus-border{
        width: 100%;
        transition: 0.4s;
        left: 0;
    }
    .quest_items .sec-quest .form-group .btn{
        position: absolute;
        right: -30px;
        top: 40px;
        visibility: hidden;
    }
    .quest_items .sec-quest .form-group .quest-handle{
        cursor: move;
        position: absolute;
        left: -30px;
        top: -2px;
        font-size: 15pt;
        color: #868686;
        visibility: hidden;
    }
	    .quest_items .sec-quest:hover .form-group .btn, .quest_items .sec-quest:hover .form-group span{
        visibility: visible;
    }

</style>

<section id="main-content">
    <section class="wrapper">
        <div class="row mt" style="margin-top:35px">
            <div class="showback quest">
                <div class="row header-quest">
                    <div class="col-xs-2">
                        <img src="<?= base_url()?>assets/images/white-logo.png" width="100" height="100">
                    </div>
                    <div class="col-xs-6">
                        <h1>Nama Projek</h1>
                        <h3>Nama Klien</h3>
                    </div>
                </div>
                <div class="row desc-quest">
                    <div class="col-xs-3">

                    </div>
                    <div class="col-xs-8 items">
                        <p>Kami ingin tahu lebih lanjut tentang anda dan apa visi dari projek yang anda buat.</p>
                        <p>Nama Klien</p>
                    </div>
                </div>
                <div class="quest_items" id="items">
                     <div class="row sec-quest" data-id="1" id="quest-1">
                        <div class="col-xs-12 form-group">
                            <span class="quest-handle"><i class="fa fa-bars" aria-hidden="true"></i></span>
                            <label><input class="title-quest" type="text" name="quest1" value="Judul Pertanyaan 1"><span class="focus-border"></span></label>
                            <textarea class="form-control isi-quest-sec" id="quest_no_1"  data-autoresize rows="2" placeholder="Isikan Pertanyaan anda disini"></textarea>
                        </div>
                    </div>
                </div>
                <p id="hasil">
				</p>

                <div class="row add-sec-quest">
                    <div class="col-xs-12 form-group">
                        <a class="btn btn-add-quest-sec">
                            <h5><i class="fa fa-plus-circle" aria-hidden="true"></i>Tambahkan Pertanyaan/Section</h5>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<script src="<?= base_url();?>assets/js/Sortable.min.js"></script>
<script>
    //    Sortable
	var sort = document.getElementById("items");
	var hasilsort = Sortable.create(sort, {
		group: "question",
        handle: ".quest-handle",
		animation: 150,
		dataIdAttr: 'data-id',
		store: {
			get: function (sortable) {
				var order = localStorage.getItem(sortable.options.group);
				return order ? order.split('|') : [];
			},
			set: function (sortable) {
				var order = sortable.toArray();
//				document.getElementById("hasil").innerHTML=order;
				var hasilorder = document.getElementById("hasil").innerHTML=order;
				return hasilorder;
			}
		}
	});



    //  autowidth input
    $.fn.textWidth = function(text, font) {

        if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);

        $.fn.textWidth.fakeEl.text(text || this.val() || this.text() || this.attr('placeholder')).css('font', font || this.css('font'));

        return $.fn.textWidth.fakeEl.width();
    };

    $('.title-quest').on('input', function() {
        var inputWidth = $(this).textWidth();
        $(this).css({
            width: inputWidth+10
        })
    }).trigger('input');

    function inputWidth(elem, minW, maxW) {
        elem = $(this);
    }
    var targetElem = $('.title-quest');
    inputWidth(targetElem);

    // autoheight textarea
    jQuery.each(jQuery('textarea[data-autoresize]'), function() {
        var offset = this.offsetHeight - this.clientHeight;

        var resizeTextarea = function(el) {
            jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
        };
        jQuery(this).on('keyup input', function() { resizeTextarea(this); }).removeAttr('data-autoresize');
    });

    // tambah section quest
    var data_id = 2;
    $('.btn-add-quest-sec').on('click',function(){
		var newhandlerquest = $('<span class="quest-handle"><i class="fa fa-bars" aria-hidden="true"></i></span>');
		var newsectionquest = $('<div class="row sec-quest" data-id="'+data_id+'" id="quest-'+data_id+'"></div>');
		var newinputquest = $('<div class="col-xs-12 form-group"><label> <input class="title-quest" type="text" name="quest1" value="Judul Pertanyaan '+data_id+'"><span class="focus-border"></span></label><textarea class="form-control isi-quest-sec" data-autoresize rows="2" placeholder="Isikan Pertanyaan anda disini" id="quest_no_'+data_id+'"></textarea></div>');
		var newbtndeletequest= $('<button class="btn btn-delete-quest" btndelquest="'+data_id+'"><i class="fa fa-trash" aria-hidden="true"></i></button>');
		data_id++;
		$('.quest_items').append(newsectionquest.append(newinputquest.append(newhandlerquest,newbtndeletequest)));
		newinputquest.select();
		Object.values(hasilsort.options.store.set(hasilsort));
    });

    //Hapus quest
    $(document).on('click','.btn-delete-quest',function(){
		var data_id_quest = $(this).attr('btndelquest');
		$('#quest-'+data_id_quest).remove();
		data_id_quest--;
		Object.values(hasilsort.options.store.set(hasilsort));
//		document.getElementById("hasil").innerHTML = ;
    });


</script>
