<!DOCTYPE html>
<html lang="id">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Software pendukung dan pengelolaan penjualan untuk freelancer dan agency dalam mengirim proposal hingga mendapatkan pembayaran tepat waktu.">
    <meta name="author" content="Monika">
    <meta name="keyword" content="Proposal, Invoice, Escrow, Penawaran, Lacak Proposal, Tracking, Closing, Proposal Goal">

    <title>Monika - Login</title>
    <link rel="shortcut icon" href="assets/img/favicon.ico">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/monika.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">

	<!-- Hotjar Tracking Code for https://app.monika.id -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:751855,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115539143-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-115539143-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1971319419754936');
      fbq('track', 'PageView');
    </script>

    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=1971319419754936&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
  </head>
  <body>
	  <div id="login-page" class="login-page">
	  	<div class="container" style="margin-top:10%">
	  		<div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-6 hidden-xs" style="padding-top:40px">
	  			<div style="color:#fff;">
	  				<img width="150" src="<?php echo base_url(); ?>assets/img/monika_logo.png">
	  				<h3 style="margin-top:20px;">Menangkan klien dalam waktu singkat</h3>
	  				<p style="font-size:16px;line-height:40px;"><i class="fa fa-check" aria-hidden="true"></i> <span style="color:#46bfb2;">Mudah</span> membuatnya</p>
	  				<p style="font-size:16px;line-height:40px;"><i class="fa fa-check" aria-hidden="true"></i> <span style="color:#46bfb2;">Banyak template</span> proposal sesuai bidang</p>
	  				<p style="font-size:16px;line-height:40px;"><i class="fa fa-check" aria-hidden="true"></i> <span style="color:#46bfb2;">Aman</span>, data hingga pembayaran lebih aman</p>
	  				<p style="font-size:16px;line-height:40px;"><i class="fa fa-check" aria-hidden="true"></i> <span style="color:#46bfb2;font-style: italic;">On-time</span>, terima pembayaran tepat waktu</p>
	  			</div>
	  		</div>
	  		<div class="col-sm-6 col-xs-12">
		  		<?php
	                  $success_msg = $this->session->flashdata('success_msg');
	                  $error_msg = $this->session->flashdata('error_msg');
	                ?>
			      <form class="form-login" id="form-login" method="post" action="<?php echo isset($redirect) ? base_url('user/login_user/?redirect='.$redirect): base_url('user/login_user'); ?>">
			        <h2 class="form-login-heading">Login</h2>
			        <div class="login-wrap">
			            <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" autocomplete="off" autofocus>
			            <br>
			            <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Password" autocomplete="off">
			            <label class="checkbox">
			                <span class="pull-left">
			                    <a data-toggle="modal" href="login.html#myModal"> Lupa Password?</a>
			                </span>
			            </label>
                  <?php if($this->session->flashdata('status')){
                    echo $this->session->flashdata('status');
                  }else{
                  	  if($this->session->flashdata('status_verif')==1){
	                    echo "<div class='alert alert-success'><p>Verifikasi email berhasil, silahkan login</p></div>";
	                  }else if($this->session->flashdata('status_verif')==2){
	                    echo "<div class='alert alert-danger'><p>Verifikasi email belum berhasil</p></div>";
	                  }else if($this->session->flashdata('status_regis')==1){
	                    echo "<div class='alert alert-success'><p>Registrasi berhasil, silahkan cek email untuk verifikasi</p></div>";
	                  }else if($this->session->flashdata('status_pass')==1){
	                    echo "<div class='alert alert-success'><p>Password berhasil diubah, silahkan login kembali</p></div>";
	                  }else if($this->session->flashdata('status_pass')==2){
	                    echo "<div class='alert alert-success'><p>Password gagal diubah, silahkan cek email kembali</p></div>";
	                  }
                  }
                  ?>
			            <button class="btn btn-theme btn-block" id="btnlogin" type="button"><i class="fa fa-lock"></i>LOGIN</button>
			            <?php if($success_msg){ ?>
	                    <div class="alert alert-success">
		                      <?php echo $success_msg; ?>
		                    </div>
		                <?php }if($error_msg){ ?>
		                    <div class="alert alert-danger">
		                      <?php echo $error_msg; ?>
		                    </div>
		                <?php } ?>
			            <hr>
			            <div class="registration">
			                Belum memiliki akun?<br/>
			                <a class="" href="<?php echo site_url('register'); ?>">
			                    Daftar di sini
			                </a>
			            </div>

			        </div>
			      </form>
			</div>

	  	</div>
	  </div>

	  <!-- Modal -->
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Lupa Password ?</h4>
                  </div>
            <form id="resetPassword" name="resetPassword" method="post" action="<?php echo base_url();?>User/ForgotPassword" onsubmit ='return validate()'>
                  <div class="modal-body">
                      <p>Masukkan email anda untuk reset password.</p>
                  <input type="email" name="user_email" id="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
              </div>
              <div class="modal-footer">
                 <button data-dismiss="modal" class="btn btn-default" type="button">Batal</button>
                 <button class="btn btn-theme" value="submit" type="submit">Kirim</button>
             </div>
          </form>
              </div>
          </div>
        </div>
      <!-- modal -->

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("<?php echo base_url(); ?>assets/img/login-bg_after.jpg", {speed: 500});
    </script>

    <script type="text/javascript" src="<?php echo base_url();?>node_modules/sweetalert/dist/sweetalert.min.js"> </script>

    <script type="text/javascript">
      function ValidateEmail(email) {
          //var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
          var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return expr.test(email);
      };

      $(document).ready(function(){
      	$('#user_email,#user_password').on('keypress',function (e) {
		  if (e.which == 13) {
		    $('#btnlogin').trigger('click');
		  }
		});

      	var format = /[$%^&*()+\=\[\]{};'"\\|<>\/?]/;

      	$('#btnlogin').on('click',function(){
      	  if($('#user_email').val() == '')
          {
            swal("Oops","Email wajib diisi","error");
          }
          else if(!ValidateEmail($('#user_email').val()))
          {
            swal("Oops","Pastikan format email benar","error");
          }
          else if($('#user_password').val() == '')
          {
            swal("Oops","Password wajib diisi","error");
          }
          else
          {
          	if(format.test($('#user_email').val()))
            {
              swal("Oops","Mohon tidak menginput special character!","error");
            }
            else if(format.test($('#user_password').val()))
            {
              swal("Oops","Mohon tidak menginput special character!","error");
            }
            else
            {
            	$('#btnlogin').prop('disabled',true).html('Masuk...');
            	$('#form-login').submit();
            }
          }
      	});
      });
  	</script>
  	<!--Start of Tawk.to Script-->
	<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/5a619ebb4b401e45400c3764/default';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
	})();
	</script>
	<!--End of Tawk.to Script-->
  </body>
</html>
