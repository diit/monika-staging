  <section id="main-content" >
     <section class="wrapper">
         <div class="container" style="width:100%">
              <div class="row mt" style="margin-left:0px; margin-right:0px">
                <div class="col-md-4 col-xs-12">
                    <p style="text-align:center;font-weight: bold;">Most Read Proposal</p>
                    <div class="panel-body text-center">
                      <canvas id="pie-top-proposal" height="200" width="300"></canvas>
                    </div>
                </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="panel-body text-center">
                        <canvas id="bar-top-time" width="100" height="60"></canvas>
                      </div>
                    <p style="text-align: center;font-size: 12px;margin-top: 0;padding: 0;">Berikut adalah waktu disaat klien Anda membaca proposal</p>
                  </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <p style="text-align:center;font-weight: bold;">Statistik Konten</p>
                      <div style="margin-top: 20px;" class="table-responsive">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th>Konten</th>
                              <th>Rata-rata Lama Membaca (detik)</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
                            <?php if(count($avg_readtime) > 0) { ?>
                                  <?php foreach ($avg_readtime as $ar) { ?>
                                    <tr>
                                      <td><?php echo count(explode("section_",$ar->section)) == 2 ? ucfirst(explode("section_",$ar->section)[1]) : ucfirst(explode("section_",$ar->section)[0]); ?></td>
                                      <td><?php echo round($ar->rata); ?></td>
                                    </tr>
                                  <?php } ?>
                            <?php }else{ ?>
                                  <tr>
                                    <td colspan="2">
                                      <div style="text-align: center;margin-top: 50px;">
                                        <i class="fa fa-pause" style="font-size: 2.5em;color: #797979;"></i>
                                          <h4>Belum ada data</h4>
                                      </div>
                                    </td>
                                  </tr>
                            <?php } ?>
                          <?php }else{ ?>
                            <?php if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1){ ?>
                              <?php if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4){ ?>
                                <?php if(count($avg_readtime) > 0) { ?>
                                  <?php foreach ($avg_readtime as $ar) { ?>
                                    <tr>
                                      <td><?php echo count(explode("section_",$ar->section)) == 2 ? ucfirst(explode("section_",$ar->section)[1]) : ucfirst(explode("section_",$ar->section)[0]); ?></td>
                                      <td><?php echo round($ar->rata); ?></td>
                                    </tr>
                                  <?php } ?>
                                <?php }else{ ?>
                                  <tr>
                                    <td colspan="2">
                                      <div style="text-align: center;margin-top: 50px;">
                                        <i class="fa fa-pause" style="font-size: 2.5em;color: #797979;"></i>
                                          <h4>Belum ada data</h4>
                                      </div>
                                    </td>
                                  </tr>
                                <?php } ?>
                              <?php }else{ ?>
                                <tr>
                                  <td colspan="2"><p style="text-align: center;"><a href="<?php echo site_url('plan'); ?>"><i class="fa fa-info-circle"></i> Ubah plan ke "Agency" untuk melihat statistik</a></p></td>
                                </tr>
                              <?php } ?>
                            <?php }else{ ?>
                              <tr>
                                <td colspan="2"><p style="text-align: center;"><a href="<?php echo site_url('plan'); ?>"><i class="fa fa-info-circle"></i> Ubah plan ke "Agency" untuk melihat statistik</a></p></td>
                              </tr>
                            <?php } ?>
                          <?php } ?>
                          </tbody>
                        </table>
                      </div>
                </div>
              </div>
              <div class="row">
                    <div class="col-md-6">
                      <div class="container">
                        <div class="row">
                          <p class="col-md-3 col-xs-6"><label>Est. Income</label></p>
                          <p class="col-md-3 col-xs-5">
                            <strong><?php echo $estimated_revenue > 0 || $estimated_revenue != NULL ? 'Rp '.number_format($estimated_revenue,0,',','.') :'-';  ?></strong>
                          </p>
                        </div>
                        <div class="row">
                          <p class="col-md-3 col-xs-6"><label>Rata-rata proposal dibaca pada pukul</label></p>
                          <p class="col-md-3 col-xs-5">
                          <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
                            <?php echo $best_time_read && $best_time_read != NULL ? $best_time_read.':00' : ' - '; ?>
                          <?php }else{ ?>
                            <?php if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1){ ?>
                              <?php if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4){ ?>
                                <?php echo $best_time_read && $best_time_read != NULL ? $best_time_read.':00' : ' - '; ?>
                              <?php }else{ ?>
                                <a href="<?php echo site_url('plan'); ?>"><i class="fa fa-info-circle tooltips" data-placement="bottom" data-original-title="Ubah plan ke Agency untuk melihat statistik"></i></a>
                              <?php } ?>
                            <?php }else{ ?>
                              <a href="<?php echo site_url('plan'); ?>"><i class="fa fa-info-circle tooltips" data-placement="bottom" data-original-title="Ubah plan ke Agency untuk melihat statistik"></i></a>
                            <?php } ?>
                          <?php } ?>
                          </p>
                        </div>
                        <div class="row">
                          <p class="col-md-3 col-xs-6"><label>Rata-rata proposal dibaca sebanyak</label></p>
                          <p class="col-md-3 col-xs-5">
                          <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
                            <?php echo $avg_read && round($avg_read) > 0 ? round($avg_read) : '0'; ?> x
                          <?php }else{ ?>
                            <?php if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1){ ?>
                              <?php if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4){ ?>
                                <?php echo $avg_read && round($avg_read) > 0 ? round($avg_read) : '0'; ?> x
                              <?php }else{ ?>
                                <a href="<?php echo site_url('plan'); ?>"><i class="fa fa-info-circle tooltips" data-placement="bottom" data-original-title="Ubah plan ke Agency untuk melihat statistik"></i></a>
                              <?php } ?>
                            <?php }else{ ?>
                              <a href="<?php echo site_url('plan'); ?>"><i class="fa fa-info-circle tooltips" data-placement="bottom" data-original-title="Ubah plan ke Agency untuk melihat statistik"></i></a>
                            <?php } ?>
                          <?php } ?>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div style="text-align: center;">
                            <button class="btn btn-default btn-lg" id="create_proposal" style="text-align: left;"><i class="fa fa-file-text-o" aria-hidden="true"></i> Buat Proposal</button>
                            <a href="<?php echo site_url('invoice/baru') ?>" class="btn btn-default btn-lg" style="text-align: left;"><i class="fa fa-list-alt" aria-hidden="true"></i> Kirim Invoice</a>
                      </div>
                    </div>
                  </div>
            <div class="row mt" id="">
              <div class="col-md-6 col-xs-12">
                <div class="white-panel" style="text-align: left;padding: 15px;height: 400px;">
                  <h4 style="margin-left:15px; margin-top:0">RIWAYAT
                    <span class="tooltips" data-placement="bottom" data-original-title="Monitor semua status dokumen dan aktivitas internal Anda maupun tim" style="font-size:12px;"><i class="fa fa-info-circle"></i></span>
                  </h4>
                  <?php if($log_document && count($log_document) > 0){ ?>
                    <div style="padding: 5px;overflow-y: scroll;max-height: 340px;">
                      <?php
                        foreach ($log_document as $ld) {
                          $random = md5(mt_rand(1,10000));
                          $first = substr($random,0,5);
                          $last = substr($random,5,10);
                          $urlrand = $first.$ld->proposal_id.$last;
                          switch ($ld->action) {
                            case 'upload':
                              $act = 'mengunggah foto';
                              break;
                            case 'remove':
                              $act = 'menghapus';
                              break;
                            case 'edit':
                              $act = 'mengubah';
                              break;
                            case 'copy':
                              $act = 'copy';
                              break;
                            case 'duplicate':
                              $act = 'menduplikasi';
                              break;
                            case 'add':
                              $act = 'menambahkan section';
                              break;
                            case 'download':
                              $act = 'download PDF';
                              break;
                            default:
                              $act = $ld->action;
                              break;
                          }
                      ?>
                        <div style="padding: 5px;font-size: 11px;border-bottom: solid 1px #f7f5f5;">
                            <div style="font-size: 13px;">
                            <?= $ld->user_name ?> <?= $act ?> <?=  str_replace('_', ' ', $ld->section); ?> <a href='<?php echo site_url('proposal/lihat/'.$urlrand);?>'><?= $ld->info_projek ?></a>.
                          </div>
                          <div>
                            <span><?= $this->monikalib->format_date_indonesia($ld->logtime).' '.date('H:i:s',strtotime($ld->logtime)); ?></span>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  <?php }else{ ?>
                      <div class="col-md-12" style="font-size: 14px;">
                                <div style="text-align: center;margin-top: 100px;">
                                  <i class="fa fa-history" style="font-size: 3em;color: #797979;"></i>
                                    <h4>Belum ada riwayat dokumen</h4>
                                </div>
                      </div>
                  <?php } ?>
                </div>
              </div>
              <div class="col-md-6 col-xs-12">
                <div class="white-panel" style="text-align: left;padding: 15px;height: 400px;overflow-y: scroll;">
                  <div class="row">
                    <div class="col-md-12">
                      <h4 style="margin-top:0">PROPOSAL <a style="font-size: 12px;text-decoration: underline;" href="<?php echo site_url('proposal'); ?>" class="tooltips" data-placement="bottom" data-original-title="Klik untuk melihat semua proposal">lihat semua</a></h4>
                      <?php
                          if($proposal && count($proposal) > 0){
                            foreach ($proposal as $key=>$prop) {
                              $id_projek = $prop->id;
                              $id_client = $prop->id_client;
                              $klien = $prop->nama_pic;
                              $email = $prop->email;
                              $namaproj = $prop->info_projek;
                              $tgl = $prop->tgl_buat;
                              $status = $prop->status;
                              $user_id = $prop->user_id;
                              $user_name = $prop->user_name;
                              $id_invoice = $prop->id_invoice;
                              $project_fee = $prop->project_fee;
                              $random = md5(mt_rand(1,10000));
                              $first = substr($random,0,5);
                              $last = substr($random,5,10);
                              $urlrand = $first.$id_projek.$last;
                      ?>
                          <div class="col-md-12" style="padding: 5px 0; border-bottom: solid 1px #f7f5f5;">
                            <div style="font-size: 11px;">
                              <div class="col-md-9 col-xs-9" style="font-size: 14px;">
                                <?php echo $namaproj; ?>
                              </div>
                              <div class="col-md-3 col-xs-3">
                                  <!-- Tombol Aksi -->
                                    <span class="btn-group group-action" style="margin-top:0">
                                        <a class="btn btn-default btn-edit editprop tooltips" style="padding:5px;" data-placement="top" data-original-title="Edit" id="editprop" name="btn-edit" href='<?php echo site_url('proposal/lihat/'.$urlrand);?>'><i style="padding: 0;" class="fa fa-fw s fa-pencil" aria-hidden="true"></i></a>
<!--
                                        <button class="btn btn-default dupliprop tooltips" data-placement="top" data-original-title="Duplikat" <?php //echo $key == 0 ?'id="duplicatepro"':''; ?> pid="<?php//$id_projek?>" pname="<?php //echo $namaproj; ?>">
                                            <i class="fa fa-fw fa-clone" aria-hidden="true"></i>
                                        </button>
-->
                                   </span>
                              </div>
                            </div>
                          </div>
                        <?php } ?>
                      <?php }else{ ?>
                          <div class="row">
                            <div style="padding: 5px;font-size: 11px;">
                              <div class="col-md-8 col-xs-6" style="font-size: 14px;">
                                Belum ada proposal
                              </div>
                            </div>
                          </div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <h4 style="margin-top:15px;">INVOICE <a style="font-size: 12px; text-decoration: underline;" href="<?php echo site_url('invoice'); ?>" class="tooltips" data-placement="bottom" data-original-title="Klik untuk melihat semua invoice">lihat semua</a></h4>
                        <?php
                          if($top_invoice && count($top_invoice) > 0){
                            foreach($top_invoice as $d){
                                $random = md5(mt_rand(1,10000));
                                $first = substr($random,0,5);
                                $last = substr($random,5,10);
                                $urlrand = $first.$d->id.$last;
                        ?>
                          <div class="col-md-12" style="padding: 5px 0; border-bottom: solid 1px #f7f5f5;">
                            <div style="font-size: 11px;">
                              <div class="col-md-9 col-xs-9" style="font-size: 14px;">
                                <?php
                                    if(!empty($d->judul)) { echo $d->judul; }
                                    elseif((empty($d->judul)) && (!empty($d->number))) { echo $d->number; }
                                    else{ echo 'Dokumen invoice'; }
                                ?>
                              </div>
                              <div class="col-md-3 col-xs-3">
                                  <!-- Tombol Aksi -->
                                    <span class="btn-group group-action" style="margin-top:0">
                                        <a class="btn btn-default btn-edit editprop tooltips" style="padding:5px;" data-placement="top" data-original-title="Edit" id="editprop" name="btn-edit" href='<?php echo site_url('invoice/lihat/'.$urlrand);?>'>
                                            <i class="fa fa-fw s fa-pencil" style="padding: 0;" aria-hidden="true"></i>
                                        </a>
                                   </span>
                              </div>
                            </div>
                          </div>
                        <?php } ?>
                      <?php }else{ ?>
                          <div class="row">
                            <div style="padding: 5px;font-size: 11px;">
                              <div class="col-md-12" style="font-size: 14px;">
                                <div style="text-align: center;">
                                  <i class="fa fa-list-alt" style="font-size: 3em;color: #797979;"></i>
                                        <h4>Belum ada invoice</h4>
                                        <a href="<?= site_url('invoice/baru') ?>" class="btn btn-default">
                                            <i class="fa fa-plus" aria-hidden="true"></i> Buat Invoice Baru
                                        </a>
                                </div>
                              </div>
                            </div>
                          </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
         </div>
     </section>
  </section>

  <!-- Modal Tentang Usaha -->
    <div class="modal" id="new_proposal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center" id="" style="font-weight:bold">Buat Proposal</h4>
                </div>
                <div class="modal-body">
                  <div class="col-md-12" style="text-align: center;">
                    <div class="col-md-6 col-xs-6">
                      <a href="<?php echo site_url('proposal/baru'); ?>">
                        <div style="width: 100%;padding: 100px; border: solid 1px #ccc; border-radius: 4px; color: #585858;">
                          <i class="fa fa-file-o" aria-hidden="true" style="font-size: 40px;"></i><br>
                          <h4>Buat Baru</h4>
                          <p style="font-size: 16px;">Mulai dengan data dan informasi yang benar-benar baru.</p>
                        </div>
                      </a>
                    </div>
                    <div class="col-md-6 col-xs-6">
                      <a href="<?php echo site_url('proposal/?act=duplicate'); ?>">
                        <div style="width: 100%;padding: 100px; border: solid 1px #ccc; border-radius: 4px; color: #585858;">
                          <i class="fa fa-clone" aria-hidden="true" style="font-size: 40px;"></i><br>
                          <h4>Duplikat</h4>
                          <p style="font-size: 16px;"><span style="color: green; font-weight: 800;">Hemat Waktu!</span> Cukup dengan duplikasi dari proposal yang ada.</p>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="modal-footer" style="border-top:none">
                </div>
            </div>
        </div>
    </div>
<!-- End of Modal Tentang Usaha -->
