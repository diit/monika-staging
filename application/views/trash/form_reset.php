<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Monika - Reset Password</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  		<div class="col-lg-6 pointbtn2">
	  			<div style="padding:30px 50px 0px 70px;margin: 150px auto 0;color:#fff;">
	  				<img width="150" src="<?php echo base_url(); ?>assets/img/monika_logo.png">
	  				<h3 style="margin-top:20px;">Tanpa repot, disukai client.</h3>
	  				<p style="font-size:16px;line-height:40px;"><i class="fa fa-check" aria-hidden="true"></i> <span style="color:#46bfb2;">Mudah diakses</span></p>
	  				<p style="font-size:16px;line-height:40px;"><i class="fa fa-check" aria-hidden="true"></i> <span style="color:#46bfb2;">Desain simpel</span>, fokus pada informasi</p>
	  				<p style="font-size:16px;line-height:40px;"><i class="fa fa-check" aria-hidden="true"></i> <span style="color:#46bfb2;">Terus terpantau</span>, status penawaran hingga penagihan</p>
	  			</div>
	  		</div>
	  		<div class="col-lg-5">
		  		<?php
	                  $success_msg = $this->session->flashdata('success_msg');
	                  $error_msg = $this->session->flashdata('error_msg');
	                ?>
			      <form class="form-login" method="post" action="<?php echo base_url('user/getPassword'); ?>">
			        <h2 class="form-login-heading">Ubah Password Baru</h2>
			        <div class="login-wrap">
                  <input value="<?= $saltid?>" hidden name="saltid">
			            <input type="password" class="form-control" name="new_password" placeholder="Masukan Password Baru">
			            <label class="checkbox">
			                <span class="pull-right">
			                    <a data-toggle="modal" href="login.html#myModal"> Lupa Password?</a>
			                </span>
			            </label>
                  <?php if($this->session->flashdata('status')==1){
                    echo "<div class='alert alert-success'><p>Silahkan cek email anda untuk reset password</p></div>";
                  }else if($this->session->flashdata('status')==2){
                    echo "<div class='alert alert-danger'><p>Email tidak ditemukan</p></div>";
                  }else if($this->session->flashdata('status_verif')==1){
                    echo "<div class='alert alert-success'><p>Verifikasi email berhasil, silahkan login</p></div>";
                  }else if($this->session->flashdata('status_verif')==2){
                    echo "<div class='alert alert-danger'><p>Verifikasi email belum berhasil</p></div>";
                  }else if($this->session->flashdata('status_regis')==1){
                    echo "<div class='alert alert-success'><p>Registrasi berhasil, silahkan cek email untuk verifikasi</p></div>";
                  }
                  ?>
			            <button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> Submit</button>
			            <?php if($success_msg){ ?>
	                    <div class="alert alert-success">
		                      <?php echo $success_msg; ?>
		                    </div>
		                <?php }if($error_msg){ ?>
		                    <div class="alert alert-danger">
		                      <?php echo $error_msg; ?>
		                    </div>
		                <?php } ?>
			            <hr>
			            <div class="registration">
			                Belum memiliki akun?<br/>
			                <a class="" href="<?php echo site_url('register'); ?>">
			                    Daftar di sini
			                </a>
			            </div>

			        </div>
					<img style="margin:5px auto;" class="pointbtn3" width="100" src="<?php echo base_url(); ?>assets/img/monika_logo.png">
			      </form>
			</div>

	  	</div>
	  </div>

	  <!-- Modal -->
			          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
			              <div class="modal-dialog">
			                  <div class="modal-content">
			                      <div class="modal-header">
			                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                          <h4 class="modal-title">Lupa Password ?</h4>
			                      </div>
                            <form id="resetPassword" name="resetPassword" method="post" action="<?php echo base_url();?>User/ForgotPassword" onsubmit ='return validate()'>
  			                      <div class="modal-body">
  			                          <p>Masukkan email anda untuk reset password.</p>
                                  <input type="email" name="user_email" id="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
                              </div>
                              <div class="modal-footer">
                                 <button data-dismiss="modal" class="btn btn-default" type="button">Batal</button>
                                 <button class="btn btn-theme" value="submit" type="submit">Kirim</button>
                             </div>
                          </form>
			                  </div>
			              </div>
			          </div>
			          <!-- modal -->

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("<?php echo base_url(); ?>assets/img/login-bg.jpg", {speed: 500});
    </script>
  </body>
</html>
