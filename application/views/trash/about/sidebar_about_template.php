<!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">

              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li class="mt">
                      <a <?php echo ($this->uri->segment(1) == 'syarat-ketentuan') ? 'class="active"':''; ?>  href="<?php echo site_url('syarat-ketentuan'); ?>">
                          <i class="fa fa-clipboard"></i>
                          <p style="font-size: 12px;" class="pointbtn5">Syarat & Ketentuan</p>
                          <span class="pointbtn4">Syarat & Ketentuan</span>
                      </a>
                  </li>
                  <li>
                        <a <?php echo ($this->uri->segment(1) == 'kebijakan-privasi') ? 'class="active"':''; ?>  href="<?php echo site_url('kebijakan-privasi'); ?>">
                          <i class="fa fa-balance-scale"></i>
                          <p style="font-size: 12px;" class="pointbtn5">Kebijakan Privasi</p>
                          <span class="pointbtn4">Kebijakan Privasi</span>
                        </a>
                  </li>
                  <li>
                        <a <?php echo ($this->uri->segment(1) == 'faq') ? 'class="active"':''; ?>  href="<?php echo site_url('faq'); ?>">
                          <i class="fa fa-question-circle" aria-hidden="true"></i>
                          <p style="font-size: 12px;" class="pointbtn5">Pertanyaan Umum</p>
                          <span class="pointbtn4">Pertanyaan Umum</span>
                        </a>
                  </li>
                  <li>
                        <a <?php echo ($this->uri->segment(1) == 'cara-membuat-proposal') ? 'class="active"':''; ?>  href="<?php echo site_url('cara-membuat-proposal'); ?>">
                          <i class="fa fa-paperclip" aria-hidden="true"></i>
                          <p style="font-size: 12px;" class="pointbtn5">Cara Membuat Proposal</p>
                          <span class="pointbtn4">Cara Membuat Proposal</span>
                        </a>
                  </li>
                  <li>
                        <a <?php echo ($this->uri->segment(1) == 'cara-membuat-invoice') ? 'class="active"':''; ?>  href="<?php echo site_url('cara-membuat-invoice'); ?>">
                          <i class="fa fa-question-circle" aria-hidden="true"></i>
                          <p style="font-size: 12px;" class="pointbtn5">Cara Membuat Invoice</p>
                          <span class="pointbtn4">Cara Membuat Invoice</span>
                        </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>

      </aside>
