<section id="main-content">
    <section class="wrapper">
        <div class="row" style="margin:0px">
            <div class="container" style="margin:20px;">
                <div class="col-xs-12 jarak white-panel-abt" style="text-align: left;">
                    <h2> Kebijakan dan Privasi</h2><br><br>
                    <p>Mohon dibaca dengan teliti bagian kebijakan privasi sebelum menggunakan <a href="https://monika.id" target="_blank">monika.id</a> (“Layanan”,”Kami”). Privasi adalah bagian penting bagi kami. Kebijakan privasi ini dibuat untuk menjelaskan informasi apa saja yang kami peroleh, informasi yang Anda peroleh dan bagaimana kami menggunakannya untuk layanan platform dan website kami.  Dengan mengakses layanan kami berarti Anda menyetujui kebijakan yang sudah kami buat. Pengguna hanya secara sukarela mengirimkan informasi tersebut kepada kami. Pengguna selalu dapat menolak untuk memberikan informasi identifikasi pribadi, kecuali bahwa hal itu dapat mencegahnya untuk terlibat dalam kegiatan terkait situs tertentu.</p>
                    <h3>DATA PRIBADI PENGGUNA (PENGGUNA)</h3>
                    <p>Kami mengumpulkan data pribadi pengguna dari Pengguna kami melalui berbagai cara termasuk, namun tidak terbatas pada, saat pengguna mengunjungi website, berlangganan newsletter, dan berhubungan dengan aktivitas, layanan, fitur atau sumber lain yang kami sediakan pada di situs kami.</p>
                    <p>Di antara jenis data pribadi yang dikumpulkan layanan kami dengan sendirinya atau melalui pihak ketiga, antaranya; alamat email, alamat IP, cookie, lokasi, dan tanda tangan.</p>
                    <p>Rincian lengkap dalam setiap jenis Data Pribadi yang dikumpulkan disediakan dalam bagian khusus dari kebijakan privasi ini dengan penjelasan spesifik yang ditampilkan sebelum pengumpulan Data. Kami akan mengumpulkan dan menggunakan informasi pribadi semata-mata untuk memberikan keamanan dari pengguna dan memenuhi tujuan Anda, kecuali jika kami mendapatkan persetujuan dari orang-orang yang bersangkutan atau yang diwajibkan oleh undang-undang.</p>
                    <h3>DATA KLIEN (PIHAK KETIGA)</h3>
                    <p>Kami mengumpulkan data klien (pihak ketiga) dari Pengguna yang melakukan aktivitas, layanan dan fitur pada website kami.</p>
                    <p>Di antara jenis data pribadi dari klien yang dikumpulkan layanan kami dengan sendirinya dikarenakan adanya penggunaan fitur antaranya; alamat email, alamat IP, cookies dan lokasi.</p>
                    <p>Rincian lengkap dalam setiap jenis Data Klien yang dikumpulkan disediakan dalam bagian khusus dari kebijakan privasi ini dengan penjelasan spesifik yang ditampilkan sebelum pengumpulan Data. Kami akan mengumpulkan dan menggunakan infromasi pihak ketiga semata-mata untuk memenuhi kelengkapan isi dari dokumen yang merupakan produk dari monika.id seperti proposal dan invoice serta tujuan keamanan dari data klien agar tidak terjadi penyalahgunaan, kecuali jika kami mendapatkan persetujuan dari orang-orang yang bersangkutan atau yang diwajibkan oleh undang-undang.</p>
                    <h3>DATA NON-PRIBADI PENGGUNA</h3>
                    <p>Kami dapat mengumpulkan informasi identifikasi non-pribadi tentang pengguna kapan pun ketika berinteraksi dengan website kami, informasi non-pribadi mungkin termasuk nama browser, jenis komputer, dan informasi teknis tentang Pengguna yang terhubung dengan layanan kami, seperti sistem operasi dan penyedia layanan internet yang digunakan dan informasi serupa lainnya.</p>
                    <h3>INFORMASI LOKASI</h3>
                    <p>Layanan kami dapat mengumpulkan dan menggunakan informasi lokasi Anda (misalnya, menggunakan GPS pada perangkat mobile Anda) untuk menyediakan layanan fungsionalitas tertentu pada layanan kami. Informasi lokasi Anda mungkin mengalami penyalahgunaan dan pemantauan oleh orang lain, jadi mohon berhati-hati jika Anda memilih untuk mengaktifkan fungsionalitas lokasi. Kami juga menggunakan informasi lokasi Anda secara agregat.</p>
                    <h3>BAGAIMANA KAMI MENGGUNAKAN DATA PRIBADI</h3>
                    <p>Monika.id mengambil dan menggunakan informasi data pribadi untuk tujuan sebagai berikut:</p>
                    <p><ol>
                            <li>Memperbaiki layanan pelanggan kami<br>Informasi yang Anda berikan membantu kami menanggapi permintaan layanan pelanggan dan kebutuhan dukungan Anda secara lebih efisien.
                            </li>
                            <li>Memperbaiki Website kami<br>Kami membutuhkan umpan balik dari Anda mengenai produk dan layanan kami.
                            </li>
                            <li>Mengirim Email secara berkala<br>Kami kemungkinan akan menggunakan email Anda untuk mengirimkan informasi dan pembaruan yang berkaitan dengan pesanan mereka. Jika Pengguna memutuskan untuk ikut serta ke milis kami, mereka akan menerima email yang berisi berita terbaru dari layanan kami, pembaruan, informasi produk atau layanan terkait, dan lain-lain. Jika sewaktu-waktu Pengguna ingin berhenti berlangganan menerima email di masa mendatang, kami menyertakan rincian instruksi berhenti berlangganan di bagian bawah setiap email.
                            </li>
                    </ol></p>
                    <h3>PERLINDUNGAN DATA PRIBADI</h3>
                    <p>Kami menerapkan praktik pengumpulan, penyimpanan dan pengelolahan data yang tepat dan tindakan pengamanan untuk melindungi dari akses, perubahan, pengungkapan, atau penghancuran informasi pribadi, nama pengguna, kata sandi, informasi transaksi dan Data yang tersimpan pada di website kami secara tidak sah. Namun, ada resiko yang melekat dalam penggunaan internet dan kami tidak akan menjamin bahwa informasi Anda sepenuhnya aman.</p>
                    <h3>BERBAGI DATA PRIBADI PENGGUNA DENGAN PIHAK KETIGA</h3>
                    <p>Kami tidak menjual, menukar, atau menyewakan data pribadi Anda kepada pihak lain. Kami akan membagikan informasi demografis gabungan generik yang tidak terkait dengan informasi identifikasi pribadi apa pun mengenai pengunjung dan pengguna dengan mitra bisnis Anda, afiliasi terpercaya dan pengiklan untuk tujuan yang diuraikan diatas.</p>
                    <h3>INFORMASI YANG DIKIRIM KE PIHAK LUAR</h3>
                    <p>Pengguna harus memahami bahwa data pribadi yang secara sukarela pengguna masukan dan kirimkan ke dalam pihak luar dapat dilihat dan digunakan oleh pihak lain. Kami tidak dapat menggendalikan penggunaan informasi tersebut, dan dengan menggunakan layanan tersebut Pengguna menanggung risiko bahwa informasi pribadi yang Pengguna berikan dapat dilihat dan digunakan oleh pihak ketiga.</p>
                    <h3>MENGUBAH ATAU MENGHAPUS DATA ANDA</h3>
                    <p>Semua pengguna terdaftar dapat meninjau, memperbarui atau memperbaiki informasi pribadi yang diberikan dalam pendaftaran atau profil akun dengan mengubah "preferensi pengguna" mereka dalam akun mereka. Jika Pengguna memilih untuk keluar sepenuhnya, maka akun Pengguna mungkin menjadi dinonaktifkan. Namun, kami tidak dapat menghapus Data Pribadi dari catatan dari pihak ketiga yang telah disediakan dengan informasi Pengguna sesuai dengan kebijakan ini.</p>
                </div>
            </div>
        </div><!-- /row -->
    </section><!-- /wrapper -->
</section><!-- /MAIN CONTENT -->
