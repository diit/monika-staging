<?php
$this->monikalib->detailProposal();


 ?>
<!DOCTYPE html>
<html lang="id">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Software otomatisasi yang membatu mengelola pemasaran freelancer dan ahensi dalam membuat hingga melacak proposal dan faktur untuk memenangkan klien dalam waktu singkat">
    <meta name="author" content="Monika">
    <meta name="keyword" content="Proposal, Invoice, Penawaran, Lacak Proposal, Tracking, Closing, Proposal Goal">
    <title>MONIKA - Manajemen Dokumen</title>
    <link rel="shortcut icon" href="assets/img/favicon.ico">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lineicons/style.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/monika.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/content-tools.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style-dimention.css">

    <link href="<?php echo base_url(); ?>assets/css/spectrum.css" rel="stylesheet">

    <script src="<?php echo base_url()?>assets/js/jquery3.2.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap3.3.7.min.js"></script>

    <!-- Hotjar Tracking Code for https://app.monika.id -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:751855,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars"></div>
              </div>

            <!--logo start-->
            <a href="<?php echo site_url('home'); ?>" class="logo pointbtn">
              <img width="100" src="<?php echo base_url(); ?>assets/img/monika_logo_white.png">
            </a>
            <!--logo end-->

            <div class="top-menu">
              <div class="nav pull-right">
                <?php if($this->session->userdata('status')){ ?>
                  <a href="<?php echo site_url('home'); ?>" style="margin-top: 5px;" class="btn btn-primary">Home</a>
                <?php }else{ ?>
                  <a href="<?php echo site_url('login'); ?>" style="margin-top: 5px;" class="btn btn-primary">Login</a>
                <?php } ?>
            	</div>
            </div>
        </header>
      <!--header end-->
      <!-- Start Sidebar -->
        <?php $this->load->view('about/sidebar_about_template.php');  ?>
      <!-- End Sidebar -->
      <!-- Start Content -->
        <?php  $this->load->view($content);?>
      <!-- End Content -->
  </section>


    <script src="<?php echo base_url(); ?>assets/js/chart-master/Chart.js"></script>


    <!-- Intro JS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/introjs/introjs.css">
    <script type="text/javascript" src="<?php echo base_url();?>node_modules/sweetalert/dist/sweetalert.min.js"> </script>
    <script src="<?=base_url().'assets/introjs/intro.js'?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/monika.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/prop_validation.js"></script>
    <script src="<?php echo base_url(); ?>assets/content-tools.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/editor.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>assets/js/common-scripts.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/spectrum.js"></script>

    <script type="text/javascript">
      $(document).ready(function () {
        $('#tab-new').on('click',function(){
              $('#container-new').fadeIn();
              $('#tab-new').addClass("tab-active");
              $('#container-duplicate').fadeOut();
              $('#tab-duplicate').removeClass("tab-active");
        });

        $('#tab-duplicate').on('click',function(){
              $('#container-duplicate').fadeIn();
              $('#tab-duplicate').addClass("tab-active");
              $('#container-new').fadeOut();
              $('#tab-new').removeClass("tab-active");
        });

        $('.click-contact').on('click',function(){
          $('html,body').animate({scrollTop: $('.contact').offset().top - 50 },'slow');
        });
      });
    </script>


  </body>
</html>
