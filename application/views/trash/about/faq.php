<section id="main-content">
    <section class="wrapper">
        <div class="row" style="margin:0px">
            <div class="container" style="margin:20px;">
                <div class="col-xs-12 jarak white-panel-abt" style="text-align: left;">
                    <h2> Pertanyaan Umum</h2><br><br>
                    <div class="panel-group" id="accordion">
                      <div class="panel panel-default">
                        <div class="panel-heading" style="text-transform:none;">
                          <a data-toggle="collapse" data-parent="#accordion" href="#faq1">
                            <h5 class="panel-title" style="color:#757575;font-weight: 400;">
                              Apa itu Monika?
                            </h5>
                        </a>
                        </div>
                        <div id="faq1" class="panel-collapse collapse">
                          <div class="panel-body">Monika adalah aplikasi untuk membuat proposal (quotation / penawaran) dan invoice berbasis online.</div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" style="text-transform:none;">
                          <a data-toggle="collapse" data-parent="#accordion" href="#faq2">
                            <h4 class="panel-title" style="color:#757575;font-weight: 400;">
                              Mengapa Monika?
                            </h4>
                        </a>
                        </div>
                        <div id="faq2" class="panel-collapse collapse">
                          <div class="panel-body">Aplikasi monika sangat simpel. Anda hanya perlu persiapkan data klien dan menambahkan sedikit informasi terkait untuk proposal atau invoice. Tidak perlu berganti perangkat atau aplikasi dokumen, semua diakses melalui satu aplikasi begitu juga klien Anda.</div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" style="text-transform:none;">
                          <a data-toggle="collapse" data-parent="#accordion" href="#faq3">
                            <h4 class="panel-title" style="color:#757575;font-weight: 400;">
                              Bagaimana cara mulai menggunakannya?
                            </h4>
                        </a>
                        </div>
                        <div id="faq3" class="panel-collapse collapse">
                          <div class="panel-body">Sangatlah mudah.
                            <ul>
                              <li>Pertama buat akun terlebih dahulu di <a href="https://app.monika.id/login" target="_blank">https://app.monika.id/login</a></li>
                              <li>Pilih dokumen mana yang akan Anda buat</li>
                              <li>Masukkan data klien (atau Anda juga bisa memasukkan daftar klien yang banyak terlebih dahulu)</li>
                              <li>Masukkan informasi terkait tujuan Anda</li>
                              <li>Bagikan link dokumen melalui whatsapp, email atau SMS.</li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" style="text-transform:none;">
                          <a data-toggle="collapse" data-parent="#accordion" href="#faq4">
                            <h4 class="panel-title" style="color:#757575;font-weight: 400;">
                              Apakah perlu mencetak dokumen untuk menandatangani?
                            </h4>
                        </a>
                        </div>
                        <div id="faq4" class="panel-collapse collapse">
                          <div class="panel-body">Tidak perlu. Cukup berikan link dokumen kepada klien Anda, arahkan untuk mengklik "Tandatangani". Client atau Anda bisa menAndatangani dokumen langsung dari smartphone, tablet ataupun laptop. Anda tetap bisa mengunduh versi PDF dari dokumen.</div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" style="text-transform:none;">
                          <a data-toggle="collapse" data-parent="#accordion" href="#faq5">
                            <h4 class="panel-title" style="color:#757575;font-weight: 400;">
                              Apakah bisa mencobanya secara Gratis?
                            </h4>
                        </a>
                        </div>
                        <div id="faq5" class="panel-collapse collapse">
                          <div class="panel-body">Anda bisa gunakan secara Gratis dengan batasan 2 dokumen. Selebihnya Anda bisa memilih paket sesuai kebutuhan mulai dari Rp 79.000,- untuk pengguna personal.</div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" style="text-transform:none;">
                          <a data-toggle="collapse" data-parent="#accordion" href="#faq6">
                            <h4 class="panel-title" style="color:#757575;font-weight: 400;">
                              Saya tidak bisa desain tetapi saya ingin proposal saya terlihat menarik untuk klien. Apakah bisa?
                            </h4>
                        </a>
                        </div>
                        <div id="faq6" class="panel-collapse collapse">
                          <div class="panel-body">
                            Proposal yang baik adalah proposal yang informasinya dapat langsung dimengerti oleh klien. Maka dari itu, kami mempersiapkan bagaimana agar informasi yang akan Anda sampaikan dapat terlihat jelas oleh klien ketimbang memikirkan atribut desain. Anda cukup menulis proposal. Jika Anda merasa bosan dengan proposal yang ada, Anda
                        boleh menghubungi tim kami dan siap membantu Anda.
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" style="text-transform:none;">
                          <a data-toggle="collapse" data-parent="#accordion" href="#faq7">
                            <h4 class="panel-title" style="color:#757575;font-weight: 400;">
                              Apakah isi proposal di Monika.id bisa diubah?
                            </h4>
                        </a>
                        </div>
                        <div id="faq7" class="panel-collapse collapse">
                          <div class="panel-body">
                            YA, setiap isi proposal di Monika.id yang dibuat bisa diubah 100%. Anda cukup mengubah dan menghapus bagian yang dirasa perlu untuk diubah.
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" style="text-transform:none;">
                          <a data-toggle="collapse" data-parent="#accordion" href="#faq8">
                            <h4 class="panel-title" style="color:#757575;font-weight: 400;">
                              Apakah logo Monika.id akan muncul di dalam proposal yang dikirim ke klien kami?
                            </h4>
                        </a>
                        </div>
                        <div id="faq8" class="panel-collapse collapse">
                          <div class="panel-body">
                            Tidak, semua pelanggan berbayar mendapatkan pilihan, jadi tidak ada watermark monika.id di dalam proposal yang akan digunakan ke klien.
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" style="text-transform:none;">
                          <a data-toggle="collapse" data-parent="#accordion" href="#faq9">
                            <h4 class="panel-title" style="color:#757575;font-weight: 400;">
                              Apakah Monika juga memberikan layanan untuk Perusahaan besar?
                            </h4>
                        </a>
                        </div>
                        <div id="faq9" class="panel-collapse collapse">
                          <div class="panel-body">
                            Kami juga menawarkan fitur untuk Enterprise , dimana Anda bisa menikmati layanan bebas tak berbatas dan bisa menambahkan tim di akun Anda. Hubungi tim kami, klik chat sebelah kanan.
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div><!-- /row -->
    </section><!-- /wrapper -->
</section><!-- /MAIN CONTENT -->
