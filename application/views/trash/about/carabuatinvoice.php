<section id="main-content">
    <section class="wrapper">
        <div class="row" style="margin:0px">
            <div class="container" style="margin:20px;">
                <div class="col-xs-12 jarak white-panel-abt">
                    <h2 style="text-align: center;margin: 25px 0;">Berikut adalah cara membuat Invoice</h2>
                    <div class="vendor-tabs text-center">
                        <div id="tab-new" class="col-md-3 col-xs-6 col-md-offset-3 tab tab-active">
                            <p>Buat Baru</p>
                        </div>
                        <div id="tab-duplicate" class="col-md-3 col-xs-6 tab">
                            <p>Sesuai Proposal</p>
                        </div>
                    </div>
                    <div id="container-new">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="content-step">
                                    <img class="img-responsive" src="https://monika.id/assets/images/tutorial/invoice_new_1.png">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-step">
                                    <h2 class="step-number">1</h2>
                                    <h4>Klik tombol "Buat Baru" di halaman Invoice.</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="content-step">
                                    <img class="img-responsive" src="https://monika.id/assets/images/tutorial/invoice_new_2.png">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-step">
                                    <h2 class="step-number">2</h2>
                                    <h4>Isikan data klien Anda atau pilih dari data yang telah disimpan sebelumnya.</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="content-step">
                                    <img class="img-responsive" src="https://monika.id/assets/images/tutorial/invoice_new_3.png">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-step">
                                    <h2 class="step-number">3</h2>
                                    <h4>Isi data harga penawaran Anda. Anda bisa menambahkan baris baru atau menghapusnya. Isi kolom tax bila Anda ingin menambahkan.</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="content-step">
                                    <img class="img-responsive" src="https://monika.id/assets/images/tutorial/invoice_new_4.png">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-step">
                                    <h2 class="step-number">4</h2>
                                    <h4>Bila semua sudah diisi, klik "Simpan"</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="content-step">
                                    <img class="img-responsive" src="https://monika.id/assets/images/tutorial/invoice_new_5.png">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-step">
                                    <h2 class="step-number">5</h2>
                                    <h4>Setelah semua data lengkap, saatnya mengirimkan invoice. Ada 2 cara mengirimkan invoice. Pertama, Anda bisa langsung mengirimkan melalui form kirim di halaman Edit. Kedua, Anda bisa mengirimkan melalui media lain seperti chat dan email Anda sendiri dengan cara menyalin (copy) link URL invoice dengan klik tombol "COPY URL".</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="content-step">
                                    <img class="img-responsive" src="https://monika.id/assets/images/tutorial/invoice_new_6.png">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-step">
                                    <h2 class="step-number">6</h2>
                                    <h4>Saatnya menantikan status invoice yang telah dikirim. Anda akan menerima notifikasi di email saat invoice Anda telah dibaca atau disetujui klien. Anda pun bisa melihat secara detail statistik bagaimana klien Anda membaca invoice seperti berapa kali dibaca, berapa lama dan kapan saja invoice dibaca.</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="container-duplicate">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="content-step">
                                    <img class="img-responsive" src="https://monika.id/assets/images/tutorial/invoice_generate_1.png">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-step">
                                    <h2 class="step-number">1</h2>
                                    <h4>Bila Anda sudah membuat proposal / quotation kepada klien yang dimaksud, Anda bisa mencetak invoice langsung berdasarkan angka penawaran yang telah disepakati. Pada halaman daftar proposal, pilih proposal kepada klien yang dimaksud, lalu klik tombol <i class="fa fa-"></i>.</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="content-step">
                                    <img class="img-responsive" src="https://monika.id/assets/images/tutorial/invoice_generate_2.png">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-step">
                                    <h2 class="step-number">2</h2>
                                    <h4>Masuk ke halaman Invoice, Anda akan melihat invoice yang telah dibuat sesuai nama proposal yang dimaksud. Klik tombol <i class="fa fa-pencil"></i> untuk mengubah data.</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="content-step">
                                    <img class="img-responsive" src="https://monika.id/assets/images/tutorial/invoice_generate_3.png">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-step">
                                    <h2 class="step-number">3</h2>
                                    <h4>Setelah semua data lengkap, saatnya mengirimkan invoice. Ada 2 cara mengirimkan invoice. Pertama, Anda bisa langsung mengirimkan melalui form kirim di halaman Edit. Kedua, Anda bisa mengirimkan melalui media lain seperti chat dan email Anda sendiri dengan cara menyalin (copy) link URL invoice dengan klik tombol "COPY URL".</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="content-step">
                                    <img class="img-responsive" src="https://monika.id/assets/images/tutorial/invoice_generate_4.png">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-step">
                                    <h2 class="step-number">4</h2>
                                    <h4>Saatnya menantikan status invoice yang telah dikirim. Anda akan menerima notifikasi di email saat invoice Anda telah dibaca atau disetujui klien. Anda pun bisa melihat secara detail statistik bagaimana klien Anda membaca invoice seperti berapa kali dibaca, berapa lama dan kapan saja invoice dibaca.</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /row -->
    </section><!-- /wrapper -->
</section><!-- /MAIN CONTENT -->
