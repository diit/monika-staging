<section id="main-content">
    <section class="wrapper">
        <div class="row" style="margin:0px">
            <div class="container" style="margin:20px;">
                <div class="col-xs-12 jarak white-panel-abt">
                    <h2 style="text-align: center;margin: 25px 0;">Berikut adalah cara membuat proposal</h2>
                    <div class="vendor-tabs text-center">
                        <div id="tab-new" class="col-md-3 col-xs-6 col-md-offset-3 tab tab-active">
                            <p>Buat Baru</p>
                        </div>
                        <div id="tab-duplicate" class="col-md-3 col-xs-6 tab">
                            <p>Duplikasi</p>
                        </div>
                    </div>
                    <div id="container-new">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <img class="img-responsive" src="https://monika.id/assets/images/tutorial/proposal_new_1.png">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <h2 class="step-number">1</h2>
                                        <h4>Klik tombol "Buat Baru" di halaman Dashboard atau Proposal.</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <img class="img-responsive" src="https://monika.id/assets/images/tutorial/proposal_new_2.png">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <h2 class="step-number">2</h2>
                                        <h4>Isikan informasi usaha Anda, sekilas tentang proyek, budget hingga penawaran yang akan Anda cantumkan ke dalam proposal. Data yang akan dimasukkan pada form ini adalah data proposal.</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <img class="img-responsive" src="https://monika.id/assets/images/tutorial/proposal_new_3.png">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <h2 class="step-number">3</h2>
                                        <h4>Setelah data proposal dimasukkan dan klik Simpan, akan Anda berpindah ke halaman Preview yaitu semua data yang telah Anda input akan ditampilkan berupa tampilan proposal. Anda bisa menambahkan informasi lain di luar data sebelumnya.</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <img class="img-responsive" src="https://monika.id/assets/images/tutorial/proposal_new_4.png">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <h2 class="step-number">4</h2>
                                        <h4>Setelah semua data lengkap, saatnya mengirimkan proposal. Ada 2 cara mengirimkan proposal. Pertama, Anda bisa langsung mengirimkan melalui form kirim di halaman Preview tadi. Kedua, Anda bisa mengirimkan melalui media lain seperti chat dan email Anda sendiri dengan cara menyalin (copy) link URL proposal dengan klik tombol "COPY URL".</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <img class="img-responsive" src="https://monika.id/assets/images/tutorial/proposal_new_5.png">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <h2 class="step-number">5</h2>
                                        <h4>Saatnya menantikan status proposal / quotation yang telah dikirim. Anda akan menerima notifikasi di email saat proposal Anda telah dibaca atau disetujui klien. Anda pun bisa melihat secara detail statistik bagaimana klien Anda membaca proposal seperti berapa kali dibaca, berapa lama dan kapan saja invoice dibaca.</h4>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div id="container-duplicate">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <img class="img-responsive" src="https://monika.id/assets/images/tutorial/proposal_duplicate_1.png">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <h2 class="step-number">1</h2>
                                        <h4>Duplikasi adalah cara tercepat membuat proposal. Anda memiliki daftar proposal yang sudah pernah dibuat sebelumnya ataupun contoh yang telah kami berikan. Klik tanda <i class="fa fa-clone"></i> untuk menduplikat dari proposal yang dipilih.</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <img class="img-responsive" src="https://monika.id/assets/images/tutorial/proposal_duplicate_2.png">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <h2 class="step-number">2</h2>
                                        <h4>Masukkan data klien baru atau bisa juga memilih data klien yang sudah tersimpan sebelumnya. Jangan lupa isikan deskripsikan secara singkat proposal yang akan dibuat. Lalu klik "Lanjutkan"</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <img class="img-responsive" src="https://monika.id/assets/images/tutorial/proposal_duplicate_3.png">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <h2 class="step-number">3</h2>
                                        <h4>Proposal baru berhasil dibuat, kini saatnya Anda melakukan edit data. Anda hanya perlu mengubah beberapa data penyesuaian.</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <img class="img-responsive" src="https://monika.id/assets/images/tutorial/proposal_duplicate_4.png">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <h2 class="step-number">4</h2>
                                        <h4>Setelah semua data lengkap, saatnya mengirimkan proposal. Ada 2 cara mengirimkan proposal. Pertama, Anda bisa langsung mengirimkan melalui form kirim di halaman Preview tadi. Kedua, Anda bisa mengirimkan melalui media lain seperti chat dan email Anda sendiri dengan cara menyalin (copy) link URL proposal dengan klik tombol "COPY URL".</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <img class="img-responsive" src="https://monika.id/assets/images/tutorial/proposal_duplicate_5.png">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="content-step">
                                        <h2 class="step-number">5</h2>
                                        <h4>Saatnya menantikan status proposal / quotation yang telah dikirim. Anda akan menerima notifikasi di email saat proposal Anda telah dibaca atau disetujui klien. Anda pun bisa melihat secara detail statistik bagaimana klien Anda membaca proposal seperti berapa kali dibaca, berapa lama dan kapan saja invoice dibaca.</h4>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div><!-- /row -->
    </section><!-- /wrapper -->
</section><!-- /MAIN CONTENT -->
