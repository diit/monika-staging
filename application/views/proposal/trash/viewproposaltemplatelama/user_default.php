<div class="showback">
                          <div  class="container-fluid" style="padding: 0;background: <?php echo count($style) > 0 ? $style['headerbgcolor'] : '#656464'; ?>; <?php if(count($style) > 0) { if($style['headerimageurl'] == NULL || $style['headerimageurl'] == ''){  }else{ echo 'background:url('.$style['headerimageurl'].')center center;background-size: cover;';  }} ?> color: <?php echo count($style) > 0 ? $style['headertextcolor'] : '#ffffff'; ?>;" id="headerinfo">
                             <div class="col-md-12 view-project-info" style="margin-bottom : 30px">
                              <?php if($id_klien == 1){ ?>
                                <div class="corner-ribbon blue shadow">Contoh</div>
                              <?php } ?>
                               <div class="header-color" style="background-color: #46bfb2;"></div>
                                <div class="col-md-5" style="margin: 20px 0;margin-bottom: 20px;">
                                    <div id="imgheader" style="width: 200px; height: 200px; text-align: center;<?php if(count($style) > 0) { if($style['logoimageurl'] == NULL || $style['logoimageurl'] == ''){}else{ echo 'background:url('.$style['logoimageurl'].')center center no-repeat;background-size: 100%;';  }} ?>">
                                    </div>
                                </div>
                                <div class="col-md-7" style="margin-top: 10px;margin-bottom: 20px;text-shadow: 1px 2px 2px rgba(11, 11, 11, 0.2);">
                                  <h3 class="caption" id="project_name"><?php echo $namaprojek;?></h3>
                                  <div style="width: 28px;height: 15px;margin-top: 5px;margin-bottom: 15px;border-bottom: 1px solid #fff;"></div>
                                  <h5 class="content">Dipersiapkan untuk </h5>
                                  <?php if($perusahaan != NULL || $perusahaan != ''){ ?>
                                    <h4 class="caption" id="client_name"><?php echo $perusahaan; ?></h4>
                                    <h5><?php echo $klien;?></h5>
                                  <?php }else{ ?>
                                    <h5><?php echo $klien;?></h5>
                                  <?php } ?>
                                  <h4 class="caption" style="margin-top : 20px"><?php echo $this->monikalib->format_date_indonesia($tgl); ?></h4>
                                  <div style="margin-top:20px; margin-left:-15px">
                                    <div class="col-md-6">
                                       <h5 class="content">Oleh</h5>
                                       <h4 class="caption-mini"><?php echo $oleh;?></h4>
                                    </div>
                                    <div class="col-md-6">
                                       <h5 class="content">Kontak</h5>
                                       <h4 class="caption-mini"><?php echo $kontak; ?></h4>
                                    </div>
                                 </div>
                             </div>
                           </div>
                       </div>
                   <!-- header proposal --><!-- Overview -->
                      <?php // if(isset($overview) && ($overview != NULL || $overview != '')){ ?>
                        <div style="<?php echo (isset($overview) && ($overview != NULL || $overview != ''))?'display: block;':'display: none;'; ?>" class="row"  id="section_overview">
                          <div class="section-padding">
                            <div class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-4'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-4'; } ?>" >
                              <h3 class="caption">Overview</h3>
                            </div>
                            <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-8'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8'; } ?> view-project-info" id="view-project-info">
                              <div  class="mt content" style="margin-top:20px">
                                <h5 class="content"><?php echo stripslashes($overview);?></h5>
                              </div>
                              <div class="btn-feedback-section tooltips" data-placement="right" data-original-title="Berikan komentar / feedback" id="comment-btn-section_overview" section="overview">
                                <i class="fa fa-comment fa-2x" aria-hidden="true"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php // } ?>
                      <!-- About Me -->
                      <?php // if(isset($aboutyou) && ($aboutyou != NULL || $aboutyou != '')){ ?>
                        <div style="<?php echo (isset($aboutyou) && ($aboutyou != NULL || $aboutyou != ''))?'display: block;':'display: none;'; ?>" class="row"  id="section_aboutme">
                          <div class="section-padding">
                            <div class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-4'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-4'; } ?>" >
                              <h3 class="caption">Tentang Kami</h3>
                            </div>
                            <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-8'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8'; } ?> view-project-info" id="view-project-info">
                              <div  class="mt content" style="margin-top:20px">
                                <h5 class="content"><?php echo stripslashes($aboutyou);?></h5>
                              </div>
                              <div class="btn-feedback-section tooltips" data-placement="right" data-original-title="Berikan komentar / feedback" id="comment-btn-section_aboutme" section="aboutme">
                                <i class="fa fa-comment fa-2x" aria-hidden="true"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php // } ?>
                      <?php // if($teamback && count($teamback) > 0){ ?>
                        <!-- team -->
                        <div style="<?php echo ($teamback && count($teamback) > 0)?'display: block;':'display: none;'; ?>" class="row" id="section_team">
                          <div class="section-padding">
                            <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-4'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-4'; } ?>">
                              <h3 class="caption">Portfolio</h3>
                            </div>
                            <div class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-8'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8'; } ?> view-project-info" >
                              <div  class="mt content">
                                <ol style="-webkit-padding-start: 35px;">
                                <?php foreach ($teamback as $tb){ ?>
                                  <li class="content">
                                    <h5 class="content"><?php echo stripslashes($tb['pengalaman']); ?></h5>
                                  </li>
                                <?php } ?>
                                </ol>
                              </div>
                              <div class="btn-feedback-section tooltips" data-placement="right" data-original-title="Berikan komentar / feedback" id="comment-btn-section_team" section="team">
                                <i class="fa fa-comment fa-2x" aria-hidden="true"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php // } ?>
                      <?php // if($detailkemampuan && count($detailkemampuan) > 0){ ?>
                        <!-- Team Value -->
                        <div style="<?php echo ($detailkemampuan && count($detailkemampuan) > 0)?'display: block;':'display: none;'; ?>"  class="row" id="section_team_value">
                          <div class="section-padding">
                            <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-4'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-4'; } ?>">
                              <h3 class="caption">Skill & Value</h3>
                            </div>
                            <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-8'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8'; } ?> view-project-info">
                              <div  class="mt content">
                                <ol style="-webkit-padding-start: 35px;">
                                  <?php foreach ($detailkemampuan as $kemampuan):?>
                                    <li class="content">
                                      <h5 class="content"><?php echo stripslashes($kemampuan->skill); ?></h5>
                                    </li>
                                  <?php endforeach; ?>
                                </ol>
                              </div>
                              <div class="btn-feedback-section tooltips" data-placement="right" data-original-title="Berikan komentar / feedback" id="comment-btn-section_team_value" section="team_value">
                                <i class="fa fa-comment fa-2x" aria-hidden="true"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php // } ?>
                      <?php // if($detailtahapan && count($detailtahapan) > 0){ ?>
                        <!-- Roadmap -->
                        <div style="<?php echo ($detailtahapan && count($detailtahapan) > 0)?'display: block;':'display: none;'; ?>" class="row" id="section_milestone">
                          <div class="section-padding">
                            <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-4'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-4'; } ?>">
                              <h3 class="caption">Tahapan Kerja</h3>
                            </div>
                            <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-8'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8'; } ?> view-project-info">
                              <div  class="mt content">
                                <ol style="-webkit-padding-start: 35px;">
                                  <?php foreach ($detailtahapan as $tahapan): ?>
                                      <li class="content">
                                            <h5 class="content"><?php echo stripslashes($tahapan->tahapan);?></h5>
                                    </li>
                                  <?php endforeach; ?>
                                </ol>
                              </div>
                              <div class="btn-feedback-section tooltips" data-placement="right" data-original-title="Berikan komentar / feedback" id="comment-btn-section_milestone" section="milestone">
                                <i class="fa fa-comment fa-2x" aria-hidden="true"></i>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- What you get -->
                        <div style="<?php echo (isset($keuntungan) && ($keuntungan != NULL || $keuntungan != ''))?'display: block;':'display: none;'; ?>" class="row" id="section_benefit">
                          <div class="section-padding">
                            <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-4'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-4'; } ?>">
                              <h3 class="caption">Hasil Pekerjaan</h3>
                            </div>
                            <div class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-8'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8'; } ?> view-project-info" >
                              <div  class="mt content">
                                <div class="content" style="margin-top : 10px">
                                  <h5 class="content"><?php echo ($keuntungan == NULL || $keuntungan == '')?'-':stripslashes($keuntungan); ?></h5>
                                </div>
                              </div>
                              <div class="btn-feedback-section tooltips" data-placement="right" data-original-title="Berikan komentar / feedback" id="comment-btn-section_benefit" section="benefit">
                                <i class="fa fa-comment fa-2x" aria-hidden="true"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php // } ?>
                      <?php // if($detailpemberian && count($detailpemberian) > 0){ ?>
                        <!-- Biaya dan Keuntungan -->
                        <div style="<?php echo ($detailpemberian && count($detailpemberian) > 0)?'display: block;':'display: none;'; ?>" class="row" id="section_fee">
                          <div class="section-padding">
                            <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-4'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-4'; } ?>">
                              <h3 class="caption">Biaya</h3>
                            </div>
                            <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-8'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8'; } ?> view-project-info">
                              <div  class="mt">
                                <table class="table" style="width:100%">
                                  <tbody>
                                    <?php
                                      $totalbiaya = 0;
                                      foreach ($detailpemberian as $diberi):
                                        $totalbiaya += $diberi->harga;
                                    ?>
                                        <tr>
                                          <td>
                                              <h5 class="content"><?php echo stripslashes($diberi->pemberian); ?></h5>
                                          </td>
                                          <td width="35%">
                                              <h5 class="content" style="text-align: right;">Rp <?php echo number_format($diberi->harga,0,',','.'); ?></h5>
                                          </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <tr>
                                      <td style="text-align: right"><h4 class="content">Total</h4></td>
                                      <td style="text-align: right"><h5 class="content" style="text-align: right;">Rp <?php echo number_format($totalbiaya,0,',','.'); ?></h5></td>
                                      </tr>
                                  </tbody>
                                </table>
                              </div>
                              <div class="btn-feedback-section tooltips" data-placement="right" data-original-title="Berikan komentar / feedback" id="comment-btn-section_fee" section="fee">
                                <i class="fa fa-comment fa-2x" aria-hidden="true"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php // } ?>
                      <?php // if(isset($nextstep) && ($nextstep != NULL || $nextstep != '')){ ?>
                      <!-- Next step -->
                      <div style="<?php echo (isset($nextstep) && ($nextstep != NULL || $nextstep != ''))?'display: block;':'display: none;'; ?>" class="row" id="section_nextstep">
                        <div class="section-padding">
                          <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-4'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-4'; } ?>">
                            <h3 class="caption">Langkah Selanjutnya</h3>
                          </div>
                          <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-8'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8'; } ?> view-project-info">
                            <div  class="mt content" >
                              <h5 class="content"><?php echo stripslashes($nextstep);?></h5>
                            </div>
                            <div class="btn-feedback-section tooltips" data-placement="right" data-original-title="Berikan komentar / feedback" id="comment-btn-section_nextstep" section="nextstep">
                                <i class="fa fa-comment fa-2x" aria-hidden="true"></i>
                              </div>
                          </div>
                        </div>
                      </div>
                      <?php // } ?>
                      <?php // if($detailsyarat && count($detailsyarat) > 0){ ?>
                      <!-- Term and Condition -->
                      <div style="<?php echo ($detailsyarat && count($detailsyarat) > 0)?'display: block;':'display: none;'; ?>" class="row" id="section_term">
                        <div class="section-padding">
                          <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-4'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-4'; } ?>">
                            <h3 class="caption">Syarat dan Ketentuan</h3>
                          </div>
                          <div  class="<?php if(count($style) > 0) { if($style['section_layout'] == 0){ echo 'col-md-8'; }else if($style['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8'; } ?> view-project-info">
                            <div  class="mt">
                              <ol style="-webkit-padding-start: 35px;">
                                <?php foreach ($detailsyarat as $syarat):?>
                                    <li class="content" style="margin-bottom:5px;">
                                        <h5 class="content"><?php echo stripslashes($syarat->syaratketentuan);?></h5>
                                  </li>
                                <?php endforeach; ?>
                              </ol>
                            </div>
                            <div class="btn-feedback-section tooltips" data-placement="right" data-original-title="Berikan komentar / feedback" id="comment-btn-section_term" section="term">
                                <i class="fa fa-comment fa-2x" aria-hidden="true"></i>
                              </div>
                          </div>
                        </div>
                      </div>
                      <?php // } ?>
                          <div class="row" id="ttdSection">
                            <div class="section-padding">
                              <div class="col-md-12">
                                <div class="col-md-6 col-xs-6">
                                  <div style="text-align:center;padding-top:150px;padding-bottom: 50px;">
                                    <div style="height: 80px;font-family: 'pecita';font-size: 35px;text-align: center;vertical-align: middle;border-bottom: solid 1px;"><?php echo $oleh; ?></div>
                                    <p class="content"><?php echo $oleh; ?></p>
                                  </div>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                  <div style="text-align:center;padding-top:150px;padding-bottom: 50px;">
                                    <div style="height: 80px;font-family: 'pecita';font-size: 35px;text-align: center;vertical-align: middle;border-bottom: solid 1px;"><?php echo $sign == 2 ? $klien : ''; ?></div>
                                    <p class="content"><?php echo $klien; ?></p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php //} ?>
                   </div>
                   </div><!--/showback -->
