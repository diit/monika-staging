<?php
ob_start();
$namaprojek = $detail_proposal->info_projek;
$id_klien = $detail_proposal->id_client;
$klien = $detail_proposal->nama_pic;
$perusahaan = $detail_proposal->perusahaan;
if($detail_proposal && ($detail_proposal->aboutyou != NULL || $detail_proposal->aboutyou != ''))
{
    //$aboutyou = nl2br($detail_proposal->aboutyou);
    $aboutyou = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detail_proposal->aboutyou));
}
else
{
    $aboutyou = '';
}
$email_klien = $detail_proposal->email;
$oleh = $detail_proposal->user_name;
if($this->uri->segment(1) == 'userview'){
    $team = '';
} else{
    $team = $detail_proposal->team_name;
}
$kontak = $detail_proposal->user_email;
if($detail_proposal && ($detail_proposal->project_desc != NULL || $detail_proposal->project_desc != ''))
{
    //$overview = nl2br($detail_proposal->project_desc);
    $overview = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detail_proposal->project_desc));
}
else
{
    $overview = '';
}
$pengalaman = $detail_proposal->pengalaman;
if($detail_proposal && ($detail_proposal->nextstep != NULL || $detail_proposal->nextstep != ''))
{
    //$nextstep = nl2br($detail_proposal->nextstep);
    $nextstep = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detail_proposal->nextstep));
}
else
{
    $nextstep = '';
}
$tgl = $detail_proposal->tgl_buat;
if($detailkeuntungan && ($detailkeuntungan->keuntungan != NULL || $detailkeuntungan->keuntungan != ''))
    {
        //$keuntungan = nl2br($detailkeuntungan->keuntungan);
        $keuntungan = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detailkeuntungan->keuntungan));
    }
    else
    {
        $keuntungan = '';
    }
        $duplicate = $detail_proposal->duplicate_from;
        $preview = $detail_proposal->preview;
        $style = $detail_proposal->style;

    function hex2rgb( $colour ) {
            if ( $colour[0] == '#' ) {
                    $colour = substr( $colour, 1 );
            }
            if ( strlen( $colour ) == 6 ) {
                    list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
            } elseif ( strlen( $colour ) == 3 ) {
                    list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
            } else {
                    return false;
            }
            $r = hexdec( $r );
            $g = hexdec( $g );
            $b = hexdec( $b );
            return array($r,$g,$b);
    }

    $headbgrgb = hex2rgb(count($style) > 0 ? $style['headerbgcolor'] :'#656464');
    $txtheadrgb = hex2rgb(count($style) > 0 ? $style['headertextcolor'] :'#ffffff');
$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('Proposal - '.$detail_proposal->info_projek);
// $pdf->SetHeaderMargin(0);
// $pdf->SetTopMargin(18);
// $pdf->SetLeftMargin(16);
// $pdf->SetRightMargin(16);
// $pdf->setFooterMargin(10);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('Monika');
$pdf->SetDisplayMode('real', 'default');
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setPrintHeader(false);
// $pdf->setPrintFooter(false);

$url = base_url();
$pdf->AddPage();

// ================= Header ============================
$pdf->SetTextColor($txtheadrgb['0'], $txtheadrgb['1'], $txtheadrgb['2']);
$pdf->SetFillColor($headbgrgb['0'], $headbgrgb['1'], $headbgrgb['2']);
$fontheaderSM = 12;
$fontheaderBG = 14;
$pdf->SetFont("avenirltstd", '', $fontheaderBG);
$content = $style['logoimageurl'];
$xheader = 90;
$pdf->Ln(3);
$pdf->Cell($xheader);
$test = $pdf->Cell(40,10,$detail_proposal->info_projek,0,1,'L');
$pdf->SetFont("avenirltstd", '', $fontheaderSM);
$pdf->Cell($xheader);
$test .= $pdf->Cell(40,10,'Dipersiapkan Untuk',0,1,'L');
$pdf->SetFont("avenirltstd", '', $fontheaderBG);
$pdf->Cell($xheader);
$test .= $pdf->Cell(40,0,$perusahaan,0,1,'L');
$pdf->SetFont("avenirltstd", '', $fontheaderSM);
$pdf->Cell($xheader);
$test .= $pdf->Cell(40,8,$klien,0,1,'L');
$pdf->SetFont("avenirltstd", '', $fontheaderBG);
$pdf->Cell($xheader);
$test .= $pdf->Cell(40,8,$this->monikalib->format_date_indonesia($tgl),0,1,'L');
$pdf->SetFont("avenirltstd", '', $fontheaderSM);
$pdf->Cell($xheader);
$test .= $pdf->Cell(50,10,'Oleh',0,0,'L');
$test .= $pdf->Cell(40,10,'Kontak',0,1,'L');
$pdf->Cell($xheader);
$test .= $pdf->Cell(50,7,$oleh,0,0,'L');
$test .= $pdf->Cell(40,7,$kontak,0,1,'L');
$pdf->Cell($xheader);
$test .= $pdf->Cell(60,5,$team,0,0,'L');
// Logo team
$html1 = '<img src="'.$content.'" alt="" width="150" border="0">';
$pdf->writeHTMLCell(150,'', 30, 35, $html1, 0, 1,0,true);
// Logo team
$pdf->MultiCell(210, 90, $test, 0, 'L', 1, 1, 0, 0, true);
// ================= Header ============================

// ================= content proposal ==================
$pdf->SetTextColor(0,0,0);
$pdf->Ln(10);
$txt = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
$yContent = $pdf->getY();
$xSection = 15;
$pdf->SetFont("avenirltstd", '', $fontheaderBG);
$pdf->MultiCell(60,10,'Overview', 0, 'L', 0, 1, $xSection, $yContent, true);
$pdf->SetFont("avenirltstd", '', $fontheaderSM);
$pdf->MultiCell(110, 0,$txt, 0, 'L', 0, 1, $xSection+70,$yContent , true);

$pdf->Ln(10);
$yContent2 = $pdf->getY();
$pdf->SetFont("avenirltstd", '', $fontheaderBG);
$pdf->MultiCell(60,10,'Tentang Kami', 0, 'L', 0, 1, $xSection, '', true);
$pdf->SetFont("avenirltstd", '', $fontheaderSM);
$pdf->MultiCell(110, 80,$txt, 0, 'L', 0, 1, $xSection+70,$yContent2 , true);
// ================= content proposal ==================
$pdf->Output('hlah.pdf', 'I');
