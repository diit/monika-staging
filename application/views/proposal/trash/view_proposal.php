<!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->

        <style>
            .input-group .form-control {
                z-index: 0;
            }
        </style>

      <?php if($detail_proposal) { ?>
        <script type="text/javascript">
          var arr_img = [];
        </script>

        <div class="floating-proposal-panel">
          <div style="display: flex;margin-left: auto;-webkit-box-pack: end;justify-content: flex-end;">
            <div class="" style="color: rgb(149, 155, 163);box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 15px;transition: all 0.3s;">
                <h2 class="hidden-xs" style="margin-bottom:20px">PREVIEW</h2>
                <h4 class="hidden-lg hidden-md hidden-sm">PREVIEW</h4>
          </div>
          <div id="link_proposal" class="hidden-xs" style="color: rgb(149, 155, 163);box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;width:250px;padding: 0px 15px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
            <input style="width: 100%;height:30px;border: none;" id="link" type="text" name="" value="<?=$link?>">
          </div>
          <div id="copy_link_btn" style="color: rgb(149, 155, 163);cursor: pointer;box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 20px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
            <span class="hidden-xs hidden-sm"><i class="fa fa-link"></i>  Copy URL</span>
              <i class="fa fa-link hidden-md hidden-lg"></i>
          </div>
          <!-- hidden 25 April, adit -->

          <div id="download_pdf" style="color: rgb(149, 155, 163);cursor: pointer;box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 20px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
              <span class="hidden-xs"><i class="fa fa-cloud-download"></i>  DOWNLOAD PDF</span>
              <i class="fa fa-cloud-download hidden-lg hidden-md hidden-sm"></i>
          </div>

          <?php if ($sign == 2){ ?>
            <div id="status_proposal" style="color: rgb(149, 155, 163);cursor: pointer;box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 20px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
              <span><i class="fa fa-lock"></i>  TELAH DISETUJUI</span>
            </div>
          <?php }else{ ?>
            <div id="send_proposal" style="background-color:#5ee2d4;color: #fff;cursor: pointer;box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 20px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
              <span><i class="fa fa-envelope"></i>  KIRIM</span>
            </div>
          <?php } ?>
        </div>
      </div>
      <?php if($sign < 2){ ?>
                <button type="button" class="btn btn-theme03 btn-setting hidden-lg hidden-md hidden-sm"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></button>
                <div id="setting-proposal">
                    <ul>
                        <li>
                            <label>Header Color</label>
                            <input type="text" class="headerpallete" style="">
                        </li>
                        <li>
                            <label>Header Font Color</label>
                            <input type="text" class="headertextpallete">
                        </li>
                        <li>
                            <label>Font Style</label>
                            <select style="display: inline-block;width: auto; padding: 4px 7px;height: 30px;" class="form-control" id="fontstyle">
                              <option value="Avenir" <?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['fontstyle'] == NULL){ echo 'selected style="font-family: \'Avenir\';"'; }else if($detail_proposal['style']['fontstyle'] == 'Avenir'){ echo 'selected style="font-family: \'Avenir\';"'; }}else{echo 'selected style="font-family: \'Avenir\';"';} ?> >Avenir</option>
                              <option value="Arial" <?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['fontstyle'] == 'Arial'){ echo 'selected style="font-family: Arial;"'; }else{ echo 'style="font-family: Arial;"'; }}else{echo 'style="font-family: Arial;"';} ?>>Arial</option>
                              <option value="Verdana" <?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['fontstyle'] == 'Verdana'){ echo 'selected style="font-family: Verdana;"'; }else{ echo 'style="font-family: Verdana;"'; }}else{echo 'style="font-family: Verdana;"';} ?>>Verdana</option>
                              <option value="Sans-serif" <?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['fontstyle'] == 'sans-serif'){ echo 'selected style="font-family: sans-serif;"'; }else{ echo 'style="font-family: sans-serif;"'; }}else{echo 'style="font-family: sans-serif;"';} ?>>Sans-serif</option>
                              <option value="Open Sans" <?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['fontstyle'] == 'Open Sans'){ echo 'selected style="font-family: Open Sans;"'; }else{ echo 'style="font-family: Open Sans;"'; }}else{echo 'style="font-family: Open Sans;"';} ?>>Open Sans</option>
                            </select>
                        </li>
                    </ul>
                </div>

                <div id="overlay"><h1>cek</h1></div>
      <?php
          }
        }
      ?>
      <section id="main-content" style="<?php echo ($detail_proposal && $sign < 2) ? 'margin-left: 0;margin-right: 180px':'margin-left: 100px;'; ?>;">
        <section style="margin-top: 150px;" class="wrapper">
          <div class="row mt" style="margin:0px">
            <div class="container">
              <div class="col-xs-12">
                <?php if($detail_proposal){ ?>
                    <?php $this->load->view('proposal/template/default',$detail_proposal); // bagian "proposal/template/default" nantinya diganti jd $template_proposal ?>
                <?php }else{ ?>
                  <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="row">
                      <h4>Proposal tidak ditemukan.</h4>
                    </div>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->
<?php if($detail_proposal) { ?>
      <div class="modal" id="modal_send_proposal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                     <div class="modal-content">
                         <div class="modal-header">
                           <h4 class="modal-title text-center" id="" style="font-weight:bold">Kirim Proposal</h4>
                         </div>
                         <div class="modal-body">
                            <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="label-control">Nama Pengirim</label>
                                    <input type="text" class="form-control" value="<?php echo $detail_proposal['oleh']; ?>" id="send_proposal_sender_name">
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="label-control">Email Pengirim</label>
                                    <input type="text" class="form-control" value="<?php echo $detail_proposal['kontak']; ?>" id="send_proposal_sender_email">
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="label-control">Email Klien</label>
                                    <input type="hidden" value="<?php echo $detail_proposal['klien']; ?>" id="send_proposal_client_name">
                                    <input type="text" class="form-control" value="<?php echo $detail_proposal['email_klien']; ?>" data-role="tagsinput" id="send_proposal_client_email">
                                    <span style="font-size: 10px; font-style: italic;">Masukan banyak email menggunakan tanda koma atau tekan enter</span>
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="label-control">CC</label>
                                    <input type="text" class="form-control" value="" data-role="tagsinput" id="send_proposal_client_cc">
                                    <span style="font-size: 10px; font-style: italic;">Masukan banyak email cc menggunakan tanda koma atau tekan enter</span>
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="label-control">Subjek</label>
                                    <input type="text" class="form-control" value="Proposal - <?php echo $detail_proposal['namaprojek']; ?>" id="send_proposal_client_email_subject">
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="label-control">Pesan</label>
                                    <textarea rows="8" class="form-control" id="send_proposal_client_email_message">Dengan Hormat,

Berikut Proposal [...] yang bisa Anda baca.

Klik di sini > <?php echo $link; ?>

Bila ada pertanyaan silahkan langsung tanyakan ke saya dengan balas email ini.

Terima kasih,

<?php echo $detail_proposal['oleh']; ?>

<?php echo $detail_proposal['team']; ?>
                                    </textarea>
                                  </div>
                              </div>
                            </div>
                             <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="button" class="btn btn-info" id="btn-send-proposal"><i class="fa fa-send" aria-hidden="true"></i>
                                  Kirim
                                </button>
                             </div>
                      </div>
                  </div>
                </div>
</div>

      <script type="text/javascript">
          function toTitleCase(str)
          {
            return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
          }

          $(document).ready(function()
          {
            $('.js-example-basic-single').select2({
              dropdownParent: $("#modal_edit_client")
            });

            $('#select_duplicate_client').on('change',function(){
              var client_id = $(this).val();
              //pilihClientLama(client_id);
              $.ajax({
                  type:"GET",
                  url: "<?php echo base_url()?>client/pilihclient",
                  data: {id:client_id},
                  datatype: 'json',
                  success: function(data){
                    var datanya = JSON.parse(data);
                    document.getElementById("client_id").value = client_id;
                    document.getElementById("proposal_project_client").value = datanya.nama;
                    document.getElementById("company_name").value = datanya.perusahaan;
                    document.getElementById("client_email").value = datanya.email;
                    document.getElementById("client_id").value = datanya.id;
                    //$('#klienlama').modal('hide');
                  }, error:function(error){
                    swal('error;' +eval(error));
                  }
                });
            });

            $(document).on('change', '.btn-file1 :file', function() {
            var input = $(this),
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
            });

            $('.btn-file1 :file').on('fileselect', function(event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if( input.length ) {
                    input.val(log);
                    $('.btn-save-edit-logo-image').attr('disabled',false).addClass('btn-success');
                } else {
                    if( log ) alert(log);
                }

            });

            $(document).on('change', '.btn-file2 :file', function() {
            var input = $(this),
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
            });

            $('.btn-file2 :file').on('fileselect', function(event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if( input.length ) {
                    input.val(log);
                    $('.btn-save-edit-header-image').attr('disabled',false).addClass('btn-success');
                } else {
                    if( log ) alert(log);
                }

            });

            $(document).on('change','#fontstyle',function(){
              var getfont = $(this).val();
              $.ajax({
                      method: "POST",
                      url : "<?php echo base_url(); ?>proposal/update_proposal_fontstyle",
                      data : {
                        idproposal:<?php echo $id_projek; ?>,
                        fontstyle: getfont
                      },
                      success : function(data){
                        if(data == 1)
                        {
                          $('.caption').css('font-family',getfont);
                          $('.content').css('font-family',getfont);
                        }
                        else
                        {
                          swal("Oops","Font gagal diubah. Silahkan coba kembali.","error");
                        }
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        swal("Oops","Font gagal diubah. Silahkan coba kembali.","error");
                      }
                    });
            });

            $(document).on('click','.layout-wide',function(){
                    $.ajax({
                      method: "POST",
                      url : "<?php echo base_url(); ?>proposal/update_proposal_layout",
                      data : {
                        idproposal:<?php echo $id_projek; ?>,
                        section_layout: 1
                      },
                      success : function(data){
                        if(data == 1)
                        {
                          $('.section-title').removeClass('col-md-4').addClass('col-md-12');
                          $('.section-content').removeClass('col-md-8').addClass('col-md-12');
                        }
                        else
                        {
                          swal("Oops","Layout gagal diubah. Silahkan coba kembali.","error");
                        }
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        swal("Oops","Layout gagal diubah. Silahkan coba kembali.","error");
                      }
                    });
            });

            $(document).on('click','.layout-split',function(){
              $.ajax({
                      method: "POST",
                      url : "<?php echo base_url(); ?>proposal/update_proposal_layout",
                      data : {
                        idproposal:<?php echo $id_projek; ?>,
                        section_layout: 0
                      },
                      success : function(data){
                        if(data == 1)
                        {
                          $('.section-title').removeClass('col-md-12').addClass('col-md-4');
                          $('.section-content').removeClass('col-md-12').addClass('col-md-8');
                        }
                        else
                        {
                          swal("Oops","Layout gagal diubah. Silahkan coba kembali.","error");
                        }
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        swal("Oops","Layout gagal diubah. Silahkan coba kembali.","error");
                      }
                    });
            });

            <?php if($sign < 2){ ?>
              var style = <?php echo count($detail_proposal['style']); ?>;
              var query = '';
              var headerbgcolor = '<?php echo count($detail_proposal['style']) > 0 ? $detail_proposal['style']['headerbgcolor'] :'#656464'; ?>';
              var headertextcolor = '<?php echo count($detail_proposal['style']) > 0 ? $detail_proposal['style']['headertextcolor'] :'#ffffff'; ?>';
              var headerimageurl = '<?php if(count($detail_proposal['style']) > 0) $imgheader = $detail_proposal['style']['headerimageurl']; else $imgheader = NULL; echo $imgheader; ?>';
              var logoimageurl = '<?php if(count($detail_proposal['style']) > 0) $imglogo = $detail_proposal['style']['logoimageurl']; else $imglogo = NULL; echo $imglogo; ?>';
              var sectionlayout = '<?php echo count($detail_proposal['style']) > 0 ? $detail_proposal['style']['section_layout'] : 0; ?>';
              var fontstyle = '<?php echo count($detail_proposal['style']) > 0 ? $detail_proposal['style']['fontstyle'] :'Avenir'; ?>';


              $(".headerpallete").spectrum({
                  preferredFormat: "hex",
                  color:headerbgcolor,
                  showInput: true,
                  chooseText: 'Pilih',
                  change: function(ty){
                    if(style > 0)
                    {
                      query = 'proposal/update_header_proposal';
                    }
                    else
                    {
                      query = 'proposal/insert_header_proposal';
                    }
                    headerbgcolor = ty.toHexString();
                    $.ajax({
                      method: "POST",
                      url : "<?php echo base_url(); ?>"+query,
                      data : {
                        idproposal:<?php echo $id_projek; ?>,
                        headerbgcolor: ty.toHexString(),
                        headertextcolor: headertextcolor,
                        headerimageurl: headerimageurl,
                        logoimageurl:logoimageurl,
                        section_layout : sectionlayout,
                        fontstyle : fontstyle
                      },
                      success : function(data){
                        if(data == 1)
                        {
                          style = 1;
                          $('#headerinfo').css('background',ty.toHexString());
                        }
                        else
                        {
                          swal("Oops","Warna background header gagal diubah. Silahkan coba kembali.","error");
                        }
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        swal("Oops","Warna background header gagal diubah. Silahkan coba kembali.","error");
                      }
                    });
                  }
              });

              var t = $(".headerpallete").spectrum("get");
              t.toHexString(); // "#ff0000"

              $('.headertextpallete').spectrum({
                preferredFormat: "hex",
                  color:headertextcolor,
                  showInput: true,
                  chooseText: 'Pilih Warna Text',
                  change: function(ty){
                    if(style > 0)
                    {
                      query = 'proposal/update_header_proposal';
                    }
                    else
                    {
                      query = 'proposal/insert_header_proposal';
                    }
                    headertextcolor = ty.toHexString();
                    $.ajax({
                      method: "POST",
                      url : "<?php echo base_url(); ?>"+query,
                      data : {
                        idproposal:<?php echo $id_projek; ?>,
                        headerbgcolor: headerbgcolor,
                        headertextcolor: ty.toHexString(),
                        headerimageurl: headerimageurl,
                        logoimageurl:logoimageurl,
                        section_layout : sectionlayout,
                        fontstyle : fontstyle
                      },
                      success : function(data){
                        if(data == 1)
                        {
                          style = 1;
                          $('#headerinfo').css('color',ty.toHexString());
                        }
                        else
                        {
                          swal("Oops","Warna text header gagal diubah. Silahkan coba kembali.","error");
                        }
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        swal("Oops","Warna text header gagal diubah. Silahkan coba kembali.","error");
                      }
                    });
                  }
              });

            <?php } ?>

              <?php if($detailtambahan && count($detailtambahan) > 0){ ?>
                <?php foreach ($detailtambahan as $tambahan) { ?>
                  var section_id = '<?php echo $tambahan->section_id; ?>';
                  var section_name = '<?php echo preg_replace("/[^A-Za-z0-9 ]/", "",preg_replace("/\r|\n/","",$tambahan->section_name)); ?>';
                  var section_name_show = '<?php echo preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","",$tambahan->section_name)); ?>';
                  var insert_after_pos = '<?php echo $tambahan->section_pos; ?>';
                  var content_section_detail = '';
                  var arr_img_url = [];
                  <?php if($tambahan->section_type == 'text'){ ?>
                    <?php if(count($tambahan->detail)>0){ ?>
                      <?php foreach ($tambahan->detail as $dt) { ?>
                        content_section_detail += '<h5 class="content" id="info_'+section_name.toLowerCase().replace(/ /g, "_")+'"><?php echo preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$dt->section_detail_content)); ?></h5>';
                      <?php } ?>
                    <?php } ?>
                  <?php }else if($tambahan->section_type == 'image'){ ?>
                    <?php if(count($tambahan->detail)>0){ ?>
                      <?php foreach ($tambahan->detail as $dt) { ?>
                        content_section_detail += '<div class="col-lg-6 col-md-6"><img class="img-responsive" src="<?php echo $dt->section_detail_content; ?>"></div>';
                        arr_img_url.push({'url' : '<?php echo $dt->section_detail_content; ?>','section_detail_id':<?php echo $dt->section_detail_id; ?>,'section_id' : <?php echo $dt->section_id; ?>});
                      <?php } ?>
                    <?php } ?>
                    arr_img.push({section_id : section_id,section_name : section_name, image : arr_img_url});
                    //arr_img[section_name] = arr_img_url;
                  <?php } ?>
                  <?php if($sign < 2){ ?>
                    var new_section_pos = $('<div class="row section new_section" id="section_'+ section_name.toLowerCase().replace(/ /g, "_") +'"><div class="section-padding"><div  class="<?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['section_layout'] == 0){ echo 'col-md-4'; }else if($detail_proposal['style']['section_layout'] == 1){ echo 'col-md-12'; }}else{echo 'col-md-4';} ?> section-title"><h3 class="caption" id="title_'+section_name.toLowerCase().replace(/ /g, "_")+'">'+ section_name_show +'</h3></div><div  class="<?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['section_layout'] == 0){ echo 'col-md-8'; }else if($detail_proposal['style']['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8';} ?> section-content view-project-info"><div  class="mt content" >'+ content_section_detail +'</div><div class="btn-edit-section tooltips" data-placement="right" data-original-title="Edit" id="edit-btn-section_'+section_name.toLowerCase().replace(/ /g, "_")+'" sid="'+section_id+'" section="'+section_name.toLowerCase().replace(/ /g, "_")+'" sectype="<?php echo $tambahan->section_type; ?>"><i class="fa fa-pencil"></i></div><div class="btn-remove-section tooltips" data-placement="right" data-original-title="Hapus" id="remove-btn-section_'+section_name.toLowerCase().replace(/ /g, "_")+'" sid="'+section_id+'" section="'+section_name.toLowerCase().replace(/ /g, "_")+'" sectype="<?php echo $tambahan->section_type; ?>"><i class="fa fa-trash"></i></div></div><div class="col-md-12 panel-btn-add" id="add-btn-section_'+section_name.toLowerCase().replace(/ /g, "_")+'"><div style="text-align: center; margin-top: 20px; border-top: solid 1px #d8d8d8;font-size: 25px; color: #d8d8d8;"><div style="margin-top: -15px;"><i class="fa fa-align-left btn-add-section tooltips" data-placement="bottom" data-original-title="tambah konten teks"  section="'+section_name.toLowerCase().replace(/ /g, "_")+'" style="cursor:pointer;"></i>&nbsp;<i class="fa fa-image btn-add-image tooltips" data-placement="bottom" data-original-title="tambah konten gambar" section="'+section_name.toLowerCase().replace(/ /g, "_")+'" style="cursor:pointer;"></i></div></div></div></div></div>');
                  <?php }else{ ?>
                    var new_section_pos = $('<div class="row section new_section" id="section_'+ section_name.toLowerCase().replace(/ /g, "_") +'"><div class="section-padding"><div  class="<?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['section_layout'] == 0){ echo 'col-md-4'; }else if($detail_proposal['style']['section_layout'] == 1){ echo 'col-md-12'; }}else{echo 'col-md-4';} ?> section-title"><h3 class="caption" id="title_'+section_name.toLowerCase().replace(/ /g, "_")+'">'+ section_name_show +'</h3></div><div  class="<?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['section_layout'] == 0){ echo 'col-md-8'; }else if($detail_proposal['style']['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8';} ?> section-content view-project-info"><div  class="mt content" >'+ content_section_detail +'</div></div></div></div>');
                  <?php } ?>
                      new_section_pos.insertAfter($('#'+insert_after_pos));
                <?php } ?>
              <?php } ?>

            <?php if($this->monikalib->getTotalProposal() == 0 && $preview == 1){ ?>
              var intro = introJs();
              intro.setOptions({
                exitOnOverlayClick: false,
                nextLabel: 'Selanjutnya',
                prevLabel: 'Sebelumnya',
                doneLabel: 'Selesai',
                disableInteraction: true,
                showStepNumbers:false,
                steps: [
                  {
                    intro: "Ini adalah halaman preview Proposal yang telah Anda buat. Sebelum dikirimkan kepada klien, Anda bisa melakukan sejumlah pengeditan isi atau menambahkan konten proposal."
                  },
                  {
                    element: '#add-btn-section_team',
                    intro: "Untuk menambah konten, arahkan mouse Anda ke baris salah satu konten"
                  },
                  {
                    element: '#icn-add-text',
                    intro: "Ini untuk menambahkan konten teks"
                  },
                  {
                    element: '#icn-add-image',
                    intro: "Dan ini untuk menambahkan konten gambar"
                  },
                  {
                    element: '#edit-btn-section_team',
                    intro: "Untuk mengubah konten, arahkan mouse Anda ke baris salah satu konten lalu klik icon pensil"
                  },
                  {
                    element: '#remove-btn-section_team',
                    intro: "Untuk mengahpus konten, arahkan mouse Anda ke baris salah satu konten lalu klik icon tempat sampah"
                  },
                  {
                    element: '#rightbar',
                    intro: "Ubah style proposal Anda di sini"
                  },
                  {
                    element: '#link_proposal',
                    intro: "Bagikan link proposal kepada klien Anda dengan copy di sini."
                  },
                  {
                    element: '#copy_link_btn',
                    intro: "Atau klik di sini untuk copy link proposal"
                  },
                  /*{
                    element: '#download_pdf',
                    intro: "Anda bisa men-download proposal dalam format PDF dengan menekan tombol ini"
                  },
                  */
                  {
                    element: '#<?php echo $sign == 2 ?'status_proposal':'send_proposal'; ?>',
                    intro: "<?php echo $sign == 2 ?'Status proposal yang sudah dikirimkan kepada klien bisa Anda lihat pada bagian ini. Status apakah sudah dibaca dan sudah disetujui.':'Ingin langsung mengirimkan proposal ke email klien? Klik di sini.'; ?>"
                  }
                ]
               });

              intro.onchange(function() {
                if($('.floating-proposal-panel').css('position') === 'fixed')
                {
                  $('.floating-proposal-panel').css('position','absolute');
                }

                $('#edit-btn-section_team').css('visibility','visible');
                $('#remove-btn-section_team').css('visibility','visible');
                $('#add-btn-section_team').css('visibility','visible');
                          /*
                          // Find the parent having the introjs-fixParent class
                          $parentElement = $(targetElement).parents(".introjs-fixParent");

                          // Check if the parent has the position fixed
                          if ($parentElement.css("position") === "fixed") {

                              // Change the position to absolute
                              $parentElement.css("position", "absolute");
                          }
                          */
              });

              intro.onexit(function(){
                $('.floating-proposal-panel').css('position','fixed');
                $('#edit-btn-section_team').css('visibility','hidden');
                $('#remove-btn-section_team').css('visibility','hidden');
                $('#add-btn-section_team').css('visibility','hidden');
              });

              intro.start();
            <?php } ?>

              $('#link').on('click',function(){
                $(this).select();
              });

              $('#copy_link_btn').on('click',function(){
                var copyText = document.getElementById("link");
                copyText.select();
                document.execCommand("Copy");
                $.ajax({
                      method: "POST",
                      url : "<?php echo base_url();?>proposal/log_copy_url",
                      data : {
                        proposal_id: <?php echo $id_projek; ?>
                      },
                      success : function(data){
                        if(data == 1)
                        {
                          swal("Perhatian","Kami tidak merekomendasikan Anda mengirimkan proposal ini menggunakan cara selain menggunakan tombol KIRIM di kanan atas. Bila Anda tetap menginginkan, silahkan bagikan link URL ini.","warning");
                        }
                        else
                        {
                          swal("Oops","Link gagal disalin","error");
                        }
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        swal("Oops","Link gagal disalin","error");
                      }
                    });
              });

              $('#send_proposal').on('click',function(){
                <?php if($this->session->userdata('user_status') == 1){ ?>
                  $('#modal_send_proposal').modal('show');
                <?php }else{ ?>
                  swal("Aktifkan Akun Anda","Untuk mengirim proposal, silahkan aktifkan akun Anda terlebih dahulu melalui link yang sudah kami kirimkan ke email Anda. Bila Anda tidak menerima email, silahkan hubungi Customer Service","info");
                <?php } ?>
              });

              $('#project_name_con').on('mouseover',function(){
                $('.btn-edit-title').css('visibility','visible');
              });

              $('#project_name_con').on('mouseout',function(){
                $('.btn-edit-title').css('visibility','hidden');
              });

              $('.contact-info').on('mouseover',function(){
                $('.btn-edit-email').css('visibility','visible');
              });

              $('.contact-info').on('mouseout',function(){
                $('.btn-edit-email').css('visibility','hidden');
              });

              $('.btn-edit-title').on('click',function(){
                $('#proposal_project_title').val($('#project_name').text());
                $('#modal_edit_title').modal('show');
              });

              $('.btn-edit-email').on('click',function(){
                $('#proposal_contact_email').val('<?php echo $detail_proposal['kontak']; ?>');
                $('#modal_edit_email').modal('show');
              });

              $('.btn-choose-client').on('click',function(){
                $('#klienlama').modal('show');
              });

              $('.row_client').on('click',function(){
                var id_client = $(this).attr('clid');
                pilihClientLama(id_client);
              });
              $('#client_name_con').on('mouseover',function(){
                $('.btn-edit-client').css('visibility','visible');
              });

              $('#client_name_con').on('mouseout',function(){
                $('.btn-edit-client').css('visibility','hidden');
              });

              $('.btn-edit-client').on('click',function(){
                <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
                  $('#proposal_project_client').val($('#pic_name').text());
                  $('#modal_edit_client').modal('show');
                <?php }else{ ?>
                  <?php if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1){ ?>
                    $('#proposal_project_client').val($('#pic_name').text());
                    $('#modal_edit_client').modal('show');
                  <?php }else{ ?>
                    <?php if($this->monikalib->getTotalProposal() >= 2 + $this->monikalib->getTotalReferral()){ ?>
                      $('#modal_referral').modal('show');
                    <?php }else{ ?>
                      $('#proposal_project_client').val($('#pic_name').text());
                      $('#modal_edit_client').modal('show');
                    <?php } ?>
                  <?php } ?>
                <?php } ?>
              });

              $('.btn-save-edit-title').on('click',function(){
                    $.ajax({
                      method: "POST",
                      url : "<?php echo base_url();?>proposal/update_project_title",
                      data : {
                        proposal_id:$('#prop_id').val(),
                        project_title:$('#proposal_project_title').val()
                      },
                      success : function(data){
                        if(data == 1)
                        {
                          swal('Judul berhasil diubah');
                          $('#modal_edit_title').modal('hide');
                          location.reload();
                        }
                        else
                        {
                          swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                        }
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                      }
                    });
              });

              $('.btn-save-edit-email').on('click',function(){
                    if($('#proposal_contact_email').val() == '')
                    {
                      swal("Oops","Silahkan isi email baru yang ingin ditampilkan di proposal ini","error");
                      return false;
                    }
                    else if(!ValidateEmail($('#proposal_contact_email').val()))
                    {
                      swal("Oops","Pastikan format email benar","error");
                      return false;
                    }
                    else
                    {
                      $.ajax({
                        method: "POST",
                        url : "<?php echo base_url();?>proposal/update_contact_email",
                        data : {
                          proposal_id:$('#prop_id').val(),
                          contact_email:$('#proposal_contact_email').val()
                        },
                        success : function(data){
                          if(data == 1)
                          {
                            swal('Email di proposal ini berhasil diubah');
                            $('#modal_edit_email').modal('hide');
                            location.reload();
                          }
                          else
                          {
                            swal("Oops","Email gagal diubah. Silahkan coba kembali.","error");
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          swal("Oops","Email gagal diubah. Silahkan coba kembali.","error");
                        }
                      });
                    }
              });

              $('.btn-save-edit-client').on('click',function(){
                    $.ajax({
                      method: "POST",
                      url : "<?php echo base_url();?>proposal/update_project_client",
                      data : {
                        proposal_id:$('#prop_id').val(),
                        client_name:$('#proposal_project_client').val(),
                        company_name:$('#company_name').val(),
                        client_email:$('#client_email').val(),
                        client_id:$('#client_id').val()
                      },
                      success : function(data){
                        if(data == 1)
                        {
                          swal('Klien berhasil diubah');
                          $('#modal_edit_client').modal('hide');
                          location.reload();
                        }
                        else
                        {
                          swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                        }
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                      }
                    });
              });

              $(document).on('mouseover','.section-header',function(){
                $('#form_upload_logo_image_edit,.form_upload_header_image_edit,.delete-bg,.delete-logo,.btn-team-logo,.capt-team-logo').css('visibility','visible');
              });

              $(document).on('mouseout','.section-header',function(){
                $('#form_upload_logo_image_edit,.form_upload_header_image_edit,.delete-bg,.delete-logo,.btn-team-logo,.capt-team-logo').css('visibility','hidden');
              });

              <?php if($team_logo && $team_logo->team_logo != NULL){ ?>
                $(document).on('click','.btn-team-logo',function(){
                  swal({
                      title: "Gunakan Logo Usaha",
                      text: "Anda akan menggunakan logo usaha pada proposal ini. Lanjutkan?",
                      icon: "info",
                      buttons: true,
                      dangerMode: true,
                    })
                    .then((kirim) => {
                          if (kirim) {
                             $.ajax({
                              method: "POST",
                              url : "<?php echo base_url();?>proposal/get_team_logo",
                              data : {
                                proposal_id:<?php echo $id_projek; ?>,
                                team_logo:'<?php echo $team_logo->team_logo; ?>'
                                  },
                                  success : function(data){
                                    if(data == 1)
                                    {
                                      swal('Berhasil menyimpan logo');
                                      location.reload();
                                    }
                                    else
                                    {
                                      swal("Oops","Ada kesalahan. Silahkan coba kembali.","error");
                                    }
                                  },
                                  error: function (jqXHR, textStatus, errorThrown)
                                  {
                                    swal("Oops","Ada kesalahan. Silahkan coba kembali.","error");
                                  }
                                });
                          } else {
                            close();
                          }
                    });
                });
              <?php } ?>

              $(document).on('click','.delete-logo',function(){
                $.ajax({
                      method: "POST",
                      url : "<?php echo base_url();?>proposal/delete_logo",
                      data : {
                        proposal_id:<?php echo $id_projek; ?>
                      },
                      success : function(data){
                        if(data == 1)
                        {
                          swal('Logo berhasil dihapus');
                          location.reload();
                        }
                        else
                        {
                          swal("Oops","Logo gagal dihapus. Silahkan coba kembali.","error");
                        }
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        swal("Oops","Logo gagal dihapus. Silahkan coba kembali.","error");
                      }
                    });
              });

              $(document).on('click','.delete-bg',function(){
                $.ajax({
                      method: "POST",
                      url : "<?php echo base_url();?>proposal/delete_background",
                      data : {
                        proposal_id:<?php echo $id_projek; ?>
                      },
                      success : function(data)
                      {
                        if(data == 1)
                        {
                          swal('Background berhasil dihapus');
                          location.reload();
                        }
                        else
                        {
                          swal("Oops","Background gagal dihapus. Silahkan coba kembali.","error");
                        }
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        swal("Oops","Background gagal dihapus. Silahkan coba kembali.","error");
                      }
                    });
              });

              $(document).on('mouseover','.section',function(){
                $('#add-btn-'+$(this).attr('id')).css('visibility','visible');
                $('#edit-btn-'+$(this).attr('id')).css('visibility','visible');
                $('#remove-btn-'+$(this).attr('id')).css('visibility','visible');
              });

              $(document).on('mouseout','.section',function(){
                $('#add-btn-'+$(this).attr('id')).css('visibility','hidden');
                $('#edit-btn-'+$(this).attr('id')).css('visibility','hidden');
                $('#remove-btn-'+$(this).attr('id')).css('visibility','hidden');
              });

              $(document).on('click','.btn-add-section',function(){
                $('#position_after').val('section_'+$(this).attr('section'));
                $('#new_content_title').val('');
                $('#new_content_detail').val('');
                $('#modal_add_section').modal('show');
              });

              $(document).on('click','.btn-add-image',function(){
                $('#position_image_after').val('section_'+$(this).attr('section'));
                $('#new_content_image_title').val('');
                //$('#new_content_image_detail').val('');
                $('#modal_add_image').modal('show');
              });

              $(document).on('click','.btn-remove-section',function(){
                var section = $(this).attr('section');
                swal({
                    title: toTitleCase(section.replace("_"," "))+" di proposal akan dihapus",
                    text: "Apakah Anda yakin?",
                    icon: "info",
                    buttons: true,
                    dangerMode: true,
                  })
                  .then((kirim) => {
                    if(kirim)
                    {
                      $.ajax({
                        type: "POST",
                        url: '<?=base_url()?>proposal/remove_section',
                        data: {
                            'section' : section,
                            'proposal_id' : <?php echo $id_projek; ?>
                          },
                        success: function(dt)
                        {
                          //console.log(dt);
                          if(dt == 1)
                          {
                            location.reload();
                          }
                          else
                          {
                            swal("Waw","Gagal hapus konten. Silahkan coba kembali.","error");
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          swal("Oops","Gagal hapus konten. Silahkan coba kembali.","error");
                        }
                      });
                    }
                  });
              });

              $(document).on('click','.btn-edit-section',function(){
                var section = $(this).attr('section');
                if($(this).attr('sectype') != undefined)
                {
                  var section_type = $(this).attr('sectype');
                }
                var title_text = $('#title_'+section).text();
                if(section == 'overview' ||section == 'aboutme' || section == 'benefit' || section == 'nextstep' || ($(this).attr('sectype') != undefined && section_type == 'text'))
                {
                  var info_text = $('#info_'+section).html().replace(/<br?>/gi,'\n');
                  //var info_text = $('#info_'+section).text();
                    $('#title_edit_content_text').val(title_text);
                    $('#info_edit_content_text').val(info_text);
                    $('#edited_sid').val($(this).attr('sid'));
                    $('.btn-save-edit-section').attr('section',section);
                  if(section == 'aboutme')
                  {
                    $('#link_add_saved_content').attr('section',section).html('<i class="fa fa-list"></i> Ubah dengan data '+ title_text +' yang tersimpan sebelumnya');
                  }
                  else
                  {
                    $('#link_add_saved_content').attr('section',section).html('');
                  }
                  $('#modal_edit_section_text').modal('show');
                }
                else if(section == 'team' || section == 'team_value' || section == 'milestone' || section == 'term' || ($(this).attr('sectype') != undefined && section_type == 'list'))
                {
                  $('#section_content_list').html('');
                  $('#title_edit_content_list').text(title_text);
                  $('#edited_sid').val($(this).attr('sid'));
                  $('.btn-save-edit-section').attr('section',section);
                  <?php if($detail_proposal['duplicate'] > 0 || $detail_proposal['klien'] == 'John Doe'){ ?>
                    if(section == 'team' || section == 'team_value')
                    {
                      $('#link_add_saved_content').attr('section',section).html('<i class="fa fa-list"></i> Tambahkan '+ title_text +' yang tersimpan');
                    }
                  <?php } ?>
                  var div_2 = '<div class="form-group row text-left">';
                  if(section == 'team_value')
                  {
                    div_2 += '<div class="listSkill">';
                    <?php if($detailkemampuan || count($detailkemampuan) > 0){ ?>
                      <?php $ct = count($detailkemampuan)-1; foreach ($detailkemampuan as $key => $userskill){ ?>
                          div_2 += '<div id="divadd_<?php echo $key; ?>">';
                          div_2 += '<input style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" id="hasiladdskill" name="skills" type="text" value="<?php echo addslashes($userskill->skill); ?>" class="form-control hasiladdskill">';
                          <?php if($key==0){ ?>
                            div_2 += '<button type="button" style="margin-bottom: 10px;" class="btn btn-default btn-sm addBtn addKemampuan"><i class="fa fa-plus"></i></button>';
                          <?php }else{ ?>
                            div_2 += '<button ctid="<?php echo $key; ?>" style="margin-bottom: 10px;" class="btn btn-sm btn-default btn-remove-skill"><i class="fa fa-trash"></i></button>';
                          <?php } ?>
                          div_2 += '</div>';
                      <?php } ?>
                    <?php }else{ $ct = 0; ?>
                      div_2 += '<input type="text" style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" class="form-control inpskill" name="skills" id="skills1" placeholder="Kemampuan Anda">';
                      div_2 += '<button type="button" style="margin-bottom:10px;" class="btn btn-default btn-sm addBtn addKemampuan"><i class="fa fa-plus"></i></button>';
                    <?php } ?>
                    div_2 += '</div>';
                    div_2 += '</div>';
                    $(div_2).appendTo($('#section_content_list'));
                    $('#modal_edit_section_list').modal('show');
                  }
                  else if(section == 'team')
                  {
                    div_2 += '<div class="listPengalaman">';
                    <?php if($detail_proposal['pengalaman'] || count($detail_proposal['pengalaman']) > 0){ ?>
                      <?php $th = count($detail_proposal['pengalaman'])-1; foreach ($detail_proposal['pengalaman'] as $key => $userportfolio){ ?>
                          div_2 += '<div id="divaddpengalaman_<?php echo $key; ?>">';
                          div_2 += '<input style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" id="hasiladdskill" type="text" class="form-control" name="pengalaman" id="inputpengalaman<?php echo $key; ?>" value="<?php echo addslashes($userportfolio['pengalaman']); ?>" placeholder="Pengalaman Anda">';
                          <?php if($key==0){ ?>
                            div_2 += '<button type="button" style="margin-bottom: 10px;" class="btn btn-default btn-sm addPengalaman"><i class="fa fa-plus"></i></button>';
                          <?php }else{ ?>
                            div_2 += '<button ctid="<?php echo $key; ?>" style="margin-bottom: 10px;" class="btn btn-sm btn-default btn-remove-pengalaman"><i class="fa fa-trash"></i></button>';
                          <?php } ?>
                          div_2 += '</div>';
                      <?php } ?>
                    <?php }else{ $th = 0; ?>
                      div_2 += '<input type="text" style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" class="form-control" name="pengalaman" id="inputpengalaman1" placeholder="Pengalaman Anda">';
                      div_2 += '<button type="button" style="margin-bottom:10px;" class="btn btn-default btn-sm addPengalaman"><i class="fa fa-plus"></i></button>';
                    <?php } ?>
                    div_2 += '</div>';
                    div_2 += '</div>';
                    $(div_2).appendTo($('#section_content_list'));
                    $('#modal_edit_section_list').modal('show');
                  }
                  else if(section == 'milestone')
                  {
                    div_2 += '<div class="listTahapan">';
                    <?php if(count($detailtahapan) > 0){ ?>
                      <?php $ch = count($detailtahapan)-1; foreach ($detailtahapan as $key=>$tahapan){ ?>
                          div_2 += '<div id="divaddtahapan_<?php echo $key; ?>">';
                          div_2 += '<input style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" type="text" class="form-control inptahapan" id="tahapan<?php echo $key; ?>" name="tahapan" placeholder="Tahapan kerja" value="<?php echo addslashes($tahapan->tahapan); ?>">';
                          <?php if($key==0){ ?>
                            div_2 += '<button type="button" style="margin-bottom: 10px;" class="btn btn-default btn-sm addThpnBtn"><i class="fa fa-plus"></i></button>';
                          <?php }else{ ?>
                            div_2 += '<button chid="<?php echo $key; ?>" style="margin-bottom: 10px;" class="btn btn-sm btn-default btn-remove-tahapan"><i class="fa fa-trash"></i></button>';
                          <?php } ?>
                          div_2 += '</div>';
                      <?php } ?>
                    <?php }else{ $ch = 0; ?>
                      div_2 += '<input type="text" style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" class="form-control" name="tahapan" id="tahapan1" placeholder="Pengalaman Anda">';
                      div_2 += '<button type="button" style="margin-bottom: 10px;" class="btn btn-default btn-sm addThpnBtn"><i class="fa fa-plus"></i></button>';
                    <?php } ?>
                    div_2 += '</div>';
                    div_2 += '</div>';
                    $(div_2).appendTo($('#section_content_list'));
                    $('#modal_edit_section_list').modal('show');
                  }
                  else if(section == 'term')
                  {
                    div_2 += '<div class="listTerm">';
                    <?php if(count($detailsyarat) > 0){ ?>
                      <?php $ctr = count($detailsyarat)-1; foreach ($detailsyarat as $key=>$syarat){ ?>
                          div_2 += '<div id="divaddterm_<?php echo $key; ?>">';
                          div_2 += '<input style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" type="text" class="form-control inptahapan" id="terms<?php echo $key; ?>" name="syarat" placeholder="Tahapan kerja" value="<?php echo addslashes($syarat->syaratketentuan); ?>">';
                          <?php if($key==0){ ?>
                            div_2 += '<button type="button" style="margin-bottom: 10px;" class="btn btn-default btn-sm addBtnTerm"><i class="fa fa-plus"></i></button>';
                          <?php }else{ ?>
                            div_2 += '<button ctrid="<?php echo $key; ?>" style="margin-bottom: 10px;" class="btn btn-sm btn-default btn-remove-term"><i class="fa fa-trash"></i></button>';
                          <?php } ?>
                          div_2 += '</div>';
                      <?php } ?>
                    <?php }else{ $ctr = 0; ?>
                      div_2 += '<input type="text" style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" class="form-control" name="syarat" id="terms1" placeholder="Pengalaman Anda">';
                      div_2 += '<button type="button" style="margin-bottom: 10px;" class="btn btn-default btn-sm addBtnTerm"><i class="fa fa-plus"></i></button>';
                    <?php } ?>
                    div_2 += '</div>';
                    div_2 += '</div>';
                    $(div_2).appendTo($('#section_content_list'));
                    $('#modal_edit_section_list').modal('show');
                  }
                }
                else if(section == 'fee' || ($(this).attr('sectype') != undefined && section_type == 'tabel'))
                {
                  $('#section_content_tabel').html('');
                  $('#title_edit_content_tabel').text(title_text);
                  $('.btn-save-edit-section').attr('section',section);
                  var div_2 = '<div class="form-group text-left">';
                  div_2 += '<table style="width:100%;"><thead><th>Pekerjaan</th><th>Biaya</th></thead>'
                  div_2 += '<tbody id="listDiberi">';
                    <?php if(count($detailpemberian) > 0){ ?>
                      <?php $cb = count($detailpemberian)-1; foreach ($detailpemberian as $key=>$diberi){ ?>
                          div_2 += '<tr id="traddbiaya_<?php echo $key; ?>">';
                          div_2 += '<td><input style="width: 80%;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" class="form-control" type="text" id="diberi<?php echo $key; ?>" name="diberi" placeholder="Nama pekerjaan" value="<?php echo addslashes($diberi->pemberian); ?>"></td>';
                          div_2 += '<td><span style="float: left;padding-top: 10px;">Rp</span><input class="form-control" type="text" style="width: 80%;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="biaya" placeholder="Biaya" value="<?php echo $diberi->harga; ?>" id="biaya<?php echo $key; ?>" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>';
                          <?php if($key==0){ ?>
                            div_2 += '<td><button type="button" class="btn btn-default btn-sm addYgDiberiBtn"><i class="fa fa-plus" aria-hidden="true"></i></button></td>';
                          <?php }else{ ?>
                              div_2 += '<td><button cbid="<?php echo $key; ?>" type="button" class="btn btn-default btn-sm btn-remove-biaya"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                          <?php } ?>
                          div_2 += '</tr>';
                      <?php } ?>
                    <?php }else{ $cb = 0; ?>
                      div_2 += '<tr>';
                      div_2 += '<td><input style="width: 80%;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" class="form-control" type="text" name="diberi" placeholder="Nama pekerjaan" id="diberi"></td>';
                      div_2 += '<td><span style="float: left;padding-top: 10px;">Rp</span><input class="form-control" style="width: 80%;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" type="text" name="biaya" placeholder="Biaya" id="biaya" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></td>';
                      div_2 += '<td><button type="button" class="btn btn-default btn-sm addYgDiberiBtn"><i class="fa fa-plus" aria-hidden="true"></i></button></td>';
                      div_2 += '</tr>';
                    <?php } ?>
                  div_2 += '</tbody></table></div>';
                  $(div_2).appendTo($('#section_content_tabel'));
                  $('#modal_edit_section_tabel').modal('show');
                }
                else if($(this).attr('sectype') != undefined && section_type == 'image')
                {
                  $('#section_content_tabel').html('');
                  $('#edited_images').html('');
                  $('#title_edit_content_image').text(title_text);
                  $('.btn-save-edit-section').attr('section',section);
                  var container_edit_image = '';
                  for (var i = 0; i < arr_img.length; i++) {
                    if(arr_img[i].section_name.trim() == title_text.trim())
                    {
                      console.log(arr_img[i]);
                      if(arr_img[i].image.length == 1)
                      {
                        var yay = arr_img[i].image[0];
                        container_edit_image = '<div id="div_img_'+ yay.section_detail_id +'" class="col-lg-6 col-md-6"><img class="img-responsive" src="'+yay.url+'"><i class="fa fa-close btn-remove-img" section_id="'+yay.section_id+'" imag="'+yay.section_detail_id+'" style="position:absolute;top:2px;right:10px;color:red;cursor:pointer;"></i></div>';
                        container_edit_image += '<div class="row"><div style="margin-top:10px;" class="col-md-12 col-lg-12"><label class="label-control">Tambah Gambar</label><form action="<?php echo base_url(); ?>proposal/update_image_additional" id="form_upload_image_edit" enctype="multipart/form-data" method="post" accept-charset="utf-8">';
                            container_edit_image += '<input type="hidden" name="section_id" value="'+arr_img[i].section_id+'">';
                            container_edit_image += '<input type="hidden" name="prop_id" value="<?php echo $id_projek; ?>">';
                            for (var j = 1; j <= 2; j++) {
                              container_edit_image += '<div class="col-md-12">';
                              container_edit_image += '<span style="margin:5px;position: relative;overflow: hidden;display: inline-block;" class="btn btn-success fileinput-button">';
                              container_edit_image += '<i class="fa fa-image"></i><span>Select files...</span>';
                              container_edit_image += '<input style="position: absolute;top: 0;right: 0;margin: 0;opacity: 0;-ms-filter:font-size: 200px !important;direction: ltr;cursor: pointer;" id="fileupload" img="'+i+'" type="file" name="file[]" multiple="">';
                              container_edit_image += '</span>';
                              container_edit_image += '<span id="file_name_'+i+'">asdasd</span>';
                              container_edit_image += '</div>';
                            }
                        container_edit_image += '</form></div></div>';
                      }
                      else if(arr_img[i].image.length > 1)
                      {
                        for (var j = 0; j < arr_img[i].image.length; j++) {
                          container_edit_image += '<div id="div_img_'+ arr_img[i].image[j].section_detail_id +'" class="col-lg-6 col-md-6"><img class="img-responsive" src="'+arr_img[i].image[j].url+'"><i class="fa fa-close btn-remove-img" section_id="'+ arr_img[i].image[j].section_id +'" imag="'+arr_img[i].image[j].section_detail_id+'" style="position:absolute;top:2px;right:10px;color:red;cursor:pointer;"></i></div>';
                        }
                        if(arr_img[i].image.length < 3)
                        {
                          container_edit_image += '<div class="row"><div style="margin-top:10px;" class="col-md-12 col-lg-12"><label class="label-control">Tambah Gambar</label><form action="<?php echo base_url(); ?>proposal/update_image_additional" id="form_upload_image_edit" enctype="multipart/form-data" method="post" accept-charset="utf-8">';
                            container_edit_image += '<input type="hidden" name="section_id" value="'+arr_img[i].section_id+'">';
                            container_edit_image += '<input type="hidden" name="prop_id" value="<?php echo $id_projek; ?>">';
                            for (var j = 1; j <= 3 - arr_img[i].image.length; j++) {
                              container_edit_image += '<div class="col-md-12">';
                              container_edit_image += '<span style="margin:5px;position: relative;overflow: hidden;display: inline-block;" class="btn btn-success fileinput-button">';
                              container_edit_image += '<i class="fa fa-image"></i><span>Select files...</span>';
                              container_edit_image += '<input style="position: absolute;top: 0;right: 0;margin: 0;opacity: 0;-ms-filter:font-size: 200px !important;direction: ltr;cursor: pointer;" id="fileupload" img="'+i+'" type="file" name="file[]" multiple="">';
                              container_edit_image += '</span>';
                              container_edit_image += '<span id="file_name_'+i+'">asdasd</span>';
                              container_edit_image += '</div>';
                            }
                          container_edit_image += '</form></div></div>';
                          $('.btn-save-edit-image').css('display','inline-block');
                        }
                        else
                        {
                          $('.btn-save-edit-image').css('display','none');
                        }
                      }
                      else
                      {
                        var container_edit_image = '<form action="<?php echo base_url(); ?>proposal/update_image_additional" id="form_upload_image_edit" enctype="multipart/form-data" method="post" accept-charset="utf-8">';
                        for (var i = 0; i < arr_img.length; i++) {
                          if(arr_img[i].section_name.trim() == title_text.trim())
                          {
                            container_edit_image += '<input type="hidden" name="section_id" value="'+arr_img[i].section_id+'">';
                            container_edit_image += '<input type="hidden" name="prop_id" value="<?php echo $id_projek; ?>">';
                            for (var j = 1; j <= 4; j++) {
                              container_edit_image += '<div class="col-md-12">';
                              container_edit_image += '<span style="margin:5px;position: relative;overflow: hidden;display: inline-block;" class="btn btn-success fileinput-button">';
                              container_edit_image += '<i class="fa fa-image"></i><span>Select files...</span>';
                              container_edit_image += '<input style="position: absolute;top: 0;right: 0;margin: 0;opacity: 0;-ms-filter:font-size: 200px !important;direction: ltr;cursor: pointer;" id="fileupload" img="'+i+'" type="file" name="file[]" multiple="">';
                              container_edit_image += '</span>';
                              container_edit_image += '<span id="file_name_'+i+'">asdasd</span>';
                              container_edit_image += '</div>';
                            }
                          }
                        }
                        container_edit_image += '</form>';
                      }
                    }
                  }
                  $('#edited_images').append($(container_edit_image));
                  $('#modal_edit_section_image').modal('show');
                }
                else
                {
                  swal('tunggu ya kawan, masih dibetulkan ini rupanya.');
                }
              });

              $(document).on('click','.btn-remove-img',function(){
                var this_img = $(this);
                $.ajax({
                    type: "POST",
                    url: '<?=base_url()?>proposal/remove_image',
                    data: {
                        'section_id' : this_img.attr('section_id'),
                        'section_detail_id' : this_img.attr('imag')
                      },
                    success: function(data)
                    {
                      $('#div_img_'+this_img.attr('imag')).remove();
                      var container_edit_image = '';
                              container_edit_image += '<div class="col-md-12">';
                              container_edit_image += '<span style="margin:5px;position: relative;overflow: hidden;display: inline-block;" class="btn btn-success fileinput-button">';
                              container_edit_image += '<i class="fa fa-image"></i><span>Select files...</span>';
                              container_edit_image += '<input style="position: absolute;top: 0;right: 0;margin: 0;opacity: 0;-ms-filter:font-size: 200px !important;direction: ltr;cursor: pointer;" id="fileupload" img="1" type="file" name="file[]" multiple="">';
                              container_edit_image += '</span>';
                              container_edit_image += '<span id="file_name_1">asdasd</span>';
                              container_edit_image += '</div>';
                        $('#edited_images').append($(container_edit_image));
                        //$('.btn-save-edit-image').css('display','inline-block');
                        location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      swal("Oops","Gagal hapus gambar. Silahkan coba kembali.","error");
                    }
                  });
              });

              /*edit section list*/
              // Create a new list item when clicking on the "Tambah Kemampuan" button
              var ct = <?php echo $ct; ?>;
              $(document).on('click','.addKemampuan',function(){
                var newdivinputskill = $('<div id="divadd_'+ parseInt(ct+1)  +'"></div>');

                var newinputskill = $('<input id="hasiladdskill'+ parseInt(ct+1) +'" name="skills" type="text" style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" class="form-control hasiladdskill">');

                var newbtndelete = $('<button ctid="'+ parseInt(ct+1) +'" style="margin-bottom: 10px;" class="btn btn-sm btn-default btn-remove-skill"><i class="fa fa-trash"></i></button>');


                var inputValue = $('#hasiladdskill'+ parseInt(ct-1)).val();
                if(inputValue == '')
                {
                  swal("Anda harus menuliskan kemampuan anda terlebih dahulu!");
                }
                else
                {
                  ct++;
                  $('.listSkill').append(newdivinputskill.append(newinputskill).append(newbtndelete));
                  newinputskill.select();
                }
              });

              $(document).on('click','.btn-remove-skill',function(){
                var ctid = $(this).attr('ctid');
                $('#divadd_'+ctid).remove();
                ct--;
              });

              // Create a new list item when clicking on the "Tambah Pengalaman" button
              var th = <?php echo $th; ?>;
              $(document).on('click','.addPengalaman',function(){
                var newdivinputpengalaman = $('<div id="divaddpengalaman_'+  parseInt(th+1)  +'"></div>');

                var newinputpengalaman = $('<input id="inputpengalaman'+parseInt(th+1)+'" style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" name="pengalaman" type="text" class="form-control hasiladdpengalaman">');

                var newbtndeletepengalaman = $('<button thid="'+ parseInt(th+1) +'" style="margin-bottom: 10px;" class="btn btn-sm btn-default btn-remove-pengalaman"><i class="fa fa-trash"></i></button>');


                var latestpengalaman = $('#inputpengalaman'+ parseInt(th-1)).val();
                if(latestpengalaman == '')
                {
                  swal("Isikan pengalaman pada isian terakhir");
                }
                else
                {
                  th++;
                  $('.listPengalaman').append(newdivinputpengalaman.append(newinputpengalaman).append(newbtndeletepengalaman));
                  newinputpengalaman.select();
                }
              });

              $(document).on('click','.btn-remove-pengalaman',function(){
                var thid = $(this).attr('ctid');
                $('#divaddpengalaman_'+thid).remove();
                th--;
              });


              var ch = <?php echo $ch; ?>;
              $(document).on('click','.addThpnBtn',function(){
                var newdivinputtahapan = $('<div id="divaddtahapan_'+ parseInt(ch+1)+'"></div>');

                var newinputtahapan = $('<input style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" id="tahapan'+ parseInt(ch+1)+'" name="tahapan" type="text" class="form-control hasiladdtahapan">');

                var newbtndeletetahapan = $('<button style="margin-bottom:10px;" chid="'+ parseInt(ch+1)+'" class="btn btn-sm btn-default btn-remove-tahapan"><i class="fa fa-trash"></i></button>');


                var inputValuetahapan = $('#tahapan'+ parseInt(ch-1)).val();
                if(inputValuetahapan == '')
                {
                  swal("Anda harus menuliskan tahapan pada isian terakhir.");
                }
                else
                {
                  ch++;
                  $('.listTahapan').append(newdivinputtahapan.append(newinputtahapan).append(newbtndeletetahapan));
                  newinputtahapan.select();
                }
              });

              $(document).on('click','.btn-remove-tahapan',function(){
                var chid = $(this).attr('chid');
                $('#divaddtahapan_'+chid).remove();
                ch--;
              });

              var ctr = <?php echo $ctr; ?>;
              $(document).on('click','.addBtnTerm',function(){
                var newdivinputterm = $('<div id="divaddterm_'+ parseInt(ctr+1) +'"></div>');
                var newinputterm = $('<input style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" id="terms'+ctr+'" name="syarat" type="text" class="form-control hasiladdterm">');
                var newbtndeleteterm = $('<button style="margin-bottom:10px;" ctrid="'+ parseInt(ctr+1) +'" class="btn btn-sm btn-default btn-remove-term"><i class="fa fa-trash"></i></button>');

                var inputValueterm = $('#terms'+ parseInt(ctr-1)).val();
                if(inputValueterm == '')
                {
                  swal("Isi terlebih dahulu syarat & ketentuan pada isian terakhir");
                }
                else
                {
                  ctr++;
                  $('.listTerm').append(newdivinputterm.append(newinputterm).append(newbtndeleteterm));
                  newinputterm.select();
                }
              });

              $(document).on('click','.btn-remove-term',function(){
                var ctrid = $(this).attr('ctrid');
                $('#divaddterm_'+ctrid).remove();
                ctr--;
              });

              var cb = <?php echo $cb; ?>;
              $(document).on('click','.addYgDiberiBtn',function(){
                var newrowbiaya = $('<tr id="traddbiaya_'+cb+'"></tr>');
                var newinputdiberi = $('<td><input style="width: 80%;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" class="form-control" id="diberi'+cb+'" type="text" name="diberi" placeholder="Tahapan"></td>');
                var newinputbiaya = $('<td><span style="float: left;padding-top: 10px;">Rp</span><input class="form-control" id="biaya'+cb+'" style="width: 80%;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" type="text" name="biaya" placeholder="Harga" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 &amp;&amp; key  <= 57) || key == 8);"></td>');
                var newbtndeletebiaya = $('<td><button cbid="'+cb+'" class="btn btn-default btn-sm btn-remove-biaya" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button></td>');

                var inputdiberi = $('#diberi').val();
                var inputbiaya = $('#biaya').val();
                if(inputdiberi == '' || inputbiaya == '')
                {
                  swal("Anda harus menuliskan biaya pertama terlebih dahulu!");
                }
                else
                {
                  cb++;
                  $('#listDiberi').append(newrowbiaya.append(newinputdiberi).append(newinputbiaya).append(newbtndeletebiaya));
                  newinputdiberi.select();
                }

              });

              $(document).on('click','.btn-remove-biaya',function(){
                var cbid = $(this).attr('cbid');
                $('#traddbiaya_'+cbid).remove();
                cb--;
              });

              /*end edit section list*/

              $(document).on('click','.btn-save-image',function(){
                var upload_images=[];
                $('[name="file[]"]').each(function(){
                  var file = $(this);
                  var values = file.map(function(){
                    return $(this).val();
                  }).get().join();
                  if(values != '')
                  {
                    upload_images.push(values);
                  }
                });

                if($('#new_content_image_title').val() == 0)
                {
                  swal("Oops","Isi judul konten","error");
                }
                else if(upload_images.length == 0)
                {
                  swal("Oops","Pilih gambar yang akan ditambahkan","error");
                }
                else
                {
                  $('#form_upload_image_content').submit();
                }
              });

              $(document).on('change','[name="file[]"]',function(){
                $('#file_name_'+$(this).attr('img')).text($(this).val());
              });

              $(document).on('click','.btn-save-section',function(){
                $(this).html('Menambahkan...').attr('disable','disabled');
                var insert_after = $('#position_after').val();
                var proposal_id = $('#prop_id').val();
                var id_section = $('#new_content_title').val().replace(/ /g, "_").toLowerCase();
                if($('#new_content_title').val() == '')
                {
                  swal('Judul konten wajib diisi');
                  $(this).html('<i class="fa fa-plus" aria-hidden="true"></i>Tambahkan').attr('disable','');
                }
                else if($('#new_content_detail').val() == '')
                {
                  swal('Silahkan isi konten untuk ' + $('#new_content_title').val());
                  $(this).html('<i class="fa fa-plus" aria-hidden="true"></i>Tambahkan').attr('disable','');
                }
                else
                {
                  $.ajax({
                    type: "POST",
                    url: '<?=base_url()?>proposal/add_new_section',
                    data: {
                        'proposal_id' : proposal_id,
                        'user_id' : <?php echo $this->session->userdata('user_id'); ?>,
                        'section_name' : $('#new_content_title').val(),
                        'section_pos' : insert_after,
                        'section_detail_content' : $('#new_content_detail').val()
                      },
                    success: function(data)
                    {
                      $('.btn-save-section').html('<i class="fa fa-plus" aria-hidden="true"></i>Tambahkan').attr('disable','');
                      $('#modal_add_section').modal('hide');
                      var new_section = $('<div class="row section" id="section_'+ id_section +'"><div class="section-padding"><div  class="<?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['section_layout'] == 0){ echo 'col-md-4'; }else if($detail_proposal['style']['section_layout'] == 1){ echo 'col-md-12'; }}else{echo 'col-md-4';} ?> section-title"><h3 class="caption" id="title_'+id_section+'">'+ $('#new_content_title').val() +'</h3></div><div  class="<?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['section_layout'] == 0){ echo 'col-md-8'; }else if($detail_proposal['style']['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8';} ?> section-content view-project-info"><div  class="mt content" ><h5 class="content" id="info_'+id_section+'">'+ $('#new_content_detail').val() +'</h5></div><div class="btn-edit-section tooltips" data-placement="right" data-original-title="Edit" id="edit-btn-section_'+id_section+'" section="'+id_section+'" sectype="text"><i class="fa fa-pencil"></i></div><div class="btn-remove-section tooltips" data-placement="right" data-original-title="Hapus" id="remove-btn-section_'+id_section+'" section="'+id_section+'" sectype="text"><i class="fa fa-trash"></i></div></div><div class="col-md-12 panel-btn-add" id="add-btn-section_'+id_section+'"><div style="text-align: center; margin-top: 20px; border-top: solid 1px #d8d8d8;font-size: 25px; color: #d8d8d8;"><div style="margin-top: -15px;"><i class="fa fa-align-left btn-add-section tooltips" data-placement="bottom" data-original-title="tambah konten teks" section="'+id_section+'" style="cursor:pointer;"></i>&nbsp;<i class="fa fa-image btn-add-image tooltips" data-placement="bottom" data-original-title="tambah konten gambar" section="'+id_section+'" style="cursor:pointer;"></i></div></div></div></div></div>');
                      new_section.insertAfter($('#'+insert_after));
                      location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      swal("Oops","Konten gagal ditambahkan. Silahkan coba kembali.","error");
                    }
                  });
                }
              });


              $(document).on('click','.btn-save-edit-section',function(){
                var edited_section = $(this).attr('section');
                var edited_sid = $('#edited_sid').val();
                if(edited_sid <= 0 )
                {
                  if(edited_section == 'overview')
                  {
                    if($('#info_edit_content_text').val() == '')
                    {
                      swal("Oops","Konten masih kosong.","error");
                    }
                    else
                    {
                      $.ajax({
                        method: "POST",
                        url : "<?php echo base_url();?>proposal/update_section_projek",
                        data : {
                          proposal_id:$('#edited_prop_id').val(),
                          project_desc:$('#info_edit_content_text').val()
                        },
                        success : function(data){
                          if(data == 1)
                          {
                              swal('Deskripsi Proyek berhasil diubah');
                              $('#modal_edit_section_text').modal('hide');
                              location.reload();
                          }
                          else
                          {
                            swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                        }
                      });
                    }
                  }
                  else if(edited_section == 'aboutme')
                  {
                    if($('#info_edit_content_text').val() == '')
                    {
                      swal("Oops","Konten masih kosong.","error");
                    }
                    else
                    {
                      $.ajax({
                        method: "POST",
                        url : "<?php echo base_url();?>proposal/update_section_aboutme",
                        data : {
                          proposal_id:$('#edited_prop_id').val(),
                          about:$('#info_edit_content_text').val()
                        },
                        success : function(data){
                          if(data == 1)
                          {
                            swal('<?php echo($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1 && $this->monikalib->getUserPlan()->plan_id >= 3 ) ? 'Tentang Kami':'Tentang Saya' ?> berhasil diubah');
                            $('#modal_edit_section_text').modal('hide');
                            location.reload();
                          }
                          else
                          {
                            swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                        }
                      });
                    }
                  }
                  else if(edited_section == 'benefit')
                  {
                    if($('#info_edit_content_text').val() == '')
                    {
                      swal("Oops","Konten masih kosong.","error");
                    }
                    else
                    {
                      $.ajax({
                        method: "POST",
                        url : "<?php echo base_url();?>proposal/update_section_benefit",
                        data : {
                          proposal_id:$('#edited_prop_id').val(),
                          keuntungan:$('#info_edit_content_text').val()
                        },
                        success : function(data){
                          if(data == 1)
                          {
                            swal('Benefit/Hasil berhasil diubah');
                            $('#modal_edit_section_text').modal('hide');
                            location.reload();
                          }
                          else
                          {
                            swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                        }
                      });
                    }
                  }
                  else if(edited_section == 'nextstep')
                  {
                    if($('#info_edit_content_text').val() == '')
                    {
                      swal("Oops","Konten masih kosong.","error");
                    }
                    else
                    {
                      $.ajax({
                        method: "POST",
                        url : "<?php echo base_url();?>proposal/update_section_nextstep",
                        data : {
                          proposal_id:$('#edited_prop_id').val(),
                          nextstep:$('#info_edit_content_text').val()
                        },
                        success : function(data){
                          if(data == 1)
                          {
                            swal('Langkah Selanjutnya berhasil diubah');
                            $('#modal_edit_section_text').modal('hide');
                            location.reload();
                          }
                          else
                          {
                            swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                        }
                      });
                    }
                  }
                  else if(edited_section == 'team')
                  {
                    var portfolio=[];
                    $('[name="pengalaman"]').each(function(){
                      if($(this).val() != '')
                      {
                        portfolio.push($(this).val());
                      }
                    });
                    if(portfolio.length <= 0)
                    {
                      swal("Oops","Portfolio masih kosong.","error");
                    }
                    else
                    {
                      $.ajax({
                        method: "POST",
                        url : "<?php echo base_url();?>proposal/update_section_portfolio",
                        data : {
                          proposal_id:$('#edited_prop_id').val(),
                          portfolio:portfolio
                        },
                        success : function(data){
                          if(data == 1)
                          {
                            swal('Portfolio berhasil diubah');
                            $('#modal_edit_section_list').modal('hide');
                            location.reload();
                          }
                          else
                          {
                            swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                        }
                      });
                    }
                  }
                  else if(edited_section == 'team_value')
                  {
                    var skills=[];
                    $('[name="skills"]').each(function(){
                      if($(this).val() != '')
                      {
                        skills.push($(this).val());
                      }
                    });

                    if(skills.length <= 0)
                    {
                      swal("Oops","Skill masih kosong.","error");
                    }
                    else
                    {
                      $.ajax({
                        method: "POST",
                        url : "<?php echo base_url();?>proposal/update_section_skills",
                        data : {
                          proposal_id:$('#edited_prop_id').val(),
                          skills:skills
                        },
                        success : function(data){
                          if(data == 1)
                          {
                            swal('Skill & Value berhasil diubah');
                            $('#modal_edit_section_list').modal('hide');
                            location.reload();
                          }
                          else
                          {
                            swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                        }
                      });
                    }
                  }
                  else if(edited_section == 'milestone')
                  {
                    var milestone=[];
                    $('[name="tahapan"]').each(function(){
                      if($(this).val() != '')
                      {
                        milestone.push($(this).val());
                      }
                    });

                    if(milestone.length <= 0)
                    {
                      swal("Oops","Milestone/Tahapan masih kosong.","error");
                    }
                    else
                    {
                      $.ajax({
                        method: "POST",
                        url : "<?php echo base_url();?>proposal/update_section_milestone",
                        data : {
                          proposal_id:$('#edited_prop_id').val(),
                          milestone:milestone
                        },
                        success : function(data){
                          if(data == 1)
                          {
                            swal('Milestone/Tahapan berhasil diubah');
                            $('#modal_edit_section_list').modal('hide');
                            location.reload();
                          }
                          else
                          {
                            swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                        }
                      });
                    }
                  }
                  else if(edited_section == 'term')
                  {
                    var terms=[];
                    $('[name="syarat"]').each(function(){
                      if($(this).val() != '')
                      {
                        terms.push($(this).val());
                      }
                    });

                    if(terms.length <= 0)
                    {
                      swal("Oops","Syarat & Ketentuan masih kosong.","error");
                    }
                    else
                    {
                      $.ajax({
                        method: "POST",
                        url : "<?php echo base_url();?>proposal/update_section_terms",
                        data : {
                          proposal_id:$('#edited_prop_id').val(),
                          terms:terms
                        },
                        success : function(data){
                          if(data == 1)
                          {
                            swal('Syarat & Ketentuan berhasil diubah');
                            $('#modal_edit_section_list').modal('hide');
                            location.reload();
                          }
                          else
                          {
                            swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                        }
                      });
                    }
                  }
                  else if(edited_section == 'fee')
                  {
                    var diberi = [];
                    $('[name="diberi"]').each(function(){
                      if($(this).val() != '')
                      {
                        diberi.push($(this).val());
                      }
                    });
                    var biaya=[];
                    $('[name="biaya"]').each(function(){
                      if($(this).val() != '')
                      {
                        biaya.push($(this).val());
                      }
                    });

                    if(diberi.length <= 0 || biaya.length <= 0)
                    {
                      swal("Oops","Biaya masih kosong.","error");
                    }
                    else
                    {
                      $.ajax({
                        method: "POST",
                        url : "<?php echo base_url();?>proposal/update_section_fee",
                        data : {
                          proposal_id:$('#edited_tabel_prop_id').val(),
                          fee:biaya,
                          job:diberi
                        },
                        success : function(data){
                          if(data == 1)
                          {
                            swal('Biaya berhasil diubah');
                            $('#modal_edit_section_tabel').modal('hide');
                            location.reload();
                          }
                          else
                          {
                            swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                        }
                      });
                    }
                  }
                }
                else
                {
                    $.ajax({
                      method: "POST",
                      url : "<?php echo base_url();?>proposal/update_additional_section_text",
                      data : {
                        proposal_id:$('#edited_prop_id').val(),
                        edited_sid:edited_sid,
                        title: $('#title_edit_content_text').val(),
                        content:$('#info_edit_content_text').val()
                      },
                      success : function(data){
                        if(data == 1)
                        {
                          swal($('#title_edit_content_text').val() + ' berhasil diubah');
                          $('#modal_edit_section_text').modal('hide');
                          location.reload();
                        }
                        else
                        {
                          swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                        }
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        swal("Oops","Konten gagal diubah. Silahkan coba kembali.","error");
                      }
                    });
                }
              });

              $(document).on('click','.btn-save-edit-image',function(){
                var upload_images=[];
                $('[name="file[]"]').each(function(){
                  var file = $(this);
                  var values = file.map(function(){
                    return $(this).val();
                  }).get().join();
                  if(values != '')
                  {
                    upload_images.push(values);
                  }
                });

                if(upload_images.length == 0)
                {
                  swal("Oops","Pilih gambar yang akan ditambahkan","error");
                }
                else
                {
                  $('#form_upload_image_edit').submit();
                }
              });

              $(document).on('click','.btn-save-edit-header-image',function(){
                if($('#imgInp').val() == '')
                {
                  swal("Oops","Pilih gambar yang akan ditambahkan","error");
                }
                else
                {
                  $('#form_upload_header_image_edit').submit();
                }
              });

              $(document).on('click','.btn-save-edit-logo-image',function(){
                if($('#imglg').val() == '')
                {
                  swal("Oops","Pilih gambar yang akan ditambahkan","error");
                }
                else
                {
                  $('#form_upload_logo_image_edit').submit();
                }
              });

              $(document).on('click','#link_add_saved_content',function(){
                if($(this).attr('section') == 'aboutme')
                {
                  var replaceaboutme = '<?php echo preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$this->session->userdata('user_about'))); ?>';
                  $('#info_edit_content_text').val(replaceaboutme.replace(/<br\s*\/?>/g, '\n'))
                }
                else if($(this).attr('section') == 'team')
                {
                  <?php if($this->session->userdata('userportfolio') != NULL || count($this->session->userdata('userportfolio')) > 0){ ?>
                    <?php foreach ($this->session->userdata('userportfolio') as $key => $userportfolio){ ?>
                      var newdivinputpengalaman = $('<div id="divaddpengalaman_'+  parseInt(th+1)  +'"></div>');

                      var newinputpengalaman = $('<input id="inputpengalaman'+parseInt(th+1)+'" style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" name="pengalaman" type="text" class="form-control hasiladdpengalaman" value="<?php echo addslashes($userportfolio); ?>">');

                      var newbtndeletepengalaman = $('<button thid="'+ parseInt(th+1) +'" style="margin-bottom: 10px;" class="btn btn-sm btn-default btn-remove-pengalaman"><i class="fa fa-trash"></i></button>');

                      th++;

                      $('.listPengalaman').append(newdivinputpengalaman.append(newinputpengalaman).append(newbtndeletepengalaman));
                      newinputpengalaman.select();

                    <?php } ?>
                  <?php } ?>
                }
                else if($(this).attr('section') == 'team_value')
                {
                  <?php if($this->session->userdata('userskill') != NULL && count($this->session->userdata('userskill')) > 0){ ?>
                    <?php foreach ($this->session->userdata('userskill') as $key => $userskill){ ?>
                      var newdivinputskill = $('<div id="divadd_'+ parseInt(ct+1)  +'"></div>');

                      var newinputskill = $('<input id="hasiladdskill'+ parseInt(ct+1) +'" name="skills" type="text" style="float: left;width: 80%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px 0px 1px;border-bottom: 1px solid rgb(0, 0, 0);border-radius: 0px;" class="form-control hasiladdskill" value="<?php echo addslashes($userskill); ?>">');

                      var newbtndelete = $('<button ctid="'+ parseInt(ct+1) +'" style="margin-bottom: 10px;" class="btn btn-sm btn-default btn-remove-skill"><i class="fa fa-trash"></i></button>');

                      ct++;

                      $('.listSkill').append(newdivinputskill.append(newinputskill).append(newbtndelete));
                      newinputskill.select();
                    <?php } ?>
                  <?php } ?>
                }
              });

              $('#btn-send-proposal').on('click',function(){
                if($('#send_proposal_sender_name').val() == '')
                {
                  swal("Oops","Silahkan isi nama Anda / pengirim proposal yang diinginkan","error");
                }
                else if($('#send_proposal_sender_email').val() == '')
                {
                  swal("Oops","Silahkan isi email Anda / pengirim proposal yang diinginkan","error");
                }
                else if(!ValidateEmail($('#send_proposal_sender_email').val()))
                {
                  swal("Oops","Pastikan format email pengirim sudah benar","error");
                }
                else if($('#send_proposal_client_name').val() == '')
                {
                  swal("Oops","Nama klien belum diisi","error");
                }
                else if($('#send_proposal_client_email').tagsinput('items').length <= 0)
                {
                  swal("Oops","Email klien belum diisi","error");
                }
                else if($('#send_proposal_client_email_subject').val() == '')
                {
                  swal("Oops","Subject email belum diisi","error");
                }
                else if($('#send_proposal_client_email_message').val() == '')
                {
                  swal("Oops","Isi email masih kosong","error");
                }
                else
                {
                  var em = $('#send_proposal_client_email').tagsinput('items');
                  var cc = $('#send_proposal_client_cc').tagsinput('items');

                  if(em.length > 0)
                  {
                   for(var j=0;j<em.length;j++)
                   {
                      if(!ValidateEmail(em[j]))
                      {
                        swal("Oops","Pastikan format email benar","error");
                        return false;
                      }
                   }
                  }

                  if(cc.length > 0)
                  {
                   for(var i=0;i<cc.length;i++)
                   {
                      if(!ValidateEmail(cc[i]))
                      {
                        swal("Oops","Pastikan format email di cc benar","error");
                        return false;
                      }
                   }
                  }

                  swal({
                    title: "Proposal akan dikirim",
                    text: "Apakah Anda yakin?",
                    icon: "info",
                    buttons: true,
                    dangerMode: true,
                  })
                  .then((kirim) => {
                    if (kirim) {
                      $('#btn-send-proposal').attr('disabled',true).html('Mengirim...');
                      $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>proposal/send_proposal",
                        data: {
                            'sender_name' : $('#send_proposal_sender_name').val(),
                            'sender_email' : $('#send_proposal_sender_email').val(),
                            'client_email' : $('#send_proposal_client_email').val(),
                            'client_cc' : $('#send_proposal_client_cc').val(),
                            'client_name' : $('#send_proposal_client_name').val(),
                            'email_message' : $('#send_proposal_client_email_message').val(),
                            'email_subject' : $('#send_proposal_client_email_subject').val()
                            },
                        success: function(data){
                          $('#btn-send-proposal').attr('disabled',false).html('<i class="fa fa-send" aria-hidden="true"></i> Kirim');
                          if(data == 1)
                          {
                            swal('Proposal berhasil dikirim');
                            $('#modal_send_proposal').modal('hide');
                          }
                          else
                          {
                            swal('Ada kesalahan');
                          }

                        }, error:function(error){
                          swal('Ada kesalahan');
                        }
                      });
                    }
                  });
                }
              });

              $('#download_pdf').click( function(e) {
                <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
                    e.preventDefault();
                    <?php
                        $random = md5(mt_rand(1,10000));
                        $first = substr($random,0,5);
                        $last = substr($random,5,10);
                        $urlrand = $first.$id_projek.$last;
                    ?>
                    // window.open("<?php echo site_url('proposal/download/'.$urlrand);?>","_blank");
                    window.open("<?php echo site_url('proposal/tcpdf/'.$urlrand);?>","_blank");
                    return false;
                <?php }else{ ?>
                  <?php if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1){ ?>
                    e.preventDefault();
                    <?php
                        $random = md5(mt_rand(1,10000));
                        $first = substr($random,0,5);
                        $last = substr($random,5,10);
                        $urlrand = $first.$id_projek.$last;
                    ?>
                    window.open("<?php echo site_url('proposal/tcpdf/'.$urlrand);?>","_blank");
                    // window.open("<?php echo site_url('proposal/download/'.$urlrand);?>","_blank");
                    return false;
                  <?php }else{ ?>
                    swal("Oops","Saat ini Anda menggunakan Plan Personal. Ubah plan menjadi \"Agency\" untuk bisa download PDF Proposal.","info", {
                      buttons: {
                        cancel:true,
                        upgrade:{
                          text: "Ubah Plan",
                          value: "upgrade",
                        }
                      },
                    })
                    .then((value) => {
                      switch (value) {

                        case "upgrade":
                          window.location = "<?php echo site_url('plan'); ?>";
                          break;

                        default:
                          break;
                      }
                    });
                  <?php } ?>
                <?php } ?>
              });

              $('.btn-refer-to-friend').on('click',function(){
                if($('#friend_email').val() == '')
                {
                  swal("Oops","Email belum diisi","error");
                }
                else
                {
                          var em = $('#friend_email').tagsinput('items');

                          if(em.length > 0)
                          {
                           for(var j=0;j<em.length;j++)
                           {
                              if(!ValidateEmail(em[j]))
                              {
                                swal("Oops","Pastikan format email benar","error");
                                return false;
                              }
                           }
                          }

                          swal({
                            title: "Undangan akan dikirim",
                            text: "Apakah Anda yakin?",
                            icon: "info",
                            buttons: true,
                            dangerMode: true,
                          })
                          .then((kirim) => {
                            if (kirim) {
                              $('.btn-refer-to-friend').attr('disabled',true).html('Mengirim...');
                              $.ajax({
                                type:"POST",
                                url: "<?php echo base_url()?>home/refer_to_friend",
                                data: {
                                    'user_id' : <?php echo $this->session->userdata('user_id');?>,
                                    'refer_email' : $('#friend_email').val()
                                    },
                                success: function(data){
                                  $('.btn-refer-to-friend').attr('disabled',false).html('<i class="fa fa-send" aria-hidden="true"></i> Kirim');
                                  if(data == 1)
                                  {
                                    swal('Ajakan berhasil dikirim');
                                    $('#friend_email').val('');
                                    $('#friend_email').tagsinput('removeAll');
                                    $('#modal_referral').modal('hide');
                                  }
                                  else if(data == 0)
                                  {
                                    swal("Info","Ajakan sudah pernah dikirim via email. Coba ajak teman lainnya.","info");
                                    $('#friend_email').val('');
                                    $('#friend_email').tagsinput('removeAll');
                                  }
                                  else
                                  {
                                    swal('Ada kesalahan');
                                  }

                                }, error:function(error){
                                  swal('Ada kesalahan');
                                }
                              });
                            }
                          });
                }
              });

            });

          function ValidateEmail(email) {
              //var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
              var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              return expr.test(email);
          };

          function pilihClientLama(id){
            $.ajax({
              type:"GET",
              url: "<?php echo base_url()?>client/pilihclient",
              data: {id:id},
              datatype: 'json',
              success: function(data){
                var datanya = JSON.parse(data);
                document.getElementById("client_name").value = datanya.nama;
                document.getElementById("company_name").value = datanya.perusahaan;
                document.getElementById("client_email").value = datanya.email;
                document.getElementById("client_id").value = datanya.id;
                $('#klienlama').modal('hide');
              }, error:function(error){
                swal('error;' +eval(error));
              }
            });
          }

          function update(id){
            var idproposal = id;
            var info_projek = $('#editnamaprojek').val();
            var pengalaman = $('#editteamback').val();
            var kemampuan=[];
            var idkemampuan=[];
            $('[name="editkemampuan"]').each (function() {
              if($(this).val() != '')
              {
                kemampuan.push($(this).val());
              }
            });
            $('[name="idkemampuan"]').each (function() {
              if($(this).val() != '')
              {
                idkemampuan.push($(this).val());
              }
            });
            var tahapan=[];
            var tgl_tahapan=[];
            var idtahapan=[];

            $('[name="edittahapan"]').each (function() {
              if($(this).val() != '')
              {
                tahapan.push($(this).val());
              }
            });
            $('[name="tgl_tahapan"]').each (function() {
              if($(this).val() != '')
              {
                tgl_tahapan.push($(this).val());
              }
            });
            $('[name="idtahapan"]').each (function() {
              if($(this).val() != '')
              {
                idtahapan.push($(this).val());
              }
            });

            var keuntungan = $('#editkeuntungan').val();
            var nextstep = $('#editnextstep').val();
            var syarat=[];
            var idsyarat=[];
            $('[name="editsyarat"]').each (function() {
              if($(this).val() != '')
              {
                syarat.push($(this).val());
              }
            });
            $('[name="idsyarat"]').each (function() {
              if($(this).val() != '')
              {
                idsyarat.push($(this).val());
              }
            });

            var pemberian=[];
            var harga=[];
            var idpemberian=[]
            $('[name="editpemberian"]').each (function() {
              if($(this).val() != '')
              {
                pemberian.push($(this).val());
              }
            });
            $('[name="editharga"]').each (function() {
              if($(this).val() != '')
              {
                harga.push($(this).val());
              }
            });
            $('[name="idpemberian"]').each (function() {
              if($(this).val() != '')
              {
                idpemberian.push($(this).val());
              }
            });


            $.ajax({
              method: "POST",
              url : "<?php echo base_url();?>proposal/update",
              data : {idproposal:idproposal,
                info_projek:info_projek,
                nextstep : nextstep,
                pengalaman :pengalaman,
                kemampuan : kemampuan,
                idkemampuan:idkemampuan,
                tahapan : tahapan,
                tgl_tahapan: tgl_tahapan,
                idtahapan: idtahapan,
                keuntungan : keuntungan,
                syarat : syarat,
                idsyarat : idsyarat,
                pemberian:pemberian,
                harga:harga,
                idpemberian:idpemberian
              },
              success : function(data){
                location.reload();
              }
            });

          }

          function copy(){
            var copyText = document.getElementById("link");
            copyText.select();
            document.execCommand("Copy");
            swal("Berhasil","Link berhasil disalin","success");
          }

          $(document).ready(function() {
            $('.btn-setting').click(function() {
                    $('#setting-proposal').toggle("slideRight");
                $('#overlay').fadeIn();
            });

            var iScrollPos = 0;

            $(window).scroll(function() {
              var iCurScrollPos = $(this).scrollTop();
                    if (iCurScrollPos > iScrollPos)
              {
                //$('.btn-setting').fadeOut();
                  $('#setting-proposal').fadeOut();
                  $('#overlay').fadeOut();
              }
              else
              {
                //$('.btn-setting').fadeIn();
                  $('#setting-proposal').fadeOut();
                  $('#overlay').fadeOut();
              }
              iScrollPos = iCurScrollPos;
            });

            $('#overlay').on('click',function(){
              $('#setting-proposal').fadeOut();
              $(this).fadeOut();
            });

            $('.caption').css('font-family','<?php echo (count($detail_proposal['style']) > 0 && ($detail_proposal['style']['fontstyle'] != NULL || $detail_proposal['style']['fontstyle'] != ''))?$detail_proposal['style']['fontstyle']:'Avenir'; ?>');
            $('.content').css('font-family','<?php echo (count($detail_proposal['style']) > 0 && ($detail_proposal['style']['fontstyle'] != NULL || $detail_proposal['style']['fontstyle'] != ''))?$detail_proposal['style']['fontstyle']:'Avenir'; ?>');
        });
      </script>
<?php } ?>
