<section id="main-content">
  <section class="wrapper">
      <div class="row" id="formproposal" style="margin:5% 0px 0px 0px;">
          <div class="container" style="width:100%">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 text-center">
                  <?php   $cat = $this->uri->segment(3); ?>
                  <input type="hidden" id="user_email" name="user_email" value="<?php echo $this->session->userdata('user_email')?>">
                  <input type="hidden" id="kategori" name="kategori" value="<?php echo $cat;?>">
                    <div class="tab-content">
                          <div id="client" class="tab-pane fade in active">
                            <h3><i class="fa fa-user-circle-o" aria-hidden="true"></i>Klien</h3>
                            <div class="btnKlien">
                              <?php if ($ada > 0) {?>
                                  <button class="btn btn-theme03 klienlama" id="pilihclient" data-toggle="modal">
                                    <i class="fa fa-user" aria-hidden="true"></i> Pilih klien
                                  </button>
                                  <button class="btn btn-theme03 modalKlien" id="tambahclient" data-toggle="modal"><i class="fa fa-user-plus" aria-hidden="true"></i> Tambah klien baru</button>
                                  <?php foreach ($lastclient as $klien): ?>
                                      <div class="form-group text-left clientterbaru">
                                          <label for="">Nama Klien</label>
                                          <input class="form-control" type="text" name="namaKlien" value="<?= $klien->nama_pic;?>" id="namaKlien"disabled>
                                      </div>
                                      <div class="form-group text-left clientterbaru">
                                          <label for="">Nama Perusahaan</label>
                                          <input class="form-control" type="text" name="perusahaanKlien" value="<?= $klien->perusahaan;?>" id="perusahaanKlien"disabled>
                                      </div>
                                      <div class="form-group text-left clientterbaru">
                                          <label for="">Email</label>
                                          <input class="form-control" class="clientterbaru" type="text" name="emailKlien" value="<?= $klien->email;?>" id="emailKlien"disabled>
                                      </div>
                                      <input type="hidden" name="id_client" id="id-client" value="<?= $klien->id_client;?>">
                                  <?php endforeach; ?>
                                  <a class="btn btn-next" name="btnselanjutnya" data-toggle="pill" href="#projectinfo" style="margin-top:3%;">Info Proyek <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                <?php } else { ?>
                                  <!-- END OF MODAL ADD CLIENT-->
                                  <h3>Anda belum memiliki Klien</h3>
                                  <button id="step3" class="btn btn-theme03 modalKlien" data-toggle="modal" style="margin-top:10px;"><i class="fa fa-user-plus" aria-hidden="true" style="margin-right:5px;"></i> Tambah klien baru</button>
                                <?php }; ?>
                            </div>
                          </div>
                          <div id="projectinfo" class="tab-pane fade">
                              <h3><i class="fa fa-file-text-o" aria-hidden="true"></i>Info Proyek</h3>
                              <div class="form-group text-left">
                                <label for="" class="">NAMA PROJEK</label><span style="margin-left:20px" class="error_form" id="projectname_error"></span>
                                <input type="text" class="form-control" id="projectname" name="projectname" placeholder="Projek Baru" value="">
                              </div>
                              <div class="form-group text-left">
                                <label for="" class="">DESKRIPSI DAN TUJUAN PROJEK</label> <i data-toggle="tooltip" data-placement="top" title="Jelaskan secara terperinci tentang proyek yang akan Anda kerjakan untuk klien serta bagaimana value yang Anda berikan." class="fa fa-info-circle hidden-xs" aria-hidden="true"></i>
                                <span style="margin-left:20px"class="error_form" id="projectdesc_error"></span>
                                <textarea name="projectdesc" id="projectdesc" rows="8" cols="80" class="form-control" placeholder="Apa yang akan anda kerjakan"></textarea>
                              </div>
                              <a class="btn btn-next" data-toggle="pill" href="#client"><i class="fa fa-arrow-left" aria-hidden="true"></i>Klien</a>
                              <a class="btn btn-next" name="btnselanjutnya" href="#aboutyoucontainer" id="btnselanjutnya1" data-toggle="pill">Tentang Anda <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                          </div>
                          <div id="aboutyoucontainer" class="tab-pane fade">
                            <h3><i class="fa fa-user-o" aria-hidden="true"></i>Tentang Anda</h3>
                            <div class="form-group text-left">
                              <label for="" class="">CERITAKAN DIRI ANDA</label>
                              <textarea name="aboutyou" id="aboutyou" rows="10" cols="80" class="form-control" placeholder="Saya adalah"><?php echo $this->session->userdata('user_about') != NULL ? $this->session->userdata('user_about') : ''; ?></textarea>
                            </div>
                            <a class="btn btn-next" data-toggle="pill" href="#projectinfo"><i class="fa fa-arrow-left" aria-hidden="true"></i>Info Proyek</a>
                            <a class="btn btn-next" name="btnselanjutnya" id="btnselanjutnya2" data-toggle="pill" href="#deliver">Portfolio & Pengalaman Anda <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                          </div>
                          <div id="deliver" class="tab-pane fade">
                              <h3><i class="fa fa-paper-plane" aria-hidden="true"></i>Team Value / Portofolio Anda</h3>
                              <div class="form-group row text-left">
                                <label for="">SKILL</label> <i data-toggle="tooltip" data-placement="top" title="Tambahkan kemampuan Anda atau tim yang sangat memberikan nilai lebih untuk proyek ini." class="fa fa-info-circle" aria-hidden="true"></i>
                                <div class="listSkill">
                                  <?php if($this->session->userdata('userskill') != NULL && count($this->session->userdata('userskill')) > 0){ ?>
                                    <?php $ct = count($this->session->userdata('userskill'))-1; foreach ($this->session->userdata('userskill') as $key => $userskill){ ?>
                                      <div id="divadd_<?php echo $key; ?>">
                                        <input id="hasiladdskill" name="skills" type="text" value="<?php echo $userskill; ?>" class="form-control hasiladdskill">
                                        <?php if($key==0){ ?>
                                          <button type="button" class="btn btn-defaul btn-sm addBtn addKemampuan"><i class="fa fa-plus"></i></button>
                                        <?php }else{ ?>
                                          <button ctid="<?php echo $key; ?>" class="btn btn-sm btn-danger btn-remove-skill"><i class="fa fa-trash"></i></button>
                                        <?php } ?>
                                      </div>
                                    <?php } ?>
                                  <?php }else{ $ct = 0; ?>
                                    <input type="text" class="form-control inpskill" name="skills" id="skills1" placeholder="Kemampuan Anda">
                                    <button type="button" class="btn btn-defaul btn-sm addBtn addKemampuan"><i class="fa fa-plus"></i></button>
                                  <?php } ?>
                                </div>
                              </div>
                              <div class="form-group row text-left">
                                <label for="">PENGALAMAN</label> <i data-toggle="tooltip" data-placement="top" title="Pengalaman kerja Anda sangat ingin diketahui klien. Ceritakan dengan baik pencapaian-pencapaian Anda." class="fa fa-info-circle" aria-hidden="true"></i>
                                <div class="listPengalaman">
                                  <?php if($this->session->userdata('userportfolio') != NULL && count($this->session->userdata('userportfolio')) > 0){ ?>
                                    <?php $th = count($this->session->userdata('userportfolio'))-1; foreach ($this->session->userdata('userportfolio') as $key => $userportfolio){ ?>
                                      <div id="divaddpengalaman_<?php echo $key; ?>">
                                        <input type="text" class="form-control" name="pengalaman" id="inputpengalaman<?php echo $key; ?>" value="<?php echo $userportfolio; ?>" placeholder="Pengalaman Anda">
                                        <?php if($key==0){ ?>
                                          <button type="button" class="btn btn-defaul btn-sm addPengalaman"><i class="fa fa-plus"></i></button>
                                        <?php }else{ ?>
                                          <button ctid="<?php echo $key; ?>" class="btn btn-sm btn-danger btn-remove-pengalaman"><i class="fa fa-trash"></i></button>
                                        <?php } ?>
                                      </div>
                                    <?php } ?>
                                  <?php }else{ $th = 0; ?>
                                    <input type="text" class="form-control" name="pengalaman" id="inputpengalaman1" placeholder="Pengalaman Anda">
                                    <button type="button" class="btn btn-defaul btn-sm addPengalaman"><i class="fa fa-plus"></i></button>
                                  <?php } ?>
                                </div>
                              </div>
                              <a class="btn btn-next" data-toggle="pill" href="#aboutyoucontainer"><i class="fa fa-arrow-left" aria-hidden="true"></i>Tentang Anda</a>
                              <a class="btn btn-next" name="btnselanjutnya" id="btnselanjutnya3" data-toggle="pill" href="#milestone">Tahapan Proyek <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                          </div>
                          <div id="milestone" class="tab-pane fade">
                              <h3><i class="fa fa-calendar-check-o" aria-hidden="true"></i>Tahapan</h3>
                              <p>Apa saja yang akan Anda lakukan selama pengerjaan proyek? (Optional) <i data-toggle="tooltip" data-placement="top" title="Tambahkan tahapan-tahapan Anda dalam mengerjakan proyek ini" aria-hidden="true"></i></p>
                              <div class="form-group text-left">
                                <div class="listTahapan">
                                  <div id="divaddtahapan_1">
                                    <input type="text" class="form-control inptahapan" id="tahapan1" name="tahapan" placeholder="Tahapan kerja" value="Meeting pertama pendeskripsian masalah dan kebutuhan klien sekaligus brainstorming ide solusi yang memungkinkan">
                                    <button type="button" class="btn btn-defaul btn-sm addThpnBtn"><i class="fa fa-plus"></i></button>
                                  </div>
                                  <div id="divaddtahapan_2">
                                    <input type="text" class="form-control inptahapan" id="tahapan2" name="tahapan" placeholder="Tahapan kerja" value="Riset internal tim serta pendeskripsian sumber daya yang dibutuhkan">
                                    <button chid="2" class="btn btn-sm btn-danger btn-remove-tahapan"><i class="fa fa-trash"></i></button>
                                  </div>
                                  <div id="divaddtahapan_3">
                                    <input type="text" class="form-control inptahapan" id="tahapan3" name="tahapan" placeholder="Tahapan kerja" value="Desain konseptual">
                                    <button chid="3" class="btn btn-sm btn-danger btn-remove-tahapan"><i class="fa fa-trash"></i></button>
                                  </div>
                                </div>
                              </div>
                              <a class="btn btn-next" data-toggle="pill" href="#deliver"><i class="fa fa-arrow-left" aria-hidden="true"></i>Portfolio & Pengalaman Anda</a>
                              <a class="btn btn-next" name="btnselanjutnya" id="btnselanjutnya4" data-toggle="pill" href="#pricing"> Biaya <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                          </div>
                          <div id="pricing" class="tab-pane fade">
                              <h3><i class="fa fa-money" aria-hidden="true"></i> Biaya</h3>
                              <div class="row text-left">
                                  <div class="col-sm-7 hidden-xs">Pekerjaan</div>
                                  <div class="col-sm-4 hidden-xs">Biaya</div>
                                  <div class="col-xs-12 hidden-lg hidden-md hidden-sm">Pekerjaan dan Biaya</div>
                                  <div class="col-sm-1 hidden-xs"></div>

                                  <div id="listDiberi">
                                    <div id="0">
                                  <div class="col-sm-7 col-xs-12 form-group">
                                      <input class="form-control" type="text" name="diberi" placeholder="Nama pekerjaan" id="diberi">
                                  </div>
                                  <div class="col-sm-4 col-xs-10 form-group" style="white-space:nowrap">
                                      <label style="display:inline-block">Rp</label>
                                      <input style="display:inline-block" class="form-control" type="number" name="biaya" placeholder="Biaya" id="biaya" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);">
                                  </div>
                                  <div class="col-sm-1 col-xs-2 text-right">
                                      <button type="button" class="btn btn-default addYgDiberiBtn"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                  </div>
                                  </div>
                                </div>
                                  <div class="col-xs-12 form-group">
                                    <label for="">Apa saja yang didapat?</label>
                                    <textarea name="benefit" rows="8" class="form-control" id="benefit" placeholder="Apa saja yang didapat pada proyek ini"></textarea>
                                  </div>
                              </div>

                              <a class="btn btn-next" data-toggle="pill" href="#milestone"><i class="fa fa-arrow-left" aria-hidden="true"></i>Tahapan Proyek</a>
                              <a class="btn btn-next" name="btnselanjutnya" id="btnselanjutnya5" data-toggle="pill" href="#nextstepcontainer">Setelah Disetujui <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                          </div>
                          <div id="nextstepcontainer" class="tab-pane fade">
                            <h4><i class="fa fa-caret-square-o-right" aria-hidden="true"></i>Apa yang akan dilakukan setelah proposal ini disetujui? <i data-toggle="tooltip" data-placement="top" title="Jelaskan apa yang akan dilakukan setelah proposal ini disetujui seperti meeting, mengirim mockup, dsb." class="fa fa-info-circle" aria-hidden="true"></i></h4>
                            <div class="form-group text-left">
                              <textarea name="nextstep" rows="10" cols="80" class="form-control" id="nextstep" style="resize:unset;" placeholder="Apa yang akan anda lakukan setelah proposal disetujui">Bila Anda telah membaca keseluruhan proposal ini dan setuju dengan penawaran / penjelasan, silahkan klik "SETUJUI" di kanan atas layar proposal ini ini atau hubungi kami untuk mengkonfirmasi bahwa Anda setuju. Setelah mendapat notifikasi persetujuan Anda, kami akan informasikan langkah selanjutnya.</textarea>
                            </div>
                            <a class="btn btn-next" data-toggle="pill" href="#pricing"><i class="fa fa-arrow-left" aria-hidden="true"></i>Biaya Proyek</a>
                            <a class="btn btn-next" name="btnselanjutnya" id="btnselanjutnya6" data-toggle="pill" href="#termandcond">Syarat & Ketentuan Proyek <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                          </div>
                          <div id="termandcond" class="tab-pane fade">
                              <h3><i class="fa fa-list-alt" aria-hidden="true"></i>Syarat dan Ketentuan</h3>
                              <div class="form-group row text-left">
                                <label for="">Syarat dan Ketentuan</label>  <i data-toggle="tooltip" data-placement="top" title="Sebutkan syarat, ketentuan dan batasan kerja selama pengerjaan proyek ini." class="fa fa-info-circle" aria-hidden="true"></i>
                                <div class="listTerm">
                                  <div id="divaddterm_1">
                                    <input type="text" class="form-control" id="terms1" name="syarat" placeholder="Syarat dan Ketentuan yang anda ajukan" value="Struktur pembayaran yaitu 50% di muka (Down Payment) dan 50% setelah proyek selesai dan disetukui. Pembayaran melalui Transfer Bank.">
                                    <button type="button" class="btn btn-default btn-sm addBtnTerm"><i class="fa fa-plus"></i></button>
                                  </div>
                                  <div id="divaddterm_2">
                                    <input type="text" class="form-control" id="terms2" name="syarat" placeholder="Syarat dan Ketentuan yang anda ajukan" value="Sumber daya yang dibutuhkan untuk desain, seperti font, stok foto, dan ikon, tidak termasuk dalam harga di atas. Itu semua akan dibayar oleh klien, dan hak untuk menggunakannya adalah milik mereka.">
                                    <button ctrid="2" class="btn btn-sm btn-danger btn-remove-term"><i class="fa fa-trash"></i></button>
                                  </div>
                                  <div id="divaddterm_3">
                                    <input type="text" class="form-control" id="terms3" name="syarat" placeholder="Syarat dan Ketentuan yang anda ajukan" value="Pekerjaan tambahan untuk komponen yang muncul dalam lingkup pekerjaan akan dikenakan biaya per jam (contoh: Di luar jumlah revisi yang dijelaskan).">
                                    <button ctrid="3" class="btn btn-sm btn-danger btn-remove-term"><i class="fa fa-trash"></i></button>
                                  </div>
                                  <div id="divaddterm_4">
                                    <input type="text" class="form-control" id="terms4" name="syarat" placeholder="Syarat dan Ketentuan yang anda ajukan" value="Komponen baru yang tidak dijelaskan dalam lingkup pekerjaan akan dinilai dalam perkiraan baru.">
                                    <button ctrid="4" class="btn btn-sm btn-danger btn-remove-term"><i class="fa fa-trash"></i></button>
                                  </div>
                                  <div id="divaddterm_5">
                                    <input type="text" class="form-control" id="terms5" name="syarat" placeholder="Syarat dan Ketentuan yang anda ajukan" value="Hitungan hari kerja dimulai hanya setelah klien menyediakan semua sumber daya yang diminta, seperti gambar, detail kontak, dll.">
                                    <button ctrid="5" class="btn btn-sm btn-danger btn-remove-term"><i class="fa fa-trash"></i></button>
                                  </div>
                                  <div id="divaddterm_6">
                                    <input type="text" class="form-control" id="terms6" name="syarat" placeholder="Syarat dan Ketentuan yang anda ajukan" value="Hak penggunaan: Setelah pekerjaan selesai dan pembayaran telah diterima, klien akan memiliki hak atas semua.">
                                    <button ctrid="6" class="btn btn-sm btn-danger btn-remove-term"><i class="fa fa-trash"></i></button>
                                  </div>
                                  <div id="divaddterm_7">
                                    <input type="text" class="form-control" id="terms7" name="syarat" placeholder="Syarat dan Ketentuan yang anda ajukan" value="Jika terjadi pembatalan proyek setelah pekerjaan dimulai, klien akan membayar sebagian pekerjaannya.">
                                    <button ctrid="7" class="btn btn-sm btn-danger btn-remove-term"><i class="fa fa-trash"></i></button>
                                  </div>
                                </div>
                              </div>
                              <a class="btn btn-next" data-toggle="pill" href="#nextstepcontainer"><i class="fa fa-arrow-left" aria-hidden="true"></i>Setelah Disetujui</a>
                              <button class="btn btn-primary" id="btn-buat"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>Buat Proposal</button>
                          </div>
                        </div>

                        <!-- Modal Category -->
                        <div class="modal fade" id="category" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title" id="">Pilih Kategori Anda</h4>
                                </div>
                                <div class="modal-body pilihCat">
                                  <div class="row">
                                      <div class="col-md-4 col-sm-4 col-xs-4">
                                          <a href="<?php echo base_url('proposal/baru/1');?>" style="padding-right: 165px;"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>Event</a>
                                      </div>
                                      <div class="col-md-4 col-sm-4 col-xs-4">
                                          <a href="<?php echo base_url('proposal/baru/2');?>"><i class="fa fa-desktop" aria-hidden="true"></i>Web Dev. / Design</a>
                                      </div>
                                      <div class="col-md-4 col-sm-4 col-xs-4">
                                          <a href="<?php echo base_url('proposal/baru/3');?>" style="padding-right: 85px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>UI/UX Design </a>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-4 col-sm-4 col-xs-4">
                                        <a href="<?php echo base_url('proposal/baru/4');?>" style="padding-right: 71px;"><i class="fa fa-comments-o" aria-hidden="true"></i>Interior Design</a>
                                      </div>
                                      <div class="col-md-4 col-sm-4 col-xs-4">
                                          <a href="<?php echo base_url('proposal/baru/5');?>" style="padding-right: 157px;"><i class="fa fa-globe" aria-hidden="true"></i>Arsitek</a>
                                      </div>
                                      <div class="col-md-4 col-sm-4 col-xs-4">
                                          <a href="<?php echo base_url('proposal/baru/6');?>" style="padding-right: 97px;"><i class="fa fa-clipboard" aria-hidden="true"></i>Copywriting</a>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-4 col-sm-4 col-xs-4">
                                        <a href="<?php echo base_url('proposal/baru/7');?>" style="padding-right: 117px;"><i class="fa fa-book" aria-hidden="true"></i>Translator</a>
                                      </div>
                                      <div class="col-md-4 col-sm-4 col-xs-4">
                                        <a href="<?php echo base_url('proposal/baru/8');?>" style="padding-right: 85px;"><i class="fa fa-money" aria-hidden="true"></i>Product Sales</a>
                                      </div>
                                      <div class="col-md-4 col-xs-4 col-sm-4">
                                        <a href="<?php echo base_url('proposal/baru/9');?>" style="padding-right: 143px;"><i class="fa fa-rss" aria-hidden="true"></i>Blogger</a>
                                      </div>
                                  </div>
                                </div>
                            </div>
                          </div>
                        </div>
                        <!-- Modal Category -->

                        <!-- <!-MODAL ADD CLIENT-->
                                  <div class="modal fade" id="modalKlien" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" >
                                      <div class="modal-dialog">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                              <h4 class="modal-title" id="">Masukan Data Klien Baru Anda</h4>
                                            </div>
                                            <div class="modal-body" >
                                              <div class="form-group text-left">
                                                <label for="pic" class="control-label">Nama PIC</label>
                                                <input type="text" class="form-control form-client-baru" id="pic" name="pic" placeholder="Nama PIC">
                                              </div>
                                              <div class="form-group text-left">
                                                <label for="email" class="control-label">Email</label>
                                                <input type="email" class="form-control form-client-baru" id="email" name="email" placeholder="Email Klien">
                                              </div>
                                              <div class="form-group text-left">
                                                <label for="perusahaan" class="control-label">Nama Tim / Perusahaan</label> <span style="float: right;">(optional)</span>
                                                <input type="text" class="form-control form-client-baru" id="perusahaan" name="perusahaan" placeholder="Nama Perusahaan">
                                              </div>
                                              <div class="form-group text-left">
                                                <label for="telephone" class="control-label">No. Handphone</label> <span style="float: right;">(optional)</span>
                                                <input type="text" class="form-control form-client-baru" id="telephone" onkeypress="var key = event.keyCode || event.charCode; return ((key >= 48 && key <= 57) || key == 8);" name="telephone" placeholder="Nomor Telephone">
                                              </div>
                                              <div class="form-group text-left">
                                                <label for="alamat" class="control-label">Alamat Usaha</label> <span style="float: right;">(optional)</span>
                                                <textarea class="form-control form-client-baru" id="alamat" name="alamat"></textarea>
                                              </div>
                                              <div class="form-group text-left">
                                                <label for="kota" class="control-label">Kota</label> <span style="float: right;">(optional)</span>
                                                <input type="text" class="form-control form-client-baru" id="kota" name="kota" placeholder="Kota">
                                              </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-theme" id="btntambahklien">Tambah</button>
                                            </div>
                                          </div>
                                      </div>
                                  </div>
              </div>
              </div>
      </div>
        </section>
        <!-- MODAL EXIST CLIENT-->
              <div class="modal fade" id="klienlama" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" id="closeclientexist" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title" id="">Pilih Klien</h4>
                        </div>
                        <div class="modal-body">
                          <table class="table table-hover table-striped" style="width:100%">
                            <tbody class="tabelKlien">
                                <?php foreach ($user_client as $row) {?>
                                  <tr class="row_client" clid="<?php echo $row->id_client; ?>" id="client_<?php echo $row->id_client; ?>">
                                      <td>
                                          <p>
                                              <b><?php echo $row->perusahaan;?></b>
                                          </p>
                                      </td>
                                      <td>
                                          <p>
                                              <b><?php echo $row->nama_pic;?></b>
                                          </p>
                                          <p><?php echo $row->telephone;?></p>
                                      </td>
                                      <td>
                                          <p><?php echo $row->alamat_usaha;?></p>
                                          <p><?php echo $row->kota;?></p>
                                      </td>
                                  </tr>
                              <?php } ?>
                              </tbody>
                              <tbody id="hasilCari"></tbody>
                          </table>
                            <!--
                            <?php // foreach ($user_client as $klienuser) {?>
                                <ul id="listKlienLama">
                                    <li><a href="#" onclick="pilihClientLama(<?php // echo $klienuser->id_client;?>)" class="pilihKlien"> <?php // echo $klienuser->nama_pic.' - '. $klienuser->perusahaan; ?></a></li>
                                </ul>
                            <?php // } ?>
                            -->
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                      </div>
                  </div>
              </div>
              <!-- END OF MODAL EXIST CLIENT-->
      <!-- </form> -->
  <!-- wrapperfdfsdafasdgdfh -->
</section><!-- /MAIN CONTENT -->
<script type="text/javascript">
function ValidateEmail(email) {
      //var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return expr.test(email);
  };

function pilihClientLama(id){
  $.ajax({
    type:"GET",
    url: "<?php echo base_url()?>client/pilihclient",
    data: {id:id},
    datatype: 'json',
    success: function(data){
      var datanya = JSON.parse(data);
      document.getElementById("namaKlien").value = datanya.nama;
      document.getElementById("perusahaanKlien").value = datanya.perusahaan;
      document.getElementById("emailKlien").value = datanya.email;
      document.getElementById("id-client").value = datanya.id;
      $('#klienlama').modal('hide');
    }, error:function(error){
      swal('error;' +eval(error));
    }
  });
}

$(document).ready(function(){
$('[name="btnselanjutnya"]').click(function(){
  var projectname = $('#projectname').val();
  var projectdesc = $('#projectdesc').val();
  var id_client = $('#id-client').val();
  var aboutyou =$('#aboutyou').val();
  var skills=$('[name="skills"]').val();
  var pengalaman = $('[name="pengalaman"]').val();
  var tahapan=$('[name="tahapan"]').val();
  var tanggaltahapan=  $('[name="tanggaltahapan"]').val();
  var diberi = $('[name="diberi"]').val();
  var biaya=$('[name="biaya"]').val();
  var benefit = $('#benefit').val();
  var nextstep = $('#nextstep').val();
  var syarat = $('[name="syarat"]').val();
});



$('[data-toggle="tooltip"]').tooltip();

$('#btntambahklien').click(function(){
  var pic =  $('#pic').val();
  var perusahaan =  $('#perusahaan').val();
  var email =  $('#email').val();
  var telephone =  $('#telephone').val();
  var alamat =  $('#alamat').val();
  var kota =  $('#kota').val();
  var user_id = <?=$this->session->userdata('user_id')?>;
  if(pic==''||email==''){
    swal("Oops","Pastikan Anda telah melengkapi isian","error");
    return false;
  }else if(!ValidateEmail(email)){
    swal("Oops","Pastikan format email sudah benar","error");
    return false;
  }else{
    $.ajax({
      type: "POST",
      url: '<?=base_url()?>client/addclient',
      data: {pic:pic,perusahaan:perusahaan,email:email,telephone:telephone,alamat:alamat,kota:kota,user_id:user_id },
      success: function(data)
        {
          if(data == 1)
          {
            swal("Berhasil","Klien berhasil ditambahkan","success")
            .then((value) => {
              location.reload();
            });
          }
          else
          {
            swal("Oops","Tambah klien gagal","error");
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            swal("Oops","Tambah klien gagal","error");
        }
    });
  }
});

$('#btnselanjutnya1').click(function(){
    var projectname = $('#projectname').val();
    var projectdesc = $('#projectdesc').val();
    if(projectname=='' || projectdesc==''){
      swal("Maaf","Anda harus mengisi formulir secara lengkap","error");
      return false;
    }
    else
    {
       $.ajax({
        type: "POST",
        data:{
            info_projek: projectname
        },
            url: '<?php echo base_url()."proposal/cek_nama_projek"; ?>',
        }).done(function(res){
           if(res > 0)
            {
              swal("Project dengan nama ini sudah ada.");
              //$('#projectinfo').trigger('click');
              return false;
            }
            else
            {
              $('#projectinfo').removeClass('active in');
              $('#aboutyoucontainer').addClass('in active');
            }
        });
    }
});

$(document).on('click','#btn-buat',function(){
      //var proposalid = $('#proposalid').val();
      //var pengalamanid = $('#pengalamanid').val();
      //var keuntunganid = $('#keuntunganid').val();
      var idclient = $('#id-client').val();

      var user_email = $('#user_email').val();
      var projectname = $('#projectname').val();
      var projectdesc = $('#projectdesc').val();
      var id_client = $('#id-client').val();
      var aboutyou =$('#aboutyou').val();
      var skills=[];
      $('[name="skills"]').each(function(){
        skills.push($(this).val());
      });

      //console.log(skills);

      var pengalaman=[];
      $('[name="pengalaman"]').each(function(){
        pengalaman.push($(this).val());
      });

      var tahapan=[];
      var tanggaltahapan=[];
      $('[name="tahapan"]').each (function() {
          tahapan.push($(this).val());
      });
      $('[name="tanggaltahapan"]').each(function(){
          tanggaltahapan.push($(this).val());
        });
      var diberi = [];
      $('[name="diberi"]').each(function(){
        diberi.push($(this).val());
      });
      var biaya=[];
      $('[name="biaya"]').each(function(){
        biaya.push($(this).val());
      });
      var benefit = $('#benefit').val();
      var nextstep = $('#nextstep').val();
      var syarat = [];
      $('[name="syarat"]').each(function(){
        syarat.push($(this).val());
      });
      if($('[name="syarat"]').val()!=''){
        $.ajax({
          type:'post',
          url: '<?=base_url()."proposal/create_proposal"?>',
          data:{projectname:projectname,
            projectdesc:projectdesc,
            user_email:user_email,
            //idproposal:proposalid,
            aboutyou:aboutyou,
            idclient:id_client,
            //idpengalaman : pengalamanid,
            pengalaman:pengalaman,
            kemampuan:skills,
            tanggaltahapan:tanggaltahapan,
            tahapan:tahapan,
            //idkeuntungan:keuntunganid,
            keuntungan:benefit,
            pemberian:diberi,
            biaya:biaya,
            nextstep:nextstep,
            syarat:syarat},
            success:function(data)
            {
              swal('Berhasil','Proposal berhasil disimpan','success')
              .then((value) => {
                var urli = '<?php echo base_url();?>';
                window.location.replace(urli+'proposal/lihat/'+data);
              });
            },
            error: function (error) {
              swal('Gagal','Silahkan cek ulang kembali isian anda', 'error');
            }
          });
      }else{
        swal('Maaf', 'Anda harus mengisi formulir secara lengkap', 'error');
        return false;
      }
});

$('.klienlama').on('click',function(){
  $('#klienlama').modal('show');
})

$('.modalKlien').on('click',function(){
  $('#modalKlien').modal('show');
})

$(document).on('keypress','.inpskill,.hasiladdskill',function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
    $('.addKemampuan').trigger('click');
    return false;
  }
});

// Create a new list item when clicking on the "Tambah Kemampuan" button
var ct = <?php echo $ct; ?>;
$('.addKemampuan').on('click',function(){
  var newdivinputskill = $('<div id="divadd_'+  parseInt(ct+1)  +'"></div>');

  var newinputskill = $('<input id="hasiladdskill" name="skills" type="text" class="form-control hasiladdskill">');

  var newbtndelete = $('<button ctid="'+ parseInt(ct+1) +'" class="btn btn-sm btn-danger btn-remove-skill"><i class="fa fa-trash"></i></button>');


  var inputValue = $('#skills').val();
  if(inputValue == '')
  {
    swal("Anda harus menuliskan kemampuan anda terlebih dahulu!");
  }
  else
  {
    ct++;
    $('.listSkill').append(newdivinputskill.append(newinputskill).append(newbtndelete));
    newinputskill.select();
  }
});

$(document).on('click','.btn-remove-skill',function(){
  var ctid = $(this).attr('ctid');
  $('#divadd_'+ctid).remove();
  ct--;
});


// Create a new list item when clicking on the "Tambah Pengalaman" button
var th = <?php echo $th; ?>;
$('.addPengalaman').on('click',function(){
  var newdivinputpengalaman = $('<div id="divaddpengalaman_'+  parseInt(th+1)  +'"></div>');

  var newinputpengalaman = $('<input id="inputpengalaman'+parseInt(th+1)+'" name="pengalaman" type="text" class="form-control hasiladdpengalaman">');

  var newbtndeletepengalaman = $('<button thid="'+ parseInt(th+1) +'" class="btn btn-sm btn-danger btn-remove-pengalaman"><i class="fa fa-trash"></i></button>');


  var latestpengalaman = $('#inputpengalaman'+th).val();
  if(latestpengalaman == '')
  {
    swal("Isikan pengalaman pada isian terakhir");
  }
  else
  {
    th++;
    $('.listPengalaman').append(newdivinputpengalaman.append(newinputpengalaman).append(newbtndeletepengalaman));
    newinputpengalaman.select();
  }
});

$(document).on('click','.btn-remove-pengalaman',function(){
  var thid = $(this).attr('ctid');
  $('#divaddpengalaman_'+thid).remove();
  th--;
});


$(document).on('keypress','.inptahapan,.hasiladdtahapan',function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
    $('.addThpnBtn').trigger('click');
    return false;
  }
});

var ch = 4;
$('.addThpnBtn').on('click',function(){
  var newdivinputtahapan = $('<div id="divaddtahapan_'+ch+'"></div>');

  var newinputtahapan = $('<input id="hasiladdtahapan" id="tahapan'+ch+'" name="tahapan" type="text" class="form-control hasiladdtahapan">');

  var newbtndeletetahapan = $('<button chid="'+ch+'" class="btn btn-sm btn-danger btn-remove-tahapan"><i class="fa fa-trash"></i></button>');


  var inputValuetahapan = $('#tahapan'+ parseInt(ch-1)).val();
  if(inputValuetahapan == '')
  {
    swal("Anda harus menuliskan tahapan pada isian terakhir.");
  }
  else
  {
    ch++;
    $('.listTahapan').append(newdivinputtahapan.append(newinputtahapan).append(newbtndeletetahapan));
    newinputtahapan.select();
  }
});

$(document).on('click','.btn-remove-tahapan',function(){
  var chid = $(this).attr('chid');
  $('#divaddtahapan_'+chid).remove();
  ch--;
});

var cb = 1;
$('.addYgDiberiBtn').on('click',function(){
  var newrowbiaya = $('<div id="traddbiaya_'+cb+'"></div>');
  var newinputdiberi = $('<div class="col-sm-7 col-xs-12 form-group"><input class="form-control" id="diberi'+cb+'" type="text" name="diberi" placeholder="Tahapan"></div>');
  var newinputbiaya = $('<div class="col-sm-4 col-xs-10 form-group" style="white-space:nowrap"><label style="display:inline-block">Rp</label><input style="display:inline-block" class="form-control" type="number" name="biaya" placeholder="Biaya" id="biaya'+cb+'" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);"></div>');
  var newbtndeletebiaya = $('<div class="col-sm-1 col-xs-2 text-right"><button cbid="'+cb+'" class="btn btn-danger btn-sm btn-remove-biaya" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button></div>');

  var inputdiberi = $('#diberi').val();
  var inputbiaya = $('#biaya').val();
  if(inputdiberi == '' || inputbiaya == '')
  {
    swal("Anda harus menuliskan biaya pertama terlebih dahulu!");
  }
  else
  {
    cb++;
    $('#listDiberi').append(newrowbiaya.append(newinputdiberi).append(newinputbiaya).append(newbtndeletebiaya));
    newinputdiberi.select();
  }

});

$(document).on('click','.btn-remove-biaya',function(){
  var cbid = $(this).attr('cbid');
  $('#traddbiaya_'+cbid).remove();
  cb--;
});

// Create a new list item of term and cond when clicking on the "Tambah syarat" button\
$(document).on('keypress','#terms,.hasiladdterm',function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
    $('.addBtnTerm').trigger('click');
    return false;
  }
});

var ctr = 8;
$('.addBtnTerm').on('click',function(){
  var newdivinputterm = $('<div id="divaddterm_'+ctr+'"></div>');
  var newinputterm = $('<input id="terms'+ctr+'" name="syarat" type="text" class="form-control hasiladdterm">');
  var newbtndeleteterm = $('<button ctrid="'+ctr+'" class="btn btn-sm btn-danger btn-remove-term"><i class="fa fa-trash"></i></button>');

  var inputValueterm = $('#terms'+ parseInt(ctr-1)).val();
  if(inputValueterm == '')
  {
    swal("Isi terlebih dahulu syarat & ketentuan pada isian terakhir");
  }
  else
  {
    ctr++;
    $('.listTerm').append(newdivinputterm.append(newinputterm).append(newbtndeleteterm));
    newinputterm.select();
  }
});

$(document).on('click','.btn-remove-term',function(){
  var ctrid = $(this).attr('ctrid');
  $('#divaddterm_'+ctrid).remove();
  ctr--;
});


$('.row_client').on('click',function(){
  var id_client = $(this).attr('clid');
  pilihClientLama(id_client);
});


})

</script>
