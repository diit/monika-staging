<body>
  <section id="main-content" >
     <section class="wrapper">
         <div class="container" style="width:100%">
            <div class="row mt" id="listProposal">
              <div class="col-xs-12">
              <button class="btn btn-theme03 pull-right tooltips" data-placement="bottom" data-original-title="Hemat waktu dengan duplikasi dari proposal yang sudah ada" style="margin-left: 10px;" id="start-intro-duplicate"><i class="fa fa-clone"></i> Duplikat</button>
              <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
                <a href="<?php echo site_url('proposal/baru'); ?>" id="createpro" class="btn btn-default pull-right">
                  <i class="fa fa fa-file-o" aria-hidden="true"></i> Buat Baru
                </a>
              <?php }else{ ?>
                <?php if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1){ ?>
                  <a href="<?php echo site_url('proposal/baru'); ?>" id="createpro" class="btn btn-default pull-right">
                    <i class="fa fa fa-file-o" aria-hidden="true"></i> Buat Baru
                  </a>
                <?php }else{ ?>
                  <?php if((int) $this->monikalib->getTotalProposal() + (int) $this->monikalib->getTotalInvoice() >= 2 + $this->monikalib->getTotalReferral()){ ?>
                    <button class="btn btn-default pull-right" id="reachlimit">
                      <i class="fa fa fa-file-o" aria-hidden="true"></i> Buat Baru
                    </button>
                  <?php }else{ ?>
                    <a href="<?php echo site_url('proposal/baru'); ?>" id="createpro" class="btn btn-default pull-right">
                      <i class="fa fa fa-file-o" aria-hidden="true"></i> Buat Baru
                    </a>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
                    <h3 style="margin-left:15px; margin-top:0">PROPOSAL <?php if($this->monikalib->getTotalProposal() == 0){ ?>
                            <span class="tooltips start-tutorial" data-placement="bottom" data-original-title="Lihat kembali tutorial cara mulai membuat proposal" style="font-size:12px;text-decoration:underline;cursor:pointer;"><i class="fa fa-info"></i> Tutorial</span>
                    <?php } ?></h3>
                  &nbsp;
                       <!--Kolom pencarian-->
                            <div class="form-group" style="margin:0px 0 20px 0">
                              <input type="text" class="form-control" id="cariProposal" placeholder="Cari Projek atau Nama klien anda">
                            </div>
                        <!--Kolom pencarian-->
                        <div id="hasilCariProposal"></div>
                        <?php
                          if($proposal && count($proposal) > 0){
//                              die(print_r($proposal));
                            foreach ($proposal as $key=>$prop) {
                              $id_projek = $prop->id;
                              $id_client = $prop->id_client;
                              $klien = $prop->nama_pic;
                              $email = $prop->email;
                              $namaproj = $prop->info_projek;
                              $tgl = $prop->tgl_buat;
                              $status = $prop->status;
                              $user_id = $prop->user_id;
                              $user_name = $prop->user_name;
                              $id_invoice = $prop->id_invoice;
                              $project_fee = $prop->project_fee;
                             ?>
                             <div class="col-xs-12 mb-short list_proposal" id="div_list_prop_<?php echo $id_projek; ?>">
                               <!-- WHITE PANEL - TOP USER -->
                               <div  style="height:auto;text-align:left;" class="row white-panel pn">
                                  <?php if($id_client == 1){ ?>
                                    <div class="corner-ribbon blue shadow">Contoh</div>
                                  <?php } ?>
                                   <div class="row">
                                       <div class="container" style="width:100%">
                                    <?php
                                        $random = md5(mt_rand(1,10000));
                                        $first = substr($random,0,5);
                                        $last = substr($random,5,10);
                                        $urlrand = $first.$id_projek.$last;
                                     ?>
                                           <a href="<?php echo site_url('proposal/lihat/'.$urlrand);?>">
                                             <div class="col-xs-8 white-header">
                                               <h5><b><?php echo $namaproj; ?></b></h5>
                                             </div>
                                           </a>
                                           <div class="col-xs-3 col-xs-offset-1" id="proposal_status">
                                          <?php if ($status == 0){ ?>
                                               <div <?php echo $key==0 ? 'id="propstat"':''; ?> class="label label-warning text-right prop-stat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Tersimpan</div>
                                             <?php } elseif ($status == 1){?>
                                               <div <?php echo $key==0 ? 'id="propstat"':''; ?> class="label label-primary text-right prop-stat"><i class="fa fa-eye" aria-hidden="true"></i>Sudah Dibaca</div>
                                             <?php } elseif ($status == 2){?>
                                               <div <?php echo $key==0 ? 'id="propstat"':''; ?> class="label label-success text-right prop-stat"><i class="fa fa-check" aria-hidden="true"></i>Disetujui</div>
                                             <?php } ?>
                                           </div>
                                       </div>
                                   </div>
                                   <div class="row">
                                       <div class="col-md-2 col-sm-2 col-xs-12">
                                         <p class="mt"><b>Klien</b></p>
                                         <p class="pcontent"><?php echo $klien; ?></p>
                                       </div>
                                       <div class="col-md-3 col-sm-2 col-xs-12">
                                         <p class="mt"><b>Email</b></p>
                                         <p class="pcontent"><?php echo $email; ?></p>
                                       </div>
                                       <div class="col-md-2 col-sm-2 col-xs-12">
                                         <p class="mt"><b>Est. Income</b></p>
                                         <p class="pcontent">
                                          <?php echo $project_fee == NULL ? '-':'Rp '.number_format($project_fee,0,',','.'); ?>
                                         </p>
                                       </div>
                                       <div class="col-md-2 col-sm-2 col-xs-12">
                                         <p class="mt"><b>Dipersiapkan oleh</b></p>
                                         <p class="pcontent"><?php echo $user_id == $this->session->userdata('user_id') ? 'Anda' : $user_name; ?></p>
                                       </div>
                                       <div class="col-md-3 col-sm-6 col-xs-12">
                                        <!-- Tombol Aksi -->
                                          <span class="btn-group group-action" style="padding:5px">
                                              <a class="btn btn-default btn-edit editprop tooltips" data-placement="top" data-original-title="Edit" id="editprop" name="btn-edit" href='<?php echo site_url('proposal/lihat/'.$urlrand);?>'>
                                                  <i class="fa fa-fw s fa-pencil" aria-hidden="true"></i>
                                              </a>
                                              <button class="btn btn-default dupliprop tooltips" data-placement="top" data-original-title="Duplikat" <?php echo $key == 0 ?'id="duplicatepro"':''; ?> pid="<?=$id_projek?>" pname="<?php echo $namaproj; ?>">
                                                  <i class="fa fa-fw fa-clone" aria-hidden="true"></i>
                                              </button>
                                              <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
                                                <a href="<?php echo site_url('proposal/stat/'.$urlrand); ?>" class="btn btn-default tooltips" id="analpro" data-placement="top" data-original-title="Statistik">
                                                      <i class="fa fa-line-chart" aria-hidden="true"></i>
                                                </a>
                                              <?php }else{ ?>
                                                <?php if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1){ ?>
                                                  <a href="<?php echo site_url('proposal/stat/'.$urlrand); ?>" class="btn btn-default tooltips" id="analpro" data-placement="top" data-original-title="Statistik">
                                                      <i class="fa fa-line-chart" aria-hidden="true"></i>
                                                  </a>
                                                <?php }else{ ?>
                                                  <button class="btn btn-default statpro tooltips" id="analpro" data-placement="top" data-original-title="Statistik" pid="<?=$id_projek?>">
                                                      <i class="fa fa-line-chart" aria-hidden="true"></i>
                                                  </button>
                                                <?php } ?>
                                              <?php } ?>
                                              <button class="btn btn-default btn-delete archprop tooltips" data-placement="top" data-original-title="Arsipkan" pid="<?=$id_projek?>">
                                                  <i class="fa fa-fw fa-archive" aria-hidden="true"></i>
                                              </button>
                                              <?php if($id_invoice == NULL){ ?>
                                                <button class="btn btn-default invoice-prop tooltips" data-placement="top" data-original-title="Generate Invoice" pid="<?=$id_projek?>" pname="<?=$id_client?>" pproj="<?=$namaproj?>">
                                                    <i class="fa fa-fw fa-list-alt"></i>
                                                </button>
                                              <?php }else{ ?>
                                                <a href="<?php echo site_url('invoice/lihat/'.$id_invoice); ?>" class="btn btn-default tooltips" data-placement="top" data-original-title="Lihat Invoice">
                                                    <i class="fa fa-fw fa-list-alt"></i>
                                                </a>
                                              <?php } ?>

                                         </span>
                                   <!-- Tombol Aksi -->
                                     </div>
                                 </div>
                               </div>
                               <span style="font-size:10px;position: absolute;right: 10px;bottom: 5px;">Dibuat pada <?php echo $this->monikalib->format_date_indonesia($tgl); ?></span>
                             </div><!-- /col-md-4 -->
                        <?php } ?>
                      <?php }else{ ?>
                      <div class="col-md-12">
                        <div class="col-md-6 col-sm-10 col-xs-12">
                          <div style="padding: 20px 20px 20px 0;margin-top: 60px;">
                            <img class="img-responsive" src="<?php echo base_url(); ?>assets/img/ss_proposal.png">
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-11 col-xs-12">
                          <div style="margin-top: 60px;padding: 20px;">
                            <h3>Menangkan Proyek Sekarang Juga!</h3>
                            <p style="font-size: 16px;line-height: 24px;">Monika adalah software berbasis website pertama yang memberikan kemudahan membuat dan melacak proposal yang telah dikirimkan. Fokus kami adalah kesuksesan Anda berdasarkan tingkat proposal yang disetujui oleh klien Anda.</p>
                            <a href="<?php echo site_url('proposal/baru'); ?>" class="btn btn-success">Buat Proposal Pertama Anda</a>
                          </div>
                        </div>
                      </div>
                     <?php } ?>
                  </div>
                </div>

                <div class="modal" id="modal_create_proposal" role="dialog" aria-labelledby="" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                         <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title text-center" id="" style="font-weight:bold">Buat Proposal</h4>
                         </div>
                         <div class="modal-body">
                          <h4 style="text-align: center;">Pilih template</h4>
                          <div class="row">
                            <?php if(count($list_template) > 0){ ?>
                              <?php foreach ($list_template as $lt) { ?>
                                 <div class="col-md-4 mb-short">
                                   <!-- WHITE PANEL - TOP USER -->
                                   <div style="height:auto;text-align:left;" class="white-panel pn">
                                     <div class="row" id="proposal_status" style="padding-bottom: 0;">
                                      <div class="col-md-12 white-header">
                                         <p class="pcontent"><?php echo $lt->info_projek; ?></p>
                                         <p><i class="fa fa-cloud-download"></i> 17x</p>
                                       </div>
                                     </div>
                                     <div class="row">
                                       <div class="col-md-12">
                                        <p class="btn-group group-action pull-right" style="margin-right: 10px;">
                                                  <a class="btn btn-default btn-edit btn-sm editprop tooltips" data-placement="bottom" data-original-title="Lihat" name="btn-edit" href='<?php echo site_url('template/lihat/'.$lt->id);?>'>
                                                      <i class="fa fa-fw s fa-eye" ></i>
                                                  </a>
                                                  <button class="btn btn-default btn-sm duplitemplate tooltips" data-placement="bottom" data-original-title="Pilih" pid="4">
                                                      <i class="fa fa-fw fa-check"></i>
                                                  </button>

                                             </p>
                                       </div>
                                     </div>
                                   </div>
                                <?php } ?>
                              <?php } ?>
                             </div>
                          </div>
                          <h4 style="text-align: center;">atau</h4>
                          <div class="row" style="text-align: center;">
                            <a href="<?php echo base_url('proposal/baru');?>" class="btn btn btn-theme03 btn-lg">Buat Baru</a>
                          </div>
                         </div>
                      </div>
                  </div>
                </div>

                <div class="modal" id="modal_referral" role="dialog" aria-labelledby="" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                     <div class="modal-content">
                         <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title text-center" id="" style="font-weight:bold">Akses Masih Terbatas</h4>
                         </div>
                         <div class="modal-body">
                          <p>Anda masih menggunakan Plan Personal dengan limit hanya 2 proposal dan 2 invoice. Untuk menambah limit proposal, undang kerabat Anda untuk bergabung:</p>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="label-control">Masukkan alamat email (bisa input lebih dari 1 email)</label>
                                    <input type="text" class="form-control" name="friend_email" data-role="tagsinput" id="friend_email">
                                  </div>
                              </div>
                              <div class="col-md-12">
                                <button type="button" class="btn btn-primary btn-refer-to-friend"><i class="fa fa-send" aria-hidden="true"></i>
                                  Kirim
                                </button>
                                <span>atau</span>
                                <a href="<?php echo site_url('plan'); ?>" class="btn btn-success btn-priority-access">
                                  Upgrade Plan
                                </a>
                              </div>
                            </div>
                         </div>
                      </div>
                  </div>
                </div>
         </div>
     </section>
  </section>
