<?php
//$this->load->library('fpdf_gen');
//header('Content-type: application/pdf');
$namaprojek = $detail_proposal->info_projek;
$id_klien = $detail_proposal->id_client;
$klien = $detail_proposal->nama_pic;
$perusahaan = $detail_proposal->perusahaan;

  if($detail_proposal && ($detail_proposal->aboutyou != NULL || $detail_proposal->aboutyou != ''))
	{
	  //$aboutyou = nl2br($detail_proposal->aboutyou);
	  $aboutyou = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detail_proposal->aboutyou));
	}
	else
	{
	  $aboutyou = '';
	}
	$email_klien = $detail_proposal->email;
	$oleh = $detail_proposal->user_name;
    if($this->uri->segment(1) == 'userview'){
        $team = '';
    } else{
        $team = $detail_proposal->team_name;
    }
	$kontak = $detail_proposal->user_email;
	if($detail_proposal && ($detail_proposal->project_desc != NULL || $detail_proposal->project_desc != ''))
	{
		//$overview = nl2br($detail_proposal->project_desc);
		$overview = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detail_proposal->project_desc));
	}
	else
	{
	  $overview = '';
	}
	$pengalaman = $detail_proposal->pengalaman;
	if($detail_proposal && ($detail_proposal->nextstep != NULL || $detail_proposal->nextstep != ''))
	{
		//$nextstep = nl2br($detail_proposal->nextstep);
		$nextstep = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detail_proposal->nextstep));
	}
	else
	{
	  $nextstep = '';
	}
	$tgl = $detail_proposal->tgl_buat;
	if($detailkeuntungan && ($detailkeuntungan->keuntungan != NULL || $detailkeuntungan->keuntungan != ''))
		{
			//$keuntungan = nl2br($detailkeuntungan->keuntungan);
			$keuntungan = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detailkeuntungan->keuntungan));
		}
		else
		{
		  $keuntungan = '';
		}
			$duplicate = $detail_proposal->duplicate_from;
			$preview = $detail_proposal->preview;
			$style = $detail_proposal->style;

ini_set('max_execution_time', 300); //300 seconds = 5 minutes
//$pdf= new FPDF('P','mm','A4'); //L For Landscape / P For Portrait
$pdf= new FPDF('P','mm','A4'); //L For Landscape / P For Portrait
$judul = $detail_proposal->info_projek;
//$pdf->Footer($judul);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->AddFont('Avenir','','avenir.php');
$pdf->AddFont('Verdana','','verdana.php');
$pdf->AddFont('OpenSans','','opensans-regular.php');
$pdf->AddFont('Pecita','','pecita.php');
$pdf->SetTitle('Proposal');

function hex2rgb( $colour ) {
        if ( $colour[0] == '#' ) {
                $colour = substr( $colour, 1 );
        }
        if ( strlen( $colour ) == 6 ) {
                list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
        } elseif ( strlen( $colour ) == 3 ) {
                list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
        } else {
                return false;
        }
        $r = hexdec( $r );
        $g = hexdec( $g );
        $b = hexdec( $b );
        return array($r,$g,$b);
}

$headbgrgb = hex2rgb(count($style) > 0 ? $style['headerbgcolor'] :'#656464');
$txtheadrgb = hex2rgb(count($style) > 0 ? $style['headertextcolor'] :'#ffffff');

//Box header color or img

if(count($style) > 0)
{
    if(!empty($style['headerimageurl']))
    {
        $pdf->Image($style['headerimageurl'],0,0,210,75,0);
    }
    else
    {
        $pdf->SetFillColor($headbgrgb['0'],$headbgrgb['1'],$headbgrgb['2']);
        $pdf->Rect(0,0,210,75,'F');
    }
}
else
{
    $pdf->SetFillColor($headbgrgb['0'],$headbgrgb['1'],$headbgrgb['2']);
    $pdf->Rect(0,0,210,75,'F');
}

if(count($style) > 0)
{
    if($style['fontstyle'] == 'Avenir'){
        $pdf->SetFont('Avenir','',10);
    }elseif($style['fontstyle'] == 'Arial'){
        $pdf->SetFont('Arial','',10);
    }elseif($style['fontstyle'] == 'Verdana'){
        $pdf->SetFont('Verdana','',10);
    }elseif($style['fontstyle'] == 'Open Sans'){
        $pdf->SetFont('OpenSans','',10);
    }else{
        $pdf->SetFont('Arial','',10);
    }
}
else
{
    $pdf->SetFont('Avenir','',10);
}

//Header
if (!empty($style['logoimageurl'])){
    $gambar = $style['logoimageurl'];
    $pdf->Image($gambar,35,15,33,0,'png');
} else{
    $gambar = base_url('assets/images/searchicon.png');
    $pdf->Image($gambar,0,0,0.00001,0);
}
$pdf->SetTextColor($txtheadrgb['0'],$txtheadrgb['1'],$txtheadrgb['2']);
//die(".$style['fontstyle'].");
$pdf->Ln();
$pdf->Cell(80);
$pdf->Cell(44,5,$detail_proposal->info_projek,0,1,'L');
$pdf->Ln(5);
$pdf->Cell(80);
$pdf->Cell(44,5,'Dipersiapkan Untuk',0,0,'L');
$pdf->Ln(8);
$pdf->Cell(80);
$pdf->Cell(44,5,$perusahaan,0,0,'L');
$pdf->Ln();
$pdf->Cell(80);
$pdf->Cell(44,5,$klien,0,0,'L');
$pdf->Ln(8);
$pdf->Cell(80);
$pdf->Cell(44,5,$this->monikalib->format_date_indonesia($tgl),0,0,'L');
$pdf->Ln(7);
$pdf->Cell(80);
$pdf->Cell(44,5,'Oleh',0,0,'L');
$pdf->Cell(44,5,'Kontak',0,0,'L');
$pdf->Ln(5);
$pdf->Cell(80);
$pdf->Cell(44,5,$oleh,0,0,'L');
$pdf->MultiCell(0,5,$kontak,0,'L');
$pdf->Ln(1);
$pdf->Cell(80);
$pdf->Cell(44,5,$team,0,0,'L');

if(count($style) > 0)
{
    $section_layout = $style['section_layout'];
}
else
{
    $section_layout = 0;
}

if ($section_layout == 0){
				$pdf->SetTextColor(0,0,0);
				$pdf->Ln(30);
        //Section
        if($detail_proposal && count($detail_proposal) > 0){
					if($detail_proposal->project_desc !== NULL && $detail_proposal->project_desc !== ''){
            $pdf->Cell(10);
            $pdf->Cell(70,5,'Overview',0,0,'L');
            $pdf->MultiCell(0,5,$detail_proposal->project_desc,0,'L');
						$pdf->Ln(10);
					}
//					die(print_r(isset($detail_proposal->project_dest)));
        }
        if($detail_proposal && count($detail_proposal) > 0){
						if( $detail_proposal->aboutyou !== NULL && $detail_proposal->aboutyou !== ''){
            $pdf->Cell(10);
            $pdf->Cell(70,5,'Tentang Saya',0,0,'L');
            $pdf->MultiCell(0,5,$detail_proposal->aboutyou,0,'L');
            $pdf->Ln(10);
					}
        }
        if($pengalaman && $pengalaman > 0){
					if($pengalaman['0']['deleted'] !== 1){
            $pdf->Cell(10);
            $pdf->Cell(70,5,'Portfolio',0,0,'L');
            $i=1;
            foreach($pengalaman as $tb){
                if ($i==1 ){
                    $pdf->MultiCell(0,7,$i.'. '.$tb['pengalaman'],0,'L');
                } else {
                    $pdf->Cell(80);
                    $pdf->Cell(11.9,7,$i.'. '.$tb['pengalaman'],0,1,'L');
                }
                $i++;
            }
						$pdf->Ln(10);
					}
        }

        if($detailkemampuan && count($detailkemampuan) > 0){
            $pdf->Cell(10);
            $pdf->Cell(70,5,'Skill & Value',0,0,'L');
            $a=1;
            foreach($detailkemampuan as $kemampuan){
                if ($a==1 ){
                    $pdf->MultiCell(0,5,$a.'. '.$kemampuan->skill,0,'L');
                } else {
                    $pdf->Cell(80);
                    $pdf->Cell(11.9,7,$a.'. '.$kemampuan->skill,0,1,'L');
                }
                $a++;
            }
					  $pdf->Ln(10);
        }
        if($detailtahapan && count($detailtahapan) > 0){
            $pdf->Cell(10);
            $pdf->Cell(70,5,'Tahapan Kerja',0,0,'L');
            $b=1;
            foreach($detailtahapan as $tahapan){
                if ($b==1 ){
                    $pdf->MultiCell(0,5,$b.'. '.$tahapan->tahapan,0,'L');
                } else {
                    $pdf->Cell(80);
                    $pdf->Cell(11.9,7,$b.'. '.$tahapan->tahapan,0,1,'L');
                }
                $b++;
            }
						$pdf->Ln(10);
        }

        if($detailkeuntungan && count($detailkeuntungan) > 0){
                $pdf->Cell(10);
                $pdf->Cell(70,5,'Hasil Pekerjaan',0,0,'L');
                $pdf->MultiCell(0,5,$detailkeuntungan->keuntungan,0,'L');
        }

        //Additional Section
        if($detailtambahan && count($detailtambahan) > 0){
                    $g=1;
                    foreach ($detailtambahan as $tambahan) {
                        $pdf->Ln(10);
                        $pdf->Cell(10);
                        $pdf->Cell(70,5,$tambahan->section_name,0,0,'L');
                        if (($tambahan->section_type == 'image')){
                            $pdf->MultiCell(0,5,'Lampiran '.$g,0,'L');
                            $g++;
                        }elseif(($tambahan->section_type == 'text')){
                            foreach($tambahan->detail as $dt){
                            $pdf->MultiCell(0,5,$dt->section_detail_content,0,'L');
                            }
                        }

                    }
					           $pdf->Ln(10);
        }

        //End of Additional Section

        if($detailpemberian && count($detailpemberian) > 0){
            $pdf->Cell(10);
            $pdf->Cell(70,5,'Biaya',0,0,'L');
            $c=1;
            $totalbiaya = 0;
            foreach($detailpemberian as $diberi){
                $totalbiaya += $diberi->harga;
                if ($c==1 ){
                    $pdf->Cell(80,7,$diberi->pemberian,0,0,'L');
                    $pdf->MultiCell(0,7,number_format($diberi->harga,0,',','.'),0,'R');
                } else {
                    $pdf->Cell(80);
                    $pdf->Cell(80,7,$c.'. '.$diberi->pemberian,0,0,'L');
                    $pdf->MultiCell(0,7,'Rp '.number_format($diberi->harga,0,',','.'),0,'R');
                }
                $c++;
            }

            $pdf->Ln();
            $pdf->Cell(145);
            $pdf->Cell(20,7,'Total',0,0,'L');
            $pdf->MultiCell(0,5,'Rp '.number_format($totalbiaya,0,',','.'),0,'R');
						$pdf->Ln(10);
        }

				if($detail_proposal->nextstep !== NULL && $detail_proposal->nextstep !== ''){
						$pdf->Cell(10);
						$pdf->Cell(70,5,'Langkah Selanjutnya',0,0,'L');
						$pdf->MultiCell(0,5,$detail_proposal->nextstep,0,'L');
						$pdf->Ln(10);
				}

				if($detailsyarat && count($detailsyarat) > 0){
						$pdf->Cell(10);
						$pdf->Cell(70,5,'Syarat dan Ketentuan',0,0,'L');
						$a=1;
						foreach($detailsyarat as $syarat){
								if ($a==1 ){
										$pdf->MultiCell(0,5,$a.'. '.$syarat->syaratketentuan,0,'L');
								} else {
										$pdf->Cell(80);
										$pdf->MultiCell(0,5,$a.'. '.$syarat->syaratketentuan,0,'L');
								}
								$a++;
						}
				}
        //End of Section
}elseif($section_layout == 1){
        $pdf->SetTextColor(0,0,0);
        $pdf->Ln(30);

				if($detail_proposal && count($detail_proposal) > 0){
					if($detail_proposal->project_desc !== NULL && $detail_proposal->project_desc !== ''){
						$pdf->Cell(10);
						$pdf->Cell(70,5,'Overview',0,1,'L');
						$pdf->Ln();
						$pdf->Cell(10);
						$pdf->MultiCell(0,5,$detail_proposal->project_desc,0,'L');
						$pdf->Ln(10);
					}
				}
				 if($detail_proposal && count($detail_proposal) > 0){
						if( $detail_proposal->aboutyou !== NULL && $detail_proposal->aboutyou !== ''){
							$pdf->Cell(10);
							$pdf->Cell(70,5,'Tentang Saya',0,1,'L');
							$pdf->Ln();
							$pdf->Cell(10);
							$pdf->MultiCell(0,5,$detail_proposal->aboutyou,0,'L');
							$pdf->Ln(10);
						}
        }

				if($pengalaman && $pengalaman > 0){
					if($pengalaman['0']['deleted'] !== 1){
							$pdf->Cell(10);
							$pdf->Cell(70,5,'Portfolio',0,0,'L');
							$pdf->Ln();
							$i=1;
							foreach($pengalaman as $tb){
											$pdf->Ln(5);
											$pdf->Cell(10);
											$pdf->MultiCell(0,2,$i.'. '.$tb['pengalaman'],0,'L');
									$i++;
							}
        			$pdf->Ln(10);
					}
				}

				if($detailkemampuan && count($detailkemampuan) > 0){
        		$pdf->Cell(10);
						$pdf->Cell(70,5,'Skill & Value',0,0,'L');
						$pdf->Ln();
						$i=1;
						foreach($detailkemampuan as $kemampuan){
										$pdf->Ln(5);
										$pdf->Cell(10);
										$pdf->MultiCell(0,2,$i.'. '.$kemampuan->skill,0,'L');
								$i++;
						}
						$pdf->Ln(10);
				}

			 	if($detailtahapan && count($detailtahapan) > 0){
						$pdf->Cell(10);
						$pdf->Cell(70,5,'Tahapan Kerja',0,0,'L');
						$pdf->Ln();
						$i=1;
						foreach($detailtahapan as $tahapan){
										$pdf->Ln(2);
										$pdf->Cell(10);
										$pdf->MultiCell(0,5,$i.'. '.$tahapan->tahapan,0,'L');
								$i++;
						}
					$pdf->Ln(10);
				}

				if($detailkeuntungan && count($detailkeuntungan) > 0){
						$pdf->Cell(10);
						$pdf->Cell(70,5,'Hasil Pekerjaan',0,1,'L');
						$pdf->Ln();
						$pdf->Cell(10);
						$pdf->MultiCell(0,5,$detailkeuntungan->keuntungan,0,'L');
				}

        //Additional Section
        if($detailtambahan && count($detailtambahan) > 0){
            $g=1;
            foreach ($detailtambahan as $tambahan) {
                $pdf->Ln(10);
                $pdf->Cell(10);
                $pdf->Cell(70,5,$tambahan->section_name,0,0,'L');
                $pdf->Ln();
                $pdf->Cell(10);
                if (($tambahan->section_type == 'image')){
                    $pdf->Cell(70,5,'Lampiran '.$g,0,0,'L');
                    $g++;
                }elseif(($tambahan->section_type == 'text')){
                    foreach($tambahan->detail as $dt){
                        $pdf->MultiCell(0,5,$dt->section_detail_content,0,'L');
                        $pdf->Ln(-5);
                    }
                }

            }
					 $pdf->Ln(10);
        }
        //End of Additional Section

  			if($detailpemberian && count($detailpemberian) > 0){
						$pdf->Cell(10);
						$pdf->Cell(70,5,'Biaya',0,0,'L');
						$pdf->Ln(8);
						$c=1;
						$totalbiaya = 0;
						foreach($detailpemberian as $diberi){
								$totalbiaya += $diberi->harga;
			//            if ($c==1 ){
			//                $pdf->Cell(80,7,$diberi->pemberian,0,0,'L');
			//                $pdf->MultiCell(0,7,number_format($diberi->harga,0,',','.'),0,'R');
			//            } else {
										$pdf->Cell(10);
										$pdf->Cell(80,5,$diberi->pemberian,'T',0,'L');
			//                $pdf->Cell(80,7,'Rp '.number_format($diberi->harga,0,',','.'),0,0,'R');
										$pdf->MultiCell(0,5,'Rp '.number_format($diberi->harga,0,',','.'),'T','R');
			//            }
								$c++;
						}

						$pdf->Ln();
						$pdf->Cell(10);
			//        $pdf->Cell(135);
						$pdf->Cell(135,10,'Total','T',0,'R');
						$pdf->MultiCell(0,10,'Rp '.number_format($totalbiaya,0,',','.'),'T','R');
						$pdf->Ln(10);
				}

				if($detail_proposal->nextstep !== NULL && $detail_proposal->nextstep !== ''){
						$pdf->Cell(10);
						$pdf->Cell(70,5,'Langkah Selanjutnya',0,0,'L');
						$pdf->Ln(8);
						$pdf->Cell(10);
						$pdf->MultiCell(0,5,$detail_proposal->nextstep,0,'L');
						$pdf->Ln(10);
				}

				if($detailsyarat && count($detailsyarat) > 0){
						$pdf->Cell(10);
						$pdf->Cell(70,5,'Syarat dan Ketentuan',0,0,'L');
						$pdf->Ln(8);
						$a=1;
						foreach($detailsyarat as $syarat){
										$pdf->Cell(10);
										$pdf->MultiCell(0,5,$a.'. '.$syarat->syaratketentuan,0,'L');
		//            }
								$a++;
						}
				}
        //End of Section
}

//Tandan Tangan
$pdf->SetFont('Pecita','',20);
$pdf->Ln(40);
$pdf->Cell(5);
$pdf->Cell(85,20,$oleh,'B',0,'C');
$pdf->Cell(10);
if ($sign < 2){
    $pdf->Cell(85,20,' ','B',0,'C');
} elseif ($sign == 2){
    $pdf->Cell(85,20,$klien,'B',0,'C');
}

if(count($style) > 0)
{
    if($style['fontstyle'] == 'Avenir'){
        $pdf->SetFont('Avenir','',10);
    }elseif($style['fontstyle'] == 'Arial'){
        $pdf->SetFont('Arial','',10);
    }elseif($style['fontstyle'] == 'Verdana'){
        $pdf->SetFont('Verdana','',10);
    }elseif($style['fontstyle'] == 'Open Sans'){
        $pdf->SetFont('OpenSans','',10);
    }else{
        $pdf->SetFont('Arial','',10);
    }
}
else
{
    $pdf->SetFont('Avenir','',10);
}

$pdf->Ln(13);
$pdf->Cell(5);
$pdf->Cell(85,25,$oleh,0,0,'C');
$pdf->Cell(10);
$pdf->Cell(85,25,$klien,0,0,'C');

if ($detailtambahan && count($detailtambahan)>0){
    //Lampiran Gambar
    $pdf->AddPage();
    $dta = 1;
    foreach ($detailtambahan as $tambahan) {
        if ($tambahan->section_type == 'image'){
            $lamp = 'Lampiran '.$dta;
            $pdf->Cell(10);
            $pdf->Cell(70,10,$lamp,0,2,'L');
            $in = 1;
                foreach($tambahan->detail as $dt){
    //                if ($in % 2 = 0){
    //                    $pdf->Cell(60,5,$pdf->Image($dt->section_detail_content, $pdf->GetX(), $pdf->GetY(), 55),0,2,'L');
    //                }else{
                        $pdf->Cell(60,5,$pdf->Image($dt->section_detail_content, $pdf->GetX(), $pdf->GetY(), 55),0,0,'L');
    //                }

                }
            $pdf->Ln(80);
            $dta++;
        }

    }
}

$pdf->Output('I','Proposal - '.$detail_proposal->info_projek.'.pdf');


?>
