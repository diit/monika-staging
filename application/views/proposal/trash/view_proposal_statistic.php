<section id="main-content">
    <section class="wrapper">
        <div class="row" style="margin:0px">
            <div class="container" style="margin:20px;">
                <div class="col-lg-12 col-xs-12 white-panel-abt" style="text-align: left;">
                    <div class="col-lg-12" style="padding: 20px;">
                      <a href="<?php echo site_url('home'); ?>"><i class="fa fa-long-arrow-left"></i> Kembali</a>
                      <h4>Statistik
                          <?php
                            $random = md5(mt_rand(1,10000));
                            $first = substr($random,0,5);
                            $last = substr($random,5,10);
                            $urlrand = $first.$detail_stat->id.$last;
                         ?>
                        <a href="<?php echo site_url('proposal/lihat/'.$urlrand); ?>" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> Lihat Proposal</a>
                      </h4>
                      <hr>
                        <h5>Judul Proposal : <?php echo $detail_stat->info_projek; ?></h5>
                            <div class="row">
                              <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
                                  <div class="col-md-5">
                                      <p style="text-align:center;font-weight: bold;">Statistik Konten</p>
                                      <div style="margin-top: 20px;" class="table-responsive">
                                        <table class="table table-hover">
                                          <thead>
                                            <tr>
                                              <th>Konten</th>
                                              <th>Avg. Waktu Baca (detik)</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php if(count($avg_readtime) > 0) { ?>
                                                <?php foreach ($avg_readtime as $ar) { ?>
                                                  <tr>
                                                    <td><?php echo count(explode("section_",$ar->section)) == 2 ? ucfirst(explode("section_",$ar->section)[1]) : ucfirst(explode("section_",$ar->section)[0]); ?></td>
                                                    <td><?php echo round($ar->rata); ?></td>
                                                  </tr>
                                                <?php } ?>
                                              <?php }else{ ?>
                                                <tr>
                                                  <td colspan="2"><p style="text-align: center;">Belum ada data</p></td>
                                                </tr>
                                              <?php } ?>
                                          </tbody>
                                        </table>
                                      </div>
                                  </div>
                                  <div class="col-md-5 col-xs-12">
                                    <div style="margin-top: 30px;" class="row">
                                      <p class="col-md-8 col-xs-8"><label>Rata-rata proposal dibaca pada pukul</label></p>
                                      <p class="col-md-3 col-xs-3"><?php echo $best_time_read && $best_time_read != NULL ? $best_time_read.':00' : ' - '; ?></p>
                                    </div>
                                    <div class="row">
                                      <p class="col-md-8 col-xs-8"><label>Proposal dibaca sebanyak</label></p>
                                      <p class="col-md-3 col-xs-3"><?php echo $detail_stat->counter_baca && round($detail_stat->counter_baca) > 0 ? round($detail_stat->counter_baca) : '0'; ?> x </p>
                                    </div>
                                  </div>
                              <?php }else{ ?>
                                <?php if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1){ ?>
                                  <div class="col-md-5">
                                      <p style="text-align:center;font-weight: bold;">Statistik Konten</p>
                                      <div style="margin-top: 20px;" class="table-responsive">
                                        <table class="table table-hover">
                                          <thead>
                                            <tr>
                                              <th>Konten</th>
                                              <th>Avg. Waktu Baca (detik)</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php if(count($avg_readtime) > 0) { ?>
                                                <?php foreach ($avg_readtime as $ar) { ?>
                                                  <tr>
                                                    <td><?php echo count(explode("section_",$ar->section)) == 2 ? ucfirst(explode("section_",$ar->section)[1]) : ucfirst(explode("section_",$ar->section)[0]); ?></td>
                                                    <td><?php echo round($ar->rata); ?></td>
                                                  </tr>
                                                <?php } ?>
                                              <?php }else{ ?>
                                                <tr>
                                                  <td colspan="2"><p style="text-align: center;">Belum ada data</p></td>
                                                </tr>
                                              <?php } ?>
                                          </tbody>
                                        </table>
                                      </div>
                                  </div>
                                  <div class="col-md-5 col-xs-12">
                                    <div style="margin-top: 30px;" class="row">
                                      <p class="col-md-8 col-xs-8"><label>Rata-rata proposal dibaca pada pukul</label></p>
                                      <p class="col-md-3 col-xs-3"><?php echo $best_time_read && $best_time_read != NULL ? $best_time_read.':00' : ' - '; ?></p>
                                    </div>
                                    <div class="row">
                                      <p class="col-md-8 col-xs-8"><label>Proposal dibaca sebanyak</label></p>
                                      <p class="col-md-3 col-xs-3"><?php echo $detail_stat->counter_baca && round($detail_stat->counter_baca) > 0 ? round($detail_stat->counter_baca) : '0'; ?> x </p>
                                    </div>
                                  </div>
                                <?php }else{ ?>
                                  <div class="col-md-5">
                                    <h5>Anda masih menggunakan Plan Personal. Untuk bisa melihat statistik proposal silahkan upgrade plan Anda minimal ke Startup.</h5>
                                  </div>
                                  <div class="col-md-12">
                                    <a href="<?php echo site_url('plan'); ?>" class="btn btn-success btn-priority-access">
                                      Upgrade Plan
                                    </a>
                                  </div>
                                <?php } ?>
                              <?php } ?>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
