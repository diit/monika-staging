<style media="screen">
#main-content .wrapper .col-md-12 .showback .row .view-project-info .mt ol{
  font-family: 'Avenir';
  font-weight: 500;
  line-height: 1.1;
  font-size: 20px;
}
#main-content .wrapper .col-md-12 .showback .row .view-project-info .mt textarea, #main-content .wrapper .col-md-12 .showback .row .view-project-info .mt input, #main-content .wrapper .col-md-12 .showback .row .view-project-info table tbody tr td{
   font-family: 'Avenir';
   font-weight: 500;
   line-height: 1.1;
   font-size: 20px;
 }
#main-content .wrapper .col-md-12 .showback .row .view-project-info .mt textarea, #main-content .wrapper .col-md-12 .showback .row .view-project-info .mt input{
  width: 100%;
  border: none;
  resize: none;
  overflow: hidden;
  border-bottom: 2px solid transparent;
}
#main-content .wrapper .col-md-12 .showback .row .view-project-info .mt textarea:focus, #main-content .wrapper .col-md-12 .showback .row .view-project-info .mt input:focus, #main-content .wrapper .col-md-12 .showback #ttdSection .ttdOleh:focus{
  border: none;
  border-bottom: 2px solid #46bfb2;
  outline: none;
}
#main-content .wrapper .col-md-12 .showback #ttdSection .ttdOleh{
  width: 100%;
  border: none;
  border-bottom: 2px solid #797979;
  margin-bottom: 20px;
  font-family: 'pecita';
  font-size: 30pt;
  text-align: center;
}
#main-content .wrapper .col-md-12 .showback #ttdSection .ttdOleh[type="text" i]:disabled{
  background-color: transparent;
  cursor: no-drop;
}
#main-content .wrapper .col-md-12 .showback .row .view-project-info #headerinfo h4{
  margin-top:0px;
}
.btn-fab{
  position:fixed;
  padding: 0;
}

.alert-signed{
  position:absolute;
  bottom:1150px;
  right:-490px;
  padding: 0;
}

.black-box{
  background-color: #393939;
  margin-top: 10px;
  border-radius: 3px;
}

.black-box .content{
  margin: 10px;
  margin: 10px;
  padding-top: 10px;
  padding-bottom: 10px
}
.black-box .content .form-input{
  border-radius: 4px;
  height: 34px;
  margin-right: 3px;
}

.content {
  font-family: 'Avenir';
  font-weight: 300;
  font-size: 16px;
}

.caption {
  font-family: 'Avenir';
  font-weight: 600;
  font-size: 18px;
}

.caption-mini {
  font-family: 'Avenir';
  font-weight: 600;
  font-size: 13px;
}

.tabelKlien table tr
{
  cursor: pointer;
}

</style>


<div style="top: 0;" class="floating-proposal-panel">
<div class="proposal-menu-panel-right">
  <h3 class="hidden-xs">PROPOSAL</h3>
    <h4 class="hidden-lg hidden-md hidden-sm" style="margin-top:22px">PROPOSAL</h4>
</div>
<div style="display: flex;margin-left: auto;-webkit-box-pack: end;justify-content: flex-end;">
  <!-- hidden 25 April, adit -->
  <?php // if((!empty($plan)) && ($plan->status == 1) && ($plan->plan_id != 1) && ($plan->plan_id != 2)) { ?>
    <!--
    <div id="download_proposal" style="color: rgb(149, 155, 163);cursor: pointer;box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 20px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
        <span class="hidden-xs"><i class="fa fa-cloud-download"></i>  DOWNLOAD PDF</span>
        <i class="fa fa-cloud-download hidden-lg hidden-md hidden-sm"></i>
    </div>
    -->
  <?php // } ?>
  <?php if ($detail_proposal['id_klien'] > 1){ ?>
    <?php if ($sign == 2){ ?>
      <div style="color: rgb(149, 155, 163);box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 20px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
         <span><i class="fa fa-lock"></i>  TELAH DISETUJUI</span>
      </div>
    <?php }else{ ?>
      <div id="acc_proposal" prop-id="<?php echo $id_projek ?>" style="background-color:#5ee2d4;color: #fff;cursor: pointer;box-sizing: border-box;font-size: 14px;font-weight:bold;display: flex;line-height: 100%;text-transform: uppercase;height: 100%;-webkit-box-align: center;align-items: center;-webkit-box-pack: center;justify-content: center;padding: 0px 20px;transition: all 0.3s;border-left: 1px solid rgb(216, 218, 220);">
        <span><i class="fa fa-check"></i>  SETUJUI</span>
      </div>
    <?php } ?>
  <?php } ?>
</div>
</div>

<section id="main-content" style="margin-left: 0;">
  <section class="wrapper" style="margin-top: 30px;">
        <div class="row mt">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="row mt">
            <div class="col-md-12 col-sm-12 col-lg-12">
              <?php $this->load->view('proposal/template/user_default',$detail_proposal); // bagian "proposal/template/default" nantinya diganti jd $template_proposal ?>
            </div>

              <div class="modal" id="modal_add_feedback" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                         <div class="modal-header">
                           <h4 class="modal-title text-center" id="" style="font-weight:bold">Berikan Feedback</h4>
                         </div>
                         <div class="modal-body">
                            <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="label-control" id="feedback_section_name"></label>
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <textarea class="form-control" rows="10" id="feedback"></textarea>
                                  </div>
                              </div>
                            </div>
                         </div>
                         <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn btn-info btn-save-feedback" section=""><i class="fa fa-check" aria-hidden="true"></i>
                              Simpan
                            </button>
                         </div>
                      </div>
                  </div>
                </div>

            <!--FLOATING-->
              <?php if ($sign != 2): ?>
                <div style="visibility: hidden;" class="floatButton" >
                  <div class="row btn-fab" >
                    <div id="console">
                      <p>--sementara ditampilkan dulu<br>untuk Johan--</p>
                      <ul>
                            <li data-time="0" data-field="Judul" tot-wak="0">Judul: <span>0</span>s</li>
                            <li data-time="0" data-field="Overview" tot-wak="0">Overview: <span>0</span>s</li>
                            <li data-time="0" data-field="Aboutme" tot-wak="0">Aboutme: <span>0</span>s</li>
                            <li data-time="0" data-field="Background" tot-wak="0">Background: <span>0</span>s</li>
                            <li data-time="0" data-field="Value" tot-wak="0">Value: <span>0</span>s</li>
                            <li data-time="0" data-field="Milestone" tot-wak="0">Milestone: <span>0</span>s</li>
                            <li data-time="0" data-field="Benefit" tot-wak="0">Benefit: <span>0</span>s</li>
                            <li data-time="0" data-field="Fee" tot-wak="0">Fee: <span>0</span>s</li>
                            <li data-time="0" data-field="Nextstep" tot-wak="0">Nextstep: <span>0</span>s</li>
                            <li data-time="0" data-field="Term" tot-wak="0">Term: <span>0</span>s</li>
                            <?php if(count($detailtambahan) > 0){ ?>
                              <?php foreach ($detailtambahan as $tambahan) { ?>
                                <li data-time="0" data-field="section_<?php echo strtolower(str_replace(' ','_',$tambahan->section_name)); ?>" tot-wak="0"><?php echo $tambahan->section_name; ?>: <span>0</span>s</li>
                              <?php } ?>
                            <?php } ?>
                      </ul>
                      </div>
                  </div>
                </div>
              <?php endif; ?>
              <!--END FLOATING-->
            </div><!-- /col-lg-12 -->
        </div><!--/ row -->
  </section><!--/wrapper -->
</section><!-- /MAIN CONTENT -->
<script src="<?php echo base_url(); ?>assets/js/screentime.js"></script>
<script type="text/javascript">

var proposal_id = <?php echo $id_projek; ?>;
  $(document).ready(function(){
      <?php if($detailtambahan && count($detailtambahan) > 0){ ?>
        <?php foreach ($detailtambahan as $tambahan) { ?>
          var section_id = '<?php echo $tambahan->section_id; ?>';
          var section_name = '<?php echo preg_replace("/[^A-Za-z0-9 ]/", "",preg_replace("/\r|\n/","",$tambahan->section_name)); ?>';
          var section_name_show = '<?php echo preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","",$tambahan->section_name)); ?>';
          var insert_after_pos = '<?php echo $tambahan->section_pos; ?>';
          var content_section_detail = '';
          <?php if($tambahan->section_type == 'text'){ ?>
            <?php if(count($tambahan->detail)>0){ ?>
              <?php foreach ($tambahan->detail as $dt) { ?>
                content_section_detail += '<h5 class="content" id="info_'+section_name.toLowerCase().replace(/ /g, "_")+'"><?php echo preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$dt->section_detail_content)); ?></h5>';
              <?php } ?>
            <?php } ?>
          <?php }else if($tambahan->section_type == 'image'){ ?>
            <?php if(count($tambahan->detail)>0){ ?>
              <?php foreach ($tambahan->detail as $dt) { ?>
                content_section_detail += '<div class="col-lg-6 col-md-6"><img class="img-responsive" src="<?php echo $dt->section_detail_content; ?>"></div>';
                <?php } ?>
            <?php } ?>
          <?php } ?>

            var new_section_pos = $('<div class="row section new_section" id="section_'+ section_name.toLowerCase().replace(/ /g, "_") +'"><div class="section-padding"><div  class="<?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['section_layout'] == 0){ echo 'col-md-4'; }else if($detail_proposal['style']['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-4'; } ?>"><h3 class="caption" id="title_'+section_name.toLowerCase().replace(/ /g, "_")+'">'+ section_name_show +'</h3></div><div  class="<?php if(count($detail_proposal['style']) > 0) { if($detail_proposal['style']['section_layout'] == 0){ echo 'col-md-8'; }else if($detail_proposal['style']['section_layout'] == 1){ echo 'col-md-12'; }}else{ echo 'col-md-8'; } ?> view-project-info"><div  class="mt content" >'+ content_section_detail +'</div><div class="btn-feedback-section tooltips" data-placement="right" data-original-title="Berikan komentar / feedback" id="comment-btn-section_'+section_name.toLowerCase().replace(/ /g, "_")+'" section="'+section_name.toLowerCase().replace(/ /g, "_")+'"><i class="fa fa-comment fa-2x" aria-hidden="true"></i></div></div></div></div>');
              new_section_pos.insertAfter($('#'+insert_after_pos));
        <?php } ?>
      <?php } ?>

      $('.caption').css('font-family','<?php echo (count($detail_proposal['style']) > 0 && ($detail_proposal['style']['fontstyle'] != NULL || $detail_proposal['style']['fontstyle'] != ''))?$detail_proposal['style']['fontstyle']:'Avenir'; ?>');
      $('.content').css('font-family','<?php echo (count($detail_proposal['style']) > 0 && ($detail_proposal['style']['fontstyle'] != NULL || $detail_proposal['style']['fontstyle'] != ''))?$detail_proposal['style']['fontstyle']:'Avenir'; ?>');

      <?php if($detail_proposal['baca'] == 1){ ?>
        /*sementara komen dulu untuk menampilkan intro*/

              var intro = introJs();
              intro.setOptions({
                exitOnOverlayClick: false,
                nextLabel: 'Selanjutnya',
                prevLabel: 'Sebelumnya',
                doneLabel: 'Selesai',
                disableInteraction: true,
                showStepNumbers:false,
                steps: [
                  {
                    intro: "Ini adalah proposal format digital yang bisa Anda baca dan setujui. Anda juga bisa mengunduh versi PDF"
                  }
                  <?php // if((!empty($plan)) && ($plan->status == 1) && ($plan->plan_id != 1) && ($plan->plan_id != 2)) { ?>
                  /*,
                  {
                    element: '#download_proposal',
                    intro: "Klik di sini untuk mengunduh proposal dalam format PDF"
                  }*/
                  <?php // } ?>
                  <?php if ($id_klien > 1){ ?>
                  ,
                  {
                    element: '#acc_proposal',
                    intro: "Apabila Anda menyetujui proposal ini, silahkan klik di sini."
                  }
                  <?php } ?>
                ]
               });

              intro.onchange(function() {
                if($('.floating-proposal-panel').css('position') === 'fixed')
                {
                  $('.floating-proposal-panel').css('position','absolute');
                }
              });

              intro.onexit(function(){
                $('.floating-proposal-panel').css('position','fixed');
              });

              intro.start();

      <?php } ?>

    <?php if ($sign != 2){ ?>
      var varscreen;
      var ids = $('.new_section').map(function() {
        return $(this).attr('id');
      });

      var format = [];
      for (i = 0; i < ids.length; i++)
      {
        var raw_format = {
          selector: '#'+ids[i],
          name: ids[i]
        };
        format.push(raw_format);
      }


      var dataSection = [
        { selector: '#headerinfo',
          name: 'Judul'
        },
        { selector: '#section_overview',
          name: 'Overview'
        },
        { selector: '#section_aboutme',
          name: 'Aboutme'
        },
        { selector: '#section_team',
          name: 'Background'
        },
        { selector: '#section_team_value',
          name: 'Value'
        },
        { selector: '#section_milestone',
          name: 'Milestone'
        },
        { selector: '#section_benefit',
          name: 'Benefit'
        },
        { selector: '#section_fee',
          name: 'Fee'
        },
        { selector: '#section_nextstep',
          name: 'Nextstep'
        },
        { selector: '#section_term',
          name: 'Term'
        }
      ];


      var fullSection = format.concat(dataSection);
      var time = 0;
      var counter = 0;
      //console.log(varscreen);
      $.screentime({
        fields: fullSection,
        reportInterval: 1,
        percentOnScreen: "65%",
        callback: function(data) {
          $.each(data, function(key, val) {
            var $elem = $('#console li[data-field="' + key + '"]');
            var current = parseInt($elem.data('time'), 10);
            $elem.data('time', current + val);
            $elem.find('span').html(current += val);
            $elem.attr('tot-wak',current);
            //varscreen[key] = current;
          });

          var cumi;
          time++;
          if(time%10 == 0)
          {
            counter++;
            varscreen = [];
            $('li[tot-wak]').each(function(){
              cumi = {};
              cumi[$(this).attr('data-field')] = parseInt($(this).attr('tot-wak'));
              varscreen.push(cumi);
            });

            //console.log(counter);

            $.ajax({
              url : "<?php echo site_url('userview/tracking')?>",
              data : {
                proposal_id : proposal_id,
                proposal_section_track : varscreen,
                tracking_count : counter
              },
              type: "POST",
              success: function(data)
              {
                //console.log(data);
              }
            });
          }
        }
      });
    <?php } ?>

    <?php if((!empty($plan)) && ($plan->status == 1) && ($plan->plan_id != 1) && ($plan->plan_id != 2)) { ?>
      $('#download_proposal').click( function(e) {
              e.preventDefault();
                <?php
                    $random = md5(mt_rand(1,10000));
                    $first = substr($random,0,5);
                    $last = substr($random,5,10);
                    $urlrand = $first.$id_projek.$last;
                 ?>
              window.open("<?php echo site_url('userview/download/'.$urlrand);?>","_blank");
              return false;
      });
    <?php } ?>

    $(document).on('click','.btn-feedback-section',function(){
      $('#feedback').val('');
      $('#feedback_section_name').text('Section : ' + $(this).attr('section'));
      $('#modal_add_feedback').modal('show');
    });

    $(document).on('click','.btn-save-feedback',function(){
      $(this).html('Menyimpan...').attr('disable','disabled');
      if($('#feedback').val() == '')
      {
        $('.btn-save-feedback').html('<i class="fa fa-check" aria-hidden="true"></i> Simpan').attr('disable','');
        swal("Oops","Feedback belum diisi","error");
      }
      else
      {
                      $.ajax({
                        method: "POST",
                        url : "<?php echo base_url();?>proposal/add_feedback",
                        data : {
                          proposal_id:<?php echo $id_projek; ?>,
                          section_name:$('#feedback_section_name').text().split('Section : ')[1],
                          feedback:$('#feedback').val()
                        },
                        success : function(data){
                          $('.btn-save-feedback').html('<i class="fa fa-check" aria-hidden="true"></i> Simpan').attr('disable','');
                          if(data == 1)
                          {
                              swal('Feedback berhasil disimpan dan diteruskan ke pengirim proposal');
                              $('#modal_add_feedback').modal('hide');
                              //location.reload();
                          }
                          else
                          {
                            swal("Oops","Feedback gagal disimpan. Silahkan coba kembali.","error");
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          $('.btn-save-feedback').html('<i class="fa fa-check" aria-hidden="true"></i> Simpan').attr('disable','');
                          swal("Oops","Feedback gagal disimpan. Silahkan coba kembali.","error");
                        }
                      });
      }
    });
});

$('#acc_proposal').on('click',function(){
  var proposal_id = $(this).attr('prop-id');
  swal({
    title: "Setujui Proposal?",
    text: "Setelah disetujui, pengirim akan diberitahukan mengenai hal ini",
    icon: "info",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $.ajax({
        url : "<?php echo site_url('userview/signing')?>",
        data : {
          proposal_id : proposal_id
        },
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          swal("Proposal berhasil disetujui", {
            icon: "success",
          });
          window.location.reload();
        }
      });
    }
  });
})

function setujui(id){
  swal({
    title: "Setujui Proposal?",
    text: "Setelah disetujui, pengirim akan diberitahukan mengenai hal ini",
    icon: "info",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $.ajax({
        url : "<?php echo site_url('userview/signing')?>/"+id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          swal("Proposal berhasil disetujui", {
            icon: "success",
          });
          window.location.reload();
        }
      });
    }
  });
}

function ulas(){
  swal({
    title: "Are you sure?",
    text: "Once deleted, you will not be able to recover this imaginary file!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      swal("Poof! Your imaginary file has been deleted!", {
        icon: "success",
      });
    } else {
      swal("Your imaginary file is safe!");
    }
  });
}

</script>
