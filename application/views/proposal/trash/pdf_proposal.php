<style>
    @page {
        margin: 0px;
/*        font-family: 'helvetica';*/
    }
    .head {
        width: 100%;
        color: <?php echo $detail_proposal->style['headertextcolor']; ?>;
        background-color: <?php echo $detail_proposal->style['headerbgcolor']; ?>;
        padding: 20px 25px 20px 25px;
    }
    .title {
        margin-bottom: 40px;
    }
    .sub-title {
            font-size: 14px;
        }
    .dear {
        font-weight: bold;
        font-size: 1.2em;
    }
    .content {
        padding: 40px 25px 20px 25px;
    }
    .content tr td {
        padding: 10px;
        line-height: 1.5;
    }
    table ol, table ul {
        margin: 0px 0px 0px -25px;
        list-style-position: inside;
        display: inline-block;
    }
    .section {
        font-weight: bold;
        vertical-align: top;
        width: 20%;
    }
</style>
<table class="head">
    <tr>
        <td style="width:50%; color:#656464;">.</td>
        <td colspan="2">
            <h3 class="title"><?php echo $detail_proposal->info_projek; ?></h3>
            <p class="sub-title">Dipersiapkan untuk</p>
            <p class="dear"><?php echo $detail_proposal->nama_pic; ?></p>
            <p><?php echo date("d-M-Y", strtotime($detail_proposal->tgl_buat)); ?></p><br><br>
        </td>
    </tr>
    <tr>
        <td style="width:50%; color:#656464;">.</td>
        <td>
            <p class="sub-title">Oleh</p>
            <p><?php echo $detail_proposal->user_name; ?></p>
        </td>
        <td>
            <p class="sub-title">Kontak</p>
            <p><?php echo $detail_proposal->user_email; ?></p>
        </td>
    </tr>
</table>
<!--<img src="https://pbs.twimg.com/profile_images/949398397450153984/Tr__oGmX_400x400.jpg">-->
<table class="content">
    <?php if (!empty($detail_proposal->project_desc) || (($detail_proposal->project_desc))!= '') { ?>
    <tr>
        <td class="section">Overview</td>
        <td><?php echo nl2br($detail_proposal->project_desc); ?></td>
    </tr>
    <?php } ?>
    <?php
        $pengalaman = $detail_proposal->pengalaman;
        if($pengalaman && count($pengalaman) > 0) {
    ?>
     <tr>
        <td class="section">Portfolio</td>
        <td>
            <ol>
            <?php
            foreach($pengalaman as $d) {
                echo "<li>".$d['pengalaman']."</li>";
            }
            ?>
            </ol>
        </td>
    </tr>
    <?php } ?>
    <?php if($detail_kemampuan && count($detail_kemampuan) > 0){ ?>
    <tr>
        <td class="section">Skill & Value</td>
        <td>
            <ol>
            <?php
            foreach($detail_kemampuan as $d) {
                echo "<li>".$d->skill."</li>";
            }
            ?>
            </ol>
        </td>
    </tr>
    <?php } ?>
    <?php if ($detail_keuntungan && (!empty($detail_keuntungan->keuntungan) || (($detail_keuntungan->keuntungan))!= '')) { ?>
    <tr>
        <td class="section">Tambahan</td>
        <td><?php echo nl2br($detail_keuntungan->keuntungan); ?></td>
    </tr>
    <?php } ?>
</table>
<table class="content" style="padding-top:0px">
    <?php if($detail_pemberian && count($detail_pemberian) > 0){ ?>
    <tr>
        <td class="section">Biaya</td>
        <td style="width:55%">
            <?php
            $i=0;
            foreach ($detail_pemberian as $d) {
                echo $d->pemberian."<br>";
            $i++;
            }
            echo "<br>TOTAL";
            ?>
        </td>
        <td>
            <?php
            for($x=0; $x<$i; $x++){
                echo "Rp <br>";
            }
            echo "<br>Rp";
            ?>
        </td>
        <td align="right">
            <?php
            $total=0;
            foreach ($detail_pemberian as $d) {
                $total += $d->harga;

                echo number_format($d->harga,0,',','.').",-<br>";
            }

            echo "<br>".number_format($total,0,',','.').",-";
            ?>
        </td>
    </tr>
    <?php } ?>
    <?php if (!empty($detail_proposal->nextstep) || (($detail_proposal->nextstep))!= '') { ?>
    <tr>
        <td class="section">Langkah Selanjutnya</td>
        <td colspan="3"><?php echo nl2br($detail_proposal->nextstep); ?></td>
    </tr>
    <?php } ?>
    <?php if($detail_syarat && count($detail_syarat) > 0){ ?>
    <tr>
        <td class="section">Syarat dan Ketentuan</td>
        <td colspan="3">
            <ol>
            <?php
            foreach ($detail_syarat as $d){
                echo "<li>".$d->syaratketentuan."</li>";
            }
            ?>
            </ol>
        </td>
    </tr>
    <?php } ?>
</table>

<!--Tanda tangan-->
<table class="content" style="margin-top:100px">
    <tr>
        <td style="width:380px; text-align:center"><?php echo $detail_proposal->user_name; ?></td>
        <td style="width:380px; text-align:center"><?php echo $detail_proposal->nama_pic; ?></td>
    </tr>
</table>
