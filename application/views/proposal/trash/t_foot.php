<div class="modal" id="modal_duplicate_proposal" role="dialog" aria-labelledby="" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                     <div class="modal-content">
                         <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title text-center" id="" style="font-weight:bold">Duplikat Proposal</h4>
                         </div>
                         <div class="modal-body">
                          <p>Silahkan isi data untuk proposal baru</p>
                          <form role="form" method="post" id="form_duplicate" action="<?php echo base_url('proposal/duplicate'); ?>">
                            <div class="row">
                              <div class="col-md-12">
                                <select class="js-example-basic-single" name="select_duplicate_client" id="select_duplicate_client" style="width:100%">
                                    <option value="0">Pilih dari daftar client</option>
                                    <?php foreach ($user_client as $row) {?>
                                    <option value="<?php echo $row->id_client;?>"><?php echo $row->nama_pic;?> | <?php echo $row->perusahaan;?></option>
                                    <?php } ?>
                                </select>
                                  &nbsp;
                                  <p> atau isi klien baru</p>
                                  <div class="form-group" style="margin-top: 10px;">
                                    <label class="label-control">Nama Klien</label>
                                    <input type="text" class="form-control" style="width: 60%;" name="client_name" id="client_name">
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="label-control">Nama Perusahaan</label> <span>(Optional)</span>
                                    <input type="text" class="form-control" name="company_name" id="company_name">
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <input type="hidden" id="client_id" name="client_id">
                                    <label class="label-control">Email Klien</label> <span>(Optional)</span>
                                    <input type="text" class="form-control" name="client_email" id="client_email">
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="label-control">Nama Proyek</label>
                                    <input type="text" class="form-control" name="new_project_name" id="new_project_name">
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <input type="hidden" id="prop_id" name="prop_id">
                                    <label class="label-control">Deskripsi Proyek</label>
                                    <textarea class="form-control" rows="5" name="project_desc" id="project_desc"></textarea>
                                  </div>
                              </div>
                            </div>
                          </form>
                         </div>
                         <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn btn-info btn-continue-duplicate"><i class="fa fa-check" aria-hidden="true"></i>
                              Lanjutkan
                            </button>
                         </div>
                      </div>
                  </div>
                </div>

<!--Script Home.php-->
<script src="<?= base_url() ?>assets/js/bootstrap-tagsinput.js"></script>
<script src="<?= base_url() ?>assets/js/select2.min.js"></script>
<script src="<?= base_url() ?>assets/js/Chart.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        <?php if ($content == 'proposal/proposal.php' && $this->session->userdata('role') == 1 && $tutorial == 0 && ($this->session->userdata('user_category') != NULL || $this->session->userdata('user_about') != NULL)) {?>
              var intro = introJs();
              intro.setOptions({
                exitOnOverlayClick: false,
                nextLabel: 'Selanjutnya',
                prevLabel: 'Sebelumnya',
                doneLabel: 'Selesai',
                disableInteraction: true,
                steps: [
                  {
                    intro: "Pada halaman ini Anda bisa melihat daftar proposal yang sudah dibuat juga perkembangannya."
                  },
                  {
                    element: '#duplicatepro',
                    intro: "Bila Anda sering membuat proposal berulang-ulang dengan isi yang sama atau dengan beberapa perubahan, gunakan dulikasi proposal yang akan sangat menghemat waktu Anda"
                  },
                  {
                    element: '#createpro',
                    intro: "Untuk membuat proposal dengan pengisian baru silahkan klik tombol Buat baru"
                  },
                  {
                    element: '#editprop',
                    intro: "Atau gunakan contoh template yang sudah ada di sini untuk pembuatan yang lebih cepat"
                  },
                  {
                    element: '#propstat',
                    intro: "Anda bisa melacak status proposal di sini. Pastikan Anda mengetahui sejauh mana klien memahami proposal yang dibaca melalui prediksi berdasarkan lamanya waktu membaca proposal per bagian"
                  },
                  {
                    element: '#analpro',
                    intro: "Ketahui ketertarikan / perilaku klien saat membaca proposal Anda dengan mengetahui sejauh mana klien memahami proposal yang dibaca melalui prediksi berdasarkan lamanya waktu membaca proposal per bagian"
                  }
                ]
               });
                // $('.introjs-prevbutton').text('Sebelumnya');
                intro.start();

                intro.oncomplete(function() {
                      $.ajax({
                        type: "POST",
                        data:{
                          tutorial: 1,
                          id: '<?=$this->session->userdata('user_id')?>'
                        },
                        url: '<?php echo base_url()."user/settutorial"; ?>',
                      }).done(function(res){
                        window.location='<?=base_url()."proposal"?>';
                      });
                });

                intro.onexit(function() {
                      $.ajax({
                        type: "POST",
                        data:{
                          tutorial: 1,
                          id: '<?=$this->session->userdata('user_id')?>'
                        },
                        url: '<?php echo base_url()."user/settutorial"; ?>',
                      }).done(function(res){
                        window.location='<?=base_url()."proposal"?>';
                      });
                });
          <?php }else if($content == 'proposal/new_proposal.php' && $initial_proposal == 0){ ?>
              var intro = introJs();
              <?php if($ada > 0){ ?>
                intro.setOptions({
                exitOnOverlayClick: false,
                nextLabel: 'Selanjutnya',
                prevLabel: 'Sebelumnya',
                doneLabel: 'Selesai',
                disableInteraction: true,
                steps: [
                  {
                    intro: "Mari mulai membuat proposal pertama Anda "
                  },
                  {
                    element: '#pilihclient',
                    intro: "Silahkan pilih daftar klien bila sebelumnya Anda sudah pernah menginput"
                  },
                  {
                    element: '#tambahclient',
                    intro: "Atau isi baru data klien Anda di sini."
                  }
                ]
               });
              <?php } else { ?>
                intro.setOptions({
                exitOnOverlayClick: false,
                nextLabel: 'Selanjutnya',
                prevLabel: 'Sebelumnya',
                doneLabel: 'Selesai',
                disableInteraction: true,
                steps: [
                  {
                    intro: "Mari mulai membuat proposal pertama Anda"
                  },
                  {
                    element: '#tambahclient',
                    intro: "Tambahkan data klien Anda di sini"
                  }
                ]
               });
              <?php }  ?>
                intro.start();
                intro.oncomplete(function() {

                  $.ajax({
                        type: "POST",
                        data:{
                          initial_proposal: 1,
                          email: '<?=$this->session->userdata('user_email')?>'
                        },
                        url: '<?php echo base_url()."user/set_initial_proposal"; ?>',
                      }).done(function(res){
                       //
                      });
                });

                intro.onexit(function() {

                  $.ajax({
                        type: "POST",
                        data:{
                          initial_proposal: 1,
                          email: '<?=$this->session->userdata('user_email')?>'
                        },
                        url: '<?php echo base_url()."user/set_initial_proposal"; ?>',
                      }).done(function(res){
                       //
                      });
                });

          <?php }  ?>
    });

      function ValidateEmail(email) {
              var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              return expr.test(email);
      };

    $(document).ready(function(){
      <?php if($content == 'home.php'){ ?>
          var chartlabel = [];
          var datachart = [];
          var bgchart = [];

          var barlabel = [];
          var databar = [];
      <?php
          if(count($top_proposal) > 0)
          {
            foreach ($top_proposal as $top)
            {
              $hash = md5('color' . $top->proposal_id); // modify 'color' to get a different palette
              list($r,$g,$b) = array(
                  hexdec(substr($hash, 0, 2)), // r
                  hexdec(substr($hash, 2, 2)), // g
                  hexdec(substr($hash, 4, 2))); //b
      ?>
        chartlabel.push('<?php echo strlen($top->detail) > 25 ? substr(addslashes($top->detail),0,23).'...': addslashes($top->detail); ?>');
        datachart.push('<?php echo $top->total; ?>');
        bgchart.push('<?php echo 'rgb('.$r.','.$g.','.$b.')'; ?>');
      <?php
          }
        }else{
      ?>
        chartlabel.push('Belum ada data tracking');
        datachart.push('1');
        bgchart.push('rgb(70, 191, 178)');
      <?php } ?>

      var ctx = document.getElementById('pie-top-proposal').getContext('2d');
      var chartprop = new Chart(ctx,
          {
            "type":"pie",
            "data":{
              "labels":chartlabel,
              "datasets":[{
                  "label":"My First Dataset",
                  "data":datachart,
                  "backgroundColor":bgchart
              }]
            }
          }
        );

      <?php
        if(count($time_range) > 0)
        {
          foreach ($time_range as $tr)
          {
      ?>
            barlabel.push('<?php echo $tr->hour.'.00'; ?>');
            databar.push('<?php echo $tr->count; ?>');
      <?php
          }
        }
        else
        {
      ?>


      <?php } ?>
        console.log(databar);
        var ctx2 = document.getElementById('bar-top-time');
        var stackedBar = new Chart(ctx2, {
            type: 'bar',
            data:{
              labels:barlabel,
              datasets:[{
                  label:"Waktu Proposal Dibaca",
                  data:databar
              }]
            }
        });

      <?php } ?>

      var counter_duplicate = 1;
      var intro3 = introJs();
      $('.dupliprop').on('click',function(){
        intro3.exit();
        var propid = $(this).attr('pid');
        $.ajax({
            type:"POST",
            url: "<?php echo base_url()?>proposal/ajax_get_proposal_by_id",
            data: {proposal_id:propid},
            datatype: 'json',
            success: function(data){
              var datanya = JSON.parse(data);
              <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
                    $('#prop_id').val(propid);
                    $('#client_name').val('');
                    $('#client_id').val('');
                    $('#client_email').val('');
                    $('#company_name').val('');
                    $('#new_project_name').val(datanya.info_projek + ' Copy ' + counter_duplicate);
                    $('#project_desc').val(datanya.project_desc);
                    $('#modal_duplicate_proposal').modal('show');
                    $('#modal_duplicate_proposal').modal({backdrop: 'static',keyboard: false});
              <?php }else{ ?>
                <?php if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1){ ?>
                    $('#prop_id').val(propid);
                    $('#client_name').val('');
                    $('#client_id').val('');
                    $('#client_email').val('');
                    $('#company_name').val('');
                    $('#new_project_name').val(datanya.info_projek + ' Copy ' + counter_duplicate);
                    $('#project_desc').val(datanya.project_desc);
                    $('#modal_duplicate_proposal').modal('show');
                    $('#modal_duplicate_proposal').modal({backdrop: 'static',keyboard: false});
                <?php }else{ ?>
                  <?php if($this->monikalib->getTotalProposal() >= 2 + $this->monikalib->getTotalReferral()){ ?>
                    $('#modal_referral').modal('show');
                  <?php }else{ ?>
                    $('#prop_id').val(propid);
                    $('#client_name').val('');
                    $('#client_id').val('');
                    $('#client_email').val('');
                    $('#company_name').val('');
                    $('#new_project_name').val(datanya.info_projek + ' Copy ' + counter_duplicate);
                    $('#project_desc').val(datanya.project_desc);
                    $('#modal_duplicate_proposal').modal('show');
                    $('#modal_duplicate_proposal').modal({backdrop: 'static',keyboard: false});
                  <?php } ?>
                <?php } ?>
              <?php } ?>
            }, error:function(error){
              swal('error;' +eval(error));
            }
        });
      });

      $('#start-intro-duplicate').on('click',function(){
              intro3.setOptions({
                nextLabel: 'Selanjutnya',
                prevLabel: 'Sebelumnya',
                doneLabel: 'Selesai',
                steps: [
                  {
                    element: '#duplicatepro',
                    intro: "Klik di sini untuk menghemat waktu Anda, cukup <strong>duplikat</strong> ubah beberapa isi proposal."
                  }
                ]
               });
                // $('.introjs-prevbutton').text('Sebelumnya');
                intro3.start();
      });

      <?php if(isset($act) && $act == 'duplicate'){ ?>
        $('#start-intro-duplicate').trigger('click');
      <?php } ?>

      $('#reachlimit').on('click',function(){
        $('#modal_referral').modal('show');
      });

      $('#create_proposal').on('click',function(){
        $('#new_proposal').modal('show');
      });

      $('.statpro').on('click',function(){
                swal("Oops","Ubah plan menjadi \"Startup\" atau \"Agency\" untuk melihat statistik per proposal.","info", {
                  buttons: {
                    cancel:true,
                    upgrade:{
                      text: "Ubah Plan",
                      value: "upgrade",
                    }
                  },
                })
                .then((value) => {
                  switch (value) {

                    case "upgrade":
                      window.location = "<?php echo site_url('plan'); ?>";
                      break;

                    default:
                      break;
                  }
                });
      });

      $('#select_duplicate_client').on('change',function(){
        var client_id = $(this).val();
        //pilihClientLama(client_id);
        $.ajax({
            type:"GET",
            url: "<?php echo base_url()?>client/pilihclient",
            data: {id:client_id},
            datatype: 'json',
            success: function(data){
              var datanya = JSON.parse(data);
              document.getElementById("client_name").value = datanya.nama;
              document.getElementById("company_name").value = datanya.perusahaan;
              document.getElementById("client_email").value = datanya.email;
              document.getElementById("client_id").value = datanya.id;
              //$('#klienlama').modal('hide');
            }, error:function(error){
              swal('error;' +eval(error));
            }
          });
      });

      $('.btn-refer-to-friend').on('click',function(){
        if($('#friend_email').val() == '')
        {
          swal("Oops","Email belum diisi","error");
        }
        else
        {
                  var em = $('#friend_email').tagsinput('items');

                  if(em.length > 0)
                  {
                   for(var j=0;j<em.length;j++)
                   {
                      if(!ValidateEmail(em[j]))
                      {
                        swal("Oops","Pastikan format email benar","error");
                        return false;
                      }
                   }
                  }

                  swal({
                    title: "Undangan akan dikirim",
                    text: "Apakah Anda yakin?",
                    icon: "info",
                    buttons: true,
                    dangerMode: true,
                  })
                  .then((kirim) => {
                    if (kirim) {
                      $('.btn-refer-to-friend').attr('disabled',true).html('Mengirim...');
                      $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>home/refer_to_friend",
                        data: {
                            'user_id' : <?php echo $this->session->userdata('user_id');?>,
                            'refer_email' : $('#friend_email').val()
                            },
                        success: function(data){
                          $('.btn-refer-to-friend').attr('disabled',false).html('<i class="fa fa-send" aria-hidden="true"></i> Kirim');
                          if(data == 1)
                          {
                            swal('Ajakan berhasil dikirim');
                            $('#friend_email').val('');
                            $('#friend_email').tagsinput('removeAll');
                            $('#modal_referral').modal('hide');
                          }
                          else if(data == 0)
                          {
                            swal("Info","Ajakan sudah pernah dikirim via email. Coba ajak teman lainnya.","info");
                            $('#friend_email').val('');
                            $('#friend_email').tagsinput('removeAll');
                          }
                          else
                          {
                            swal('Ada kesalahan');
                          }

                        }, error:function(error){
                          swal('Ada kesalahan');
                        }
                      });
                    }
                  });
        }
      });
      $('.archprop').on('click',function(){
        var proposal_id = $(this).attr('pid');
        swal({
          title: "Apakah Anda yakin?",
          text: "Proposal masih bisa diakses pada menu Arsip.",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
                type: "POST",
                url: '<?=base_url()?>arsip/archive',
                data: {
                    'pid' : proposal_id
                  },
                success: function(data)
                {
                  $('#div_list_prop_'+proposal_id).remove();
                  swal("Archived","Proposal berhasil diarsipkan","success");
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                  swal("Oops","Proposal gagal diarsipkan. Silahkan mencoba kembali.","error");
                }
            });
          }
        });
      });


      $('.btn-continue-duplicate').on('click',function(){
        $(this).attr('disable',true);
        if($('#client_name').val()=='')
        {
          swal('Silahkan isi nama klien atau pilih dari daftar klien');
          return false;
        }
        else if($('#new_project_name').val() == '')
        {
          swal('Nama proyek harus diisi');
          return false;
        }
        else if($('#project_desc').val() == '')
        {
          swal('Deskripsi proyek seperti tujuan juga wajib diisi');
          return false;
        }
        else
        {
          counter_duplicate ++;
          $('#form_duplicate').submit();
        }
      });

      $('.start-tutorial').on('click',function(){
        var intro2 = introJs();
              intro2.setOptions({
                exitOnOverlayClick: false,
                nextLabel: 'Selanjutnya',
                prevLabel: 'Sebelumnya',
                doneLabel: 'Selesai',
                disableInteraction: true,
                steps: [
                  {
                    element: '#duplicatepro',
                    intro: "Bila Anda sering membuat proposal berulang-ulang dengan isi yang sama atau dengan beberapa perubahan, gunakan dulikasi proposal yang akan sangat menghemat waktu Anda"
                  },
                  {
                    element: '#editprop',
                    intro: "Atau gunakan contoh template yang sudah ada di sini untuk pembuatan yang lebih cepat"
                  },
                  {
                    element: '#createpro',
                    intro: "Untuk membuat proposal dengan pengisian baru silahkan klik tombol Buat baru"
                  },
                  {
                    element: '#propstat',
                    intro: "Anda bisa melacak status proposal di sini."
                  },
                  {
                    element: '#analpro',
                    intro: "Ketahui ketertarikan / perilaku klien saat membaca proposal Anda dengan mengetahui sejauh mana klien memahami proposal yang dibaca melalui prediksi berdasarkan lamanya waktu membaca proposal per bagian"
                  }
                ]
               });
                // $('.introjs-prevbutton').text('Sebelumnya');
                intro2.start();
      });

        $(document).ready(function() {
            $('.js-example-basic-single').select2({
              dropdownParent: $("#modal_duplicate_proposal")
            });
        });

        $('.invoice-prop').on('click',function(){
          var proposal_id = $(this).attr('pid');
          var klien_id = $(this).attr('pname');
          var projek = $(this).attr('pproj');
          $.ajax({
                type: "POST",
                url: '<?=base_url()?>invoice/generate',
                data: {
                    'pid' : proposal_id,
                    'cid' :  klien_id,
                    'projek' : projek
                },
                success: function()
                {
                    swal("Invoice created","Invoice berhasil dibuat. Cek di menu invoice.","success");
                    location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    swal("Oops","Invoice gagal terbuat. Silahkan mencoba kembali.","error");
                }
            });
        });

        $('#cariProposal').keyup(function(){
          var query = $(this).val();
            $.ajax({
              url:"<?php echo base_url();?>proposal",
              method:"post",
              data:{query:query},
              success:function(data){
                $('.list_proposal').remove();
                $('#hasilCariProposal').html(data);
              }
            });
        });

        $('.toggle-editor-button').on('click',function(){
          if($('#rightbar').css('right') == '0px')
          {
            $('#main-content').css({'margin-left': '100px','margin-right': '0px'});
            $('#rightbar').css('right','-270px');
            $('.toggle-editor-button').html('<i class="fa fa-angle-double-left fa-2x" aria-hidden="true"></i>');
          }
          else
          {
            $('#main-content').css({'margin-left': '0px','margin-right': '180px'});
            $('#rightbar').css('right','0px');
            $('.toggle-editor-button').html('<i class="fa fa-angle-double-right fa-2x" aria-hidden="true"></i>');
          }
        });
    });
  </script>
<!-- End of Script Home.php-->
