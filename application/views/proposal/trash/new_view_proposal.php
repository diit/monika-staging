<style>
    .floating-proposal-panel div{
        display: flex;
        margin-left: auto;
        -webkit-box-pack: end;
        justify-content: flex-end;
    }
    .floating-proposal-panel div div{
        color: rgb(149, 155, 163);
        box-sizing: border-box;
        font-size: 14px;
        font-weight:bold;
        display: flex;
        line-height: 100%;
        text-transform: uppercase;
        height: 100%;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-pack: center;
        justify-content: center;
        padding: 0px 15px;
        transition: all 0.3s;
    }
    .floating-proposal-panel div div h2{
        margin-bottom:20px;
    }
    .floating-proposal-panel div #link_proposal, .floating-proposal-panel div #copy_link_btn, .floating-proposal-panel div #download_pdf{
        color: rgb(149, 155, 163);
        box-sizing: border-box;
        font-size: 14px;
        font-weight:bold;
        display: flex;
        line-height: 100%;
        text-transform: uppercase;
        height: 100%;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-pack: center;
        justify-content: center;
        width:20px;
        padding: 0px 15px;
        transition: all 0.3s;
        border-left: 1px solid rgb(216, 218, 220);
    }
    .floating-proposal-panel div #link_proposal input#link{
        width: 100%;
        height:30px;
        border: none;
    }
    .floating-proposal-panel div #send_proposal{
        background-color:#5ee2d4;
        color: #fff;
        cursor: pointer;
        box-sizing: border-box;
        font-size: 14px;
        font-weight:bold;
        display: flex;
        line-height: 100%;
        text-transform: uppercase;
        height: 100%;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-pack: center;
        justify-content: center;
        padding: 0px 20px;
        transition: all 0.3s;
        border-left: 1px solid rgb(216, 218, 220);
    }
    .floating-proposal-panel div #copy_link_btn, .floating-proposal-panel div #download_pdf{
/*        color: rgb(149, 155, 163);*/
        cursor: pointer;
    }
    #main-content .row .showback .container-fluid{
        background: url(https://res.cloudinary.com/monika-id/image/upload/v1521688750/phpF62A_v4lhvm.jpg) center center;
        background-size: cover;
        color: #ffffff;
        padding-bottom: 8%;
        margin-bottom: 20px;
    }
    #main-content .row .showback #header-proposal {
        padding-top: 75px;
    }
    #main-content .row .showback #header-proposal h5{
        font-size: 13pt;
    }
    #main-content .row .showback #header-proposal #header-img{
        margin-top: 40px;
/*        padding-left: 10px;*/
    }
    #main-content .row .showback #header-proposal #header-img img{
        margin-left: 20px;
    }
    #main-content .row .showback #header-proposal #header-info .header-nonbold{
        padding-top: 5px;
        padding-bottom: 5px;
    }
    #main-content .row .showback{
        padding-bottom: 20px;
    }
    #main-content .row .showback #items .row{
        padding: 20px 50px 20px 50px;
        position: relative;
        margin: 0px;
        transition: all 0.3s ease;
/*        margin-top: 10px;*/
    }
    #main-content .row .showback #items .row .sec-handle{
        position: absolute;
        top: 18px;
        left: 30px;
        font-size: 15pt;
        cursor: -webkit-grabbing;
        visibility: hidden;
/*        transition: all 0.3s ease;*/
    }
    #main-content .row .showback #items .row:hover{
        background: #ececec;
    }
    #main-content .row .showback #items .row:hover .sec-handle{
        visibility: visible;
    }
    #main-content .row .showback #items .row .col-md-5 .title-sec{
        font-weight: bold;
        background-color: transparent;
        border: 0;
        padding: 3px 0px;
        cursor: text;
        box-sizing: border-box;
        transition: width 0.25s;
        font-size: 18px;
        width: 100%;
    }
    #main-content .row .showback #items .row .col-md-7 .content-sec{
        font-size: 18px;
        cursor: text;
        background-color: transparent;
        border: 0;
        resize: none;
        width: 100%;
        overflow: hidden;
    }
    #main-content .row .showback #items .row .col-md-7 .content-sec:focus{
        outline: none;
    }
    #main-content .row .showback #items .row .col-md-5 .title-sec:focus{
        outline: none;
    }
     .title-sec ~ .focus-border, .content-sec ~ .focus-border-content{
        position: absolute;
        bottom: 0;
        left: 50%;
        width: 0;
        height: 2px;
        background-color: var(--main-color);
        transition: 0.4s;
    }
    .title-sec:focus ~ .focus-border, .content-sec:focus ~ .focus-border-content{
        width: 90%;
        transition: 0.4s;
        left: 15px;
    }
    #main-content .row .showback #items .row .btn-delete-sec{
        position: absolute;
        top: 55px;
        right: 15px;
        visibility: hidden;
        color: #000;
    }
   .btn-add-sec{
        position: fixed;
        bottom: 100px;
        right: 30px;
/*        visibility: hidden;*/
        color: #000;
       z-index: 1;
    }
    #main-content .row .showback #items .row .btn-delete-sec:hover{
        background: #d9534f;
        color: #fff;
        transition: all 0.3s ease;
    }
    .btn-add-sec:hover{
        background: #428bca;
        color: #fff;
        transition: all 0.2s ease;
    }
    #main-content .row .showback #items .row:hover .btn-delete-sec{
        visibility: visible;
    }
</style>

<!--Panel header-->
<div class="floating-proposal-panel">
    <div>
        <div>
            <h2 class="hidden-xs">PREVIEW</h2>
            <h4 class="hidden-lg hidden-md hidden-sm">PREVIEW</h4>
        </div>
        <div id="link_proposal" class="hidden-xs">
            <input id="link" type="text" value="Link ajaib">
        </div>
        <div id="copy_link_btn">
            <span class="hidden-xs hidden-sm"><i class="fa fa-link"></i>Copy URL</span>
            <i class="fa fa-link hidden-md hidden-lg"></i>
        </div>
        <div id="download_pdf">
          <span class="hidden-xs"><i class="fa fa-cloud-download"></i>  DOWNLOAD PDF</span>
          <i class="fa fa-cloud-download hidden-lg hidden-md hidden-sm"></i>
        </div>
        <div id="send_proposal">
          <span><i class="fa fa-envelope"></i> KIRIM</span>
        </div>
    </div>
</div>
 <button class="btn btn-add-sec" btnaddsec="1" data-toggle="tooltip" data-placement="top" title="Klik disini menambah section"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Section</button>
<!--Panel header-->
<!--Panel Style Section-->

<!--Panel Style Section-->

<section id="main-content">
      <section style="margin-top: 180px;" class="wrapper">
          		<div class="row mt" style="margin:0px">
                    <div class="container">
              			<div class="col-xs-12">
                                  <div class="showback" style="min-height:600px">
<!--Header Proposal-->
                                      <!-- Header Gambar-->
                                      <div class="container-fluid">
                                            <div class="row" id="header-proposal">
                                                <div class="col-md-offset-1 col-md-3 text-center" id="header-img">
                                                    <img src="https://res.cloudinary.com/monika-id/image/upload/v1521607149/phpB8AC_jufuwj.png" class="img img-responsive">
                                                </div>
                                                <div class="col-md-offset-1 col-md-6" id="header-info">
                                                    <h5><b>Projek buat revisi view proposal</b></h5>
                                                    <h5 class="header-nonbold">Dipersiapkan Untuk</h5>
                                                    <h5><b>Perjojonan</b></h5>
                                                    <h5 class="header-nonbold">Jojo</h5>
                                                    <h5><b>20 Maret 2018</b></h5>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                                <h5 class="header-nonbold">Oleh</h5>
                                                                <h5><b>Wage</b></h5>
                                                                <h5><b>Apa aja</b></h5>
                                                        </div>
                                                        <div class="col-md-6" style="padding-left:50px;">
                                                                <h5 class="header-nonbold">Kontak</h5>
                                                                <h5><b>freeze1308@gmail.com</b></h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
<!--Header Proposal-->
<!--Section Proposal-->
                                        <div class="sec-items" id="items">
                                            <div class="row sec-prop" data-id="1" id-sec="1">
                                                    <span class="sec-handle"  data-toggle="tooltip" data-placement="top" title="Tekan dan tarik untuk mengganti posisi section"><i class="fa fa-bars" aria-hidden="true"></i></span>
                                                    <div class="col-md-5">
                                                        <input class="title-sec" type="text" name="sec_namasec" value="Overview" data-toggle="tooltip" data-placement="top" title="Klik disini untuk edit"><span class="focus-border"></span>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <textarea class="content-sec" data-autoresize rows="2" data-toggle="tooltip" data-placement="top" title="Klik disini untuk edit">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum hadesktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.                                   </textarea><span class="focus-border-content"></span>
                                                    </div>
                                                    <button class="btn btn-delete-sec" btndelsec="1" data-toggle="tooltip" data-placement="right" title="Klik disini menghapus section"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                        <p id="hasil"></p>
<!--Section Proposal-->
                                  </div>
                         </div>
                    </div>
            </div>
    </section>
</section>
<script src="<?= base_url();?>assets/js/Sortable.min.js"></script>
<script>
    //    Sortable section
	var sort = document.getElementById("items");
	var hasilsort = Sortable.create(sort, {
		group: "section",
        handle: ".sec-handle",
		animation: 150,
		dataIdAttr: 'data-id',
		store: {
			get: function (sortable) {
				var order = localStorage.getItem(sortable.options.group);
				return order ? order.split('|') : [];
			},
			set: function (sortable) {
				var order = sortable.toArray();
				var hasilorder = document.getElementById("hasil").innerHTML=order;
				return hasilorder;
			}
		}
	});

        //Autowidth Section
     jQuery.each(jQuery('textarea[data-autoresize]'), function() {
        var offset = this.offsetHeight - this.clientHeight;

        var resizeTextarea = function(el) {
            jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
        };
        jQuery(this).on('keyup input', function() { resizeTextarea(this); }).removeAttr('data-autoresize');
    });

    //Tambah Section
    var data_id = 10;
    $('.btn-add-sec').on('click',function(){
		var newhandlersec = $('<span class="sec-handle"><i class="fa fa-bars" aria-hidden="true"></i></span>');
		var newsection = $('<div class="row sec-prop" data-id="'+data_id+'" id="id-sec-'+data_id+'"></div>');
		var newinputsec = $('<div class="col-md-5"><input class="title-sec" type="text" name="sec_namasec" value="Section '+data_id+'"><span class="focus-border"></span></div><div class="col-md-7"><textarea class="content-sec" data-autoresize rows="2" id="section-tambahan-'+data_id+'" >Setion Tambahan '+data_id+'</textarea><span class="focus-border-content"></span></div>');
		var newbtndelsec= $('<button class="btn btn-delete-sec" btndelsec="'+data_id+'" nama-sec="sec_namasec"><i class="fa fa-trash" aria-hidden="true"></i></button>');
//        var newbtnaddsec= $('<button class="btn btn-add-sec" btnaddsec="'+data_id+'"><i class="fa fa-plus" aria-hidden="true"></i></button>');
		data_id++;
		$('.sec-items').append(newsection.append(newinputsec,newhandlersec,newbtndelsec));
		$('#section-tambahan-'+data_id+'').focus();
		Object.values(hasilsort.options.store.set(hasilsort));
    });

    //Delete section
    $(document).on('click','.btn-delete-sec',function(){
		var data_id_sec = $(this).attr('btndelsec');
		var nama_sec = $(this).attr('nama-sec');
        swal({
          title: ""+nama_sec+" diproposal akan dihapus",
          text: "Apakah Anda Yakin?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((hapusSection) => {
          if (hapusSection) {
            $('#id-sec-'+data_id_sec).remove();
            swal("Section "+nama_sec+" berhasil dihapus!", {
              icon: "success",
            });
            Object.values(hasilsort.options.store.set(hasilsort));
          }
        });

    });

    //Tooltip content section
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip('toggle');
    });



</script>
