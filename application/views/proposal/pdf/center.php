<?php
ob_start();
// =========== setup style ====================
function hex2rgb( $colour ) {
    if ( $colour[0] == '#' ) {
            $colour = substr( $colour, 1 );
    }
    if ( strlen( $colour ) == 6 ) {
            list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
    } elseif ( strlen( $colour ) == 3 ) {
            list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
    } else {
            return false;
    }
    $r = hexdec( $r );
    $g = hexdec( $g );
    $b = hexdec( $b );
    return array($r,$g,$b);
}

// proposal style
if(!empty($style)){
    $headbgrgb = hex2rgb($style->headerbgcolor);
    $txtheadrgb = hex2rgb($style->headertextcolor);
    $fontdb = $style->fontstyle;
    if($fontdb == 'avenir'){
        $font = 'avenirltstd';
    }elseif($fontdb == 'opensans'){
        $font = 'opensans';
    }elseif($fontdb == 'arial'){
        $font = 'arial';
    }elseif($fontdb == 'verdana'){
        $font = 'verdana';
    }else{
        $font = 'avenirltstd';
    }
}else{
	$headbgrgb = hex2rgb('#656464');
	$txtheadrgb = hex2rgb('#ffffff');
	$font = 'avenirltstd';
}
// end of proposal style
// =========== end of setup style ====================

$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('Proposal - '.$detailProp->judul);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('Monika');
$pdf->SetDisplayMode('real', 'default');
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setPrintHeader(false);
$pdf->AddPage();

$fontheaderSM = 10;
$fontheaderMD = 11;
$fontheaderMD2 = 12;
$fontheaderBG = 14;
$lineStyle = $style = array('width' => 1.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array($txtheadrgb['0'], $txtheadrgb['1'], $txtheadrgb['2']));
$lineStyle2 = $style = array('width' => 1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array($headbgrgb['0'], $headbgrgb['1'], $headbgrgb['2']));


// $pdf->SetFont("verdana", '', $fontheaderBG);
// $pdf->SetFont("opensans", '', $fontheaderBG);
// ================= Header ============================
$pdf->SetTextColor($txtheadrgb['0'], $txtheadrgb['1'], $txtheadrgb['2']);
$pdf->SetFillColor($headbgrgb['0'], $headbgrgb['1'], $headbgrgb['2']);

$pdf->MultiCell(80, 30, '', 0, 'L', 1, 1, 65, 0, true);

$pdf->Ln(10);
$pdf->SetFont($font, '', 26);
$pdf->MultiCell(120, 30, $detailProp->judul, 0, 'C', 0, 1, 45, 40, true);
$yLine = $pdf->getY()-12;
$xLine = 85;
$pdf->Line($xLine, $yLine, $xLine+40, $yLine, $lineStyle); //x1,y1,x2,y2

$pdf->SetFont($font, '', $fontheaderBG);
$pdf->Cell(190,10,$detailProp->nama_pic,0,1,'C');
$pdf->SetFont($font, '', $fontheaderMD);
$pdf->Cell(190,0,$detailProp->perusahaan,0,1,'C');
$pdf->Ln(10);
$pdf->Cell(190,15,'Oleh',0,1,'C');
$pdf->Cell(190,0,$detailProp->user_name,0,1,'C');
$pdf->Cell(190,10,$team->team_name,0,1,'C');
$pdf->Cell(190,0,$detailProp->user_email,0,0,'C');

$pdf->MultiCell(220, 40, '', 0, 'L', 1, 1, 0, 95, true);

// Logo team
$content = $detailProp->logo_url;
if(!empty($content)){
	// $html1 = '<img src="'.$content.'" alt="" width="150" border="0">';
	// $pdf->writeHTMLCell(150,'', 30, 35, $html1, 0, 1,0,true);
	$pdf->Image($content, 95, 10, 20,'','');
}
// end of Logo team

// ================= end of Header ============================
// ================= content ============================
$pdf->SetTextColor(73,80,87);
$xSection = 16;
$pdf->Ln(10);
$pdf->setCellHeightRatio(1.5);
foreach($detailSec as $sec){
	$yAfter = $pdf->getY()+50;
	if($yAfter >= 290){
		$pdf->AddPage();
	}else{
		$yContent = $pdf->getY();
		if($sec->sec_tipe == 0){
			$pdf->SetFont($font, '', $fontheaderMD2);
            $pdf->MultiCell(200,10,$sec->sec_judul, 0, 'L', 0, 1, $xSection, $yContent, true);
            $lebar = $pdf->GetStringWidth($sec->sec_judul);
            $panjangLine = ($lebar + 20) + $xSection+1;
			$pdf->Line($xSection+1, $yContent+10, $panjangLine, $yContent+10, $lineStyle2);
			$pdf->SetFont($font, '', $fontheaderSM);
			$pdf->MultiCell(180, 0,$sec->sec_isi, 0, 'L', 0, 1, $xSection, $yContent+15 , true);
			$pdf->Ln(10);
		}elseif($sec->sec_tipe == 1){
			$CI =& get_instance();
			$CI->load->model('proposal_model');
			$detailItem = $CI->proposal_model->detailSecTable($sec->sec_id, $detailProp->id);
			$pdf->SetFont($font, '', $fontheaderMD2);
			$pdf->MultiCell(200,10,$sec->sec_judul, 0, 'L', 0, 1, $xSection, $yContent, true);
            $lebar = $pdf->GetStringWidth($sec->sec_judul);
            $panjangLine = ($lebar + 20) + $xSection+1;
			$pdf->Line($xSection+1, $yContent+10, $panjangLine, $yContent+10, $lineStyle2);
			$pdf->SetFont($font, '', $fontheaderSM);
			$pdf->Ln(3);
			foreach($detailItem as $item){
				$pdf->MultiCell(100, 30, $item->item, 0, 'L', 0, 0, $xSection,'', true);
				$pdf->MultiCell(20, 30, $item->biaya, 0, 'R', 0, 1, '', '', true);
				$yAfterItem = $pdf->getY()+50;
				if($yAfterItem >= 290){
					$pdf->AddPage();
				}
			}
			$pdf->Ln(2);
		}elseif($sec->sec_tipe == 2){
			$instance =& get_instance();
			$instance->load->model('proposal_model');
			$detailImage = $instance->proposal_model->detailSecImage($sec->sec_id, $detailProp->id);
			$pdf->SetFont($font, '', $fontheaderMD2);
			$pdf->MultiCell(200,10,$sec->sec_judul, 0, 'L', 0, 1, $xSection, $yContent, true);
            $lebar = $pdf->GetStringWidth($sec->sec_judul);
            $panjangLine = ($lebar + 20) + $xSection+1;
			$pdf->Line($xSection+1, $yContent+10, $panjangLine, $yContent+10, $lineStyle2);
			$xForImg = $xSection;
			foreach($detailImage as $image){
				$content1 = $image->image_dir;
				$pdf->Image($content1, $xForImg, $yContent+20, 45,'','');
				$xForImg = $xForImg + 57;
			}
			$pdf->MultiCell(200,50,' ', 0, 'L', 0, 1, '', $yContent+20, true);
			$pdf->Ln(20);
		}
	}

}
// ================= end of content ============================
// ================= signature ===================================
$pdf->Ln(20);
$pdf->SetFont($font, '', $fontheaderMD);
$pdf->Cell(130);
$pdf->Cell(30,8,$detailProp->signature,0,1,'C');
$pdf->Cell(110);
$pdf->Cell(70,8,$detailProp->user_name,0,1,'C');
// ================= end of signature ============================
$pdf->Output($detailProp->judul, 'I');
?>
