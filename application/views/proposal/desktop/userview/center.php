<div class="row justify-content-center font-color" id="header">
    <div class="col-6 bg-color">
        <div id="photo" style="margin: 40px auto 30px auto" class="text-center">
            <?php if($detailProp->logo_url !== NULL){?>
                <img src="<?= $detailProp->logo_url?>" alt=""  style="width:60px; height:60px;">
            <?php }?>
        </div>
    </div>
    <div class="col-8 text-center">
        <h1 style="line-height:1.5; margin-top:60px" class="bold"><?= $detailProp->judul ?></h1>
        <hr id="line" style="border-width: 6px; border-color: #111111; width: 150px;"></hr>
        &nbsp;
        <h5 class="bold"><?= $detailProp->nama_pic ?></h5>
        <p><?= $detailProp->perusahaan ?></p>
        <br><br>
    </div>
    <div class="col-11 text-center bg-color">
        &nbsp;
        <p class="text-little">Oleh</p>
        <p style="line-height:1.6">
            <span class="bold"><?= $detailProp->user_name ?></span><br>
            <?= $team->team_name ?><br>
            <span class="text-little"><?= $detailProp->user_email ?></span><br>
        </p>
    </div>
</div>

<div id="fill" class="sec-sortable">
    <?php foreach($detailSec as $sec){
            if($sec->sec_tipe == 0){ ?>
            <div class="row section justify-content-center section--userview" secID="<?= $sec->sec_id?>" id="<?= $sec->sec_id?>" style="margin-bottom:40px">
                <div class="col-11">
                    <p class="userview-judul-section bold" style="width:50%; font-size:1.5em; line-height:1.5"><?= $sec->sec_judul;?></p>
                    <?php $lenght=(strlen($sec->sec_judul)*10)+60; ?>
                    <hr class="border-bg-color" style="border-width: 4px; border-color: gold; width: <?= $lenght ?>px; margin-left: 0; margin-top:5px"></hr>
                    <p class="userview-isi-section"><?= $sec->sec_isi;?></p>
                </div>
            </div>
        <?php }elseif($sec->sec_tipe == 1){
                $CI =& get_instance();
                $CI->load->model('proposal_model');
                $detailItem = $CI->proposal_model->detailSecTable($sec->sec_id, $detailProp->id);
                ?>
            <div class="row section justify-content-center section--userview" secID="<?= $sec->sec_id?>" id="<?= $sec->sec_id?>" style="margin-bottom:40px">
                <div class="col-11">
                    <p class="userview-judul-section bold" style="width:50%; font-size:1.5em; line-height:1.5"><?= $sec->sec_judul;?></p>
                    <?php $lenght=(strlen($sec->sec_judul)*10)+60; ?>
                    <hr class="border-bg-color" style="border-width: 4px; border-color: gold; width: <?= $lenght ?>px; margin-left: 0; margin-top:5px"></hr>
                    <table class="section-table userview-isi-section">
                        <tbody class="section-table-body">
                        <?php foreach($detailItem as $item){?>
                            <tr class="item-in-section">
                                <td>
                                    <p class="userview-item-section"><?= $item->item;?></p>
                                </td>
                                <td class="text-right">
                                    <p class="userview-biaya-section"><?= 'Rp '.number_format($item->biaya,0,',','.');?></p>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php }elseif($sec->sec_tipe == 2){
                $instance =& get_instance();
                $instance->load->model('proposal_model');
                $detailImage = $instance->proposal_model->detailSecImage($sec->sec_id, $detailProp->id);
            ?>
                <div class="row section justify-content-center section--userview" secID="<?= $sec->sec_id?>" id="<?= $sec->sec_id?>" style="margin-bottom:60px">
                    <div class="col-11">
                        <p class="userview-judul-section bold" style="width:50%; font-size:1.5em; line-height:1.5"><?= $sec->sec_judul;?></p>
                        <?php $lenght=(strlen($sec->sec_judul)*10)+60; ?>
                        <hr class="border-bg-color" style="border-width: 4px; border-color: gold; width: <?= $lenght ?>px; margin-left: 0; margin-top:5px"></hr>
                        <div class="row userview-isi-section">
                        <?php foreach($detailImage as $image){?>
                            <div class="col-4 photo-in-section">
                                <img class="show-image" imgID="<?= $image->image_id?>" src="<?= $image->image_dir ?>" alt="">
                            </div>
                        <?php }?>
                        </div>
                    </div>
                </div>
        <?php }?>
    <?php }?>
</div>

<div class="row" style="margin-top:140px">
    <div class="col-4 offset-md-7 text-center">
        <p><?= $this->monikalib->format_date_indonesia($detailProp->tgl_buat) ?></p>
        <p><?=$detailProp->signature?></p>
        <p><?=$detailProp->user_name?></p>
    </div>
</div>
