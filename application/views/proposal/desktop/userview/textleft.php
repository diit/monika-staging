<div class="row justify-content-center bg-color font-color" id="header">
    <div class="col-11">
        <div id="photo">
            <?php if($detailProp->logo_url !== NULL){?>
                <img class="proposal-logo" src="<?= $detailProp->logo_url?>" alt="" style="width:100px;">
            <?php }?>
        </div>

        <h1 style="width:80%; line-height:1.5; text-transform:uppercase; margin-top:40px" class="bold"><?= $detailProp->judul ?></h1>
        <hr id="line" style="border-width: 6px; border-color: #111111; width: 150px; margin-left: 0;"></hr><br><br>

        <p class="text-little">Dipersiapkan untuk</p>
        <h5 class="bold"><?= $detailProp->nama_pic;?></h5>
        <p><?= $detailProp->perusahaan;?></p>
        &nbsp;
        <p class="text-little">Oleh</p>
        <p style="line-height:1.6">
            <span class="bold"><?= $detailProp->user_name ?></span><br>
            <?= $team->team_name ?><br>
            <span class="text-little"><?= $detailProp->user_email ?></span><br>
            <span class="text-small"><?= $this->monikalib->format_date_indonesia($detailProp->tgl_buat) ?></span>
        </p>
    </div>
</div>

<div id="fill" class="sec-sortable">
    <?php foreach($detailSec as $sec){
            if($sec->sec_tipe == 0){ ?>
            <div class="row section justify-content-center section--userview" secID="<?= $sec->sec_id?>" id="<?= $sec->sec_id?>" style="margin-bottom:40px">
                <div class="col-11">
                    <p class="userview-judul-section upper bold"><?= $sec->sec_judul;?></p>
                    <hr id="line" style="border-width: 2px; border-color: #111111; width: 50px; margin-left: 0px"></hr>
                    <p class="userview-isi-section"><?= $sec->sec_isi;?></p>
                </div>
            </div>
        <?php }elseif($sec->sec_tipe == 1){
                $CI =& get_instance();
                $CI->load->model('proposal_model');
                $detailItem = $CI->proposal_model->detailSecTable($sec->sec_id, $detailProp->id);
                ?>
            <div class="row section justify-content-center section--userview" secID="<?= $sec->sec_id?>" id="<?= $sec->sec_id?>" style="margin-bottom:40px">
                <div class="col-11">
                    <p class="userview-judul-section upper bold"><?= $sec->sec_judul;?></p>
                    <hr id="line" style="border-width: 2px; border-color: #111111; width: 50px; margin-left: 0px"></hr>
                    <table class="section-table userview-isi-section">
                        <tbody class="section-table-body">
                        <?php foreach($detailItem as $item){?>
                            <tr class="item-in-section">
                                <td>
                                    <p class="userview-item-section"><?= $item->item;?></p>
                                </td>
                                <td class="text-right">
                                    <p class="userview-biaya-section"><?= 'Rp '.number_format($item->biaya,0,',','.');?></p>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php }elseif($sec->sec_tipe == 2){
                $instance =& get_instance();
                $instance->load->model('proposal_model');
                $detailImage = $instance->proposal_model->detailSecImage($sec->sec_id, $detailProp->id);
            ?>
                <div class="row section justify-content-center section--userview" secID="<?= $sec->sec_id?>" id="<?= $sec->sec_id?>" style="margin-bottom:60px">
                    <div class="col-11">
                        <p class="userview-judul-section upper bold"><?= $sec->sec_judul;?></p>
                        <hr id="line" style="border-width: 2px; border-color: #111111; width: 50px; margin-left: 0px"></hr>
                        <div class="row userview-isi-section">
                        <?php foreach($detailImage as $image){?>
                            <div class="col-4 photo-in-section">
                                <img class="show-image" imgID="<?= $image->image_id?>" src="<?= $image->image_dir ?>" alt="">
                            </div>
                        <?php }?>
                        </div>
                    </div>
                </div>
        <?php }?>
    <?php }?>
</div>

<div class="row" style="margin-top:140px">
    <div class="col-4 offset-md-7 text-center">
        <p><?=$detailProp->signature?></p>
        <p><?=$detailProp->user_name?></p>
    </div>
</div>
