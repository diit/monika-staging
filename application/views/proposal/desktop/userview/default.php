<div class="row bg-color font-color" id="header">
    <div class="col-sm-4 text-center">
        <div id="photo">
            <?php if($detailProp->logo_url !== NULL){?>
                <img class="proposal-logo" src="<?= $detailProp->logo_url?>" alt="" style="margin: 25% auto 0 auto;width:150px;">
            <?php }?>
        </div>
    </div>
    <div class="col-sm-8 form-group">
        <h5><?= $detailProp->judul;?></h5>
        <p class="text-little">Dipersiapkan untuk</p>
        <h5><?= $detailProp->perusahaan;?></h5>
        <p><?= $detailProp->nama_pic;?></p>
        <p class="text-little">10 April 2018</p>

        <div class="row">
            <div class="col">
                <p class="text-little">Oleh</p>
                <p><?= $detailProp->user_name;?></p>
                <p class="bold"><?= $team->team_name?></p>
            </div>
            <div class="col">
                <p class="text-little">Kontak</p>
                <p><?= $detailProp->user_email;?></p>
            </div>
        </div>
    </div>
</div>
<div id="fill" class="sec-sortable">
    <?php foreach($detailSec as $sec){
            if($sec->sec_tipe == 0){ ?>
            <div class="row section section--userview" secID="<?= $sec->sec_id?>" id="<?= $sec->sec_id?>">
                <div class="col-sm-4">
                    <p class="userview-judul-section"><?= $sec->sec_judul;?></p>
                </div>
                <div class="col-sm-8">
                    <p class="userview-isi-section"><?= $sec->sec_isi;?></p>
                </div>
            </div>
        <?php }elseif($sec->sec_tipe == 1){
                $CI =& get_instance();
                $CI->load->model('proposal_model');
                $detailItem = $CI->proposal_model->detailSecTable($sec->sec_id, $detailProp->id);
                ?>
            <div class="row section section--userview" secID="<?= $sec->sec_id?>" id="<?= $sec->sec_id?>">
                <div class="col-sm-4">
                    <p class="userview-judul-section"><?= $sec->sec_judul;?></p>
                </div>
                <div class="col-sm-8">
                    <table class="section-table userview-isi-section">
                        <tbody class="section-table-body">
                        <?php foreach($detailItem as $item){?>
                            <tr class="item-in-section">
                                <td style="width:80%">
                                    <p class="userview-item-section"><?= $item->item;?></p>
                                </td>
                                <td class="text-right">
                                    <p class="userview-biaya-section"><?= 'Rp '.number_format($item->biaya,0,',','.');?></p>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php }elseif($sec->sec_tipe == 2){
                $instance =& get_instance();
                $instance->load->model('proposal_model');
                $detailImage = $instance->proposal_model->detailSecImage($sec->sec_id, $detailProp->id);
            ?>
                <div class="row section section--userview" secID="<?= $sec->sec_id?>" id="<?= $sec->sec_id?>">
                    <div class="col-sm-4">
                    <p class="userview-judul-section"><?= $sec->sec_judul;?></p>
                    </div>
                    <div class="col-sm-8">
                        <div class="row userview-isi-section">
                        <?php foreach($detailImage as $image){?>
                            <div class="col-4 photo-in-section">
                                <img class="show-image" imgID="<?= $image->image_id?>" src="<?= $image->image_dir ?>" alt="">
                            </div>
                        <?php }?>
                        </div>
                    </div>
                </div>
        <?php }?>
    <?php }?>
    </div>
