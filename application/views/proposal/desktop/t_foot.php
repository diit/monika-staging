<script src="<?= base_url() ?>assets/js/typeahead.jquery.js"></script>
<script src="<?= base_url() ?>node_modules/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?= base_url(); ?>assets/js/spectrum.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-tagsinput.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-toggle.min.js"></script>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();

        //SCRIPT INDEX
        <?php if(($this->uri->segment(1) == 'proposal') && (empty($this->uri->segment(2)))) { ?>
        $('.generate-invoice').on('click',function(){
            swal({
                title: "Buat invoice dari proposal?",
                text: "Invoice bisa diakses dan diedit pada menu Invoice.",
                buttons: true,
                confirmMode: true,
            });
        });

        $('.arsip-proposal').on('click',function(){
            var proposal_id = $(this).attr('pid');
            swal({
              title: "Apakah Anda yakin?",
              text: "Proposal masih bisa diakses pada menu Arsip.",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $.ajax({
                    type: "POST",
                    url: '<?=base_url()?>arsip/archive',
                    data: {
                        'pid' : proposal_id
                      },
                    success: function(data)
                    {
                      $('#div_list_prop_'+proposal_id).remove();
                      swal("Archived","Proposal berhasil diarsipkan","success");
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      swal("Oops","Proposal gagal diarsipkan. Silahkan mencoba kembali.","error");
                    }
                });
              }
            });
        });

        $('#cari-proposal').keyup(function(){
          var query = $(this).val();
            $.ajax({
              url:"<?php echo base_url();?>proposal/proposal",
              method:"post",
              data:{query:query},
              success:function(data){
                $('#list-proposal').remove();
                $('#list-cari-proposal').html(data);
              }
            });
        });

        function ValidateEmail(email) {
        var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return expr.test(email);
        };

        function isEmpty(el){
            return !$.trim(el.html())
        }
        
        $('#klien_proposal_baru').on('change',function(){
            var value = $('#klien_proposal_baru').val();
            if(value == 1){
                $('#create-new-client').collapse('show');
            }else{
                $('#create-new-client').collapse('hide');
            }
        });

        $(document).on('click','#new-proposal-baru',function(){
            var klien = $('#klien_proposal_baru').val();
            var user = '<?php echo $this->session->userdata('user_id');?>';
            var projekBaru = $('#new-nama-projek').val();
            var overviewBaru = $('#new-overview-proposal').val();

            if (projekBaru == ''){
                $('#project-msg').empty().append("<div class='alert alert-danger text-little'>Nama Proyek atau proposal wajib diisi</div>");
            }
            if (klien == 0){
                $('#client-msg').empty().append("<div class='alert alert-danger text-little'>Klien wajib dipilih</div>");
            }
            if (overviewBaru ==''){
                $('#overview-msg').empty().append("<div class='alert alert-danger text-little'>Overview proposal wajib diisi</div>");
            }
            if ((projekBaru !='') && (klien !=0) && (overviewBaru !='')){
                document.getElementById('new-proposal-baru').innerHTML = 'Membuat . . .';
                $('#new-proposal-baru').attr('disabled','true');

                //new proposal with new client
                if (klien == 1){
                    var pic =  $('#pic').val();
                    var perusahaan =  $('#perusahaan').val();
                    var email =  $('#email').val();
                    var telephone =  $('#telephone').val();
                    var alamat =  $('#alamat').val();
                    var kota =  $('#kota').val();
                    if(pic==''||perusahaan==''||email==''||telephone==''){
                        swal("Oops","Pastikan Anda telah melengkapi isian","error");
                        return false;
                    }
                    else if (!ValidateEmail(email)){
                        swal("Oops","Format email masih salah","error");
                        return false;
                    }
                    else{
                        $.ajax({
                            type: "POST",
                            url: "<?=base_url()?>proposal/newProposalNewClient",
                            data: {
                                pic:pic,
                                perusahaan:perusahaan,
                                email:email,
                                telephone:telephone,
                                alamat:alamat,
                                kota:kota,
                                user_id:user,
                                projek : projekBaru,
                                overviewbaru : overviewBaru
                            },
                            datatype: 'json',
                            success: function(data){
                                var datanya = JSON.parse(data);
                                console.log('ID proposal baru = '+datanya);
                                $('#proposalModal').modal('hide');
                                location.href = '<?php echo site_url('proposal/viewproposal/'); ?>' + datanya;
                            },
                            error:function(error){
                                console.log(error);
                            }
                        });
                    }
                }
                //end of new proposal with new client

                //new proposal client choosen
                else{
                    $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>proposal/newproposaltoDB",
                        data: {
                            user : user,
                            projek : projekBaru,
                            overviewbaru : overviewBaru,
                            klien : klien,
                        },
                        datatype: 'json',
                        success: function(data){
                            var datanya = JSON.parse(data);
                            console.log('ID proposal baru = '+datanya);
                            $('#proposalModal').modal('hide');
                            location.href = '<?php echo site_url('proposal/viewproposal/'); ?>' + datanya;
                        }, error:function(error){
                            console.log(error);
                        }
                    });
                }
                //end of new proposal client choosen
            }
        });
        <?php } ?>
        // END OF SCRIPT INDEX

        // SCRIPT CREATE
        <?php if($this->uri->segment(2) == 'viewproposal') { ?>
        $('#share-link').on('click',function(){
            $(this).select();
        });

        $(function(){
            var resize = function (node) {
                var offset = node.offsetHeight - node.clientHeight;
                jQuery(node).css('height', 'auto').css('height', node.scrollHeight + offset);
            };
            jQuery(document).bind("ready", function(){
                jQuery('textarea[data-autoresize]')
                    .bind('keyup input', function () {
                        resize(this);
                    })
                    .removeAttr('data-autoresize')
                    .addClass("resizing")
                    .trigger("input");
            });
            jQuery(document).trigger("ready");
        });

        <?php if(isset($detailSec)){?>
            var sec = <?php echo count($detailSec);?>+1;
        <?php } ?>
        var propid = $('#proposal-id').val();

        $(function() {
            $('#toggle-event').change(function() {
                var share = $(this).prop('checked');
                $.ajax({
                    type: "POST",
                    url: "<?=base_url()?>proposal/updatePermissionDocument/"+propid,
                    data: {share : share},
                    datatype: 'json',
                    success: function(response){
                        swal('Update Permission','Hak akses proposal berhasil diupdate','success');
                    }, error:function(error){
                        swal('Oops','Hak akses proposal gagal diupdate','error');
                    }
                });
            });
        });

        // Tambah section text
        $('#add-section-text').on('click',function(){
             $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>proposal/addSectionProposal",
                data: {
                    prop_id : propid,
                    section_urutan : sec,
                    judul : 'Judul Section '+sec,
                    isi : 'Isi Section '+sec,
                    tipe : 0
                },
                datatype: 'json',
                success: function(response){
                    swal('Section berhasil ditambahkan');
                    location.reload();
                }, error:function(error){
                    swal('Section Gagal ditambahkan');
                }
            });
        });
        // end of Tambah section text

        // Tambah section table
        $('#add-section-table').on('click',function(){
             $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>proposal/addSectionProposal",
                data: {
                    prop_id : propid,
                    section_urutan : sec,
                    judul : 'Judul Section '+sec,
                    item : 'item',
                    biaya :  0,
                    tipe : 1
                },
                datatype: 'json',
                success: function(response){
                    swal('Section berhasil ditambahkan');
                    location.reload();
                }, error:function(error){
                    swal('Section Gagal ditambahkan');
                }
            });
        });
        // end of Tambah section table

        // Tambah section images
        $('#add-section-images').on('click',function(){
             $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>proposal/addSectionProposal",
                data: {
                    prop_id : propid,
                    section_urutan : sec,
                    isi : 'Section dengan isi gambar',
                    judul : 'Section Gambar '+sec,
                    tipe : 2
                },
                datatype: 'json',
                success: function(response){
                    swal('Section berhasil ditambahkan');
                    location.reload();
                }, error:function(error){
                    swal('Section Gagal ditambahkan');
                }
            });
            sec++;
        });
        // end of Tambah section images

        // upload images
            // in-section
            $(document).on('click','.photo-tiles',function(){
                $('#image-upload').trigger('click');
            });
            $(document).on('change','#image-upload',function(){
                $("#btn-upload-img").click();
            });
            // end of images in-section
            // in-header-proposal
            $(document).on('click','.pilih-logo-baru',function(){
                $('#upload-header-image').trigger('click');
            });
            $(document).on('change','#upload-header-image',function(){
                document.getElementById('upload-logo-button').innerHTML = '<div class="loader" style="left:35%"></div> Uploading....';
                $("#btn-upload-img-header").click();
            });
            $(document).on('click','.pilih-logo-usaha',function(){
                var team_id = <?= $this->session->userdata('team_id')?>;
                document.getElementById('upload-logo-button').innerHTML = '<div class="loader" style="left:35%"></div> Uploading....';
                $.ajax({
                    type:"POST",
                    url: "<?php echo base_url()?>proposal/useOwnLogo",
                    data: {
                        team_id : team_id,
                        prop_id : propid
                    },
                    datatype: 'json',
                    success: function(response){
                        if(response == 11){
                            location.reload();
                        }else if(response == 0){
                            swal('Team anda belum memiliki logo');
                        }
                    }, error:function(error){
                        console.log('gagal update logo baru');
                    }
                });
            });
            // end of image in-header-proposal
            // hapus header proposal
            $(document).on('click','#hapus-logo-proposal',function(){
                swal({
                    title: "Hapus Logo di Proposal",
                    text: "Anda akan menghapus logo proposal. Lanjutkan?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    }).then((result) => {
                    if (result) {
                        $.ajax({
                            type:"POST",
                            url: "<?php echo base_url()?>proposal/delete_logo",
                            data: {
                                proposal_id : propid,
                            },
                            datatype: 'json',
                            success: function(response){
                                console.log('berhasil hapus logo')
                                location.reload();
                            }, error:function(error){
                                console.log('gagal hapus logo')
                                console.log(error);
                            }
                        });
                        $(this).closest('.section').remove();
                        sec--;
                    }
                })
            });
            // end of hapus header proposal
        // end of upload images
        
        // add item in section table
        $(document).on('click','.add-item',function(){
            var sectid = $(this).parent().parent().attr('secID');
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>proposal/addItemSection",
                data: {
                    prop_id : propid,
                    item :  'Item',
                    biaya : 0,
                    sec_id : sectid
                },
                datatype: 'json',
                success: function(response){
                    console.log(response);
                    location.reload();
                }, error:function(error){
                    console.log('gagal hapus section');
                }
            });
            sec++;
        });
        // end of add item in section table

        // Hapus section
        $(document).on('click','.delete-section',function(){
            var sectid = $(this).parent().attr('secID');
            swal({
                title: "Hapus Section",
                text: "Anda akan menghapus section ini. Lanjutkan?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                }).then((result) => {
                if (result) {
                    $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>proposal/deleteSectionProposal",
                        data: {
                            prop_id : propid,
                            section_id : sectid
                        },
                        datatype: 'json',
                        success: function(response){
                            console.log('berhasil hapus section');
                        }, error:function(error){
                            console.log('gagal hapus section');
                        }
                    });
                    $(this).closest('.section').remove();
                    sec--;
                }
            })
        });
        // end of Hapus section

        // Hapus item in section table
        $(document).on('click','.delete-item',function(){
            var itemid = $(this).parent().parent().attr('itemID');
            swal({
                title: "Hapus Item",
                text: "Anda akan menghapus item ini. Lanjutkan?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                }).then((result) => {
                if (result) {
                    $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>proposal/deleteItemSection",
                        data: {
                            item_id : itemid
                        },
                        datatype: 'json',
                        success: function(response){
                            console.log('berhasil hapus item');
                        }, error:function(error){
                            console.log('gagal hapus item');
                        }
                    });
                    $(this).parent().parent().remove();
                }
            })
        });

        // delete images
        $(document).on('click','.delete-image-section',function(){
            var imgid = $(this).attr('imgID');
            swal({
                title: "Hapus Gambar",
                text: "Anda akan menghapus gambar ini. Lanjutkan?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                }).then((result) => {
                if (result) {
                    $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>proposal/deleteImageSecProposal",
                        data: {
                            img_id : imgid
                        },
                        datatype: 'json',
                        success: function(response){
                            swal('Berhasil hapus gambar');
                            location.reload();
                        }, error:function(error){
                            swal('Gagal hapus gambar');
                        }
                    });
                }
            })
        });
        // end of delete images

        // autosave update judul section
        var timer;
        $(".judul-section").keyup(function(){
            var value = $(this).val();
            var sectid = $(this).parent().parent().attr('secID');
            clearTimeout(timer);
            document.getElementById('statusSave').innerHTML = '<div class="loader"></div> Saving....';
            if (event.keyCode >= 65 && event.keyCode <= 90 || event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode == 8 || event.keyCode == 13){
                timer = setTimeout(function () {
                    $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>proposal/updateJudulSection",
                        data: {
                            judul : value,
                            secid : sectid
                        },
                        datatype: 'json',
                        success: function(response){
                            if(response){
                                console.log(response);
                               document.getElementById('statusSave').innerHTML = 'Saved!';
                            }
                        }, error:function(error){
                            console.log('gagal update isi section');
                        }
                    });
                }, 1000);
            }
        });
        // end of autosave update judul section

        // autosave update isi section
        $(".isi-section").keyup(function(){
            var value = $(this).val();
            var sectid = $(this).parent().parent().attr('secID');
            clearTimeout(timer);
            document.getElementById('statusSave').innerHTML = '<div class="loader"></div> Saving....';
            if (event.keyCode >= 65 && event.keyCode <= 90 || event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode == 8 || event.keyCode == 13){
                timer = setTimeout(function () {
                    $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>proposal/updateIsiSection",
                        data: {
                            isi : value,
                            secid : sectid
                        },
                        datatype: 'json',
                        success: function(response){
                            if(response){
                                console.log(response);
                               document.getElementById('statusSave').innerHTML = 'Saved!';
                            }
                        }, error:function(error){
                            console.log('gagal update isi section');
                        }
                    });
                }, 1000);
            }
        });
        // end of autosave update isi section

        // autosave update item-input
        $(".item-input").keyup(function(){
            var value = $(this).val();
            var itemid = $(this).parent().parent().attr('itemID');
            clearTimeout(timer);
            document.getElementById('statusSave').innerHTML = '<div class="loader"></div> Saving....';
            if (event.keyCode >= 65 && event.keyCode <= 90 || event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode == 8 || event.keyCode == 13){
                timer = setTimeout(function () {
                    $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>proposal/updateItemSection",
                        data: {
                            isi : value,
                            item_id : itemid
                        },
                        datatype: 'json',
                        success: function(response){
                            if(response){
                               console.log(response);
                               document.getElementById('statusSave').innerHTML = 'Saved!';
                            }
                        }, error:function(error){
                            console.log('gagal update isi section');
                        }
                    });
                }, 1000);
            }
        });
        // end of autosave update item-input

        // autosave update item-input
        $(".biaya-input").keyup(function(){
            var value = $(this).val();
            var itemid = $(this).parent().parent().attr('itemID');
            clearTimeout(timer);
            document.getElementById('statusSave').innerHTML = '<div class="loader"></div> Saving....';
            if (event.keyCode >= 65 && event.keyCode <= 90 || event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode == 8 || event.keyCode == 13){
                timer = setTimeout(function () {
                    $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>proposal/updateBiayaSection",
                        data: {
                            isi : value,
                            item_id : itemid
                        },
                        datatype: 'json',
                        success: function(response){
                            if(response){
                               console.log(response);
                               document.getElementById('statusSave').innerHTML = 'Saved!';
                            }
                        }, error:function(error){
                            console.log('gagal update isi section');
                        }
                    });
                }, 1000);
            }
        });
        // end of autosave update item-input

        // autosave update signature
        $("#signature").keyup(function(){
            var value = $(this).val();
            clearTimeout(timer);
            document.getElementById('statusSave').innerHTML = '<div class="loader"></div> Saving....';
            if (event.keyCode >= 65 && event.keyCode <= 90 || event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode == 8 || event.keyCode == 13){
                timer = setTimeout(function () {
                    $.ajax({
                        type:"POST",
                        url:"<?=base_url()?>proposal/updateSignature",
                        data: {
                            prop_id: propid,
                            signature: value
                        },
                        datatype: 'json',
                        success: function(response){
                            console.log(response);
                            document.getElementById('statusSave').innerHTML = 'Saved!';
                        }, error: function(error){
                            console.log('gagal update isi section');
                        }
                    });
                }, 1000);
            }
        });
        // end of autosave update signature

        // autosave update title
        $("#title-prop").keyup(function(){
            var value = $(this).val();
            clearTimeout(timer);
            document.getElementById('statusSave').innerHTML = '<div class="loader"></div> Saving....';
            if (event.keyCode >= 65 && event.keyCode <= 90 || event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode == 8 || event.keyCode == 13){
                timer = setTimeout(function () {
                    $.ajax({
                        type:"POST",
                        url:"<?=base_url()?>proposal/updateJudul",
                        data: {
                            prop_id: propid,
                            title: value
                        },
                        datatype: 'json',
                        success: function(response){
                            console.log(response);
                            document.getElementById('statusSave').innerHTML = 'Saved!';
                        }, error: function(error){
                            console.log('gagal update judul proposal');
                        }
                    });
                }, 1000);
            }
        });
        // end of autosave update signature

        // modal fullsize image section
        $(document).ready(function() {
            $('.show-image').on('click', function(){
                var id_img = $(this).attr('imgID');
                $.ajax({
                    type: "POST",
                    url: '<?=base_url()?>userview/getImageSection',
                    data: {id_img:id_img},
                    dataType:'json',
                    success: function(data){
                        if(data){
                            var img = data[0];
                            $('#fullsize-image').attr("src", img.image_dir);
                            $('#modal-image').modal('show');
                        }
                    },
                    error: function (error){
                        console.log(error);
                    }
                });
            });
        });
        // end of modal fullsize image section

        // download pdf
        var link_id = $('#proposal-id').val();
        $(document).on('click','#download-proposal',function(){
            location.href = '<?= base_url('proposal/new_download/')?>'+link_id;
        });
        // end of download pdf

        // kirim proposal
        $('#btn-kirim-proposal').on('click',function(){
            var client_email = $('#send_proposal_client_email').val();
            var subject = $('#send_proposal_client_email_subject').val();
            var message = $('#send_proposal_client_email_message').val();

            if (client_email == '') {
                $('#email-client-msg').empty().append("<div class='alert alert-fit alert-danger text-little'>Email klien wajib diisi (minimal 1)</div>");
            }
            else if (subject == '') {
                $('#subject-msg').empty().append("<div class='alert alert-fit alert-warning text-little'>Kami menyarankan untuk mengisi subject email Anda</div>");
            }
            else if (message == '') {
                $('#message-msg').empty().append("<div class='alert alert-fit alert-danger text-little'>Pesan email wajib diisi</div>");
            }
            else {
                $('#btn-kirim-proposal').attr('disabled',true).html('Mengirim...');
                $.ajax({
                    type:"POST",
                    url: "<?php echo base_url()?>proposal/send_proposal",
                    data: {
                        'prop_id' : propid,
                        'sender_name' : $('#send_proposal_sender_name').val(),
                        'sender_email' : $('#send_proposal_sender_email').val(),
                        'client_email' : client_email,
                        'client_cc' : $('#send_proposal_client_cc').val(),
                        'client_name' : $('#send_proposal_client_name').val(),
                        'email_message' : $('#send_proposal_client_email_message').text(),
                        'email_subject' : subject
                    },
                    success: function(data){
                        // swal(data);
                        if(data == 1)
                        {
                            $('#btn-kirim-proposal').attr('disabled',false).html('<i class="fa fa-send" aria-hidden="true"></i> Kirim');
                            swal('Proposal Terkirim','Proposal berhasil dikirim ke email klien Anda','success');
                            $('#modal_kirim_proposal').modal('hide');
                        }
                        else
                        {
                            swal('Oops','Ada kesalahan kirim','error');
                        }

                    }, error:function(error){
                        swal('Oops','Ada kesalahan data','error');
                    }
                });
            }
        });
        // end of kirim proposal

        // proposal_style
        <?php if(!empty($style)){ ?>
                var fontstyle = '<?= $style->fontstyle?>';
                var headerbgcurrent = '<?= $style->headerbgcolor;?>';
                var headercolorcurrent = '<?= $style->headertextcolor;?>';
        <?php } else{ ?>
                var fontstyle = 'avenir';
                var headerbgcurrent = $('.default-bg').css("background-color");
                var headercolorcurrent = $('.default-font').css("color");
        <?php } ?>

        $('#proposal-content').css('font-family',fontstyle);
        $('#tool-font-style').val(fontstyle);
        $('#proposal-content .font-color').css('color', headercolorcurrent);
        $('#proposal-content .bg-color').css('background-color', headerbgcurrent);
        $('#proposal-content #header #line').css('border-color', headercolorcurrent);
        $('#proposal-content .border-bg-color').css('border-color',headerbgcurrent);
        // end of proposal_style

        // header font color
        $(".headerfont").spectrum({
            preferredFormat: "hex",
            color: headercolorcurrent,
            showInput: true,
            chooseText: 'Pilih',
            change: function(colorpick){
                var warna = colorpick.toHexString();
                $.ajax({
                    method: "POST",
                    url : "<?php echo base_url(); ?>proposal/updateHeaderFontColor",
                    data : {
                        idproposal: propid,
                        headfontcolor: warna
                    },
                    success : function(data){
                        if(data == 1){
                            $('#proposal-content .font-color').css('color', warna);
                            $('#proposal-content #header #line').css('border-color', warna);
                        }else{
                            swal("Oops","warna font header gagal diubah. Silahkan coba kembali.","error");
                        }
                    },error: function (jqXHR, textStatus, errorThrown){
                        swal("Oops","warna font header gagal diubah. Silahkan coba kembali.","error");
                    }
                });
            }
        });
        // end of header font color

        // header bg color
        $(".headerbg").spectrum({
            preferredFormat: "hex",
            color: headerbgcurrent,
            showInput: true,
            chooseText: 'Pilih Warna Header',
            change: function(colorpick){
                var warnabg = colorpick.toHexString();
                $.ajax({
                    method: "POST",
                    url : "<?php echo base_url(); ?>proposal/updateHeaderBgColor",
                    data : {
                        idproposal: propid,
                        headerbgcolor: warnabg
                    },
                    success : function(data){
                        if(data == 1){
                            $('#proposal-content .bg-color').css('background-color', warnabg);
                            $('#proposal-content .border-bg-color').css('border-color',warnabg);
                        }else{
                            swal("Oops","background header gagal diubah. Silahkan coba kembali.","error");
                        }
                    },error: function (jqXHR, textStatus, errorThrown){
                        swal("Oops","background header gagal diubah. Silahkan coba kembali.","error");
                    }
                });
            }
        });
        // end of header bg color

        // end of colorpicker toolbar

        // tool change font family
        $('#tool-font-style').on('change', function(){
            var font = $(this).val();
            $.ajax({
                method: "POST",
                url : "<?php echo base_url(); ?>proposal/updateProposalFontstyle",
                data : {
                    idproposal: propid,
                    fontstyle: font
                },
                success : function(data){
                    if(data == 1){
                        $('#proposal-content').css('font-family',font);
                    }else{
                        swal("Oops","Font gagal diubah. Silahkan coba kembali.","error");
                    }
                },error: function (jqXHR, textStatus, errorThrown){
                    swal("Oops","Font gagal diubah. Silahkan coba kembali.","error");
                }
            });
            
        });
        // end of tool change font family

        $('#copy-share-link').on('click',function(){
            var copyText = document.getElementById("share-link");
            swal({
                title: 'Perhatian',
                text: 'Kami tidak merekomendasikan Anda mengirimkan proposal ini menggunakan cara selain menggunakan tombol KIRIM di kanan atas. Bila Anda tetap menginginkan, silahkan bagikan link URL ini.'
            })
            .then((willShare)=>{
                if (willShare){
                    copyText.select();
                    document.execCommand("Copy");
                    $.ajax({
                        method: "POST",
                        url: "<?=base_url()?>proposal/log_copy_url",
                        data: { proposal: propid },
                        success: function(data){
                            swal('Link Copied','Link url proposal berhasil tersalin','success');
                        },error: function (jqXHR, textStatus, errorThrown){
                            swal('Coping Failed','Link url proposal gagal disalin','danger');
                        }
                    });
                }
            });
        });

        $('.selected').click(function(){
            var template = $(this).attr('tid');
            var id = $(this).attr('pid');
            $.ajax({
                method: "POST",
                url: "<?=base_url()?>proposal/updateTemplate",
                data: {
                    template: template,
                    id: id,
                },
                success: function(data){
                    var datanya = JSON.parse(data);
                    location.href = '<?= site_url('proposal/viewproposal/') ?>' + datanya;
                },
                error: function (jqXHR, textStatus, errorThrown){
                    swal("Template gagal terpasang","Invalid template proposal.","error");
                }
            });
        });

        $('.prop-design').click(function(){
            //swal('aku pilih ini');
            var template = $(this).attr('tid');

            $.ajax({
                method: "POST",
                url: "<?=base_url()?>proposal/updateTemplate",
                data: {
                    template: template,
                    id: propid,
                },
                success: function(data){
                    var datanya = JSON.parse(data);
                    location.href = '<?= site_url('proposal/viewproposal/') ?>' + datanya;
                },
                error: function (jqXHR, textStatus, errorThrown){
                    swal("Template gagal terpasang","Invalid template proposal.","error");
                }
            });
        });

        $('#change-client').click(function(){
            $('#modal-change-client').modal('show');
        });

        $('#ganti-client').on('click',function(){
            var value = $('#select-client').val();
            $.ajax({
                method: 'post',
                url: '<?=base_url()?>proposal/updateClientProposal',
                data: {
                    client: value,
                    id: propid
                },
                success: function(data){
                    $('#modal-change-client').modal('hide');
                    swal('Data Klien Proposal Updated','','success');
                    var datanya = JSON.parse(data);
                    location.href = '<?= site_url('proposal/viewproposal/') ?>' + datanya;
                },
                error: function (jqXHR, textStatus, errorThrown){
                    swal("Client Gagal Diganti","Data client proposal gagal diupdate.","error");
                }
            });
        });
        <?php } ?>
        // END OF SCRIPT CREATE
    });
</script>
