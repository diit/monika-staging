<?php
ob_start();
// =========== setup style ====================
function hex2rgb( $colour ) {
    if ( $colour[0] == '#' ) {
            $colour = substr( $colour, 1 );
    }
    if ( strlen( $colour ) == 6 ) {
            list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
    } elseif ( strlen( $colour ) == 3 ) {
            list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
    } else {
            return false;
    }
    $r = hexdec( $r );
    $g = hexdec( $g );
    $b = hexdec( $b );
    return array($r,$g,$b);
}

// proposal style
if(!empty($propstyle)){
	foreach($propstyle as $style){
		$headbgrgb = hex2rgb($style->headerbgcolor);
		$txtheadrgb = hex2rgb($style->headertextcolor);
		$fontdb = $style->fontstyle;
		if($fontdb == 'avenir'){
			$font = 'avenirltstd';
		}elseif($fontdb == 'opensans'){
			$font = 'opensans';
		}elseif($fontdb == 'Arial'){
			$font = 'arial';
		}elseif($fontdb == 'verdana'){
			$font = 'verdana';
		}else{
			$font = 'avenirltstd';
		}
	}
}else{
	$headbgrgb = hex2rgb('#656464');
	$txtheadrgb = hex2rgb('#ffffff');
	$font = 'avenirltstd';
}
// end of proposal style
// =========== end of setup style ====================

$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('Proposal - '.$detailProp->judul);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('Monika');
$pdf->SetDisplayMode('real', 'default');
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setPrintHeader(false);
$pdf->AddPage();

$fontheaderSM = 10;
$fontheaderMD = 11;
$fontheaderBG = 14;

// $pdf->SetFont("verdana", '', $fontheaderBG);
// $pdf->SetFont("opensans", '', $fontheaderBG);
// ================= Header ============================
$pdf->SetTextColor($txtheadrgb['0'], $txtheadrgb['1'], $txtheadrgb['2']);
$pdf->SetFillColor($headbgrgb['0'], $headbgrgb['1'], $headbgrgb['2']);
$xheader = 90;
$pdf->SetFont($font, '', $fontheaderBG);
$pdf->Ln(3);
$pdf->Cell($xheader);
$test = $pdf->Cell(40,10,$detailProp->judul,0,1,'L');
$pdf->SetFont($font, '', $fontheaderMD);
$pdf->Cell($xheader);
$test .= $pdf->Cell(40,10,'Dipersiapkan Untuk',0,1,'L');
$pdf->SetFont($font, '', $fontheaderBG);
$pdf->Cell($xheader);
$test .= $pdf->Cell(40,0,$detailProp->perusahaan,0,1,'L');
$pdf->SetFont($font, '', $fontheaderMD);
$pdf->Cell($xheader);
$test .= $pdf->Cell(40,8,$detailProp->nama_pic,0,1,'L');
$pdf->SetFont($font, '', $fontheaderBG);
$pdf->Cell($xheader);
$test .= $pdf->Cell(40,8,$this->monikalib->format_date_indonesia($detailProp->tgl_buat),0,1,'L');
$pdf->SetFont($font, '', $fontheaderMD);
$pdf->Cell($xheader);
$test .= $pdf->Cell(50,10,'Oleh',0,0,'L');
$test .= $pdf->Cell(40,10,'Kontak',0,1,'L');
$pdf->Cell($xheader);
$test .= $pdf->Cell(50,7,$detailProp->user_name,0,0,'L');
$test .= $pdf->Cell(40,7,$detailProp->user_email,0,1,'L');
$pdf->Cell($xheader);
$test .= $pdf->Cell(60,5,$team->team_name,0,0,'L');

// Logo team
$content = $detailProp->logo_url;
if(!empty($content)){
	// $html1 = '<img src="'.$content.'" alt="" width="150" border="0">';
	// $pdf->writeHTMLCell(150,'', 30, 35, $html1, 0, 1,0,true);
	$pdf->Image($content, 30, 30, 50,'','');
}
// end of Logo team
$pdf->MultiCell(210, 90, $test, 0, 'L', 1, 1, 0, 0, true);
// ================= end of Header ============================
// ================= content ============================
$pdf->SetTextColor(0,0,0);
$xSection = 15;
$pdf->Ln(10);
$pdf->setCellHeightRatio(1.5);
foreach($detailSec as $sec){
	$yContent = $pdf->getY();
	if($sec->sec_tipe == 0){ 
		$pdf->SetFont($font, '', $fontheaderBG);
		$pdf->MultiCell(60,10,$sec->sec_judul, 0, 'L', 0, 1, $xSection, $yContent, true);
		$pdf->SetFont($font, '', $fontheaderSM);
		$pdf->MultiCell(110, 0,$sec->sec_isi, 0, 'L', 0, 1, $xSection+70,$yContent , true);
	}elseif($sec->sec_tipe == 1){
		$CI =& get_instance();
		$CI->load->model('proposal_model');
		$detailItem = $CI->proposal_model->detailSecTable($sec->sec_id, $detailProp->id);
		$pdf->SetFont($font, '', $fontheaderBG);
		$pdf->MultiCell(60,10,$sec->sec_judul, 0, 'L', 0, 0, $xSection, $yContent, true);
		$pdf->SetFont($font, '', $fontheaderSM);
		foreach($detailItem as $item){
			$pdf->MultiCell(55, 7, $item->item, 0, 'L', 0, 0, $xSection+70, '', true);
			$pdf->MultiCell(55, 7, $item->biaya, 0, 'R', 0, 1, '', '', true);
		}
	}elseif($sec->sec_tipe == 2){
		$instance =& get_instance();
		$instance->load->model('proposal_model');
		$detailImage = $instance->proposal_model->detailSecImage($sec->sec_id, $detailProp->id);
		$pdf->SetFont($font, '', $fontheaderBG);
		$pdf->MultiCell(60,10,$sec->sec_judul, 0, 'L', 0, 1, $xSection, $yContent, true);
		$xForImg = $xSection+70;
		foreach($detailImage as $image){
			$content1 = $image->image_dir;
			// $img = '<img src="'.$content.'" alt="" width="150" border="0">';
			// $pdf->writeHTMLCell(150,'', 30, 35, $img, 0, 1,0,true);
			$pdf->Image($content1, $xForImg, $yContent, 38,'','');
			$xForImg = $xForImg + 42;
		}
	}
	$pdf->Ln(10);
}
// ================= end of content ============================

$pdf->Output($detailProp->judul, 'I');
?>
