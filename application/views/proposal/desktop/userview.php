<div id="user-view-content">
    <div class="row justify-content-center">
        <!-- menu -->
        <div class="col-sm-12">
            <div class="box toolbar-box">
                <?php
                    $random = md5(mt_rand(1,10000));
                    $first = substr($random,0,5);
                    $last = substr($random,5,10);
                    $id_prop = $first.$detailProp->id.$last;
                ?>
                <button class="btn btn-sm btn-primary pull-right" id="accept" prop-id="<?=$id_prop?>"><i class="fas fa-check"></i> setujui</button>
                <button class="btn btn-sm btn-outline-secondary pull-right" <?= ($this->monikalib->currentUserPlan()==0) ? 'id="notice-plan"' : 'id="download-proposal-userview"' ?>><i class="far fa-arrow-alt-circle-down"></i> download</button>
                <p class="pull-left">PREVIEW PROPOSAL</p>
            </div>
        </div>
        <!-- end of menu -->

        <!-- proposal content -->
        <div class="col-md-9 col-sm-11">
            <div class="box top-space" id="proposal-content">
                <?php $this->load->view('proposal/desktop/userview/'.$detailProp->template); ?>
            </div>
        </div>
        <!-- end of proposal content -->
    </div>
</div>
<!-- modal image section -->
<div class="modal fade" id="modal-image" tabindex="-1" role="dialog" aria-labelledby="modalImageLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <img src="" alt="" id="fullsize-image">
    </div>
  </div>
</div>
<!-- end of modal image section -->
<div id="console" style="display:none">
    <ul>
<?php foreach($detailSec as $sec){?>
        <li data-time="0" data-field="<?= $sec->sec_id?>" total-time=""><?= $sec->sec_judul?> - <span class="total">0</span>s</li>
<?php }?>
    </ul>
</div>
<?php 
    $random = md5(mt_rand(1,10000));
    $first = substr($random,0,5);
    $last = substr($random,5,10);
    $prop_id = $first.$detailProp->id.$last;
?>
<input type="hidden" value="<?= $prop_id?>" id="proposal-link-id">
<script src="<?php echo base_url(); ?>assets/js/screentime.js"></script>
<script src="<?= base_url() ?>node_modules/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        // proposal_style
        <?php if(!empty($style)){ ?>
            var fontstyle = '<?= $style->fontstyle ?>';
            var headerbgcurrent = '<?= $style->headerbgcolor ?>';
            var headercolorcurrent = '<?= $style->headertextcolor ?>';
        <?php } else{ ?>
            var fontstyle = 'avenir';
            var headerbgcurrent = 'grey';
            var headercolorcurrent = '#fff';
        <?php }?>

        $('#proposal-content').css('font-family',fontstyle);
        $('#proposal-content .font-color').css('color', headercolorcurrent);
        $('#proposal-content .bg-color').css('background-color', headerbgcurrent);
        $('#proposal-content #header #line').css('border-color', headercolorcurrent);
        $('#proposal-content .border-bg-color').css('border-color',headerbgcurrent);
        // end of proposal_style

        $('.show-image').on('click', function(){
            var id_img = $(this).attr('imgID');
            $.ajax({
                type: "POST",
                url: '<?=base_url()?>userview/getImageSection',
                data: {id_img:id_img},
                dataType:'json',
                success: function(data){
                    if(data){
                        var img = data[0];
                        $('#fullsize-image').attr("src", img.image_dir);
                        $('#modal-image').modal('show');
                    }
                },
                error: function (error){
                    console.log(error);
                }
            });
        });

        var varscreen;
        var ids = $('.section--userview').map(function() {
            return $(this).attr('secID');
        });

        var section = $('.userview-judul-section').map(function(){
            return $(this).text();
        });

        var format = [];
        for (i = 0; i < ids.length; i++)
        {
            var raw_format = {
                selector: '#'+ids[i],
                name: ids[i]
            };
            format.push(raw_format);
        }
        // console.log(format);       

        // console.log(fullSection);
        var time = 0;
        var counter = 0;
        var proposal_id = <?= $detailProp->id;?>;
        $.screentime({
            fields: format,
            reportInterval: 1,
            percentOnScreen: "65%",
            callback: function(data) {
                $.each(data, function(key, val) {
                    var $elem = $('#console li[data-field="' + key + '"]'); //pilih element untuk store value per section
                    var current = parseInt($elem.data('time'), 10); // initial variabel
                        $elem.data('time', current + val); // store value pada atribute data-time
                        // $elem.find('span').html(current += val); // menampilkan total waktu pada element span di dalam element li yang dipilih diawal
                        $elem.attr('total-time',current);
                    });
                time++;
                if(time%5 == 0)
                {
                    counter++;
                    time_per_section = [];
                    var section_time = $('#console li');
                    $(section_time).each(function(){
                        section = {};
                        section[$(this).data('field')] = $(this).attr('total-time');
                        time_per_section.push(section);
                    });
                    // console.log(time_per_section);
                    $.ajax({
                        type: "POST",
                        url : "<?php echo site_url('userview/tracking')?>",
                        data : {
                            proposal_id : proposal_id,
                            proposal_section_track : time_per_section,
                            tracking_count : counter
                        },
                        success: function(data)
                        {
                            // console.log('Berhasil');
                        }
                    });
                }
            }
        });

        var link_id = $('#proposal-link-id').val();
        $(document).on('click','#download-proposal-userview',function(){
            location.href = '<?= base_url('userview/downloadPDF/')?>'+link_id;
        });

        $(document).on('click','#accept',function(){
            var proposal_id = $(this).attr('prop-id');
            swal({
                title: 'Setujui proposal ini?',
                text: 'Persetujuan proposal akan secara otomatis diberitahukan kepada pengirim proposal ini.',
                button: true,
                dangerMode: true
            })
            .then((willDelete) => {
            if (willDelete) {
              $.ajax({
                url : "<?php echo site_url('userview/signing')?>",
                data : {
                  proposal_id : proposal_id
                },
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                  swal("Proposal berhasil disetujui", {
                    icon: "success",
                  });
                  window.location.reload();
                }
              });
            }
          });
        });

        $('#notice-plan').click(function(){
            swal('Download PDF Proposal','Proposal belum bisa di download, karena pengirim menggunakan plan Personal. Silahkan hubungi pengirim untuk upgrade plan.');
        });
    });
</script>
