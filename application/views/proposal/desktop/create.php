<?php
    $random = md5(mt_rand(1,10000));
    $first = substr($random,0,5);
    $last = substr($random,5,10);
    $prop_id = $first.$detailProp->id.$last;
?>
<div id="content">
    <div class="row justify-content-center">
        <!-- proposal menu -->
        <div class="col-sm-12">
            <div class="box toolbar-box">
                <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-kirim-proposal"><i class="far fa-envelope"></i> kirim</button>

                <button class="btn btn-outline-secondary pull-right" <?= (($this->monikalib->currentUserPlan()==0) || (count($this->monikalib->getUserPlan())==0)) ? 'data-toggle="tooltip" data-placement="bottom" title="Upgrade plan untuk download proposal"' : 'id="download-proposal"' ?>><i class="far fa-arrow-alt-circle-down"></i> download</button>

                <button class="btn btn-outline-secondary pull-right" id="copy-share-link"><i class="far fa-clone"></i> copy url share</button>
                <input class="form-control no-border pull-right text-little" value="<?= $link?>" style="width:250px" id="share-link">
                <h3 class="pull-left">PREVIEW</h3>
            </div>
        </div>
        <!-- end of proposal menu -->

        <!-- proposal toolbar -->
        <div class="col-md-9 col-sm-11">
            <div class="box top-space toolbar-box" style="height:57px">
                <div class="row">
                    <div class="col-md-3">
                        <select class="form-control text-little" style="display:inline-block; width:90%" id="tool-font-style">
                            <option value="avenir">Avenir</option>
                            <option value="Arial">Arial</option>
                            <option value="verdana">Verdana</option>
                            <option value="Sans-serif">Sans serif</option>
                            <option value="opensans">Open Sans</option>
                        </select>
                    </div>
                    <div class="col-md-3 no-padding" style="margin-top:3px">
                        <p class="d-block float-left mr-2 text-little">Header font color :</p>
                        <div class="d-block input-group">
                            <input type="text" class="headerfont" class="form-control" >
                        </div>
                    </div>
                    <div class="col-md-3" style="margin-top:3px">
                        <p class="d-block float-left mr-2 text-little">Header color :</p>
                        <div class="input-group d-block"> 
                            <input type="text" class="headerbg" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3 text-little">
                        <?php if ($detailProp->user_id == $this->session->userdata('user_id')) { ?>
                        <form class="form-inline">
                            <input type="checkbox" data-toggle="toggle" data-on="share to team" data-off="private" id="toggle-event" data-onstyle="primary" data-size="small" <?= ($detailProp->share_team == 1) ? 'checked' : '' ?>>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of proposal toolbar -->
        <!-- proposal content -->
        <div class="col-md-9 col-sm-11">
            <div class="box top-space" id="proposal-content">
                <!-- load template proposal -->
                <?php $this->load->view('proposal/template/'.$detailProp->template); ?>
                <!-- end of load template proposal -->
            </div>
            <!-- hidden variable for proposal -->
            <input type="hidden" value="<?=$prop_id?>" id="proposal-id">
            <!-- end of hidden variable for proposal -->
        </div>
        <!-- end of proposal content -->
    </div>

    <!-- choose template -->
    <div class="fix-bottom">
        <div class="accordion" id="choose-template">
            <div class="card">
              <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne">
                <p class="mb-0">
                    Pilih template proposal
                </p>
              </div>

            <div id="collapseOne" class="collapse">
                <div class="card-body">
                    <ul id="list-template">
                        <?php foreach($template as $d){ ?>
                        <li class="prop-design" tid="<?=$d->name?>">
                            <img src="<?=base_url()?>assets/img/proposal_template/<?=$d->img?>" style="height:150px">
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
          </div>
        </div>
    </div>
    <!-- end of choose template -->
</div>

<!-- modal image section -->
<div class="modal fade" id="modal-image" tabindex="-1" role="dialog" aria-labelledby="modalImageLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="">
        <img src="" alt="" id="fullsize-image" style="height:auto; width:100%">
    </div>
  </div>
</div>
<!-- end of modal image section -->

<!-- modal kirim  -->
<div class="modal fade modal-kirim" id="modal-kirim-proposal" tabindex="-1" role="dialog" aria-labelledby="modalKirimProposal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header text-center">
            <h5 class="modal-title">Kirim Proposal</h5>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label for="">Nama Pengirim</label>
                    <input class="form-control" type="text" placeholder="nama pengirim" value="<?= $detailProp->user_name?>" id="send_proposal_sender_name" readonly>
                </div>
                <div class="col-md-6">
                    <label for="">Email Pengirim</label>
                    <input class="form-control" type="email" placeholder="email pengirim" value="<?= $detailProp->user_email?>" id="send_proposal_sender_email" readonly>
                </div>
            </div> <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Email Klien</label>
                        <input type="hidden" value="<?= $detailProp->nama_pic?>" id="send_proposal_client_name">
                        <input class="form-control" type="text" data-role="tagsinput" value="<?= $detailProp->client_email?>" id="send_proposal_client_email">
                        <div id="email-client-msg"></div>
                    </div>
                    <div class="form-group">
                        <label for="">CC</label>
                        <input class="form-control" type="email" data-role="tagsinput" placeholder="email klien" id="send_proposal_client_cc">
                    </div>
                    <div class="form-group">
                        <label for="">Subjek</label>
                        <input class="form-control" type="text" placeholder="Judul Proposal" value="<?= $detailProp->judul?>" id="send_proposal_client_email_subject">
                        <div id="subject-msg"></div>
                    </div>
                    <div class="form-group">
                        <label for="">Pesan</label>
                        <textarea class="form-control" name="pesan-proposal" cols="30" rows="10" id="send_proposal_client_email_message">Dengan Hormat,
Berikut Proposal [...] yang bisa Anda baca. 
Klik di sini > <?= $link?> 
Bila ada pertanyaan silahkan langsung tanyakan ke saya dengan balas email ini.

Terima kasih,
<?= $detailProp->user_name?> 
<?= $team->team_name?></textarea>
                        <div id="message-msg"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">batal</button>
            <button class="btn btn-primary" type="button" id="btn-kirim-proposal">kirim</button>
        </div>
    </div>
  </div>
</div>
<!-- end of modal kirim  -->

<!-- modal crud logo -->
<div class="modal fade" id="upload-header-logo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="exampleModalLongTitle">Upload Logo Proposal Anda</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="upload-logo-button">
                <button class="btn btn-primary pilih-logo-usaha"><i class="fas fa-id-card-alt"></i> Gunakan Logo Usaha</button>
                <button class="btn btn-tosca pilih-logo-baru"><i class="fas fa-image"></i> Pilih Logo Baru</button>
                <?php if($detailProp->logo_url !== null){?>
                    <button class="btn btn-danger" id="hapus-logo-proposal"><i class="fas fa-trash-alt"></i> Hapus Logo</button>
                <?php }?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<!-- end of modal crud logo -->

<!-- modal change client -->
<div class="modal fade" id="modal-change-client" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header text-center">
            <h5 class="modal-title">Ganti Klien Proposal</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <select class="form-control" id="select-client">
                <?php
                foreach($client as $d) {
                    $random = md5(mt_rand(1,10000));
                    $first = substr($random,0,5);
                    $last = substr($random,5,10);
                    $id = $first.$d->id_client.$last;
                ?>
                <option value="<?=$id?>" <?= $detailProp->id_client == $d->id_client ? 'selected' : '' ?>><?=$d->nama_pic?></option>
                <?php } ?>
            </select>
        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">batal</button>
            <button class="btn btn-primary" type="button" id="ganti-client">ganti</button>
        </div>
      </div>
    </div>
</div>
<!-- end of modal change client -->
