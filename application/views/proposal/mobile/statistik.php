<div id="content">
    <div class="box">
        <?php
            $random = md5(mt_rand(1,10000));
            $first = substr($random,0,5);
            $last = substr($random,5,10);
            $urlrand = $first.$detailProp->id.$last;
        ?>
        <p>Statistik proposal: <a href="#" class="bold"><?= $detailProp->judul ?></a></p>
        &nbsp;
        <table class="table dashboard">
            <tr class="text-center">
                <th>Konten</th>
                <th>Rata-rata Lama Membaca <br>(detik)</th>
            </tr>
            <?php
            if (($this->monikalib->currentUserPlan()==0) || (count($this->monikalib->getUserPlan())==0)) { ?>
                <tr>
                    <td colspan="2" class="text-center"><br><a href="<?=site_url('plan')?>" class="btn btn-outline-primary btn-small text-small">upgrade plan untuk tahu lebih lengkap</a></td>
                </tr>
            <?php
            } else {
                if(!empty($konten_baca)) {
                    foreach ($konten_baca as $d) { ?>
                <tr>
                    <td><?= $d->section ?></td>
                    <td class="text-center"><?= round($d->lama) ?></td>
                </tr>
                <?php } }
                else { ?>
                <tr>
                    <td colspan="2" class="text-center grey" style="padding-top:40px">Dokumen belum dibaca</td>
                </tr>
            <?php } } ?>
        </table>
        &nbsp;
        <table class="no-border">
            <tr>
                <td>Rata-rata dibaca pada pukul</td>
                <td style="padding-left:10px" class="bold"> :
                    <?php if (($this->monikalib->currentUserPlan()==0) || (count($this->monikalib->getUserPlan())==0)) { ?>
                    <i class="fas fa-info-circle"></i>
                    <?php } else { ?>
                    <?= ((empty($best_hour->hour))&&(empty($best_minute->minute))) ? '-' : $best_hour->hour.'.'.$best_minute->minute ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td>Dibaca sebanyak</td>
                <td style="padding-left:10px" class="bold"> :
                    <?php if (($this->monikalib->currentUserPlan()==0) || (count($this->monikalib->getUserPlan())==0)) { ?>
                    <i class="fas fa-info-circle"></i>
                    <?php } else { ?>
                    <?= $total_baca->total ?> x
                    <?php } ?>
                </td>
            </tr>
        </table>
    </div>
</div>
