<script src="<?= base_url() ?>node_modules/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-tagsinput.js"></script>

<script>
    $(document).ready(function() {
        // SCRIPT INDEX
        <?php if (($this->uri->segment(1) == 'proposal') && (empty($this->uri->segment(2)))) { ?>
        $('#history').hide();
        $('.view-history').click(function() {
            $('#history').toggle();
        });

        $('.generate-invoice').on('click',function(){
            swal({
                title: "Buat invoice dari proposal?",
                text: "Invoice bisa diakses dan diedit pada menu Invoice.",
                buttons: true,
                confirmMode: true,
            });
        });

        $('.arsip-proposal').on('click',function(){
            var proposal_id = $(this).attr('pid');
            swal({
              title: "Apakah Anda yakin?",
              text: "Proposal masih bisa diakses pada menu Arsip.",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $.ajax({
                    type: "POST",
                    url: '<?=base_url()?>arsip/archive',
                    data: {
                        'pid' : proposal_id
                      },
                    success: function(data)
                    {
                      $('#div_list_prop_'+proposal_id).remove();
                      swal("Archived","Proposal berhasil diarsipkan","success");
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      swal("Oops","Proposal gagal diarsipkan. Silahkan mencoba kembali.","error");
                    }
                });
              }
            });
        });

        $('#cari-proposal').keyup(function(){
          var query = $(this).val();
            $.ajax({
              url:"<?php echo base_url();?>proposal/proposal",
              method:"post",
              data:{query:query},
              success:function(data){
                $('#list-proposal').remove();
                $('#list-cari-proposal').html(data);
              }
            });
        });

        $('#add').click(function(){
            swal('Buat proposal melalui desktop view di PC atau Laptop.');
        });
        <?php } ?>
        // END OF SCRIPT INDEX

        //SCRIPT DETAIL
        <?php if ($this->uri->segment(2) == 'viewproposal') { ?>
        swal('Preview Proposal','Proposal yang tampil mungkin tidak sesuai dengan design yang Anda buat, karena menyesuaikan layar gadget Anda. Download proposal atau lihat menggunakan PC atau laptop untuk melihat design yang lebih baik.');

        <?php
            $random = md5(mt_rand(1,10000));
            $first = substr($random,0,5);
            $last = substr($random,5,10);
            $id_prop = $first.$detailProp->id.$last;
        ?>

        function copyToClipboard(text) {
            var selected = false;
            var el = document.createElement('textarea');
            el.value = text;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            if (document.getSelection().rangeCount > 0) {
                selected = document.getSelection().getRangeAt(0)
            }
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
            if (selected) {
                document.getSelection().removeAllRanges();
                document.getSelection().addRange(selected);
            }
        };

        $('#copy-share-link').on('click',function(){
            swal({
                title: 'Perhatian',
                text: 'Kami tidak merekomendasikan Anda mengirimkan proposal ini menggunakan cara selain menggunakan tombol KIRIM di kanan atas. Bila Anda tetap menginginkan, silahkan bagikan link URL ini.'
            })
            .then((willShare)=>{
                if (willShare){
                    copyToClipboard('<?=$link?>');
                    $.ajax({
                        method: "POST",
                        url: "<?=base_url()?>proposal/log_copy_url",
                        data: { proposal: '<?$id_prop?>' },
                        success: function(data){
                            swal('Link Copied','Link url proposal berhasil tersalin','success');
                        },error: function (jqXHR, textStatus, errorThrown){
                            swal('Coping Failed','Link url proposal gagal disalin','danger');
                        }
                    });
                }
            });
        });

        $(document).on('click','#download-proposal',function(){
            location.href = '<?= base_url('proposal/new_download/'.$id_prop)?>';
        });

        $('#notice-plan').on('click',function(){
            swal('Download Proposal','Upgrade plan ke Startup atau Agency untuk download proposal');
        });

        $('#btn-kirim-proposal').on('click',function(){
            var client_email = $('#send_proposal_client_email').val();
            var subject = $('#send_proposal_client_email_subject').val();
            var message = $('#send_proposal_client_email_message').val();

            if (client_email == '') {
                $('#email-client-msg').empty().append("<div class='alert alert-fit alert-danger text-little'>Email klien wajib diisi (minimal 1)</div>");
            }
            else if (subject == '') {
                $('#subject-msg').empty().append("<div class='alert alert-fit alert-warning text-little'>Kami menyarankan untuk mengisi subject email Anda</div>");
            }
            else if (message == '') {
                $('#message-msg').empty().append("<div class='alert alert-fit alert-danger text-little'>Pesan email wajib diisi</div>");
            }
            else {
                $('#btn-kirim-proposal').attr('disabled',true).html('Mengirim...');
                $.ajax({
                    type:"POST",
                    url: "<?php echo base_url()?>proposal/send_proposal",
                    data: {
                        'prop_id' : '<?=$id_prop?>',
                        'sender_name' : $('#send_proposal_sender_name').val(),
                        'sender_email' : $('#send_proposal_sender_email').val(),
                        'client_email' : client_email,
                        'client_cc' : $('#send_proposal_client_cc').val(),
                        'client_name' : $('#send_proposal_client_name').val(),
                        'email_message' : $('#send_proposal_client_email_message').text(),
                        'email_subject' : subject
                    },
                    success: function(data){
                        if(data == 1)
                        {
                            $('#btn-kirim-proposal').attr('disabled',false).html('<i class="fa fa-send" aria-hidden="true"></i> Kirim');
                            swal('Proposal Terkirim','Proposal berhasil dikirim ke email klien Anda','success');
                            $('#modal_kirim_proposal').modal('hide');
                        }
                        else
                        {
                            swal('Oops','Ada kesalahan kirim','error');
                        }

                    }, error:function(error){
                        swal('Oops','Ada kesalahan data','error');
                    }
                });
            }
        });

        $('#edit').click(function(){
            swal('Edit Proposal','Anda bisa mengubah konten proposal melalui PC atau laptop');
        });
        <?php } ?>
        //END OF SCRIPT DETAIL
    });
</script>
