<div id="content">
    <!-- menu -->
    <div class="box toolbar-box">
        <?php
            $random = md5(mt_rand(1,10000));
            $first = substr($random,0,5);
            $last = substr($random,5,10);
            $id_prop = $first.$detailProp->id.$last;
        ?>
        <button class="btn btn-sm btn-outline-primary pull-right" data-toggle="modal" data-target="#modal-kirim-proposal"><i class="far fa-envelope"></i> kirim</button>
        <button class="btn btn-sm btn-outline-secondary pull-right" <?= (($this->monikalib->currentUserPlan()==0) || ($this->monikalib->getUserPlan()==1)) ? 'id="notice-plan"' : 'id="download-proposal"' ?> ><i class="far fa-arrow-alt-circle-down"></i> download</button>
        <button class="btn btn-sm btn-outline-secondary pull-right" id="copy-share-link"><i class="far fa-clone"></i> share url</button>
        <p style="color:transparent">.</p>
    </div>
    <!-- end of menu -->

    <!-- proposal content -->
    <div class="box top-space" id="proposal-content">
        <div id="header">
            <h5 class="bold"><?= $detailProp->judul;?></h5>
            &nbsp;
            <p class="text-little">Dipersiapkan untuk</p>
            <h5><?= $detailProp->perusahaan;?></h5>
            <p><?= $detailProp->nama_pic;?></p>
            <p class="text-little">10 April 2018</p>

            <p><span class="text-little">Oleh</span><br><?= $detailProp->user_name;?></p>
            <p class="bold"><?= $team->team_name?></p>
            <p><span class="text-little">Kontak</span><br><?= $detailProp->user_email;?></p>
        </div>

        <div id="fill" class="sec-sortable">
            <?php foreach($detailSec as $sec){
                if($sec->sec_tipe == 0){ ?>
                <div class="section section--userview" secID="<?= $sec->sec_id?>" id="<?= $sec->sec_id?>">
                    <p class="userview-judul-section bold"><?= $sec->sec_judul;?></p>
                    <p class="userview-isi-section"><?= $sec->sec_isi;?></p>
                </div>
                <?php }
                elseif($sec->sec_tipe == 1){
                    $CI =& get_instance();
                    $CI->load->model('proposal_model');
                    $detailItem = $CI->proposal_model->detailSecTable($sec->sec_id, $detailProp->id);
                ?>
                <div class="section section--userview" secID="<?= $sec->sec_id?>" id="<?= $sec->sec_id?>">
                    <p class="userview-judul-section bold"><?= $sec->sec_judul;?></p>
                    <table class="section-table userview-isi-section">
                        <tbody class="section-table-body">
                        <?php foreach($detailItem as $item){?>
                            <tr class="item-in-section">
                                <td style="width:80%">
                                    <p class="userview-item-section"><?= $item->item;?></p>
                                </td>
                                <td class="text-right">
                                    <p class="userview-biaya-section"><?= 'Rp '.number_format($item->biaya,0,',','.');?></p>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
                <?php }
                    elseif($sec->sec_tipe == 2){
                        $instance =& get_instance();
                        $instance->load->model('proposal_model');
                        $detailImage = $instance->proposal_model->detailSecImage($sec->sec_id, $detailProp->id);
                ?>
                <div class="row section section--userview" secID="<?= $sec->sec_id?>" id="<?= $sec->sec_id?>">
                    <p class="userview-judul-section"><?= $sec->sec_judul;?></p>
                    <div class="userview-isi-section">
                        <?php foreach($detailImage as $image){?>
                                <div class="photo-in-section">
                                    <img class="show-image" imgID="<?= $image->image_id?>" src="<?= $image->image_dir ?>" alt="">
                                </div>
                        <?php }?>
                    </div>
                </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <!-- end of proposal content -->

    <button id="edit" class="btn btn-primary btn-fab"><i class="fas fa-pen"></i></button>

    <div id="console" style="display:none">
        <ul>
    <?php foreach($detailSec as $sec){?>
            <li data-time="0" data-field="<?= $sec->sec_id?>" total-time=""><?= $sec->sec_judul?> - <span class="total">0</span>s</li>
    <?php }?>
        </ul>
    </div>
</div>

<!-- modal image section -->
<div class="modal fade" id="modal-image" tabindex="-1" role="dialog" aria-labelledby="modalImageLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <img src="" alt="" id="fullsize-image">
    </div>
  </div>
</div>
<!-- end of modal image section -->

<!-- modal kirim  -->
<div class="modal fade modal-kirim" id="modal-kirim-proposal" tabindex="-1" role="dialog" aria-labelledby="modalKirimProposal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header text-center">
            <h5 class="modal-title">Kirim Proposal</h5>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label for="">Nama Pengirim</label>
                    <input class="form-control" type="text" placeholder="nama pengirim" value="<?= $detailProp->user_name?>" id="send_proposal_sender_name" readonly>
                </div>
                <div class="col-md-6">
                    <label for="">Email Pengirim</label>
                    <input class="form-control" type="email" placeholder="email pengirim" value="<?= $detailProp->user_email?>" id="send_proposal_sender_email" readonly>
                </div>
            </div> <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Email Klien</label>
                        <input type="hidden" value="<?= $detailProp->nama_pic?>" id="send_proposal_client_name">
                        <input class="form-control" type="text" data-role="tagsinput" value="<?= $detailProp->client_email?>" id="send_proposal_client_email">
                        <div id="email-client-msg"></div>
                    </div>
                    <div class="form-group">
                        <label for="">CC</label>
                        <input class="form-control" type="email" data-role="tagsinput" placeholder="email klien" id="send_proposal_client_cc">
                    </div>
                    <div class="form-group">
                        <label for="">Subjek</label>
                        <input class="form-control" type="text" placeholder="Judul Proposal" value="<?= $detailProp->judul?>" id="send_proposal_client_email_subject">
                        <div id="subject-msg"></div>
                    </div>
                    <div class="form-group">
                        <label for="">Pesan</label>
                        <textarea class="form-control" name="pesan-proposal" cols="30" rows="10" id="send_proposal_client_email_message">Dengan Hormat,
Berikut Proposal [...] yang bisa Anda baca.
Klik di sini > <?= $link?>
Bila ada pertanyaan silahkan langsung tanyakan ke saya dengan balas email ini.

Terima kasih,
<?= $detailProp->user_name?>
<?= $team->team_name?></textarea>
                        <div id="message-msg"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">batal</button>
            <button class="btn btn-primary" type="button" id="btn-kirim-proposal">kirim</button>
        </div>
    </div>
  </div>
</div>
<!-- end of modal kirim  -->
