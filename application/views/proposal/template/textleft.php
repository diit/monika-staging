<div class="row justify-content-center bg-color font-color default-bg default-font" id="header" <?= (empty($propstyle)) ? 'style="background-color:gold;color:#111111"' : '' ?>>
    <div class="col-sm-11">
        <!-- logo proposal -->
        <div id="photo" style="margin: 10px 0 40px 0; width:100px; height:100px; border:<?=$detailProp->logo_url == null ? 'dashed 3px rgba(143,143,143,0.5)' : '0px' ?>">
            <?php if($detailProp->logo_url == null){?>
            <button class="proposal-logo" data-toggle="modal" data-target="#upload-header-logo" style="height:100px; width:100px"><i class="fas fa-images"></i></button>
            <?php }else{?>
            <div data-toggle="tooltip" data-placement="top" title="Klik untuk update gambar">
            <img class="proposal-logo" src="<?= $detailProp->logo_url?>" alt="" style="width:100px; margin-left:-3px;margin-top: -3px;" data-toggle="modal" data-target="#upload-header-logo">
            </div>
            <?php }?>
            <form class="upload-img-prop" action="<?= site_url('proposal/uploadLogoHeader')?>" method="post" enctype="multipart/form-data">
                <input type="file" name="userfile" id="upload-header-image" accept=".jpeg, .jpg, .png, .JPEG, .PNG">
                <input type="hidden" name="prop_id" id="proposal_id" value="<?= $detailProp->id?>">
                <button type="submit" class="btn btn-info pull-right" id="btn-upload-img-header">Upload File</button>
            </form>
        </div>
        <!-- end of logo proposal -->

        <textarea data-autoresize style="width:80%; line-height:1.5; text-transform:uppercase; font-size:2.5em; margin-left:-3px; border-radius:5px" class="form-control bold font-color" id="title-prop" rows="1"><?= $detailProp->judul ?></textarea>
        <hr id="line" style="border-width: 6px; border-color: #111111; width: 150px; margin-left: 0;"></hr><br><br>
        <p class="text-little">Dipersiapkan untuk <button class="btn btn-primary" id="change-client" data-toggle="tooltip" data-placement="top" title="Ganti client" style="border-radius: 50%; position: relative; left: 10px; bottom: 5px;"><i class="far fa-address-book" style="padding-top:3px"></i></button></p>
        <h5 class="bold"><?= $detailProp->nama_pic ?></h5>
        <p><?= $detailProp->perusahaan ?></p>
        &nbsp;
        <p class="text-little">Oleh</p>
        <p style="line-height:1.6">
            <span class="bold"><?= $detailProp->user_name ?></span><br>
            <?= $team->team_name ?><br>
            <span class="text-little"><?= $detailProp->user_email ?></span><br>
            <span class="text-small"><?= $this->monikalib->format_date_indonesia($detailProp->tgl_buat) ?></span>
        </p>
    </div>
</div>
<div id="fill" class="sec-sortable">
    <?php foreach($detailSec as $sec){

            //section text
            if($sec->sec_tipe == 0){ ?>
            <div class="row section justify-content-center" secID="<?= $sec->sec_id?>">
                <div class="col-sm-11">
                    <textarea data-autoresize class="form-control bold no-resize judul-section upper" rows="1" style="width:50%"><?= $sec->sec_judul;?></textarea>
                    <hr id="line" style="border-width: 2px; border-color: #111111; width: 50px; margin-left: 15px; margin-top:5px"></hr>
                    <textarea data-autoresize class="form-control no-resize isi-section" rows="4"><?= $sec->sec_isi;?></textarea>
                </div>
                <button class="btn btn-danger btn-sm delete-section text-small"><i class="fas fa-trash-alt"></i> Hapus Section</button>
            </div>
        <?php
            }
            //end of section text

            //section table
            elseif($sec->sec_tipe == 1){
                $CI =& get_instance();
                $CI->load->model('proposal_model');
                $detailItem = $CI->proposal_model->detailSecTable($sec->sec_id, $detailProp->id);
                ?>
            <div class="row section justify-content-center" secID="<?= $sec->sec_id?>">
                <div class="col-sm-11">
                    <textarea data-autoresize class="form-control bold no-resize judul-section upper" rows="1" style="width:50%"><?= $sec->sec_judul;?></textarea>
                    <hr id="line" style="border-width: 2px; border-color: #111111; width: 50px; margin-left: 15px; margin-top:5px"></hr>
                    <table class="section-table">
                        <tbody class="section-table-body">
                        <?php foreach($detailItem as $item){?>
                            <tr class="item-in-section" itemID="<?= $item->item_id?>">
                                <td>
                                    <textarea data-autoresize class="form-control no-resize item-input" rows="1" placeholder="item"><?= $item->item;?></textarea>
                                </td>
                                <td>
                                    <textarea data-autoresize class="form-control no-resize biaya-input" rows="1" placeholder="harga item"><?= $item->biaya;?></textarea>
                                </td>
                                <td>
                                    <button class="btn btn-outline-danger delete-item text-little"><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <button class="btn btn-outline-grey btn-sm add-item text-small"><i class="fas fa-plus"></i> Tambah row Item</button>
                </div>
                <button class="btn btn-danger btn-sm delete-section text-small"><i class="fas fa-trash-alt"></i> Hapus Section</button>
            </div>
        <?php
            }
            //end of section table

            //section image
            elseif($sec->sec_tipe == 2){
                $instance =& get_instance();
                $instance->load->model('proposal_model');
                $detailImage = $instance->proposal_model->detailSecImage($sec->sec_id, $detailProp->id);
                $jmlImg = count($detailImage);;
            ?>
                <div class="row section justify-content-center" secID="<?= $sec->sec_id?>">
                    <div class="col-sm-11">
                        <textarea data-autoresize class="form-control bold no-resize judul-section upper" rows="1" placeholder="Judul Section" style="width:50%"><?= $sec->sec_judul;?></textarea>
                        <hr id="line" style="border-width: 2px; border-color: #111111; width: 50px; margin-left: 15px; margin-top:5px"></hr>
                        <div class="row">
                        <?php foreach($detailImage as $image){?>
                                <div class="col-4 photo-in-section text-center">
                                    <img class="show-image" imgID="<?= $image->image_id?>" src="<?= $image->image_dir ?>" alt="" style="width:100%"> <br>
                                    <button class="btn btn-outline-danger btn-sm delete-image-section text-small" imgID="<?= $image->image_id ?>"><i class="fas fa-trash-alt"></i> Hapus Gambar</button>
                                </div>
                        <?php }?>

                        <?php for ($i = 3; $jmlImg < $i; $i--) {?>
                                <div class="col-4"><button class="photo-tiles"><i class="fas fa-images"></i></button></div>
                        <?php } ?>
                        </div>
                    </div>
                    <button class="btn btn-danger btn-sm delete-section text-small"><i class="fas fa-trash-alt"></i> Hapus Section</button>
                    <form class="upload-img-form" action="<?= site_url('proposal/addImageSection')?>" method="post" enctype="multipart/form-data">
                        <input type="file" name="userfile" id="image-upload" accept=".jpeg, .jpg, .png, .JPEG, .PNG">
                        <input type="hidden" name="prop_id" id="proposal_id" value="<?= $detailProp->id?>">
                        <input type="hidden" name="sec_id" id="sec_id" value="<?= $sec->sec_id?>">
                        <button type="submit" class="btn btn-info pull-right" id="btn-upload-img">Upload File</button>
                    </form>
                </div>
        <?php   }
                //end of section image
        }
        ?>
        <div class="text-center"><button class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#pilihan-section" aria-expanded="false" aria-controls="pilihan-section" id="btn-pilih-sec">tambah section</button></div>
    </div>
<br>
<div class="collapse" id="pilihan-section">
    <div class="card card-body text-center">
        <div>
            <button class="btn btn-primary btn-sm" id="add-section-text"><i class="fas fa-align-center"></i> Text</button>
            <button class="btn btn-primary btn-sm" id="add-section-images"><i class="far fa-images"></i> Image</button>
            <button class="btn btn-primary btn-sm" id="add-section-table"><i class="fas fa-table"></i> Table</button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-4 offset-md-7 text-center">
        <textarea data-autoresize class="form-control no-resize text-center" rows="1" placeholder="Signature" id="signature"><?=$detailProp->signature?></textarea>

        <p><?=$detailProp->user_name?></p>
    </div>
</div>
