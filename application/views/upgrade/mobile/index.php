<div id="content">
    <h3 class="text-center">Pilih Benefit</h3>
    <p class="text-center">untuk terus fokus dan memenangkan banyak klien</p>&nbsp;

    <div class="box text-center">
        <p class="box-title bold">personal</p>
        <div class="line-separator"></div>&nbsp;
        <ul class="no-point" style="margin-left:-45px">
            <li class="bold">Gratis</li>
            <li>1 User</li>
            <li>2 Dokumen</li>
            &nbsp;
            <li>
                <?php if ((count($this->monikalib->getUserPlan()) !=0 ) && ($this->monikalib->getUserPlan()->plan_id == 1)) { ?>
                <a href="#how-to-pay" class="btn btn-outline-primary btn-block btn-upgrade" data-pricing="0">Aktif</a>
                <?php } elseif (($this->monikalib->remainTrialDay() == false) && (empty($this->monikalib->getUserPlan()->plan_id))) { ?>
                <a href="#how-to-pay" class="btn btn-outline-primary btn-block btn-upgrade" data-pricing="0">Aktif</a>
                <?php } else { ?>
                <a href="#how-to-pay" class="btn btn-primary btn-block btn-upgrade" data-pricing="0">Downgrade</a>
                <?php } ?>
            </li>
        </ul>
    </div>

    <div class="box top-space text-center">
        <p class="box-title bold">startup</p>
        <div class="line-separator"></div>&nbsp;
        <ul class="no-point two-spacing" style="margin-left:-45px">
            <li class="bold">Rp 39 K / bln</li>
            <li>1 User</li>
            <li>3 Proposal</li>
            <li>3 Invoice</li>
            <li>PDF Download</li>
            <li class="bold"><a href="#stars">InDocument Feedback *</a></li>
            <li class="bold"><a href="#stars">Document Insight **</a></li>
            &nbsp;
            <li>
                <?php if ((!empty($this->monikalib->getUserPlan()->plan_id)) && ($this->monikalib->getUserPlan()->plan_id == 2)) { ?>
                <a href="#how-to-pay" class="btn btn-outline-primary btn-block btn-upgrade" data-pricing="1">Aktif</a>
                <?php } else { ?>
                <a href="#how-to-pay" class="btn btn-primary btn-block btn-upgrade" data-pricing="1">Pilih ini</a>
                <?php } ?>
            </li>
        </ul>
    </div>

    <div class="box top-space text-center">
        <p class="box-title bold">agency</p>
        <div class="line-separator"></div>&nbsp;
        <ul class="no-point two-spacing" style="margin-left:-45px">
            <li class="bold">Rp 159 K / bln</li>
            <li>Hingga 5 User</li>
            <li>Tanpa Batas Proposal</li>
            <li>Tanpa Batas Invoice</li>
            <li>PDF Download</li>
            <li class="bold"><a href="#stars">InDocument Feedback *</a></li>
            <li class="bold"><a href="#stars">Document Insight **</a></li>
            <li class="bold"><a href="#">Follow-up Reminder</a></li>&nbsp;
            <li>
                <?php if ((!empty($this->monikalib->getUserPlan()->plan_id)) && ($this->monikalib->getUserPlan()->plan_id == 3)) { ?>
                <a href="#how-to-pay" class="btn btn-outline-primary btn-block btn-upgrade" data-pricing="2">Aktif</a>
                <?php } elseif ($this->monikalib->remainTrialDay()!=false) { ?>
                <a href="#how-to-pay" class="btn btn-outline-primary btn-block btn-upgrade" data-pricing="2">Aktif</a>
                <?php } else { ?>
                <a href="#how-to-pay" class="btn btn-primary btn-block btn-upgrade" data-pricing="2">Saya lebih cocok ini</a>
                <?php } ?>
            </li>
        </ul>
    </div>

    <div class="box top-space text-center">
        <p class="box-title bold">enterprise</p>
        <div class="line-separator"></div>&nbsp;
        <ul class="no-point two-spacing" style="margin-left:-45px">
            <li class="bold">Price By Call</li>
            <li>Semua Fitur Agency</li>
            <li>Custom Request</li>
            <li>Account Manager</li>
            &nbsp;
            <li><button class="btn btn-primary btn-block btn-upgrade2" data-pricing="3">Kirim penawaran</button></li>
        </ul>
    </div>

    &nbsp;
    <div id="stars">
        <p class="text-little">*) InDocument Feedback adalah fitur di mana klien dapat memberikan feedback langsung tentang isi proposal yang sedang dibaca.<br>
        **) Ketahui kapan, berapa kali dan berapa lama klien membaca proposal Anda.</p>
    </div>

    <div class="box top-space two-spacing" id="how-to-pay">
        <p class="bold">Cara upgrade plan:</p>
        <ol>
            <li>Pilih plan sesuai kebutuhan Anda</li>
            <li>
                <p>Transfer sesuai pricing plan ke:</p>
                <p><img src="<?=base_url()?>assets/img/bca.png" style="width:100px"></p>
                <p><input value="Deny Salvana Ervan" readonly id="nama-rek" style="border:unset"> &nbsp;<button class="btn btn-outline-grey btn-small text-small" id="copy-nama">salin</button> <br> <input value="8365060807" readonly id="no-rek" style="border:unset"> &nbsp;<button class="btn btn-outline-grey btn-small text-small" id="copy-no">salin</button></p>
            </li>
            <li>Konfirmasi ke kami dengan melampirkan bukti transfer melalui <a class="bold" href="mailto:deny@monika.id?Subject=Konfirmasi%20pembayaran%20plan">email</a> atau <a href="#" class="bold" onclick="$crisp.push(['do', 'chat:open'])">chat dengen customer support kami</a></li>
            <li>Dalam estimasi maksimal 10 menit setelah konfirmasi, maka plan monika yang Anda pilih segera aktif.</li>
        </ol>
        &nbsp;
        <p class="grey text-little">*Kami sedang mengembangkan payment gateway untuk kemudahan Anda upgrade plan selanjutnya, dan akan kami update sesegera mungkin.</p>
    </div>
</div>

<!-- modal penawaran enterprise -->
<div class="modal fade form-space" id="modal_request_quotation" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Request Penawaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="label-control grey text-little">Subjek</label>
                    <input type="text" class="form-control" id="request_proposal_subject" value="Request Penawaran Monika">
                </div>
                <div class="form-group">
                    <label class="label-control grey text-little">Pesan</label>
                    <textarea rows="8" class="form-control" id="request_proposal_content">Dear Tim Monika,

Mohon kirimkan penawaran Enterprise Plan untuk perusahaan tempat saya bekerja.
Adapun beberapa pertanyaan kami yaitu:
- [TULIS PERTANYAAN]
- [TULIS PERTANYAAN]

Terima kasih
</textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-request-quotation">Kirim</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of modal penawaran enterprise -->
