<script src="<?= base_url() ?>node_modules/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $(document).ready(function(){
        $('.btn-upgrade').on('click',function(){
            swal({
                title: 'Upgrade plan sekarang',
                text: 'Jika ingin upgrade Plan, silahkan hubungi customer support kami',
                buttons: {
                    email: true,
                    chat: true
                }
            })
            .then((value) => {
                switch (value) {

                case "email":
                  window.location = "mailto:cs@monika.id";
                  break;

                case "chat":
                  $crisp.push(['do', 'chat:open']);
                  break;

                default:
                  break;
                }
            });
        });

        $('button[data-pricing="3"]').on('click',function(){
            $('#modal_request_quotation').modal('show');
        });

        $('#btn-request-quotation').on('click',function(){
            if($('#request_proposal_subject').val() == ''){
                swal("Oops","Subject belum diisi","error");
            }
            else if($('#request_proposal_content').val() == ''){
              swal("Oops","Request belum diisi","error");
            }
            else
            {
              $('#btn-request-quotation').attr('disabled',true).html('Mengirim...');
                  $.ajax({
                    type:"POST",
                    url: "<?php echo base_url()?>plan/send_request",
                    data: {
                        'email_message' : $('#request_proposal_content').val(),
                        'email_subject' : $('#request_proposal_subject').val()
                        },
                    success: function(data){
                      $('#btn-request-quotation').attr('disabled',false).html('<i class="fa fa-send" aria-hidden="true"></i> Kirim');
                      if(data == 1)
                      {
                        swal('Request berhasil dikirim','Tim Monika akan mengirimkan quotation penawaran berdasarkan kebutuhan Anda melalui email ini.','success');
                        $('#modal_request_quotation').modal('hide');
                      }
                      else
                      {
                        swal('Oops','Ada kesalahan','warning');
                      }

                    }, error:function(error){
                        swal('Oops','Ada kesalahan','warning');
                    }
                  });
            }
        });

        $('#copy-nama').on('click',function(){
            var copyText = document.getElementById("nama-rek");
            copyText.select();
            document.execCommand("Copy");
            swal('Nama rekening tersalin');
        });

        $('#copy-no').on('click',function(){
            var copyText = document.getElementById("no-rek");
            copyText.select();
            document.execCommand("Copy");
            swal('Nomor rekening tersalin');
        });

        $('.loader-primary').hide();
        $('#submit-promo').on('click', function(){
            var kode = $('#kode-promo').val();
            $('#btn-name').hide();
            $('.loader-primary').show();
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>plan/submit_code",
                data: {
                        'kode' : kode
                    },
                success: function(data){
                    if(data == 1){
                        swal('Selamat!','Selamat Kode Promo berhasil digunakan!','success')
                        .then((value) => {
                            location.reload();
                        });
                    }else{
                        swal('Oops','Kode promo tidak berlaku','warning')
                        .then((value) => {
                            location.reload();
                        });
                    }
                }, error:function(error){
                    swal('Oops','Ada kesalahan','warning')
                    .then((value) => {
                        location.reload();
                    });
                }
                });
        });
    });
</script>
