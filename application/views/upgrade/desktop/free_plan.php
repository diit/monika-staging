<div id="content">
    <div class="box text-center">
        <div class="row justify-content-center">
            <div class="col-md-5 mt-3 mb-3">
                <h3>Get Your Free Plan!</h3>
                <?php if ($status_promo == 0){ ?>
                <div class="form-group text-left mt-3">
                    <label for="">Kode Promo</label>
                    <input class="form-control" type="text" id="kode-promo">
                    <button class="btn btn-primary mt-4 w-100" id="submit-promo"><span id="btn-name">Submit Promo</span><div class="loader-primary"></div></button>
                </div>
                <?php }else{?>
                    <h4 class="mt-5">Anda Telah Submit Promo <br> Agency Plan akan aktif setelah masa trial habis</h4>
                    <p class="text-little grey mt-5">*Hubungi customer support kami jika Agency plan belum aktif setelah masa trial habis</p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>