
<!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->

      <section id="main-content">
        <section class="wrapper">
          <div class="container">

            <div class="row mt">
                <div class="col-lg-12">
                  <h3>Dapatkan Akses Prioritas </h3>
                  <p>Anda berkesempatan mendapat Akses Prioritas
                  selama periode Beta Testing ini. <br>Anda cukup membayar sebesar Rp 50.000,-
                  untuk mendapatkan:<br><br>
                  1) Bebas request fitur untuk kebutuhan Anda.<br>
                  2) Paid Plan Startup langsung untuk 2 bulan.<br>
                  3) <strong>Lifetime discount 15%</strong> untuk semua Paid Plan.<br>
                  </p>
                  <p>Hubungi kami melalui chat di kanan bawah atau email ke <a href="mailto:info@monika.id">info@monika.id</a></p>
                </div>






            </div>
          </div>

          </section>
              <!-- </form> -->
          <!-- wrapper -->
      </section><!-- /MAIN CONTENT -->
