
<!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->

      <section id="main-content">
        <section class="wrapper">
            <div class="row mt">
              <div class="container">
                <div class="col-xs-12 white-panel-abt" style="text-align: left;padding: 20px;">
                  <h3>Status Akun</h3>
                  <div class="row">
                    <div class="col-md-8">
                      <div style="margin-top: 20px;" class="table-responsive">
                        <table class="table table-responsive">
                          <tbody>
                              <tr>
                                <td>Plan</td>
                                <td>:</td>
                                <td><?php echo ($plan == NULL || $plan->status > 1) ? 'FREE' : $plan->plan.' <a href="'.site_url('plan/payment/'.$plan->invoice).'"><i class="fa fa-long-arrow-right"></i>Lihat invoice</a>'; ?></td>
                              </tr>
                              <tr>
                                <td>Status</td>
                                <td>:</td>
                                <td>
                                  <?php
                                    if($plan == NULL || $plan->status > 1)
                                    {
                                      echo 'Aktif ';
                                    }
                                    else
                                    {
                                      if($plan->status == 0)
                                      {
                                        if($plan->transfer_receipt == NULL)
                                        {
                                          echo 'Menunggu Pembayaran <a href="'.site_url('plan/payment/'.$plan->invoice).'"><i class="fa fa-long-arrow-right"></i>Invoice & Metode Pembayaran</a>';
                                        }
                                        else
                                        {
                                          echo 'Dalam verifikasi oleh admin';
                                        }
                                      }
                                      else if($plan->status == 1)
                                      {
                                        echo 'Aktif (sejak '.$this->monikalib->format_date_indonesia($plan->active_time).')';
                                      }
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <td>Periode</td>
                                <td>:</td>
                                <td>
                                  <?php
                                    if($plan == NULL || $plan->status > 1)
                                    {
                                      echo ' - ';
                                    }
                                    else
                                    {
                                      if($plan->status == 0)
                                      {
                                        echo ' - ';
                                      }
                                      else if($plan->status == 1)
                                      {
                                        $start_date = $this->monikalib->format_date_indonesia($plan->active_time);
                                        if($plan->plan_id == 1 || $plan->plan_id == 3)
                                        {
                                          $interval = 30;
                                        }
                                        else if($plan->plan_id == 2 || $plan->plan_id == 4)
                                        {
                                          $interval = 360;
                                        }
                                        $end_date = $this->monikalib->format_date_add_indonesia($plan->active_time,$interval);
                                        echo $start_date.' s/d '.$end_date;
                                      }
                                    }
                                  ?>
                                </td>
                              </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8">
                      <p style="margin: 0 0 12px;font-size: 14px; font-weight: 800;">Riwayat</p>
                      <div style="margin-top: 20px;" class="table-responsive">
                        <table class="table table-hover">
                          <thead>
                            <th>Plan</th>
                            <th>Periode</th>
                            <th>Status</th>
                          </thead>
                          <tbody>
                          <?php if($history && count($history) > 0) { ?>
                            <?php foreach ($history as $hs) { ?>
                              <tr>
                                <td><?php echo $hs->plan; ?></td>
                                <td>
                                  <?php
                                    if($plan->status == 0)
                                      {
                                        echo ' - ';
                                      }
                                      else if($plan->status == 1)
                                      {
                                        $start_date = $this->monikalib->format_date_indonesia($plan->active_time);
                                        if($plan->plan_id == 1 || $plan->plan_id == 3)
                                        {
                                          $interval = 30;
                                        }
                                        else if($plan->plan_id == 2 || $plan->plan_id == 4)
                                        {
                                          $interval = 360;
                                        }
                                        $end_date = $this->monikalib->format_date_add_indonesia($plan->active_time,$interval);
                                        echo $start_date.' s/d '.$end_date;
                                      }
                                  ?>
                                </td>
                                <td>
                                  <?php
                                    if($hs->status > 1)
                                    {
                                      echo 'Aktif';
                                    }
                                    else
                                    {
                                      if($hs->status == 0)
                                      {
                                        if($plan->transfer_receipt == NULL)
                                        {
                                          echo 'Menunggu Pembayaran';
                                        }
                                        else
                                        {
                                          echo 'Dalam verifikasi oleh admin';
                                        }
                                      }
                                      else if($hs->status == 1)
                                      {
                                        echo 'Aktif';
                                      }
                                    }
                                  ?>
                                </td>
                              </tr>
                            <?php } ?>
                          <?php }else{ ?>
                            <tr>
                              <td colspan="2"><p style="text-align: center;">Belum ada data</p></td>
                            </tr>
                          <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </section><!-- wrapper -->
      </section><!-- /MAIN CONTENT -->
