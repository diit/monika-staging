<section id="main-content">
    <section class="wrapper">
        <div class="row mt">
          <div class="container">
            <div class="col-xs-12 white-panel-abt" style="text-align: left;padding: 20px;">
              <h3>Tagihan #<?php echo $invoice_detail->invoice; ?> </h3>
              <div class="row">
                <div class="col-md-8">
                  <div style="margin-top: 20px;" class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>Kepada</th>
                            <th>Invoice No.</th>
                            <th>Tanggal</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php if($invoice_detail) { ?>
                            <tr>
                              <td><?php echo $invoice_detail->team_name; ?></td>
                              <td><?php echo $invoice_detail->invoice; ?></td>
                              <td><?php echo $invoice_detail->upgrade_time; ?></td>
                              <td>
                                <?php
                                  if($invoice_detail->status == 0)
                                  {
                                    if($invoice_detail->transfer_receipt == NULL)
                                    {
                                      echo 'Menunggu Pembayaran';
                                    }
                                    else
                                    {
                                      echo 'Dalam verifikasi oleh admin';
                                    }
                                  }
                                  else
                                  {
                                    echo 'Lunas';
                                  }
                                ?>

                              </td>
                            </tr>
                        <?php }else{ ?>
                          <tr>
                            <td colspan="4"><p style="text-align: center;">Belum ada data</p></td>
                          </tr>
                        <?php } ?>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <p style="margin: 0 0 12px;font-size: 14px; font-weight: 800;">Detail Pembayaran</p>
                  <div style="margin-top: 20px;" class="table-responsive">
                    <table class="table table-hover">
                      <tbody>
                      <?php if($invoice_detail) { ?>
                          <tr>
                            <td>Plan : <?php echo $invoice_detail->plan; ?></td>
                            <td>Rp <?php echo number_format($invoice_detail->price,0,',','.'); ?></td>
                          </tr>
                          <?php if($invoice_detail->disc != NULL){ ?>
                          <tr>
                            <td>Promo (Kode : <?php echo $invoice_detail->promo_code; ?> | Disc. <?php echo $invoice_detail->disc; ?>%)</td>
                            <td>Rp <?php echo number_format($invoice_detail->price * $invoice_detail->disc/100,0,',','.'); ?></td>
                          </tr>
                          <?php } ?>
                          <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>Rp <?php echo number_format($invoice_detail->price - ($invoice_detail->price * $invoice_detail->disc/100),0,',','.'); ?></strong></td>
                          </tr>
                      <?php }else{ ?>
                        <tr>
                          <td colspan="2"><p style="text-align: center;">Belum ada data</p></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <?php if($invoice_detail && ($invoice_detail->status == 0 && $invoice_detail->transfer_receipt == NULL)){ ?>
                <div class="row">
                  <div class="col-md-8">
                    <p style="margin: 0 0 12px;font-size: 14px; font-weight: 800;">Metode Pembayaran</p>
                    <div class="col-md-7">
                          <p>Transfer Bank</p>
                          <ul style="padding-left:10px;">
                            <li><img src="<?php echo site_url('assets/images/logo-bca.png') ?>" width="40"><span style="margin-left:10px;font-size: 13px;">No. Rekening : 8410096073 a/n Aditya Dwi Putra</span></li>
                            <li><img src="<?php echo site_url('assets/images/logo-mandiri.png') ?>" width="40"><span style="margin-left:10px;font-size: 13px;">No. Rekening : 1360016081140 a/n Aditya Dwi Putra</span></li>
                            <li><img src="<?php echo site_url('assets/images/logo-bni.png') ?>" width="40"><span style="margin-left:10px;font-size: 13px;">No. Rekening : 0295460244 a/n Aditya Dwi Putra</span></li>
                          </ul>
                          <div style="text-align: center;">
                            <?php echo form_open_multipart('plan/upload_bank_transfer_receipt',array('id' => 'form_upload_bank_transfer_receipt'));?>
                                            <div class="input-group" style="width:100%;margin: auto;">
                                                <span class="input-group-btn">
                                                    <span class="btn btn-default btn-file">
                                                      <input type="hidden" name="invoice" value="<?php echo $invoice_detail->invoice; ?>">
                                                      <i class="fa fa-image"></i> <input type="file" name="file" id="imginvoice">
                                                    </span>
                                                </span>
                                                <input type="text" class="form-control" readonly>
                                                <span class="input-group-btn">
                                                  <button class="btn btn-success btn-save-bank-transfer-receipt" type="button">Upload Bukti Transfer</button>
                                                </span>
                                            </div>
                                            </form>
                            <!--<button type="button" class="btn btn-success upload-transfer">Upload Bukti Transfer</button>-->
                          </div>
                        </div>
                        <div class="col-md-5">
                          <p>Paypal & Credit Card</p>
                          <script src="https://www.paypalobjects.com/api/checkout.js"></script>
                          <div id="paypal-button-container"></div>
                          <script>
                          // Render the PayPal button

                          paypal.Button.render({

                              // Set your environment

                              env: 'production', // sandbox | production

                              // Specify the style of the button

                              style: {
                                  label: 'pay',
                                  size:  'small', // small | medium | large | responsive
                                  shape: 'rect',   // pill | rect
                                  color: 'gold'   // gold | blue | silver | black
                              },

                              // PayPal Client IDs - replace with your own
                              // Create a PayPal app: https://developer.paypal.com/developer/applications/create

                              client: {
                                  sandbox:    'ASKbLJ5oR0Ksa1NKHMpW-ZkYKlLj7ImpG-yAORXIdbcqlMUmQAuXAit4kJxvu3ySURbs5jnhpX87TzqF',
                                  production: 'AVo7Zw-rjhtGBSmG4xe2Xd9eCdWOBAqU2fME7TkUFMrA4IGQhu61UzdbqoTFmvuFSH7cm3ys5oZ9QTkp'
                              },

                              // Wait for the PayPal button to be clicked

                              payment: function(data, actions) {
                                  return actions.payment.create({
                                      payment: {
                                          transactions: [
                                              {
                                                  amount: { total: '<?php echo round($invoice_detail->price - ($invoice_detail->price * $invoice_detail->disc/100)/13000); ?>', currency: 'USD' }
                                              }
                                          ]
                                      }
                                  });
                              },

                              // Wait for the payment to be authorized by the customer

                              onAuthorize: function(data, actions) {
                                  return actions.payment.execute().then(function() {
                                      //window.alert('Payment Complete!');
                                      $.ajax({
                                        type: "POST",
                                        url: '<?php echo site_url('plan/paid'); ?>',
                                        data: {
                                            'invoice' : <?php echo $invoice_detail->invoice; ?>,
                                            'payment' : 2 //paypal
                                          },
                                        success: function(data)
                                        {
                                          if(data == 1)
                                          {
                                            fbq('track', 'Upgrade', {'value':'<?php echo $invoice_detail->price - ($invoice_detail->price * $invoice_detail->disc/100); ?>','currency':'IDR'});
                                            swal("Berhasil", "Pembayaran telah kami terima!", "success")
                                            .then((value) => {
                                              //location.reload();
                                              window.location = '<?php echo site_url('plan/status'); ?>';
                                            });
                                          }
                                          else
                                          {
                                            swal("Oops","Pembayaran gagal. Silahkan coba kembali atau hubungi Customer Service.","error");
                                          }
                                        },
                                        error: function (jqXHR, textStatus, errorThrown)
                                        {
                                          swal("Oops","Pembayaran gagal. Silahkan coba kembali atau hubungi Customer Service.","error");
                                        }
                                    });
                                  });
                              }

                          }, '#paypal-button-container');

                      </script>
                        </div>
                  </div>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
    </section><!-- wrapper -->
</section><!-- /MAIN CONTENT -->


<script type="text/javascript">
    $(document).ready(function()
    {
        $(document).on('click','.btn-save-bank-transfer-receipt',function(){
            if($('#imginvoice').val() == '')
            {
              swal("Oops","Silahkan pilih gambar bukti transfer terlebih dahulu","error");
            }
            else
            {
              $('#form_upload_bank_transfer_receipt').submit();
            }
          });
    });
</script>
