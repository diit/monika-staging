<!--main content start-->
<style media="screen">
#main-content .col-lg-12 ul .btn-primary{
  width: 100%;
}
</style>
<section id="main-content">
  <section class="wrapper">
    <div class="row mt">
        <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <!-- BASIC PROGRESS BARS -->
            <div style="min-height:600px;">
                <div class="col-lg-12">
                        <h3 class="pull-center" style="text-align:center;">Pilih Benefit</h3>
          <h4 class="pull-center" style="text-align:center;margin-bottom:50px;">untuk terus fokus dan memenangkan banyak klien</h4>
                    </div>
        <div class="row pointbtn2 pricing">
                    <div class="col-sm-3">
            <div class="panel panel-default text-center">
              <div class="panel-heading">
                <strong>PERSONAL</strong>
              </div>
              <div class="panel-body">
                <p class="panel-title price">GRATIS <span class="price-cents">&nbsp;</span></p>
              </div>
              <ul class="list-group">
                <li class="list-group-item">1 User</li>
                <li class="list-group-item">2 Dokumen</li>
                <li class="list-group-item"> - </li>
                <li class="list-group-item"> - </li>
                <li class="list-group-item"> - </li>
                <li class="list-group-item"> - </li>
                <li class="list-group-item"> - </li>
                <li class="list-group-item"><label class="btn">Aktif</label></li>
              </ul>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="panel panel-default text-center">
              <div class="panel-heading">
                <strong>STARTUP</strong>
              </div>
              <div class="panel-body">
                <p class="panel-title price">Rp 39 rb <span class="price-cents">/ bln</span></p>
              </div>
              <ul class="list-group">
                <li class="list-group-item">1 User</li>
                <li class="list-group-item">3 Proposal</li>
                <li class="list-group-item">3 Invoice</li>
                <li class="list-group-item">PDF Download</li>
                <li class="list-group-item"><span style="color:#46bfb2;">InDocument Feedback *</span></li>
                <li class="list-group-item"><span style="color:#46bfb2;font-weight: 800;">Document Insight **</span></li>
                <li class="list-group-item"> - </li>
                <li class="list-group-item"><button data-pricing="1" class="btn btn-primary click-upgrade">Pilih ini</button></li>
              </ul>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="panel panel-default text-center">
              <div class="panel-heading">
                <strong>AGENCY</strong>
              </div>
              <div class="panel-body">
                <p class="panel-title price">Rp 159 rb <span class="price-cents">/ bln</span></p>
              </div>
              <ul class="list-group">
                <li class="list-group-item"><strong>Hingga 5 User</strong></li>
                <li class="list-group-item">Tanpa Batas Proposal</li>
                <li class="list-group-item">Tanpa Batas Invoice</li>
                <li class="list-group-item">PDF Download</li>
                <li class="list-group-item"><span style="color:#46bfb2;">InDocument Feedback *</span></li>
                <li class="list-group-item"><span style="color:#46bfb2;">Document Insight **</span></li>
                <li class="list-group-item"><span style="color:#46bfb2;font-weight: 800;">Follow-up Reminder</span></li>
                <li class="list-group-item"><button data-pricing="2" class="btn btn-primary click-upgrade">Saya lebih cocok yang ini</button></li>
              </ul>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="panel panel-default text-center">
              <div class="panel-heading">
                <strong>ENTERPRISE</strong>
              </div>
              <div class="panel-body">
                <p class="panel-title price">Price By Call <span class="price-cents">&nbsp;</span></p>
              </div>
              <ul class="list-group">
                <li class="list-group-item" style="border-bottom-color:#fff;">&nbsp;</li>
                <li class="list-group-item" style="border-color:#fff;"></li>
                <li class="list-group-item" style="border-color:#fff;"></li>
                <li class="list-group-item" style="border-color:#fff;">Semua fitur Agency</li>
                <li class="list-group-item" style="border-color:#fff;">Custom Request</li>
                <li class="list-group-item" style="border-color:#fff;">Account Manager</li>
                <li class="list-group-item" style="border-color:#fff;"></li>
                <li class="list-group-item" style="border-color:#fff;"></li>
                <li class="list-group-item" style="border-color:#fff;">&nbsp;</li>
                <li class="list-group-item"><button data-pricing="3" class="btn btn-primary click-quotation">Kirimkan saya penawaran</button></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row pointbtn2">
          <p style="padding-left:15px;font-size:12px;">*) InDocument Feedback adalah fitur di mana klien dapat memberikan feedback langsung tentang isi proposal yang sedang dibaca.</p>
          <p style="padding-left:15px;font-size:12px;">**) Ketahui kapan, berapa kali dan berapa lama klien membaca proposal Anda.</p>
        </div>
        <div class="row pointbtn3">
          <div class="col-sm-3">
            <div class="panel panel-default text-center">
              <div class="panel-heading">
                <strong>PERSONAL</strong>
              </div>
              <div class="panel-body">
                <p class="panel-title price">GRATIS</p>
              </div>
              <ul class="list-group">
                <li class="list-group-item">1 User</li>
                <li class="list-group-item">2 Dokumen per bulan</li>
                <li class="list-group-item"><label class="btn">Aktif</label></li>
              </ul>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="panel panel-default text-center">
              <div class="panel-heading">
                <strong>STARTUP</strong>
              </div>
              <div class="panel-body">
                <p class="panel-title price">Rp 39 rb <span class="price-cents">/ bln</span></p>
              </div>
              <ul class="list-group">
                <li class="list-group-item">1 User</li>
                <li class="list-group-item">3 Proposal</li>
                <li class="list-group-item">3 Invoice</li>
                <li class="list-group-item">PDF Download</li>
                <li class="list-group-item"><span style="color:#46bfb2;">InDocument Feedback *</span></li>
                <li class="list-group-item"><span style="color:#46bfb2;font-weight: 800;">Document Insight **</span></li>
                <li class="list-group-item"><button data-pricing="1" class="btn btn-primary click-upgrade">Pilih ini</button></li>
              </ul>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="panel panel-default text-center">
              <div class="panel-heading">
                <strong>AGENCY</strong>
              </div>
              <div class="panel-body">
                <p class="panel-title price">Rp 159 rb <span class="price-cents">/ bln</span></p>
              </div>
              <ul class="list-group">
                <li class="list-group-item"><strong>Hingga 5 User</strong></li>
                <li class="list-group-item">Tanpa Batas Proposal</li>
                <li class="list-group-item">Tanpa Batas Invoice</li>
                <li class="list-group-item">PDF Download</li>
                <li class="list-group-item"><span style="color:#46bfb2;">InDocument Feedback *</span></li>
                <li class="list-group-item"><span style="color:#46bfb2;">Document Insight **</span></li>
                <li class="list-group-item"><span style="color:#46bfb2;font-weight: 800;">Follow-up Reminder</span></li>
                <li class="list-group-item"><button data-pricing="2" class="btn btn-primary click-upgrade">Saya lebih cocok yang ini</button></li>
              </ul>
            </div>
            <p style="padding-left:15px;font-size:12px;">*) InDocument Feedback adalah fitur di mana klien dapat memberikan feedback langsung tentang isi proposal yang sedang dibaca.</p>
            <p style="padding-left:15px;font-size:12px;">**) Ketahui kapan, berapa kali dan berapa lama klien membaca proposal Anda.</p>
          </div>

          <div class="col-sm-3">
            <div class="panel panel-default text-center">
              <div class="panel-heading">
                <strong>ENTERPRISE</strong>
              </div>
              <div class="panel-body">
                <p class="panel-title price">Price By Call</p>
              </div>
              <ul class="list-group">
                <li class="list-group-item">Semua fitur Agency</li>
                <li class="list-group-item">Custom Request</li>
                <li class="list-group-item">Account Manager</li>
                <li class="list-group-item"><button data-pricing="3" class="btn btn-primary click-quotation">Kirimkan saya penawaran</button></li>
              </ul>
            </div>
          </div>
        </div>
    </div>

            </div><!--/showback -->



        </div><!-- /col-lg-12 -->
        </div>

  </section><!--/wrapper -->
</section><!-- /MAIN CONTENT -->

<div class="modal" id="modal_onprocess_upgrade" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
<div class="modal-dialog">
 <div class="modal-content">
     <div class="modal-header">
       <h4 class="modal-title text-center" id="" style="font-weight:bold">Upgrade Dalam Proses</h4>
     </div>
     <div class="modal-body pilihCat">
       <div class="row">
         <div class="col-md-12 col-xs-12">
           <div style="text-align: center;">
             <h4>Silahkan lakukan pembayaran sesuai metode yang Anda pilih</h4>
             <p style="padding:10px 50px;font-size: 14px;">Anda sudah memilih plan sebelumnya. Apabila telah melakukan pembayaran silahkan upload bukti pembayaran di sini.</p>
           </div>
         </div>
       </div>
     </div>
     <div class="modal-footer">
      <a href="<?php echo site_url('plan/payment/'.$this->monikalib->getUserPlan()->invoice); ?>" class="btn btn-success">Tagihan & Cara Pembayaran</a>
     </div>
 </div>
</div>
</div>

      <div class="modal" id="modal_upgrade" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
       <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <div class="modal-header">
               <h4 class="modal-title text-center" id="" style="font-weight:bold">Upgrade Plan</h4>
             </div>
             <div class="modal-body pilihCat">
               <div class="row">
                 <div class="col-md-12 col-xs-12">
                   <div style="text-align: center;">
                     <h3 id="plan_title">Plan Startup sesuai untuk Anda yang baru memulai usaha</h3>
                     <div class="col-md-5" style="text-align: left;padding: 20px 0 20px 60px;">
                        <p id="feat1" style="font-size:16px;line-height:20px;"><i style="color:#46bfb2;" class="fa fa-check" aria-hidden="true"></i> Untuk 1 pengguna</p>
                        <p style="font-size:16px;line-height:20px;"><i style="color:#46bfb2;" class="fa fa-check" aria-hidden="true"></i> Tanpa batas dokumen</p>
                        <p style="font-size:16px;line-height:20px;"><i style="color:#46bfb2;" class="fa fa-check" aria-hidden="true"></i> Download PDF</p>
                        <p style="font-size:16px;line-height:20px;"><i style="color:#46bfb2;" class="fa fa-check" aria-hidden="true"></i> Nama Usaha Anda di proposal</p>
                        <p id="feat2" style="font-size:16px;line-height:20px;"><i style="color:red;" class="fa fa-ban" aria-hidden="true"></i> In-proposal feedback</p>
                        <p id="feat3" style="font-size:16px;line-height:20px;"><i style="color:red;" class="fa fa-ban" aria-hidden="true"></i> Proposal Insight Analytics</p>
                        <p id="feat4" style="font-size:16px;line-height:20px;"><i style="color:red;" class="fa fa-ban" aria-hidden="true"></i> Pilihan Template</p>
                      </div>
                      <div class="col-md-6" style="text-align: left;padding: 10px 30px;">
                        <div class="row">
                          <p style="font-size: 17px;font-weight: 800;">Pilih Periode</p>
                          <div class="radio">
                            <label style="font-size: 15px;"><input id="monthly" type="radio" name="plan" value="1"><span id="tprice_monthly"><strong>Bulanan</strong> - Rp 39.000 / bulan</span></label>
                          </div>
                          <hr>
                          <div class="radio">
                            <label style="font-size: 15px;"><input id="yearly" type="radio" name="plan" value="2"><span id="tprice_yearly_1"><strong>Tahunan</strong> - Rp 29.000 / bulan</span><br><span id="tprice_yearly_2" style="font-size: 12px;">Bayar Rp 300.000 per tahun</span></label>
                          </div>
                        </div>
                        <div class="row">
                          <p style="font-size: 17px;font-weight: 800;">Metode Pembayaran</p>
                          <div class="col-md-12 col-xs-12">
                             <div class="alert alert-success" role="alert">
                              Sementara kami hanya menerima pembayaran melalui Transfer Bank dan Paypal.
                             </div>
                           </div>
                          <div class="col-md-7 col-xs-6">
                            <p>Transfer Bank</p>
                            <p>
                                <span style="margin-right:3px;"><img width="40" src="<?php echo site_url('assets/images/logo-bca.png') ?>"></span>
                                <span style="margin-right:3px;"><img width="40" src="<?php echo site_url('assets/images/logo-mandiri.png') ?>"></span>
                                <span style="margin-right:3px;"><img width="40" src="<?php echo site_url('assets/images/logo-bni.png') ?>"></span>
                            </p>
                          </div>
                          <div class="col-md-5 col-xs-6">
                            <p>Paypal & Credit Card</p>
                            <p><i style="font-size: 28px;" class="fa fa-cc-paypal"></i></p>
                          </div>
                        </div>
                        <div class="row">
                          <p style="font-size: 17px;font-weight: 800;">Redeem Voucher</p>
                          <div class="col-md-12 col-xs-12">
                            <div class="input-group">
                              <input class="form-control" placeholder="Masukan kode voucher bila ada" type="text" name="promo" id="promo">
                              <span class="input-group-btn">
                                <button class="btn btn-default" id="btn-check-promo" type="button">Cek Promo</button>
                              </span>
                            </div>
                            <p id="notif_promo" style="font-size: 11px;"></p>
                          </div>
                        </div>
                      </div>
                   </div>
                 </div>
                <!-- comment sampai beta test selesai -->
                <!--
                   <div class="col-md-6 col-sm-6 col-xs-6">
                     <div class="concat  catid="1">
                       <label>
                         <input type="radio" name="plan" id="plan1" value="1" >
                         <i class="fa fa-check" aria-hidden="true"></i> STARTUP
                       </label>
                        <i style="position: absolute;right: 23px;top: 35%;display: none;" id="check_1" class="fa fa-check check_active"></i>
                     </div>
                   </div>
                   <div class="col-md-6 col-sm-6 col-xs-6">
                     <div class="concat  catid="2">
                       <label>
                         <input type="radio" name="plan" id="plan2" value="2" >
                         <i class="fa fa-check" aria-hidden="true"></i> AGENCY
                       </label>
                        <i style="position: absolute;right: 23px;top: 35%;display: none;" id="check_2" class="fa fa-check check_active"></i>
                     </div>
                   </div>
                  <div class="col-md-7 col-xs-6">
                    <p>Transfer Bank</p>
                    <p>
                        <span style="margin-right:3px;"><img width="40" src="<?php //echo site_url('assets/images/logo-bca.png') ?>"></span>
                        <span style="margin-right:3px;"><img width="40" src="<?php //echo site_url('assets/images/logo-mandiri.png') ?>"></span>
                        <span style="margin-right:3px;"><img width="40" src="<?php //echo site_url('assets/images/logo-bni.png') ?>"></span>
                    </p>
                  </div>
                  <div class="col-md-5 col-xs-6">
                    <p>Paypal & Credit Card</p>
                    <p><i style="font-size: 28px;" class="fa fa-cc-paypal"></i></p>
                  </div>
                </div>
                <div class="row">
                  <p style="font-size: 17px;font-weight: 800;">Redeem Voucher</p>
                  <div class="col-md-12 col-xs-12">
                    <div class="input-group">
                      <input class="form-control" placeholder="Masukan kode voucher bila ada" type="text" name="promo" id="promo">
                      <span class="input-group-btn">
                        <button class="btn btn-default" id="btn-check-promo" type="button">Cek Promo</button>
                      </span>
                    </div>
                    <p id="notif_promo" style="font-size: 11px;"></p>
                  </div>
                </div>
              </div>
           </div>
         </div>
        <!-- comment sampai beta test selesai -->
        <!--
           <div class="col-md-6 col-sm-6 col-xs-6">
             <div class="concat  catid="1">
               <label>
                 <input type="radio" name="plan" id="plan1" value="1" >
                 <i class="fa fa-check" aria-hidden="true"></i> STARTUP
               </label>
                <i style="position: absolute;right: 23px;top: 35%;display: none;" id="check_1" class="fa fa-check check_active"></i>
             </div>
           </div>
           <div class="col-md-6 col-sm-6 col-xs-6">
             <div class="concat  catid="2">
               <label>
                 <input type="radio" name="plan" id="plan2" value="2" >
                 <i class="fa fa-check" aria-hidden="true"></i> AGENCY
               </label>
                <i style="position: absolute;right: 23px;top: 35%;display: none;" id="check_2" class="fa fa-check check_active"></i>
             </div>
           </div>
         -->
       </div>
     </div>
     <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      <button type="button" class="btn btn-success btn-upgrade">Lanjutkan</button>
     </div>
 </div>
</div>
</div>

<div class="modal" id="modal_request_quotation" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog modal-md">
         <div class="modal-content">
             <div class="modal-header">
               <h4 class="modal-title text-center" id="" style="font-weight:bold">Request Penawaran</h4>
             </div>
             <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                        <label class="label-control">Subjek</label>
                        <input type="text" class="form-control" id="request_proposal_subject" value="Request Penawaran Monika">
                      </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                        <label class="label-control">Pesan</label>
                        <textarea rows="8" class="form-control" id="request_proposal_content">Dear Tim Monika,

Mohon kirimkan penawaran Enterprise Plan untuk perusahaan tempat saya bekerja.
Adapun beberapa pertanyaan kami yaitu:
- [TULIS PERTANYAAN]
- [TULIS PERTANYAAN]

Terima kasih
</textarea>
                      </div>
                  </div>
                </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-info" id="btn-request-quotation"><i class="fa fa-send" aria-hidden="true"></i>
                      Kirim
                    </button>
                 </div>
          </div>
      </div>
    </div>
</div>

<script type="application/javascript">
  $(document).ready(function () {
    $('.click-upgrade').on('click',function(){
        <?php if($this->monikalib->getUserPlan() == NULL){ ?>
          var price = $(this).attr('data-pricing');
          if(price == 1)
          {
            $('#plan_title').text('Plan Startup sesuai untuk Anda yang baru memulai usaha');
            $('#feat1').html('<i style="color:#46bfb2;" class="fa fa-check" aria-hidden="true"></i> Untuk 1 pengguna');
            $('#feat2').html('<i style="color:red;" class="fa fa-ban" aria-hidden="true"></i> In-proposal feedback');
            $('#feat3').html('<i style="color:red;" class="fa fa-ban" aria-hidden="true"></i> Proposal Insight Analytics');
            $('#feat4').html('<i style="color:red;" class="fa fa-ban" aria-hidden="true"></i> Pilihan Template');
            $('#monthly').val('1');
            $('#yearly').val('2');
            $('#tprice_monthly').html('<strong>Bulanan</strong> - Rp 39.000 / bulan');
            $('#tprice_yearly_1').html('<strong>Tahunan</strong> - Rp 29.000 / bulan');
            $('#tprice_yearly_2').text('Bayar Rp 300.000 per tahun');
          }
          else if(price == 2)
          {
            $('#plan_title').text('Plan Agency sesuai untuk Anda yang telah memiliki tim sales & marketing');
            $('#feat1').html('<i style="color:#46bfb2;" class="fa fa-check" aria-hidden="true"></i> Hingga 5 pengguna');
            $('#feat2').html('<i style="color:#46bfb2;" class="fa fa-check" aria-hidden="true"></i> In-proposal feedback');
            $('#feat3').html('<i style="color:#46bfb2;" class="fa fa-check" aria-hidden="true"></i> Proposal Insight Analytics');
            $('#feat4').html('<i style="color:#46bfb2;" class="fa fa-check" aria-hidden="true"></i> Pilihan Template');
            $('#monthly').val('3');
            $('#yearly').val('4');
            $('#tprice_monthly').html('<strong>Bulanan</strong> - Rp 159.000 / bulan');
            $('#tprice_yearly_1').html('<strong>Tahunan</strong> - Rp 139.000 / bulan <strong>(Hemat 25%)</strong>');
            $('#tprice_yearly_2').html('Bayar Rp 1.400.000 per tahun');

          }
          $('#modal_upgrade').modal('show');
        <?php }else{ ?>
          <?php if($this->monikalib->getUserPlan()->status == 0){ ?>
            //$('#modal_onprocess_upgrade').modal('show');
            swal("Upgrade Dalam Proses","Anda sudah memilih plan sebelumnya. Silahkan lakukan pembayaran sesuai metode yang Anda pilih","info",{button: "Aww yiss!"})
            .then((value) => {
              window.location = '<?php echo site_url('plan/payment/'.$this->monikalib->getUserPlan()->invoice); ?>';
            });
          <?php }else if($this->monikalib->getUserPlan()->status == 1){ ?>
            //$('#modal_plan_active').modal('show');
            //swal("Plan Masih Aktif","Untuk mengubah plan, silahkan hubungin customer service.","info");
            swal("Plan Masih Aktif","Untuk mengubah plan, silahkan hubungin customer service.","info", {
              buttons: {
                email:true ,
                chat: true,
              },
            })
            .then((value) => {
              switch (value) {

                case "email":
                  window.location = "mailto:cs@monika.id";
                  break;

                case "chat":
                  Tawk_API.toggle();
                  break;

                default:
                  break;
              }
            });
          <?php } ?>
        <?php } ?>
    });

    var cekpro = 0;
    $('#btn-check-promo').on('click',function(){
      var promo = $('#promo').val();
      if(promo == '')
      {
        swal("Oops","Masukan kode voucer","alert");
      }
      else
      {
        $('#btn-check-promo').attr('disabled',true).text('Checking...');
        $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>plan/check_promo",
                        data: {
                            'promo' : promo
                            },
                        success: function(data){
                          if(data == 0)
                          {
                            $('#promo').val('');
                            $('#btn-check-promo').attr('disabled',false).text('Cek Promo');
                            $('#notif_promo').text('Tidak ada promo').css('color','red');
                          }
                          else
                          {
                            cekpro = 1;
                            $('#btn-check-promo').attr('disabled',false).text('Cek Promo');
                            $('#notif_promo').text('Promo ditemukan : Disc. '+ data + '%').css('color','green');
                          }

                        }, error:function(error){
                          swal("Oops","Terjadi kesalahan, silahkan hubungi customer service.","warning");
                          $('.btn-upgrade').attr('disabled',false).text('Lanjutkan');
                          $('#promo').val('');
                          $('#btn-check-promo').attr('disabled',false).text('Cek Promo');
                        }
                      });
      }
    });

    $('.btn-upgrade').on('click',function(){
      var plan = $('input[name="plan"]:checked').val();
      if(plan == undefined)
      {
        swal("Oops","Silahkan pilih periode","info");
      }
      else
      {
        if(cekpro == 0 && $('#promo').val() != '')
        {
          swal("Oops","Silahkan Cek Promo terlebih dahulu","info");
        }
        else
        {
          $('.btn-upgrade').attr('disabled',true).text('Memproses...');
          $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>plan/upgrade",
                        data: {
                            'plan' : plan,
                            'promo' : $('#promo').val()
                            },
                        success: function(data){
                          if(data == 0)
                          {
                            swal("Oops","Terjadi kesalahan, silahkan hubungi customer service.","warning");
                            $('.btn-upgrade').attr('disabled',false).text('Lanjutkan');
                          }
                          else
                          {
                            window.location = '<?php echo site_url('plan/payment/'); ?>'+ data;
                          }

                        }, error:function(error){
                          swal("Oops","Terjadi kesalahan, silahkan hubungi customer service.","warning");
                          $('.btn-upgrade').attr('disabled',false).text('Lanjutkan');
                        }
          });
        }
      }
    });

    $('.click-quotation').on('click',function(){
      $('#modal_request_quotation').modal('show');
    });

    $('#btn-request-quotation').on('click',function(){
                if($('#request_proposal_subject').val() == '')
                {
                  swal("Oops","Subject belum diisi","error");
                }
                else if($('#request_proposal_content').val() == '')
                {
                  swal("Oops","Request belum diisi","error");
                }
                else
                {
                  $('#btn-request-quotation').attr('disabled',true).html('Mengirim...');

                      $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>plan/send_request",
                        data: {
                            'email_message' : $('#request_proposal_content').val(),
                            'email_subject' : $('#request_proposal_subject').val()
                            },
                        success: function(data){
                          $('#btn-request-quotation').attr('disabled',false).html('<i class="fa fa-send" aria-hidden="true"></i> Kirim');
                          if(data == 1)
                          {
                            swal('Request berhasil dikirim');
                            $('#btn-send-proposal').modal('hide');
                          }
                          else
                          {
                            swal('Ada kesalahan');
                          }

                        }, error:function(error){
                          swal('Ada kesalahan');
                        }
                      });
                }
    });
  });
</script>
