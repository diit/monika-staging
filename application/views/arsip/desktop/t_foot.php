<script src="<?= base_url() ?>node_modules/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();

        $('#cari-invoice').hide();

        $('#pills-proposal-tab').click(function(){
            $('#cari-proposal').show();
            $('#cari-invoice').hide();
        });

        $('#pills-invoice-tab').click(function(){
            $('#cari-proposal').hide();
            $('#cari-invoice').show();
        });

        $('#cari-proposal').keyup(function(){
          var query = $(this).val();
            $.ajax({
              url:"<?php echo base_url();?>arsip/cariProposalArsip",
              method:"post",
              data:{query:query},
              success:function(data){
                $('#list-proposal').remove();
                $('#list-cari-proposal').html(data);
              }
            });
        });

        $('#cari-invoice').keyup(function(){
          var query = $(this).val();
            $.ajax({
              url:"<?php echo base_url();?>arsip/cariInvoiceArsip",
              method:"post",
              data:{query:query},
              success:function(data){
                $('#list-invoice').remove();
                $('#list-cari-invoice').html(data);
              }
            });
        });

        $('.unarchive-proposal').click(function(){
            var id = $(this).attr('pid');
            swal({
                title: 'Unarchive Proposal',
                text: 'Yakin ingin unarchive proposal ini?',
                button: true,
                dangerMode: true
            })
            .then((willUndo)=>{
                if(willUndo){
                    $.ajax({
                        type: 'POST',
                        url: '<?=base_url()?>arsip/unarchive',
                        data: {
                            'id_p' : id
                        },
                        success: function(data){
                            swal('Proposal berhasil di unarchive','dokumen bisa Anda cek di menu proposal','success');
                            $('#list_prop_'+id).remove();
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            swal('Oops ada kesalahan','Proposal gagal diunarchive','error');
                        }
                    });
                }
            });
        });

        $('.unarchive-invoice').click(function(){
            var id = $(this).attr('iid');
            swal({
                title: 'Unarchive Invoice',
                text: 'Yakin ingin unarchive invoice ini?',
                button: true,
                dangerMode: true
            })
            .then((willUndo)=>{
                if(willUndo){
                    $.ajax({
                        type: 'POST',
                        url: '<?=base_url()?>arsip/unarchiveInvoice',
                        data: {
                            'id_i' : id
                        },
                        success: function(data){
                            swal('Invoice berhasil di unarchive','dokumen bisa Anda cek di menu invoice','success');
                            $('#list_inv_'+id).remove();
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            swal('Oops ada kesalahan','Invoice gagal diunarchive','error');
                        }
                    });
                }
            });
        });
    });
</script>
