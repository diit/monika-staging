<div id="content">
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-proposal-tab" data-toggle="pill" href="#pills-proposal" role="tab" aria-selected="true">Proposal</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-invoice-tab" data-toggle="pill" href="#pills-invoice" role="tab" aria-selected="false">Invoice</a>
                </li>
            </ul>

            <div class="tab-content" id="pills-tabContent">
                <!-- archieve proposal -->
                <div class="tab-pane fade show active" id="pills-proposal" role="tabpanel" aria-labelledby="pills-proposal-tab">
                    <div class="box">
                        <ul class="list-view" id="list-proposal">
                        <?php
                        if(count($arsip_prop)>0) {
                            foreach ($arsip_prop as $d) {
                                $random = md5(mt_rand(1,10000));
                                $first = substr($random,0,5);
                                $last = substr($random,5,10);
                                $prop_id = $first.$d->id.$last;
                        ?>
                            <li id="list_prop_<?= $prop_id ?>">
                                <p class="title-list-view"><?= $d->judul ?></p>
                                <p class="mini-text grey">Dibuat <?= $this->monikalib->format_date_indonesia($d->tgl_buat) ?></p>
                                <p class="header">Klien</p>
                                <p class="text-thin"><?= $d->nama_pic ?></p>
                                <p class="header">Email Klien</p>
                                <p class="text-thin"><?= $d->email ?></p>
                                <p class="header">Tgl Kirim</p>
                                <p class="text-thin"><?php echo (empty($d->tgl_kirim))?'Belum dikirim':$this->monikalib->format_date_indonesia($d->tgl_kirim) ?></p><br>
                                <div class="row space-bottom" style="margin-left:0">
                                    <button class="btn btn-mobile btn-sm btn-outline-primary unarchive-proposal" pid="<?= $prop_id ?>"><i class="fas fa-undo"></i></button>
                                </div>
                            </li>
                            <?php } } else { ?>
                            <div class="text-center empty-doc">
                                <i class="fas fa-inbox"></i>
                                <p>Belum ada dokumen yang terarsipkan.</p>
                            </div>
                            <?php } ?>
                        </ul>

                        <!-- hasil pencarian -->
                        <ul class="list-view" id="list-cari-proposal">
                        </ul>
                        <!-- end of hasil pencarian -->
                    </div>
                </div>
                <!-- end of archieve proposal -->

                <!-- archieve invoice -->
                <div class="tab-pane fade" id="pills-invoice" role="tabpanel" aria-labelledby="pills-invoice-tab">
                    <div class="box">
                        <ul class="list-view" id="list-invoice">
                        <?php
                        if(count($arsip_invoice)>0) {
                            foreach($arsip_invoice as $d) {
                                $random = md5(mt_rand(1,10000));
                                $first = substr($random,0,5);
                                $last = substr($random,5,10);
                                $in_id = $first.$d->id.$last;
                        ?>
                            <li id="list_inv_<?= $in_id ?>">
                                <p class="title-list-view"><?= $d->judul ?></p>
                                <p class="mini-text grey">Dibuat <?= $this->monikalib->format_date_indonesia($d->created_at) ?></p>
                                <p class="header">Nama PIC</p>
                                <p class="text-thin"><?= $d->nama_pic ?></p>
                                <div class="row">
                                    <div class="col">
                                        <p class="header">Kirim</p>
                                        <p class="text-thin"><?php echo ((empty($d->send_invoice)) || ($d->send_invoice == '0000-00-00 00:00:00'))? 'Belum dikirim' : $this->monikalib->format_date_indonesia($d->send_invoice); ?></p>
                                    </div>
                                    <div class="col">
                                        <p class="header">Deadline</p>
                                        <p class="text-thin"><?php echo ((empty($d->due_invoice)) || ($d->due_invoice == '0000-00-00 00:00:00'))? 'Not Set' : $this->monikalib->format_date_indonesia($d->due_invoice); ?></p>
                                    </div>
                                </div>
                                <?php
                                    $CI =& get_instance();
                                    $CI->load->model('invoice_model');
                                    $result = $CI->invoice_model->getItem($d->id);
                                    $total = 0;
                                    foreach($result  as $row){
                                        $total = $total + $row->harga;
                                    }
                                    $tagihan = ((($d->tax)/100)*$total)+$total;
                                ?>
                                <div class="row">
                                    <div class="col">
                                        <p class="header">Total Tagihan</p>
                                        <p class="text-thin">Rp <?= number_format($tagihan,'0',',','.') ?></p><br>
                                    </div>
                                    <div class="col">
                                        <p class="header">Dipersiapkan Oleh</p>
                                        <p class="text-thin"><?= ($d->id_user == $this->session->userdata('user_id')) ? 'Anda' : $d->user_name ?></p>
                                    </div>
                                </div>
                                <div class="row space-bottom" style="margin-left:0">
                                    <button class="btn btn-sm btn-outline-primary btn-mobile unarchive-invoice" iid="<?= $in_id ?>"><i class="fas fa-undo"></i></button>
                                </div>
                            </li>
                            <?php } } else { ?>
                            <div class="text-center empty-doc">
                                <i class="fas fa-inbox"></i>
                                <p>Belum ada dokumen yang terarsipkan.</p>
                            </div>
                            <?php } ?>
                        </ul>

                        <!-- hasil pencarian -->
                        <ul class="list-view" id="list-cari-invoice">
                        </ul>
                        <!-- end of hasil pencarian -->
                    </div>
                </div>
                <!-- end of archieve invoice -->
            </div>
        </div>
    </div>
</div>
