<?php
if($result > 0) {
foreach($arsip as $d) {
    $random = md5(mt_rand(1,10000));
    $first = substr($random,0,5);
    $last = substr($random,5,10);
    $in_id = $first.$d->id.$last;
?>
<li id="list_search_inv_<?= $in_id ?>">
    <a href="<?php echo site_url('invoice/lihat/'.$in_id); ?>"><p class="title-list-view"><?= $d->judul ?></p></a>
    <p class="mini-text grey">Dibuat <?= $this->monikalib->format_date_indonesia($d->created_at) ?></p>
    <p class="header">Nama PIC</p>
    <p class="text-thin"><?= $d->nama_pic ?></p>
    <div class="row">
        <div class="col">
            <p class="header">Kirim</p>
            <p class="text-thin"><?php echo ((empty($d->send_invoice)) || ($d->send_invoice == '0000-00-00 00:00:00'))? 'Belum dikirim' : $this->monikalib->format_date_indonesia($d->send_invoice); ?></p>
        </div>
        <div class="col">
            <p class="header">Deadline</p>
            <p class="text-thin"><?php echo ((empty($d->due_invoice)) || ($d->due_invoice == '0000-00-00 00:00:00'))? 'Not Set' : $this->monikalib->format_date_indonesia($d->due_invoice); ?></p>
        </div>
    </div>
    <?php
        $CI =& get_instance();
        $CI->load->model('invoice_model');
        $result = $CI->invoice_model->getItem($d->id);
        $total = 0;
        foreach($result  as $row){
            $total = $total + $row->harga;
        }
        $tagihan = ((($d->tax)/100)*$total)+$total;
    ?>
    <div class="row">
        <div class="col">
            <p class="header">Total Tagihan</p>
            <p class="text-thin">Rp <?= number_format($tagihan,'0',',','.') ?></p><br>
        </div>
        <div class="col">
            <p class="header">Dipersiapkan Oleh</p>
            <p class="text-thin"><?= ($d->id_user == $this->session->userdata('user_id')) ? 'Anda' : $d->user_name ?></p>
        </div>
    </div>
    <div class="row space-bottom" style="margin-left:0">
        <button class="btn btn-sm btn-outline-primary btn-mobile unarchive-invoice" iid="<?= $in_id ?>"><i class="fas fa-undo"></i></button>
    </div>
</li>
<?php } } else { ?>
<p>Arsip invoice yang Anda cari tidak ditemukan.</p>
<?php } ?>

<script>
    $(document).ready(function(){
        $('.unarchive-invoice').click(function(){
            var id = $(this).attr('iid');
            swal({
                title: 'Unarchive Invoice',
                text: 'Yakin ingin unarchive invoice ini? '+id,
                button: true,
                dangerMode: true
            })
            .then((willUndo)=>{
                if(willUndo){
                    $.ajax({
                        type: 'POST',
                        url: '<?=base_url()?>arsip/unarchiveInvoice',
                        data: {
                            'id_i' : id
                        },
                        success: function(data){
                            swal('Invoice berhasil di unarchive','dokumen bisa Anda cek di menu invoice','success');
                            $('#list_search_inv_'+id).remove();
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            swal('Oops ada kesalahan','Invoice gagal diunarchive','error');
                        }
                    });
                }
            });
        });
    });
</script>
