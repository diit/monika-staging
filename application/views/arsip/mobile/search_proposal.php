<?php
if($result>0) {
foreach ($arsip as $d) {
    $random = md5(mt_rand(1,10000));
    $first = substr($random,0,5);
    $last = substr($random,5,10);
    $prop_id = $first.$d->id.$last;
?>
<li id="list_search_prop_<?= $prop_id ?>">
    <p class="title-list-view"><?= $d->judul ?></p>
    <p class="mini-text grey">Dibuat <?= $this->monikalib->format_date_indonesia($d->tgl_buat) ?> | Oleh <?php echo $d->user_id == $this->session->userdata('user_id') ? 'Anda' : $d->user_name; ?></p>
    <p class="header">Klien</p>
    <p class="text-thin"><?= $d->nama_pic ?></p>
    <p class="header">Email Klien</p>
    <p class="text-thin"><?= $d->email ?></p>
    <p class="header">Tgl Kirim</p>
    <p class="text-thin"><?php echo (empty($d->tgl_kirim))?'Belum dikirim':$this->monikalib->format_date_indonesia($d->tgl_kirim) ?></p><br>
    <div class="row space-bottom" style="margin-left:0">
        <button class="btn btn-mobile btn-sm btn-outline-primary unarchive-proposal" pid="<?= $prop_id ?>"><i class="fas fa-undo"></i></button>
    </div>
</li>
<?php } } else { ?>
<p>Arsip proposal yang Anda cari tidak ditemukan.</p>
<?php } ?>

<script>
    $(document).ready(function(){
        $('.unarchive-proposal').click(function(){
            var id = $(this).attr('pid');
            swal({
                title: 'Unarchive Proposal',
                text: 'Yakin ingin unarchive proposal ini?',
                button: true,
                dangerMode: true
            })
            .then((willUndo)=>{
                if(willUndo){
                    $.ajax({
                        type: 'POST',
                        url: '<?=base_url()?>arsip/unarchive',
                        data: {
                            'id_p' : id
                        },
                        success: function(data){
                            swal('Proposal berhasil di unarchive','dokumen bisa Anda cek di menu proposal','success');
                            $('#list_search_prop_'+id).remove();
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            swal('Oops ada kesalahan','Proposal gagal diunarchive','error');
                        }
                    });
                }
            });
        });
    });
</script>
