<style media="screen">
  #main-content .row #proposal_status{
    padding: 5px 22px 10px 15px;
  }
  #main-content .row #proposal_status a{
    color:#46bfb2;
  }
  #main-content .row #proposal_status a h5{
    /*display: inline-flex;*/
    font-family: 'Avenir';
    float: left;
    font-weight: bold;
  }
  #main-content .row #proposal_status span{
    padding: 2px 6px 0px 6px;
    line-height: 3;
    float: right;
  }
  #main-content .row #proposal_status span i.fa{
    color:#fff;
  }
  .empty-panel {
    margin-top: 90px;
  	text-align: center;
  }
</style>

<style>
    .tab-pane{
        padding: 20px 0 20px 0;
    }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:active{
        color: #555;
        background-color: transparent;
        border: unset;
        font-weight: bold;
    }
    .nav-tabs > li.active > a:hover{
        background-color: transparent;
        border: unset;
        font-weight: bold;
    }
    .nav-tabs > li > a{
        color: #555;
        text-decoration: none;
    }
    .nav-tabs > li > a:hover{
        background-color: transparent;
        border: unset;
        font-weight: bold;
        color: #46bfb2;
    }
</style>
