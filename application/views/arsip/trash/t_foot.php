<script>
$(document).ready(function(){
    $('#cariArsipProposal').keyup(function(){
    var query = $(this).val();
      $.ajax({
        url:"<?php echo base_url();?>arsip/cariProposalArsip",
        method:"post",
        data:{query:query},
        success:function(data){
          $('.listProposalArsip').remove();
          $('#hasilCariArsipProposal').html(data);
        }
      });
    });

    $('#cariArsipInvoice').keyup(function(){
    var query = $(this).val();
      $.ajax({
        url:"<?php echo base_url();?>arsip/cariInvoiceArsip",
        method:"post",
        data:{query:query},
        success:function(data){
          $('.listInvoiceArsip').remove();
          $('#hasilCariArsipInvoice').html(data);
        }
      });
    });
});

</script>
