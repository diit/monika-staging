<section id="main-content">
    <section class="wrapper">
      <div class="row mt" style="margin:0">
        <div class="container">

            <h3 style="margin:40px 0 20px 15px;">ARSIP</h3>

            <div>
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Proposal</a></li>
                <li role="presentation" class=""><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Invoice</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <!-- Section Cari-->
                    <div class="form-group" style="margin:0px 0 20px 0">
                      <input type="text" class="form-control" id="cariArsipProposal" placeholder="Cari Nama atau Perusahaan klien anda">
                    </div>
                    <div id="hasilCariArsipProposal"></div>
                    <!-- Section Cari-->
                    <?php if(count($arsip_prop)>0){ ?>
                        <?php
                      foreach ($arsip_prop as $prop) {
                        $id_projek = $prop->id;
                        $klien = $prop->nama_pic;
                        $email = $prop->email;
                        $namaproj = $prop->info_projek;
                        $tgl = $prop->tgl_buat;
                        $status = $prop->status;
                       ?>
                        <div class="col-xs-12 mb-short listProposalArsip" id="div_list_proposal_<?= $id_projek ?>">
                             <!-- WHITE PANEL - TOP USER -->
                             <div  style="height:auto;text-align:left;" class="row white-panel pn">
                                 <div class="row">
                                     <div class="container" style="width:100%">
                                         <div class="col-xs-8 white-header">
                                             <h5><b><?= $namaproj ?></b></h5>
                                         </div>
                                         <?php if($status == 1) { ?>
                                         <div class="col-xs-3 col-xs-offset-1" id="proposal_status">
                                             <div class="label label-primary text-right prop-stat">Terkirim</div>
                                         </div>
                                         <?php } ?>
                                      </div>
                                 </div>
                               <div class="row">
                                 <div class="col-md-4 col-sm-2 col-xs-12">
                                   <p class="mt"><b>Klien</b></p>
                                   <p class="pcontent"><?= $klien ?></p>
                                 </div>
                                 <div class="col-md-3 col-sm-2 col-xs-12">
                                   <p class="mt"><b>Email Klien</b></p>
                                   <p class="pcontent"><?= $email ?></p>
                                 </div>
                                 <div class="col-md-3 col-sm-2 col-xs-12">
                                   <p class="mt"><b>Tanggal Proyek</b></p>
                                   <p class="pcontent"><?= $this->monikalib->format_date_indonesia($tgl) ?></p>
                                  </div>
                                   <div class="col-md-2 col-sm-6 col-xs-12">
                                      <!-- Tombol Aksi -->
                                        <span class="btn-group group-action">
                                            <a class="btn btn-default btn-delete archprop" value="<?=$id_projek?>"  href='arsip/unarchive/<?=$id_projek?>'>
                                         <i class="fa fa-fw fa-archive"></i>Unarchive</a>
                                       </span>
                                      <!-- /Tombol Aksi -->
                                   </div>
                               </div>
                             </div>
                          </div>

                     <?php } }else{ ?>
                    <div class="col-md-12 col-sm-4 mb">
                      <div class="empty-panel">

                        <h1 class="mt"><i class="fa fa-archive fa-3x"></i></h1>
                          <div class="darkblue-header">
                              <h2>Data arsip kosong.</h2>
                              <h5>Proposal yang diarsipkan akan anda temukan disini </h5>
                          </div>

                      </div>
                    </div>
                  <?php } ?>
                </div>

                  <div role="tabpanel" class="tab-pane" id="profile">
                      <?php if(count($arsip_prop)>0){ ?>
                            <div class="form-group" style="margin:0px 0 20px 0">
                              <input type="text" class="form-control" id="cariArsipInvoice" placeholder="Cari Nama atau Perusahaan klien anda">
                            </div>
                            <div id="hasilCariArsipInvoice"></div>
                          <?php foreach($arsip_invoice as $d) { ?>
                            <div class="col-xs-12 mb-short listInvoiceArsip" id="div_list_invoice_<?= $d->id ?>">
                             <!-- WHITE PANEL - TOP USER -->
                             <div  style="height:auto;text-align:left;" class="row white-panel pn">
                                 <div class="row">
                                     <div class="container" style="width:100%">
                                         <div class="col-xs-8 white-header">
                                             <h5><b><?= $d->judul ?></b></h5>
                                         </div>
                                         <?php if($d->status == 0) { ?>
                                         <div class="col-xs-3 col-xs-offset-1" id="proposal_status">
                                             <div class="label label-warning text-right prop-stat">Belum Terbayar</div>
                                         </div>
                                         <?php } ?>
                                         <?php if($d->status == 1) { ?>
                                         <div class="col-xs-3 col-xs-offset-1" id="proposal_status">
                                             <div class="label label-primary text-right prop-stat">Lunas</div>
                                         </div>
                                         <?php } ?>
                                      </div>
                                 </div>
                               <div class="row">
                                 <div class="col-md-4 col-sm-2 col-xs-12">
                                   <p class="mt"><b>Nama PIC</b></p>
                                   <p class="pcontent"><?= $d->nama_pic ?></p>
                                 </div>
                                 <div class="col-md-2 col-sm-2 col-xs-12">
                                   <p class="mt"><b>Tanggal Kirim</b></p>
                                   <p class="pcontent"><?= ($d->send_invoice=='')? 'Not Set' : $this->monikalib->format_date_indonesia($d->send_invoice) ?></p>
                                 </div>
                                 <div class="col-md-2 col-sm-2 col-xs-12">
                                   <p class="mt"><b>Deadline Pembayaran</b></p>
                                   <p class="pcontent"><?= ($d->due_invoice=='')? 'Not Set' : $this->monikalib->format_date_indonesia($d->due_invoice) ?></p>
                                 </div>
                                 <div class="col-md-2 col-sm-2 col-xs-12">
                                     <?php
                                      $CI =& get_instance();
                                      $CI->load->model('invoice_model');
                                      $result = $CI->invoice_model->getItem($d->id);
                                      $total = 0;
                                      foreach($result  as $row){
                                          $total = $total + $row->harga;
                                      }
                                      $tagihan = ((($d->tax)/100)*$total)+$total;
                                     ?>
                                   <p class="mt"><b>Total Tagihan</b></p>
                                   <p class="pcontent">Rp <?= number_format($tagihan,'0',',','.') ?></p>
                                  </div>
                                   <div class="col-md-2 col-sm-6 col-xs-12">
                                      <!-- Tombol Aksi -->
                                        <span class="btn-group group-action">
                                            <a class="btn btn-default btn-delete archprop" value="<?= $d->id ?>"  href='arsip/unarchiveInvoice/<?= $d->id ?>'>
                                         <i class="fa fa-fw fa-archive"></i>Unarchive</a>
                                       </span>
                                      <!-- /Tombol Aksi -->
                                   </div>
                               </div>
                             </div>
                          </div>
                      <?php } } else { ?>
                            <div class="col-md-12 col-sm-4 mb">
                              <div class="empty-panel">

                                <h1 class="mt"><i class="fa fa-archive fa-3x"></i></h1>
                                  <div class="darkblue-header">
                                      <h2>Data arsip kosong.</h2>
                                      <h5>Invoice yang diarsipkan akan anda temukan disini </h5>
                                  </div>

                              </div>
                            </div>
                      <?php } ?>
                  </div>
              </div>
            </div>
        </div><!-- /col-md-4 -->
      </div><!--/ row -->
    </section><!--/wrapper -->
</section><!-- /MAIN CONTENT -->
