<?php
if ($result > 0) {
    foreach($klien as $d) {
        $random = md5(mt_rand(1,10000));
        $first = substr($random,0,5);
        $last = substr($random,5,10);
        $id_kl = $first.$d->id_client.$last;
?>
<li>
    <a kl_id="<?= $id_kl ?>" class="show-klien"><p class="title-list-view"><?= $d->perusahaan ?></p></a>
    <p class="header"><?= $d->nama_pic ?></p>
    <p class="text-thin"><?= $d->email ?></p>
    <p class="text-thin"><?= $d->telephone ?></p>
    <p class="text-thin"><?= $d->alamat_usaha ?></p>
    <p class="text-thin"><?= $d->kota ?></p>
    &nbsp;
    <div class="space-bottom">
        <button class="btn btn-sm btn-outline-primary btn-mobile delete-kl" kid="<?= $id_kl ?>"><i class="far fa-trash-alt"></i></button>
    </div>
</li>
<?php }
    }
    else {
?>
<p>Data klien yang Anda cari tidak ada.</p>
<?php } ?>

<script>
    $(document).ready(function() {
        $('#modalKlien').modal({
            show: false,
            keyboard: false
        });

        function ValidateEmail(email) {
            var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return expr.test(email);
        };

        function isEmpty(el){
            return !$.trim(el.html())
        }

        $('#btn-add').click(function(){
            var pic =  $('#pic').val();
            var perusahaan =  $('#perusahaan').val();
            var email =  $('#email').val();
            var telephone =  $('#telephone').val();
            var alamat =  $('#alamat').val();
            var kota =  $('#kota').val();
            var user_id = <?=$this->session->userdata('user_id')?>;
            if(pic==''||perusahaan==''||email==''||telephone==''){
                swal("Oops","Pastikan Anda telah melengkapi isian","error");
                return false;
            }
            else if (!ValidateEmail(email)){
                swal("Oops","Format email masih salah","error");
                return false;
            }
            else{
                $.ajax({
                type: "POST",
                url: '<?php echo site_url('client/addclient');?>',
                data: {
                    pic:pic,
                    perusahaan:perusahaan,
                    email:email,
                    telephone:telephone,
                    alamat:alamat,
                    kota:kota,
                    user_id:user_id
                },
                success: function(data){
                    if(data == 1){
                        swal("Berhasil","Klien berhasil ditambahkan","success");
                        location.reload();
                    }
                    else{
                        swal("Oops","Tambah klien gagal","error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    swal("Oops","Tambah klien gagal","error");
                }
              });
            }
        });

        $('.delete-kl').on('click', function(){
            var id_klien = $(this).attr('kid');
            swal({
              title: id_klien,
              text: "Data klien akan dihapus permanen.",
              buttons: true,
              dangerMode: true,
            });
        });

        $('.show-klien').on('click', function(){
            var id_client = $(this).attr('kl_id');
            $.ajax({
                type: "POST",
                url: '<?=base_url()?>client/fetchData',
                data: {id_client:id_client},
                dataType:'json',
                success: function(data){
                if(data){
                    var client = data[0];
                    $('#picEditClient').val(client.nama_pic);
                    $('#perusahaanEditClient').val(client.perusahaan);
                    $('#emailEditClient').val(client.email);
                    $('#telephoneEditClient').val(client.telephone);
                    $('#alamatEditClient').val(client.alamat_usaha);
                    $('#kotaEditClient').val(client.kota);
                    $('#hiddenId').val(client.id_client);
                    $('#modal-edit').modal('show');
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal("Oops","Ada kesalahan","error");
            }});
        });
    });
</script>
