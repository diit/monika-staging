<section id="main-content">
  <section class="wrapper">
      <div class="container">
            <div class="row mt">
                <div class="col-xs-12">
                    <h3 class="pull-left">KELOLA KLIEN </h3>

                        <?php if(count($klien) > 0){ ?>
                          <button type="button" style="display: inline-block;margin-top:20px;" class="btn btn-theme03 pull-right" data-toggle="modal" data-target="#modalKlien"  id="btn_addclient">
                              <i class="fa fa-plus" aria-hidden="true"></i> Tambah Klien
                          </button>
                          <div class="form-group" style="margin:80px 0 20px 0">
                              <input type="text" class="form-control" id="cariKlien" placeholder="Cari Nama atau Perusahaan klien anda">
                          </div>
                          <table class="table table-hover table-striped">
                              <tbody id="tabelKlien">
                                  <?php
                                    if($klien!=false){
                                      foreach($klien as $row){
                                     ?>
                                    <tr>
                                        <td>
                                            <p class="hidden-lg hidden-md hidden-sm"><b><?=$row->nama_pic?></b></p>
                                            <p><b><?=$row->perusahaan?></b></p>
                                            <p class="hidden-lg hidden-md hidden-sm"><?=$row->telephone?></p>
                                            <p class="hidden-lg hidden-md hidden-sm"><?=$row->kota?></p>
                                        </td>
                                        <td class="hidden-xs">
                                            <p><b><?=$row->nama_pic?></b></p>
                                            <p><?=$row->telephone?></p>
                                        </td>
                                        <td class="hidden-xs">
                                            <p><?=$row->alamat_usaha?></p>
                                            <p><?=$row->kota?></p>
                                        </td>
                                        <td class="column-action">
                                            <div class="btn-group" >
                                                <button class="btn btn-default btn-edit" name="btn-edit"  value="<?=$row->id_client?>" type="button">
                                                    <i class="fa fa-fw s fa-pencil"></i>Edit</button>
                                                <button class="btn btn-default btn-delete" name="btn-delete" value="<?=$row->id_client?>" type="button">
                                                    <i class="fa fa-fw fa-remove"></i>Delete</button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php }} ?>
                                </tbody>
                                <tbody id="hasilCari">

                                </tbody>
                            </table>
                        <?php }else{ ?>
                          <div class="col-md-12 col-sm-4 mb">
                            <div class="empty-panel">
                              <h1 class="mt"><i class="fa fa-users fa-3x"></i></h1>
                                <div class="darkblue-header">
                                    <h2>Data klien kosong.</h2>
                                    <h5>Klien memuat nama Person-In-Charge (PIC) yang menjadi penerima proposal anda </h5>
                                    <button type="button" style="display: inline-block;margin-top:20px;" class="btn btn-theme03" data-toggle="modal" data-target="#modalKlien"  id="btn_addclient">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Tambah Klien
                                    </button>
                                </div>
                            </div>
                          </div>
                        <?php } ?>
              </div>
                <?php echo $this->pagination->create_links(); ?>
            <!-- <!-MODAL ADD CLIENT-->
              <div class="modal" id="modalKlien" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" >
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title" id="">Masukan Data Klien Baru Anda</h4>
                                </div>
                                <div class="modal-body" >
                                      <div class="form-group text-left">
                                        <label for="" class="">Nama PIC</label>
                                        <input type="text" class="form-control" id="pic" name="pic" placeholder="Nama PIC">
                                      </div>
                                      <div class="form-group text-left">
                                        <label for="" class="">Nama Tim / Perusahaan</label>
                                        <input type="text" class="form-control" id="perusahaan" name="perusahaan" placeholder="Nama Perusahaan">
                                      </div>
                                      <div class="form-group text-left">
                                        <label for="" class="">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email Resmi">
                                      </div>
                                      <div class="form-group text-left">
                                        <label for="" class="">No. Handphone</label>
                                        <input type="text" class="form-control" id="telephone" onkeypress="var key = event.keyCode || event.charCode; return ((key >= 48 && key <= 57) || key == 8);" name="telephone" placeholder="Nomor Telephone">
                                      </div>
                                      <div class="form-group text-left">
                                        <label for="" class="">Alamat Usaha</label>
                                        <textarea class="form-control" id="alamat" name="alamat"></textarea>
                                      </div>
                                      <div class="form-group text-left">
                                        <label for="" class="">Kota</label>
                                        <input type="text" class="form-control" id="kota" name="kota" placeholder="Kota">
                                      </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-theme03" id="btntambah">Tambah</button>
                                </div>
                          </div>
                    </div>
              </div>
            <!-- END OF MODAL ADD CLIENT-->
            <!-- <!-MODAL EDIT CLIENT-->
              <div class="modal fade" id="modalEdit" role="dialog" aria-labelledby="" aria-hidden="true" >
                    <div class="modal-dialog modal-sm">
                          <div class="modal-content">
                                <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" id="">Edit Klien Anda</h4>
                                </div>
                                <div class="modal-body" >
                                      <div class="form-group text-left">
                                        <label for="" class="">CONTACT PERSON CLIENT</label>
                                        <input type="text" class="form-control" id="picEditClient" name="picEditClient" placeholder="Nama PIC">
                                      </div>
                                      <div class="form-group text-left">
                                        <label for="" class="">NAMA PERUSAHAAN</label><span style="float: right;">(optional)</span>
                                        <input type="text" class="form-control" id="perusahaanEditClient" name="perusahaanEditClient" placeholder="Nama Perusahaan">
                                      </div>
                                      <div class="form-group text-left">
                                        <label for="" class="">EMAIL</label>
                                        <input type="email" class="form-control" id="emailEditClient" name="emailEditClient" placeholder="Email Resmi">
                                      </div>
                                      <div class="form-group text-left">
                                        <label for="" class="">TELEPHONE</label>
                                        <input type="text" class="form-control" id="telephoneEditClient" onkeypress="var key = event.keyCode || event.charCode; return ((key >= 48 && key <= 57) || key == 8);" name="telephoneEdit" placeholder="Nomor Telephone">
                                      </div>
                                      <div class="form-group text-left">
                                        <label for="" class="">ALAMAT PERUSAHAAN</label><span style="float: right;">(optional)</span>
                                        <textarea class="form-control" id="alamatEditClient" name="alamatEditClient"></textarea>
                                      </div>
                                      <div class="form-group text-left">
                                        <label for="" class="">KOTA</label><span style="float: right;">(optional)</span>
                                        <input type="text" class="form-control" id="kotaEditClient" name="kotaEditClient" placeholder="Kota">
                                      </div>
                                      <input hidden id="hiddenId" >
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-theme03" id="btnSimpanclient">Simpan</button>
                                </div>
                          </div>
                    </div>
              </div>
            <!-- END OF MODAL EDIT CLIENT-->
            <!-- <!-MODAL DELETE CLIENT-->
              <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" >
                    <div class="modal-dialog modal-sm">
                          <div class="modal-content">
                                <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" id="">Hapus Klien</h4>
                                </div>
                                <div class="modal-body" >
                                      <input hidden id="hiddenIdDelete">
                                      <p><b>Apakah anda yakin ingin menghapus ?</b></p>
                                      <p style="text-align:center" id="namadelete"></p>
                                </div>
                                <div class="modal-footer">
                                      <button style="width:100px" class="btn btn-default btn-sm" data-dismiss="modal">Tidak</button>
                                      <button style="width:100px" class="btn btn-theme btn-sm" id="btnhapus">Ya</button>
                                </div>
                          </div>
                    </div>
              </div>
            <!--END OF MODAL DELETE CLIENT-->
          </div>
        </div>
    </section>
</section>
