<style media="screen">
      .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus{
        background-color: #46bfb2;
        border-color: #46bfb2;
      }
      .pagination > li > a, .pagination > li > span{
        color: #46bfb2;
      }
      .empty-panel {
        margin-top: 90px;
      	text-align: center;
      }

</style>
