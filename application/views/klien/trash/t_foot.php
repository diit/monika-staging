<script type="text/javascript">
$(document).ready(function(){

  $('#btntambah').click(function(){
    var pic =  $('#pic').val();
    var perusahaan =  $('#perusahaan').val();
    var email =  $('#email').val();
    var telephone =  $('#telephone').val();
    var alamat =  $('#alamat').val();
    var kota =  $('#kota').val();
    var user_id = <?=$this->session->userdata('user_id')?>;
    if(pic==''||perusahaan==''||email==''||telephone==''){
      swal("Oops","Pastikan Anda telah melengkapi isian","error");
      return false;
    }else if (!ValidateEmail(email)){
      swal("Oops","Format email masih salah","error");
      return false;
    }else{
      $.ajax({
        type: "POST",
        url: '<?php echo site_url('client/addclient');?>',
        data: {pic:pic,perusahaan:perusahaan,email:email,telephone:telephone,alamat:alamat,kota:kota,user_id:user_id },
        success: function(data)
        {
          if(data == 1)
          {
            swal("Berhasil","Klien berhasil ditambahkan","success")
            .then((value) => {
              location.reload();
            });
          }
          else
          {
            swal("Oops","Tambah klien gagal","error");
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            swal("Oops","Tambah klien gagal","error");
        }
      });
    }
  });

  $(document).on('click', 'button.btn-edit,button.btn-edit2', function() {
    var id_client = $(this).val();
    $.ajax({
      type: "POST",
      url: '<?=base_url()?>client/fetchData',
      data: {id_client:id_client},
      dataType:'json',
      success: function(data){
        if(data)
        {
          var client = data[0];
            $('#picEditClient').val(client.nama_pic);
            $('#perusahaanEditClient').val(client.perusahaan);
            $('#emailEditClient').val(client.email);
            $('#telephoneEditClient').val(client.telephone);
            $('#alamatEditClient').val(client.alamat_usaha);
            $('#kotaEditClient').val(client.kota);
            $('#hiddenId').val(client.id_client);
            $('#modalEdit').modal('show');
        }
      },
        error: function (jqXHR, textStatus, errorThrown)
        {
            swal("Oops","Ada kesalahan","error");
        }
    });
  });

  $('#btnSimpanclient').click(function(){
    var pic =   $('#picEditClient').val();
    var perusahaan = $('#perusahaanEditClient').val();
    var email = $('#emailEditClient').val();
    var telephone =  $('#telephoneEditClient').val();
    var alamat = $('#alamatEditClient').val();
    var kota =  $('#kotaEditClient').val();
    var id_client =$('#hiddenId').val();

    if(pic==''||perusahaan==''||email==''||telephone==''){
      swal("Oops","Pastikan Anda telah melengkapi isian","error");
      return false;
    }else{
      $.ajax({
        type: "POST",
        url: '<?=base_url()?>client/updateclient',
        data: {pic:pic,perusahaan:perusahaan,email:email,telephone:telephone,alamat:alamat,kota:kota,id_client:id_client },
        success: function(data){
          swal("Berhasil","Data Klien berhasil diubah","success")
          .then((value) => {
            location.reload();
          });
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            swal("Oops","Ada kesalahan","error");
        }
      });
    }
  });

  $(document).on('click', 'button.btn-delete,button.btn-delete2', function(){
    $('#modalDelete').modal('show');
    var id_client=$(this).val();
    $('#hiddenIdDelete').val(id_client);
    $.ajax({
      type: "POST",
      url: '<?=base_url()?>client/fetchData',
      data: {id_client:id_client},
      dataType:'json',
      success: function(data){
        if(!data.error){
          if(data.proposal_exist){
            $('#namadelete').html('<strong class="text text-danger">Klien digunakan dalam proposal<strong>');
            $('#btnhapus').prop("disabled",true);
          }else{
            var client = data.client;
            $('#namadelete').html('"'+client.nama_pic+'"');
            $('#btnhapus').prop("disabled",false);
          }
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        swal("Oops","Ada kesalahan","error");
      }
    });
  });

  $('#btnhapus').click(function(){
    var id = $('#hiddenIdDelete').val();
    $.ajax({
      type: "POST",
      url: '<?=base_url()?>client/delete',
      data: {id_client:id},
      dataType:'json',
      success: function(data){
      }
    });
      location.reload();
  });

  function isEmpty( el ){
      return !$.trim(el.html())
  }

  $('#cariKlien').keyup(function(){
    var query = $(this).val();
    // var table = document.getElementById("myTable")
      $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>client/view",
        data:{query:query},
        success:function(data)
        {
          $('#tabelKlien').remove();
            $('#hasilCari').html(data);
//          var klien = JSON.parse(data);
          // if (isEmpty($('#hasilCari'))) {

          // }
<<<<<<< HEAD
//          var i = 0;
//          var tr = document.createElement('tr');
//          var td = document.createElement('td');
//          for (i; i < klien.jumlahData; i++) {
//            var text = klien.list[i].pic;
//            td.innerHTML = text;
//            tr.appendChild(td);
//            $('#hasilCari').append(tr);
//          }
=======
          var i = 0;
          var tr = document.createElement('tr');
          var td = document.createElement('td');
          for (i; i < klien.jumlahData; i++) {
            var text = klien.list[i].pic;
            td.innerHTML = text;
            tr.appendChild(td);
            $('#hasilCari').append(tr);
          }
>>>>>>> origin/percobaan
          // console.log(klien.list[0].pic);
          // $('#hasilCari').text(klien.list);
        }
      });
  });
})
</script>
