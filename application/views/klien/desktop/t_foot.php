<script src="<?= base_url() ?>node_modules/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $(document).ready(function() {
        //script index
        $('[data-toggle="tooltip"]').tooltip();

        $('#modalKlien').modal({
            show: false,
            keyboard: false
        });

        function ValidateEmail(email) {
            var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return expr.test(email);
        };

        function isEmpty(el){
            return !$.trim(el.html())
        }

        $('#btn-add').click(function(){
            var pic =  $('#pic').val();
            var perusahaan =  $('#perusahaan').val();
            var email =  $('#email').val();
            var telephone =  $('#telephone').val();
            var alamat =  $('#alamat').val();
            var kota =  $('#kota').val();
            var user_id = <?=$this->session->userdata('user_id')?>;
            if(pic==''||perusahaan==''||email==''||telephone==''){
                swal("Oops","Pastikan Anda telah melengkapi isian","error");
                return false;
            }
            else if (!ValidateEmail(email)){
                swal("Oops","Format email masih salah","error");
                return false;
            }
            else{
                $.ajax({
                type: "POST",
                url: '<?php echo site_url('client/addclient');?>',
                data: {
                    pic:pic,
                    perusahaan:perusahaan,
                    email:email,
                    telephone:telephone,
                    alamat:alamat,
                    kota:kota,
                    user_id:user_id
                },
                success: function(data){
                    if(data == 1){
                        swal("Berhasil","Klien berhasil ditambahkan","success");
                        location.reload();
                    }
                    else{
                        swal("Oops","Tambah klien gagal","error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    swal("Oops","Tambah klien gagal","error");
                }
              });
            }
        });

        $('.delete-kl').on('click', function(){
            var id_klien = $(this).attr('kid');
            swal({
              title: "Apakah Anda yakin?",
              text: "Data klien akan dihapus permanen.",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
                if(willDelete){
                    $.ajax({
                        type: "POST",
                        url: '<?=base_url()?>client/delete',
                        data: {'id_client' : id_klien},
                        success: function(data)
                        {
                            swal("Archived","Klien berhasil dihapus","success");
                            location.reload();
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            swal("Oops","Klien gagal dihapus. Silahkan mencoba kembali.","error");
                        }
                    });
                }
            });
        });

        $('.show-klien').on('click', function(){
            var id_client = $(this).attr('kl_id');
            $.ajax({
                type: "POST",
                url: '<?=base_url()?>client/fetchData',
                data: {id_client:id_client},
                dataType:'json',
                success: function(data){
                if(data){
                    var client = data[0];
                    $('#picEditClient').val(client.nama_pic);
                    $('#perusahaanEditClient').val(client.perusahaan);
                    $('#emailEditClient').val(client.email);
                    $('#telephoneEditClient').val(client.telephone);
                    $('#alamatEditClient').val(client.alamat_usaha);
                    $('#kotaEditClient').val(client.kota);
                    $('#hiddenId').val(id_client);
                    $('#modal-edit').modal('show');
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal("Oops","Ada kesalahan","error");
            }});
        });

        $('#cari-klien').keyup(function(){
          var query = $(this).val();
            $.ajax({
              url:"<?php echo base_url();?>client/cariKlien",
              method:"post",
              data:{query:query},
              success:function(data){
                $('#list-klien').remove();
                $('#list-cari-klien').html(data);
              }
            });
        });

        $('#btn-simpan').click(function(){
            var pic =   $('#picEditClient').val();
            var perusahaan = $('#perusahaanEditClient').val();
            var email = $('#emailEditClient').val();
            var telephone =  $('#telephoneEditClient').val();
            var alamat = $('#alamatEditClient').val();
            var kota =  $('#kotaEditClient').val();
            var id_client = $('#hiddenId').val();

            if(pic==''||perusahaan==''||email==''||telephone==''){
                swal("Oops","Pastikan Anda telah melengkapi isian","error");
                return false;
            }else{
                $.ajax({
                    type: "POST",
                    url: '<?=base_url()?>client/updateclient',
                    data: {
                        pic:pic,
                        perusahaan:perusahaan,
                        email:email,
                        telephone:telephone,
                        alamat:alamat,
                        kota:kota,
                        id_client:id_client
                    },
                    success: function(data){
                        swal("Berhasil","Data Klien berhasil diubah","success")
                        location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        swal("Oops","Ada kesalahan","error");
                    }
                });
            }
        });
    });
</script>
