<div id="content">
    <a href="#" data-toggle="modal" data-target="#modalKlien" class="btn btn-primary pull-right">tambah klien</a>
    <h2 class="content-title">Kontak Klien</h2>
    <div class="box">
        <ul class="list-view" id="list-klien">
            <?php
            if(count($klien) > 0) {
                foreach($klien as $d) {
                    $random = md5(mt_rand(1,10000));
                    $first = substr($random,0,5);
                    $last = substr($random,5,10);
                    $id_kl = $first.$d->id_client.$last;
            ?>
            <li>
                <a href="#" kl_id="<?= $id_kl ?>" class="show-klien">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="title-list-view"><?= $d->perusahaan ?></p>
                    </div>
                </div>
                </a>
                <div class="row body-list-view">
                    <div class="col-sm-2">
                        <p class="header"><?= $d->nama_pic ?></p>
                    </div>
                    <div class="col-sm-3">
                        <p class="text-thin"><?= $d->email ?></p>
                        <p class="text-thin"><?= $d->telephone ?></p>
                    </div>
                    <div class="col-sm-5">
                        <p class="text-thin"><?= $d->alamat_usaha ?></p>
                        <p class="text-thin"><?= $d->kota ?></p>
                    </div>
                    <div class="col-sm-2">
                        <div class="btn-group" role="group">
                            <a href="#" class="btn btn-sm btn-outline-primary delete-kl" data-toggle="tooltip" data-placement="top" title="Hapus data" kid="<?= $id_kl ?>"><i class="far fa-trash-alt"></i></a>
                        </div>
                    </div>
                </div>
            </li>
            <?php } } else { ?>
            <div class="text-center empty-doc">
                <i class="fas fa-inbox"></i>
                <p>Belum ada data klien yang disimpan.</p>
            </div>
            <?php } ?>
        </ul>

        <?php echo $this->pagination->create_links(); ?>

        <!-- hasil pencarian -->
        <ul class="list-view" id="list-cari-klien"></ul>
        <!-- end of hasil pencarian -->
    </div>
</div>

<!-- add klien -->
<div class="modal fade form-space" tabindex="-1" role="dialog" id="modalKlien">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Data Klien Baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="col-sm-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-user"></i></div>
                            </div>
                            <input type="text" class="form-control" id="pic" name="pic" placeholder="Nama PIC">
                        </div>
                        &nbsp;
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-briefcase"></i></div>
                            </div>
                            <input type="text" class="form-control" id="perusahaan" name="perusahaan" placeholder="Nama Tim/Perusahaan (optional)">
                        </div>
                        &nbsp;
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-envelope"></i></div>
                            </div>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email Resmi">
                        </div>
                        &nbsp;
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-phone"></i></div>
                            </div>
                            <input type="text" class="form-control" id="telephone" onkeypress="var key = event.keyCode || event.charCode; return ((key >= 48 && key <= 57) || key == 8);" name="telephone" placeholder="Nomor Telephone">
                        </div>
                        &nbsp;
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-map-marked-alt"></i></div>
                            </div>
                            <textarea class="form-control" rows="3" id="alamat" name="alamat">Alamat klien/perusahaan</textarea>
                        </div>
                        &nbsp;
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-map-marker-alt"></i> </div>
                            </div>
                            <input type="text" class="form-control" id="kota" name="kota" placeholder="Kota/kabupaten">
                        </div>
                    </div>
                </div>
                &nbsp;
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-add">tambah</button>
            </div>
        </div>
    </div>
</div>
<!-- end of add client -->

<!-- edit klien -->
<div class="modal fade form-space" tabindex="-1" role="dialog" id="modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Data Klien</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="col-sm-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-user"></i></div>
                            </div>
                            <input type="text" class="form-control" id="picEditClient" name="picEditClient" placeholder="Nama PIC">
                        </div>
                        &nbsp;
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-briefcase"></i></div>
                            </div>
                            <input type="text" class="form-control" id="perusahaanEditClient" name="perusahaanEditClient" placeholder="Nama Tim/Perusahaan (optional)">
                        </div>
                        &nbsp;
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-envelope"></i></div>
                            </div>
                            <input type="email" class="form-control" id="emailEditClient" name="emailEditClient" placeholder="Email Resmi">
                        </div>
                        &nbsp;
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-phone"></i></div>
                            </div>
                            <input type="text" class="form-control" id="telephoneEditClient" onkeypress="var key = event.keyCode || event.charCode; return ((key >= 48 && key <= 57) || key == 8);" name="telephoneEditClient" placeholder="Nomor Telephone">
                        </div>
                        &nbsp;
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-map-marked-alt"></i></div>
                            </div>
                            <textarea class="form-control" rows="3" id="alamatEditClient" name="alamatEditClient">Alamat klien/perusahaan</textarea>
                        </div>
                        &nbsp;
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-map-marker-alt"></i> </div>
                            </div>
                            <input type="text" class="form-control" id="kotaEditClient" name="kotaEditClient" placeholder="Kota/kabupaten">
                        </div>
                        <input hidden id="hiddenId" >
                    </div>
                </div>
                &nbsp;
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-simpan">simpan</button>
            </div>
        </div>
    </div>
</div>
<!-- end of edit client -->
