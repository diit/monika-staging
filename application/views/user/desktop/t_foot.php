<script src="<?= base_url() ?>node_modules/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();

        function ValidateEmail(email) {
              var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              return expr.test(email);
        };

        // upload logo corp
        $('#upload-logo-corp').on('click', function(){
            $('#upload-logo-corp-input').click();
        });
        $('#upload-logo-corp-input').on('change', function(){
            document.getElementById('upload-logo-corp').innerHTML = 'Uploading....';
            $('#btn-upload-corp-logo').click();
        });
        $('#delete-logo-corp').on('click', function(){
            swal({
                title: "Hapus Logo di Team Anda",
                text: "Anda akan menghapus logo team. Lanjutkan?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                }).then((result) => {
                if (result) {
                    $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>user/delete_team_logo",
                        data: {
                            team_id : <?= $this->session->userdata('team_id');?>,
                        },
                        datatype: 'json',
                        success: function(response){
                            if(response == 1){
                                swal('Berhasil hapus logo team')
                                location.reload();
                            }else{
                                swal('Gagal hapus logo. silahkan hubungi admin kami');
                            }
                        }, error:function(error){
                            // console.log('gagal hapus logo')
                            console.log(error);
                        }
                    });
                }
            })
        });
        // end of upload logo corp

        var cs = 1;
        $(document).on('click','#btn-tambah',function(){
            var email1 = $('#inputmemberemail1').val();

            if (cs==1){
                if (email1 == ''){
                    $('#invite-msg').empty().append('<div class="alert alert-fit text-little alert-danger alert-dismissible fade show" role="alert">Email member harus diisi.<button type="button" class="close text-little" data-dismiss="alert" aria-label="Close" style="margin-top:-3px"><span aria-hidden="true">&times;</span></button></div>');
                }
                else if (!ValidateEmail(email1)){
                    $('#invite-msg').empty().append('<div class="alert alert-fit text-little alert-danger alert-dismissible fade show" role="alert">Masukkan format email dengan benar.<button type="button" class="close text-little" data-dismiss="alert" aria-label="Close" style="margin-top:-3px"><span aria-hidden="true">&times;</span></button></div>');
                }
                else{
                    cs++;

                    var divaddemail = $('<div id="invite'+ parseInt(cs) +'" class="row"></div>');
                    var inputemail = $('<div class="col-8"><input name="memberemail" id="inputmemberemail'+ cs +'" type="text" class="form-control" placeholder="email member" style="margin-bottom:10px"></div>');
                    var btndeleteemail = $('<div class="col-4"><i emailid="'+ cs +'" class="grey pointer far fa-trash-alt delete-invitation" style="padding-top:10px"></i></div>');
                    $('.listemail').append(divaddemail.append(inputemail).append(btndeleteemail));
                    inputemail.select();
                }
            }
            else{
                var latestemail = $('#inputmemberemail'+cs).val();
                if(latestemail == ''){
                    $('#invite-msg').empty().append('<div class="alert alert-fit text-little alert-danger alert-dismissible fade show" role="alert">Email member harus diisi.<button type="button" class="close text-little" data-dismiss="alert" aria-label="Close" style="margin-top:-3px"><span aria-hidden="true">&times;</span></button></div>');
                }
                else if(!ValidateEmail(latestemail)){
                    $('#invite-msg').empty().append('<div class="alert alert-fit text-little alert-danger alert-dismissible fade show" role="alert">Masukkan format email dengan benar.<button type="button" class="close text-little" data-dismiss="alert" aria-label="Close" style="margin-top:-3px"><span aria-hidden="true">&times;</span></button></div>');
                }
                else{
                    cs++;

                    var divaddemail = $('<div id="invite'+ parseInt(cs) +'" class="row"></div>');
                    var inputemail = $('<div class="col-8"><input name="memberemail" id="inputmemberemail'+ cs +'" type="text" class="form-control" placeholder="email member" style="margin-bottom:10px"></div>');
                    var btndeleteemail = $('<div class="col-4"><i emailid="'+ cs +'" class="grey pointer far fa-trash-alt delete-invitation" style="padding-top:10px"></i></div>');
                    $('.listemail').append(divaddemail.append(inputemail).append(btndeleteemail));
                    inputemail.select();
                }

            }
        });

        $(document).on('click','.delete-invitation',function(){
            var emailid = $(this).attr('emailid');
            $('#invite'+emailid).remove();
            cs--;
        });

        $('#invite').on('click',function(){
            var invites=[];

            $('[name="memberemail"]').each(function(){
                if($(this).val() != ''){
                    invites.push($(this).val());
                }
            });

            if(invites.length <= 0){
                swal("Masukkan email member yang akan diundang");
            }
            else{
                $(this).attr('disable',true);
                $(this).html('Mengirim...');
                $.ajax({
                    type: "POST",
                    data:{
                        invites: invites
                    },
                    url: '<?php echo base_url()."user/invite_team"; ?>',
                    success: function(data)
                    {
                        if(data == 1)
                        {
                            $('#modalAddMember').modal('hide');
                            swal('Berhasil','Undangan telah dikirim','success');
                            location.reload();
                        }
                        else
                        {
                            $(this).attr('disable',false);
                            $(this).html('kirim invitation');
                            $('#modalAddMember').modal('hide');
                            swal("Oops","Undangan gagal dikirim. Silahkan coba kembali.","error");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        $(this).attr('disable',false);
                        $(this).html('kirim invitation');
                        $('#modalAddMember').modal('hide');
                        swal("Oops","Undangan gagal dikirim. Email sudah Anda undang sebelumnya.","error");
                    }
                });
            }
        });

        $('#edit-usaha').click(function(){
            $.ajax({
                type: "POST",
                url: "<?=base_url()?>user/update_usaha",
                data: {
                    name : $('#nama-usaha').val(),
                    desc : $('#deskripsi').val()
                },
                datatype: 'json',
                success: function(response){
                    swal('Update Tim/Usaha','Informasi tim/usaha berhasil diedit','success');
                    location.reload();
                },
                error:function(error){
                    swal('Informasi tim/usaha gagal diedit');
                }
            });
        });
    });
</script>
