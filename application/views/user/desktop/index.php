<div id="content">
    <div class="row">
        <div class="col-6">
            <!-- profile -->
            <div class="box">
                <p class="box-title">profil</p>
                <div class="form-group">
                    <label class="grey text-little">Nama Lengkap</label>
                    <input type="text" class="form-control text-little" value="<?php echo $profil->user_name; ?>" id="namaEdit" name="namaEdit" placeholder="Nama Lengkap">
                </div>
                <div class="form-group">
                    <label class="grey text-little">Email</label>
                    <input type="email" class="form-control text-little" value="<?php echo $profil->user_email; ?>" id="emailEdit" name="emailEdit" placeholder="Email Resmi" readonly>
                </div>
                <div class="form-group">
                    <label class="grey text-little">Nomor Handphone</label>
                    <input type="text" class="form-control text-little" value="<?php echo $profil->user_mobile; ?>" id="mobileEdit" onkeypress="var key = event.keyCode || event.charCode; return ((key >= 48 && key <= 57) || key == 8);" name="mobileEdit" placeholder="Nomor Handphone">
                </div>
                <div class="form-group">
                    <label class="grey text-little">Kota Domisili</label>
                    <input type="text" class="form-control text-little" value="<?php echo $profil->kota; ?>" id="kotaEdit" name="kotaEdit" placeholder="Kota">
                </div>
                <div class="form-group">
                    <label class="grey text-little">Alamat</label>
                    <textarea class="form-control text-little" id="alamatEdit" name="alamatEdit" placeholder="Alamat"><?php echo $profil->alamat; ?></textarea>
                </div>
                &nbsp;
                <br>
                <button class="btn btn-sm btn-primary pull-right">edit profile</button>
                <br>
            </div>
            <!-- end of profile -->
        </div>
        <div class="col-6">
            <!-- tentang usaha -->
            <div class="box">
                <p class="box-title">tentang usaha</p>
                <?php if(!empty($logo->team_logo)){ ?>
                    <img src="<?= $logo->team_logo ?>" alt="img-<?= $profil->user_corp ?>" id="photo-profile">
                <?php } else { ?>
                    <p><i class="far fa-user-circle grey text-little-light" style="font-size:5em"></i></p>
                <?php } ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group" role="group" aria-label="Button edit Logo">
                            <button type="button" class="btn btn-primary" id="upload-logo-corp">Upload Logo Baru</button>
                            <?php if(!empty($logo->team_logo)){?>
                            <button type="button" class="btn btn-danger" id="delete-logo-corp">Hapus Logo</button>
                            <?php } ?>
                        </div>
                        <form class="upload-img-logo-corp" action="<?= site_url('user/upload_image_logo')?>" method="post" enctype="multipart/form-data" style="display:none;">
                            <input type="file" name="userfile" id="upload-logo-corp-input" accept=".jpeg, .jpg, .png, .JPEG, .PNG">
                            <button type="submit" class="btn" id="btn-upload-corp-logo" >Upload File</button>
                        </form>
                    </div>
                </div>
                <div class="form-group">
                    <label class="grey text-little">Nama Tim / Perusahaan</label>
                    <input type="text" class="form-control text-little" value="<?php echo ($profil->user_corp == $profil->user_name)?'':$profil->user_corp; ?>" id="nama-usaha" placeholder="Nama Perusahaan">
                </div>
                <div class="form-group">
                    <label class="grey text-little">Deskripsi</label>
                    <textarea class="form-control text-little" id="deskripsi" placeholder="deskripsi usaha"><?=$profil->about?></textarea>
                </div>
                &nbsp;
                <br>
                <button class="btn btn-sm btn-primary pull-right" id="edit-usaha">edit usaha</button>
                <br>
            </div>
            <!-- end of tentang usaha -->

            <!-- tim member -->
            <div class="box top-space">
                <button class="btn btn-sm btn-outline-primary pull-right" <?= (($this->monikalib->currentUserPlan()==0) && ($this->monikalib->limitDoc()=='personal')) ? 'data-toggle="tooltip" data-placement="left" title="Upgrade plan untuk invite team member"' : 'data-toggle="modal" data-target="#modalAddMember"' ?>>invite member</button>
                <p class="box-title">tim member</p>
                &nbsp;
                <table class="table text-little">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Nama</th>
                      <th scope="col">Role</th>
                      <th scope="col">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i=1;
                    foreach ($tim as $d) {
                    ?>
                    <tr>
                        <th scope="row"><?= $i; ?></th>
                        <td><?php echo $d->user_name == $this->session->userdata('user_name') ? 'Anda':$d->user_name; ?></td>
                        <td>
                            <?php if($d->role == 0){ ?>
                            <span> - </span>
                            <?php }else if($d->role == 1){ ?>
                            <span class="badge badge-success">Baca</span>
                            <span class="badge badge-secondary">Edit</span>
                            <span class="badge badge-primary">Admin</span>
                            <?php }else if($d->role == 2){ ?>
                            <span class="badge badge-success">Baca</span>
                            <span class="badge badge-secondary">Edit</span>
                            <?php }else if($d->role == 3){ ?>
                            <span class="badge badge-success">Baca</span>
                            <?php } ?>
                        </td>
                        <td><?php echo $d->role == 0 ?'Pending':'Aktif'; ?></td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
            </div>
            <!-- end of tim member -->
        </div>
    </div>
</div>

<!-- modal add member -->
<div class="modal fade" id="modalAddMember" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title text-center">Undang Tim Member Baru</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-little">Masukkan email aktif member yang akan diundang. Kami mengirimkan email pemberitahuan ke member baru melalui email tersebut.</p>
                <div id="invite-msg"></div>

                <div class="listemail">
                    <div id="invite1" class="row">
                        <div class="col-8">
                        <input class="form-control" placeholder="email member" id="inputmemberemail1" name="memberemail" type="text" style="margin-bottom:10px">
                        </div>
                    </div>
                </div>
                <button class="btn btn-outline-grey btn-sm text-small" id="btn-tambah">tambah</button>
                &nbsp;
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-sm" id="invite">kirim invitation</button>
            </div>
        </div>
    </div>
</div>
<!-- end of modal add member -->
