<script type="application/javascript">
    function ValidateEmail(email) {
              //var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
              var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              return expr.test(email);
      };
    $(document).ready(function () {

      $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if( input.length ) {
                    input.val(log);
                    $('.btn-save-team-logo-image').attr('disabled',false).addClass('btn-success');
                } else {
                    if( log ) alert(log);
                }

        });


        $('#btnsimpanprofil').click(function(){
                var nama =   $('#namaEdit').val();
                var perusahaan = $('#perusahaanEdit').val();
                var mobile =  $('#mobileEdit').val();
                var alamat = $('#alamatEdit').val();
                var kota =  $('#kotaEdit').val();
                var user_id =$('#hiddenId').val();
                if(namaEdit==''||perusahaanEdit==''||mobileEdit==''||alamatEdit==''||kotaEdit==''){
                  return false;
                }else{
                  $.ajax({
                    type: "POST",
                    url: '<?=base_url()?>user/updateUser',
                    data: {user_name:nama,user_corp:perusahaan,user_mobile:mobile,alamat:alamat,kota:kota,user_id:user_id },
                    success: function(data)
                    {
                       swal("Berhasil","Profil telah diubah","success")
                       .then((value) => {
                         location.reload();
                       });
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        swal("Oops","Edit profil gagal. Silahkan isi semua kolom yang tersedia","error");
                    }
                  });
                }
              });

        $('#add-member').on('click',function(){
          <?php if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){ ?>
            $('#modalmember').modal('show');
          <?php }else{ ?>
            <?php if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1){ ?>
              <?php if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4){ ?>
                $('#modalmember').modal('show');
              <?php }else if($this->monikalib->getUserPlan()->plan_id == 1 || $this->monikalib->getUserPlan()->plan_id == 2){ ?>
                swal("Oops","Untuk menambah member tim, ubah plan menjadi \"Agency\".","info", {
                  buttons: {
                    cancel:true,
                    upgrade:{
                      text: "Ubah Plan",
                      value: "upgrade",
                    }
                  },
                })
                .then((value) => {
                  switch (value) {

                    case "upgrade":
                      window.location = "<?php echo site_url('plan'); ?>";
                      break;

                    default:
                      break;
                  }
                });
              <?php } ?>
            <?php }else{ ?>
              swal("Oops","Saat ini Anda menggunakan Plan Personal. Untuk menambah member tim, ubah plan menjadi \"Agency\".","info", {
                  buttons: {
                    cancel:true,
                    upgrade:{
                      text: "Ubah Plan",
                      value: "upgrade",
                    }
                  },
                })
                .then((value) => {
                  switch (value) {

                    case "upgrade":
                      window.location = "<?php echo site_url('plan'); ?>";
                      break;

                    default:
                      break;
                  }
                });
            <?php } ?>
          <?php } ?>
        });

        $('#btntambah').on('click',function(){
          var thisbtn = $(this);
          var invites=[];
          $('[name="memberemail"]').each(function(){
            if($(this).val() != '')
            {
              invites.push($(this).val());
            }
          });

          if(invites.length <= 0)
          {
            swal("Masukkan email member yang akan diundang");
          }
          else
          {
            thisbtn.attr('disable',true);
            thisbtn.html('Mengirim...');
            $.ajax({
                            type: "POST",
                            data:{
                              invites: invites
                            },
                            url: '<?php echo base_url()."user/invite_team"; ?>',
                            success: function(data)
                            {
                               if(data == 1)
                              {
                                thisbtn.attr('disable',false);
                                thisbtn.text('Undang');
                                swal('Berhasil','Undangan telah dikirim','success')
                                .then((value) => {
                                  $('#category_edit').modal('hide');
                                  location.reload();
                                });
                              }
                              else
                              {
                                thisbtn.attr('disable',false);
                                thisbtn.text('Undang');
                                swal("Oops","Undangan gagal dikirim. Silahkan coba kembali.","error");
                              }
                            },
                            error: function (jqXHR, textStatus, errorThrown)
                            {
                              thisbtn.attr('disable',false);
                                thisbtn.text('Undang');
                                swal("Oops","Undangan gagal dikirim. Email sudah Anda undang sebelumnya.","error");
                            }

                          });
          }
        });

        var cs = 1;
            $(document).on('click','.addemail',function(){
              var divaddskill = $('<div id="invite'+ parseInt(cs) +'"></div>');
              var inputskill = $('<input style="float: left;width: 85%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" name="memberemail" id="memberemail'+ cs +'" type="text" class="form-control memberemail" placeholder="Email" maxlength="50">');
              var btndeleteskill = $('<button emailid="'+ cs +'" style="margin-top: 13px;padding: 3px 7px;font-size: 11px;border-radius: 3px;" type="button" class="btn btn-default btn-sm deleteemail"><i class="fa fa-trash"></i></button>');
              var latesskill = $('#inputmemberemail'+cs).val();
              if(latesskill == '')
              {
                swal("Isikan email member pada isian terakhir");
              }
              else if(!ValidateEmail(latesskill))
              {
                swal("Isi format email yang benar");
              }
              else
              {
                cs++;
                $('.listemail').append(divaddskill.append(inputskill).append(btndeleteskill));
                inputskill.select();
              }
            });

            $(document).on('click','.deleteemail',function(){
              var emailid = $(this).attr('emailid');
              $('#invite'+emailid).remove();
              cs--;
            });

        $(document).on('click','.btn-save-team-logo-image',function(){
                if($('#imglg').val() == '')
                {
                  swal("Oops","Pilih gambar yang akan ditambahkan","error");
                }
                else
                {
                  $('#form_upload_team_logo').submit();
                }
              });
    });
</script>
