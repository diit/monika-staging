<?php
if(count($this->monikalib->getUserPlan())<=0){
    $active = 0;
    $plan = 'Personal';
}
else{
    $active = $this->monikalib->getUserPlan()->plan_id;
    switch ($active) {
        case "1":
        $plan = 'Personal';
        break;

        case "2":
        $plan = 'Startup';
        break;

        case "3":
        $plan = 'Agency';
        break;

        case "3":
        $plan = 'Enterprise';
        break;

        default:
        break;
    }
}
?>
<div id="content" class="user-mobile">
    <!-- user -->
    <a href="<?=site_url('profile')?>">
        <div class="box">
            <div class="row">
                <div class="col-3 text-center">
                    <?php if(!empty($logo->team_logo)){ ?>
                    <img src="<?= $logo->team_logo ?>" alt="img-<?= $profil->user_corp ?>" id="photo-profile">
                    <?php } else { ?>
                    <i class="far fa-user-circle grey-light" style="font-size:4em"></i>
                    <?php } ?>
                </div>
                <div class="col-7">
                    <p><?= $profil->user_name ?></p>
                    <p class="text-little no-margin"><?php echo ($profil->user_corp == $profil->user_name)?'':$profil->user_corp; ?></p>
                    <p class="text-little grey no-margin">Plan <?= $plan ?></p>
                </div>
                <div class="col-2 text-right"><br><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </a>
    <!-- end of user -->

    <!-- menu -->
    <a href="#">
        <div class="box top-space">
            <ul class="list-view-user">
                <a href="<?= site_url('team-member') ?>"><li class="line-separator">Tim Member <i class="fas fa-angle-right pull-right"></i></li></a>
                <a href="<?=site_url('klien')?>"><li class="line-separator">Kontak Klien <i class="fas fa-angle-right pull-right"></i></li></a>
                <a href="<?= site_url('arsip') ?>"><li class="line-separator">Arsip <i class="fas fa-angle-right pull-right"></i></li></a>
                <a href="<?= site_url('plan') ?>"><li class="line-separator">Plan <i class="fas fa-angle-right pull-right"></i></li></a>
                <a href="<?=site_url('faq')?>"><li class="line-separator">Bantuan <i class="fas fa-angle-right pull-right"></i></li></a>
                <a href="<?=site_url('policy')?>"><li class="line-separator">Policy <i class="fas fa-angle-right pull-right"></i></li></a>
                <li id="logout" class="no-padding no-margin">Logout</li>
            </ul>
        </div>
    </a>
    <!-- end of menu -->
</div>
