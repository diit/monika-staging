<div id="content">
    <div class="box">
        <ul class="list-view">
            <?php foreach ($tim as $d) { ?>
            <li>
                <p><?php echo $d->user_name == $this->session->userdata('user_name') ? 'Anda':$d->user_name; ?></p>
                <div class="row">
                    <div class="col-8">
                        <?php if($d->role == 0){ ?>
                        <span> - </span>
                        <?php }else if($d->role == 1){ ?>
                        <span class="badge badge-success">Baca</span>
                        <span class="badge badge-secondary">Edit</span>
                        <span class="badge badge-primary">Admin</span>
                        <?php }else if($d->role == 2){ ?>
                        <span class="badge badge-success">Baca</span>
                        <span class="badge badge-secondary">Edit</span>
                        <?php }else if($d->role == 3){ ?>
                        <span class="badge badge-success">Baca</span>
                        <?php } ?>
                    </div>
                    <div class="col-4">
                        <p class="text-thin text-little pull-right"><?php echo $d->role == 0 ?'Pending':'Aktif'; ?></p>
                    </div>
                </div>
            </li>
            <?php } ?>
        </ul>

        <button class="btn btn-primary btn-fab" <?= (($this->monikalib->currentUserPlan()==0) && ($this->monikalib->limitDoc()=='personal')) ? 'id="notice-plan"' : 'data-toggle="modal" data-target="#modalAddMember"' ?>><i class="fas fa-plus"></i></button>
    </div>
</div>

<!-- modal add member -->
<div class="modal fade" id="modalAddMember" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title text-center">Undang Tim Member Baru</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-little">Masukkan email aktif member yang akan diundang. Kami mengirimkan email pemberitahuan ke member baru melalui email tersebut.</p>
                <div id="invite-msg"></div>

                <div class="listemail">
                    <div id="invite1" class="row">
                        <div class="col-10">
                        <input class="form-control" placeholder="email member" id="inputmemberemail1" name="memberemail" type="text" style="margin-bottom:10px">
                        </div>
                    </div>
                </div>
                <button class="btn btn-outline-grey btn-sm text-small" id="btn-tambah">tambah</button>
                &nbsp;
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-sm" id="invite">kirim invitation</button>
            </div>
        </div>
    </div>
</div>
<!-- end of modal add member -->
