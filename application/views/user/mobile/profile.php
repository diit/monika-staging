<?php
if(count($this->monikalib->getUserPlan())<=0){
    $active = 0;
    $plan = 'Personal';
}
else{
    $active = $this->monikalib->getUserPlan()->plan_id;
    switch ($active) {
        case "1":
        $plan = 'Personal';
        break;

        case "2":
        $plan = 'Startup';
        break;

        case "3":
        $plan = 'Agency';
        break;

        case "3":
        $plan = 'Enterprise';
        break;

        default:
        break;
    }
}
?>

<div id="content" class="detail-profile">
    <div class="box">
        <div class="text-center">
        <?php if(!empty($logo->team_logo)){ ?>
        <img src="<?= $logo->team_logo ?>" alt="img-<?= $profil->user_corp ?>" id="photo-profile">
        <?php } else { ?>
        <i class="far fa-user-circle grey-light" style="font-size:4em"></i>
        <?php } ?>
        </div>
        <p class="text-center bold"><?= $profil->user_name ?></p>
        &nbsp;
        <p><span class="grey text-little">Nama Tim / Perusahaan</span> <br> <?php echo ($profil->user_corp == $profil->user_name)?'-':$profil->user_corp; ?></p>
        <p><span class="grey text-little">Email Aktif</span> <br> <?= $profil->user_email ?></p>
        <p><span class="grey text-little">Nomor Telepon</span> <br> <?= $profil->user_mobile ?></p>
        <p><span class="grey text-little">Kota Domisili</span> <br> <?php echo (empty($profil->kota))?'-':$profil->kota; ?></p>
        <p><span class="grey text-little">Alamat</span> <br> <?php echo (empty($profil->alamat))?'-':$profil->alamat;  ?></p>
        <p><span class="grey text-little">Tentang Usaha</span> <br> <?php echo ($this->session->userdata('user_about') == NULL)?'': trim($this->session->userdata('user_about')) ?></p>
        <p><span class="grey text-little">Package Aktif</span> <br> Plan <?= $plan ?></p>

        <button id="edit" class="btn btn-primary btn-fab"><i class="fas fa-pen"></i></button>
    </div>
</div>
