<script src="<?= base_url() ?>node_modules/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $(document).ready(function(){
        // SCRIPT INDEX
        <?php if($this->uri->segment(1) == 'user') { ?>
        $('#logout').on('click',function(){
            swal({
              title: "Yakin akhiri sesi?",
              buttons: "logout",
              dangerMode: true
            })
            .then((willLogout) => {
                if(willLogout){
                    location.href = '<?= site_url('user/logout') ?>';
                }
            });
        });
        <?php } ?>
        // END OF SCRIPT INDEX

        // SCRIPT PROFILE
        <?php if($this->uri->segment(1) == 'profile') { ?>
        $('#edit').on('click',function(){
            swal('Untuk saat ini, Anda hanya bisa mengedit profile melalui desktop view di PC atau Laptop');
        });
        <?php } ?>
        // END OF SCRIPT PROFILE

        // SCRIPT TEAM MEMBER
        <?php if($this->uri->segment(1) == 'team-member') { ?>
        $('#notice-plan').click(function(){
            swal('Plan Personal','Upgrade plan untuk invite team member');
        });

        function ValidateEmail(email) {
              var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              return expr.test(email);
        };

        var cs = 1;
        $(document).on('click','#btn-tambah',function(){
            var email1 = $('#inputmemberemail1').val();

            if (cs==1){
                if (email1 == ''){
                    $('#invite-msg').empty().append('<div class="alert alert-fit text-little alert-danger alert-dismissible fade show" role="alert">Email member harus diisi.<button type="button" class="close text-little" data-dismiss="alert" aria-label="Close" style="margin-top:-3px"><span aria-hidden="true">&times;</span></button></div>');
                }
                else if (!ValidateEmail(email1)){
                    $('#invite-msg').empty().append('<div class="alert alert-fit text-little alert-danger alert-dismissible fade show" role="alert">Masukkan format email dengan benar.<button type="button" class="close text-little" data-dismiss="alert" aria-label="Close" style="margin-top:-3px"><span aria-hidden="true">&times;</span></button></div>');
                }
                else{
                    cs++;

                    var divaddemail = $('<div id="invite'+ parseInt(cs) +'" class="row"></div>');
                    var inputemail = $('<div class="col-10"><input name="memberemail" id="inputmemberemail'+ cs +'" type="text" class="form-control" placeholder="email member" style="margin-bottom:10px"></div>');
                    var btndeleteemail = $('<div class="col-2"><i emailid="'+ cs +'" class="grey pointer far fa-trash-alt delete-invitation" style="padding-top:10px"></i></div>');
                    $('.listemail').append(divaddemail.append(inputemail).append(btndeleteemail));
                    inputemail.select();
                }
            }
            else{
                var latestemail = $('#inputmemberemail'+cs).val();
                if(latestemail == ''){
                    $('#invite-msg').empty().append('<div class="alert alert-fit text-little alert-danger alert-dismissible fade show" role="alert">Email member harus diisi.<button type="button" class="close text-little" data-dismiss="alert" aria-label="Close" style="margin-top:-3px"><span aria-hidden="true">&times;</span></button></div>');
                }
                else if(!ValidateEmail(latestemail)){
                    $('#invite-msg').empty().append('<div class="alert alert-fit text-little alert-danger alert-dismissible fade show" role="alert">Masukkan format email dengan benar.<button type="button" class="close text-little" data-dismiss="alert" aria-label="Close" style="margin-top:-3px"><span aria-hidden="true">&times;</span></button></div>');
                }
                else{
                    cs++;

                    var divaddemail = $('<div id="invite'+ parseInt(cs) +'" class="row"></div>');
                    var inputemail = $('<div class="col-10"><input name="memberemail" id="inputmemberemail'+ cs +'" type="text" class="form-control" placeholder="email member" style="margin-bottom:10px"></div>');
                    var btndeleteemail = $('<div class="col-2"><i emailid="'+ cs +'" class="grey pointer far fa-trash-alt delete-invitation" style="padding-top:10px"></i></div>');
                    $('.listemail').append(divaddemail.append(inputemail).append(btndeleteemail));
                    inputemail.select();
                }

            }
        });

        $(document).on('click','.delete-invitation',function(){
            var emailid = $(this).attr('emailid');
            $('#invite'+emailid).remove();
            cs--;
        });

        $('#invite').on('click',function(){
            var invites=[];

            $('[name="memberemail"]').each(function(){
                if($(this).val() != ''){
                    invites.push($(this).val());
                }
            });

            if(invites.length <= 0){
                swal("Masukkan email member yang akan diundang");
            }
            else{
                $(this).attr('disable',true);
                $(this).html('Mengirim...');
                $.ajax({
                    type: "POST",
                    data:{
                        invites: invites
                    },
                    url: '<?php echo base_url()."user/invite_team"; ?>',
                    success: function(data)
                    {
                        if(data == 1)
                        {
                            $('#modalAddMember').modal('hide');
                            swal('Berhasil','Undangan telah dikirim','success');
                            location.reload();
                        }
                        else
                        {
                            $(this).attr('disable',false);
                            $(this).html('kirim invitation');
                            $('#modalAddMember').modal('hide');
                            swal("Oops","Undangan gagal dikirim. Silahkan coba kembali.","error");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        $(this).attr('disable',false);
                        $(this).html('kirim invitation');
                        $('#modalAddMember').modal('hide');
                        swal("Oops","Undangan gagal dikirim. Email sudah Anda undang sebelumnya.","error");
                    }
                });
            }
        });
        <?php } ?>
        // END OF SCRIPT TEAM MEMBER
    });
</script>

<?php if($this->uri->segment(1) == 'user') { ?>
<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="798a792f-1ac9-4623-ae90-3bf3c539b6fa";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
<?php } ?>
