<section id="main-content">
    <section class="wrapper">
        <div class="row" style="margin:0px">
            <div class="container" style="margin:20px;">
                <div class="col-lg-12 col-xs-12 white-panel-abt" style="text-align: left;">
                    <div class="col-lg-7" style="padding: 20px;">
                      <h4>Referral</h4>
                        <h5>Anda masih menggunakan Plan Personal dengan limit hanya 2 proposal. Untuk menambah limit proposal, undang kerabat Anda untuk bergabung menggunakan email:</h5>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="label-control">Masukkan alamat email (bisa input lebih dari 1 email)</label>
                                    <input type="text" class="form-control" name="friend_email" data-role="tagsinput" id="friend_email">
                                  </div>
                              </div>
                              <div class="col-md-12">
                                <button type="button" class="btn btn-primary btn-refer-to-friend"><i class="fa fa-send" aria-hidden="true"></i>
                                  Kirim
                                </button>
                                <span>atau</span>
                                <a href="<?php echo site_url('plan'); ?>" class="btn btn-success btn-priority-access">
                                  Upgrade Plan
                                </a>
                              </div>
                            </div>
                    </div>
                </div>
            </div>
        </div><!-- /row -->
    </section><!-- /wrapper -->
</section><!-- /MAIN CONTENT -->


<script type="application/javascript">
    function ValidateEmail(email) {
              //var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
              var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              return expr.test(email);
      };
    $(document).ready(function () {
        $('.btn-refer-to-friend').on('click',function(){
        if($('#friend_email').val() == '')
        {
          swal("Oops","Email belum diisi","error");
        }
        else
        {
                  var em = $('#friend_email').tagsinput('items');

                  if(em.length > 0)
                  {
                   for(var j=0;j<em.length;j++)
                   {
                      if(!ValidateEmail(em[j]))
                      {
                        swal("Oops","Pastikan format email benar","error");
                        return false;
                      }
                   }
                  }

                  swal({
                    title: "Undangan akan dikirim",
                    text: "Apakah Anda yakin?",
                    icon: "info",
                    buttons: true,
                    dangerMode: true,
                  })
                  .then((kirim) => {
                    if (kirim) {
                      $('.btn-refer-to-friend').attr('disabled',true).html('Mengirim...');
                      $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>home/refer_to_friend",
                        data: {
                            'user_id' : <?php echo $this->session->userdata('user_id');?>,
                            'refer_email' : $('#friend_email').val()
                            },
                        success: function(data){
                          $('.btn-refer-to-friend').attr('disabled',false).html('<i class="fa fa-send" aria-hidden="true"></i> Kirim');
                          if(data == 1)
                          {
                            swal('Ajakan berhasil dikirim');
                            $('#friend_email').val('');
                            $('#friend_email').tagsinput('removeAll');
                          }
                          else if(data == 0)
                          {
                            swal("Info","Ajakan sudah pernah dikirim via email. Coba ajak teman lainnya.","info");
                          }
                          else
                          {
                            swal('Ada kesalahan');
                          }

                        }, error:function(error){
                          swal('Ada kesalahan');
                        }
                      });
                    }
                  });
        }
      });
    });
</script>
