<section id="main-content">
    <section class="wrapper">
        <div class="row" style="margin:0px">
            <div class="container" style="margin:20px;">
                <div class="col-lg-12 col-xs-12 white-panel-abt" style="text-align: left;">
                    <div class="col-lg-6" style="padding: 20px;">
                        <h4>Profil</h4>
                          <div class="form-group text-left">
                            <label for="" class="">Nama Lengkap</label>
                            <input type="text" class="form-control" value="<?php echo $profil->user_name; ?>" id="namaEdit" name="namaEdit" placeholder="Nama Lengkap">
                          </div>
                          <div class="form-group text-left">
                            <label for="" class="">Nama Tim/Perusahaan</label>
                            <input type="text" class="form-control" value="<?php echo ($profil->user_corp == $profil->user_name)?'':$profil->user_corp; ?>" id="perusahaanEdit" name="perusahaanEdit" placeholder="Nama Perusahaan">
                          </div>
                          <div class="form-group text-left">
                            <label for="" class="">Email</label>
                            <input type="email" class="form-control" value="<?php echo $profil->user_email; ?>" id="emailEdit" name="emailEdit" placeholder="Email Resmi" readonly>
                          </div>
                          <div class="form-group text-left">
                            <label for="" class="">Nomor Handphone</label>
                            <input type="text" class="form-control" value="<?php echo $profil->user_mobile; ?>" id="mobileEdit" onkeypress="var key = event.keyCode || event.charCode; return ((key >= 48 && key <= 57) || key == 8);" name="mobileEdit" placeholder="Nomor Handphone">
                          </div>
                          <div class="form-group text-left">
                            <label for="" class="">Kota Domisili</label>
                            <input type="text" class="form-control" value="<?php echo $profil->kota; ?>" id="kotaEdit" name="kotaEdit" placeholder="Kota">
                          </div>
                          <div class="form-group text-left">
                            <label for="" class="">Alamat</label>
                            <textarea class="form-control" value="<?php echo $profil->alamat; ?>" id="alamatEdit" name="alamatEdit"></textarea>
                          </div>
                          <div class="form-group">
                            <button type="button" class="btn btn-theme pull-right" id="btnsimpanprofil">Simpan</button>
                          </div>
                    </div>
                    <div class="col-lg-6" style="padding: 20px;">
                          <div class="row">
                            <div class="col-lg-12">
                              <h4>Logo</h4>
                              <div class="col-md-4" style="margin:5px auto;">
                                            <div style="width: 100px; height: 100px; text-align: center;<?php if($logo) { if($logo->team_logo == NULL || $logo->team_logo == ''){ echo 'border: solid 1px;'; }else{ echo 'background:url('.$logo->team_logo.')center center no-repeat;background-size: 100%;';  }} ?>">
                                              <?php
                                                if($logo)
                                                  {
                                                    if($logo->team_logo == NULL || $logo->team_logo == '')
                                                    {
                                                      echo '<i style="font-size: 20px;margin-top: 40%;" class="fa fa-image"></i><br>No Logo';
                                                    }
                                                  }
                                                  else
                                                  {
                                                    echo '<i style="font-size: 50px;margin-top: 40%;" class="fa fa-image"></i><br> No Logo';
                                                  }
                                                ?>
                                            </div>
                              </div>
                              <div class="col-md-6">
                                <div style="margin:30px auto;">
                                                <?php echo form_open_multipart('user/upload_image_logo',array('id' => 'form_upload_team_logo'));?>
                                                <div class="input-group" style="width:200px;margin: auto;">
                                                    <span class="input-group-btn">
                                                        <span class="btn btn-default btn-file">
                                                            <i class="fa fa-image"></i> <input type="file" name="file" id="imglg">
                                                        </span>
                                                    </span>
                                                    <input type="text" class="form-control" readonly>
                                                    <span class="input-group-btn">
                                                      <button class="btn btn-save-team-logo-image" disabled="true" type="button">Upload</button>
                                                    </span>
                                                </div>
                                                </form>
                                              </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                                <h4>Tim</h4>
                                <?php if($this->session->userdata('role') == 1){ ?>
                                  <button id="add-member" class="btn pull-right">Tambah Member</button>
                                <?php } ?>
                                <table class="table">
                                  <thead>
                                    <th>Nama</th>
                                    <th width="150">Role</th>
                                    <th>Status</th>
                                    <th></th>
                                  </thead>
                                  <tbody>
                                    <?php foreach ($tim as $tm) {?>
                                      <tr>
                                        <td><?php echo $tm->user_name == $this->session->userdata('user_name') ? 'Anda':$tm->user_name; ?></td>
                                        <td>
                                          <?php if($tm->role == 0){ ?>
                                            <span> - </span>
                                          <?php }else if($tm->role == 1){ ?>
                                            <span class="label label-success">Baca</span>
                                            <span class="label label-info">Edit</span>
                                            <span class="label label-primary">Admin</span>
                                          <?php }else if($tm->role == 2){ ?>
                                            <span class="label label-success">Baca</span>
                                            <span class="label label-info">Edit</span>
                                          <?php }else if($tm->role == 3){ ?>
                                            <span class="label label-success">Baca</span>
                                          <?php } ?>
                                        </td>
                                        <td><?php echo $tm->role == 0 ?'Pending':'Aktif'; ?></td>
                                        <td>&nbsp;</td>
                                      </tr>
                                    <?php } ?>
                                  </tbody>
                                </table>
                            </div>
                          </div>
                        <input hidden id="hiddenId" >
                    </div>
                </div>
            </div>
        </div><!-- /row -->
    </section><!-- /wrapper -->
</section><!-- /MAIN CONTENT -->

<!-- <!-MODAL ADD CLIENT-->
              <div class="modal" id="modalmember" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" >
                <div class="modal-dialog" style="width:400px;">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title" id="">Undang member baru</h4>
                    </div>
                    <div class="modal-body" >
                      <div class="form-group text-left listemail">
                        <label for="" class="">Masukan email member yang akan diundang dan kami akan mengirimkan mereka email.</label>
                        <div id="invite">
                            <input style="float: left;width: 85%;margin-right: 20px;box-shadow: unset;background-color: transparent;border-width: 0px;border-bottom: 1px solid #000;border-radius: 0px;" id="inputmemberemail1" name="memberemail" type="text" class="form-control memberemail" value="" placeholder="Email" maxlength="50">
                            <button style="margin-top: 6px;padding: 3px 7px;font-size: 11px;border-radius: 3px;" type="button" class="btn btn-default btn-sm addemail"><i class="fa fa-plus"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-theme03" id="btntambah">Undang</button>
                    </div>
                  </div>
                </div>
              </div>
            <!-- END OF MODAL ADD CLIENT-->
