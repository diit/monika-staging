<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();

        function ValidateEmail(email) {
            var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return expr.test(email);
        };

        function isEmpty(el){
            return !$.trim(el.html())
        }

        $('#klien_proposal_baru').on('change',function(){
            var value = $('#klien_proposal_baru').val();
            if(value == 1){
                $('#create-new-client').collapse('show');
            }else{
                $('#create-new-client').collapse('hide');
            }
        });

        $(document).on('click','#new-proposal-baru',function(){
            var klien = $('#klien_proposal_baru').val();
            var user = '<?php echo $this->session->userdata('user_id');?>';
            var projekBaru = $('#new-nama-projek').val();
            var overviewBaru = $('#new-overview-proposal').val();

            if (projekBaru == ''){
                $('#project-msg').empty().append("<div class='alert alert-danger text-little'>Nama Proyek atau proposal wajib diisi</div>");
            }
            else if (klien == 0){
                $('#client-msg').empty().append("<div class='alert alert-danger text-little'>Klien wajib dipilih</div>");
            }
            else if (overviewBaru ==''){
                $('#overview-msg').empty().append("<div class='alert alert-danger text-little'>Overview proposal wajib diisi</div>");
            }
            else if ((projekBaru !='') && (klien !=0) && (overviewBaru !='')){
                document.getElementById('new-proposal-baru').innerHTML = 'Membuat . . .';
                $('#new-proposal-baru').attr('disabled','true');

                //new proposal with new client
                if (klien == 1){
                    var pic =  $('#pic').val();
                    var perusahaan =  $('#perusahaan').val();
                    var email =  $('#email').val();
                    var telephone =  $('#telephone').val();
                    var alamat =  $('#alamat').val();
                    var kota =  $('#kota').val();
                    if(pic==''||perusahaan==''||email==''||telephone==''){
                        swal("Oops","Pastikan Anda telah melengkapi isian","error");
                        return false;
                    }
                    else if (!ValidateEmail(email)){
                        swal("Oops","Format email masih salah","error");
                        return false;
                    }
                    else{
                        $.ajax({
                            type: "POST",
                            url: "<?=base_url()?>proposal/newProposalNewClient",
                            data: {
                                pic:pic,
                                perusahaan:perusahaan,
                                email:email,
                                telephone:telephone,
                                alamat:alamat,
                                kota:kota,
                                user_id:user,
                                projek : projekBaru,
                                overviewbaru : overviewBaru
                            },
                            datatype: 'json',
                            success: function(data){
                                var datanya = JSON.parse(data);
                                console.log('ID proposal baru = '+datanya);
                                $('#proposalModal').modal('hide');
                                location.href = '<?php echo site_url('proposal/viewproposal/'); ?>' + datanya;
                            },
                            error:function(error){
                                console.log(error);
                            }
                        });
                    }
                }
                //end of new proposal with new client

                //new proposal client choosen
                else{
                    $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>proposal/newproposaltoDB",
                        data: {
                            user : user,
                            projek : projekBaru,
                            overviewbaru : overviewBaru,
                            klien : klien,
                        },
                        datatype: 'json',
                        success: function(data){
                            var datanya = JSON.parse(data);
                            console.log('ID proposal baru = '+datanya);
                            $('#proposalModal').modal('hide');
                            location.href = '<?php echo site_url('proposal/viewproposal/'); ?>' + datanya;
                        }, error:function(error){
                            console.log(error);
                        }
                    });
                }
                //end of new proposal client choosen
            }
        });

        var chartlabel = [];
        var datachart = [];
        var bgchart = [];

         <?php
         if(count($top_read)>0)
         {
             foreach ($top_read as $top)
             {
                 $hash = md5('color' . $top->id); // modify 'color' to get a different palette
                   list($r,$g,$b) = array(
                       hexdec(substr($hash, 0, 2)), //r
                       hexdec(substr($hash, 2, 2)), //g
                       hexdec(substr($hash, 4, 2)) //b
                   );
         ?>
             chartlabel.push('<?= addslashes($top->judul) ?> - <?= $top->nama_pic ?>');
             datachart.push('<?= $top->jml ?>');
             bgchart.push('<?php echo 'rgb('.$r.','.$g.','.$b.')'; ?>');
         <?php
             }
         }
         else
         {
         ?>
            chartlabel.push('Belum ada data tracking');
            datachart.push('1');
            bgchart.push('rgb(70, 191, 178)');
         <?php } ?>

        var ctx = document.getElementById("myChart").getContext('2d');

        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: datachart,
                    backgroundColor: bgchart,
                    borderColor: ['#fff']
                }],
                labels: chartlabel
            },
            options: {
                legend: {
                    display: false,
                    labels: {
                        fontFamily: 'Avenir',
                        fontColor: '#111111'
                    },
                    onClick: (e) => e.stopPropagation()
                }
            }
        });

        document.getElementById('js-legend').innerHTML = myChart.generateLegend();

        var linelabel = [];
        var dataline = [];

        <?php
        if(count($time_read) > 2) {
            foreach ($time_read as $tr) {
        ?>
                linelabel.push('<?php echo $tr->hour.'.00'; ?>');
                dataline.push('<?php echo $tr->jml; ?>');
        <?php } 
        }
        else { ?>
            var linelabel = ['1.00','4.00','7.00','12.00','13.00','14.00'];
            var dataline = [2,3,2,5,1,3]; 
        <?php } ?>

        <?php if(($this->monikalib->currentUserPlan()==0) && (count($time_read) > 2)) { ?>
            var bg = 'transparent';
            var border = 'transparent';
        <?php } else { ?>
            var bg = 'rgba(79,78,161,0.4)';
            var border = '#4f4ea1';
        <?php } ?>

        var ctx = document.getElementById("myLineChart");
        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: linelabel,
                datasets: [{
                    data: dataline,
                    backgroundColor: bg,
                    borderColor: border
                }],
            },
            options: {
                legend: {
                    display: false
                },
                elements: {
                    line: {
                        tension: 0.3,
                    }
                },
                scales: {
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    });
</script>
