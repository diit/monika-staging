<div id="content">

    <div class="row">
        <!-- most read -->
        <div class="col-md-4">
            <div class="box" id="chart-proposal">
                <p class="box-title">PALING BANYAK DIBACA</p>

                <canvas id="myChart"></canvas>
                <div id="js-legend" class="chart-legend"></div>
            </div>
        </div>
        <!-- end of most read -->

        <!-- statistic content -->
        <div class="col-md-5">
            <div class="box" id="stat-content">
                <p class="box-title">STATISTIK KONTEN</p>
                <table class="table dashboard">
                    <tr class="text-center">
                        <th>Konten</th>
                        <th>Rata-rata Lama Membaca <br>(detik)</th>
                    </tr>

                    <?php
                    if(count($avg_readtime) > 0) {
                        foreach ($avg_readtime as $d) { ?>
                    <tr>
                        <td><?= $d->sec_judul ?></td>
                        <td class="text-center"><?= $d->lama ?></td>
                    </tr>
                    <?php } }
                    else { ?>
                    <tr>
                        <td colspan="2" class="text-center grey" style="padding-top:40px">Belum ada dokumen yang dibuat atau dikirim</td>
                    </tr>
                    <?php } ?>
                </table>
                <?php if($this->monikalib->currentUserPlan()==0) { ?>
                &nbsp;
                <p class="text-center"><a href="<?=site_url('plan')?>" class="btn btn-outline-primary btn-small text-small">upgrade plan untuk tahu lebih lengkap</a></p>
                <?php } ?>
            </div>
        </div>
        <!-- end of statistic content -->

        <div class="col-md-3">
            <a class="btn btn-outline-primary btn-block" <?= (($this->monikalib->currentUserPlan()==0) && ($this->monikalib->limitDoc()=='personal') && ($this->monikalib->getTotalProposal()>=2)) ? 'data-toggle="tooltip" data-placement="left" title="Dokumen limit. Upgrade plan untuk buat proposal"' : 'data-toggle="modal" data-target="#proposalModal"' ?> >BUAT PROPOSAL</a>
            <a <?= (($this->monikalib->currentUserPlan()==0) && ($this->monikalib->limitDoc()=='personal') && ($this->monikalib->getTotalInvoice()>=2)) ? 'data-toggle="tooltip" data-placement="left" title="Dokumen limit. Upgrade plan untuk buat invoice"' : 'href="'. site_url('invoice/create').'"' ?>  class="btn btn-outline-primary btn-block">BUAT INVOICE</a>

            <!-- summary document -->
            <div class="box top-space">
                <table class="table no-border" id="report">
                    <tr>
                        <td style="width:50%">
                            <p class="text-little">Rata-rata proposal dibaca pada pukul</p>
                        </td>
                        <td>
                            <?php if($this->monikalib->currentUserPlan()==0) { ?>
                            <p class="hi-light text-right"><a href="<?=site_url('plan')?>"><i class="fas fa-info-circle" data-toggle="tooltip" data-placement="left" title="Upgrade plan untuk melihat"></i></a></p>
                            <?php } else { ?>
                            <p class="hi-light text-right"><?= ((empty($best_hour_read->hour))&&(empty($best_minute_read->minute))) ? '-' : $best_hour_read->hour.'.'.$best_minute_read->minute ?></p>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="text-little">Rata-rata proposal dibaca sebanyak</p>
                        </td>
                        <td>
                            <?php if($this->monikalib->currentUserPlan()==0) { ?>
                            <p class="hi-light text-right"><a href="<?=site_url('plan')?>"><i class="fas fa-info-circle" data-toggle="tooltip" data-placement="left" title="Upgrade plan untuk melihat"></i></a></p>
                            <?php } else { ?>
                            <p class="hi-light text-right"><?= ((empty($avg_read->rerata)) || ($avg_read->rerata == 0)) ? '0' : $avg_read->rerata ?>x</p>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="text-little">Estimasi income</p>
                        </td>
                        <td>
                            <p class="text-little text-right">-</p>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- end of summary document -->
        </div>

        <!-- graphic content -->
        <div class="col-md-6">
            <div class="box top-space">
                <p class="box-title">WAKTU SAAT CLIENT ANDA MEMBACA PROPOSAL</p>
                &nbsp;
                <canvas id="myLineChart" height="120px"></canvas>
                <?php if(count($time_read) <= 2) { ?>
                <div id="dummy-read">
                    <p class="text-center">Belum ada data yang diolah. <br> Graphic show with data dummy</p>
                </div>
                <?php } elseif($this->monikalib->currentUserPlan()==0) { ?>
                <div id="dummy-read">
                    <p class="text-center"><a href="<?=site_url('plan')?>" class="btn btn-outline-primary btn-small text-small">upgrade plan untuk tahu lebih lengkap</a></p>
                </div>
                <?php } ?>
            </div>
        </div>
        <!-- end of graphic content -->

        <!-- top 5 proposal -->
        <div class="col-md-3">
            <div class="box top-space">
                <p class="box-title">PROPOSAL</p>
                <?php if(count($top_proposal) > 0){ ?>
                <ul class="no-point text-little document">
                    <?php
                    foreach ($top_proposal as $d) {
                        $random = md5(mt_rand(1,10000));
                        $first = substr($random,0,5);
                        $last = substr($random,5,10);
                        $urlrand = $first.$d->id.$last;
                    ?>
                    <li><a href="<?= site_url('proposal/viewproposal/'.$urlrand) ?>"><span class="bold"><?= $d->judul ?></span> - <?= $d->nama_pic ?></a></li>
                    <?php } ?>
                </ul>
                <?php }
                else { ?>
                <div class="text-center">
                    &nbsp;
                    <h2><i class="far fa-file-alt grey"></i></h2>
                    <p class="text-center grey text-little">Belum ada Proposal yang dibuat</p>
                    <p class="text-center"><a href="<?=site_url('proposal')?>" class="btn btn-outline-primary btn-little">Buat Sekarang</a></p>
                </div>
                <?php } ?>
            </div>
        </div>
        <!-- end of top 5 proposal -->

        <!-- top 5 invoice -->
        <div class="col-md-3">
            <div class="box top-space">
                <p class="box-title">INVOICE</p>

                <?php if(count($top_invoice) > 0){ ?>
                <ul class="no-point text-little document">
                    <?php
                    foreach($top_invoice as $d){
                        $random = md5(mt_rand(1,10000));
                        $first = substr($random,0,5);
                        $last = substr($random,5,10);
                        $urlrand = $first.$d->id.$last;
                    ?>
                    <li><a href="<?php echo site_url('invoice/detail/'.$urlrand);?>"><span class="bold">
                        <?php echo (empty($d->judul)) ? 'Invoice' : $d->judul ?>
                        </span> - <?= $d->nama_pic ?></a></li>
                    <?php } ?>
                </ul>
                <?php }
                else { ?>
                <div class="text-center">
                    &nbsp;
                    <h2><i class="fab fa-wpforms grey"></i></h2>
                    <p class="text-center grey text-little">Belum ada Invoice yang dibuat</p>
                    <p class="text-center"><a href="<?=site_url('invoice')?>" class="btn btn-outline-primary btn-little">Buat Sekarang</a></p>
                </div>
                <?php } ?>
            </div>
        </div>
        <!-- end of top 5 proposal -->
    </div>
</div>

<!-- Modal buat Proposal-->
<div class="modal fade" id="proposalModal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center">Deskripsi dan Info Proyek Proposal</h5>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-sm-6 form-group">
                    <label>Nama Proyek</label>
                    <input class="form-control" type="text" required id="new-nama-projek">
                    <div id="project-msg"></div>
              </div>
              <div class="col-sm-6 form-group">
                    <label>Klien</label>
                    <select class="form-control" name="klien_proposal_baru" id="klien_proposal_baru" style="width:100%">
                        <option value="0">Pilih client</option>
                        <option value="1">Client Baru</option>
                        <?php foreach ($user_client as $row) {?>
                        <option value="<?php echo $row->id_client;?>"><?php echo $row->nama_pic;?> | <?php echo $row->perusahaan;?></option>
                        <?php } ?>
                    </select>
                    <div id="client-msg"></div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-12">
                <div class="collapse mb-3" id="create-new-client">
                    <div class="card card-body">
                        <div class="row justify-content-center">
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-user"></i></div>
                                    </div>
                                    <input type="text" class="form-control" id="pic" name="pic" placeholder="Nama PIC">
                                </div>
                                &nbsp;
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-briefcase"></i></div>
                                    </div>
                                    <input type="text" class="form-control" id="perusahaan" name="perusahaan" placeholder="Nama Tim/Perusahaan (optional)">
                                </div>
                                &nbsp;
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-envelope"></i></div>
                                    </div>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email Resmi">
                                </div>
                                &nbsp;
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-phone"></i></div>
                                    </div>
                                    <input type="text" class="form-control" id="telephone" onkeypress="var key = event.keyCode || event.charCode; return ((key >= 48 && key <= 57) || key == 8);" name="telephone" placeholder="Nomor Telephone">
                                </div>
                                &nbsp;
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-map-marked-alt"></i></div>
                                    </div>
                                    <textarea class="form-control" rows="3" id="alamat" name="alamat" placeholder="Alamat klien/perusahaan"></textarea>
                                </div>
                                &nbsp;
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-map-marker-alt"></i> </div>
                                    </div>
                                    <input type="text" class="form-control" id="kota" name="kota" placeholder="Kota/kabupaten">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="row">
              <div class="col-sm-12 form-group">
                  <label>Deskripsi Proposal</label>
                  <div id="overview-msg"></div>
                  <textarea class="form-control no-resize" rows="4" id="new-overview-proposal"></textarea>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="new-proposal-baru">buat proposal</button>
      </div>
    </div>
  </div>
</div>
<!-- end of modal buat proposal -->
