<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>

<script>
    $(document).ready(function() {
        $('#history').hide();
        $('.view-history').click(function() {
            $('#history').toggle();
        });

        var chartlabel = [];
        var datachart = [];
        var bgchart = [];
        <?php
        if(count($top_read) > 0)
        {
            foreach ($top_read as $top)
            {
                $hash = md5('color' . $top->id); // modify 'color' to get a different palette
                  list($r,$g,$b) = array(
                      hexdec(substr($hash, 0, 2)), //r
                      hexdec(substr($hash, 2, 2)), //g
                      hexdec(substr($hash, 4, 2)) //b
                  );
        ?>
            chartlabel.push('<?= addslashes($top->judul) ?>');
            datachart.push('<?= $top->jml ?>');
            bgchart.push('<?php echo 'rgb('.$r.','.$g.','.$b.')'; ?>');
        <?php
            }
        }
        else
        {
        ?>
            chartlabel.push('Belum ada data tracking');
            datachart.push('1');
            bgchart.push('rgb(70, 191, 178)');
        <?php } ?>

        var ctx = document.getElementById("myChartMobile").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: datachart,
                    backgroundColor: bgchart,
                    borderColor: ['#fff']
                }],
                labels: chartlabel
            },
            options: {
                legend: {
                    display: false,
                    labels: {
                        fontFamily: 'Avenir',
                        fontColor: '#111111'
                    },
                    onClick: (e) => e.stopPropagation()
                }
            }
        });

        document.getElementById('js-legend-mobile').innerHTML = myChart.generateLegend();

        var linelabel = [];
        var dataline = [];

        <?php
        if(count($time_read) > 2) {
            foreach ($time_read as $tr) {
        ?>
                linelabel.push('<?php echo $tr->hour.'.00'; ?>');
                dataline.push('<?php echo $tr->jml; ?>');
        <?php }
        }
        else { ?>
            var linelabel = ['1.00','4.00','7.00','12.00','13.00','14.00'];
            var dataline = [2,3,2,5,1,3];
        <?php } ?>

        <?php if(($this->monikalib->currentUserPlan()==0) && (count($time_read) > 2)) { ?>
            var bg = 'transparent';
            var border = 'transparent';
        <?php } else { ?>
            var bg = 'rgba(79,78,161,0.4)';
            var border = '#4f4ea1';
        <?php } ?>

        var ctx = document.getElementById("myLineChartMobile");
        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: linelabel,
                datasets: [{
                    data: dataline,
                    backgroundColor: bg,
                    borderColor: border
                }],
            },
            options: {
                legend: {
                    display: false
                },
                elements: {
                    line: {
                        tension: 0.3,
                    }
                },
                scales: {
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    });
</script>
