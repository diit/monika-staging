<div id="content">
    <div class="row">
        <!-- graphic content -->
        <div class="col-12">
            <div class="box">
                <p class="box-title">waktu saat klien anda membaca proposal</p>

                <canvas id="myLineChartMobile" height="130px;"></canvas>
                <?php if(count($time_read) <= 2) { ?>
                <div id="dummy-read">
                    <p class="text-center text-little">Belum ada data yang diolah. <br> Graphic show with data dummy</p>
                </div>
                <?php } elseif($this->monikalib->currentUserPlan()==0) { ?>
                <div id="dummy-read">
                    &nbsp;
                    <p class="text-center"><a href="<?=site_url('plan')?>" class="btn btn-outline-primary btn-small text-small">upgrade plan untuk tahu lebih lengkap</a></p>
                </div>
                <?php } ?>
            </div>
        </div>
        <!-- end of graphic content -->

        <!-- most read -->
        <div class="col-7">
            <div class="box top-space">
                <p class="box-title">PALING BANYAK DIBACA</p>

                <canvas id="myChartMobile"></canvas>
                <div id="js-legend-mobile" class="chart-legend"></div>
            </div>
        </div>
        <!-- end of most read -->

        <!-- summary -->
        <div class="col-5">
            <div class="box top-space" id="summary">
                <p class="read">Rata-rata proposal dibaca pada pukul</p>
                <?php if($this->monikalib->currentUserPlan()==0) { ?>
                <p class="big"><a href="<?=site_url('plan')?>"><i class="fas fa-info-circle"></i></a></p>
                <?php } else { ?>
                <p class="big"><?= ((empty($best_hour_read->hour))&&(empty($best_minute_read->minute))) ? '-' : $best_hour_read->hour.'.'.$best_minute_read->minute ?></p>
                <?php } ?>

                <p class="read">Rata-rata proposal dibaca sebanyak</p>
                <?php if($this->monikalib->currentUserPlan()==0) { ?>
                <p class="big"><a href="<?=site_url('plan')?>"><i class="fas fa-info-circle"></i></a></p>
                <?php } else { ?>
                <p class="big"><?= ((empty($avg_read->rerata)) || ($avg_read->rerata == 0)) ? '0' : $avg_read->rerata ?>x</p>
                <?php } ?>

                <p class="read">Estimasi income</p>
                <p class="read bold">-</p>
            </div>
        </div>
        <!-- end of summary -->

        <!-- statistic content -->
        <div class="col-12">
            <div class="box top-space">
                <p class="box-title">STATISTIK KONTEN</p>
                <table class="table dashboard">
                    <tr class="text-center">
                        <th>Konten</th>
                        <th>Rata-rata Lama Membaca <br>(detik)</th>
                    </tr>

                    <?php
                    if(count($avg_readtime) > 0) {
                        foreach ($avg_readtime as $d) { ?>
                    <tr>
                        <td><?= $d->sec_judul ?></td>
                        <td class="text-center">
                            <?= $d->lama ?>
                        </td>
                    </tr>
                    <?php } }
                    else { ?>
                    <tr>
                        <td colspan="2" class="text-center grey" style="padding-top:40px">Belum ada dokumen yang dibuat atau dikirim</td>
                    </tr>
                    <?php } ?>
                </table>
                <?php if($this->monikalib->currentUserPlan()==0) { ?>
                &nbsp;
                <p class="text-center"><a href="<?=site_url('plan')?>" class="btn btn-outline-primary btn-small text-small">upgrade plan untuk tahu lebih lengkap</a></p>
                <?php } ?>
            </div>
        </div>
        <!-- end of statistic content -->
    </div>
</div>
