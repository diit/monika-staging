<div class="row justify-content-center" id="policy">
    <div class="col-11">
        <div class="box">
            <div class="row justify-content-center">
                <div class="col-11 text-little">
                    <a href="<?= site_url('register') ?>" class="btn btn-primary" id="btn-register"><i class="fas fa-angle-left"></i> register</a>

                    <h4> Syarat dan Ketentuan</h4><br>
                    <p>Mohon dibaca dengan teliti sebelum menggunakan layanan dari <a href="https://monika.id" target="_blank">monika.id</a> (“Layanan”,”Kami”). Dengan mendaftar akun di monika.id berarti Anda secara sukarela untuk terikat dengan persyaratan dan ketentuan layanan ini, semua undang-undang dan peraturan yang berlaku, dan setuju bahwa Anda bertanggung jawab untuk mematuhi undang-undang yang berlaku. Jika Anda tidak menyetujui persyaratan dan ketentuan dari kami. Anda tidak diperbolehkan mengakses layanan dari kami.</p>
                    <p>Monika.id adalah website yang menyediakan layanan untuk membuat proposal (penawaran) dan invoice serta melakukan pelacakan ketika proposal dan invoice dikirimkan ke pihak yang dituju. Melalui layanan terkait (secara keseluruhan, layanan tersebut, termasuk fitur) tunduk pada Syarat dan Ketentuan berikut (sebagaimana yang telah diubah dari waktu ke waktu, “Ketentuan Layanan”). Kami berhak, atas pertimbangan kami sendiri, untuk mengubah atau memodifikasi sebagian dari Ketentuan Layanan ini kapan saja. Jika kami melakukan ini, kami akan mengeposkan perubahan pada halaman ini dan akan menunjukkan di bagian atas halaman ini tanggal persyaratan ini terakhir direvisi. Kami juga akan memberi tahu Anda, baik melalui antarmuka pengguna Layanan, dalam pemberitahuan email atau melalui cara masuk akal lainnya.</p>
                    <div id="more">
                    <p>Dengan mengakses atau menggunakan Layanan, Anda setuju untuk terikat oleh Persyaratan ini. Jika Anda tidak setuju dengan bagian dari persyaratan mana pun, Anda mungkin tidak dapat mengakses Layanan.</p>
                    <h5>KONTEN</h5>
                    <p>Kami memiliki hak kekayaan intelektual dari setiap dan semua komponen layanan yang dapat dilindungi termasuk konten otomatis yang dibuat oleh monika.id.</p>
                    <p>Kami tidak bertanggung jawab atas kerusakan langsung, tidak langsung atau konsekuensial yang timbul dari atau terkait dengan penggunaan Anda atas layanan ini.</p>
                    <p>Layanan kami memungkinkan Anda mengeposkan, menautkan, menyimpan, berbagi, dan menyediakan informasi, teks, grafik, atau materi lain ("Konten") tertentu. Anda bertanggung jawab atas Konten yang Anda kirim ke Layanan, termasuk legalitas, keandalan, dan kesesuaiannya.</p>
                    <p>Dengan mengeposkan Konten ke Layanan, Anda memberi kami hak dan lisensi untuk menyimpan dan menampilkan (ke tujuan yang Anda tentukan) Konten semacam itu di dan melalui Layanan. Anda menyimpan setiap dan semua hak Anda atas Konten yang Anda kirimkan, poskan atau tampilkan pada atau melalui Layanan dan Anda bertanggung jawab untuk melindungi hak-hak tersebut.</p>
                    <p>Anda menyatakan dan menjamin bahwa: (i) Konten itu milik Anda (Anda memilikinya) atau Anda memiliki hak untuk menggunakannya dan memberi kami hak dan lisensi sebagaimana ditentukan dalam Persyaratan ini, dan (ii) pengeposan Konten Anda atau melalui Layanan tidak melanggar hak privasi, hak publisitas, hak cipta, hak kontrak atau hak lainnya dari orang mana pun.</p>
                    <h5>AKUN PENGGUNA</h5>
                    <p>Bila Anda membuat akun bersama kami, Anda harus memberi kami informasi yang akurat, lengkap, dan terkini setiap saat. Kegagalan untuk melakukannya merupakan pelanggaran Persyaratan, yang dapat mengakibatkan penghentian segera akun Anda pada Layanan kami.</p>
                    <p>Anda bertanggung jawab untuk melindungi kata sandi yang Anda gunakan untuk mengakses Layanan dan untuk aktivitas atau tindakan apa pun di bawah kata sandi Anda, apakah kata sandi Anda dengan Layanan atau layanan pihak ketiga kami.</p>
                    <p>Anda setuju untuk tidak mengungkapkan kata sandi Anda kepada pihak ketiga manapun. Anda harus segera memberi tahu kami saat mengetahui adanya pelanggaran keamanan atau penggunaan akun Anda yang tidak sah.</p>
                    <p>Anda mungkin tidak menggunakan nama pengguna nama orang atau entitas lain atau yang tidak tersedia secara sah untuk digunakan, nama atau merek dagang yang tunduk pada hak orang atau entitas lain selain Anda tanpa otorisasi yang sesuai, atau nama yang sebaliknya menyinggung, vulgar atau cabul.</p>
                    <h5>LAYANAN KAMI DAN LANGGANAN BERBAYAR</h5>
                    <p>monika.id menyediakan layanan membuat proposal dan invoice. Layanan monika.id tertentu diberikan kepada Anda secara gratis. Layanan monika.id lainnya memerlukan pembayaran sebelum Anda mengaksesnya. Layanan monika yang dapat diakses setelah pembayaran disebut sebagai “Layanan Personal”, “Layanan Agency” dan “Layanan Enterprise”. Layanan monika.id yang tidak memerlukan pembayaran hanya berlaku untuk 2 kali pembuatan dokumen.</p>
                    <h5>PEMBAYARAN, PEMBATALAN DAN MASA TUNGGU</h5>
                    <p>Langganan berbayar pada dapat dibeli dengan cara (1) membayar biaya berlangganan bulanan; atau (2) membayaran secara kontrak tahunan; untuk “Layanan Enterprise” melalui penawaran oleh sales secara negosiasi.</p>
                    <p>Anda dapat mengubah pilihan Anda untuk atau tanpa alasan apapun dan menerima pengembalian dana secara penuh dari semua uang yang dibayarkan dalam waktu 14 hari (“Periode Tunggu”). Pengembalian uang tidak akan, bagaimanapun, diberikan kepada Anda jika Anda mengakses layanan monika.id pada periode Masa Tunggu.</p>
                    <p>Langganan berbayar Anda yang akan berakhir akan diingatkan seminggu sebelum masa berlangganan habis.</p>
                    <h5>KEAMANAN DAN PENGENDALIAN</h5>
                    <p>Akun, password dan keamanan - Jika Anda telah diberikan pilihan untuk membuka akun di Situs Web ini yang akan memberikan Anda akses ke bagian-bagian tertentu yang dilindungi oleh password di situs ini dan Anda memilih untuk melakukannya, Anda harus melengkapi proses pendaftaran dengan memberikan kepada kami informasi yang terbaru, lengkap dan akurat sebagaimana yang diminta di dalam formulir pendaftaran tersebut, dan memilih password dan nama pengguna. Anda sepenuhnya bertanggung jawab untuk menjaga kerahasiaan password dan akun dan untuk setiap dan semua aktivitas yang terjadi dalam akun Anda. Anda setuju untuk (a) secepatnya memberitahu Pemilik jika akun Anda digunakan tanpa seizin Anda atau jika Anda mengetahui adanya pelanggaran terhadap keamanan, dan (b) benar-benar keluar dari akun di akhir setiap sesi online. Pemilik tidak akan bertanggung jawab atas kerugian yang mungkin timbul sebagai akibat dari password atau akun Anda digunakan orang lain, baik dengan ataupun tanpa sepengetahuan Anda. Meskipun demikian, Anda dapat dimintai pertanggungjawaban atas kerugian yang diderita oleh Pemilik atau pihak lain yang disebabkan penggunaan akun atau password Anda oleh orang lain. Anda dilarang menggunakan akun orang lain kapanpun, dengan atau tanpa seizin pemilik akun.</p>
                    <p>Dokumen yang telah Anda buat dalam proses pembuatan proposal telah dilindungi dengan cara enkripsi. Enkripsi monika.id memastikan bahwa hanya Anda yang berhak mengakses dokumen-dokumen dalam website. Dokumen Anda diamankan dengan sebuah kunci dan hanya Anda dan klien Anda saja yang dapat memiliki kunci spesial untuk dapat mengakses setiap dokumen yang Anda kirim. Keamanan digital menjadi sangat penting bagi monika.id agar tidak terjadi hal yang tidak diinginkan melalui kriminal digital.</p>
                    <h5>LINK KE WEBSITE LAIN</h5>
                    <p>Layanan kami mungkin berisi tautan ke situs web pihak ketiga atau layanan yang tidak dimiliki atau dikendalikan oleh kami.</p>
                    <p>monika.id tidak memiliki kendali atas, dan tidak bertanggung jawab atas, konten, kebijakan privasi, atau praktik dari situs web atau layanan pihak ketiga manapun. Anda selanjutnya mengetahui dan setuju bahwa monika.id tidak bertanggung jawab atau bertanggung jawab secara langsung atau tidak langsung atas kerusakan atau kerugian yang disebabkan atau diduga disebabkan oleh atau sehubungan dengan penggunaan atau kepercayaan atas konten, barang atau layanan yang tersedia pada atau melalui situs web atau layanan semacam itu.</p>
                    <p>Kami sangat menyarankan Anda untuk membaca syarat dan ketentuan dan kebijakan privasi dari situs web pihak ketiga atau layanan yang Anda kunjungi.</p>
                    <h5>PERUBAHAN</h5>
                    <p>Kami berhak, atas pertimbangan kami sendiri, untuk mengubah atau mengganti persyaratan ini kapan saja. Jika ada revisi materi, kami akan melakukan pemberitahuan setidaknya 30 hari sebelum persyaratan baru mulai berlaku. Apa yang merupakan material akan ditentukan atas kebijakan kami.</p>
                    <p>Dengan terus mengakses atau menggunakan Layanan kami setelah revisi tersebut menjadi efektif, Anda setuju untuk terikat oleh persyaratan yang telah direvisi. Jika Anda tidak menyetujui persyaratan baru, mohon berhenti menggunakan Layanan.</p>
                    <h5>HUBUNGI KAMI</h5>
                    <p>Jika Anda memiliki pertanyaan seputar syarat dan ketentuan yang berlaku, Anda bisa menghubungi kami melalui alamat email sebagai berikut: <a href="mailto:info@monika.id">info@monika.id</a></p>
                    </div>

                </div>
                <a id="read-more">Baca lebih lengkap ...</a>
            </div>
        </div>
    </div>
</div>

<ul id="medsos">
    <a href="https://www.facebook.com/usemonika" target="_blank">
        <li>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                <path id="fb" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z    M67.521,24.89l-6.76,0.003c-5.301,0-6.326,2.519-6.326,6.215v8.15h12.641L67.07,52.023H54.436v32.758H41.251V52.023H30.229V39.258   h11.022v-9.414c0-10.925,6.675-16.875,16.42-16.875l9.851,0.015V24.89L67.521,24.89z" fill="#4f4ea1"/>
            </svg>
        </li>
    </a>
    <a href="https://www.instagram.com/usemonika/" target="_blank">
        <li>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                <path id="in" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z    M30.562,81.966h-13.74V37.758h13.74V81.966z M23.695,31.715c-4.404,0-7.969-3.57-7.969-7.968c0.001-4.394,3.565-7.964,7.969-7.964   c4.392,0,7.962,3.57,7.962,7.964C31.657,28.146,28.086,31.715,23.695,31.715z M82.023,81.966H68.294V60.467   c0-5.127-0.095-11.721-7.142-11.721c-7.146,0-8.245,5.584-8.245,11.35v21.869H39.179V37.758h13.178v6.041h0.185   c1.835-3.476,6.315-7.14,13-7.14c13.913,0,16.481,9.156,16.481,21.059V81.966z" fill="#4f4ea1"/>
            </svg>
        </li>
    </a>
    <a href="https://www.linkedin.com/company/monikasales/" target="_blank">
        <li>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                <ellipse id="ig" cx="48.944" cy="48.719" rx="14.465" ry="14.017" fill="#4f4ea1"/>
                <path id="ig" d="M71.333,49.501c0,11.981-10.024,21.692-22.389,21.692s-22.389-9.711-22.389-21.692c0-2.147,0.324-4.221,0.924-6.18h-6.616    v30.427c0,1.576,1.288,2.863,2.863,2.863h50.159c1.576,0,2.865-1.287,2.865-2.863V43.321h-6.341    C71.008,45.28,71.333,47.354,71.333,49.501z" fill="#4f4ea1"/>
                <path id="ig" d="M65.332,35.11h8.141c1.784,0,3.242-1.458,3.242-3.242v-7.762c0-1.785-1.458-3.243-3.242-3.243h-8.141    c-1.785,0-3.243,1.458-3.243,3.243v7.762C62.088,33.651,63.547,35.11,65.332,35.11z" fill="#4f4ea1"/>
                <path id="ig" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z     M75.645,84.891H22.106c-5.087,0-9.246-3.765-9.246-9.244V22.105c0-5.481,4.159-9.245,9.246-9.245h53.539    c5.086,0,9.246,3.764,9.246,9.245v53.542C84.891,81.126,80.73,84.891,75.645,84.891z" fill="#4f4ea1"/>
            </svg>
        </li>
    </a>
</ul>
