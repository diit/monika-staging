<div class="row justify-content-center" id="policy">
    <div class="col-11">
        <div class="box">
            <div class="row justify-content-center">
                <div class="col-11 text-little">
                    <a href="<?= site_url('register') ?>" class="btn btn-primary" id="btn-register"><i class="fas fa-angle-left"></i> register</a>

                    <h4> Syarat dan Ketentuan</h4><br>
                    <p>Mohon dibaca dengan teliti bagian kebijakan privasi sebelum menggunakan <a href="https://monika.id" target="_blank">monika.id</a> (“Layanan”,”Kami”). Privasi adalah bagian penting bagi kami. Kebijakan privasi ini dibuat untuk menjelaskan informasi apa saja yang kami peroleh, informasi yang Anda peroleh dan bagaimana kami menggunakannya untuk layanan platform dan website kami.  Dengan mengakses layanan kami berarti Anda menyetujui kebijakan yang sudah kami buat. Pengguna hanya secara sukarela mengirimkan informasi tersebut kepada kami. Pengguna selalu dapat menolak untuk memberikan informasi identifikasi pribadi, kecuali bahwa hal itu dapat mencegahnya untuk terlibat dalam kegiatan terkait situs tertentu.</p>
                    <h5>DATA PRIBADI PENGGUNA (PENGGUNA)</h5>
                    <p>Kami mengumpulkan data pribadi pengguna dari Pengguna kami melalui berbagai cara termasuk, namun tidak terbatas pada, saat pengguna mengunjungi website, berlangganan newsletter, dan berhubungan dengan aktivitas, layanan, fitur atau sumber lain yang kami sediakan pada di situs kami.</p>
                    <div id="more">
                    <p>Di antara jenis data pribadi yang dikumpulkan layanan kami dengan sendirinya atau melalui pihak ketiga, antaranya; alamat email, alamat IP, cookie, lokasi, dan tanda tangan.</p>
                    <p>Rincian lengkap dalam setiap jenis Data Pribadi yang dikumpulkan disediakan dalam bagian khusus dari kebijakan privasi ini dengan penjelasan spesifik yang ditampilkan sebelum pengumpulan Data. Kami akan mengumpulkan dan menggunakan informasi pribadi semata-mata untuk memberikan keamanan dari pengguna dan memenuhi tujuan Anda, kecuali jika kami mendapatkan persetujuan dari orang-orang yang bersangkutan atau yang diwajibkan oleh undang-undang.</p>
                    <h5>DATA KLIEN (PIHAK KETIGA)</h5>
                    <p>Kami mengumpulkan data klien (pihak ketiga) dari Pengguna yang melakukan aktivitas, layanan dan fitur pada website kami.</p>
                    <p>Di antara jenis data pribadi dari klien yang dikumpulkan layanan kami dengan sendirinya dikarenakan adanya penggunaan fitur antaranya; alamat email, alamat IP, cookies dan lokasi.</p>
                    <p>Rincian lengkap dalam setiap jenis Data Klien yang dikumpulkan disediakan dalam bagian khusus dari kebijakan privasi ini dengan penjelasan spesifik yang ditampilkan sebelum pengumpulan Data. Kami akan mengumpulkan dan menggunakan infromasi pihak ketiga semata-mata untuk memenuhi kelengkapan isi dari dokumen yang merupakan produk dari monika.id seperti proposal dan invoice serta tujuan keamanan dari data klien agar tidak terjadi penyalahgunaan, kecuali jika kami mendapatkan persetujuan dari orang-orang yang bersangkutan atau yang diwajibkan oleh undang-undang.</p>
                    <h5>DATA NON-PRIBADI PENGGUNA</h5>
                    <p>Kami dapat mengumpulkan informasi identifikasi non-pribadi tentang pengguna kapan pun ketika berinteraksi dengan website kami, informasi non-pribadi mungkin termasuk nama browser, jenis komputer, dan informasi teknis tentang Pengguna yang terhubung dengan layanan kami, seperti sistem operasi dan penyedia layanan internet yang digunakan dan informasi serupa lainnya.</p>
                    <h5>INFORMASI LOKASI</h5>
                    <p>Layanan kami dapat mengumpulkan dan menggunakan informasi lokasi Anda (misalnya, menggunakan GPS pada perangkat mobile Anda) untuk menyediakan layanan fungsionalitas tertentu pada layanan kami. Informasi lokasi Anda mungkin mengalami penyalahgunaan dan pemantauan oleh orang lain, jadi mohon berhati-hati jika Anda memilih untuk mengaktifkan fungsionalitas lokasi. Kami juga menggunakan informasi lokasi Anda secara agregat.</p>
                    <h5>BAGAIMANA KAMI MENGGUNAKAN DATA PRIBADI</h5>
                    <p>Monika.id mengambil dan menggunakan informasi data pribadi untuk tujuan sebagai berikut:</p>
                    <p><ol>
                            <li>Memperbaiki layanan pelanggan kami<br>Informasi yang Anda berikan membantu kami menanggapi permintaan layanan pelanggan dan kebutuhan dukungan Anda secara lebih efisien.
                            </li>
                            <li>Memperbaiki Website kami<br>Kami membutuhkan umpan balik dari Anda mengenai produk dan layanan kami.
                            </li>
                            <li>Mengirim Email secara berkala<br>Kami kemungkinan akan menggunakan email Anda untuk mengirimkan informasi dan pembaruan yang berkaitan dengan pesanan mereka. Jika Pengguna memutuskan untuk ikut serta ke milis kami, mereka akan menerima email yang berisi berita terbaru dari layanan kami, pembaruan, informasi produk atau layanan terkait, dan lain-lain. Jika sewaktu-waktu Pengguna ingin berhenti berlangganan menerima email di masa mendatang, kami menyertakan rincian instruksi berhenti berlangganan di bagian bawah setiap email.
                            </li>
                    </ol></p>
                    <h5>PERLINDUNGAN DATA PRIBADI</h5>
                    <p>Kami menerapkan praktik pengumpulan, penyimpanan dan pengelolahan data yang tepat dan tindakan pengamanan untuk melindungi dari akses, perubahan, pengungkapan, atau penghancuran informasi pribadi, nama pengguna, kata sandi, informasi transaksi dan Data yang tersimpan pada di website kami secara tidak sah. Namun, ada resiko yang melekat dalam penggunaan internet dan kami tidak akan menjamin bahwa informasi Anda sepenuhnya aman.</p>
                    <h5>BERBAGI DATA PRIBADI PENGGUNA DENGAN PIHAK KETIGA</h5>
                    <p>Kami tidak menjual, menukar, atau menyewakan data pribadi Anda kepada pihak lain. Kami akan membagikan informasi demografis gabungan generik yang tidak terkait dengan informasi identifikasi pribadi apa pun mengenai pengunjung dan pengguna dengan mitra bisnis Anda, afiliasi terpercaya dan pengiklan untuk tujuan yang diuraikan diatas.</p>
                    <h5>INFORMASI YANG DIKIRIM KE PIHAK LUAR</h5>
                    <p>Pengguna harus memahami bahwa data pribadi yang secara sukarela pengguna masukan dan kirimkan ke dalam pihak luar dapat dilihat dan digunakan oleh pihak lain. Kami tidak dapat menggendalikan penggunaan informasi tersebut, dan dengan menggunakan layanan tersebut Pengguna menanggung risiko bahwa informasi pribadi yang Pengguna berikan dapat dilihat dan digunakan oleh pihak ketiga.</p>
                    <h5>MENGUBAH ATAU MENGHAPUS DATA ANDA</h5>
                    <p>Semua pengguna terdaftar dapat meninjau, memperbarui atau memperbaiki informasi pribadi yang diberikan dalam pendaftaran atau profil akun dengan mengubah "preferensi pengguna" mereka dalam akun mereka. Jika Pengguna memilih untuk keluar sepenuhnya, maka akun Pengguna mungkin menjadi dinonaktifkan. Namun, kami tidak dapat menghapus Data Pribadi dari catatan dari pihak ketiga yang telah disediakan dengan informasi Pengguna sesuai dengan kebijakan ini.</p>
                    </div>
                </div>
                <a id="read-more">Baca lebih lengkap ...</a>
            </div>
        </div>
    </div>
</div>

<ul id="medsos">
    <a href="https://www.facebook.com/usemonika" target="_blank">
        <li>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                <path id="fb" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z    M67.521,24.89l-6.76,0.003c-5.301,0-6.326,2.519-6.326,6.215v8.15h12.641L67.07,52.023H54.436v32.758H41.251V52.023H30.229V39.258   h11.022v-9.414c0-10.925,6.675-16.875,16.42-16.875l9.851,0.015V24.89L67.521,24.89z" fill="#4f4ea1"/>
            </svg>
        </li>
    </a>
    <a href="https://www.instagram.com/usemonika/" target="_blank">
        <li>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                <path id="in" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z    M30.562,81.966h-13.74V37.758h13.74V81.966z M23.695,31.715c-4.404,0-7.969-3.57-7.969-7.968c0.001-4.394,3.565-7.964,7.969-7.964   c4.392,0,7.962,3.57,7.962,7.964C31.657,28.146,28.086,31.715,23.695,31.715z M82.023,81.966H68.294V60.467   c0-5.127-0.095-11.721-7.142-11.721c-7.146,0-8.245,5.584-8.245,11.35v21.869H39.179V37.758h13.178v6.041h0.185   c1.835-3.476,6.315-7.14,13-7.14c13.913,0,16.481,9.156,16.481,21.059V81.966z" fill="#4f4ea1"/>
            </svg>
        </li>
    </a>
    <a href="https://www.linkedin.com/company/monikasales/" target="_blank">
        <li>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                <ellipse id="ig" cx="48.944" cy="48.719" rx="14.465" ry="14.017" fill="#4f4ea1"/>
                <path id="ig" d="M71.333,49.501c0,11.981-10.024,21.692-22.389,21.692s-22.389-9.711-22.389-21.692c0-2.147,0.324-4.221,0.924-6.18h-6.616    v30.427c0,1.576,1.288,2.863,2.863,2.863h50.159c1.576,0,2.865-1.287,2.865-2.863V43.321h-6.341    C71.008,45.28,71.333,47.354,71.333,49.501z" fill="#4f4ea1"/>
                <path id="ig" d="M65.332,35.11h8.141c1.784,0,3.242-1.458,3.242-3.242v-7.762c0-1.785-1.458-3.243-3.242-3.243h-8.141    c-1.785,0-3.243,1.458-3.243,3.243v7.762C62.088,33.651,63.547,35.11,65.332,35.11z" fill="#4f4ea1"/>
                <path id="ig" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z     M75.645,84.891H22.106c-5.087,0-9.246-3.765-9.246-9.244V22.105c0-5.481,4.159-9.245,9.246-9.245h53.539    c5.086,0,9.246,3.764,9.246,9.245v53.542C84.891,81.126,80.73,84.891,75.645,84.891z" fill="#4f4ea1"/>
            </svg>
        </li>
    </a>
</ul>
