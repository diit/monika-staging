<script>
    $(document).ready(function(){
        // SCRIPT INDEX
        $('#more1').hide();
        $('#more2').hide();

        $('#read-more1').click(function(){
            $('#more1').show();
            $('#more2').hide();
            $('#read-more1').hide();
            $('#read-more2').show();
        });

        $('#read-more2').click(function(){
            $('#more2').show();
            $('#more1').hide();
            $('#read-more2').hide();
            $('#read-more1').show();
        });
        // END OF SCRIPT INDEX
    });
</script>

<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="798a792f-1ac9-4623-ae90-3bf3c539b6fa";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
