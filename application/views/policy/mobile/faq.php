<div id="content">
    <div class="box">
        <div id="faq">
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="true" aria-controls="collapseOne">Apa itu Monika?</button>
                        </h5>
                    </div>
                    <div id="faq1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body text-thin">Monika adalah aplikasi untuk membuat proposal (quotation / penawaran) dan invoice berbasis online.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="collapseTwo">Mengapa Monika?</button>
                        </h5>
                    </div>
                    <div id="faq2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body text-thin">Aplikasi monika sangat simpel. Anda hanya perlu persiapkan data klien dan menambahkan sedikit informasi terkait untuk proposal atau invoice. Tidak perlu berganti perangkat atau aplikasi dokumen, semua diakses melalui satu aplikasi begitu juga klien Anda.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq3" aria-expanded="false" aria-controls="collapseTwo">Bagaimana cara mulai menggunakannya?</button>
                        </h5>
                    </div>
                    <div id="faq3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body text-thin">Sangatlah mudah.
                            <ol>
                                <li>Pertama buat akun terlebih dahulu di</li>
                                <li>Pilih dokumen mana yang akan Anda buat</li>
                                <li>Masukkan data klien (atau Anda juga bisa memasukkan daftar klien yang banyak terlebih dahulu)</li>
                                <li>Masukkan informasi terkait tujuan Anda</li>
                                <li>Bagikan link dokumen melalui whatsapp, email atau SMS.</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq4" aria-expanded="false" aria-controls="collapseTwo">Apakah perlu mencetak dokumen untuk menandatangani?</button>
                        </h5>
                    </div>
                    <div id="faq4" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body text-thin">Tidak perlu. Cukup berikan link dokumen kepada klien Anda, arahkan untuk mengklik "Tandatangani". Client atau Anda bisa menAndatangani dokumen langsung dari smartphone, tablet ataupun laptop. Anda tetap bisa mengunduh versi PDF dari dokumen.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq5" aria-expanded="false" aria-controls="collapseTwo">Apakah bisa mencobanya secara Gratis?</button>
                        </h5>
                    </div>
                    <div id="faq5" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body text-thin">Anda bisa gunakan secara Gratis dengan batasan 2 dokumen. Selebihnya Anda bisa memilih paket sesuai kebutuhan mulai dari Rp 39.000,- untuk pengguna personal.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq6" aria-expanded="false" aria-controls="collapseTwo">Saya tidak bisa desain tetapi saya ingin proposal saya terlihat menarik untuk klien. Apakah bisa?</button>
                        </h5>
                    </div>
                    <div id="faq6" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body text-thin">Proposal yang baik adalah proposal yang informasinya dapat langsung dimengerti oleh klien. Maka dari itu, kami mempersiapkan bagaimana agar informasi yang akan Anda sampaikan dapat terlihat jelas oleh klien ketimbang memikirkan atribut desain. Anda cukup menulis proposal. Jika Anda merasa bosan dengan proposal yang ada, Anda boleh menghubungi tim kami dan siap membantu Anda.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq9" aria-expanded="false" aria-controls="collapseTwo">Apakah isi proposal di Monika.id bisa diubah?</button>
                        </h5>
                    </div>
                    <div id="faq9" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body text-thin">YA, setiap isi proposal di Monika.id yang dibuat bisa diubah 100%. Anda cukup mengubah dan menghapus bagian yang dirasa perlu untuk diubah.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq10" aria-expanded="false" aria-controls="collapseTwo">Apakah logo Monika.id akan muncul di dalam proposal yang dikirim ke klien kami?</button>
                        </h5>
                    </div>
                    <div id="faq10" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body text-thin">Tidak, semua pelanggan berbayar mendapatkan pilihan, jadi tidak ada watermark monika.id di dalam proposal yang akan digunakan ke klien.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq11" aria-expanded="false" aria-controls="collapseTwo">Apakah Monika juga memberikan layanan untuk Perusahaan besar?</button>
                        </h5>
                    </div>
                    <div id="faq11" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body text-thin">Kami juga menawarkan fitur untuk Enterprise , dimana Anda bisa menikmati layanan bebas tak berbatas dan bisa menambahkan tim di akun Anda. Hubungi tim kami, klik chat sebelah kanan.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
