<div id="content" class="policy">
    <!-- syarat ketentuan -->
    <div class="box">
        <p class="box-title">syarat dan ketentuan</p>
        <div class="text-little">
            <p>Mohon dibaca dengan teliti sebelum menggunakan layanan dari <a href="https://monika.id" target="_blank">monika.id</a> (“Layanan”,”Kami”). Dengan mendaftar akun di monika.id berarti Anda secara sukarela untuk terikat dengan persyaratan dan ketentuan layanan ini, semua undang-undang dan peraturan yang berlaku, dan setuju bahwa Anda bertanggung jawab untuk mematuhi undang-undang yang berlaku. Jika Anda tidak menyetujui persyaratan dan ketentuan dari kami. Anda tidak diperbolehkan mengakses layanan dari kami.</p>
            <div id="more1">
            <p>Monika.id adalah website yang menyediakan layanan untuk membuat proposal (penawaran) dan invoice serta melakukan pelacakan ketika proposal dan invoice dikirimkan ke pihak yang dituju. Melalui layanan terkait (secara keseluruhan, layanan tersebut, termasuk fitur) tunduk pada Syarat dan Ketentuan berikut (sebagaimana yang telah diubah dari waktu ke waktu, “Ketentuan Layanan”). Kami berhak, atas pertimbangan kami sendiri, untuk mengubah atau memodifikasi sebagian dari Ketentuan Layanan ini kapan saja. Jika kami melakukan ini, kami akan mengeposkan perubahan pada halaman ini dan akan menunjukkan di bagian atas halaman ini tanggal persyaratan ini terakhir direvisi. Kami juga akan memberi tahu Anda, baik melalui antarmuka pengguna Layanan, dalam pemberitahuan email atau melalui cara masuk akal lainnya.</p>
            <p>Dengan mengakses atau menggunakan Layanan, Anda setuju untuk terikat oleh Persyaratan ini. Jika Anda tidak setuju dengan bagian dari persyaratan mana pun, Anda mungkin tidak dapat mengakses Layanan.</p>
            <h5>KONTEN</h5>
            <p>Kami memiliki hak kekayaan intelektual dari setiap dan semua komponen layanan yang dapat dilindungi termasuk konten otomatis yang dibuat oleh monika.id.</p>
            <p>Kami tidak bertanggung jawab atas kerusakan langsung, tidak langsung atau konsekuensial yang timbul dari atau terkait dengan penggunaan Anda atas layanan ini.</p>
            <p>Layanan kami memungkinkan Anda mengeposkan, menautkan, menyimpan, berbagi, dan menyediakan informasi, teks, grafik, atau materi lain ("Konten") tertentu. Anda bertanggung jawab atas Konten yang Anda kirim ke Layanan, termasuk legalitas, keandalan, dan kesesuaiannya.</p>
            <p>Dengan mengeposkan Konten ke Layanan, Anda memberi kami hak dan lisensi untuk menyimpan dan menampilkan (ke tujuan yang Anda tentukan) Konten semacam itu di dan melalui Layanan. Anda menyimpan setiap dan semua hak Anda atas Konten yang Anda kirimkan, poskan atau tampilkan pada atau melalui Layanan dan Anda bertanggung jawab untuk melindungi hak-hak tersebut.</p>
            <p>Anda menyatakan dan menjamin bahwa: (i) Konten itu milik Anda (Anda memilikinya) atau Anda memiliki hak untuk menggunakannya dan memberi kami hak dan lisensi sebagaimana ditentukan dalam Persyaratan ini, dan (ii) pengeposan Konten Anda atau melalui Layanan tidak melanggar hak privasi, hak publisitas, hak cipta, hak kontrak atau hak lainnya dari orang mana pun.</p>
            <h5>AKUN PENGGUNA</h5>
            <p>Bila Anda membuat akun bersama kami, Anda harus memberi kami informasi yang akurat, lengkap, dan terkini setiap saat. Kegagalan untuk melakukannya merupakan pelanggaran Persyaratan, yang dapat mengakibatkan penghentian segera akun Anda pada Layanan kami.</p>
            <p>Anda bertanggung jawab untuk melindungi kata sandi yang Anda gunakan untuk mengakses Layanan dan untuk aktivitas atau tindakan apa pun di bawah kata sandi Anda, apakah kata sandi Anda dengan Layanan atau layanan pihak ketiga kami.</p>
            <p>Anda setuju untuk tidak mengungkapkan kata sandi Anda kepada pihak ketiga manapun. Anda harus segera memberi tahu kami saat mengetahui adanya pelanggaran keamanan atau penggunaan akun Anda yang tidak sah.</p>
            <p>Anda mungkin tidak menggunakan nama pengguna nama orang atau entitas lain atau yang tidak tersedia secara sah untuk digunakan, nama atau merek dagang yang tunduk pada hak orang atau entitas lain selain Anda tanpa otorisasi yang sesuai, atau nama yang sebaliknya menyinggung, vulgar atau cabul.</p>
            <h5>LAYANAN KAMI DAN LANGGANAN BERBAYAR</h5>
            <p>monika.id menyediakan layanan membuat proposal dan invoice. Layanan monika.id tertentu diberikan kepada Anda secara gratis. Layanan monika.id lainnya memerlukan pembayaran sebelum Anda mengaksesnya. Layanan monika yang dapat diakses setelah pembayaran disebut sebagai “Layanan Personal”, “Layanan Agency” dan “Layanan Enterprise”. Layanan monika.id yang tidak memerlukan pembayaran hanya berlaku untuk 2 kali pembuatan dokumen.</p>
            <h5>PEMBAYARAN, PEMBATALAN DAN MASA TUNGGU</h5>
            <p>Langganan berbayar pada dapat dibeli dengan cara (1) membayar biaya berlangganan bulanan; atau (2) membayaran secara kontrak tahunan; untuk “Layanan Enterprise” melalui penawaran oleh sales secara negosiasi.</p>
            <p>Anda dapat mengubah pilihan Anda untuk atau tanpa alasan apapun dan menerima pengembalian dana secara penuh dari semua uang yang dibayarkan dalam waktu 14 hari (“Periode Tunggu”). Pengembalian uang tidak akan, bagaimanapun, diberikan kepada Anda jika Anda mengakses layanan monika.id pada periode Masa Tunggu.</p>
            <p>Langganan berbayar Anda yang akan berakhir akan diingatkan seminggu sebelum masa berlangganan habis.</p>
            <h5>KEAMANAN DAN PENGENDALIAN</h5>
            <p>Akun, password dan keamanan - Jika Anda telah diberikan pilihan untuk membuka akun di Situs Web ini yang akan memberikan Anda akses ke bagian-bagian tertentu yang dilindungi oleh password di situs ini dan Anda memilih untuk melakukannya, Anda harus melengkapi proses pendaftaran dengan memberikan kepada kami informasi yang terbaru, lengkap dan akurat sebagaimana yang diminta di dalam formulir pendaftaran tersebut, dan memilih password dan nama pengguna. Anda sepenuhnya bertanggung jawab untuk menjaga kerahasiaan password dan akun dan untuk setiap dan semua aktivitas yang terjadi dalam akun Anda. Anda setuju untuk (a) secepatnya memberitahu Pemilik jika akun Anda digunakan tanpa seizin Anda atau jika Anda mengetahui adanya pelanggaran terhadap keamanan, dan (b) benar-benar keluar dari akun di akhir setiap sesi online. Pemilik tidak akan bertanggung jawab atas kerugian yang mungkin timbul sebagai akibat dari password atau akun Anda digunakan orang lain, baik dengan ataupun tanpa sepengetahuan Anda. Meskipun demikian, Anda dapat dimintai pertanggungjawaban atas kerugian yang diderita oleh Pemilik atau pihak lain yang disebabkan penggunaan akun atau password Anda oleh orang lain. Anda dilarang menggunakan akun orang lain kapanpun, dengan atau tanpa seizin pemilik akun.</p>
            <p>Dokumen yang telah Anda buat dalam proses pembuatan proposal telah dilindungi dengan cara enkripsi. Enkripsi monika.id memastikan bahwa hanya Anda yang berhak mengakses dokumen-dokumen dalam website. Dokumen Anda diamankan dengan sebuah kunci dan hanya Anda dan klien Anda saja yang dapat memiliki kunci spesial untuk dapat mengakses setiap dokumen yang Anda kirim. Keamanan digital menjadi sangat penting bagi monika.id agar tidak terjadi hal yang tidak diinginkan melalui kriminal digital.</p>
            <h5>LINK KE WEBSITE LAIN</h5>
            <p>Layanan kami mungkin berisi tautan ke situs web pihak ketiga atau layanan yang tidak dimiliki atau dikendalikan oleh kami.</p>
            <p>monika.id tidak memiliki kendali atas, dan tidak bertanggung jawab atas, konten, kebijakan privasi, atau praktik dari situs web atau layanan pihak ketiga manapun. Anda selanjutnya mengetahui dan setuju bahwa monika.id tidak bertanggung jawab atau bertanggung jawab secara langsung atau tidak langsung atas kerusakan atau kerugian yang disebabkan atau diduga disebabkan oleh atau sehubungan dengan penggunaan atau kepercayaan atas konten, barang atau layanan yang tersedia pada atau melalui situs web atau layanan semacam itu.</p>
            <p>Kami sangat menyarankan Anda untuk membaca syarat dan ketentuan dan kebijakan privasi dari situs web pihak ketiga atau layanan yang Anda kunjungi.</p>
            <h5>PERUBAHAN</h5>
            <p>Kami berhak, atas pertimbangan kami sendiri, untuk mengubah atau mengganti persyaratan ini kapan saja. Jika ada revisi materi, kami akan melakukan pemberitahuan setidaknya 30 hari sebelum persyaratan baru mulai berlaku. Apa yang merupakan material akan ditentukan atas kebijakan kami.</p>
            <p>Dengan terus mengakses atau menggunakan Layanan kami setelah revisi tersebut menjadi efektif, Anda setuju untuk terikat oleh persyaratan yang telah direvisi. Jika Anda tidak menyetujui persyaratan baru, mohon berhenti menggunakan Layanan.</p>
            <h5>HUBUNGI KAMI</h5>
            <p>Jika Anda memiliki pertanyaan seputar syarat dan ketentuan yang berlaku, Anda bisa menghubungi kami melalui alamat email sebagai berikut: <a href="mailto:info@monika.id">info@monika.id</a></p>
            </div>
        </div>
        <a href="#" id="read-more1">Baca lebih lengkap ...</a>
    </div>
    <!-- end of syarat ketentuan -->

    <!-- kebijakan -->
    <div class="box top-space">
        <p class="box-title">kebijakan privasi</p>
        <div class="text-little">
            <p>Mohon dibaca dengan teliti bagian kebijakan privasi sebelum menggunakan <a href="https://monika.id" target="_blank">monika.id</a> (“Layanan”,”Kami”). Privasi adalah bagian penting bagi kami. Kebijakan privasi ini dibuat untuk menjelaskan informasi apa saja yang kami peroleh, informasi yang Anda peroleh dan bagaimana kami menggunakannya untuk layanan platform dan website kami.  Dengan mengakses layanan kami berarti Anda menyetujui kebijakan yang sudah kami buat. Pengguna hanya secara sukarela mengirimkan informasi tersebut kepada kami. Pengguna selalu dapat menolak untuk memberikan informasi identifikasi pribadi, kecuali bahwa hal itu dapat mencegahnya untuk terlibat dalam kegiatan terkait situs tertentu.</p>
            <div id="more2">
            <h5>DATA PRIBADI PENGGUNA (PENGGUNA)</h5>
            <p>Kami mengumpulkan data pribadi pengguna dari Pengguna kami melalui berbagai cara termasuk, namun tidak terbatas pada, saat pengguna mengunjungi website, berlangganan newsletter, dan berhubungan dengan aktivitas, layanan, fitur atau sumber lain yang kami sediakan pada di situs kami.</p>
            <p>Di antara jenis data pribadi yang dikumpulkan layanan kami dengan sendirinya atau melalui pihak ketiga, antaranya; alamat email, alamat IP, cookie, lokasi, dan tanda tangan.</p>
            <p>Rincian lengkap dalam setiap jenis Data Pribadi yang dikumpulkan disediakan dalam bagian khusus dari kebijakan privasi ini dengan penjelasan spesifik yang ditampilkan sebelum pengumpulan Data. Kami akan mengumpulkan dan menggunakan informasi pribadi semata-mata untuk memberikan keamanan dari pengguna dan memenuhi tujuan Anda, kecuali jika kami mendapatkan persetujuan dari orang-orang yang bersangkutan atau yang diwajibkan oleh undang-undang.</p>
            <h5>DATA KLIEN (PIHAK KETIGA)</h5>
            <p>Kami mengumpulkan data klien (pihak ketiga) dari Pengguna yang melakukan aktivitas, layanan dan fitur pada website kami.</p>
            <p>Di antara jenis data pribadi dari klien yang dikumpulkan layanan kami dengan sendirinya dikarenakan adanya penggunaan fitur antaranya; alamat email, alamat IP, cookies dan lokasi.</p>
            <p>Rincian lengkap dalam setiap jenis Data Klien yang dikumpulkan disediakan dalam bagian khusus dari kebijakan privasi ini dengan penjelasan spesifik yang ditampilkan sebelum pengumpulan Data. Kami akan mengumpulkan dan menggunakan infromasi pihak ketiga semata-mata untuk memenuhi kelengkapan isi dari dokumen yang merupakan produk dari monika.id seperti proposal dan invoice serta tujuan keamanan dari data klien agar tidak terjadi penyalahgunaan, kecuali jika kami mendapatkan persetujuan dari orang-orang yang bersangkutan atau yang diwajibkan oleh undang-undang.</p>
            <h5>DATA NON-PRIBADI PENGGUNA</h5>
            <p>Kami dapat mengumpulkan informasi identifikasi non-pribadi tentang pengguna kapan pun ketika berinteraksi dengan website kami, informasi non-pribadi mungkin termasuk nama browser, jenis komputer, dan informasi teknis tentang Pengguna yang terhubung dengan layanan kami, seperti sistem operasi dan penyedia layanan internet yang digunakan dan informasi serupa lainnya.</p>
            <h5>INFORMASI LOKASI</h5>
            <p>Layanan kami dapat mengumpulkan dan menggunakan informasi lokasi Anda (misalnya, menggunakan GPS pada perangkat mobile Anda) untuk menyediakan layanan fungsionalitas tertentu pada layanan kami. Informasi lokasi Anda mungkin mengalami penyalahgunaan dan pemantauan oleh orang lain, jadi mohon berhati-hati jika Anda memilih untuk mengaktifkan fungsionalitas lokasi. Kami juga menggunakan informasi lokasi Anda secara agregat.</p>
            <h5>BAGAIMANA KAMI MENGGUNAKAN DATA PRIBADI</h5>
            <p>Monika.id mengambil dan menggunakan informasi data pribadi untuk tujuan sebagai berikut:</p>
            <p><ol>
                    <li>Memperbaiki layanan pelanggan kami<br>Informasi yang Anda berikan membantu kami menanggapi permintaan layanan pelanggan dan kebutuhan dukungan Anda secara lebih efisien.
                    </li>
                    <li>Memperbaiki Website kami<br>Kami membutuhkan umpan balik dari Anda mengenai produk dan layanan kami.
                    </li>
                    <li>Mengirim Email secara berkala<br>Kami kemungkinan akan menggunakan email Anda untuk mengirimkan informasi dan pembaruan yang berkaitan dengan pesanan mereka. Jika Pengguna memutuskan untuk ikut serta ke milis kami, mereka akan menerima email yang berisi berita terbaru dari layanan kami, pembaruan, informasi produk atau layanan terkait, dan lain-lain. Jika sewaktu-waktu Pengguna ingin berhenti berlangganan menerima email di masa mendatang, kami menyertakan rincian instruksi berhenti berlangganan di bagian bawah setiap email.
                    </li>
            </ol></p>
            <h5>PERLINDUNGAN DATA PRIBADI</h5>
            <p>Kami menerapkan praktik pengumpulan, penyimpanan dan pengelolahan data yang tepat dan tindakan pengamanan untuk melindungi dari akses, perubahan, pengungkapan, atau penghancuran informasi pribadi, nama pengguna, kata sandi, informasi transaksi dan Data yang tersimpan pada di website kami secara tidak sah. Namun, ada resiko yang melekat dalam penggunaan internet dan kami tidak akan menjamin bahwa informasi Anda sepenuhnya aman.</p>
            <h5>BERBAGI DATA PRIBADI PENGGUNA DENGAN PIHAK KETIGA</h5>
            <p>Kami tidak menjual, menukar, atau menyewakan data pribadi Anda kepada pihak lain. Kami akan membagikan informasi demografis gabungan generik yang tidak terkait dengan informasi identifikasi pribadi apa pun mengenai pengunjung dan pengguna dengan mitra bisnis Anda, afiliasi terpercaya dan pengiklan untuk tujuan yang diuraikan diatas.</p>
            <h5>INFORMASI YANG DIKIRIM KE PIHAK LUAR</h5>
            <p>Pengguna harus memahami bahwa data pribadi yang secara sukarela pengguna masukan dan kirimkan ke dalam pihak luar dapat dilihat dan digunakan oleh pihak lain. Kami tidak dapat menggendalikan penggunaan informasi tersebut, dan dengan menggunakan layanan tersebut Pengguna menanggung risiko bahwa informasi pribadi yang Pengguna berikan dapat dilihat dan digunakan oleh pihak ketiga.</p>
            <h5>MENGUBAH ATAU MENGHAPUS DATA ANDA</h5>
            <p>Semua pengguna terdaftar dapat meninjau, memperbarui atau memperbaiki informasi pribadi yang diberikan dalam pendaftaran atau profil akun dengan mengubah "preferensi pengguna" mereka dalam akun mereka. Jika Pengguna memilih untuk keluar sepenuhnya, maka akun Pengguna mungkin menjadi dinonaktifkan. Namun, kami tidak dapat menghapus Data Pribadi dari catatan dari pihak ketiga yang telah disediakan dengan informasi Pengguna sesuai dengan kebijakan ini.</p>
            </div>
        </div>
        <a href="#" id="read-more2">Baca lebih lengkap ...</a>
    </div>
    <!-- end of kebijakan -->
</div>
