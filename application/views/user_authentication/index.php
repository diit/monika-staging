<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">

    <link href="<?= base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <script src="<?= base_url() ?>assets/js/jquery3.2.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap3.3.7.min.js"></script>
</head>
<body>
    <h1>Sign In With Google Account</h1>
    <a class="btn btn-default" href="<?php echo $loginURL; ?>">Login with google</a>
</body>
