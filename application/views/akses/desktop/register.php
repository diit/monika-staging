<div class="col-sm-6 col-md-5 text-center" id="form-register">
    <div class="row justify-content-center">
        <div class="col-sm-11 col-md-8">
            <img src="<?= base_url() ?>assets/img/monika-logo-mobile.png" id="brand">
            <form class="form-inline" name="registerForm" id="registerForm" method="post" action="<?php echo isset($inv)? base_url('user/register_user/?inv='.$inv) :base_url('user/register_user'); ?>">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-user"></i></div>
                    </div>
                    <input type="text" class="form-control" placeholder="Nama lengkap" id="username" name="user_name" autocomplete="off">
                </div>
                <div id="name-msg"></div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-briefcase"></i></div>
                    </div>
                    <input type="text" class="form-control" placeholder="Nama usaha (optional)" id="nama-usaha" name="user_corp" autocomplete="off">
                </div>
                <div id="usaha-msg"></div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-envelope"></i></div>
                    </div>
                    <?php if (!empty($inv)){ ?>
                    <input type="email" class="form-control" placeholder="Email" id="mail" name="user_email" autocomplete="off" value="<?=$data_inv->email?>" readonly>
                    <?php }
                    else { ?>
                    <input type="email" class="form-control" placeholder="Email" id="mail" name="user_email" autocomplete="off">
                    <?php } ?>
                </div>
                <div id="email-msg"></div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-phone"></i></div>
                    </div>
                    <input type="text" class="form-control" placeholder="Nomor handphone" id="mobile" name="user_mobile" pattern="^[0-9]{10,13}$" autocomplete="off" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);">
                </div>
                <div id="phone-msg"></div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-key"></i></div>
                    </div>
                    <input type="password" class="form-control" placeholder="Password" id="password" name="user_password" autocomplete="off">
                </div>
                <div id="password-msg"></div>
            </form>

            <!-- feedback register account -->
            <?php
            if($this->session->flashdata('status_regis')==1)
            {
                echo "<div class='alert alert-danger'>Ada kesalahan, silahkan <a id='chat' href='#'>klik di sini</a> untuk berbicara dengan Customer Service kami.</div>";
            }
            else if($this->session->flashdata('status_regis')==2)
            {
                echo "<div class='alert alert-danger'>Email sudah digunakan, silahkan gunakan email lain.</div>";
            }
            ?>

            <?php
            $success_msg = $this->session->flashdata('success_msg');
            $error_msg = $this->session->flashdata('error_msg');
            ?>
            <?php if($success_msg){ ?>
                <div class="alert alert-success"><?php echo $success_msg; ?></div>
            <?php }if($error_msg){ ?>
                <div class="alert alert-danger"><?php echo $error_msg; ?></div>
            <?php } ?>
            <!-- end of feedback register account -->

            <p class="text-left text-small grey">Dengan klik Register, Anda telah menyetujui <a href="<?= site_url('syaratketentuan') ?>">Syarat dan Ketentuan</a> serta <a href="<?= site_url('kebijakanprivasi') ?>">Kebijakan Privasi</a> monika.id</p>
            <button class="btn btn-block btn-primary" id="btnsubmit"><span id="btn-name">register</span><div class="loader-akses"></div></button>
            &nbsp;
            <p class="note">Sudah memiliki akun? <br><a href="<?= site_url('login') ?>">Login disini</a></p>
        </div>
    </div>
</div>

<script>
    function ValidateEmail(email) {
        var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return expr.test(email);
    };

    $('.form-control').on('keypress',function (e) {
        if (e.which == 13) {
            $('#btnsubmit').trigger('click');
        }
    });

    $(document).ready(function(){
        $('#chat').on('click',function(){
            $crisp.push(['do', 'chat:open']);
        });

        $('.loader-akses').hide();
        $('#mail').change(function(){
          if($('#mail').val()==''){
            $('#email-alert').hide();
            $('#btnsubmit').prop('disabled',false);
          }else{
            x = $('#mail').val();
            $.ajax({
              type:"POST",
              url: "<?php echo base_url()?>user/checkEmail",
              data: {email:x},
              dataType: 'json',
              success: function(data){
                if(data.status == "false"){
                  $('#email-alert').html('Email sudah terdaftar');
                  $('#email-alert').attr('class','alert alert-danger');
                  $('#email-alert').show();
                  $('#mail').focus();
                  $('#btnsubmit').prop('disabled',true);
                }else if(data.status=="true"){
                  $('#email-alert').html('Email valid');
                  $('#email-alert').attr('class','alert alert-success');
                  $('#email-alert').hide();
                  $('#btnsubmit').prop('disabled',false);
                }
              }
            });
          }
        });

        $('#mobile').change(function(){
          if($('#mobile').val()==''){
            $('#phone-alert').hide();
            $('#btnsubmit').prop('disabled',false);
          }else{
            y = $('#mobile').val();
            $.ajax({
              type:"POST",
              url: "<?php echo base_url()?>user/checkPhone",
              data: {phone:y},
              dataType: 'json',
              success: function(data){
                if(data.status == "false"){
                  $('#phone-alert').html('Nomor Handphone sudah terdaftar');
                  $('#phone-alert').attr('class','alert alert-danger');
                  $('#phone-alert').show();
                  $('#mobile').focus();
                  $('#btnsubmit').prop('disabled',true);
                }else if(data.status=="true"){
                  $('#phone-alert').html('Phone valid');
                  $('#phone-alert').attr('class','alert alert-success');
                  $('#phone-alert').hide();
                  $('#btnsubmit').prop('disabled',false);
                }
              }
            });
          }
        });

        var format = /[$%^&*()+\=\[\]{};'"\\|<>\/?]/;

        $('#btnsubmit').on('click',function(){
          if($('#username').val() == '')
          {
              $('#name-msg').empty().append("<div class='alert alert-danger'>Isi nama Anda</div>");
          }
          else if($('#mail').val() == '')
          {
            $('#email-msg').empty().append("<div class='alert alert-danger'>Email wajib diisi</div>");
          }
          else if(!ValidateEmail($('#mail').val()))
          {
            $('#email-msg').empty().append("<div class='alert alert-danger'>Pastikan format email benar</div>");
          }
          else if($('#password').val() == '')
          {
            $('#password-msg').empty().append("<div class='alert alert-danger'>Password wajib diisi</div>");
          }
          else
          {
            if(format.test($('#username').val()))
            {
                $('#name-msg').empty().append("<div class='alert alert-danger'>Mohon tidak menginput special character</div>");
            }
            else if(format.test($('#mail').val()))
            {
                $('#email-msg').empty().append("<div class='alert alert-danger'>Mohon tidak menginput special character</div>");
            }
            else if(format.test($('#password').val()))
            {
                $('#password-msg').empty().append("<div class='alert alert-danger'>Mohon tidak menginput special character</div>");
            }
            else
            {
                fbq('track', 'CompleteRegistration');
                $('#registerForm').submit();
                $('#btn-name').hide();
                $('.loader-akses').show();
            }
          }
        });

      });
</script>
