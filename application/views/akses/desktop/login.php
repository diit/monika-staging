<div class="col-sm-6 col-md-5 text-center" id="form-login">
    <div class="row justify-content-center">
        <div class="col-sm-11 col-md-8">
            <img src="<?= base_url() ?>assets/img/monika-logo-mobile.png" id="brand">
            <form class="form-inline" id="login" method="post" action="<?php echo isset($redirect) ? base_url('user/login_user/?redirect='.$redirect): base_url('user/login_user'); ?>">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-envelope"></i></div>
                    </div>
                    <input type="email" class="form-control" placeholder="Email" id="user_email" name="user_email" autocomplete="off">
                </div>
                <div id="email-msg"></div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-key"></i></div>
                    </div>
                    <input type="password" class="form-control" placeholder="Password" id="user_password" name="user_password" autocomplete="off">
                </div>
                <div id="password-msg"></div>
            </form>
            <!-- feedback email account -->
            <?php if($this->session->flashdata('status'))
                {
                    echo $this->session->flashdata('status');
                }
                else
                {
                    if($this->session->flashdata('status_verif')==1)
                    {
                       echo "<div class='alert alert-success'>Verifikasi email berhasil, silahkan login</div>";
                    }
                    else if($this->session->flashdata('status_verif')==2)
                    {
                       echo "<div class='alert alert-danger'>Verifikasi email belum berhasil</div>";
                    }
                    else if($this->session->flashdata('status_regis')==1)
                    {
                       echo "<div class='alert alert-success'>Registrasi berhasil, silahkan cek email untuk verifikasi</div>";
                    }
                    else if($this->session->flashdata('status_pass')==1)
                    {
                       echo "<div class='alert alert-success'>Password berhasil diubah, silahkan login kembali</div>";
                    }
                    else if($this->session->flashdata('status_pass')==2)
                    {
                       echo "<div class='alert alert-success'>Password gagal diubah, silahkan cek email kembali</div>";
                    }
                }
            ?>
            <!-- end of feedback email account -->

            <!-- feedback login account -->
            <?php
                $success_msg = $this->session->flashdata('success_msg');
                $error_msg = $this->session->flashdata('error_msg');
            ?>
            <?php if($success_msg){ ?>
                <div class="alert alert-success"><?php echo $success_msg; ?></div>
            <?php }if($error_msg){ ?>
                <div class="alert alert-danger"><?php echo $error_msg; ?></div>
            <?php } ?>
            <!-- end of feedback email account -->
            &nbsp;
            <p class="text-left"><a data-toggle="modal" href="#" data-target="#modalLupaPassword">Lupa password?</a></p>
            <button class="btn btn-block btn-primary" id="btn-login"><span id="btn-name">login</span><div class="loader-akses"></div></button>
            &nbsp;
            <p class="note">Belum memiliki akun? <br><a href="<?= site_url('register') ?>">Daftar sekarang</a></p>
        </div>
    </div>
</div>
</div>

<!-- modal forgot password -->
<div aria-hidden="true" role="dialog" id="modalLupaPassword" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title">Lupa Password?</h5>
            </div>
            <form id="resetPassword" name="resetPassword" method="post" action="<?php echo base_url();?>User/ForgotPassword" onsubmit='return validate()'>
                <div class="modal-body">
                    <p>Masukkan email anda untuk reset password.</p>
                    <input type="email" name="user_email" id="email" placeholder="Email" autocomplete="off" class="form-control">
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default">Batal</button>
                    <button class="btn btn-primary" type="submit">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end of modal forgot password -->

<script type="text/javascript">
    function ValidateEmail(email) {
        var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return expr.test(email);
    };

    $(document).ready(function(){
        $('#chat').on('click',function(){
            $crisp.push(['do', 'chat:open']);
        });

        $('#user_email,#user_password').on('keypress',function (e) {
            if (e.which == 13) {
                $('#btn-login').trigger('click');
            }
        });

      	var format = /[$%^&*()+\=\[\]{};'"\\|<>\/?]/;

        $('.loader-akses').hide();
      	$('#btn-login').on('click',function(){
      	  if($('#user_email').val() == '')
          {
              $('#email-msg').empty().append("<div class='alert alert-danger'>Email wajib diisi</div>");
          }
          else if(!ValidateEmail($('#user_email').val()))
          {
              $('#email-msg').empty().append("<div class='alert alert-danger'>Pastikan format email benar</div>");
          }
          else if($('#user_password').val() == '')
          {
              $('#password-msg').empty().append("<div class='alert alert-danger'>Password wajib diisi</div>");
          }
          else
          {
          	if(format.test($('#user_email').val()))
            {
                $('#email-msg').empty().append("<div class='alert alert-danger'>Mohon tidak menginput special character</div>");
            }
            else if(format.test($('#user_password').val()))
            {
                $('#password-msg').empty().append("<div class='alert alert-danger'>Mohon tidak menginput special character</div>");
            }
            else
            {
            	$('#login').submit();
                $('#btn-name').hide();
                $('.loader-akses').show();
            }
          }
      	});
    });
</script>
