<div class="col-sm-6 col-md-5 text-center" id="form-reset-password">
    <div class="row justify-content-center">
        <div class="col-sm-11 col-md-8">
            <img src="<?= base_url() ?>assets/img/monika-logo-mobile.png" id="brand">
            <?php
              $success_msg = $this->session->flashdata('success_msg');
              $error_msg = $this->session->flashdata('error_msg');
            ?>
            <form class="form-login" method="post" action="<?php echo base_url('user/getPassword'); ?>">
                <input value="<?= $saltid?>" hidden name="saltid">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-key"></i></div>
                    </div>
                    <input type="password" class="form-control" placeholder="Password baru" name="new_password" autocomplete="off" required>
                </div>

                <?php if($this->session->flashdata('status')==1){
                    echo "<div class='alert alert-success alert-fit'><p>Silahkan cek email anda untuk reset password</p></div>";
                  }else if($this->session->flashdata('status')==2){
                    echo "<div class='alert alert-danger alert-fit'><p>Email tidak ditemukan</p></div>";
                  }else if($this->session->flashdata('status_verif')==1){
                    echo "<div class='alert alert-success alert-fit'><p>Verifikasi email berhasil, silahkan login</p></div>";
                  }else if($this->session->flashdata('status_verif')==2){
                    echo "<div class='alert alert-danger alert-fit'><p>Verifikasi email belum berhasil</p></div>";
                  }else if($this->session->flashdata('status_regis')==1){
                    echo "<div class='alert alert-success alert-fit'><p>Registrasi berhasil, silahkan cek email untuk verifikasi</p></div>";
                  }
                  ?>
                &nbsp;
                <button type="submit" class="btn btn-primary btn-block">reset password</button>

                <p class="note"><a href="<?=site_url('login')?>">Login</a> | <a href="<?=site_url('register')?>">Register</a></p>
            </form>
        </div>
    </div>
</div>
</div>
