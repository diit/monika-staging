<div class="row justify-content-center" id="form-akses">
    <div class="col-11">
        <div class="box">
            <div class="row justify-content-center">
                <div class="col-11">
                    <form class="form-inline" id="login" method="post" action="<?php echo isset($redirect) ? base_url('user/login_user/?redirect='.$redirect): base_url('user/login_user'); ?>">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-envelope"></i></div>
                            </div>
                            <input type="email" class="form-control" placeholder="Email" id="user_email" name="user_email" autocomplete="off">
                            <div id="email-msg"></div>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-key"></i></div>
                            </div>
                            <input type="password" class="form-control" placeholder="Password" id="user_password" name="user_password" autocomplete="off">
                            <div id="password-msg"></div>
                        </div>
                    </form>

                    <!-- feedback email account -->
                    <?php if($this->session->flashdata('status'))
                        {
                            echo $this->session->flashdata('status');
                        }
                        else
                        {
                            if($this->session->flashdata('status_verif')==1)
                            {
                               echo "<div class='alert alert-success'>Verifikasi email berhasil, silahkan login</div>";
                            }
                            else if($this->session->flashdata('status_verif')==2)
                            {
                               echo "<div class='alert alert-danger'>Verifikasi email belum berhasil</div>";
                            }
                            else if($this->session->flashdata('status_regis')==1)
                            {
                               echo "<div class='alert alert-success'>Registrasi berhasil, silahkan cek email untuk verifikasi</div>";
                            }
                            else if($this->session->flashdata('status_pass')==1)
                            {
                               echo "<div class='alert alert-success'>Password berhasil diubah, silahkan login kembali</div>";
                            }
                            else if($this->session->flashdata('status_pass')==2)
                            {
                               echo "<div class='alert alert-success'>Password gagal diubah, silahkan cek email kembali</div>";
                            }
                        }
                    ?>
                    <!-- end of feedback email account -->

                    <!-- feedback login account -->
                    <?php
                        $success_msg = $this->session->flashdata('success_msg');
                        $error_msg = $this->session->flashdata('error_msg');
                    ?>
                    <?php if($success_msg){ ?>
                        <div class="alert alert-success"><?php echo $success_msg; ?></div>
                    <?php }if($error_msg){ ?>
                        <div class="alert alert-danger"><?php echo $error_msg; ?></div>
                    <?php } ?>
                    <!-- end of feedback email account -->

                    <p><a data-toggle="modal" href="#" data-target="#modalLupaPassword">Lupa password?</a></p>
                    <button class="btn btn-block btn-primary" id="btn-login"><span id="btn-name">login</span><div class="loader-akses"></div></button>

                    <br><br>
                    <p class="text-center">Belum memiliki akun? <br><a href="<?= site_url('register') ?>">Daftar sekarang</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<ul id="medsos">
    <a href="https://www.facebook.com/usemonika" target="_blank">
        <li>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                <path id="fb" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z    M67.521,24.89l-6.76,0.003c-5.301,0-6.326,2.519-6.326,6.215v8.15h12.641L67.07,52.023H54.436v32.758H41.251V52.023H30.229V39.258   h11.022v-9.414c0-10.925,6.675-16.875,16.42-16.875l9.851,0.015V24.89L67.521,24.89z" fill="#4f4ea1"/>
            </svg>
        </li>
    </a>
    <a href="https://www.instagram.com/usemonika/" target="_blank">
        <li>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                <path id="in" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z    M30.562,81.966h-13.74V37.758h13.74V81.966z M23.695,31.715c-4.404,0-7.969-3.57-7.969-7.968c0.001-4.394,3.565-7.964,7.969-7.964   c4.392,0,7.962,3.57,7.962,7.964C31.657,28.146,28.086,31.715,23.695,31.715z M82.023,81.966H68.294V60.467   c0-5.127-0.095-11.721-7.142-11.721c-7.146,0-8.245,5.584-8.245,11.35v21.869H39.179V37.758h13.178v6.041h0.185   c1.835-3.476,6.315-7.14,13-7.14c13.913,0,16.481,9.156,16.481,21.059V81.966z" fill="#4f4ea1"/>
            </svg>
        </li>
    </a>
    <a href="https://www.linkedin.com/company/monikasales/" target="_blank">
        <li>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                <ellipse id="ig" cx="48.944" cy="48.719" rx="14.465" ry="14.017" fill="#4f4ea1"/>
                <path id="ig" d="M71.333,49.501c0,11.981-10.024,21.692-22.389,21.692s-22.389-9.711-22.389-21.692c0-2.147,0.324-4.221,0.924-6.18h-6.616    v30.427c0,1.576,1.288,2.863,2.863,2.863h50.159c1.576,0,2.865-1.287,2.865-2.863V43.321h-6.341    C71.008,45.28,71.333,47.354,71.333,49.501z" fill="#4f4ea1"/>
                <path id="ig" d="M65.332,35.11h8.141c1.784,0,3.242-1.458,3.242-3.242v-7.762c0-1.785-1.458-3.243-3.242-3.243h-8.141    c-1.785,0-3.243,1.458-3.243,3.243v7.762C62.088,33.651,63.547,35.11,65.332,35.11z" fill="#4f4ea1"/>
                <path id="ig" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z     M75.645,84.891H22.106c-5.087,0-9.246-3.765-9.246-9.244V22.105c0-5.481,4.159-9.245,9.246-9.245h53.539    c5.086,0,9.246,3.764,9.246,9.245v53.542C84.891,81.126,80.73,84.891,75.645,84.891z" fill="#4f4ea1"/>
            </svg>
        </li>
    </a>
</ul>

<!-- Modal -->
<div aria-hidden="true" role="dialog" id="modalLupaPassword" class="modal fade">
    <div class="modal-dialog modal-sm text-center">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title">Lupa Password?</h5>
            </div>
            <form id="resetPassword" name="resetPassword" method="post" action="<?php echo base_url();?>User/ForgotPassword" onsubmit='return validate()'>
                <div class="modal-body row justify-content-center">
                    <div class="col-10">
                        <p>Masukkan email anda untuk reset password.</p>
                        <input type="email" name="user_email" id="email" placeholder="Email" autocomplete="off" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default">Batal</button>
                    <button class="btn btn-primary" type="submit">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- modal -->

<script type="text/javascript">
    function ValidateEmail(email) {
        var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return expr.test(email);
    };

    $(document).ready(function(){
        $('#chat').on('click',function(){
            $crisp.push(['do', 'chat:open']);
        });

        $('#user_email,#user_password').on('keypress',function (e) {
            if (e.which == 13) {
                $('#btn-login').trigger('click');
            }
        });

      	var format = /[$%^&*()+\=\[\]{};'"\\|<>\/?]/;

        $('.loader-akses').hide();
      	$('#btn-login').on('click',function(){
      	  if($('#user_email').val() == '')
          {
              $('#email-msg').empty().append("<div class='alert alert-danger'>Email wajib diisi</div>");
          }
          else if(!ValidateEmail($('#user_email').val()))
          {
              $('#email-msg').empty().append("<div class='alert alert-danger'>Pastikan format email benar</div>");
          }
          else if($('#user_password').val() == '')
          {
              $('#password-msg').empty().append("<div class='alert alert-danger'>Password wajib diisi</div>");
          }
          else
          {
          	if(format.test($('#user_email').val()))
            {
                $('#email-msg').empty().append("<div class='alert alert-danger'>Mohon tidak menginput special character</div>");
            }
            else if(format.test($('#user_password').val()))
            {
                $('#password-msg').empty().append("<div class='alert alert-danger'>Mohon tidak menginput special character</div>");
            }
            else
            {
            	$('#login').submit();
                $('#btn-name').hide();
                $('.loader-akses').show();
            }
          }
      	});
    });
</script>
