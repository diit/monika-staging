<div class="row justify-content-center" id="form-akses">
    <div class="col-11">
        <div class="box">
            <div class="row justify-content-center">
                <div class="col-11">
                    <form class="form-inline" name="registerForm" id="registerForm" method="post" action="<?php echo isset($inv)? base_url('user/register_user/?inv='.$inv) :base_url('user/register_user'); ?>">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-user"></i></div>
                            </div>
                            <input type="text" class="form-control" placeholder="Nama lengkap" id="username" name="user_name" autocomplete="off" style="min-width:70%">
                            <div id="name-msg" style="width:100%"></div>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-briefcase"></i></div>
                            </div>
                            <input type="text" class="form-control" placeholder="Nama usaha (optional)" id="nama-usaha" name="user_corp" autocomplete="off">
                            <div id="usaha-msg"></div>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-envelope"></i></div>
                            </div>
                            <?php if (!empty($inv)){ ?>
                            <input type="email" class="form-control" placeholder="Email" id="mail" name="user_email" autocomplete="off" value="<?=$data_inv->email?>" readonly>
                            <?php }
                            else { ?>
                            <input type="email" class="form-control" placeholder="Email" id="mail" name="user_email" autocomplete="off">
                            <?php } ?>
                            <div id="email-msg"></div>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-phone"></i></div>
                            </div>
                            <input type="text" class="form-control" placeholder="Nomor handphone" id="mobile" name="user_mobile" pattern="^[0-9]{10,13}$" autocomplete="off" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8);">
                            <div id="phone-msg"></div>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-key"></i></div>
                            </div>
                            <input type="password" class="form-control" placeholder="Password" id="password" name="user_password" autocomplete="off">
                            <div id="password-msg"></div>
                        </div>
                    </form>

                    <!-- feedback register account -->
                    <?php
                    if($this->session->flashdata('status_regis')==1)
                    {
                        echo "<div class='alert alert-danger'>Ada kesalahan, silahkan <a id='chat' href='#'>klik di sini</a> untuk berbicara dengan Customer Service kami.</div>";
                    }
                    else if($this->session->flashdata('status_regis')==2)
                    {
                        echo "<div class='alert alert-danger'>Email sudah digunakan, silahkan gunakan email lain.</div>";
                    }
                    ?>

                    <?php
                    $success_msg = $this->session->flashdata('success_msg');
                    $error_msg = $this->session->flashdata('error_msg');
                    ?>
                    <?php if($success_msg){ ?>
                        <div class="alert alert-success"><?php echo $success_msg; ?></div>
                    <?php }if($error_msg){ ?>
                        <div class="alert alert-danger"><?php echo $error_msg; ?></div>
                    <?php } ?>
                    <!-- end of feedback register account -->

                    <p class="text-small grey">Dengan klik Register, Anda telah menyetujui <a href="<?= site_url('syaratketentuan') ?>">Syarat dan Ketentuan</a> serta <a href="<?= site_url('kebijakanprivasi') ?>">Kebijakan Privasi</a> monika.id</p>
                    <button class="btn btn-block btn-primary" id="btnsubmit"><span id="btn-name">register</span><div class="loader-akses"></div></button>
                    &nbsp;
                    <p class="text-center">Sudah memiliki akun? <br><a href="<?= site_url('login') ?>">Login disini</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<ul id="medsos">
    <a href="https://www.facebook.com/usemonika" target="_blank">
        <li>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                <path id="fb" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z    M67.521,24.89l-6.76,0.003c-5.301,0-6.326,2.519-6.326,6.215v8.15h12.641L67.07,52.023H54.436v32.758H41.251V52.023H30.229V39.258   h11.022v-9.414c0-10.925,6.675-16.875,16.42-16.875l9.851,0.015V24.89L67.521,24.89z" fill="#4f4ea1"/>
            </svg>
        </li>
    </a>
    <a href="https://www.linkedin.com/company/monikasales/" target="_blank">
        <li>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                <path id="in" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z    M30.562,81.966h-13.74V37.758h13.74V81.966z M23.695,31.715c-4.404,0-7.969-3.57-7.969-7.968c0.001-4.394,3.565-7.964,7.969-7.964   c4.392,0,7.962,3.57,7.962,7.964C31.657,28.146,28.086,31.715,23.695,31.715z M82.023,81.966H68.294V60.467   c0-5.127-0.095-11.721-7.142-11.721c-7.146,0-8.245,5.584-8.245,11.35v21.869H39.179V37.758h13.178v6.041h0.185   c1.835-3.476,6.315-7.14,13-7.14c13.913,0,16.481,9.156,16.481,21.059V81.966z" fill="#4f4ea1"/>
            </svg>
        </li>
    </a>
    <a href="https://www.instagram.com/usemonika/" target="_blank">
        <li>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 97.75 97.75" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve">
                <ellipse id="ig" cx="48.944" cy="48.719" rx="14.465" ry="14.017" fill="#4f4ea1"/>
                <path id="ig" d="M71.333,49.501c0,11.981-10.024,21.692-22.389,21.692s-22.389-9.711-22.389-21.692c0-2.147,0.324-4.221,0.924-6.18h-6.616    v30.427c0,1.576,1.288,2.863,2.863,2.863h50.159c1.576,0,2.865-1.287,2.865-2.863V43.321h-6.341    C71.008,45.28,71.333,47.354,71.333,49.501z" fill="#4f4ea1"/>
                <path id="ig" d="M65.332,35.11h8.141c1.784,0,3.242-1.458,3.242-3.242v-7.762c0-1.785-1.458-3.243-3.242-3.243h-8.141    c-1.785,0-3.243,1.458-3.243,3.243v7.762C62.088,33.651,63.547,35.11,65.332,35.11z" fill="#4f4ea1"/>
                <path id="ig" d="M48.875,0C21.882,0,0,21.882,0,48.875S21.882,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.868,0,48.875,0z     M75.645,84.891H22.106c-5.087,0-9.246-3.765-9.246-9.244V22.105c0-5.481,4.159-9.245,9.246-9.245h53.539    c5.086,0,9.246,3.764,9.246,9.245v53.542C84.891,81.126,80.73,84.891,75.645,84.891z" fill="#4f4ea1"/>
            </svg>
        </li>
    </a>
</ul>

<script>
    function ValidateEmail(email) {
        var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return expr.test(email);
    };

    $('.form-control').on('keypress',function (e) {
        if (e.which == 13) {
            $('#btnsubmit').trigger('click');
        }
    });

    $(document).ready(function(){
        $('#chat').on('click',function(){
            $crisp.push(['do', 'chat:open']);
        });

        $('.loader-akses').hide();
        $('#mail').change(function(){
          if($('#mail').val()==''){
            $('#email-alert').hide();
            $('#btnsubmit').prop('disabled',false);
          }else{
            x = $('#mail').val();
            $.ajax({
              type:"POST",
              url: "<?php echo base_url()?>user/checkEmail",
              data: {email:x},
              dataType: 'json',
              success: function(data){
                if(data.status == "false"){
                  $('#email-alert').html('Email sudah terdaftar');
                  $('#email-alert').attr('class','alert alert-danger');
                  $('#email-alert').show();
                  $('#mail').focus();
                  $('#btnsubmit').prop('disabled',true);
                }else if(data.status=="true"){
                  $('#email-alert').html('Email valid');
                  $('#email-alert').attr('class','alert alert-success');
                  $('#email-alert').hide();
                  $('#btnsubmit').prop('disabled',false);
                }
              }
            });
          }
        });

        $('#mobile').change(function(){
          if($('#mobile').val()==''){
            $('#phone-alert').hide();
            $('#btnsubmit').prop('disabled',false);
          }else{
            y = $('#mobile').val();
            $.ajax({
              type:"POST",
              url: "<?php echo base_url()?>user/checkPhone",
              data: {phone:y},
              dataType: 'json',
              success: function(data){
                if(data.status == "false"){
                  $('#phone-alert').html('Nomor Handphone sudah terdaftar');
                  $('#phone-alert').attr('class','alert alert-danger');
                  $('#phone-alert').show();
                  $('#mobile').focus();
                  $('#btnsubmit').prop('disabled',true);
                }else if(data.status=="true"){
                  $('#phone-alert').html('Phone valid');
                  $('#phone-alert').attr('class','alert alert-success');
                  $('#phone-alert').hide();
                  $('#btnsubmit').prop('disabled',false);
                }
              }
            });
          }
        });

        var format = /[$%^&*()+\=\[\]{};'"\\|<>\/?]/;

        $('#btnsubmit').on('click',function(){
          if($('#username').val() == '')
          {
              $('#name-msg').empty().append("<div class='alert alert-danger'>Isi nama Anda</div>");
          }
          else if($('#mail').val() == '')
          {
            $('#email-msg').empty().append("<div class='alert alert-danger'>Email wajib diisi</div>");
          }
          else if(!ValidateEmail($('#mail').val()))
          {
            $('#email-msg').empty().append("<div class='alert alert-danger'>Pastikan format email benar</div>");
          }
          else if($('#password').val() == '')
          {
            $('#password-msg').empty().append("<div class='alert alert-danger'>Password wajib diisi</div>");
          }
          else
          {
            if(format.test($('#username').val()))
            {
                $('#name-msg').empty().append("<div class='alert alert-danger'>Mohon tidak menginput special character</div>");
            }
            else if(format.test($('#mail').val()))
            {
                $('#email-msg').empty().append("<div class='alert alert-danger'>Mohon tidak menginput special character</div>");
            }
            else if(format.test($('#password').val()))
            {
                $('#password-msg').empty().append("<div class='alert alert-danger'>Mohon tidak menginput special character</div>");
            }
            else
            {
                fbq('track', 'CompleteRegistration');
                $('#registerForm').submit();
                $('#btn-name').hide();
                $('.loader-akses').show();
            }
          }
        });

      });
</script>
