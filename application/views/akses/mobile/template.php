<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.ico">
    <title>Welcome - Monika App</title>
    <meta name="description" content="Software otomatisasi yang membatu mengelola pemasaran freelancer dan ahensi dalam membuat hingga melacak proposal dan faktur untuk memenangkan klien dalam waktu singkat">
    <meta name="author" content="Monika">
    <meta name="keyword" content="Proposal, Invoice, Penawaran, Lacak Proposal, Tracking, Closing, Proposal Goal">

    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/font-awesome-5/css/all.css">
    <link rel="stylesheet/less" href="<?= base_url() ?>assets/css/style.less">
    <link rel="stylesheet/less" href="<?= base_url() ?>assets/css/style_mobile.less">
    <link rel="stylesheet/less" href="<?= base_url() ?>assets/css/style_mobile_akses.less">

    <script src="<?= base_url() ?>assets/js/bootstrap-4/js/jquery-3.3.1.slim.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap-4/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap-4/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/js/less.min.js" ></script>

    <!-- analytics script -->
    <?php $this->load->view('template/global_analytics'); ?>
    <!-- end of analytics script -->

</head>
<body>

    <div id="banner">
        <img src="<?= base_url() ?>assets/img/monika_logo_white.png" id="brand" style="width:150px"><br><br>
        <p>Menangkan klien dalam waktu singkat</p>
    </div>

    <?php $this->load->view($content); ?>

    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="798a792f-1ac9-4623-ae90-3bf3c539b6fa";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

</body>
</html>
