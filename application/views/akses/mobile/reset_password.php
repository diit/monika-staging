<div class="row justify-content-center" id="form-akses">
    <div class="col-11">
        <div class="box">
            <div class="row justify-content-center">
                <div class="col-11">
                    <form class="form-login" method="post" action="<?php echo base_url('user/getPassword'); ?>">
                        &nbsp;
                        <input value="<?= $saltid?>" hidden name="saltid">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-key"></i></div>
                            </div>
                            <input type="password" class="form-control" placeholder="Password baru" name="new_password" autocomplete="off" required>
                        </div>

                        <?php if($this->session->flashdata('status')==1){
                            echo "<div class='alert alert-success alert-fit'><p>Silahkan cek email anda untuk reset password</p></div>";
                          }else if($this->session->flashdata('status')==2){
                            echo "<div class='alert alert-danger alert-fit'><p>Email tidak ditemukan</p></div>";
                          }else if($this->session->flashdata('status_verif')==1){
                            echo "<div class='alert alert-success alert-fit'><p>Verifikasi email berhasil, silahkan login</p></div>";
                          }else if($this->session->flashdata('status_verif')==2){
                            echo "<div class='alert alert-danger alert-fit'><p>Verifikasi email belum berhasil</p></div>";
                          }else if($this->session->flashdata('status_regis')==1){
                            echo "<div class='alert alert-success alert-fit'><p>Registrasi berhasil, silahkan cek email untuk verifikasi</p></div>";
                          }
                          ?>
                        &nbsp;
                        <button type="submit" class="btn btn-primary btn-block">reset password</button>

                        <p style="margin-top:60px" class="text-center"><a href="<?=site_url('login')?>">Login</a> | <a href="<?=site_url('register')?>">Register</a></p>
                        &nbsp;
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
