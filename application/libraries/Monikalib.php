<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Monikalib
{
	public function __construct()
	{
		$this->CI =& get_instance();
	    $this->CI->load->model('user_model');
	    $this->CI->load->model('proposal_model');
	    $this->CI->load->model('invoice_model');
	}

	function getCategory()
	{
		$listcategory = $this->CI->user_model->getUserCategory();
	    return $listcategory;
	}

	function getTutorialStatus()
	{
		$TotalProposal = $this->CI->user_model->getTutorialStatus($this->CI->session->userdata('user_id'));
		return $TotalProposal;
	}

	function getTotalProposal()
	{
		$TotalProposal = $this->CI->proposal_model->getProposalByUser($this->CI->session->userdata('user_id'));
		return $TotalProposal;
	}

	function getTotalProposalByTeam()
	{
		$TotalProposal = $this->CI->proposal_model->proposalByTeam();
		return $TotalProposal;
	}

	function getTotalInvoice()
	{
		$TotalProposal = $this->CI->invoice_model->getInvoiceByUser($this->CI->session->userdata('user_id'));
		return $TotalProposal;
	}

	function getTotalInvoiceByTeam()
	{
		$TotalProposal = $this->CI->invoice_model->getInvoiceByTeam();
		return $TotalProposal;
	}

	function getTotalReferral()
	{
		$TotalReferral = $this->CI->user_model->getReferralByUser($this->CI->session->userdata('user_id'));
		return $TotalReferral;
	}

	function getUserPlan()
	{
		$currentplan = $this->CI->user_model->getcurrentplan();
		return $currentplan;
	}

    function currentUserPlan()
    {
        //get plan id
        if (($this->CI->user_model->remainTrialDay()==false) && (!empty($this->CI->user_model->getcurrentplan()))){
            return $plan = $this->CI->user_model->getcurrentplan()->plan_id;
        }
        //trial finish
        elseif (($this->CI->user_model->remainTrialDay()==false) && (empty($this->CI->user_model->getcurrentplan()))){
            return $plan = 0;
        }
        //trial period
        elseif (($this->CI->user_model->remainTrialDay()!=false) && (empty($this->CI->user_model->getcurrentplan()))){
            return $plan = 14;
        }
    }

    function limitDoc()
    {
        //trial finish
        if (($this->CI->user_model->remainTrialDay()==false) && (empty($this->CI->user_model->getcurrentplan()))){
            return $limit = 'personal';
        }
    }

    function remainTrialDay()
    {
        $remain = $this->CI->user_model->remainTrialDay();
        return $remain;
    }

	function getCategoryByEmail(){
		  $row=$this->CI->user_model->getUserByEmail($this->CI->session->userdata('user_email'));
			$listcategory = $this->CI->user_model->getUserCategory();
			$userCat = $row['kategori'];
			foreach ($listcategory as $kat){
				if ($kat->id == $userCat ) {
					return $kat->kategori;
				}

			}
	}

	function detailProposal(){
		$drafted = $this->CI->proposal_model->getProposalByStatus($this->CI->session->userdata('user_id'),0);
		$terkirim = $this->CI->proposal_model->getProposalByStatus($this->CI->session->userdata('user_id'),0);
		$dibaca = $this->CI->proposal_model->getProposalByStatus($this->CI->session->userdata('user_id'),1);
		$disetujui = $this->CI->proposal_model->getProposalByStatus($this->CI->session->userdata('user_id'),2);
		$jmldraft = $drafted->num_rows();
		$jmlkirim = $terkirim->num_rows();
		$jmlbaca = $dibaca->num_rows();
		$jmlsetuju = $disetujui->num_rows();

		$dtljml = array(
			'draft' => $jmldraft,
			'kirim' => $jmlkirim,
			'baca' => $jmlbaca,
			'setuju' => $jmlsetuju
		);
		return $dtljml;
		// die(print_r($dtljml));
	}

	function detailProposalTeam(){
		$drafted = $this->CI->proposal_model->getProposalTeamByStatus(0);
		$terkirim = $this->CI->proposal_model->getProposalTeamByStatus(0);
		$dibaca = $this->CI->proposal_model->getProposalTeamByStatus(1);
		$disetujui = $this->CI->proposal_model->getProposalTeamByStatus(2);
		$jmldraft = $drafted;
		$jmlkirim = $terkirim;
		$jmlbaca = $dibaca;
		$jmlsetuju = $disetujui;

		$dtljml = array(
			'draft' => $jmldraft,
			'kirim' => $jmlkirim,
			'baca' => $jmlbaca,
			'setuju' => $jmlsetuju
		);
		return $dtljml;
//		 die(print_r($dtljml));
	}

	function detailInvoice(){
		$drafted = $this->CI->invoice_model->getInvoiceByStatus($this->CI->session->userdata('user_id'),0);
		$terkirim = $this->CI->invoice_model->getInvoiceByStatus($this->CI->session->userdata('user_id'),0);
		$dibaca = $this->CI->invoice_model->getInvoiceByStatus($this->CI->session->userdata('user_id'),1);
		$disetujui = $this->CI->invoice_model->getInvoiceByStatus($this->CI->session->userdata('user_id'),2);
		$jmldraft = $drafted->num_rows();
		$jmlkirim = $terkirim->num_rows();
		$jmlbaca = $dibaca->num_rows();
		$jmlsetuju = $disetujui->num_rows();

		$dtljml = array(
			'draft' => $jmldraft,
			'kirim' => $jmlkirim,
			'baca' => $jmlbaca,
			'setuju' => $jmlsetuju
		);
		return $dtljml;
		// die(print_r($dtljml));
	}

	function detailInvoiceTeam(){
		$drafted = $this->CI->invoice_model->getInvoiceByStatus(0);
		$terkirim = $this->CI->invoice_model->getInvoiceByStatus(0);
		$dibaca = $this->CI->invoice_model->getInvoiceByStatus(1);
		$disetujui = $this->CI->invoice_model->getInvoiceByStatus(2);
		$jmldraft = $drafted;
		$jmlkirim = $terkirim;
		$jmlbaca = $dibaca;
		$jmlsetuju = $disetujui;

		$dtljml = array(
			'draft' => $jmldraft,
			'kirim' => $jmlkirim,
			'baca' => $jmlbaca,
			'setuju' => $jmlsetuju
		);
		return $dtljml;
		// die(print_r($dtljml));
	}

	function format_date_indonesia($date)
	{
		$date = DateTime::createFromFormat('Y-m-d H:i:s', $date);
		$bulan = array ('Jan'=>'Januari', // array bulan konversi
			'Feb'=>'Februari',
			'Mar'=>'Maret',
			'Apr'=>'April',
			'May'=>'Mei',
			'Jun'=>'Juni',
			'Jul'=>'Juli',
			'Aug'=>'Agustus',
			'Sep'=>'September',
			'Oct'=>'Oktober',
			'Nov'=>'November',
			'Dec'=>'Desember'
		);
		$newdate =  $date->format('d').' '.$bulan[$date->format('M')].' '.$date->format('Y');
		return $newdate;
	}

	function format_date_add_indonesia($date,$add)
	{
		$date = DateTime::createFromFormat('Y-m-d H:i:s', $date);
		$date = date_add($date,date_interval_create_from_date_string($add." days"));
		$bulan = array ('Jan'=>'Januari', // array bulan konversi
			'Feb'=>'Februari',
			'Mar'=>'Maret',
			'Apr'=>'April',
			'May'=>'Mei',
			'Jun'=>'Juni',
			'Jul'=>'Juli',
			'Aug'=>'Agustus',
			'Sep'=>'September',
			'Oct'=>'Oktober',
			'Nov'=>'November',
			'Dec'=>'Desember'
		);
		$newdate =  $date->format('d').' '.$bulan[$date->format('M')].' '.$date->format('Y');
		return $newdate;
	}

    function getPlanNow()
    {
        $plan = $this->CI->invoice_model->getPlanNow();
        return $plan;
    }

}

?>
