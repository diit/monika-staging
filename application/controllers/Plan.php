<?php
  require 'vendor/autoload.php';
  use Mailgun\Mailgun;
  use Cloudinary\Cloudinary;
  class Plan extends CI_Controller
  {
    private $data;
    public function __construct()
    {
            parent::__construct();
      		  $this->load->helper('url');
      	    $this->load->model('user_model');
            $this->load->model('laporan_model');
            $this->load->model('plan_model');
            $this->load->library('session');
            $this->data['tutorial'] = $this->monikalib->getTutorialStatus();

            // configure Cloudinary API connection
            \Cloudinary::config(array(
                "cloud_name" => "monika-id",
                "api_key" => "665942816591155", /*874837483274837*/
                "api_secret" => "2w5o13cnwyBrErhTUwAtLtSrrv8" /* a676b67565c6767a6767d6767f676fe1 */
            ));

      if($this->session->userdata('status') !== 'login' )
      {
        redirect('login');
      }
    }

    public function index()
    {
      $data = $this->data;
      $data['page'] = 'Upgrade Plan';
      $data['t_head'] = 'upgrade/t_head.php';
      $data['t_foot'] = 'upgrade/t_foot.php';
      $data['content'] = 'upgrade/biaya.php';
      $this->load->view("template/main.php",$data);
    }

    function referral()
    {
      $data = $this->data;
      $data['content'] = 'upgrade/referral.php';
      $this->load->view("template/main.php",$data);
    }

    function status()
    {
      $data = $this->data;
      $team_id = $this->session->userdata('team_id');
      $data['page'] = 'Status Upgrade';
      $data['t_head'] = 'upgrade/t_head';
      $data['t_foot'] = 'upgrade/t_foot';
      $data['plan'] = $this->monikalib->getUserPlan();
      $data['history'] = $this->user_model->getplanhistory($team_id);
      $data['content'] = 'upgrade/status.php';
      $this->load->view("template/main.php",$data);
    }

    function payment($invoice)
    {
      $data = $this->data;
      $data['page'] = 'Payment Upgrade';
      $data['t_head'] = 'upgrade/t_head';
      $data['t_foot'] = 'upgrade/t_foot';
      $data['invoice_detail'] = $this->user_model->invoice_detail($invoice);
      $data['content'] = 'upgrade/payment.php';
      $this->load->view("template/main.php",$data);
    }

    function send_request()
    {
      $data = $this->input->post();

          $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
                    $domain = "ml.monika.id";

                    # Make the call to the client.
                    $result = $mgClient->sendMessage($domain, array(
                      'from'    => $this->session->userdata('user_name').' via Monika.id <info@monika.id>',
                      'to'      => 'adit@monika.id',
                      'h:Reply-To'=> $this->session->userdata('user_email'),
                      'subject' => $data['email_subject'],
                      'html'    => '<div style="font-family:verdana;font-size:13px;">'.nl2br($data['email_message']).'</div>',
                    ));

                    echo 1;
    }

    function upgrade()
    {
      $invoice = mt_rand(100000000,1000000000);
      $data = $this->input->post();
      $promo = $data['promo'];
      if($promo == '' || $promo == NULL)
      {
        $promo = NULL;
      }
      $data_upgrade = array(
                'invoice'=>$invoice,
                'plan_id'=>$data['plan'],
                'user_id'=>$this->session->userdata('user_id'),
                'team_id'=>$this->session->userdata('team_id'),
                'payment_method'=>0,
                'status'=>0,
                'upgrade_time'=>date("Y-m-d h:i:s"),
                'payment_time'=>NULL,
                'active_time'=>NULL,
                'promo_code'=>$promo
              );
      $status = $this->user_model->insert_upgrade_data($data_upgrade);
      if($status)
      {
        $url = site_url('plan/payment/'.$invoice);

                    $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
                    $domain = "ml.monika.id";

                    # Make the call to the client.
                    $result = $mgClient->sendMessage($domain, array(
                      'from'    => 'Monika.id <info@monika.id>',
                      'to'      => $this->session->userdata('user_email'),
                      'subject' => 'Lakukan pembayaran upgrade plan invoice #'.$invoice,
                      'html'    => '<div style="font-family: verdana;">
                                    <p>Hi '.$this->session->userdata('user_name').',</p>
                                      <p>Silahkan lakukan pembayaran untuk mengaktifkan plan Anda. Pembayaran bisa dilakukan melalui Transfer Bank atau Paypal</p>
                                      <p><a href="'.$url.'">Klik di sini untuk cara pembayaran</a></p>
                                      <p>Best,<br><br>Monika.id</p></div>',
                    ));
        echo $invoice;
      }
      else
      {
        echo 0;
      }
    }

    function paid()
    {
      $invoice = $this->input->post('invoice');
      $payment_method = $this->input->post('payment');
      $team_id = $this->session->userdata('team_id');
      $bayar = $this->user_model->plan_paid($invoice,$team_id,$payment_method);
      if($bayar)
      {
        # Make the call to the client.
        if($payment_method == 2)
        {
          $url = site_url('plan/status');

          $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
          $domain = "ml.monika.id";
          $result = $mgClient->sendMessage($domain, array(
                      'from'    => 'Monika.id <info@monika.id>',
                      'to'      => $this->session->userdata('user_email'),
                      'subject' => 'Pembayaran plan Anda invoice #'.$invoice.' berhasil!',
                      'html'    => '<div style="font-family: verdana;">
                                    <p>Hi '.$this->session->userdata('user_name').',</p>
                                      <p>Pembayaran Plan Anda invoice #'.$invoice.' melalui Paypal berhasil dilakukan.</p>
                                      <p><a href="'.$url.'">Klik di sini untuk lihat riwayat pembayaran</a></p>
                                      <p>Best,<br><br>Monika.id</p></div>',
                    ));
        }
        echo 1;
      }
      else
      {
        echo 0;
      }
    }

    public function upload_bank_transfer_receipt()
        {
          $invoice = $this->input->post('invoice');
          $team_id = $this->session->userdata('team_id');
          $user_name = $this->session->userdata('user_name');
          $images = $_FILES["file"];
          $data_upload_receipt = \Cloudinary\Uploader::upload($images['tmp_name'], array("use_filename" => TRUE));
          $save_transfer_receipt = $this->user_model->update_logo_transfer_receipt($team_id,$invoice,$data_upload_receipt['secure_url']);
          if($save_transfer_receipt)
          {
            $url = site_url('plan/status');

            $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
            $domain = "ml.monika.id";
            $result = $mgClient->sendMessage($domain, array(
                        'from'    => 'Monika.id <info@monika.id>',
                        'to'      => 'monika.proposal@gmail.com',
                        'subject' => 'Pembayaran plan invoice #'.$invoice.' dari '.$user_name,
                        'html'    => '<div style="font-family: verdana;">
                                      <p>Ada pembayaran dari '.$user_name.',</p>
                                        <p>Invoice #'.$invoice.' melalui Transfer Bank.</p>
                                        <p>Best,<br><br>Monika.id</p></div>',
                      ), array(
                        'attachment' => array($data_upload_receipt['secure_url'],$data_upload_receipt['secure_url'])
                      ));

            $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
            $domain = "ml.monika.id";
            $result = $mgClient->sendMessage($domain, array(
                        'from'    => 'Monika.id <info@monika.id>',
                        'to'      => $this->session->userdata('user_email'),
                        'subject' => 'Pembayaran plan invoice #'.$invoice.' telah diterima',
                        'html'    => '<div style="font-family: verdana;">
                                      <p>Pembayaran Anda invoice #'.$invoice.' telah kami terima.</p>
                                        <p>Mohon tunggu, aktivasi sedang diproses.</p>
                                        <p>Best,<br><br>Monika.id</p></div>',
                      ));

            redirect('plan/status');
          }
        }

    function check_promo()
    {
      $promo = $this->input->post('promo');
      $check_promo = $this->user_model->check_promo($promo);
      if($check_promo)
      {
        echo $check_promo->disc;
      }
      else
      {
        echo 0;
      }
    }

    function plan()
    {
        if($this->mobile_detect->isMobile()){
          $data['branch'] = 'User / Plan';
          $data['back'] = site_url('user');
          $data['t_head'] = 'upgrade/mobile/t_head';
          $data['t_foot'] = 'upgrade/mobile/t_foot';
          $data['content'] = 'upgrade/mobile/index';
          $this->load->view("template/mobile/main",$data);
        }
        else{
          $team_id = $this->session->userdata('team_id');
          $data['page'] = 'Plan';
          $data['branch'] = '';
          $data['log_document'] = $this->laporan_model->getLog();

          $data['t_head'] = 'upgrade/desktop/t_head';
          $data['t_foot'] = 'upgrade/desktop/t_foot';
          $data['content'] = 'upgrade/desktop/index';
          $this->load->view("template/desktop/main",$data);
        }
    }

    function freeplan(){
      $team_id = $this->session->userdata('team_id');
      $data['page'] = 'Plan';
      $user_id = $this->session->userdata('user_id');
      $data['branch'] = '';
      $check_status_promo = $this->plan_model->check_status_promo($user_id);

        $data['log_document'] = $this->laporan_model->getLog();
      $data['status_promo'] = $check_status_promo[0]->promo;
        if($this->mobile_detect->isMobile()){
            $data['t_head'] = 'upgrade/mobile/t_head';
          $data['t_foot'] = 'upgrade/mobile/t_foot';
          $data['content'] = 'upgrade/mobile/free_plan';
          $this->load->view("template/mobile/main",$data);
        }
        else{
            $data['t_head'] = 'upgrade/desktop/t_head';
          $data['t_foot'] = 'upgrade/desktop/t_foot';
          $data['content'] = 'upgrade/desktop/free_plan';
          $this->load->view("template/desktop/main",$data);
        }
    }

    function submit_code(){
      $user_id = $this->session->userdata('user_id');
      $kode = $this->input->post('kode');
      $check_promo  = $this->plan_model->check_promo($kode);
      $check_status_promo = $this->plan_model->check_status_promo($user_id);
      if($check_promo == true && $check_status_promo[0]->promo == 0){
        $data = array(
          'promo' => 1
        );
        $update_plan = $this->plan_model->update_status_promo($user_id, $data);
        if($update_plan == true){
          echo 1;
        }else{
          echo 0;
        }
      }else{
        echo 0;
      }
    }
  }
?>
