<?php
require 'vendor/autoload.php';
use Mailgun\Mailgun;
class Userview extends CI_Controller
{
  private $data;
  public function __construct()
  {
          parent::__construct();
          $this->load->helper('url');
          $this->load->model('proposal_model');
            $this->load->model('laporan_model');
            $this->load->model('client_model');
            $this->load->model('user_model');
            $this->load->model('invoice_model');
            $this->load->library('Tcpdf/Pdf');
          // $this->load->model("proposal_model");
  }

      public function cekSign($id)
      {
        $this->proposal_model->cekSign($id_projek);
      }

//      public function shareProposal($id){
//        $length = strlen($id);
//        $length = $length-15;
//        $id_projek = substr($id,5,$length);
//
//        //die($id_projek);
//        $this->proposal_model->updateStatusBaca($id_projek,1);
//        $this->proposal_model->updateLastBaca($id_projek, date('Y-m-d H:i:s'));
//        $this->proposal_model->updateCounterBaca($id_projek);
//        // die($id_projek);
//
//        $detailproposal = $this->proposal_model->shareview_prop($id_projek);
//        $detailkeuntungan = $this->proposal_model->keuntungan($id_projek);
//        $detailsyarat = $this->proposal_model->syarat($id_projek);
//        $detailtahapan = $this->proposal_model->tahapan($id_projek);
//        $detailkemampuan = $this->proposal_model->kemampuan($id_projek);
//        $detailpemberian = $this->proposal_model->pemberian($id_projek);
//        $sign = $this->proposal_model->ceksign($id_projek);
//        // die(print_r($detailpemberian));
//        $additional_section = $this->proposal_model->additional_section($id_projek);
//
//        if($detailproposal->counter_baca == 1)
//        {
//          $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
//                  $domain = "ml.monika.id";
//
//                  # Make the call to the client.
//                  $result = $mgClient->sendMessage($domain, array(
//                    'from'    => 'Monika.id <info@monika.id>',
//                    'to'      => $detailproposal->user_email,
//                    'subject' => 'Proposal Anda sudah dibaca',
//                    'html'    => '<div style="font-family: verdana;">
//                                  <p>Hi '.$detailproposal->user_name.',</p>
//                                    <p>Proposal Anda sudah dibaca '.$detailproposal->nama_pic.'! <br>Semoga segera disetujui ya.</p>
//                                    <p>Good luck!<br>Monika.id</p></div>',
//                  ));
//        }
//
//        $id_team = $detailproposal->team_id;
//
//        //$data['detail_proposal'] = $detailproposal;
//
//        $data['detail_proposal']['namaprojek'] = stripslashes($detailproposal->info_projek);
//        $data['detail_proposal']['id_klien'] = $detailproposal->id_client;
//        $data['detail_proposal']['klien'] = $detailproposal->nama_pic;
//        $data['detail_proposal']['perusahaan'] = $detailproposal->perusahaan;
//        if($detailproposal && ($detailproposal->aboutyou != NULL || $detailproposal->aboutyou != ''))
//        {
//          $data['detail_proposal']['aboutyou'] = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detailproposal->aboutyou));
//        }
//        else
//        {
//          $data['detail_proposal']['aboutyou'] = '';
//        }
//        $data['detail_proposal']['oleh'] = $detailproposal->user_name;
//        $data['detail_proposal']['kontak'] = $detailproposal->user_email;
//         if($detailproposal && ($detailproposal->project_desc != NULL || $detailproposal->project_desc != ''))
//        {
//          $data['detail_proposal']['overview'] = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detailproposal->project_desc));
//        }
//        else
//        {
//          $data['detail_proposal']['overview'] = '';
//        }
//        if($detailproposal && ($detailproposal->nextstep != NULL || $detailproposal->nextstep != ''))
//        {
//          $data['detail_proposal']['nextstep'] = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detailproposal->nextstep));
//        }
//        else
//        {
//          $data['detail_proposal']['nextstep'] = '';
//        }
//        $data['detail_proposal']['teamback'] = $detailproposal->pengalaman;
//         $data['detail_proposal']['tgl'] = $detailproposal->tgl_buat;
//        if($detailkeuntungan && ($detailkeuntungan->keuntungan != NULL || $detailkeuntungan->keuntungan != ''))
//        {
//          $data['detail_proposal']['keuntungan'] = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detailkeuntungan->keuntungan));
//        }
//        else
//        {
//          $data['detail_proposal']['keuntungan'] = '';
//        }
//        $data['detail_proposal']['sign'] = $detailproposal->status;
//        $data['detail_proposal']['baca'] = $detailproposal->counter_baca;
//        $data['detail_proposal']['preview'] = $detailproposal->preview;
//        $data['detail_proposal']['style'] = $detailproposal->style;
//
//
//        $data['detailkeuntungan'] = $detailkeuntungan;
//        $data['detailsyarat'] = $detailsyarat;
//        $data['detailtahapan'] = $detailtahapan;
//        $data['detailkemampuan'] = $detailkemampuan;
//        $data['detailpemberian'] = $detailpemberian;
//        $data['detailtambahan'] = $additional_section;
//        $data['id_projek'] = $id_projek;
//        $data['sign'] = $sign;
//        $data['plan'] = $this->user_model->getcurrentplan($id_team);
//
//        //$listprop = $this->proposal_model->proposal();
//        //$data['proposal'] = $listprop;
//        $data['page'] = 'Proposal : '.$detailproposal->info_projek;
//        $data['content'] = 'proposal/user_view_proposal';
//        $data['proposal_template'] = ''; // <--- disiapkan untuk dynamic template
//        $this->load->view('template/user_view', $data);
//      }

      public function signing()
        {
          $id = $this->input->post('proposal_id');
          $length = strlen($id);
          $length = $length-15;
          $proposal_id = substr($id,5,$length);
          $this->db->trans_begin();
          $this->proposal_model->signing($proposal_id);
          $data_user = $this->proposal_model->getProposalUserAndClient($proposal_id);
          if ($this->db->trans_status() === FALSE)
          {
                  $this->db->trans_rollback();
          }
          else
          {
                  $this->db->trans_commit();
                  $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
                  $domain = "ml.monika.id";

                  # Make the call to the client.
                  $result = $mgClient->sendMessage($domain, array(
                    'from'    => 'Monika.id <info@monika.id>',
                    'to'      => $data_user['user_email'],
                    'subject' => 'Selamat! Proposal Anda sudah disetujui',
                    'html'    => '<div style="font-family: verdana;">
                                  <p>Hi '.$data_user['user_name'].',</p>
                                    <p>Proposal Anda sudah disetujui '.$data_user['nama_pic'].'! <br>Kami ucapkan selamat sekali lagi.</p>
                                    <p>Best,<br>Monika.id</p></div>',
                  ));
          }
          echo json_encode(array("status" => TRUE));
        }

        public function tracking()
        {
          $data = $this->input->post();
          $data_tracking = array(
                  'proposal_id'=>$data['proposal_id'],
                  'user_ip'=>$_SERVER['REMOTE_ADDR'],
                  'tracking_time'=>date('Y-m-d H:i:s')
          );
          $tracking_count = $data['tracking_count'];
          $detail_tracking = $data['proposal_section_track'];
          if($tracking_count == 1)
          {
            $updateStatus = $this->proposal_model->updateStatusBaca($data['proposal_id'],1);
            $tracking = $this->proposal_model->insert_new_tracking($data_tracking,$detail_tracking);
            if($tracking)
            {
              $updateStatus;  
              echo 'berhasil insert tracking';
            }
            else
            { 
              echo 'gagal insert tracking';
            }
          }
          else
          {
            $tracking_id = $this->proposal_model->get_tracking_id($data['proposal_id']);
            //$all_detail_tracking = $this->proposal_model->get_all_detail_tracking($tracking_id->tracking_id);

            //$db_tracking = array();
            //foreach ($all_detail_tracking as $key => $adt) {
             // $db_tracking.push($adt->section);
            //}

            $new_tracking = array();
            foreach ($detail_tracking as $key => $ndt) {
              foreach ($ndt as $ky => $vl) {
                //ky = section_value
                $tracking = $this->proposal_model->update_tracking($tracking_id['tracking_id'],$ky,$vl,$data['proposal_id']);
              }
            }
            echo 1;
          }
        }

        function download($id_projek)
        {
//             $length = strlen($id_projek);
//            $length = $length-15;
//            $id_prop = substr($id_projek,5,$length);
            $length = strlen($id_projek);
            $length = $length-15;
            $id_prop = substr($id_projek,5,$length);
            $detailproposal = $this->proposal_model->lihat_prop_download($id_prop);
            $detailsyarat = $this->proposal_model->syarat($id_prop);
            $detailtahapan = $this->proposal_model->tahapan($id_prop);
            $detailkemampuan = $this->proposal_model->kemampuan($id_prop);
            $detailpengalaman = $this->proposal_model->pengalaman($id_prop);
            $detailpemberian = $this->proposal_model->pemberian($id_prop);
            $detailkeuntungan = $this->proposal_model->keuntungan($id_prop);
            $sign = $this->proposal_model->ceksign($id_prop);
            $additional_section = $this->proposal_model->additional_section($id_prop);

            if($detailproposal && ($detailproposal->project_desc != NULL || $detailproposal->project_desc != ''))
            {
              $overview = nl2br($detailproposal->project_desc);
            }
            else
            {
              $overview = NULL;
            }

            $data = array(
                "detail_proposal" => $detailproposal,
                "detailsyarat" => $detailsyarat,
                "detailtahapan" => $detailtahapan,
                "detailkemampuan" => $detailkemampuan,
                "detailpengalaman" => $detailpengalaman,
                "detailpemberian" => $detailpemberian,
                "detailkeuntungan" => $detailkeuntungan,
                "detailtambahan" => $additional_section,
                "sign" => $sign,
                "p_desc" => $overview
            );
            $titleProyek = $detailproposal->info_projek;

            $log = array(
                  'proposal_id'=>$id_prop,
                  'section'=>'proposal',
                  'action'=>'download',
                  'user_id'=> 0,
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
                  );
            $this->proposal_model->insert_proposal_log($log);

//            $html = $this->load->view('proposal/pdf_proposal', $data, true);
//            $this->load->library('dompdf_gen');
//            $this->dompdf->load_html($html);
//            $this->dompdf->render();
//            $this->dompdf->stream("Proposal ".$titleProyek.".pdf");
            $this->load->view('proposal/fpdf_proposal', $data);
        }

      function shareInvoice($id)
      {
        $length = strlen($id);
        $length = $length-15;
        $id_in = substr($id,5,$length);

        $this->invoice_model->updateStatusBaca($id_in,1);
        $this->invoice_model->updateLastBaca($id_in, date('Y-m-d H:i:s'));
        $this->invoice_model->updateCounterBaca($id_in);

        $id_prop = $this->invoice_model->view($id_in)->id_proposal;
        $id_team = $this->invoice_model->view($id_in)->id_team;

        $random = md5(mt_rand(1,10000));
        $first = substr($random,0,5);
        $last = substr($random,5,10);
        $urlproposalrand = $first.$id_prop.$last;
        $linkproposal = base_url()."share/".$urlproposalrand;
        $data['linkproposal'] = $linkproposal;
        $data['invoice'] = $this->invoice_model->view($id_in);
        $data['get_style'] = $this->invoice_model->getStyle($id_in);
        $data['item'] = $this->invoice_model->getItem($id_in);
        $data['plan'] = $this->monikalib->currentUserPlan();

        if($data['invoice']->counter_baca == 1)
        {
          $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
                  $domain = "ml.monika.id";

                  # Make the call to the client.
                  $result = $mgClient->sendMessage($domain, array(
                    'from'    => 'Monika.id <info@monika.id>',
                    'to'      => $data['invoice']->user_email,
                    'subject' => 'Invoice Anda sudah dibaca',
                    'html'    => '<div style="font-family: verdana;">
                                  <p>Hi '.$data['invoice']->user_name.',</p>
                                    <p>Invoice Anda sudah dibaca '.$data['invoice']->nama_pic.'! <br>Semoga segera proses pembayaran ya.</p>
                                    <p>Good luck!<br>Monika.id</p></div>',
                  ));
        }

        $data['page'] = 'Invoice';

//        $this->load->view("template/user_view",$data);

        if($this->mobile_detect->isMobile()){
            $data['content'] = 'invoice/mobile/userview';
            $this->load->view('template/mobile/userview',$data);
        }
        else{
            $data['content'] = 'invoice/desktop/userview';
            $this->load->view('template/desktop/userview',$data);
        }
      }

    function downloadInvoice($id_invoice)
    {

        $length = strlen($id_invoice);
        $length = $length-15;
        $id_in = substr($id_invoice,5,$length);
        $id_prop = $this->invoice_model->view($id_in)->id_proposal;
        $id_team = $this->invoice_model->view($id_in)->id_team;
        $data['invoice'] = $this->invoice_model->view($id_in);
        $data['get_style'] = $this->invoice_model->getStyle($id_in);
        $data['item'] = $this->invoice_model->getItem($id_in);
        $data['plan'] = $this->user_model->getcurrentplan($id_team);

        $log = array(
                  'id_invoice'=>$id_in,
                  'section'=>'invoice',
                  'action'=>'download',
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
          );
        $this->invoice_model->insert_invoice_log($log);

//        $html = $this->load->view('invoice/pdf_invoice', $data, true);
//        $this->load->library('dompdf_gen');
//        $this->dompdf->load_html($html);
//        $this->dompdf->render();
//        $this->dompdf->stream("Invoice.pdf");
        $this->load->view('invoice/fpdf_invoice', $data);
    }

    public function tracking_invoice(){
      $data = $this->input->post();
      $data_tracking = array(
              'invoice_id'=>$data['invoice_id'],
              'user_ip'=>$_SERVER['REMOTE_ADDR'],
              'tracking_time'=>date('Y-m-d H:i:s')
      );
      $tracking_count = $data['tracking_count'];
      $detail_tracking = $data['invoice_track'];
      //var_dump($detail_tracking);
      if($tracking_count == 1)
      {
        $tracking = $this->invoice_model->insert_new_tracking($data_tracking,$detail_tracking);
        if($tracking)
        {
          echo 1;
        }
        else
        {
          echo 0;
        }
      }
      else
      {
        $tracking_id = $this->invoice_model->get_tracking_id($data['invoice_id']);
        //$all_detail_tracking = $this->proposal_model->get_all_detail_tracking($tracking_id->tracking_id);

        //$db_tracking = array();
        //foreach ($all_detail_tracking as $key => $adt) {
          // $db_tracking.push($adt->section);
        //}

        $new_tracking = array();
        foreach ($detail_tracking as $key => $ndt) {
          foreach ($ndt as $ky => $vl) {
            //ky = section_value
            $tracking = $this->invoice_model->update_tracking($tracking_id['tracking_id'],$ky,$vl,$data['invoice_id']);
          }
        }
        echo 1;
      }
    }

    function proposalshare($id)
    {
        $length = strlen($id);
        $length = $length-15;
        $id_prop = substr($id,5,$length);

        $this->proposal_model->updateStatusBaca($id_prop,1);
        $this->proposal_model->updateLastBaca($id_prop, date('Y-m-d H:i:s'));
        $this->proposal_model->updateCounterBaca($id_prop);
        $getProposal = $this->proposal_model->detailBaruProposal($id_prop);
        $getSection = $this->proposal_model->detailSection($id_prop);
        $getTeamName = $this->client_model->teamName($getProposal->team_id);

        $data['idprop'] = $id;
        $data['team'] = $getTeamName;
        $data['detailProp'] = $getProposal;
        $data['detailSec'] = $getSection;
        $data['style'] = $this->proposal_model->getProposalStyle($id_prop);
        $data['plan'] = $this->monikalib->currentUserPlan();

        $data['page'] = 'Proposal : '.$getProposal->judul;
        if($this->mobile_detect->isMobile()){
            $data['content'] = 'proposal/mobile/userview';
            $this->load->view('template/mobile/userview',$data);
        }
        else{
            $data['content'] = 'proposal/desktop/userview';
            $this->load->view('template/desktop/userview',$data);
        }
    }

    function ketentuan()
    {
        if($this->mobile_detect->isMobile()){
            $data['content'] = 'policy/akses/mobile/ketentuan';
            $this->load->view('policy/akses/mobile/template',$data);
        }
        else{
            $data['content'] = 'policy/akses/desktop/ketentuan';
            $this->load->view('policy/akses/desktop/template',$data);
        }
    }

    function kebijakan()
    {
        if($this->mobile_detect->isMobile()){
            $data['content'] = 'policy/akses/mobile/kebijakan';
            $this->load->view('policy/akses/mobile/template',$data);
        }
        else{
            $data['content'] = 'policy/akses/desktop/kebijakan';
            $this->load->view('policy/akses/desktop/template',$data);
        }
    }

    function getImageSection(){
      $id = $this->input->post('id_img');
      $getImage = $this->proposal_model->getImage($id);
      echo json_encode($getImage);
    }

    function downloadPDF($idproposal){
      $length = strlen($idproposal);
        $length = $length-15;
        $id_prop = substr($idproposal,5,$length);
        $user_id = $this->session->userdata('user_id');
        $team_id = $this->session->userdata('team_id');

        $getProposal = $this->proposal_model->detailBaruProposal($id_prop);
        $getSection = $this->proposal_model->detailSection($id_prop);
        $getTeamName = $this->client_model->teamName($getProposal->team_id);
        $data['team'] = $getTeamName;
        $data['propstyle']= $this->proposal_model->getProposalStyle($id_prop);
        // $data['idprop'] = $id;
        $data['detailProp'] = $getProposal;
        $data['detailSec'] = $getSection;
        $this->load->view("proposal/pdf/".$getProposal->template,$data);
        echo $id_prop;
    }
    function downloadPdfInvoice($id_invoice){
        $length = strlen($id_invoice);
        $length = $length-15;
        $id_in = substr($id_invoice,5,$length);

        $data['invoice'] = $this->invoice_model->view($id_in);
        $data['get_style'] = $this->invoice_model->getStyle($id_in);
        $data['item'] = $this->invoice_model->getItem($id_in);
        $data['urlid'] = $id_invoice;

        $this->load->view('invoice/tcpdf_invoice', $data);
    }
}

 ?>
