<?php
require 'vendor/autoload.php';
use Mailgun\Mailgun;
  class Home extends CI_Controller
  {
    private $data;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('user_model');
        $this->load->model('client_model');
        $this->load->model('proposal_model');
        $this->load->model('laporan_model');
        $this->load->library('session');
        if($this->session->userdata('status') != "login"){
            redirect('login');
        }
  }

    function refer_to_friend()
    {
      $data = $this->input->post();
      $check_referral = $this->user_model->check_referral($data['refer_email']);
      if($check_referral)
      {
        echo 0;
      }
      else
      {
        $data_referral = array(
          'user_id'=> $this->session->userdata('user_id'),
          'referral_email'=> ($data['refer_email'])
        );
        $insert_referral = $this->user_model->insert_referral($data_referral);
        if($insert_referral)
        {
          $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
          $domain = 'ml.monika.id';
          $isiemail = '<p>Hi,</p>
                       <p>Coba Monika.id, aplikasi ini mempermudah kita membuat proposal dan bisa lacak kapan proposal dibaca client.<br>Monika juga bisa untuk membuat invoice dan otomatis lakukan penagihan.</p>
                       <p>Klik di sini > https://app.monika.id</p>
                       <p><br><strong>'.$this->session->userdata('user_name').'</strong></p>
                      ';

                    # Make the call to the client.
                    $result = $mgClient->sendMessage($domain, array(
                      'from'    => $this->session->userdata('user_name').' via Monika.id <info@monika.id>',
                      'to'      => $data['refer_email'],
                      'h:Reply-To'=> $this->session->userdata('user_email'),
                      'subject' => 'Coba software ini untuk permudah buat proposal',
                      'html'    => '<div style="font-family:verdana;font-size:13px;">'.$isiemail.'<br><br>---<p style="font-size:11px;">This email and proposal was created using <a href="https://monika.id">Monika.id</a></p></div>',
                    ));

                    echo 1;
        }
      }
    }

      function dashboard()
      {
          $data['page'] = 'Dashboard';
          $team_id = $this->session->userdata('team_id');
          $user_id = $this->session->userdata('user_id');

          $data['proposal'] = $this->proposal_model->getAllProposal($user_id);
          $data['avg_readtime'] = $this->laporan_model->averageKontenBaca();

          $data['top_proposal'] = $this->laporan_model->getTopFiveProposal();
          $data['top_invoice'] = $this->laporan_model->getTopFiveInvoice();
          $data['log_document'] = $this->laporan_model->getLog();
          $data['best_hour_read'] = $this->laporan_model->bestHourRead();
          $data['best_minute_read'] = $this->laporan_model->bestMinuteRead();
          $data['avg_read'] = $this->laporan_model->avgProposalRead();
          $data['time_read'] = $this->laporan_model->readTimeProposal();
          $where = array('team_id' => $team_id);
          $data['user_client'] = $this->client_model->tampil_where('client', $where)->result();
          $data['AI_client'] = $this->client_model->getCurrentAI()['AUTO_INCREMENT'];
          $data['top_read'] = $this->laporan_model->getTopReadProposal();

          if($this->mobile_detect->isMobile()){
            $data['t_head'] = 'dashboard/mobile/t_head';
            $data['t_foot'] = 'dashboard/mobile/t_foot';
            $data['content'] = 'dashboard/mobile/index';
            $data['search'] = '';
            $this->load->view("template/mobile/main",$data);
          }
          else{
            $data['t_head'] = 'dashboard/desktop/t_head';
            $data['t_foot'] = 'dashboard/desktop/t_foot';
            $data['content'] = 'dashboard/desktop/index';
            //search proposal
            $data['search'] = '<input id="cari-proposal" class="form-search" type="text" placeholder="Search and enter">';
            //end of search proposal
            $this->load->view("template/desktop/main",$data);
          }
      }

      function info()
      {
          $data['page'] = 'Info';

          $team_id = $this->session->userdata('team_id');
          $data['log_document'] = $this->laporan_model->getLog();

          if($this->mobile_detect->isMobile()){
              echo '';
          }
          else{
              $data['branch'] = '';
              $data['search'] = '';
              $data['t_head'] = 'policy/desktop/t_head';
              $data['t_foot'] = 'policy/desktop/t_foot';
              $data['content'] = 'policy/desktop/index';
              $this->load->view('template/desktop/main',$data);
          }

      }

      function policy()
      {
          $data['page'] = 'Policy';

          if($this->mobile_detect->isMobile()){
              $data['branch'] = 'User / Policy';
              $data['back'] = site_url('user');

              $data['t_head'] = 'policy/mobile/t_head';
              $data['t_foot'] = 'policy/mobile/t_foot';
              $data['content'] = 'policy/mobile/index';
              $this->load->view('template/mobile/main',$data);
          }
          else{
              echo '';
          }
      }
  }
?>
