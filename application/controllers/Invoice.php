<?php
require 'vendor/autoload.php';
use Mailgun\Mailgun;
use Cloudinary\Cloudinary;
  class Invoice extends CI_Controller 
  {
    private $data;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('invoice_model');
        $this->load->model('client_model');
        $this->load->model('user_model');
        $this->load->model('laporan_model');
        $this->load->library('session');
        $this->load->library('Tcpdf/Pdf');
        $this->data['tutorial'] = $this->monikalib->getTutorialStatus();
        if($this->session->userdata('status') != 'login' )
        {
            redirect('login');
        }

        // configure Cloudinary API connection
            \Cloudinary::config(array(
                "cloud_name" => "monika-id",
                "api_key" => "665942816591155", /*874837483274837*/
                "api_secret" => "2w5o13cnwyBrErhTUwAtLtSrrv8" /* a676b67565c6767a6767d6767f676fe1 */
            ));
    }

//    public function index()
//    {
//        $data = $this->data;
//        $data['page'] = 'Manajemen Invoice';
//
//        if (($this->invoice_model->index()->num_rows())>0)
//        {
//            $data['flag'] = 1;
//            $query = $this->input->post('query');
//            if(!isset($query)){
//                $data['listInvoice'] = $this->invoice_model->index()->result();
//              }else{
//                $data['listInvoice'] = $this->invoice_model->index()->result();
//                $output = '';
//                $query = $this->input->post('query');
//                $listInvoice = $this->invoice_model->cari_invoice($query);
//                $resultInvoice = $listInvoice->result();
//                if ($listInvoice->num_rows() > 0){
//                      foreach ($resultInvoice as $detail) {
//                            $random = md5(mt_rand(1,10000));
//                            $first = substr($random,0,5);
//                            $last = substr($random,5,10);
//                            $urlrand = $first.$detail->id.$last;
//                            $url = base_url('invoice/lihat/').$urlrand;
////                            die($url);
//                        $output .= '<div class="col-xs-12 mb-short list_invoice" id="div_list_invoice_'.$detail->id.'">';
//                        $output .= '<div  style="height:auto;text-align:left;" class="row white-panel pn">';
//                        $output .= '<div class="row">';
//                        $output .= '<div class="container" style="width:100%">';
//                        $output .= '<a href="'.$url.'"><div class="col-xs-8 white-header"><h5><b>'.$detail->judul.'</b></h5></div></a>';
//                        if($detail->status == 0) {
//                            $output .= '<div class="col-xs-3 col-xs-offset-1" id="proposal_status"><div class="label label-warning text-right prop-stat">Draft / Belum Dibaca</div></div>';
//                        }else if($detail->status == 1) {
//                            $output .= '<div class="col-xs-3 col-xs-offset-1" id="proposal_status"><div class="label label-primary text-right prop-stat">Sudah Dibaca</div></div>';
//                        }else if($detail->status == 2){
//                            $output .= '<div class="col-xs-3 col-xs-offset-1" id="proposal_status"><div class="label label-primary text-right prop-stat">LUNAS</div></div>';
//                        }
//                        $output .= '</div></div>';
//                        $output .= '<div class="row">';
//                        $output .= '<div class="col-md-4 col-sm-2 col-xs-12"><p class="mt"><b>Nama PIC</b></p><p class="pcontent">'.$detail->nama_pic.'</p></div>';
//                        $output .= '<div class="col-md-2 col-sm-2 col-xs-12"><p class="mt"><b>Tanggal Kirim</b></p><p class="pcontent">';
//                          if($detail->send_invoice==''){
//                            $output .= "Not Set";
//                          }else{
//                            $output .= $this->monikalib->format_date_indonesia($detail->send_invoice);
//                          }
//                          $output .= '</p></div>';
//                          $output .= '<div class="col-md-2 col-sm-2 col-xs-12"><p class="mt"><b>Deadline Pembayaran</b></p><p class="pcontent">';
//                          if($detail->due_invoice==''){
//                            $output .= "Not Set";
//                          }else{
//                            $output .= $this->monikalib->format_date_indonesia($detail->due_invoice);
//                          }
//                        $output .= '</p></div>';
//                        $output .= '<div class="col-md-2 col-sm-2 col-xs-12">';
//                        $result = $this->invoice_model->getItem($detail->id);
//                        $total = 0;
//                        foreach($result  as $row){
//                              $total = $total + $row->harga;
//                        }
//                        $tagihan = ((($detail->tax)/100)*$total)+$total;
//                        $output .= '<p class="mt"><b>Total Tagihan</b></p><p class="pcontent">Rp'.number_format($tagihan,'0',',','.').'</p></div>';
//                        $output .= '<div class="col-md-2 col-sm-6 col-xs-12"><span class="btn-group group-action"><a href="'.$url.'" class="btn btn-default edit-invoice tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-fw fa-pencil"></i></a><a class="btn btn-default arsip-in tooltips" data-placement="top" data-original-title="Arsipkan" iid="'.$detail->id.'"><i class="fa fa-fw fa-archive"></i></a></span></div>';
//                        $output .= '</div>';
//                        $output .= '</div>';
//                        $output .= '<span style="font-size:10px;position: absolute;right: 10px;bottom: 5px;">Dibuat pada '.$this->monikalib->format_date_indonesia($detail->created_at).'</span>';
//                        $output .= '</div>';
//                      }
//                } else {
//                      $output .= '<p>';
//                      $output .= '<b>Invoice yang anda cari belum tersedia. Silahkan Klik tombol Buat baru untuk membuat invoice baru</b>';
//                      $output .= '</p>';
//                }
//                echo $output;
//            }
//        }
//        else
//        {
//            $data['flag'] = 0;
//        }
//        // $data['listinvoice'] = $resultInvoice;
//        $data['t_head'] = 'invoice/t_head.php';
//        $data['t_foot'] = 'invoice/t_foot.php';
//        $data['content'] = 'invoice/invoice';
//        $this->load->view("template/main",$data);
//    }

//    public function baru()
//    {
//      $data = $this->data;
//      $id_team = $this->session->userdata('team_id');
//      $data['team_logo'] = $this->user_model->get_team_logo($id_team);
//      $where = array('team_id' => $id_team);
//      $data['user_client'] = $this->client_model->tampil_where('client', $where)->result();
//      $data['page'] = 'Buat Invoice';
//      $data['t_head'] = 'invoice/t_head';
//      $data['t_foot'] = 'invoice/t_foot';
//      $data['content'] = 'invoice/new_invoice';
//      $this->load->view("template/main",$data);
//    }

      function store()
      {
        $data = $this->data;
        $no_in = $this->input->post('no_in');
        $judul = addslashes(htmlentities($this->input->post('judul_in')));
        $note = addslashes(htmlentities($this->input->post('note_in')));
        $tax = $this->input->post('tax_in');
        $send = date('Y-m-d', strtotime($this->input->post('send_in')));
        $due = date('Y-m-d', strtotime($this->input->post('due_in')));
        $id_client = $this->input->post('id_klien');
        $length = strlen($id_client);
        $length = $length-15;
        $id_kl = substr($id_client,5,$length);

        $nama_kl = addslashes(htmlentities($this->input->post('namapic')));
        $nama_pr = addslashes(htmlentities($this->input->post('perusahaan')));
        $em_kl = $this->input->post('email');
        $al_kl = $this->input->post('add_klien');
        $kota_kl = $this->input->post('kota_klien');
        $notelp = $this->input->post('telp');
        $share = $this->input->post('share');

        $biaya = $this->input->post('biaya');
        $item = $this->input->post('item');
        $logo_image = $this->input->post('logo_team');

        if($id_kl == ''){
            $klien = array(
                'nama_pic' => $nama_kl,
                'perusahaan' => $nama_pr,
                'email'=>$em_kl,
                'alamat_usaha' => $al_kl,
                'kota' => $kota_kl,
                'telephone' => $notelp,
                'user_id' => $this->session->userdata('user_id'),
                'team_id' => $this->session->userdata('team_id'),
            );
            $id_klien = $this->client_model->storeClient($klien);
          }else{
            $this->client_model->updateClient($nama_kl,$nama_pr,$em_kl,$notelp,$al_kl,$kota_kl,$id_kl);
            $id_klien = $id_kl;
          }

          $info = array(
              'number' => $no_in,
              'judul' => $judul,
              'note' => $note,
              'tax' => $tax,
              'send_invoice' => $send,
              'due_invoice' => $due,
              'id_user' => $this->session->userdata('user_id'),
              'id_team' => $this->session->userdata('team_id'),
              'id_proposal' => 1,
              'id_client' => $id_klien,
              'share_team' => $share
          );

        $id_in = $this->invoice_model->storeInvoice($info);

        if($id_in)
        {
          foreach ($biaya as $key => $value) {
            $this->invoice_model->insert_item($item[$key],$value,$id_in);
          }

          $save_logo_image = $this->invoice_model->update_logo_image_invoice($id_in,$logo_image);

          $random = md5(mt_rand(1,10000));
          $first = substr($random,0,5);
          $last = substr($random,5,10);
          $urlrand = $first.$id_in.$last;
            return $urlrand;
        }
        else
        {
          echo 0;
        }
      }

    public function generate()
    {
      $data =  $this->data;
      $id_prop = $this->input->post('pid');
      $id_klien = $this->input->post('cid');
      $projek = $this->input->post('projek');

      $info = array(
          'id_proposal' => $id_prop,
          'id_client' => $id_klien,
          'id_user' => $this->session->userdata('user_id'),
          'id_team' => $this->session->userdata('team_id'),
          'judul' => $projek
      );

      $cek_prop = $this->invoice_model->cekFromProposal($id_prop);

      if($cek_prop == false)
      { 
          $id_invoice = $this->invoice_model->storeInvoice($info);
          $detailpemberian = $this->proposal_model->pemberian($id_prop);
          foreach($detailpemberian as $val){
            $this->invoice_model->insert_item($val->pemberian,$val->harga,$id_invoice);
          }
          $this->session->set_flashdata('generateInvoice',1);
      }
      else
      {
          $this->session->set_flashdata('generateInvoice',0);
      }
    }

    function view($id_invoice)
    {
        $length = strlen($id_invoice);
        $length = $length-15;
        $id_in = substr($id_invoice,5,$length);

        // die($id_in);
        $data = $this->data;
        // $id_prop = $this->invoice_model->view($id_in)->id_proposal;
        //$id_team = $this->invoice_model->view($id_invoice)->id_team;
        $id_team = $this->session->userdata('team_id');
        $where = array('team_id' => $id_team);
        $data['user_client'] = $this->client_model->tampil_where('client', $where)->result();
        $data['page'] = 'view invoice';
        $data['t_head'] = 'invoice/t_head.php';
        $data['t_foot'] = 'invoice/t_foot.php';
        $data['content'] = 'invoice/view_invoice';
        $data['invoice'] = $this->invoice_model->view($id_in);
        $data['get_style'] = $this->invoice_model->getStyle($id_in);
        $data['team_logo'] = $this->user_model->get_team_logo($id_team);
        //var_dump($data['team_logo']);die();
        $data['item'] = $this->invoice_model->getItem($id_in);
        $data['plan'] = $this->user_model->getcurrentplan($id_team);

        $this->invoice_model->updateCounterPreview($id_in);

        $random = md5(mt_rand(1,10000));
        $first = substr($random,0,5);
        $last = substr($random,5,10);
        $urlrand = $first.$id_in.$last;
        $link = base_url()."shareinvoice/".$urlrand;
        
        $data['link'] = $link;

        $this->load->view("template/main",$data);
    }

    function update()
    {
        $data = $this->data;
        $no_in = $this->input->post('no_in');
        $judul = addslashes(htmlentities($this->input->post('judul_in')));

        $id_invoice = $this->input->post('id_in');
        $length = strlen($id_invoice);
        $length = $length-15;
        $id_in = substr($id_invoice,5,$length);

        $id_proposal = $this->input->post('id_prop');
        $length = strlen($id_proposal);
        $length = $length-15;
        $id_prop = substr($id_proposal,5,$length);

        $id_klien = $this->input->post('id_klien');
        $length = strlen($id_klien);
        $length = $length-15;
        $id_kl = substr($id_klien,5,$length);

        $note = addslashes(htmlentities($this->input->post('note_in')));
        $tax = $this->input->post('tax_in');
        $send = date('Y-m-d', strtotime($this->input->post('send_in')));
        $due = date('Y-m-d', strtotime($this->input->post('due_in')));
        $biaya = $this->input->post('biaya');
        $item = $this->input->post('item');

        $share = $this->input->post('share');

        // client
        $namapic = addslashes(htmlentities($this->input->post('namapic')));
        $email = $this->input->post('email');
        $telp = addslashes(htmlentities($this->input->post('telp')));
        $perusahaan = addslashes(htmlentities($this->input->post('perusahaan')));
        $al_kl = $this->input->post('add_klien');
        $kota_kl = $this->input->post('kota_klien');
        $user_id = $this->session->userdata('user_id');
        $team_id = $this->session->userdata('team_id');
        // end of client

        // Update proccess base on client
        if($id_kl !== '' ){
          $this->invoice_model->update($no_in, $id_kl, $judul, $note, $tax, $send, $due, $share, $id_in);
          $this->client_model->updateClientDataFromInvoice($al_kl,$kota_kl,$id_kl);
          echo 1;
        }elseif($id_kl == ''){
          $klienbaru = $this->client_model->getCurrentAI();
          $this->client_model->setClient($namapic,$perusahaan,$email,$telp,$al_kl,$kota_kl,$user_id,$team_id);
          $this->invoice_model->update($no_in, $klienbaru['AUTO_INCREMENT'], $judul, $note, $tax, $send, $due, $share, $id_in);
        }
        // end of Update proccess base on client

        $this->invoice_model->delete_item($id_in);
        foreach ($biaya as $key => $value) {
          $this->invoice_model->insert_item($item[$key],$value,$id_in);
        }

        $log = array(
                  'id_invoice'=>$id_in,
                  'section'=>'invoice',
                  'action'=>'update',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
         );
        $logadded = $this->invoice_model->insert_invoice_log($log);

        if ($logadded){
          return 1;
        }else{
          return 0;
        }
    }

    function download($id_invoice){
      $length = strlen($id_invoice);
      $length = $length-15;
      $id_in = substr($id_invoice,5,$length);
//        $id_prop = $this->invoice_model->view($id_in)->id_proposal;
      $data['invoice'] = $this->invoice_model->view($id_in);
      $data['get_style'] = $this->invoice_model->getStyle($id_in);
      $data['item'] = $this->invoice_model->getItem($id_in);
      $data['urlid'] = $id_invoice;
      $log = array(
        'id_invoice'=>$id_in,
        'section'=>'invoice',
        'action'=>'download',
        'user_id'=> $this->session->userdata('user_id'),
        'logtime'=>date('Y-m-d H:i:s'),
        'user_ip'=>$_SERVER['REMOTE_ADDR']
      );
      $this->invoice_model->insert_invoice_log($log);
      $this->load->view('invoice/pdf_invoice', $data);
    }

    function sendInvoice()
    {
      $data = $this->input->post();
      $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
      $domain = "ml.monika.id";

        if($data['client_cc'] == '')
        {
          $email_entity = array(
            'from'    => $data['sender_name'].' <info@monika.id>',
            'to'      => $data['client_email'],
            'h:Reply-To'=> $data['sender_email'],
            'subject' => $data['email_subject'],
            'html'    => '<div style="font-family:verdana;font-size:13px;">'.nl2br($data['email_message']).'<br><br>---<p style="font-size:11px;">This email and invoice was created using <a href="https://monika.id">Monika.id</a></p></div>',
          );
        }
        else
        {
          $email_entity = array(
            'from'    => $data['sender_name'].' <info@monika.id>',
            'to'      => $data['client_email'],
            'cc'      => $data['client_cc'],
            'h:Reply-To'=> $data['sender_email'],
            'subject' => $data['email_subject'],
            'html'    => '<div style="font-family:verdana;font-size:13px;">'.nl2br($data['email_message']).'<br><br>---<p style="font-size:11px;">This email and invoice was created using <a href="https://monika.id">Monika.id</a></p></div>',
          );
        }

      # Make the call to the client.
      $result = $mgClient->sendMessage($domain, $email_entity);

      $result = $mgClient->sendMessage($domain, array(
        'from'    => 'Monika.id <info@monika.id>',
        'to'      => $this->session->userdata('user_email'),
        'subject' => 'Invoice kepada '.$data['client_name'].' berhasil dikirim',
        'html'    => '<div style="font-family: verdana;">
                        <p>'.$this->session->userdata('user_name').',</p>
                          <p>invoice yang ditujukan kepada '.$data['client_name'].' berhasil dikirim melalui email.</p>
                          <p>Best,<br><br>Monika.id</p></div>',
      ));

        $id_invoice = $data['id'];

        $length = strlen($id_invoice);
        $length = $length-15;
        $id_in = substr($id_invoice,5,$length);

        $tgl = date('Y-m-d H:i:s');
        $updateTglkirim = $this->invoice_model->updateTglKirim($tgl,$id_in);

      $log = array(
            'id_invoice'=>$id_in,
            'section'=>'invoice',
            'action'=>'kirim',
            'user_id'=> $this->session->userdata('user_id'),
            'logtime'=>date('Y-m-d H:i:s'),
            'user_ip'=>$_SERVER['REMOTE_ADDR']
          );
      $this->invoice_model->insert_invoice_log($log);

      if($result){
        return 1;
      }
      else{
        return 0;
      }
    }

      function arsip()
      {
          $data = $this->data;
          $id = $this->input->post('id_in');
          $length = strlen($id);
          $length = $length-15;
          $id_in = substr($id,5,$length);
          $log = array(
            'id_invoice'=>$id_in,
            'section'=>'invoice',
            'action'=>'archive',
            'user_id'=> $this->session->userdata('user_id'),
            'logtime'=>date('Y-m-d H:i:s'),
            'user_ip'=>$_SERVER['REMOTE_ADDR']
          );
          $this->invoice_model->insert_invoice_log($log);
          $this->invoice_model->archive($id_in);
      }

      public function get_team_logo()
        {
          $data = $this->input->post();
          $save_logo_image = $this->invoice_model->update_logo_image_invoice($data['id_invoice'],$data['team_logo']);
          if($save_logo_image)
          {
            echo 1;
          }
          else
          {
            echo 0;
          }
        }

      public function upload_image_logo()
        {
          $id_invoice = $this->input->post('invoiceid');
          $random = md5(mt_rand(1,10000));
                $first = substr($random,0,5);
                $last = substr($random,5,10);
                $urlrand = $first.$id_invoice.$last;
          $images = $_FILES["file"];
          $data_upload_logo_image = \Cloudinary\Uploader::upload($images['tmp_name'], array("use_filename" => TRUE));
          $save_logo_image = $this->invoice_model->update_logo_image_invoice($id_invoice,$data_upload_logo_image['secure_url']);
            redirect('invoice/lihat/'.$urlrand);
        }

      function changeStatusPayment()
      {
          $status = $this->input->post('statPay');
          $id = $this->input->post('id_in');

          $log = array(
                  'id_invoice'=>$id,
                  'section'=>'invoice',
                  'action'=>'payment',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
          );
          $this->invoice_model->insert_invoice_log($log);

          $this->invoice_model->updateStatusPayment($status, $id);
      }

      function invoice()
      {
          $data['page'] = 'Invoice';
          //search invoice
          $data['search'] = '<input id="cari-invoice" class="form-search" type="text" placeholder="Search invoice">';
          //end of search invoice
          $cari  = $this->input->post('query');

          $team_id = $this->session->userdata('team_id');
          $data['log_document'] = $this->laporan_model->getLog();
          $data['listInvoice'] = $this->invoice_model->index()->result();

          //mobile view
          if($this->mobile_detect->isMobile()){
              if(!isset($cari))
              {
                  $data['t_head'] = 'invoice/mobile/t_head';
                  $data['t_foot'] = 'invoice/mobile/t_foot';
                  $data['content'] = 'invoice/mobile/index';
                  $this->load->view("template/mobile/main",$data);
              }
              else
              {
                  $output = '';
                  $hasilCariInvoice = $this->invoice_model->cari_invoice($cari);
                  $hasilCari = $hasilCariInvoice->result();
                  if ($hasilCariInvoice->num_rows() > 0)
                  {
                      $data['result'] = $hasilCari;
                      $this->load->view('invoice/mobile/search',$data);
                  }
                  else
                  {
                      $output .= '<p>Invoice yang Anda cari tidak ada.</p>';
                  }
                  echo $output;
              }
          }
          //end of mobile view

          //desktop & tablet view
          else{
              if(!isset($cari))
              {
                  $data['t_head'] = 'invoice/desktop/t_head';
                  $data['t_foot'] = 'invoice/desktop/t_foot';
                  $data['content'] = 'invoice/desktop/index';
                  $this->load->view("template/desktop/main",$data);
              }
              else
              {
                  $output = '';
                  $hasilCariInvoice = $this->invoice_model->cari_invoice($cari);
                  $hasilCari = $hasilCariInvoice->result();
                  if ($hasilCariInvoice->num_rows() > 0)
                  {
                      $data['result'] = $hasilCari;
                      $this->load->view('invoice/desktop/search',$data);
                  }
                  else
                  {
                      $output .= '<p>Invoice yang Anda cari tidak ada.</p>';
                  }
                  echo $output;
              }
          }
          //end of desktop & tablet view
      }

      function create()
      {
          $data['page'] = 'Buat Invoice';

          //list link navigation
          $data['branch'] = '<a href="'.site_url('invoice').'">Invoice</a> / New Invoice';
          //end of list link navigation

          $team_id = $this->session->userdata('team_id');
          $data['log_document'] = $this->laporan_model->getLog();
          $where = array('team_id' => $team_id);
          $data['user_client'] = $this->client_model->tampil_where('client', $where)->result();

          $data['t_head'] = 'invoice/desktop/t_head';
          $data['t_foot'] = 'invoice/desktop/t_foot';
          $data['content'] = 'invoice/desktop/create';
          $this->load->view("template/desktop/main",$data);
      }

        function detail($id_invoice)
        {
            $length = strlen($id_invoice);
            $length = $length-15;
            $id_in = substr($id_invoice,5,$length);

            $data = $this->data;
            $id_prop = $this->invoice_model->view($id_in)->id_proposal;
            $id_team = $this->session->userdata('team_id');
            $id_user = $this->session->userdata('user_id');
            $data['log_document'] = $this->laporan_model->getLog();
            $where = array('team_id' => $id_team);
            $data['user_client'] = $this->client_model->tampil_where('client', $where)->result();
            $data['page'] = 'Detail Invoice';
            $getInvoice = $this->invoice_model->view($id_in);
            $data['invoice'] = $getInvoice;
            $data['get_style'] = $this->invoice_model->getStyle($id_in);
            $data['team_logo'] = $this->user_model->get_team_logo($id_team);
            $data['item'] = $this->invoice_model->getItem($id_in);
            $data['plan'] = $this->user_model->getcurrentplan($id_team);

//            $this->invoice_model->updateCounterPreview($id_in);

            $random = md5(mt_rand(1,10000));
            $first = substr($random,0,5);
            $last = substr($random,5,10);
            $urlrand = $first.$id_in.$last;
            $link = base_url()."shareinvoice/".$urlrand;

            $data['link'] = $link;

            //list link navigation
            $data['branch'] = '<a href="'.site_url('invoice').'">Invoice</a> / Detail Invoice';
            //end of list link navigation

            if(($getInvoice->id_user == $id_user) || (($getInvoice->id_team == $id_team) && ($getInvoice->share_team == 1))){
                if($this->mobile_detect->isMobile()){
                    $data['back'] = site_url('invoice');

                    $data['t_head'] = 'invoice/mobile/t_head';
                    $data['t_foot'] = 'invoice/mobile/t_foot';
                    $data['content'] = 'invoice/mobile/detail';
                    $this->load->view("template/mobile/main",$data);
                }
                else{
                    $data['t_head'] = 'invoice/desktop/t_head';
                    $data['t_foot'] = 'invoice/desktop/t_foot';
                    $data['content'] = 'invoice/desktop/detail';
                    $this->load->view("template/desktop/main",$data);
                }
            }
            else{
              redirect("404_override");
            }
        }

      function log_copy_url()
      {
          $invoice_id = $this->input->post('invoice');

          $length = strlen($invoice_id);
          $length = $length-15;
          $id_inv = substr($invoice_id,5,$length);

          if($id_inv != NULL)
          {
              $logdata = array(
                  'invoice_id'=>$id_inv,
                  'section'=>'invoice',
                  'action'=>'share link',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
              );
              $log = $this->invoice_model->insert_invoice_log($logdata);
              if($log){
                echo 1;
              }
              else{
                echo 0;
              }
          }
          else{
                echo 0;
          }
      }

      function updatePermissionDocument($id)
      {
          $length = strlen($id);
          $length = $length-15;
          $id_inv = substr($id,5,$length);

          $share = $this->input->post('share');
          if($share == 'true'){
              $share = 1;
          }
          else{
              $share = 0;
          }

          $update = $this->invoice_model->updateShareDocument($share,$id_inv);

          $logdata = array(
              'id_invoice'=>$id_inv,
              'section'=>'invoice',
              'action'=>'permission',
              'user_id'=> $this->session->userdata('user_id'),
              'logtime'=>date('Y-m-d H:i:s'),
              'user_ip'=>$_SERVER['REMOTE_ADDR']
          );
          $log = $this->invoice_model->insert_invoice_log($logdata);

          if($log){
              echo 1;
          }
          else{
              echo 0;
          }
      }
  }
?>
