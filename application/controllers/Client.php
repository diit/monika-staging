<?php
  class Client extends CI_Controller{
    public function __construct(){
      parent:: __construct();
      $this->load->helper('url');
      $this->load->helper('form');
      $this->load->model('user_model');
      $this->load->model('client_model');
      $this->load->model('laporan_model');
      $this->load->library('pagination');
      $this->data['tutorial'] = $this->monikalib->getTutorialStatus();
      if($this->session->userdata('status') !== 'login' )
      {
        redirect('login');
      }
    }

    function view(){
      $data = $this->data;
      $team_id = $this->session->userdata('team_id');
      $where = array('team_id' => $team_id);
      $jmlklien = $this->client_model->tampil_where('client' , $where)->num_rows();
      $config['base_url'] = base_url('').'klien/';
      $config['total_rows'] = $jmlklien;
      $config['per_page'] = 10;
      $hal = $this->uri->segment(2);
      $input  = $this->input->post('query');
      if(!isset($input)){
        $query = '';
        $data['klien'] = $this->client_model->klien_per_page($config['per_page'], $hal, $where['team_id'], $query)->result();
        $this->pagination->initialize($config);
        $listcategory = $this->user_model->getUserCategory();
        $data['listcategory'] = $listcategory;
        $data['page'] = 'Kelola Klien';
        $data['t_head'] = 'klien/t_head.php';
        $data['t_foot'] = 'klien/t_foot.php';
        $data['content'] = 'klien/klien.php';
        $this->load->view('template/main.php',$data);
      }else{
        $output = '';
        $query = $this->input->post('query');
        $klien = $this->client_model->klien_per_page($config['per_page'], 0, $where['team_id'], $query);
        $klienhasil = $klien->result();
        if ($klien->num_rows() > 0){
          foreach ($klienhasil as $detail) {
            $output .= '<tr>';
            $output .= '<td>';
            $output .= '<p>';
            $output .= '<b>'.$detail->perusahaan.'</b>';
            $output .= '</p>';
            $output .= '</td>';
            $output .= '<td>';
            $output .= '<p>';
            $output .= '<b>'.$detail->nama_pic.'</b>';
            $output .= '</p>';
            $output .= '<p>'.$detail->telephone.'</p>';
            $output .= '</td>';
            $output .= '<td>';
            $output .= '<p>'.$detail->alamat_usaha.'</p>';
            $output .= '<p>'.$detail->kota.'</p>';
            $output .= '</td>';
            $output .= '<td align="right">';
            $output .= '<div class="btn-group" >';
            $output .= '<button class="btn btn-default btn-edit2" name="btn-edit"  value="'.$detail->id_client.'" type="button">';
            $output .= '<i class="fa fa-fw s fa-pencil"></i>Edit</button>';
            $output .= '<button class="btn btn-default btn-delete2" value="'.$detail->id_client.'" type="button">';
            $output .= '<i class="fa fa-fw fa-remove"></i>Delete</button>';
            $output .= '</div>';
            $output .= '</td>';
            $output .= '</tr>';
          }
        } else {
          $output .= '<tr>';
          $output .= '<td>';
          $output .= '<p>';
          $output .= '<b>Klien Terkait belum belum anda tambahkan</b>';
          $output .= '</p>';
          $output .= '</td>';
          $output .= '</tr>';
        }
        echo $output;
      }
    }
    function addClient(){
      $pic=$this->input->post('pic');
      $perusahaan=$this->input->post('perusahaan');
      $email=$this->input->post('email');
      $telephone=$this->input->post('telephone');
      $alamat=$this->input->post('alamat');
      $kota=$this->input->post('kota');
      $user_id=$this->input->post('user_id');
      $team_id = $this->session->userdata('team_id');
      $tambah = $this->client_model->setClient($pic,$perusahaan,$email,$telephone,$alamat,$kota,$user_id,$team_id);
      if($tambah)
      {
        echo 1;
      }
      else
      {
        echo 0;
      }
    }

    function fetchData(){
        $user_email = $this->session->userdata('user_email');
        $id = $this->input->post('id_client');
        $length = strlen($id);
        $length = $length-15;
        $id_client = substr($id,5,$length);
        $this->load->model('client_model');
        $this->load->model('proposal_model');
        $result=$this->client_model->getClient($id_client);
        echo json_encode($result);
    }

    function updateClient(){
        $id = $this->input->post('id_client');
        $length = strlen($id);
        $length = $length-15;
        $id_client = substr($id,5,$length);

        $pic = $this->input->post('pic');
        $perusahaan = $this->input->post('perusahaan');
        $email = $this->input->post('email');
        $telephone = $this->input->post('telephone');
        $alamat = $this->input->post('alamat');
        $kota = $this->input->post('kota');

        $this->client_model->updateClient($pic,$perusahaan,$email,$telephone,$alamat,$kota,$id_client);
    }

    function delete(){
        $id = $this->input->post('id_client');
        $length = strlen($id);
        $length = $length-15;
        $id_client = substr($id,5,$length);
        $this->client_model->delete($id_client);
    }

    function pilihClient(){
      $id = $_GET['id'];
        $length = strlen($id);
        $length = $length-15;
        $id_client = substr($id,5,$length);
      $klienkita = $this->client_model->getClient($id_client)[0];
      $arrayklien = array(
        'nama' => $klienkita->nama_pic,
        'email' => $klienkita->email,
        'perusahaan' => $klienkita->perusahaan,
        'id' => $klienkita->id_client,
        'alamat' => $klienkita->alamat_usaha,
        'kota' => $klienkita->kota,
        'telp' => $klienkita->telephone
      );
      echo json_encode($arrayklien);
    }

    function cariKlien(){
        $query = $this->input->post('query');
        $team_id = $this->session->userdata('team_id');
        $where = array('team_id' => $team_id);
        $jmlklien = $this->client_model->tampil_where('client' , $where)->num_rows();
        $config['per_page'] = 10;
        $klien = $this->client_model->klien_per_page($config['per_page'], 0, $where['team_id'], $query);
        $data['result'] = $klien->num_rows();
        $data['klien'] = $klien->result();
        if($this->mobile_detect->isMobile()){
            $this->load->view('klien/mobile/search',$data);
        }
        else {
            $this->load->view('klien/desktop/search',$data);
        }
    }

      function klien()
      {
          $team_id = $this->session->userdata('team_id');
          $data['log_document'] = $this->laporan_model->getLog();
          //search klien
          $data['search'] = '<input id="cari-klien" class="form-search" type="text" placeholder="Search klien">';
          //end of search klien
          $data['page'] = 'Klien';

          $where = array('team_id' => $team_id);
          $config['base_url'] = base_url('').'client/klien/';
          $config['total_rows'] = $this->client_model->tampil_where('client' , $where)->num_rows();
          $config['per_page'] = 10;
          $hal = $this->uri->segment(2);
          $cari = $this->input->post('query');

          //mobile view
          if($this->mobile_detect->isMobile()){
              if(!isset($cari))
              {
                  $data['klien'] = $this->client_model->klien_per_page($config['per_page'], $hal, $where['team_id'], $cari)->result();
                  $this->pagination->initialize($config);

                  $data['t_head'] = 'klien/mobile/t_head';
                  $data['t_foot'] = 'klien/mobile/t_foot';
                  $data['content'] = 'klien/mobile/index';
                  $this->load->view("template/mobile/main",$data);
              }
              else
              {
                  $data['t_head'] = 'klien/mobile/t_head';
                  $data['t_foot'] = 'klien/mobile/t_foot';
                  $data['content'] = 'konten';
                  $this->load->view("template/mobile/main",$data);
              }
          }
          //end of mobile view

          //desktop & tablet view
          else{
              if(!isset($cari))
              {
                  $data['klien'] = $this->client_model->klien_per_page($config['per_page'], $hal, $where['team_id'], $cari)->result();
                  $this->pagination->initialize($config);

                  $data['t_head'] = 'klien/desktop/t_head';
                  $data['t_foot'] = 'klien/desktop/t_foot';
                  $data['content'] = 'klien/desktop/index';
                  $this->load->view("template/desktop/main",$data);
              }
              else
              {
                  $data['t_head'] = 'klien/desktop/t_head';
                  $data['t_foot'] = 'klien/desktop/t_foot';
                  $data['content'] = 'konten';
                  $this->load->view("template/desktop/main",$data);
              }
          }
          //end of desktop & tablet view
      }
  }
 ?>
