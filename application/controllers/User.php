<?php
require 'vendor/autoload.php';
use Mailgun\Mailgun;
use Cloudinary\Cloudinary;
// $mailgun = new Mailgun('api_key', new \Http\Adapter\Guzzle6\Client());
class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('MailChimp');
        $this->load->library('google');
        $this->load->model('user_model');
        $this->load->model('user_sosmed');
        $this->load->model('laporan_model');
        $this->load->library('session');
     }

    public function index()
    {
        if($this->session->userdata('status') == 'login' )
        {
            redirect('home');
        }

        else{
            if($this->mobile_detect->isMobile()){
                $data['content'] = 'akses/mobile/login';
                $this->load->view("akses/mobile/template",$data);
            }
            else{
                $data['content'] = 'akses/desktop/login';
                $this->load->view("akses/desktop/template",$data);
            }
        }
    }

    public function notFound(){
        $data['page'] = 'Page Not Found';

        if($this->session->userdata('status') == 'login' ){
            if($this->mobile_detect->isMobile()){
                $data['log_document'] = '';
                $data['branch'] = '';
                $data['back'] = '';
                $data['search'] = '';
                $data['t_head'] = 'errors/mobile/t_head';
                $data['t_foot'] = 'errors/mobile/t_foot';
                $data['content'] = 'errors/mobile/404_user';
                $this->load->view("template/mobile/main",$data);
            }
            else{
                $data['branch'] = '';
                $data['search'] = '';
                $data['t_head'] = 'errors/desktop/t_head';
                $data['t_foot'] = 'errors/desktop/t_foot';
                $data['content'] = 'errors/desktop/404_user';
                $this->load->view("template/desktop/main",$data);
            }
        }
        else{
            if($this->mobile_detect->isMobile()){
                $this->load->view('errors/mobile/404_nonuser');
            }
            else{
                $this->load->view('errors/desktop/404_nonuser');
            }
        }
    }

    public function register()
    {
      if($this->session->userdata('status') == 'login' )
      {
        redirect('home','refresh');
      }
      else
      {
        $inv = $this->input->get('inv');
        if($inv)
        {
          if($inv != '')
          {
              $length = strlen($inv);
              $length = $length-15;
              $invite_id = substr($inv,5,$length);

              $email = $this->user_model->getEmailInvitation($invite_id);
              $data['inv'] = $inv;
              $data['data_inv'] = $email;

              if($this->mobile_detect->isMobile()){
                    $data['content'] = 'akses/mobile/register';
                    $this->load->view("akses/mobile/template",$data);
                }
                else{
                    $data['content'] = 'akses/desktop/register';
                    $this->load->view("akses/desktop/template",$data);
                }
          }
          else
          {
              if($this->mobile_detect->isMobile()){
                    $data['content'] = 'akses/mobile/register';
                    $this->load->view("akses/mobile/template",$data);
                }
                else{
                    $data['content'] = 'akses/desktop/register';
                    $this->load->view("akses/desktop/template",$data);
                }
          }
        }
        else
        {
            if($this->mobile_detect->isMobile()){
                    $data['content'] = 'akses/mobile/register';
                    $this->load->view("akses/mobile/template",$data);
                }
                else{
                    $data['content'] = 'akses/desktop/register';
                    $this->load->view("akses/desktop/template",$data);
                }
        }
      }
    }

    public function resetForm($key)
    {
      $data = array('saltid' => $key );
        if($this->mobile_detect->isMobile()){
            $data['content'] = 'akses/mobile/reset_password';
            $this->load->view('akses/mobile/template',$data);
        }
        else{
            $data['content'] = 'akses/desktop/reset_password';
            $this->load->view('akses/desktop/template',$data);
        }
    }

    function fetchData(){
      $user_id= $this->input->post('user_id');
      $this->load->model('user_model');
      $result=$this->user_model->getUser($user_id);
      if($result==false){
        $response['error']=true;
      }else{
        $response['error']=false;
        $response['user']=$result;
      }
      echo json_encode($response);
    }

    function updateUser(){
      $this->load->model('user_model');
      $user_name = $this->input->post('user_name');
      $user_corp = $this->input->post('user_corp');
      $user_email = $this->input->post('user_email');
      $user_mobile = $this->input->post('user_mobile');
      $alamat = $this->input->post('alamat');
      $kota = $this->input->post('kota');
      $user_id = $this->session->userdata('user_id');
      $team_id = $this->session->userdata('team_id');
      if($team_id == NULL || $team_id == '')
      {
        $team_id = $user_id;
      }
      $result=$this->user_model->updateProfil($user_name,$user_corp,$user_mobile,$alamat,$kota,$user_id,$team_id);
      //set session
      $this->session->set_userdata('user_name',$user_name);
      $this->session->set_userdata('user_corp',$user_corp);
      $this->session->set_userdata('user_mobile',$user_mobile);
      echo "Successfully Saved";
    }

    public function getPassword()
    {
      $rawpass = $this->input->post('new_password');
      $newpass = md5($rawpass);
      $key = $this->input->post('saltid');
      $this->user_model->changepassword($key,$newpass);
      $this->session->set_flashdata('status_pass',1);
      redirect('login');
    }

   public function confirmation($key)
   {
       if($this->user_model->verifyemail($key))
       {
          $datauser = $this->user_model->getUserByEmailHash($key);
          # Make the call to the client.
          $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
          $domain = "ml.monika.id";

                  $result = $mgClient->sendMessage($domain, array(
                    'from'    => 'Adit from Monika.id <aditya.putra@monika.id>',
                    'to'      => $datauser['user_email'],
                    'h:Reply-To'=> 'adit@monika.id',
                    'subject' => 'Selamat bergabung! Ini yang Anda dapatkan dari Monika.id',
                    'html'    => '<div style="font-family: verdana;">
                                    <p style="font-size:16px;font-weight:800;">Akses semua Fitur Plan Agency untuk 14 hari ke depan secara cuma-cuma</p>
                                    <p>Hi '.$datauser['user_name'].',</p>
                                    <p>Saya Adit, founder Monika.id. Saya sangat senang Anda tertarik menjadi pengguna Monika.</p>
                                    <p style="font-weight:800;">Misi kami adalah membuat Anda semakin produktif dengan membuat proses bisnis menjadi simpel, fokus dan lebih cepat.</p>
                                    <p>Gunakan Monika untuk membuat dan mengirimkan dokumen seperti proposal, kontrak dan invoice dalam hitungan menit juga membuat reminder otomatis kepada klien serta melacak cara klien membaca dokumen Anda. Monika adalah semua yang dibutuhkan untuk mengatur bisnis Anda.</p>
                                    <p>Dalam banyak waktu ke depan, saya ataupun tim mungkin akan secara rutin<br>mengirimkan email insight seputar proses penawaran dan penjualan.<br>Kami juga mengharapkan kritik dan masukan Anda karena kami benar-benar ingin membuat usaha Anda jadi semakin mudah. Jadi apabila Anda menemukan hal yang membingungkan atau sesuatu yang seharusnya kami kembangkan, kami ingin mendengarnya. Jangan sungkan untuk mengirimkan email langsung ke saya di adit@monika.id.</p>
                                    <p>OK, cukup sekian dari saya. Sekarang saatnya <a href="'.site_url('login').'">membuat proposal, kontrak atau invoice pertama</a> Anda!</p>
                                    <p>P.S. Anda bisa menggunakan semua <strong>Fitur Agency Plan selama 14 hari masa uji coba</strong> seperti analytics, mengundang tim dan InDocument Feedback</p>
                                    <p><br>Best,<br><br><strong>Aditya D.Putra</strong><br>Founder of Monika.id</p>
                                  </div>'
                  ));
           $this->session->set_flashdata('status_verif',1);
           $this->session->set_userdata('user_status', 1);
           redirect('login');
       }
       else
       {
           $this->session->set_flashdata('status_verif',2);
           redirect('login');
       }
   }

    public function register_user()
    {
      $mailchimp_list_id = '5288411c42';
          $user=array(
          'user_name'=>filter_var($this->input->post('user_name'), FILTER_SANITIZE_STRING),
          'user_corp'=>filter_var($this->input->post('user_corp'), FILTER_SANITIZE_STRING),
          'user_email'=>filter_var($this->input->post('user_email'), FILTER_SANITIZE_STRING),
          'user_password'=>md5(filter_var($this->input->post('user_password'), FILTER_SANITIZE_STRING)),
          'user_mobile'=>$this->input->post('user_mobile'),
          'status' => 0);
          
            $email = $user['user_email'];
            $saltid = md5($email);
            
            $email_check = $this->user_model->email_check($email);
              if($email_check == false)
              {
                //check referral, notif to referrer if there
                $check_invitation = $this->user_model->check_invitation($email);
                if($check_invitation)
                {
                  $team_id = $check_invitation['team_id'];
                  $about = $check_invitation['about'];
                  $kategori = $check_invitation['kategori'];
                  $role = 2;
                }
                else
                {
                  $inv = $this->input->get('inv');
                  if($inv)
                  {
                    if($inv != '')
                    {
                      $length = strlen($inv);
                      $length = $length-15;
                      $invite_id = substr($inv,5,$length);

                      $check_invitation2 = $this->user_model->check_invitation_by_id($invite_id);
                      if($check_invitation2)
                      {
                        $this->user_model->update_email_invitation($invite_id,$email);
                        $team_id = $check_invitation2['team_id'];
                        $about = $check_invitation2['about'];
//                        $kategori = $check_invitation2['kategori'];
                        $role = 2;
                      }
                      else
                      {
                        if($user['user_corp'] == '')
                        {
                          $user['user_corp'] = $user['user_name'];
                        }

                        $data_team = array(
                          'team_name'=> $user['user_corp'],
                          'team_address'=>NULL,
                          'team_city'=>NULL
                        );
                        $team_id = $this->user_model->register_team($data_team);
                        $about = NULL;
                        $kategori = NULL;
                        $role = 1;
                      }
                    }
                    else
                    {
                      if($user['user_corp'] == '')
                      {
                        $user['user_corp'] = $user['user_name'];
                      }

                      $data_team = array(
                        'team_name'=> $user['user_corp'],
                        'team_address'=>NULL,
                        'team_city'=>NULL
                      );
                      $team_id = $this->user_model->register_team($data_team);
                      $about = NULL;
                      $kategori = NULL;
                      $role = 1;
                    }
                  }
                  else
                  {
                    if($user['user_corp'] == '')
                    {
                      $user['user_corp'] = $user['user_name'];
                    }

                    $data_team = array(
                      'team_name'=> $user['user_corp'],
                      'team_address'=>NULL,
                      'team_city'=>NULL
                    );
                    $team_id = $this->user_model->register_team($data_team);
                    $about = NULL;
//                    $kategori = NULL;
                    $role = 1;
                  }
                }

                
                if($team_id)
                {
                  $data_user = array(
                    'user_name'=> $user['user_name'],
                    'user_email'=> $user['user_email'],
                    'user_password'=> $user['user_password'],
                    'user_mobile'=>$user['user_mobile'],
//                    'kategori'=>$kategori,
                    'about'=>$about,
                    'team_id'=>$team_id,
                    'role'=>$role,
                    'create_date' => date("Y-m-d H:i:s")
                  );
                  $user_id = $this->user_model->register_user($data_user);

                  if($user_id)
                  {

                    // Send email to ....
                    $url = base_url()."user/confirmation/".$saltid;

                    $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
                    $domain = "ml.monika.id";

                    # Make the call to the client.
                    $result = $mgClient->sendMessage($domain, array(
                      'from'    => 'Monika.id <info@monika.id>',
                      'to'      => $email,
                      'subject' => 'Monika.id | Aktivasi akun Anda!',
                      'html'    => '<div style="font-family: verdana;">
                                    <p>Hi '.$user['user_name'].',</p>
                                      <p>Proses registrasi Anda tinggal selangkah lagi. Silahkan <a href='.$url.'>klik di sini</a> untuk verifikasi email.</p>
                                      <p>Best,<br><br>Monika.id</p></div>',
                    ));

                    //check referral, notif to referrer if there
                    $check_referral = $this->user_model->check_referral($email);
                    if($check_referral)
                    {
                      $get_user = $this->user_model->getUser($check_referral->user_id);
                      $result2 = $mgClient->sendMessage($domain, array(
                        'from'    => 'Monika.id <info@monika.id>',
                        'to'      => $get_user->user_email,
                        'subject' => 'Limit proposal Anda bertambah',
                        'html'    => '<div style="font-family: verdana;">
                                      <p>Hi '.$get_user->user_name.',</p>
                                        <p>Selamat! Limit proposal Anda bertambah. Rekan Anda, '.$user['user_name'].', telah mendaftar di Monika.</p>
                                        <p>Terima kasih,<br><br>Monika.id</p></div>',
                      ));
                    }

                    if($check_invitation)
                    {
                      //$this->create_example_proposal($user_id,$check_invitation['kategori']);

                      $result3 = $mgClient->sendMessage($domain, array(
                        'from'    => 'Monika.id <info@monika.id>',
                        'to'      => $check_invitation['user_email'],
                        'subject' => $user['user_name'].' telah bergabung ke tim Anda',
                        'html'    => '<div style="font-family: verdana;">
                                      <p>Hi '.$check_invitation['user_name'].',</p>
                                        <p>'.$user['user_name'].' telah bergabung ke tim Anda. Silahkan <a href='.site_url('user').'>klik di sini</a> untuk mengatur akses.</p>
                                        <p>Best,<br><br>Monika.id</p></div>',
                      ));
                    }

                    $result = $this->mailchimp->post("lists/$mailchimp_list_id/members", [
                            'email_address' => $user['user_email'],
                            'merge_fields' => ['FNAME'=>$user['user_name']],
                            'status'        => 'subscribed',
                    ]);

                    //$this->session->set_flashdata('status_regis',1);
                    //redirect('home');

                    $this->session->set_userdata('user_email',$data_user['user_email']);
                    $this->session->set_userdata('user_corp',$data_team['team_name']);
                    $this->session->set_userdata('user_name',$data_user['user_name']);
                    $this->session->set_userdata('user_mobile',$data_user['user_mobile']);
//                    $this->session->set_userdata('user_category',$data_user['kategori']);
                    $this->session->set_userdata('user_about',0);
                    $this->session->set_userdata('status', 'login');
                    $this->session->set_userdata('user_status', $user['status']);
                    $this->session->set_userdata('tutorial',0);
                    $this->session->set_userdata('initial_proposal',0);
                    $this->session->set_userdata('user_id',$user_id);
                    $this->session->set_userdata('trial_day',0);
//                    if($data_user['role'] == 1)
//                    {
//                      $this->session->set_userdata('userskill',NULL);
//                      $this->session->set_userdata('userportfolio',NULL);
//                    }
//                    else
//                    {
//                      $get_skill_portfolio_team = $this->user_model->get_skill_portfolio_team($data_user['team_id']);
//                      if($get_skill_portfolio_team)
//                      {
//                        $this->session->set_userdata('userskill',array_reverse($get_skill_portfolio_team['skill']));
//                        $this->session->set_userdata('userportfolio',array_reverse($get_skill_portfolio_team['portfolio']));
//                      }
//                      else
//                      {
//                        $this->session->set_userdata('userskill',NULL);
//                        $this->session->set_userdata('userportfolio',NULL);
//                      }
//                    }
                    $this->session->set_userdata('team_id',$data_user['team_id']);
                    $this->session->set_userdata('role',$data_user['role']);
                    // $sessionuser = $this->session->set_userdata('status');
                    redirect('home', 'refresh');
                  }
                  else
                  {
                    $this->session->set_flashdata('status_regis',1);
                    redirect('register');
                  }
                }
                else
                {
                  $this->session->set_flashdata('status_regis',1);
                  redirect('register');
                }
              }
              else
              {
                $this->session->set_flashdata('status_regis',2);
                redirect('register');
              }
      }

//    function create_example_proposal($user_id, $kategori){
//          $this->load->model('proposal_model');
//          $this->load->model('template_model');
//            $this->load->model('projek_model');
//            $this->load->model('kemampuan_model');
//            $this->load->model('pengalaman_model');
//            $this->load->model('pemberian_model');
//            $this->load->model('syarat_model');
//            $this->load->model('tahapan_model');
//            $this->load->model('keuntungan_model');
//
//          $user_email = $this->session->userdata('user_email');
//          $detailproposal = $this->template_model->lihat_prop($kategori,$user_id);
//          foreach ($detailproposal as $dp) {
//            $idproposal = $dp->id;
//            $detailprojek = $this->template_model->projek($idproposal);
//            $detailsyarat = $this->template_model->syarat(1); //sementara set 1
//            $detailtahapan = $this->template_model->tahapan(1); //sementara set 1
//            $detailkemampuan = $this->template_model->kemampuan($idproposal);
//            $detailpengalaman = $this->template_model->pengalaman($idproposal);
//            $detailpemberian = $this->template_model->pemberian($idproposal);
//            $detailkeuntungan = $this->template_model->keuntungan($idproposal);
//            $additional_section = $this->template_model->additional_section($idproposal);
//
//              $nama_projek = $detailprojek['info_projek'];
//              $project_desc =  $detailprojek['project_desc'];
//              $aboutyou =  $dp->aboutyou;
//              $nextstep=  $detailprojek['nextstep'];
//              //$pengalaman = $dtcopy->pengalaman;
//              if($detailkeuntungan)
//              {
//                $keuntungan = $detailkeuntungan->keuntungan;
//              }
//              else
//              {
//                $keuntungan = NULL;
//              }
//
//            $tglbuat = date('Y-m-d H:i:s');
//
//            $nama_projekbaru = $nama_projek;
//
//            // die($nama_projekbaru);
//
//            $idproposal_new = $this->proposal_model->insert_proposal($user_id,$user_email,$aboutyou,1,0,$tglbuat);
//
//            $this->projek_model->insert_projek($nama_projekbaru,$project_desc,$user_id,$nextstep,$idproposal_new);
//
//            //Insert KEMAMPUAN
//            //$this->pengalaman_model->insert_pengalaman($pengalaman,$idproposal_new);
//            foreach($detailkemampuan as $val){
//              $this->kemampuan_model->insert_kemampuan($val->skill,$idproposal_new);
//            }
//
//            //Insert Pengalaman
//            foreach($detailpengalaman as $val){
//              $this->pengalaman_model->insert_pengalaman($val->pengalaman,$idproposal_new);
//            }
//
//            //Insert Tahapan
//            foreach($detailtahapan as $val){
//              $this->tahapan_model->insert_tahapan($val->tahapan,$val->tanggal,$idproposal_new);
//            }
//
//            //Insert Keuntungan
//            if($keuntungan != NULL)
//            {
//              $this->keuntungan_model->insert_keuntungan($keuntungan,$idproposal_new);
//            }
//            foreach($detailpemberian as $val){
//              $this->pemberian_model->insert_pemberian($val->pemberian,$val->harga,$idproposal_new);
//            }
//
//            //Insert Syarat dan ketentuan
//              if($idproposal != 28)
//              {
//                foreach($detailsyarat as $val){
//                  $this->syarat_model->insert_syarat($val->syaratketentuan,$idproposal_new);
//                }
//              }
//
//            //Insert addtional section
//            /*
//            foreach ($additional_section as $as) {
//              $data_section_new = array(
//                    'section_pos'=>$as->section_pos,
//                    'section_name'=>$as->section_name,
//                    'proposal_id'=>$idproposal_new,
//                    'user_id'=>$as->user_id
//                  );
//
//              $section_id_new = $this->proposal_model->insert_new_section($data_section_new);
//
//              $data_section_detail_new = array(
//                                    'section_detail_content'=>$as->section_detail_content,
//                                    'section_id'=>$section_id_new
//                                  );
//
//              $this->proposal_model->insert_section_detail($data_section_detail_new);
//            }
//            */
//
//            //Insert addtional section
//            foreach ($additional_section as $as) {
//              $data_section_new = array(
//                    'section_pos'=>$as->section_pos,
//                    'section_name'=>$as->section_name,
//                    'proposal_id'=>$idproposal_new,
//                    'user_id'=>$as->user_id,
//                    'section_type'=>$as->section_type
//                  );
//
//              $section_id_new = $this->proposal_model->insert_new_section($data_section_new);
//
//              foreach ($as->detail as $dt) {
//                $data_section_detail_new = array(
//                                    'section_detail_content'=>$dt->section_detail_content,
//                                    'section_id'=>$section_id_new
//                                  );
//
//                $this->proposal_model->insert_section_detail($data_section_detail_new);
//              }
//            }
//
//          }
//        }

    function reactivate($kd_aktivasi)
    {
      if(!$kd_aktivasi || $kd_aktivasi == '')
      {
        $this->session->set_flashdata('status','<div class="alert alert-danger"><p>Verifikasi email belum berhasil</p></div>');
        redirect('login');
      }
      else
      {
        // Send email to ....
              $getemail = $this->user_model->get_email($kd_aktivasi);
              $urli = site_url('user/confirmation/').$kd_aktivasi;
                $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
                $domain = "ml.monika.id";

                # Make the call to the client.
                $result = $mgClient->sendMessage($domain, array(
                  'from'    => 'Monika <info@monika.id>',
                  'to'      => $getemail,
                  'subject' => 'Aktivasi akun Anda di Monika.id',
                  'html'    => '<div style="font-family: verdana;">
                                      <p>Hi '.$get_user->user_name.',</p>
                                        <p>Silahkan <a href="'.$urli.'"><strong>klik di sini</strong></a> untuk verifikasi email Anda.</p
                                        <p>Terima kasih,<br><br>Monika.id</p></div>'
                ));

                $this->session->set_flashdata('status','<div class="alert alert-success"><p>Silahkan cek email untuk verifikasi</p></div>');
                redirect('login');
      }
    }

    public function checkEmail(){
      if ($this->input->post('email')!=''){
        $email = $this->input->post('email');
        //$email = "tit.anabrian.05@yahoo.com";
        $r = $this->user_model->email_check($email);
        //die(print_r($r));
        if($r){
          $response= array("status"=>"false");
        }else{
          $response= array("status"=>"true");
        }
        echo json_encode($response);
      }
    }

    public function checkPhone(){
      if ($this->input->post('phone')!=''){
        $phone = $this->input->post('phone');
        $r = $this->user_model->phone_check($phone);
        //die(print_r($r));
        if($r){
          $response= array("status"=>"false");
        }else{
          $response= array("status"=>"true");
        }
        echo json_encode($response);
      }
    }

    public function kirim_password()
    {
          $user=array('user_email'=>$this->input->post('user_email'));
          $email = $_POST['user_email'];
          $saltid     = md5($email);
            //var_dump($user);
          $email_check=$this->user_model->email_check($user['user_email']);

              if($email_check)
              {
                // Send email to ....
                $url = "https://app.monika.id/user/resetform/".$saltid;
                $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
                $domain = "ml.monika.id";

                # Make the call to the client.
                $result = $mgClient->sendMessage($domain, array(
                  'from'    => 'Monika <info@monika.id>',
                  'to'      => $email,
                  'subject' => 'Permintaan reset password',
                  'html'    => "<html><head><head></head><body><p>Hi,</p><p>Permintaan reset password sudah diproses <h3>Monika</h3>.</p><p>Silahkan klik link dibawah ini untuk reset password kamu.</p><a href='/user/resetform/".$saltid."'>Klik Disini</a><br/><p>Salam kami,</p><p>Monika Team</p></body></html>",
                ));


                // End Send email to ....
                  // successfully sent mail to user email

                // $this->session->set_userdata('status', 'login');
                  $this->session->set_flashdata('status',1);
                redirect('login');
                // $this->session->set_flashdata('success_msg', 'Registered successfully.Now login to your account.');
                // redirect('user/login');
              }
              else
              {
                $this->session->set_flashdata('status',2);
                redirect('user');
              }
      }

    public function ForgotPassword()
    {
        $email = $this->input->post('user_email');
        $findemail = $this->user_model->ForgotPassword($email);
        if($findemail){
          if($findemail['status'] == 0)
          {
            $this->session->set_flashdata('status','<div class="alert alert-danger">Akun Anda belum aktif, mohon cek email untuk verifikasi terlebih dahulu. Atau <a href="'.site_url('user/reactivate/'.md5($findemail['user_email'])).'">klik di sini</a> untuk menerima kode aktivasi kembali.</div>');
          }
          else
          {
            $this->session->set_flashdata('status','<div class="alert alert-success"><p>Silahkan cek email anda untuk reset password</p></div>');
            $this->user_model->kirim_password($findemail['user_email']);
          }
       }else{
         $this->session->set_flashdata('status','<div class="alert alert-danger"><p>Email tidak ditemukan</p></div>');
       }
       redirect('login');
   }

    public function login()
    {
        if($this->session->userdata('status') == 'login' ){
            redirect('home');
        }
        else{
            $redirect = $this->input->get('redirect');
            if($redirect)
            {
                if($this->session->userdata('status') == 'login' ){
                    redirect($redirect,'refresh');
                }
                else
                {
                    $data['redirect'] = $redirect;
                    if($this->mobile_detect->isMobile()){
                        $data['content'] = 'akses/mobile/login';
                        $this->load->view("akses/mobile/template",$data);
                    }
                    else{
                        $data['content'] = 'akses/desktop/login';
                        $this->load->view("akses/desktop/template",$data);
                    }
                }
            }
            else
            {
                $data['content'] = 'akses/desktop/login';
                if($this->mobile_detect->isMobile()){
                    $data['content'] = 'akses/mobile/login';
                    $this->load->view("akses/mobile/template",$data);
                }
                else{
                    $data['content'] = 'akses/desktop/login';
                    $this->load->view("akses/desktop/template",$data);
                }
            }
        }
    }

    public function login_user(){
      $user_login=array(

      'user_email'=>$this->input->post('user_email'),
      'user_password'=>$this->input->post('user_password'),

        );

        if($user_login['user_email'] == '' || $user_login['user_password'] == '')
        {
          $this->session->set_flashdata('error_msg', 'Mohon masukkan email dan password');
          redirect('login');
        }
        else
        {
          if($this->user_model->check_account($user_login['user_email']) == FALSE)
          {
            $this->session->set_flashdata('error_msg', 'Email belum terdaftar.');
            redirect('login');
          }
          else
          {
            $data=$this->user_model->login_user($user_login['user_email'],md5($user_login['user_password']));
            //var_dump($data);die();
            if($data)
            {
              if($this->user_model->verified($user_login['user_email'],md5($user_login['user_password'])))
              {
                $this->session->set_userdata('user_email',$data['user_email']);
                $this->session->set_userdata('user_corp',$data['team_name']);
                $this->session->set_userdata('user_name',$data['user_name']);
//                $this->session->set_userdata('user_mobile',$data['user_mobile']);
//                $this->session->set_userdata('user_category',$data['kategori']);
                $this->session->set_userdata('user_about',$data['about']);
                $this->session->set_userdata('status', 'login');
                $this->session->set_userdata('user_status', $data['status']);
                $this->session->set_userdata('tutorial',$data['tutorial']);
//                $this->session->set_userdata('initial_proposal',$data['initial_proposal']);
                $this->session->set_userdata('user_id',$data['user_id']);
                $this->session->set_userdata('trial_day',$data['trial_day']);
//                if($data['role'] == 1)
//                {
//                  $this->session->set_userdata('userskill',array_reverse($data['skill']));
//                  $this->session->set_userdata('userportfolio',array_reverse($data['portfolio']));
//                }
//                else
//                {
//                  $get_skill_portfolio_team = $this->user_model->get_skill_portfolio_team($data['team_id']);
//                  if($get_skill_portfolio_team)
//                  {
//                    $this->session->set_userdata('userskill',array_reverse($get_skill_portfolio_team['skill']));
//                    $this->session->set_userdata('userportfolio',array_reverse($get_skill_portfolio_team['portfolio']));
//                  }
//                  else
//                  {
//                    $this->session->set_userdata('userskill',array_reverse($data['skill']));
//                    $this->session->set_userdata('userportfolio',array_reverse($data['portfolio']));
//                  }
//                }
                $this->session->set_userdata('team_id',$data['team_id']);
                $this->session->set_userdata('role',$data['role']);
                // $sessionuser = $this->session->set_userdata('status');
                $redirect = $this->input->get('redirect');
                if($redirect)
                {
                  if($redirect != '')
                  {
                    redirect($redirect,'refresh');
                  }
                  else
                  {
                    redirect('home', 'refresh');
                  }
                }
                else
                {
                  redirect('home', 'refresh');
                }
              }
              else
              {
                $this->session->set_flashdata('error_msg', 'Akun Anda belum aktif, mohon cek email untuk verifikasi terlebih dahulu. Atau <a href="'.site_url('user/reactivate/'.md5($data['user_email'])).'">klik di sini</a> untuk menerima kode aktivasi kembali. Bila dalam beberapa saat Anda belum menerima email, <a id="chat" href="#">klik di sini</a> untuk berbicara dengan Customer Service');
                $redirect = $this->input->get('redirect');
                if($redirect)
                {
                  if($redirect != '')
                  {
                    redirect('login/?redirect='.$redirect,'refresh');
                  }
                  else
                  {
                    redirect('login');
                  }
                }
                else
                {
                  redirect('login');
                }
              }
            }
            else
            {
              $this->session->set_flashdata('error_msg', 'Email / password yang dimasukkan salah');
              $redirect = $this->input->get('redirect');
              if($redirect)
                {
                  if($redirect != '')
                  {
                    redirect('login/?redirect='.$redirect,'refresh');
                  }
                  else
                  {
                    redirect('login');
                  }
                }
                else
                {
                  redirect('login');
                }
            }
          }
        }
    }
    public function logout()
    {
      $this->session->sess_destroy();
      redirect('login', 'refresh');
    }

    function setTutorial(){
      if (!empty($this->input->post('id'))){
          $id = $this->input->post('id');
          $tutorial = $this->input->post('tutorial');
          $this->user_model->setTutorial($id,$tutorial);
          $this->session->set_userdata('tutorial',$tutorial);
          echo 1;
      }else{
        echo 0;
      }
    }

    function set_initial_proposal(){
      if (!empty($this->input->post('email'))){
        $email = $this->input->post('email');
        $initial_proposal = $this->input->post('initial_proposal');
        $this->user_model->setInitialProposal($email,$initial_proposal);
        $this->session->set_userdata('initial_proposal',$initial_proposal);
      }else{
        echo 'error';
      }
    }

//    function setting()
//    {
//      $data['tutorial'] = $this->monikalib->getTutorialStatus();
//      $user_id = $this->session->userdata('user_id');
//      $data['profil'] = $this->user_model->getUser($user_id);
//      $data['logo'] = $this->user_model->get_team_logo($this->session->userdata('team_id'));
//      $data['tim'] = $this->user_model->get_team_member($this->session->userdata('team_id'));
//      //$data['content'] = 'user/setting.php';
//      //$this->load->view("main_template.php",$data);
//
//      $data['page'] = 'Profil';
//      $data['t_head'] = 'user/t_head.php';
//      $data['t_foot'] = 'user/t_foot.php';
//      $data['content'] = 'user/setting.php';
//      $this->load->view("template/main.php",$data);
//    }

    public function upload_image_logo()
    {
          // configure Cloudinary API connection
            \Cloudinary::config(array(
                "cloud_name" => "monika-id",
                "api_key" => "665942816591155", /*874837483274837*/
                "api_secret" => "2w5o13cnwyBrErhTUwAtLtSrrv8" /* a676b67565c6767a6767d6767f676fe1 */
            ));
          $team_id = $this->session->userdata('team_id');
          $images = $_FILES["userfile"];
          $data_upload_logo_image = \Cloudinary\Uploader::upload($images['tmp_name'], array("use_filename" => TRUE));
          $save_logo_image = $this->user_model->update_team_logo_image($team_id,$data_upload_logo_image['secure_url']);
          redirect('user');
    }

    function referral()
    {
      $data['tutorial'] = $this->monikalib->getTutorialStatus();
        $data['page'] = 'Referral';
        $data['t_head'] = 'user/t_head';
        $data['t_foot'] = 'user/t_foot';
      $data['content'] = 'user/referral.php';
      $this->load->view("template/main",$data);
    }

    function invite_team()
    {
      $invites = $this->input->post('invites');
      foreach ($invites as $inv) 
      {
        $datainvite = array(
          'user_id'=> $this->session->userdata('user_id'),
          'email'=>$inv
        );
        $invite = $this->user_model->invite_team($datainvite);
        if($invite)
        {
          $check_user = $this->user_model->email_check($inv);
          if($check_user == FALSE)
          {
            $random = md5(mt_rand(1,10000));
            $first = substr($random,0,5);
            $last = substr($random,5,10);
            $urlrand = $first.$invite.$last;
            $url_invitation = 'Silahkan <a href='.site_url('register/?inv='.$urlrand).'>klik di sini</a> untuk membuat akun.';
          }
          else
          {
            $url_invitation = 'Silahkan <a href='.site_url('login/?redirect=user/join_team/'.$this->session->userdata('user_id')).'>klik di sini</a> untuk bergabung.';
          }
          $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
                    $domain = "ml.monika.id";

                    # Make the call to the client.
                    $result = $mgClient->sendMessage($domain, array(
                      'from'    => 'Monika.id <info@monika.id>',
                      'to'      => $inv,
                      'subject' => $this->session->userdata['user_name'].' mengundang Anda menggunakan Monika.id',
                      'html'    => '<div style="font-family: verdana;">
                                    <p>Hi,</p>
                                      <p>'.$this->session->userdata['user_name'].' mengundang Anda sebagai tim untuk menggunakan Monika.id. '.$url_invitation.'</p>
                                      <p>Best,<br><br>Monika.id</p></div>',
                    ));
          echo 1;
        }
        else
        {
          echo 0;
        }
      }
    }

    function join_team($invitor)
    {
      if($invitor == NULL)
      {
        redirect('home');
      }
      else
      {
        $join = $this->user_model->join_team($invitor,$this->session->userdata('user_id'));
        if($join)
        {
          $this->session->set_userdata('team_id',$join->team_id);
          $this->session->set_userdata('role',2);
          redirect('user/setting');
        } 
        else
        {
          redirect('home');
        }       
      }
    }

//    public function syaratketentuan()
//    {
//      $data['content'] = 'about/syaratketentuan.php';
//      $this->load->view("about/about_template.php",$data);
//    }
//
//    public function kebijakanprivasi()
//    {
//      $data['content'] = 'about/kebijakanprivasi.php';
//      $this->load->view("about/about_template.php",$data);
//    }

    public function faq()
    {
//      $data['content'] = 'about/faq.php';
//      $this->load->view("about/about_template.php",$data);

        $data['page'] = 'Bantuan';

        if($this->mobile_detect->isMobile()){
            $data['branch'] = 'User / Bantuan';
            $data['back'] = site_url('user');

            $data['t_head'] = 'policy/mobile/t_head';
            $data['t_foot'] = 'policy/mobile/t_foot';
            $data['content'] = 'policy/mobile/faq';
            $this->load->view("template/mobile/main",$data);
        }
        else {
            echo '';
        }
    }

    public function carabuatproposal()
    {
      $data['content'] = 'about/carabuatproposal.php';
      $this->load->view("about/about_template.php",$data);
    }

    public function carabuatinvoice()
    {
      $data['content'] = 'about/carabuatinvoice.php';
      $this->load->view("about/about_template.php",$data);
    }

    public function pc_webhook()
    {
      $inputJSON = file_get_contents('php://input');
      $data = json_decode($inputJSON, TRUE);
      $mailchimp_list_id = '5288411c42';

      $token = '9aNXjATljCtKumBWboCaI5O5O2C5iDsR';
      if($data['token'] == $token)
      {
        $result = $this->mailchimp->post("lists/$mailchimp_list_id/members", [
                'email_address' => $data['item']['payload']['email'],
                'merge_fields' => ['FNAME'=>$data['item']['payload']['pic']],
                'status'        => 'subscribed',
        ]);

        if($result)
        {
          $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
                  $domain = "ml.monika.id";

                  # Make the call to the client.
                  $result = $mgClient->sendMessage($domain, array(
                    'from'    => 'Monika.id <info@monika.id>',
                    'to'      => $data['item']['payload']['email'],
                    'subject' => 'Anda berkesempatan mencoba Monika, '.$data['item']['payload']['pic'],
                    'html'    => '<div style="font-family: verdana;"><p>Hi '.$data['item']['payload']['pic'].',</p>
                                    <p>Kami sangat senang Anda tertarik menjadi pengguna Monika.</p>
                                    <p style="font-weight:800;">Misi kami adalah membantu kesuksesan Anda melalui proposal yang terbaik.</p>
                                    <p>Kami tahu bahwa mengerjakan proyek saja sudah sangat memakan waktu dan pikiran<br>(kami pun mengalaminya sebelum tercetus ide ini).<br>Kami membuat Monika untuk membantu Anda lebih produktif dan pastinya<br>membuat usaha Anda menjadi lebih profesional di mata klien.</p>
                                    <p>Sekarang saatnya mencoba dan daftarkan kembali email Anda <a href="'.site_url('register').'">di sini</a>.</p>
                                    <p><br>Best,<br><br><strong>Tim Monika</strong></p>
                                  </div>'
                  ));
        }
      }
      else
      {
        echo 'empty';
      }
    }

    public function messenger_webhook()
    {
      $mailchimp_list_id = '5288411c42';
      parse_str(file_get_contents("php://input"), $data);

      $result = $this->mailchimp->post("lists/$mailchimp_list_id/members", [
                'email_address' => $data['email'],
                'merge_fields' => ['FNAME'=>$data['first_name']],
                'status'        => 'subscribed',
      ]);

        if($result)
        {

          $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
                  $domain = "ml.monika.id";

                  # Make the call to the client.
                  $result = $mgClient->sendMessage($domain, array(
                    'from'    => 'Monika.id <info@monika.id>',
                    'to'      => $data['email'],
                    'subject' => 'Anda berkesempatan mencoba Monika, '.$data['first_name'],
                    'html'    => '<div style="font-family: verdana;"><p>Hi '.$data['first_name'].',</p>
                                    <p>Kami sangat senang Anda tertarik menjadi pengguna Monika.</p>
                                    <p style="font-weight:800;">Misi kami adalah membantu kesuksesan Anda melalui proposal yang terbaik.</p>
                                    <p>Kami tahu bahwa mengerjakan proyek saja sudah sangat memakan waktu dan pikiran<br>(kami pun mengalaminya sebelum tercetus ide ini).<br>Kami membuat Monika untuk membantu Anda lebih produktif dan pastinya<br>membuat usaha Anda menjadi lebih profesional di mata klien.</p>
                                    <p>Sekarang saatnya mencoba dan daftarkan kembali email Anda <a href="'.site_url('register').'">di sini</a>.</p>
                                    <p><br>Best,<br><br><strong>Tim Monika</strong></p>
                                  </div>'
                  ));
        }
      
    }

    function newlogin()
    {
//        $data['content'] = 'akses/desktop/login';
        $this->load->view("login");
    }

    function newregister()
    {
        $data['content'] = 'akses/desktop/register';
        $this->load->view("akses/desktop/template",$data);
    }

    function user()
    {
        if($this->session->userdata('status') == 'login' )
        {
            if($this->mobile_detect->isMobile()){
                $user_id = $this->session->userdata('user_id');
                $data['profil'] = $this->user_model->getUser($user_id);
                $id_team = $this->session->userdata('team_id');
                $data['profil'] = $this->user_model->getUser($user_id);
                $data['logo'] = $this->user_model->get_team_logo($id_team);
                $data['page'] = 'User';

                $data['t_head'] = 'user/mobile/t_head';
                $data['t_foot'] = 'user/mobile/t_foot';
                $data['content'] = 'user/mobile/index';
                $this->load->view("template/mobile/main",$data);
            }
            else{
                $data['page'] = 'User Profile';
                $data['branch'] = '';

                $id_team = $this->session->userdata('team_id');
                $data['log_document'] = $this->laporan_model->getLog();
                $user_id = $this->session->userdata('user_id');
                $data['profil'] = $this->user_model->getUser($user_id);
                $data['logo'] = $this->user_model->get_team_logo($id_team);
                $data['tim'] = $this->user_model->get_team_member($id_team);

                $data['t_head'] = 'user/desktop/t_head';
                $data['t_foot'] = 'user/desktop/t_foot';
                $data['content'] = 'user/desktop/index';
                $this->load->view("template/desktop/main",$data);
            }
        }
        else{
            redirect('login');
        }
    }

    function detailprofile()
    {
        $data['page'] = 'Profile';

        if($this->mobile_detect->isMobile()){
            $user_id = $this->session->userdata('user_id');
            $data['profil'] = $this->user_model->getUser($user_id);
            $id_team = $this->session->userdata('team_id');
            $data['logo'] = $this->user_model->get_team_logo($id_team);

            $data['branch'] = 'User / Profile';
            $data['back'] = site_url('user');

            $data['t_head'] = 'user/mobile/t_head';
            $data['t_foot'] = 'user/mobile/t_foot';
            $data['content'] = 'user/mobile/profile';
            $this->load->view("template/mobile/main",$data);
        }
        else{
            echo '';
        }
    }

    function team()
    {
        $data['page'] = 'Tim Member';

        if($this->mobile_detect->isMobile()){
            $user_id = $this->session->userdata('user_id');
            $data['profil'] = $this->user_model->getUser($user_id);

            $data['branch'] = 'User / Tim Member';
            $data['back'] = site_url('user');

            $user_id = $this->session->userdata('user_id');
            $team_id = $this->session->userdata('team_id');
            $data['profil'] = $this->user_model->getUser($user_id);
            $data['logo'] = $this->user_model->get_team_logo($team_id);
            $data['tim'] = $this->user_model->get_team_member($team_id);

            $data['t_head'] = 'user/mobile/t_head';
            $data['t_foot'] = 'user/mobile/t_foot';
            $data['content'] = 'user/mobile/team';
            $this->load->view("template/mobile/main",$data);
        }
        else{
            echo '';
        }
    }

    function delete_team_logo(){
      $team_id = $this->input->post('team_id');
      $value = null;
      $delete_logo = $this->user_model->update_team_logo_image($team_id,$value);
      if($delete_logo){
        echo 1;
      }else{
        echo 0;
      }
    }

    function update_usaha(){
        $name = $this->input->post('name');
        $desc = $this->input->post('desc');

        $update = $this->user_model->updateUsaha($name);
        $about = $this->user_model->updateAbout($desc);

        if($update){
            echo 1;
        }
        else{
            echo 0;
        }
    }
}
