<?php
  class Arsip extends CI_Controller
  {
    private $data;
    public function __construct()
    {
      parent:: __construct();
      $this->load->helper('url');
      $this->load->model('arsip_model');
      $this->load->model('user_model');
      $this->load->model('proposal_model');
      $this->load->model('laporan_model');
      $this->data['tutorial'] = $this->monikalib->getTutorialStatus();
      if($this->session->userdata('status') !== 'login' )
      {
        redirect('login');
      }
    }

    public function index()
    {
      $data = $this->data;
      $listProposal = $this->arsip_model->arsip();
      $listInvoice = $this->arsip_model->arsipInvoice();
      $data['arsip_prop'] = $listProposal;
      $data['arsip_invoice'] = $listInvoice;
      $data['page'] = 'Arsip Proposal dan Invoice';
      $data['t_head'] = 'arsip/t_head.php';
      $data['t_foot'] = 'arsip/t_foot.php';
      $data['content']= 'arsip/arsip.php';
      $this->load->view("template/main.php",$data);
    }
      function archive()
      {
          $data = $this->data;
          $id = $this->input->post('pid');
          $length = strlen($id);
          $length = $length-15;
          $id_prop = substr($id,5,$length);
          $log = array(
                  'proposal_id'=>$id_prop,
                  'section'=>'proposal',
                  'action'=>'archive',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
          );
          $this->proposal_model->insert_proposal_log($log);
          $this->arsip_model->updateArchive($id_prop);
      }
    function unarchive(){
        $id_prop = $this->input->post('id_p');
        $length = strlen($id_prop);
        $length = $length-15;
        $id = substr($id_prop,5,$length);
        $unarchive = $this->arsip_model->updateUnArchive($id);
        $log = array(
                  'proposal_id'=>$id,
                  'section'=>'proposal',
                  'action'=>'unarchive',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
        );
        $history = $this->proposal_model->insert_proposal_log($log);

        if($history){
            return 1;
        }
        else{
            return 0;
        }
    }

    function unarchiveInvoice(){
        $id_inv = $this->input->post('id_i');
        $length = strlen($id_inv);
        $length = $length-15;
        $id = substr($id_inv,5,$length);
        $this->arsip_model->updateUnArchiveInvoice($id);
        $log = array(
                  'id_invoice'=>$id,
                  'section'=>'invoice',
                  'action'=>'unarchive',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
        );
        $history = $this->invoice_model->insert_invoice_log($log);

        if ($history){
            return 1;
        }
        else{
            return 0;
        }
    }

    function cariProposalArsip(){
        $query = $this->input->post('query');
        $arsip = $this->arsip_model->cariProposalArsip($query);

        $data['result'] = $arsip->num_rows();
        $data['arsip'] = $arsip->result();

        if($this->mobile_detect->isMobile()){
            $this->load->view('arsip/mobile/search_proposal',$data);
        }
        else{
            $this->load->view('arsip/desktop/search_proposal',$data);
        }
    }
    function cariInvoiceArsip(){
        $query = $this->input->post('query');
        $arsip = $this->arsip_model->cariInvoiceArsip($query);

        $data['result'] = $arsip->num_rows();
        $data['arsip'] = $arsip->result();

        if($this->mobile_detect->isMobile()){
            $this->load->view('arsip/mobile/search_invoice',$data);
        }
        else{
            $this->load->view('arsip/desktop/search_invoice',$data);
        }
    }

      function arsip()
      {
          $team_id = $this->session->userdata('team_id');
          $data['log_document'] = $this->laporan_model->getLog();
          $data['arsip_prop'] = $this->arsip_model->arsip();
          $data['arsip_invoice'] = $this->arsip_model->arsipInvoice();

          $data['page'] = 'Arsip';

          if($this->mobile_detect->isMobile()){
            //search arsip
            $data['search'] = '<input id="cari-proposal" class="form-search" type="text" placeholder="Search arsip proposal"><input id="cari-invoice" class="form-search" type="text" placeholder="Search arsip invoice">';
            //end of search arsip

            $data['t_head'] = 'arsip/mobile/t_head';
            $data['t_foot'] = 'arsip/mobile/t_foot';
            $data['content'] = 'arsip/mobile/index';
            $this->load->view("template/mobile/main",$data);
          }
          else{
              $data['t_head'] = 'arsip/desktop/t_head';
              $data['t_foot'] = 'arsip/desktop/t_foot';
              $data['content'] = 'arsip/desktop/index';

              //search arsip proposal
              $data['search1'] = '<input id="cari-proposal" class="form-search" type="text" placeholder="Search arsip proposal">';
              //end of search arsip proposal

              //search arsip invoice
              $data['search2'] = '<input id="cari-invoice" class="form-search" type="text" placeholder="Search arsip invoice">';
              //end of search arsip invoice

              $this->load->view("template/desktop/main",$data);
          }
      }
  }


 ?>
