<?php
require 'vendor/autoload.php';
use Mailgun\Mailgun;
use Cloudinary\Cloudinary;
  class Proposal extends CI_Controller
  {
    private $data;
    public function __construct()
    {
            parent::__construct();
            $this->load->helper('url');
            $this->load->model('proposal_model');
            $this->load->model('laporan_model');
            $this->load->model('client_model');
            $this->load->model('user_model');
            $this->load->library('session');
            $this->load->library('Tcpdf/Pdf');
            $this->data['tutorial'] = $this->monikalib->getTutorialStatus();
            $this->data['initial_proposal'] = $this->session->userdata('initial_proposal');
            if($this->session->userdata('status') != "login"){
              redirect("login");
            }

            // configure Cloudinary API connection
            \Cloudinary::config(array(
                "cloud_name" => "monika-id",
                "api_key" => "665942816591155", /*874837483274837*/
                "api_secret" => "2w5o13cnwyBrErhTUwAtLtSrrv8" /* a676b67565c6767a6767d6767f676fe1 */
            ));
    }

//    function index()
//    {
//      $data = $this->data;
//        //var_dump($this->session->userdata('user_category'));die();
//      if($this->input->get('act') && $this->input->get('act') != '')
//      {
//        $data['act'] = $this->input->get('act');
//      }
//        $row=$this->user_model->getUserByEmail($this->session->userdata('user_email'));
//        $count = $this->proposal_model->getProposalByUser($this->session->userdata('user_id'));
//        $listprop = $this->proposal_model->proposal_from_all_member();
//        $user_id = $this->session->userdata('user_id');
//        $team_id = $this->session->userdata('team_id');
//        $where = array('team_id' => $team_id);
//        $input  = $this->input->post('query');
//        if(!isset($input)){
//            $data['user_client'] = $this->client_model->tampil_where('client', $where)->result();
//            $data['listcategory'] = $listcategory;
//            $data['proposal'] = $listprop;
//            $data['user_id'] = $row['user_id'];
//            $data['banyakProposal']=$count;
//            $data['page'] = 'Proposal';
//            $data['t_head'] = 'proposal/t_head.php';
//            $data['t_foot'] = 'proposal/t_foot.php';
//            $data['content'] = 'proposal/proposal.php';
//            $this->load->view("template/main.php",$data);
//        }else{
//            $output = '';
//            $queryCari = $this->input->post('query');
//            $hasilCariProposal = $this->proposal_model->cariProposal($queryCari);
////            die(print_r($hasilCariProposal));
//            $hasilCari = $hasilCariProposal->result();
////            die(print_r($hasilCari));
//            if ($hasilCariProposal->num_rows() > 0){
//                foreach ($hasilCari as $prop){
//                    $random = md5(mt_rand(1,10000));
//                    $first = substr($random,0,5);
//                    $last = substr($random,5,10);
//                    $urlrand = $first.$prop->id.$last;
//                    $output .= '<div class="col-xs-12 mb-short list_proposal" id="div_list_prop_'.$prop->id.'">';
//                    $output .= '<div  style="height:auto;text-align:left;" class="row white-panel pn">';
//                    $output .= '<div class="row"><div class="container" style="width:100%">';
//                    $output .= '<a href="proposal/lihat/'.$urlrand.'"><div class="col-xs-8 white-header"><h5><b>'.$prop->info_projek.'</b></h5></div></a>';
//                    $output .= '<div class="col-xs-3 col-xs-offset-1" id="proposal_status">';
//                    if ($prop->status == 0){
//                        $output .= '<div id="propstat" class="label label-warning text-right prop-stat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Tersimpan</div>';
//                    } elseif ($prop->status == 1){
//                        $output .= '<div id="propstat"class="label label-primary text-right prop-stat"><i class="fa fa-eye" aria-hidden="true"></i>Sudah Dibaca</div>';
//                    } elseif ($prop->status == 2){
//                        $output .= '<div <?php id="propstat" class="label label-success text-right prop-stat"><i class="fa fa-check" aria-hidden="true"></i>Disetujui</div>';
//                    }
//                    $output .= '</div></div></div>';
//                    $output .= '<div class="row">';
//                    $output .= '<div class="col-md-2 col-sm-2 col-xs-12"><p class="mt"><b>Klien</b></p><p class="pcontent">'.$prop->nama_pic.'</p></div>';
//                    $output .= '<div class="col-md-3 col-sm-2 col-xs-12"><p class="mt"><b>Email</b></p><p class="pcontent">'.$prop->email.'</p></div>';
//                    $output .= '<div class="col-md-2 col-sm-2 col-xs-12"><p class="mt"><b>Dipersiapkan oleh</b></p>';
//                    $output .= '<p class="pcontent">';
//                    if($prop->user_id == $this->session->userdata('user_id')){
//                        $output .= 'Anda';
//                    }else{
//                        $output .= $prop->user_name;
//                    }
//                    $output .= '</p>';
//                    $output .= '</div>';
//                    $output .= '<div class="col-md-3 col-sm-6 col-xs-12">';
//                    $output .= '<span class="btn-group group-action" style="padding:5px">';
//                    $output .= '<a class="btn btn-default btn-edit editprop tooltips" data-placement="top" data-original-title="Edit" id="editprop" name="btn-edit" href="proposal/lihat/'.$urlrand.'"><i class="fa fa-fw s fa-pencil" aria-hidden="true"></i></a>';
//                    $output .= '<button class="btn btn-default dupliprop tooltips" data-placement="top" data-original-title="Duplikat" id="duplicatepro" pid="'.$prop->id.'" pname="'.$prop->info_projek.'"><i class="fa fa-fw fa-clone" aria-hidden="true"></i></button>';
//                    if($this->session->userdata('trial_day') <= 14 && $this->session->userdata('role') == 1){
//                        $output .= '<a href="proposal/stat/'.$urlrand.'" class="btn btn-default tooltips" id="analpro" data-placement="top" data-original-title="Statistik"><i class="fa fa-line-chart" aria-hidden="true"></i></a>';
//                    }else{
//                        if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1){
//                            $output .= '<a href="proposal/stat/'.$urlrand.'" class="btn btn-default tooltips" id="analpro" data-placement="top" data-original-title="Statistik"><i class="fa fa-line-chart" aria-hidden="true"></i></a>';
//                        }else{
//                            $output .= '<button class="btn btn-default statpro tooltips" id="analpro" data-placement="top" data-original-title="Statistik" pid="'.$prop->id.'"><i class="fa fa-line-chart" aria-hidden="true"></i></button>';
//                        }
//                    }
//                    $output .= '<button class="btn btn-default btn-delete archprop tooltips" data-placement="top" data-original-title="Arsipkan" pid="'.$prop->id.'"><i class="fa fa-fw fa-archive" aria-hidden="true"></i></button>';
//                    if($prop->id_invoice == NULL){
//                        $output .= '<button class="btn btn-default invoice-prop tooltips" data-placement="top" data-original-title="Generate Invoice" pid="'.$prop->id.'" pname="'.$prop->id_client.'" pproj="'.$prop->info_projek.'"><i class="fa fa-fw fa-list-alt"></i></button>';
//                    }else{
//                        $output .= '<a href="invoice/lihat/'.$prop->id_invoice.'" class="btn btn-default tooltips" data-placement="top" data-original-title="Lihat Invoice"><i class="fa fa-fw fa-list-alt"></i></a>';
//                    }
//                    $output .= '<span></div>';
//                    $output .= '</div></div>';
//                    $output .= '<span style="font-size:10px;position: absolute;right: 10px;bottom: 5px;">Dibuat pada '.$this->monikalib->format_date_indonesia($prop->tgl_buat).'</span>';
//                    $output .= '</div>';
//                }
//            }else{
//                $output .= '<p>Proposal yang anda cari belum tersedia</p>';
//            }
//            echo $output;
//        }
//
//    }

//    function baru()
//    {
//      $data = $this->data;
//      $cat = $this->uri->segment(3);
//      $user_id = $this->session->userdata('user_id');
//      $team_id = $this->session->userdata('team_id');
//      $hasil = $this->client_model->checkClient($team_id);
//      $where = array('team_id' => $team_id);
//      $data['user_client'] = $this->client_model->tampil_where('client', $where)->result();
//      $data['lastclient']= $hasil->result();
//      $data['category']= $cat;
//      $data['ada']= $hasil->num_rows();
//      $data['page'] = 'Proposal Baru';
//      $data['t_head'] = 'proposal/t_head';
//      $data['t_foot'] = 'proposal/t_foot';
//      $data['content'] = 'proposal/new_proposal.php';
//      $this->load->view("template/main",$data);
//    }

//    public function hasil($id_projek){
//      $data = $this->data;
//        $length = strlen($id_projek);
//        $length = $length-15;
//        $id_prop = substr($id_projek,5,$length);
//      //$id_projek = $this->uri->segment(2);
//      $user_id = $this->session->userdata('user_id');
//      $team_id = $this->session->userdata('team_id');
//      $this->proposal_model->updateCounterPreview($id_prop);
//      $random = md5(mt_rand(1,10000));
//      $first = substr($random,0,5);
//      $last = substr($random,5,10);
//      $urlrand = $first.$id_prop.$last;
//      $link = base_url()."share/".$urlrand;
//
//      $detailproposal = $this->proposal_model->lihat_prop_by_team($id_prop,$team_id);
////        die(var_dump($detailproposal));
//      $detailkeuntungan = $this->proposal_model->keuntungan($id_prop);
//      $detailsyarat = $this->proposal_model->syarat($id_prop);
//      $detailtahapan = $this->proposal_model->tahapan($id_prop);
//      $detailkemampuan = $this->proposal_model->kemampuan($id_prop);
//      $detailpengalaman = $this->proposal_model->pengalaman($id_prop);
//      $detailpemberian = $this->proposal_model->pemberian($id_prop);
//      $sign = $this->proposal_model->ceksign($id_prop);
//      //die(print_r($detailproposal));
//
//      $additional_section = $this->proposal_model->additional_section($id_prop);
//      $where = array('team_id' => $team_id);
//
//      $data['team_logo'] = $this->user_model->get_team_logo($team_id);
//
//      $data['user_client'] = $this->client_model->tampil_where('client', $where)->result();
//
//      //$data['detail_proposal'] = $detailproposal;
//
//      $data['detail_proposal']['namaprojek'] = stripslashes($detailproposal->info_projek);
//      $data['detail_proposal']['id_klien'] = $detailproposal->id_client;
//      $data['detail_proposal']['klien'] = $detailproposal->nama_pic;
//      $data['detail_proposal']['perusahaan'] = $detailproposal->perusahaan;
//      if($detailproposal && ($detailproposal->aboutyou != NULL || $detailproposal->aboutyou != ''))
//      {
//        $data['detail_proposal']['aboutyou'] = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detailproposal->aboutyou));
//      }
//      else
//      {
//        $data['detail_proposal']['aboutyou'] = '';
//      }
//      $data['detail_proposal']['email_klien'] = $detailproposal->email;
//      $data['detail_proposal']['oleh'] = $detailproposal->user_name;
//      $data['detail_proposal']['team'] = $detailproposal->team_name;
//      $data['detail_proposal']['kontak'] = $detailproposal->user_email;
//      if($detailproposal && ($detailproposal->project_desc != NULL || $detailproposal->project_desc != ''))
//      {
//        $data['detail_proposal']['overview'] = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detailproposal->project_desc));
//      }
//      else
//      {
//        $data['detail_proposal']['overview'] = '';
//      }
//      $data['detail_proposal']['pengalaman'] = $detailproposal->pengalaman;
//      if($detailproposal && ($detailproposal->nextstep != NULL || $detailproposal->nextstep != ''))
//      {
//        $data['detail_proposal']['nextstep'] = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detailproposal->nextstep));
//      }
//      else
//      {
//        $data['detail_proposal']['nextstep'] = '';
//      }
//      $data['detail_proposal']['tgl'] = $detailproposal->tgl_buat;
//      if($detailkeuntungan && ($detailkeuntungan->keuntungan != NULL || $detailkeuntungan->keuntungan != ''))
//      {
//        $data['detail_proposal']['keuntungan'] = preg_replace("/'/", "\&#39;",preg_replace("/\r|\n/","<br>",$detailkeuntungan->keuntungan));
//      }
//      else
//      {
//        $data['detail_proposal']['keuntungan'] = '';
//      }
//      $data['detail_proposal']['duplicate'] = $detailproposal->duplicate_from;
//      $data['detail_proposal']['preview'] = $detailproposal->preview;
//      $data['detail_proposal']['style'] = $detailproposal->style;
//
//
//      $data['detailkeuntungan'] = $detailkeuntungan;
//      $data['detailsyarat'] = $detailsyarat;
//      $data['detailtahapan'] = $detailtahapan;
//      $data['detailkemampuan'] = $detailkemampuan;
//      $data['detailpemberian'] = $detailpemberian;
//      $data['detailtambahan'] = $additional_section;
//      $data['id_projek'] = $id_prop;
//      $data['link'] = $link;
//      $data['sign'] = $sign;
//        $data['page'] = 'Proposal';
//        $data['t_head'] = 'proposal/t_head.php';
//        $data['content'] = 'proposal/view_proposal.php';
//        $data['proposal_template'] = ''; // <--- disiapkan untuk dynamic template
//        $data['t_foot'] = 'proposal/t_foot.php';
//      $this->load->view("template/main.php",$data);
//
//    }

//    public function update(){
//      $data = $this->data;
//      $idproposal = $this->input->post('idproposal');
//      $info_projek = $this->input->post('info_projek');
//      $nextstep = $this->input->post('nextstep');
//      $this->projek_model->update_info($idproposal,$info_projek);
//      $this->projek_model->update_nextstep($idproposal,$nextstep);
//
//      $kemampuan = $this->input->post('kemampuan');
//      $idkemampuan = $this->input->post('idkemampuan');
//      for($i=0;$i<=count($kemampuan);$i++){
//        $this->kemampuan_model->update($idkemampuan[$i],$kemampuan[$i]);
//      }
//
//      $tahapan = $this->input->post('tahapan');
//      $tgl_tahapan = $this->input->post('tgl_tahapan');
//      $idtahapan = $this->input->post('idtahapan');
//      for($i=0;$i<=count($tahapan);$i++){
//        $this->tahapan_model->update($idtahapan[$i],$tahapan[$i],$tgl_tahapan[$i]);
//      }
//
//      $keuntungan = $this->input->post('keuntungan');
//      $this->keuntungan_model->update($idproposal,$keuntungan);
//
//      $pengalaman = $this->input->post('pengalaman');
//      $this->pengalaman_model->update_pengalaman($idproposal,$pengalaman);
//
//      $syarat = $this->input->post('syarat');
//      $idsyarat = $this->input->post('idsyarat');
//      for($i=0;$i<=count($syarat);$i++){
//        $this->syarat_model->update($idsyarat[$i],$syarat[$i]);
//      }
//
//      $pemberian = $this->input->post('pemberian');
//      $idpemberian = $this->input->post('idpemberian');
//      $harga = $this->input->post('harga');
//      for($i=0;$i<=count($syarat);$i++){
//        $this->pemberian_model->update($idpemberian[$i],$pemberian[$i],$harga[$i]);
//      }
//
//    }

//    function create_proposal(){
//      $data = $this->data;
//          $user_id = $this->session->userdata('user_id');
//          $user_email = $this->session->userdata('user_email');
//          //$idproposal = $this->input->post('idproposal');
//          //$idpengalaman = $this->input->post('idpengalaman');
//          //$idkeuntungan = $this->input->post('idkeuntungan');
//
//          // $tglbuat = date('m/d/Y h:i:s a', time());
//          $tglbuat = date('Y-m-d H:i:s');
//          // $this->db->set('tglbuat', 'DATE(NOW())', FALSE);
//
//          $info_projek_raw = $this->input->post('projectname');
//          $info_projek = addslashes(htmlentities($info_projek_raw));
//          $project_desc = addslashes(htmlentities($this->input->post('projectdesc')));
//          $aboutyou_raw = $this->input->post('aboutyou');
//          $aboutyou = addslashes(htmlentities($aboutyou_raw));
//          $id_client = $this->input->post('idclient');
//          $nextstep = addslashes(htmlentities($this->input->post('nextstep')));
//          $idproposal = $this->proposal_model->insert_proposal($user_id,$user_email,$aboutyou,$id_client,0,$tglbuat);
//          if($idproposal)
//          {
//            $this->projek_model->insert_projek($info_projek,$project_desc,$user_id,$nextstep,$idproposal);
//
//            //Insert Pengalaman dan KEMAMPUAN
//           // $pengalamam_raw = $this->input->post('pengalaman');
//            //$pengalaman = htmlentities($pengalamam_raw);
//            //$this->pengalaman_model->insert_pengalaman($pengalaman,$idproposal);
//
//            $kemampuan = $this->input->post('kemampuan');
//            foreach($kemampuan as $value_raw){
//              $val= addslashes(htmlentities($value_raw));
//              $this->kemampuan_model->insert_kemampuan($val,$idproposal);
//            }
//
//            $pengalaman = $this->input->post('pengalaman');
//            foreach($pengalaman as $value_raw){
//              $val= addslashes(htmlentities($value_raw));
//              $this->pengalaman_model->insert_pengalaman($val,$idproposal);
//            }
//
//            //Insert Tahapan
//            $tahapan = $this->input->post('tahapan');
//            //$tanggal = $this->input->post('tanggaltahapan');
//            $tanggal = NULL;
//            //$combinetahapan=array_combine($tahapan,$tanggal);
//            foreach($tahapan as $value_raw){
//              $val= addslashes(htmlentities($value_raw));
//              $this->tahapan_model->insert_tahapan($val,$tanggal,$idproposal);
//            }
//
//            //Insert Keuntungan
//
//            $keuntungan_raw = $this->input->post('keuntungan');
//            $keuntungan = addslashes(htmlentities($keuntungan_raw));
//            $pemberian = $this->input->post('pemberian');
//            $harga = $this->input->post('biaya');
//            $combinepemberian = array_combine($pemberian,$harga);
//            $idkeuntungan = $this->keuntungan_model->insert_keuntungan($keuntungan,$idproposal);
//            foreach($pemberian as $value_raw){
//              $val= addslashes(htmlentities($value_raw));
//              $this->pemberian_model->insert_pemberian($val,$combinepemberian[$value_raw],$idproposal);
//            }
//
//            //Insert Syarat dan ketentuan
//            $syarat = $this->input->post('syarat');
//            // $syarat = array('TEst','Sett')
//            foreach($syarat as $value_raw){
//              $val= addslashes(htmlentities($value_raw));
//              $this->syarat_model->insert_syarat($val,$idproposal);
//            }
//            $random = md5(mt_rand(1,10000));
//            $first = substr($random,0,5);
//            $last = substr($random,5,10);
//            $urlrand = $first.$idproposal.$last;
//            echo $urlrand;
//          }
//          else
//          {
//            return false;
//          }
//
//        }

//        function duplicate(){
//          $data = $this->data;
//          // die();
//          $id_projek = $this->input->post('prop_id');
//          $user_id = $this->session->userdata('user_id');
//          $team_id = $this->session->userdata('team_id');
//          // echo $id_projek;
//          // die();
//          // Load detail projek
//          $detailproposal = $this->proposal_model->lihat_prop_by_team($id_projek,$team_id);
//          $detailkeuntungan = $this->proposal_model->keuntungan($id_projek);
//          $detailsyarat = $this->proposal_model->syarat($id_projek);
//          $detailtahapan = $this->proposal_model->tahapan($id_projek);
//          $detailkemampuan = $this->proposal_model->kemampuan($id_projek);
//          $detailpengalaman = $this->proposal_model->pengalaman($id_projek);
//          $detailpemberian = $this->proposal_model->pemberian($id_projek);
//          $additional_section = $this->proposal_model->additional_section($id_projek);
//
//          //$idproposal_new  = $this->proposal_model->getCurrentAI()['AUTO_INCREMENT'];
//          //$idpengalaman_new  = $this->pengalaman_model->getCurrentAI()['AUTO_INCREMENT'];
//          //$idkeuntungan_new  = $this->keuntungan_model->getCurrentAI()['AUTO_INCREMENT'];
//
//          // die(print_r($detailkemampuan));
//          // detail projek yang akan di insert-duplicate
//          //foreach ($detailproposal as $dtcopy) {
//            $user_id = $this->session->userdata('user_id');
//            $idproposal = $detailproposal->idproposal;
//            $nama_projek = $this->input->post('new_project_name');
//            $project_desc =  $this->input->post('project_desc');
//            $aboutyou =  $detailproposal->aboutyou;
//            $user_email =  $detailproposal->user_email;
//            $id_client= $this->input->post('client_id');
//            if($id_client == NULL || $id_client == '')
//            {
//              $client_name = $this->input->post('client_name');
//              $company_name = $this->input->post('company_name');
//              $client_email = $this->input->post('client_email');
//              $data_new_client = array(
//                  'nama_pic'=>$client_name,
//                  'perusahaan'=>$company_name,
//                  'email'=>$client_email,
//                  'user_id'=>$user_id,
//                  'team_id'=>$team_id
//              );
//              $id_client = $this->client_model->setClientDuplicateProposal($data_new_client);
//            }
//            $nextstep=  $detailproposal->nextstep;
//
//          // $tglbuat = date('m/d/Y h:i:s a', time());
//          $tglbuat = date('Y-m-d H:i:s');
//
//          $hasilcek = $this->proposal_model->cekDuplicate($nama_projek);
//          foreach ($hasilcek as $val) {
//            $ada = $val->nama_projek;
//          }
//
//          /*
//          if (strpos($ada,'copy',-6) == false) {
//              $nama_projekbaru = $nama_projek;
//          } else {
//            $urutan = substr($ada, -1);
//            $urutanbaru = $urutan + 1;
//            $nama_projekbaru = substr_replace($ada,$urutanbaru,-1);
//          }
//          */
//
//          $nama_projekbaru = $nama_projek;
//
//          // die($nama_projekbaru);
//
//          $idproposal_new = $this->proposal_model->insert_proposal($user_id,$user_email,$aboutyou,$id_client,0,$tglbuat);
//
//          $this->projek_model->insert_projek($nama_projekbaru,$project_desc,$user_id,$nextstep,$idproposal_new);
//
//          //Insert KEMAMPUAN
//          //$this->pengalaman_model->insert_pengalaman($pengalaman,$idproposal_new);
//          foreach($detailkemampuan as $val){
//            $this->kemampuan_model->insert_kemampuan($val->skill,$idproposal_new);
//          }
//
//          //Insert Pengalaman
//          foreach($detailpengalaman as $val){
//            $this->pengalaman_model->insert_pengalaman($val->pengalaman,$idproposal_new);
//          }
//
//          //Insert Tahapan
//          foreach($detailtahapan as $val){
//            $this->tahapan_model->insert_tahapan($val->tahapan,$val->tanggal,$idproposal_new);
//          }
//
//          if($detailkeuntungan != NULL && count($detailkeuntungan) > 0)
//          {
//            $keuntungan = $detailkeuntungan->keuntungan;
//            //Insert Keuntungan
//            $idkeuntungan_new = $this->keuntungan_model->insert_keuntungan($keuntungan,$idproposal_new);
//          }
//
//          //Insert Fee
//          foreach($detailpemberian as $val){
//            $this->pemberian_model->insert_pemberian($val->pemberian,$val->harga,$idproposal_new);
//          }
//
//          //Insert Syarat dan ketentuan
//          foreach($detailsyarat as $val){
//            $this->syarat_model->insert_syarat($val->syaratketentuan,$idproposal_new);
//          }
//
//          //Insert addtional section
//          foreach ($additional_section as $as) {
//            $data_section_new = array(
//                  'section_pos'=>$as->section_pos,
//                  'section_name'=>$as->section_name,
//                  'proposal_id'=>$idproposal_new,
//                  'user_id'=>$as->user_id,
//                  'section_type'=>$as->section_type
//                );
//
//            $section_id_new = $this->proposal_model->insert_new_section($data_section_new);
//
//            foreach ($as->detail as $dt) {
//              $data_section_detail_new = array(
//                                  'section_detail_content'=>$dt->section_detail_content,
//                                  'section_id'=>$section_id_new
//                                );
//
//              $this->proposal_model->insert_section_detail($data_section_detail_new);
//            }
//          }
//
//          $log = array(
//                  'proposal_id'=>$id_projek,
//                  'section'=>'proposal',
//                  'action'=>'duplicate',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//          $this->proposal_model->insert_proposal_log($log);
//
//          redirect('proposal');
//        }

        function cek_nama_projek()
        {
          $info = $this->input->post();
          echo $this->projek_model->cek_nama_projek($info);
          exit();
        }

//        function add_new_section()
//        {
//          $data = $this->input->post();
//          $data_section = array(
//                  'section_pos'=>$data['section_pos'],
//                  'section_name'=>htmlentities($data['section_name']),
//                  'proposal_id'=>$data['proposal_id'],
//                  'user_id'=>$data['user_id'],
//                  'section_type'=>'text'
//                    );
//          $section_id = $this->proposal_model->insert_new_section($data_section);
//
//          $data_section_detail = array(
//                                'section_detail_content'=>htmlentities($data['section_detail_content']),
//                                'section_id'=>$section_id
//                              );
//          $section_detail = $this->proposal_model->insert_section_detail($data_section_detail);
//
//          $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>htmlentities($data['section_name']),
//                  'action'=>'add',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//          $this->proposal_model->insert_proposal_log($log);
//
//          return $section_detail;
//        }

//        function add_feedback()
//        {
//          $data = $this->input->post();
//          $data_feedback = array(
//            'proposal_id'=>$data['proposal_id'],
//            'section_name'=>$data['section_name'],
//            'feedback'=>htmlentities($data['feedback']),
//            'user_ip'=>$_SERVER['REMOTE_ADDR'],
//            'feedback_time'=>date('Y-m-d H:i:s')
//          );
//
//          if($this->proposal_model->insert_proposal_feedback($data_feedback))
//          {
//            echo 1;
//          }
//          else
//          {
//            echo 0;
//          }
//        }

//        function update_section_projek()
//        {
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $data_projek = array(
//                        'project_desc'=>htmlentities($data['project_desc'])
//                      );
//          $status_update = $this->projek_model->update_section_projek($proposal_id,$data_projek);
//          if($status_update)
//          {
//            $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>'projek',
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//            echo 1;
//          }
//          else
//          {
//            echo 0;
//          }
//        }

//        function update_section_aboutme()
//        {
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $data_projek = array(
//                        'aboutyou'=>htmlentities($data['about'])
//                      );
//          $status_update = $this->proposal_model->update_section_aboutme($proposal_id,$data_projek);
//          if($status_update)
//          {
//            $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>'aboutme',
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//            echo 1;
//          }
//          else
//          {
//            echo 0;
//          }
//        }

//        function update_section_benefit()
//        {
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $data_projek = array(
//                        'keuntungan'=>htmlentities($data['keuntungan'])
//                      );
//          $status_update = $this->keuntungan_model->update_section_keuntungan($proposal_id,$data_projek);
//          if($status_update)
//          {
//            $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>'benefit',
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//            echo 1;
//          }
//          else
//          {
//            echo 0;
//          }
//        }

//        function update_section_nextstep()
//        {
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $data_projek = array(
//                        'nextstep'=>htmlentities($data['nextstep'])
//                      );
//          $status_update = $this->projek_model->update_section_projek($proposal_id,$data_projek);
//          if($status_update)
//          {
//            $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>'nextstep',
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//            echo 1;
//          }
//          else
//          {
//            echo 0;
//          }
//        }

//        function update_section_portfolio()
//        {
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $portfolio = $data['portfolio'];
//          $this->pengalaman_model->delete_portfolio($proposal_id);
//          foreach($portfolio as $pt){
//            if($pt != '')
//            {
//              $val= htmlentities($pt);
//              $this->pengalaman_model->insert_pengalaman($val,$proposal_id);
//            }
//          }
//          $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>'portfolio',
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//          echo 1;
//        }

//        function update_section_skills()
//        {
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $skills = $data['skills'];
//          $this->kemampuan_model->delete_kemampuan($proposal_id);
//          foreach($skills as $sk){
//            if($sk != '')
//            {
//              $val= htmlentities($sk);
//              $this->kemampuan_model->insert_kemampuan($val,$proposal_id);
//            }
//          }
//          $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>'skills',
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//          echo 1;
//        }

//        function update_section_milestone()
//        {
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $milestone = $data['milestone'];
//          $tanggal = NULL;
//          $this->tahapan_model->delete_tahapan($proposal_id);
//
//          foreach($milestone as $ms){
//            if($ms != '')
//            {
//              $val= htmlentities($ms);
//              $this->tahapan_model->insert_tahapan($val,$tanggal,$proposal_id);
//            }
//          }
//          $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>'milestone',
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//          echo 1;
//        }

//        function update_section_terms()
//        {
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $terms = $data['terms'];
//          $this->syarat_model->delete_syarat($proposal_id);
//
//          foreach($terms as $tm){
//            if($tm != '')
//            {
//              $val= htmlentities($tm);
//              $this->syarat_model->insert_syarat($val,$proposal_id);
//            }
//          }
//          $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>'terms',
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//          echo 1;
//        }

//        function update_section_fee()
//        {
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $fee = $data['fee'];
//          $job = $data['job'];
//
//          $this->pemberian_model->delete_pemberian($proposal_id);
//
//          foreach ($fee as $key => $value) {
//            $this->pemberian_model->insert_pemberian($job[$key],$value,$proposal_id);
//          }
//
//          $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>'fee',
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//          echo 1;
//        }

//        function update_additional_section_text()
//        {
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $section_id = $data['edited_sid'];
//          $title = htmlentities($data['title']);
//          $content = htmlentities($data['content']);
//          $content_update = $this->proposal_model->update_additional_section($proposal_id,$section_id,$title,$content);
//          if($content_update)
//          {
//            $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>$data['edited_sid'],
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//            echo 1;
//          }
//          else
//          {
//            echo 0;
//          }
//        }

//        function update_project_title()
//        {
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $project_title = htmlentities($data['project_title']);
//          $title_update = $this->proposal_model->update_project_title($proposal_id,$project_title);
//          if($title_update)
//          {
//            $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>'title',
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//            echo 1;
//          }
//          else
//          {
//            echo 0;
//          }
//        }

//        function update_contact_email()
//        {
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $contact_email = $data['contact_email'];
//          $contact_email = $this->proposal_model->update_contact_email($proposal_id,$contact_email);
//          if($contact_email)
//          {
//            $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>'contact_email',
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//            echo 1;
//          }
//          else
//          {
//            echo 0;
//          }
//        }

//        function update_project_client()
//        {
//          $user_id = $this->session->userdata('user_id');
//          $team_id = $this->session->userdata('team_id');
//          $data = $this->input->post();
//          $proposal_id = $data['proposal_id'];
//          $client_id = $data['client_id'];
//          if($client_id == '' || $client_id == NULL)
//          {
//            $data_new_client = array(
//              'nama_pic'=> $data['client_name'],
//              'perusahaan'=> $data['company_name'],
//              'email'=> $data['client_email'],
//              'user_id'=>$user_id,
//              'team_id'=>$team_id
//            );
//            $client_id = $this->client_model->setClientDuplicateProposal($data_new_client);
//          }
//
//          $title_update = $this->proposal_model->update_project_client($proposal_id,$client_id);
//          if($title_update)
//          {
//            $log = array(
//                  'proposal_id'=>$data['proposal_id'],
//                  'section'=>'client',
//                  'action'=>'edit',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//            echo 1;
//          }
//          else
//          {
//            echo 0;
//          }
//        }

      function new_download($idproposal){
          $length = strlen($idproposal);
          $length = $length-15;
          $id_prop = substr($idproposal,5,$length);
          $user_id = $this->session->userdata('user_id');
          $team_id = $this->session->userdata('team_id');

          $getProposal = $this->proposal_model->detailBaruProposal($id_prop);
          $getSection = $this->proposal_model->detailSection($id_prop);
          $getTeamName = $this->client_model->teamName($getProposal->team_id);
          $getStyle = $this->proposal_model->getProposalStyle($id_prop);
          $data['team'] = $getTeamName;
          // $data['idprop'] = $id;
          $data['detailProp'] = $getProposal;
          $data['detailSec'] = $getSection;
          $data['style'] = $getStyle;
          $this->load->view("proposal/pdf/".$getProposal->template,$data);

      //     $log = array(
      //       'proposal_id'=>$id_prop,
      //       'section'=>'proposal',
      //       'action'=>'download',
      //       'user_id'=> $this->session->userdata('user_id'),
      //       'logtime'=>date('Y-m-d H:i:s'),
      //       'user_ip'=>$_SERVER['REMOTE_ADDR']
      //       );
      // $this->proposal_model->insert_proposal_log($log);
          echo $id_prop;
      }

        function send_proposal()
        {
          $data = $this->input->post();
//          die(var_dump($data));
                  $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
                  $domain = "ml.monika.id";

                if($data['client_cc'] == '')
                {
                  $email_entity = array(
                    //'from'    => $this->session->userdata('user_name').' via Monika.id <info@monika.id>',
                    'from' => $data['sender_name'].' <info@monika.id>',
                    'to'      => $data['client_email'],
                    'h:Reply-To'=> $data['sender_email'],
                    'subject' => $data['email_subject'],
                    'html'    => '<div style="font-family:verdana;font-size:13px;">'.nl2br($data['email_message']).'<br><br>---<p style="font-size:11px;">This email and proposal was created using <a href="https://monika.id">Monika.id</a></p></div>',
                  );
                }
                else
                {
                  $email_entity = array(
                    //'from'    => $this->session->userdata('user_name').' via Monika.id <info@monika.id>',
                    'from' => $data['sender_name'].' <info@monika.id>',
                    'to'      => $data['client_email'],
                    'cc'      => $data['client_cc'],
                    'h:Reply-To'=> $data['sender_email'],
                    'subject' => $data['email_subject'],
                    'html'    => '<div style="font-family:verdana;font-size:13px;">'.nl2br($data['email_message']).'<br><br>---<p style="font-size:11px;">This email and proposal was created using <a href="https://monika.id">Monika.id</a></p></div>',
                  );
                }

                  # Make the call to the client.
                  $result = $mgClient->sendMessage($domain, $email_entity);

                  $result = $mgClient->sendMessage($domain, array(
                    'from'    => 'Monika.id <info@monika.id>',
                    'to'      => $this->session->userdata('user_email'),
                    'subject' => 'Proposal kepada '.$data['client_name'].' berhasil dikirim',
                    'html'    => '<div style="font-family: verdana;">
                                    <p>'.$this->session->userdata('user_name').',</p>
                                      <p>Proposal yang ditujukan kepada '.$data['client_name'].' berhasil dikirim melalui email.</p>
                                      <p>Best,<br><br>Monika.id</p></div>',
                  ));
                  $tgl = date('Y-m-d H:i:s');
                  $updateTglkirim = $this->proposal_model->updateTglKirim($tgl,$data['prop_id']);
                    $log = array(
                          'proposal_id'=>$data['prop_id'],
                          'section'=>'proposal',
                          'action'=>'download',
                          'user_id'=> $this->session->userdata('user_id'),
                          'logtime'=>date('Y-m-d H:i:s'),
                          'user_ip'=>$_SERVER['REMOTE_ADDR']
                          );
                    $this->proposal_model->insert_proposal_log($log);
                  if($updateTglkirim){
                    return 1;
                  }else{
                    return 0;
                  }
        }

        function update_header_proposal()
        {
          $data = $this->input->post();
          $result_update = $this->proposal_model->update_header_proposal($data);
          if($result_update)
          {
            $log = array(
                  'proposal_id'=>$data['idproposal'],
                  'section'=>'header_style',
                  'action'=>'edit',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
                  );
            $this->proposal_model->insert_proposal_log($log);
            echo 1;
          }
          else
          {
            echo 0;
          }
        }

        function update_proposal_layout()
        {
          $data = $this->input->post();
          $result_update = $this->proposal_model->update_proposal_layout($data);
          if($result_update)
          {
            $log = array(
                  'proposal_id'=>$data['idproposal'],
                  'section'=>'proposal_layout',
                  'action'=>'edit',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
                  );
            $this->proposal_model->insert_proposal_log($log);
            echo 1;
          }
          else
          {
            echo 0;
          }
        }

        function updateProposalFontstyle(){
          $data = $this->input->post();
          $length = strlen($data['idproposal']);
          $length = $length-15;
          $prop_id = substr($data['idproposal'],5,$length);

          $getStyle = $this->proposal_model->getProposalStyle($prop_id);
          if($getStyle == null){
            $dataStyle = array(
              'idproposal' => $prop_id,
              'fontstyle' => $data['fontstyle']
            );
            $result = $this->proposal_model->insertProposalStyle($dataStyle);
          }else{
            $dataUpdate = array(
              'fontstyle' => $data['fontstyle']
            );
            $result = $this->proposal_model->updateProposalStyle($dataUpdate, $prop_id);
          }
          if($result){
            // $log = array(
            //       'proposal_id'=>$data['idproposal'],
            //       'section'=>'proposal_font',
            //       'action'=>'edit',
            //       'user_id'=> $this->session->userdata('user_id'),
            //       'logtime'=>date('Y-m-d H:i:s'),
            //       'user_ip'=>$_SERVER['REMOTE_ADDR']
            //       );
            // $this->proposal_model->insert_proposal_log($log);
            echo 1;
          }
          else
          {
            echo 0;
          }
        }

        function insert_header_proposal()
        {
          $data = $this->input->post();
          $result_insert = $this->proposal_model->insert_header_proposal($data);
          if($result_insert)
          {
            $log = array(
                  'proposal_id'=>$data['idproposal'],
                  'section'=>'proposal_header',
                  'action'=>'add',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
                  );
            $this->proposal_model->insert_proposal_log($log);
            echo 1;
          }
          else
          {
            echo 0;
          }
        }

//        public function custom_view(){
//          $this->load->view('custom_view', array('error' => ' ' ));
//        }

        public function upload_image_additional()
        {
          $data_section = array(
                  'section_pos'=>$this->input->post('position_image_after'),
                  'section_name'=>htmlentities($this->input->post('content_image_title')),
                  'proposal_id'=>$this->input->post('prop_id'),
                  'user_id'=>$this->session->userdata('user_id'),
                  'section_type'=>'image'
                    );
          $section_id = $this->proposal_model->insert_new_section($data_section);

          $conten_image_title = $this->input->post('content_image_title');
          $images = $_FILES["file"];
          $arr_img = 0;
            $random = md5(mt_rand(1,10000));
                $first = substr($random,0,5);
                $last = substr($random,5,10);
                $urlrand = $first.$data_section['proposal_id'].$last;
          foreach ($images["tmp_name"] as $im) {
            if($im != '')
            {
              $arr_img++;
              $data_uploaded_image = \Cloudinary\Uploader::upload($im, array("use_filename" => TRUE));
              $data_section_detail = array(
                                'section_detail_content'=>$data_uploaded_image['secure_url'],
                                'section_id'=>$section_id
                              );
              $section_detail = $this->proposal_model->insert_section_detail($data_section_detail);
            }
          }

          $log = array(
                  'proposal_id'=>$data_section['proposal_id'],
                  'section'=>'image',
                  'action'=>'add',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
                  );
            $this->proposal_model->insert_proposal_log($log);
          if($arr_img > 0)
          {
            redirect('proposal/lihat/'.$urlrand.'#section_'.$data_section['section_name']);
          }
        }

        public function update_image_additional()
        {
          $section_id = $this->input->post('section_id');
          $proposal_id = $this->input->post('prop_id');
          $images = $_FILES["file"];
          $arr_img = 0;
            $random = md5(mt_rand(1,10000));
            $first = substr($random,0,5);
            $last = substr($random,5,10);
            $urlrand = $first.$proposal_id.$last;
          foreach ($images["tmp_name"] as $im) {
            if($im != '')
            {
              $arr_img++;
              $data_uploaded_image = \Cloudinary\Uploader::upload($im, array("use_filename" => TRUE));
              $data_section_detail = array(
                                'section_detail_content'=>$data_uploaded_image['secure_url'],
                                'section_id'=>$section_id
                              );
              $section_detail = $this->proposal_model->insert_section_detail($data_section_detail);
            }
          }
          $log = array(
                  'proposal_id'=>$proposal_id,
                  'section'=>'image',
                  'action'=>'edit',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
                  );
           $this->proposal_model->insert_proposal_log($log);
          if($arr_img > 0)
          {
            redirect('proposal/lihat/'.$urlrand);
          }
        }

        public function upload_image_header()
        {
          $proposal_id = $this->input->post('prpid');
            $random = md5(mt_rand(1,10000));
            $first = substr($random,0,5);
            $last = substr($random,5,10);
            $urlrand = $first.$proposal_id.$last;
          $images = $_FILES["file"];
          $data_upload_header_image = \Cloudinary\Uploader::upload($images['tmp_name'], array("use_filename" => TRUE));
          $save_header_image = $this->proposal_model->update_header_image_proposal($proposal_id,$data_upload_header_image['secure_url']);
          $log = array(
                  'proposal_id'=>$proposal_id,
                  'section'=>'image_header',
                  'action'=>'upload',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
                  );
            $this->proposal_model->insert_proposal_log($log);

            redirect('proposal/lihat/'.$urlrand);
        }

        public function upload_image_logo()
        {
          $proposal_id = $this->input->post('proposid');
             $random = md5(mt_rand(1,10000));
                $first = substr($random,0,5);
                $last = substr($random,5,10);
                $urlrand = $first.$proposal_id.$last;
//            die($id_prop);
          $images = $_FILES["file"];
          $data_upload_logo_image = \Cloudinary\Uploader::upload($images['tmp_name'], array("use_filename" => TRUE));
          $save_logo_image = $this->proposal_model->update_logo_image_proposal($proposal_id,$data_upload_logo_image['secure_url']);
          $log = array(
                  'proposal_id'=>$proposal_id,
                  'section'=>'logo',
                  'action'=>'upload',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
                  );
            $this->proposal_model->insert_proposal_log($log);

            redirect('proposal/lihat/'.$urlrand);
        }

        public function get_team_logo()
        {
          $data = $this->input->post();
          $save_logo_image = $this->proposal_model->update_logo_image_proposal($data['proposal_id'],$data['team_logo']);
          $log = array(
                  'proposal_id'=>$data['proposal_id'],
                  'section'=>'logo',
                  'action'=>'get_team_logo',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
                  );
            $this->proposal_model->insert_proposal_log($log);
          if($save_logo_image)
          {
            echo 1;
          }
          else
          {
            echo 0;
          }
        }

        function remove_image()
        {
          $data = $this->input->post();
          $delete = $this->proposal_model->delete_image($data);
          if($delete)
          {
            echo 1;
          }
          else
          {
            echo 0;
          }
        }

//        public function remove_section()
//        {
//          $section = $this->input->post('section');
//          $proposal_id = $this->input->post('proposal_id');
//          if($section == 'overview')
//          {
//            $act_remove = $this->proposal_model->remove_overview($proposal_id);
//          }
//          else if($section == 'aboutme')
//          {
//            $act_remove = $this->proposal_model->remove_aboutme($proposal_id);
//          }
//          else if($section == 'team')
//          {
//            $act_remove = $this->proposal_model->remove_section('pengalaman',$proposal_id);
//          }
//          else if($section == 'team_value')
//          {
//            $act_remove = $this->proposal_model->remove_section('kemampuan',$proposal_id);
//          }
//          else if($section == 'milestone')
//          {
//            $act_remove = $this->proposal_model->remove_section('tahapan',$proposal_id);
//          }
//          else if($section == 'benefit')
//          {
//            $act_remove = $this->proposal_model->remove_section('keuntungan',$proposal_id);
//          }
//          else if($section == 'fee')
//          {
//            $act_remove = $this->proposal_model->remove_section('pemberian',$proposal_id);
//          }
//          else if($section == 'nextstep')
//          {
//            $act_remove = $this->proposal_model->remove_nextstep($proposal_id);
//          }
//          else if($section == 'term')
//          {
//            $act_remove = $this->proposal_model->remove_section('syarat',$proposal_id);
//          }
//          else if($section == 'biaya')
//          {
//            $act_remove = $this->proposal_model->remove_section('pemberian',$proposal_id);
//          }
//          else
//          {
//            $act_remove = $this->proposal_model->remove_additional_section($section,$proposal_id);
//          }
//
//          if($act_remove)
//          {
//            $log = array(
//                  'proposal_id'=>$proposal_id,
//                  'section'=>$section,
//                  'action'=>'remove',
//                  'user_id'=> $this->session->userdata('user_id'),
//                  'logtime'=>date('Y-m-d H:i:s'),
//                  'user_ip'=>$_SERVER['REMOTE_ADDR']
//                  );
//            $this->proposal_model->insert_proposal_log($log);
//            echo 1;
//          }
//          else
//          {
//            echo 0;
//          }
//        }

        public function delete_background()
        {
          $proposal_id = $this->input->post('proposal_id');
          if($this->proposal_model->delete_background($proposal_id))
          {
            $log = array(
                  'proposal_id'=>$proposal_id,
                  'section'=>'image_header',
                  'action'=>'remove',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
                  );
            $this->proposal_model->insert_proposal_log($log);
            echo 1;
          }
          else
          {
            echo 0;
          }
        }

        public function delete_logo()
        {
          $id = $this->input->post('proposal_id');
            $length = strlen($id);
            $length = $length-15;
            $id_prop = substr($id,5,$length);

          if($this->proposal_model->delete_logo_proposal($id_prop))
          {
            $log = array(
                  'proposal_id'=>$id_prop,
                  'section'=>'logo',
                  'action'=>'remove',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
                  );
            $this->proposal_model->insert_proposal_log($log);
            echo 1;
          }
          else
          {
            echo 0;
          }
        }

        public function log_copy_url()
        {
            $proposal_id = $this->input->post('proposal');

            $length = strlen($proposal_id);
            $length = $length-15;
            $id_prop = substr($proposal_id,5,$length);

          if($id_prop != NULL)
          {
            $logdata = array(
                  'proposal_id'=>$id_prop,
                  'section'=>'proposal',
                  'action'=>'share link',
                  'user_id'=> $this->session->userdata('user_id'),
                  'logtime'=>date('Y-m-d H:i:s'),
                  'user_ip'=>$_SERVER['REMOTE_ADDR']
                  );
            $log = $this->proposal_model->insert_proposal_log($logdata);
            if($log)
            {
              echo 1;
            }
            else
            {
              echo 0;
            }
          }
          else
          {
            echo 0;
          }
        }

//        public function ajax_get_proposal_by_id()
//        {
//          $proposal_id = $this->input->post('proposal_id');
//          $proposal = $this->proposal_model->getProposalSummary($proposal_id);
//          $dt_proposal = array(
//                  'info_projek'=>$proposal->info_projek,
//                  'project_desc'=>$proposal->project_desc
//                  );
//          echo json_encode($dt_proposal);
//        }
//
//        function stat($proposal_id)
//        {
//          $data = $this->data;
//            $length = strlen($proposal_id);
//            $length = $length-15;
//            $id_prop = substr($proposal_id,5,$length);
//          $user_id = $this->session->userdata('user_id');
//          $team_id = $this->session->userdata('team_id');
//
//          $avg_readtime = $this->laporan_model->get_avg_readtime_by_proposal_id($id_prop);
//          $best_time_read = $this->laporan_model->get_best_time_read_by_proposal_id($id_prop);
//          $detail_stat = $this->laporan_model->get_detail_proposal_stat_by_id($id_prop);
//          $data['avg_readtime'] = $avg_readtime;
//          $data['best_time_read'] = $best_time_read;
//          $data['detail_stat'] = $detail_stat;
//            $data['page'] = 'Statistik Proposal';
//            $data['t_head'] = 'proposal/t_head.php';
//            $data['t_foot'] = 'proposal/t_foot.php';
//
//          $data['content'] = 'proposal/view_proposal_statistic.php';
//          $this->load->view("template/main",$data);
//        }

//        function viewbaru($id_projek){
//            $data = $this->data;
//            $length = strlen($id_projek);
//            $length = $length-15;
//            $id_prop = substr($id_projek,5,$length);
//              //$id_projek = $this->uri->segment(2);
//            $user_id = $this->session->userdata('user_id');
//              $team_id = $this->session->userdata('team_id');
//              $this->proposal_model->updateCounterPreview($id_prop);
//              $random = md5(mt_rand(1,10000));
//              $first = substr($random,0,5);
//              $last = substr($random,5,10);
//              $urlrand = $first.$id_prop.$last;
//              $link = base_url()."share/".$urlrand;
//
//              $detailproposal = $this->proposal_model->lihat_prop_by_team($id_prop,$team_id);
//        //        die(var_dump($detailproposal));
//              $detailkeuntungan = $this->proposal_model->keuntungan($id_prop);
//              $detailsyarat = $this->proposal_model->syarat($id_prop);
//              $detailtahapan = $this->proposal_model->tahapan($id_prop);
//              $detailkemampuan = $this->proposal_model->kemampuan($id_prop);
//              $detailpengalaman = $this->proposal_model->pengalaman($id_prop);
//              $detailpemberian = $this->proposal_model->pemberian($id_prop);
//              $sign = $this->proposal_model->ceksign($id_prop);
//              //die(print_r($detailproposal));
//              $additional_section = $this->proposal_model->additional_section($id_prop);
//              $where = array('team_id' => $team_id);
//
//              $data['team_logo'] = $this->user_model->get_team_logo($team_id);
//
//              $data['user_client'] = $this->client_model->tampil_where('client', $where)->result();
//
//              $data['detail_proposal'] = $detailproposal;
//              $data['detailkeuntungan'] = $detailkeuntungan;
//              $data['detailsyarat'] = $detailsyarat;
//              $data['detailtahapan'] = $detailtahapan;
//              $data['detailkemampuan'] = $detailkemampuan;
//              $data['detailpemberian'] = $detailpemberian;
//              $data['detailtambahan'] = $additional_section;
//              $data['id_projek'] = $id_prop;
//              $data['link'] = $link;
//              $data['sign'] = $sign;
//        //       $data['page'] = 'Proposal - '.$detailproposal['info_projek'];
//                $data['page'] = 'Proposal';
//                $data['t_head'] = 'proposal/t_head.php';
//                $data['t_foot'] = 'proposal/t_foot.php';
//                $data['content'] = 'proposal/new_view_proposal.php';
//
//            $this->load->view("template/main.php",$data);
//        }

      function proposal()
      {
          $data['page'] = 'Proposal';
          //search proposal
          $data['search'] = '<input id="cari-proposal" class="form-search" type="text" placeholder="Search proposal">';
          //end of search proposal
          $user_id = $this->session->userdata('user_id');
          $team_id = $this->session->userdata('team_id');
          $data['log_document'] = $this->laporan_model->getLog();
          $data['AI_client'] = $this->client_model->getCurrentAI()['AUTO_INCREMENT'];
          $where = array('team_id' => $team_id);
          $cari  = $this->input->post('query');

          //mobile view
          if($this->mobile_detect->isMobile()){
              if(!isset($cari))
              {
                  $data['user_client'] = $this->client_model->tampil_where('client', $where)->result();
                  $data['proposal'] = $this->proposal_model->proposalByTeam();
                  $data['jumlah'] = $this->proposal_model->getProposalByUser($this->session->userdata('user_id'));
                  $row = $this->user_model->getUserByEmail($this->session->userdata('user_email'));
                  $data['user_id'] = $row['user_id'];

                  $data['t_head'] = 'proposal/mobile/t_head';
                  $data['t_foot'] = 'proposal/mobile/t_foot';
                  $data['content'] = 'proposal/mobile/index';
                  $this->load->view("template/mobile/main",$data);
              }
              else
              {
                  $output = '';
                  $hasilCariProposal = $this->proposal_model->cariProposal($cari);
                  $hasilCari = $hasilCariProposal->result();
                  if ($hasilCariProposal->num_rows() > 0)
                  {
                      $data['result'] = $hasilCari;
                      $this->load->view('proposal/mobile/search',$data);
                  }
                  else{
                      $output .= '<p>Proposal yang anda cari tidak ada.</p>';
                  }
                  echo $output;
              }
          }
          //end of mobile view

          //desktop & tablet view
          else{
              if(!isset($cari))
              {
                  $data['user_client'] = $this->client_model->tampil_where('client', $where)->result();
                  $data['proposal'] = $this->proposal_model->proposalByTeam();
                  $data['jumlah'] = $this->proposal_model->getProposalByUser($this->session->userdata('user_id'));
                  $row = $this->user_model->getUserByEmail($this->session->userdata('user_email'));
                  $data['user_id'] = $row['user_id'];

                  $data['t_head'] = 'proposal/desktop/t_head';
                  $data['t_foot'] = 'proposal/desktop/t_foot';
                  $data['content'] = 'proposal/desktop/index';
                  $this->load->view("template/desktop/main",$data);
              }
              else
              {
                  $output = '';
                  $hasilCariProposal = $this->proposal_model->cariProposal($cari);
                  $hasilCari = $hasilCariProposal->result();
                  if ($hasilCariProposal->num_rows() > 0)
                  {
                      $data['result'] = $hasilCari;
                      $this->load->view('proposal/desktop/search',$data);
                  }
                  else{
                      $output .= '<p>Proposal yang anda cari tidak ada.</p>';
                  }
                  echo $output;
              }
          }
          //end of desktop & tablet view
      }

      function viewproposal($id)
      {
          $length = strlen($id);
          $length = $length-15;
          $id_prop = substr($id,5,$length);

          $random = md5(mt_rand(1,10000));
          $first = substr($random,0,5);
          $last = substr($random,5,10);
          $urlrand = $first.$id_prop.$last;
          $link = base_url()."share/".$urlrand;
          //list link navigation
          $data['branch'] = '<a href="'.site_url('proposal').'">Proposal</a> / new proposal';
          //end of list link navigation
          $team_id = $this->session->userdata('team_id');
          $user_id = $this->session->userdata('user_id');
          $data['search'] = '';

          $data['log_document'] = $this->laporan_model->getLog();
          $getProposal = $this->proposal_model->detailBaruProposal($id_prop);
          $getSection = $this->proposal_model->detailSection($id_prop);
          $getTeamName = $this->client_model->teamName($getProposal->team_id);
          $getClient = $this->client_model->getAllClientTeam($team_id);
          $getProposalStyle = $this->proposal_model->getProposalStyle($id_prop);
          $getProposalTemplate = $this->proposal_model->getTemplate();

          $data['style']= $getProposalStyle;
          $data['team'] = $getTeamName;
          $data['idprop'] = $id;
          $data['detailProp'] = $getProposal;
          $data['detailSec'] = $getSection;
          $data['client'] = $getClient;
          $data['template'] = $getProposalTemplate;

          $data['page'] = 'Proposal - '.$getProposal->judul;
          $data['link'] = $link;

          if(($getProposal->user_id == $user_id) || (($getProposal->team_id == $team_id) && ($getProposal->share_team == 1))){
              //mobile view
              if($this->mobile_detect->isMobile()){
                  $data['back'] = site_url('proposal');
                  $data['t_head'] = 'proposal/mobile/t_head';
                  $data['t_foot'] = 'proposal/mobile/t_foot';
                  $data['content'] = 'proposal/mobile/detail';
                  $this->load->view("template/mobile/main",$data);
              }
              //end of mobile view
              //desktop view
              else{
                  $data['t_head'] = 'proposal/desktop/t_head';
                  $data['t_foot'] = 'proposal/desktop/t_foot';
                  $data['content'] = 'proposal/desktop/create';
                  $this->load->view("template/desktop/main",$data);
              }
              //end of desktop view
          }
          else{
              redirect("404_override");
          }
      }

      function newproposaltoDB(){
        $user_id = $this->input->post('user');
        $overviewbaru = $this->input->post('overviewbaru');;
        $projek = $this->input->post('projek');
        $client_id = $this->input->post('klien');
        $id = $this->proposal_model->getCurrentAI()['AUTO_INCREMENT'];
        $tglbuat = date('Y-m-d H:i:s');
        $data = array(
          'user_id' => $user_id,
          'overview' => $overviewbaru,
          'projek' => $projek,
          'judul_sec' => 'Overview',
          'urutan' => 1,
          'client_id' => $client_id,
          'tgl_buat' => $tglbuat
        );
        $insert_prop_DB = $this->proposal_model->newproposaltoDB($data);
        $random = md5(mt_rand(1,10000));
        $first = substr($random,0,5);
        $last = substr($random,5,10);
        $prop_id = $first.$insert_prop_DB.$last;
        echo json_encode($prop_id);
      }

      function deleteSectionProposal(){
        $id = $this->input->post('prop_id');
        $length = strlen($id);
        $length = $length-15;
        $id_prop = substr($id,5,$length);

        $sect_id = $this->input->post('section_id');

        $deleteSection = $this->proposal_model->deleteSection($id_prop,$sect_id);
        if($deleteSection == 1){
          return 'Section berhasil dihapus';
        }else{
          return 'Section Gagal dihapus';
        }
      }

      function addSectionProposal(){
        $id = $this->input->post('prop_id');
        $length = strlen($id);
        $length = $length-15;
        $prop_id = substr($id,5,$length);
        $urutan = $this->input->post('section_urutan');
        $judul = $this->input->post('judul');
        $tipe = $this->input->post('tipe');
        $isi = $this->input->post('isi');

        if($tipe == 0){
          $addSection = $this->proposal_model->addSectionText($prop_id,$urutan,$judul,$isi);
        }elseif($tipe == 1){
          $item = $this->input->post('item');
          $biaya = $this->input->post('biaya');
          $addSection = $this->proposal_model->addSectionTable($prop_id,$urutan,$judul,$isi,$tipe,$item,$biaya);
        }elseif ($tipe == 2) {
          $addSection = $this->proposal_model->addSectionImage($prop_id,$urutan,$isi,$judul,$tipe);
        }

        if($addSection == 1){
          return 'Section berhasil ditambah';
        }else{
          return 'Section Gagal ditambah';
        }
      }
      
      function updateJudulSection(){
        $secid = $this->input->post('secid');
        $judul = addslashes(htmlentities($this->input->post('judul')));

        $data = array(
          'sec_judul' => $judul
        );

        $updatejudul =  $this->proposal_model->updateDetailSection($data,$secid);
        if($updatejudul == 1){
          echo 'Berhasil Update Judul';
        }else{
          echo 'Gagal Update Judul';
        }
      }

      function updateIsiSection(){
        $secid = $this->input->post('secid');
        $judul = addslashes(htmlentities($this->input->post('isi')));

        $data = array(
          'sec_isi' => $judul
        );

        $updatejudul =  $this->proposal_model->updateDetailSection($data,$secid);
        if($updatejudul == 1){
          echo 'Berhasil Update Isi';
        }else{
          echo 'Gagal Update Isi';
        }
      }

      function updateSignature()
      {
          $id_prop = $this->input->post('prop_id');
          $length = strlen($id_prop);
          $length = $length-15;
          $id = substr($id_prop,5,$length);
          $data = $this->input->post('signature');

          $update = $this->proposal_model->updateSignature($data,$id);
          if($update == 1){
              echo 'Berhasil update signature';
          }
          else{
              echo 'Gagal update signature';
          }
      }

      function updateJudul()
      {
          $id_prop = $this->input->post('prop_id');
          $length = strlen($id_prop);
          $length = $length-15;
          $id = substr($id_prop,5,$length);
          $data = $this->input->post('title');

          $update = $this->proposal_model->updateJudul($data,$id);
          if($update == 1){
              echo 'Berhasil update judul';
          }
          else{
              echo 'Gagal update judul';
          }
      }

      function addItemSection(){
        $id = $this->input->post('prop_id');
        $length = strlen($id);
        $length = $length-15;
        $id_prop = substr($id,5,$length);
        $data = array(
          'item' => addslashes(htmlentities($this->input->post('item'))),
          'biaya' => $this->input->post('biaya'),
          'proposal_id' => $id_prop,
          'sec_id' => $this->input->post('sec_id')
        );

        $addItem = $this->proposal_model->addItemTable($data);
        if($addItem == 1){
          echo 'Berhasil tambah Item';
        }else{
          echo 'Gagal tambah Item';
        }
      }

      function addImageSection(){

        $prop_id = $this->input->post('prop_id');
        $sec = $this->input->post('sec_id');
        $url = base_url();        
        $random = md5(mt_rand(1,10000));
        $first = substr($random,0,5);
        $last = substr($random,5,10);
        $prop_id_url = $first.$prop_id.$last; 
        $images = $_FILES["userfile"];
        $data_image = \Cloudinary\Uploader::upload($images['tmp_name'], array("use_filename" => TRUE));
        $detailImage = array(
          'prop_id' => $prop_id,
          'sec_id' => $sec,
          'image_dir'=> $data_image['secure_url']
        );
        $addImageToDB = $this->proposal_model->uploadImageSection($detailImage);
        if($addImageToDB == 1){
          redirect("proposal/viewproposal/".$prop_id_url);
          }else{
            echo 'gagal upload logo baru';
          }
      }

      function uploadLogoHeader(){
        $prop_id = $this->input->post('prop_id');
        $url = base_url();    
        $random = md5(mt_rand(1,10000));
        $first = substr($random,0,5);
        $last = substr($random,5,10);
        $prop_id_url = $first.$prop_id.$last;    
        $images = $_FILES["userfile"];
        $data_image = \Cloudinary\Uploader::upload($images['tmp_name'], array("use_filename" => TRUE));
        $data = array(
                      'image_dir'=>$data_image['secure_url'],
                      'prop_id'=>$prop_id
                    );
        $updateLogoToDB = $this->proposal_model->uploadLogoHeader($data);
        if($updateLogoToDB == 1){
            redirect("proposal/viewproposal/".$prop_id_url);
          }else{
            echo 'gagal upload logo baru';
        }
      }

      function useOwnLogo(){
        $team_id = $this->input->post('team_id');
        $id = $this->input->post('prop_id');
          $length = strlen($id);
          $length = $length-15;
          $id_prop = substr($id,5,$length);
        
        $cek_team_logo = $this->user_model->get_team_logo($team_id);
        if($cek_team_logo !==  false){
          $detailImage = array(
            'prop_id' => $id_prop,
            'image_dir'=> $cek_team_logo->team_logo
          );
          $updateLogoProposal = $this->proposal_model->uploadLogoHeader($detailImage);
          if($updateLogoProposal){
            echo 11;
          }else{
            echo 10;
          }
        }else{
          echo 0;
        }
      }

      function deleteItemSection(){
       $item_id = $this->input->post('item_id');

        $deleteSection = $this->proposal_model->deleteItem($item_id);
        if($deleteSection == 1){
          return 'Item berhasil dihapus';
        }else{
          return 'Item Gagal dihapus';
        }
      }

      function deleteImageSecProposal(){
       $img_id = $this->input->post('img_id');

        $deleteSection = $this->proposal_model->deleteImage($img_id);
        if($deleteSection == 1){
          return 'Gambar berhasil dihapus';
        }else{
          return 'Gambar Gagal dihapus';
        }
      }

      function updateItemSection(){
        $itemid = $this->input->post('item_id');
        $item = addslashes(htmlentities($this->input->post('isi')));

        $data = array(
          'item' => $item
        );

        $updateitem = $this->proposal_model->updateDetailItem($data,$itemid);
        if($updateitem == 1){
          echo 'Berhasil update item';
        }else{
          echo 'Gagal update item';
        }
      }

      function updateBiayaSection(){
        $itemid = $this->input->post('item_id');
        $biaya = $this->input->post('isi');

        $data = array(
          'biaya' => $biaya
        );

        $updateitem = $this->proposal_model->updateDetailItem($data,$itemid);
        if($updateitem == 1){
          echo 'Berhasil update biaya';
        }else{
          echo 'Gagal update biaya';
        }
      }

      function statistik($id){
          $data = $this->data;
          $length = strlen($id);
          $length = $length-15;
          $id_prop = substr($id,5,$length);
          $user_id = $this->session->userdata('user_id');
          $team_id = $this->session->userdata('team_id');
          $data['branch'] = '<a href="'.site_url('proposal').'">Proposal</a> / Statistik Proposal';

          $getKontenBaca = $this->proposal_model->getKontenBaca($id_prop);
          $getDetailProposal = $this->proposal_model->getKontenProposal($id_prop);
          $getTotalBaca = $this->proposal_model->getTotalBaca($id_prop);
          $getBestHour = $this->proposal_model->getBestHourRead($id_prop);
          $getBestMinute = $this->proposal_model->getBestMinuteRead($id_prop);
          $data['page'] = 'Statistik Proposal';

          $data['konten_baca'] = $getKontenBaca;
          $data['detailProp'] = $getDetailProposal;
          $data['total_baca'] = $getTotalBaca;
          $data['best_hour'] = $getBestHour;
          $data['best_minute'] = $getBestMinute;

          if(($getDetailProposal->user_id == $user_id) || (($getDetailProposal->team_id == $team_id) && ($getDetailProposal->share_team == 1))){
              if($this->mobile_detect->isMobile()){
                  $data['back'] = site_url('proposal');

                  $data['t_head'] = 'proposal/mobile/t_head';
                  $data['t_foot'] = 'proposal/mobile/t_foot';
                  $data['content'] = 'proposal/mobile/statistik';
                  $this->load->view("template/mobile/main",$data);
              }
              else {
                  $data['t_head'] = 'proposal/desktop/t_head';
                  $data['t_foot'] = 'proposal/desktop/t_foot';
                  $data['content'] = 'proposal/desktop/statistik';
                  $this->load->view("template/desktop/main",$data);
              }
          }
          else{
              redirect("404_override");
          }
      }

      function updateHeaderFontColor(){
          $data = $this->input->post();
          $length = strlen($data['idproposal']);
          $length = $length-15;
          $prop_id = substr($data['idproposal'],5,$length);

        $getStyle = $this->proposal_model->getProposalStyle($prop_id);
        if($getStyle == null){
          $dataStyle = array(
            'idproposal' => $prop_id,
            'headertextcolor' => $data['headfontcolor']
          );
          $result = $this->proposal_model->insertProposalStyle($dataStyle);
        }else{
          $dataUpdate = array(
            'headertextcolor' => $data['headfontcolor']
          );
          $result = $this->proposal_model->updateProposalStyle($dataUpdate, $prop_id);
        }
        if($result){
          // $log = array(
          //       'proposal_id'=>$data['idproposal'],
          //       'section'=>'proposal_font',
          //       'action'=>'edit',
          //       'user_id'=> $this->session->userdata('user_id'),
          //       'logtime'=>date('Y-m-d H:i:s'),
          //       'user_ip'=>$_SERVER['REMOTE_ADDR']
          //       );
          // $this->proposal_model->insert_proposal_log($log);
          echo 1;
        }
        else
        {
          echo 0;
        }
      }

      function updateHeaderBgColor(){
          $data = $this->input->post();

          $length = strlen($data['idproposal']);
          $length = $length-15;
          $prop_id = substr($data['idproposal'],5,$length);

          $getStyle = $this->proposal_model->getProposalStyle($prop_id);

        if($getStyle == null){
          $dataStyle = array(
            'idproposal' => $prop_id,
            'headerbgcolor' => $data['headerbgcolor']
          );
          $result = $this->proposal_model->insertProposalStyle($dataStyle);
        }else{
          $dataUpdate = array(
            'headerbgcolor' => $data['headerbgcolor']
          );
          $result = $this->proposal_model->updateProposalStyle($dataUpdate, $prop_id);
        }
        if($result){
          // $log = array(
          //       'proposal_id'=>$data['idproposal'],
          //       'section'=>'proposal_font',
          //       'action'=>'edit',
          //       'user_id'=> $this->session->userdata('user_id'),
          //       'logtime'=>date('Y-m-d H:i:s'),
          //       'user_ip'=>$_SERVER['REMOTE_ADDR']
          //       );
          // $this->proposal_model->insert_proposal_log($log);
          echo 1;
        }
        else
        {
          echo 0;
        }
      }

      function newProposalNewClient()
      {
          $pic = $this->input->post('pic');
          $tim = $this->input->post('perusahaan');
          $email = $this->input->post('email');
          $phone = $this->input->post('telephone');
          $alamat = $this->input->post('alamat');
          $kota = $this->input->post('kota');
          $judul = $this->input->post('projek');
          $overview = $this->input->post('overviewbaru');
          $user_id = $this->input->post('user_id');
          $team_id = $this->session->userdata('team_id');

          $klien_id = $this->client_model->setClient($pic,$tim,$email,$phone,$alamat,$kota,$user_id,$team_id);

          $tglbuat = date('Y-m-d H:i:s');

          $data = array(
              'user_id' => $user_id,
              'overview' => $overview,
              'projek' => $judul,
              'judul_sec' => 'Overview',
              'urutan' => 1,
              'client_id' => $klien_id,
              'tgl_buat' => $tglbuat
          );

          $insert_prop_DB = $this->proposal_model->newproposaltoDB($data);

          $random = md5(mt_rand(1,10000));
          $first = substr($random,0,5);
          $last = substr($random,5,10);
          $prop_id = $first.$insert_prop_DB.$last;

          echo json_encode($prop_id);
      }

      function updateTemplate()
      {
          $template = $this->input->post('template');
          $id = $this->input->post('id');
          $length = strlen($id);
          $length = $length-15;
          $prop_id = substr($id,5,$length);
          $user_id = $this->session->userdata('user_id');

          $this->proposal_model->updateTemplate($template,$prop_id);

          echo json_encode($id);
      }

      function updatePermissionDocument($id)
      {
          $length = strlen($id);
          $length = $length-15;
          $prop_id = substr($id,5,$length);

          $share = $this->input->post('share');
          if($share == 'true'){
              $share = 1;
          }
          else{
              $share = 0;
          }

          $update = $this->proposal_model->updateShareDocument($share,$prop_id);

          $logdata = array(
              'proposal_id'=>$prop_id,
              'section'=>'proposal',
              'action'=>'permission',
              'user_id'=> $this->session->userdata('user_id'),
              'logtime'=>date('Y-m-d H:i:s'),
              'user_ip'=>$_SERVER['REMOTE_ADDR']
          );
          $log = $this->proposal_model->insert_proposal_log($logdata);

          if($log){
              echo 1;
          }
          else{
              echo 0;
          }
      }

      function updateClientProposal()
      {
          $id_prop = $this->input->post('id');
          $length = strlen($id_prop);
          $length = $length-15;
          $id = substr($id_prop,5,$length);

          $id_client = $this->input->post('client');
          $length = strlen($id_client);
          $length = $length-15;
          $idclient = substr($id_client,5,$length);

          $update = $this->proposal_model->updateClientProposal($idclient, $id);

          $logdata = array(
              'proposal_id'=>$id,
              'section'=>'proposal',
              'action'=>'update_client',
              'user_id'=> $this->session->userdata('user_id'),
              'logtime'=>date('Y-m-d H:i:s'),
              'user_ip'=>$_SERVER['REMOTE_ADDR']
          );
          $log = $this->proposal_model->insert_proposal_log($logdata);

          echo json_encode($id_prop);
      }
}
