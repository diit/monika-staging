<?php
  class Questionnaire extends CI_Controller
  {
    private $data;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('questionnaire_model');
        $this->load->model('');
        $this->load->model('');
        $this->load->library('session');
        $this->data['tutorial'] = $this->monikalib->getTutorialStatus();
        if($this->session->userdata('status') !== 'login' )
        {
            redirect('login');
        }
    }

    public function index(){
				$databaru = $this->questionnaire_model->getCurrentAI();
				$random = md5(mt_rand(1,10000));
				$first = substr($random,0,5);
				$last = substr($random,5,10);
				$urlrand = $first.$databaru['AUTO_INCREMENT'].$last;

        $getAllQuest = $this->questionnaire_model->getAllQuest($this->session->userdata('user_id'));
//        die(print_r($getAllQuest));
//			die($databaru['AUTO_INCREMENT']);
        $data = $this->data;
        $data['allQuest'] = $getAllQuest;
        $data['urlrand'] = $urlrand;
        $data['page'] = 'Questionnaire';
        $data['t_head'] = 'questionnaire/t_head.php';
        $data['t_foot'] = 'questionnaire/t_foot.php';
        $data['content'] = 'questionnaire/list_quest.php';
        $this->load->view("template/main.php",$data);
    }

		public function new_quest($quest_id){
				$questbaru_id = $this->questionnaire_model->getCurrentAI();
        $data = $this->data;
				$user_id = $this->session->userdata('user_id');
     		$team_id = $this->session->userdata('team_id');
				$tgl_buat = date('Y-m-d H:i:s');
				$status = 0;
				$counter_baca = 0;
				$judul_quest = 'Pertanyaan 1';
				$isi_quest = 'Isi pertanyaan 1';
				$data_id_quest = 1;
				$urutan = '1';

				$quest_baru = $this->questionnaire_model->insert_questionnaires_baru($user_id,$urutan,$status,$counter_baca,$tgl_buat);
				$detail_quest_baru = $this->questionnaire_model->insert_detail_questionnaires_baru($judul_quest,$isi_quest,$data_id_quest,$questbaru_id['AUTO_INCREMENT']);

				redirect('quest_detail/'.$quest_id);
    }

		public function get_quest($urlrand){
				$length = strlen($urlrand);
        $length = $length-15;
        $quest_id = substr($urlrand,5,$length);
				$get_detail_questionnaires = $this->questionnaire_model->get_detail_questionnaires($quest_id);
				$max_data_id = $this->questionnaire_model->get_max_data_id($quest_id);
				$data['quest_id'] = $quest_id;
				$data['tutorial'] = 2;
				$data['max_data_id'] = $max_data_id['0']->maxid;
				$data['detail_questionnaires'] = $get_detail_questionnaires;
				$data['page'] = 'Questionnaire Anda';
        $data['t_head'] = 'questionnaire/t_head.php';
        $data['t_foot'] = 'questionnaire/t_foot.php';
        $data['content'] = 'questionnaire/quest.php';
        $this->load->view("template/main.php",$data);
		}

		public function add_pertanyaan($quest_id){
				$user_id = $this->session->userdata('user_id');
				$judul_quest = $this->input->post('judulQuest');
				$isi_quest = $this->input->post('isiQuest');
				$data_id_quest = $this->input->post('dataIdQuest');
				$urutan_arr = $this->input->post('urutan');
				$urutan  = implode(",", $urutan_arr);
				$detail_quest_baru = $this->questionnaire_model->insert_detail_questionnaires_baru($judul_quest,$isi_quest,$data_id_quest,$quest_id);
				$update_urutan = $this->questionnaire_model->update_urutan_pertanyaan($urutan,$quest_id,$user_id);
		}

		public function delete_pertanyaan(){
				$user_id = $this->session->userdata('user_id');
				$quest_id = $this->input->post('questId');
				$data_id_quest = $this->input->post('dataIdQuest');
				$urutan_arr = $this->input->post('urutan');
				$urutan  = implode(",", $urutan_arr);
        $this->questionnaire_model->hapus_pertanyaan($data_id_quest,$quest_id);
        $this->questionnaire_model->update_urutan_pertanyaan($urutan,$quest_id,$user_id);
		}

    public function update_judul_pertanyaan(){
        $quest_id = $this->input->post('questId');
        $data_id_quest = $this->input->post('dataIdQuest');
        $updateJudul = $this->input->post('updateJudul');

        $this->questionnaire_model->update_judul_pertanyaan($quest_id,$data_id_quest,$updateJudul);
    }

    public function update_isi_pertanyaan(){
        $quest_id = $this->input->post('questId');
        $data_id_quest = $this->input->post('dataIdQuest');
        $updateIsi = $this->input->post('updateIsiPertanyaan');

        $this->questionnaire_model->update_isi_pertanyaan($quest_id,$data_id_quest,$updateIsi);
    }

    public function update_nama_projek(){
        $quest_id = $this->input->post('questId');
        $user_id = $this->session->userdata('user_id');
        $namaProjekUpdate = $this->input->post('updateNamaProjek');
        $this->questionnaire_model->update_nama_projek_quest($quest_id,$namaProjekUpdate,$user_id);

    }

  }
