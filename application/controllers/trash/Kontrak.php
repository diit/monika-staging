<?php
class Kontrak extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        // $this->load->model('kontrak_model');
        $this->load->library('session');
        $this->data['tutorial'] = $this->monikalib->getTutorialStatus();
        if($this->session->userdata('status') != "login"){
            redirect('login');
        }
    }
    public function index(){
        $data['page'] = 'Kontrak';
        $data['t_head'] = 'kontrak/t_head.php';
        $data['t_foot'] = 'kontrak/t_foot.php';
        $data['tutorial'] = 1;
        $data['content'] = 'kontrak/kontrak.php';
        $this->load->view('template/main.php',$data);
    }
}
?>
