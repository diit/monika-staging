<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'user/login';
$route['translate_uri_dashes'] = FALSE;

$route['404_override'] = 'user/notFound';

//access
$route['login'] = 'user/login';
$route['register'] = 'user/register';
$route['user/resetform/(:any)'] = "/user/resetForm/$1";
//end of access

//home
$route['home'] = 'home/dashboard';
//end of home

//proposal
$route['proposal']='proposal/proposal';
$route['share/(:any)']='userview/proposalshare/$1';
$route['proposal/download/(:any)']='/proposal/download/$1';
$route['proposal/statistik/(:any)'] = '/proposal/statistik/$1';
//end of proposal

//invoice
$route['invoice'] = 'invoice/invoice';
$route['invoice/lihat/(:any)'] = "/invoice/view/$1";
$route['shareinvoice/(:any)'] = '/userview/shareInvoice/$1';
$route['invoice/userdownload/(:any)'] = '/userview/downloadInvoice/$1';
//end of invoice

//klien
$route['klien']='/client/klien';
//end of klien

//arsip
$route['arsip']='/arsip/arsip';
//end of arsip

//policy
$route['syaratketentuan'] = '/userview/ketentuan';
$route['kebijakanprivasi'] = '/userview/kebijakan';
$route['info'] = '/home/info';
$route['faq'] = '/user/faq';
$route['policy'] = '/home/policy';
//end of policy

//profile
$route['user'] = '/user/user';
$route['profile'] = '/user/detailprofile';
$route['team-member'] = '/user/team';
//end of profile

//plan
$route['plan'] = 'plan/plan';
//end of plan

// free plan
$route['getfreeplan'] = 'plan/freeplan';
// end of free plan
