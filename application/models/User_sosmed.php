<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_sosmed extends CI_Model{
    function __construct() {
        $this->tableName = 'user';
        $this->primaryKey = 'user_id';
    }
    public function checkUser($data = array()){
        $this->db->select($this->primaryKey);
        $this->db->from($this->tableName);
        $this->db->where(array('oauth_provider'=>$data['oauth_provider'],'oauth_uid'=>$data['oauth_uid']));
        $query = $this->db->get();
        $check = $query->num_rows();
        if($check > 0){
            $result = $query->row_array();
            $data['updated_at'] = date("Y-m-d H:i:s");
            $update = $this->db->update($this->tableName,$data,array('user_id'=>$result['user_id']));
            $userID = $result['id'];
        }else{
            $data['create_date'] = date("Y-m-d H:i:s");
            $data['updated_at']= date("Y-m-d H:i:s");
            $insert = $this->db->insert($this->tableName,$data);
            $userID = $this->db->insert_id();
        }

        return $userID?$userID:false;
    }

    function storeTeam($data)
    {
        return $this->db->query("INSERT INTO user team_id VALUES '$data' WHERE user_id='$data'");
    }

    function lihatUser($data = array())
    {
        $this->db->select($this->primaryKey);
        $this->db->from($this->tableName);
        $this->db->where(array('oauth_provider'=>$data['oauth_provider'],'oauth_uid'=>$data['oauth_uid']));
        $query = $this->db->get();
        $check = $query->num_rows();
        if($check > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function updateUser($data = array())
    {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $update = $this->db->update($this->tableName,$data,array('user_id'=>$result['user_id']));
        $userID = $result['id'];
        return $userID;
    }

    function storeUser($data = array())
    {
        $data['create_date'] = date("Y-m-d H:i:s");
        $data['updated_at']= date("Y-m-d H:i:s");
        $this->db->insert($this->tableName,$data);
        $userID = $this->db->insert_id();
        return $userID;
    }

    function getUser($data)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id',$data);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $data_user = $query->row();
            return $data_user;
        }
        else
        {
            return false;
        }
    }
}
