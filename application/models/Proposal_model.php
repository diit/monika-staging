<?php
/**
 * Class proposal untuk mengatur semua kelengkapan proposal
 */
class Proposal_model extends CI_model
{
  public function proposal(){
    $this->db->select('client.nama_pic, client.email,projek.info_projek, proposal.id, proposal.tgl_buat, proposal.status, proposal.duplicate_from');
    $this->db->from('proposal');
    $this->db->join('projek' , 'projek.idproposal = proposal.id');
    $this->db->join('client' , 'client.id_client = proposal.id_client');
    $this->db->where('proposal.user_id', $this->session->userdata('user_id'));
    $this->db->where('proposal.arsip', 0);
    $this->db->order_by('proposal.id desc');
    if($hasil->num_rows()>0){
      return $hasil->result();
    }else{
      return false;
    }
  }

  public function proposal_from_all_member()
  {
    if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
    {
      if($this->monikalib->getUserPlan()->plan_id == 1 || $this->monikalib->getUserPlan()->plan_id == 2)
      {
        $hasil = $this->db->query('
                            SELECT * from
                            (
                              SELECT a.id_client, c.nama_pic, c.email, b.info_projek, a.id, a.tgl_buat, a.status,
                              a.duplicate_from, d.user_id, d.user_name, (select id from invoice where id_proposal = a.id order by created_at desc limit 1) as id_invoice, (select sum(harga) from pemberian where idproposal = a.id) as project_fee
                              FROM proposal a 
                              JOIN projek b ON b.idproposal = a.id 
                              JOIN client c ON c.id_client = a.id_client
                              JOIN user d on d.user_id = a.user_id
                              WHERE a.user_id = '.$this->session->userdata('user_id').' AND a.arsip = 0 
                            )x order by x.id_client desc, x.id desc
                            ');
      }
      else if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
      {
        $hasil = $this->db->query('
                            SELECT * from
                            (
                              SELECT a.id_client, c.nama_pic, c.email, b.info_projek, a.id, a.tgl_buat, a.status,
                              a.duplicate_from, d.user_id, d.user_name, (select id from invoice where id_proposal = a.id order by created_at desc limit 1) as id_invoice, (select sum(harga) from pemberian where idproposal = a.id) as project_fee
                              FROM proposal a 
                              JOIN projek b ON b.idproposal = a.id 
                              JOIN client c ON c.id_client = a.id_client
                              JOIN user d on d.user_id = a.user_id
                              WHERE a.user_id = '.$this->session->userdata('user_id').' AND a.arsip = 0 
                              UNION
                              SELECT a.id_client, c.nama_pic, c.email, b.info_projek, a.id, a.tgl_buat, a.status,
                              a.duplicate_from, d.user_id, d.user_name, (select id from invoice where id_proposal = a.id order by created_at desc limit 1) as id_invoice, (select sum(harga) from pemberian where idproposal = a.id) as project_fee
                              FROM proposal a 
                              JOIN projek b ON b.idproposal = a.id 
                              JOIN client c ON c.id_client = a.id_client
                              JOIN user d on d.user_id = a.user_id 
                              WHERE a.user_id in (select user_id from user where team_id = '.$this->session->userdata('team_id').') AND c.id_client > 1 AND a.arsip = 0
                            )x order by x.id_client desc, x.id desc
                            ');
      }
    }
    else
    {
      $hasil = $this->db->query('
                            SELECT * from
                            (
                              SELECT a.id_client, c.nama_pic, c.email, b.info_projek, a.id, a.tgl_buat, a.status,
                              a.duplicate_from, d.user_id, d.user_name, (select id from invoice where id_proposal = a.id order by created_at desc limit 1) as id_invoice, (select sum(harga) from pemberian where idproposal = a.id) as project_fee
                              FROM proposal a 
                              JOIN projek b ON b.idproposal = a.id 
                              JOIN client c ON c.id_client = a.id_client
                              JOIN user d on d.user_id = a.user_id
                              WHERE a.user_id = '.$this->session->userdata('user_id').' AND a.arsip = 0 
                            )x order by x.id_client desc, x.id desc
                            ');
    }
    
    if($hasil->num_rows()>0){
      return $hasil->result();
    }else{
      return false;
    }
  }

  public function latest_proposal_from_all_member()
  {
    if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
    {
      if($this->monikalib->getUserPlan()->plan_id == 1 || $this->monikalib->getUserPlan()->plan_id == 2)
      {
        $hasil = $this->db->query('
                            SELECT * from
                            (
                              SELECT a.id_client, c.nama_pic, c.email, b.info_projek, a.id, a.tgl_buat, a.status,
                              a.duplicate_from, d.user_id, d.user_name, (select id from invoice where id_proposal = a.id order by created_at desc limit 1) as id_invoice, (select sum(harga) from pemberian where idproposal = a.id) as project_fee
                              FROM proposal a
                              JOIN projek b ON b.idproposal = a.id
                              JOIN client c ON c.id_client = a.id_client
                              JOIN user d on d.user_id = a.user_id
                              WHERE a.user_id = '.$this->session->userdata('user_id').' AND a.arsip = 0
                            )x order by x.id_client desc, x.id desc limit 3
                            ');
      }
      else if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
      {
        $hasil = $this->db->query('
                            SELECT * from
                            (
                              SELECT a.id_client, c.nama_pic, c.email, b.info_projek, a.id, a.tgl_buat, a.status,
                              a.duplicate_from, d.user_id, d.user_name, (select id from invoice where id_proposal = a.id order by created_at desc limit 1) as id_invoice, (select sum(harga) from pemberian where idproposal = a.id) as project_fee
                              FROM proposal a
                              JOIN projek b ON b.idproposal = a.id
                              JOIN client c ON c.id_client = a.id_client
                              JOIN user d on d.user_id = a.user_id
                              WHERE a.user_id = '.$this->session->userdata('user_id').' AND a.arsip = 0
                              UNION
                              SELECT a.id_client, c.nama_pic, c.email, b.info_projek, a.id, a.tgl_buat, a.status,
                              a.duplicate_from, d.user_id, d.user_name, (select id from invoice where id_proposal = a.id order by created_at desc limit 1) as id_invoice, (select sum(harga) from pemberian where idproposal = a.id) as project_fee
                              FROM proposal a
                              JOIN projek b ON b.idproposal = a.id
                              JOIN client c ON c.id_client = a.id_client
                              JOIN user d on d.user_id = a.user_id
                              WHERE a.user_id in (select user_id from user where team_id = '.$this->session->userdata('team_id').') AND c.id_client > 1 AND a.arsip = 0
                            )x order by x.id_client desc, x.id desc limit 3
                            ');
      }
    }
    else
    {
      $hasil = $this->db->query('
                            SELECT * from
                            (
                              SELECT a.id_client, c.nama_pic, c.email, b.info_projek, a.id, a.tgl_buat, a.status,
                              a.duplicate_from, d.user_id, d.user_name, (select id from invoice where id_proposal = a.id order by created_at desc limit 1) as id_invoice, (select sum(harga) from pemberian where idproposal = a.id) as project_fee
                              FROM proposal a
                              JOIN projek b ON b.idproposal = a.id
                              JOIN client c ON c.id_client = a.id_client
                              JOIN user d on d.user_id = a.user_id
                              WHERE a.user_id = '.$this->session->userdata('user_id').' AND a.arsip = 0
                            )x order by x.id_client desc, x.id desc limit 3
                            ');
    }

    if($hasil->num_rows()>0){
      return $hasil->result();
    }else{
      return false;
    }
  }

  public function tampil_projek(){
    $this->db->select('*');
    $this->db->from('proposal');
    $this->db->join('projek' , 'projek.idproposal = proposal.id');
    $this->db->join('client' , 'client.id_client = proposal.id_client');
    $this->db->join('user' , 'user.user_id = proposal.user_id');
    $hasil = $this->db->get();
    return $hasil->result();
  }

  public function getCurrentAI(){
    $result = $this->db->query("SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'kurap-monika' AND TABLE_NAME = 'proposal'");
    return $result->row_array();
  }

  public function insert_proposal($user_id,$user_email,$aboutyou,$id_client,$statusbaca,$tglbuat){
    /*return $this->db->query("INSERT INTO proposal (user_email,aboutyou,id_client,status,tgl_buat) VALUES ('$useremail','$aboutyou','$id_client','$statusbaca','$tglbuat')");
    */
    $data_proposal = array(
                  'user_id'=>$user_id,
                  'user_email'=>$user_email,
                  'aboutyou'=>$aboutyou,
                  'id_client'=>$id_client,
                  'status'=>$statusbaca,
                  'tgl_buat'=>$tglbuat
                );
    $this->db->insert('proposal', $data_proposal);
      $id_proposal = $this->db->insert_id();
      return $id_proposal;
  }

  public function insert_tahapan($tahapan){
    return $this->db->insert('tahapan' ,$tahapan);
  }
  public function insert_diberiharga($diberiharga){
    return $this->db->insert('price' ,$diberiharga);
  }
  public function insert_skill($skill){
    return $this->db->insert('kemampuan' ,$skill);
  }
  public function insert_termandcond($termandcond){
    return $this->db->insert('termandcond' ,$termandcond);
  }
  public function insert_pengalaman($pengalaman){
    return $this->db->insert('pengalaman' ,$pengalaman);
  }
  public function insert_benefit($benefit){
    return $this->db->insert('benefit' ,$benefit);
  }
  public function insert_nextstep($nextstep){
    return $this->db->insert('nextstep' ,$nextstep);
  }

  public function insert_proposal_log($log)
  {
    return $this->db->insert('proposal_log' ,$log); 
  }

  public function insert_proposal_feedback($feedback)
  {
    return $this->db->insert('proposal_feedback' ,$feedback);
  }

  public function delete_image($del_image)
  {
    return $this->db->delete('section_detail', $del_image);
  }

  public function lihat_prop_by_team($id_projek, $team_id)
  {
    $this->db->select('proposal.*,user.user_name,team.*,projek.*,client.*');
    $this->db->from('proposal');
    $this->db->join('user' , 'proposal.user_id = user.user_id');
    $this->db->join('team' , 'user.team_id = team.team_id');
    $this->db->join('projek' , 'proposal.id = projek.idproposal');
    $this->db->join('client', 'proposal.id_client = client.id_client');
    $this->db->where('proposal.user_id in (select user_id from user where team_id = '.$team_id.')');
    $this->db->where('proposal.id', $id_projek);

//    Test baru

    if($query=$this->db->get())
    {
      if($query && $query -> num_rows() >= 1)
      {
        $prop_detail =  $query->row();
        $prop_detail->pengalaman = array();
        $prop_detail->style = array();
        $this->db->select('pengalaman,deleted');
        $this->db->from('pengalaman');
        $this->db->where('idproposal',$id_projek);
        $this->db->where('deleted',0);
        $query2=$this->db->get();
        if($query2 && $query2->num_rows() > 0)
        {
          $pengalaman = $query2->result_array();
          $prop_detail->pengalaman = $pengalaman;
        }

        $this->db->select('headerbgcolor,headertextcolor,headerimageurl,logoimageurl,section_layout,fontstyle');
        $this->db->from('proposal_style');
        $this->db->where('idproposal',$id_projek);
        $query3=$this->db->get();
        if($query3 && $query3->num_rows() > 0)
        {
          $style = $query3->row_array();
          $prop_detail->style = $style;
        }

        return $prop_detail;
      }
      else
      {
        return false;
      }
    }
    else{
      return false;
    }
  }

  public function lihat_prop($id_projek,$user_id){
    $this->db->select('*');
    $this->db->from('proposal');
    $this->db->join('user' , 'proposal.user_id = user.user_id');
    $this->db->join('projek' , 'proposal.id = projek.idproposal');
    //$this->db->join('pengalaman', 'proposal.id = pengalaman.idproposal');
    $this->db->join('client', 'proposal.id_client = client.id_client');
    //$this->db->join('keuntungan', 'keuntungan.idproposal = proposal.id');
    $this->db->where('proposal.user_id', $user_id);
    $this->db->where('proposal.id', $id_projek);
    //$hasildetail = $this->db->get();
    //return $hasildetail->result();

    if($query=$this->db->get())
    {
      if($query && $query -> num_rows() >= 1)
      {
        $prop_detail =  $query->row();
        $prop_detail->pengalaman = array();
        $prop_detail->style = array();
        $this->db->select('pengalaman,deleted');
        $this->db->from('pengalaman');
        $this->db->where('idproposal',$id_projek);
        $this->db->where('deleted',0);
        $query2=$this->db->get();
        if($query2 && $query2->num_rows() > 0)
        {
          $pengalaman = $query2->result_array();
          $prop_detail->pengalaman = $pengalaman;
        }

        $this->db->select('headerbgcolor,headertextcolor,headerimageurl,logoimageurl,section_layout,fontstyle');
        $this->db->from('proposal_style');
        $this->db->where('idproposal',$id_projek);
        $query3=$this->db->get();
        if($query3 && $query3->num_rows() > 0)
        {
          $style = $query3->row_array();
          $prop_detail->style = $style;
        }

        return $prop_detail;
      }
      else
      {
        return false;
      }
    }
    else{
      return false;
    }
  }

  public function lihat_prop_download($id_projek){
    $this->db->select('*');
    $this->db->from('proposal');
    $this->db->join('user' , 'proposal.user_id = user.user_id');
    $this->db->join('projek' , 'proposal.id = projek.idproposal');
    //$this->db->join('pengalaman', 'proposal.id = pengalaman.idproposal');
    $this->db->join('client', 'proposal.id_client = client.id_client');
    //$this->db->join('keuntungan', 'keuntungan.idproposal = proposal.id');
    $this->db->where('proposal.id', $id_projek);
    //$hasildetail = $this->db->get();
    //return $hasildetail->result();

    if($query=$this->db->get())
    {
      if($query && $query -> num_rows() >= 1)
      {
        $prop_detail =  $query->row();
        $prop_detail->pengalaman = array();
        $prop_detail->style = array();
        $this->db->select('pengalaman');
        $this->db->from('pengalaman');
        $this->db->where('idproposal',$id_projek);
        $this->db->where('deleted',0);
        $query2=$this->db->get();
        if($query2 && $query2->num_rows() > 0)
        {
          $pengalaman = $query2->result_array();
          $prop_detail->pengalaman = $pengalaman;
        }

        $this->db->select('headerbgcolor,headertextcolor,headerimageurl,logoimageurl,section_layout,fontstyle');
        $this->db->from('proposal_style');
        $this->db->where('idproposal',$id_projek);
        $query3=$this->db->get();
        if($query3 && $query3->num_rows() > 0)
        {
          $style = $query3->row_array();
          $prop_detail->style = $style;
        }

        return $prop_detail;
      }
      else
      {
        return false;
      }
    }
    else{
      return false;
    }
  }

//  public function shareview_prop($id_projek){
//    $this->db->select('proposal.id_client, nama_pic, perusahaan,user_name,project_desc, tgl_buat, proposal.status,counter_baca,proposal.preview, team.team_name, team.team_id');
//    $this->db->from('proposal');
//    $this->db->join('user' , 'proposal.user_id = user.user_id');
//    $this->db->join('team' , 'user.team_id = team.team_id');
//    $this->db->join('client', 'proposal.id_client = client.id_client');
//    $this->db->where('proposal.id', $id_projek);
//
//    if($query=$this->db->get())
//    {
//      if($query && $query -> num_rows() >= 1)
//      {
//        $prop_detail =  $query->row();
//        $prop_detail->pengalaman = array();
//        $prop_detail->style = array();
//
//        $this->db->select('pengalaman');
//        $this->db->from('pengalaman');
//        $this->db->where('idproposal',$id_projek);
//        $this->db->where('deleted',0);
//        $query2=$this->db->get();
//        if($query2 && $query2->num_rows() > 0)
//        {
//          $pengalaman = $query2->result_array();
//          //var_dump($pengalaman);
//          $prop_detail->pengalaman = $pengalaman;
//        }
//
//        $this->db->select('headerbgcolor,headertextcolor,headerimageurl,logoimageurl,section_layout,fontstyle');
//        $this->db->from('proposal_style');
//        $this->db->where('idproposal',$id_projek);
//        $query3=$this->db->get();
//        if($query3 && $query3->num_rows() > 0)
//        {
//          $style = $query3->row_array();
//          $prop_detail->style = $style;
//        }
//
//        return $prop_detail;
//      }
//      else
//      {
//        return false;
//      }
//    }
//    else{
//      return false;
//    }
//
//  }

//  public function keuntungan($id_projek){
//    $this->db->select('keuntungan.*');
//    $this->db->from('proposal');
//    $this->db->join('keuntungan', 'proposal.id = keuntungan.idproposal');
//    $this->db->where('proposal.id', $id_projek);
//    $this->db->where('deleted',0);
//    $hasildetail = $this->db->get();
//    return $hasildetail->row();
//  }
//
//  public function syarat($id_projek){
//    $this->db->select('*');
//    $this->db->from('proposal');
//    $this->db->join('syarat' , 'proposal.id = syarat.idproposal');
//    $this->db->where('proposal.id', $id_projek);
//    $this->db->where('deleted',0);
//    $hasildetail = $this->db->get();
//    return $hasildetail->result();
//  }
//
//  public function tahapan($id_projek){
//    $this->db->select('*');
//    $this->db->from('proposal');
//    $this->db->join('tahapan', 'proposal.id = tahapan.idproposal');
//    $this->db->where('proposal.id', $id_projek);
//    $this->db->where('deleted',0);
//    $hasildetail = $this->db->get();
//    return $hasildetail->result();
//  }
//  public function kemampuan($id_projek){
//    $this->db->select('*');
//    $this->db->from('kemampuan');
//    $this->db->where('idproposal', $id_projek);
//    $this->db->where('deleted',0);
//    $hasildetail = $this->db->get();
//    return $hasildetail->result();
//  }
//  public function pengalaman($id_projek){
//    $this->db->select('*');
//    $this->db->from('pengalaman');
//    $this->db->where('idproposal', $id_projek);
//    $this->db->where('deleted',0);
//    $hasildetail = $this->db->get();
//    return $hasildetail->result();
//  }
//  public function pemberian($id_projek){
//    //$idkeuntungan = $this->db->query("SELECT id FROM keuntungan WHERE idproposal=$id_projek");
//    //$idnya = $idkeuntungan->row_array();
//    //$id = $idnya['id'];
//    $this->db->select('*');
//    $this->db->from('pemberian');
//    //$this->db->join('pemberian', 'keuntungan.id= pemberian.idkeuntungan');
//    $this->db->where('idproposal', $id_projek);
//    $this->db->where('deleted',0);
//    $hasildetail = $this->db->get();
//    return $hasildetail->result();
//  }


//  public function cekSign($id_projek){
//    // $this->db->select('sign');
//    $this->db->from('proposal');
//    $this->db->where('id',$id_projek);
//    $query = $this->db->get();
//    // return $hasildetail->result();
//    // if($hasildetail == 1){
//    //   return "true";
//    // }else{
//    //   return "false";
//    // }
//
//    if($query->num_rows()>0) {
//
//       $data = $query->row_array();
//
//       $value = $data['status'];
//
//       return $value;
//
//       } else {
//
//       return false;
//
//    }
//
//  }


  function edit_save($id,$fields)
    {
          $this ->db
                ->where('id',$id)
                ->update('projek',$fields);
    }

  public function updateStatusBaca($key,$val){
    return $this->db->query("UPDATE proposal SET status ='$val' WHERE id='$key' and status < '$val' ");
   }

   public function updateLastBaca($key,$val){
    return $this->db->query("UPDATE proposal SET last_baca = '$val' WHERE id='$key'");
   }

   public function updateCounterBaca($key){
   return $this->db->query("UPDATE proposal SET counter_baca = counter_baca+1 WHERE id='$key'");
  }

  public function updateCounterPreview($key){
   return $this->db->query("UPDATE proposal SET preview = preview+1 WHERE id='$key'");
  }

  public function getProposalByUser($user_id){
    $result=$this->db->query("SELECT a.id, a.judul, b.nama_pic FROM proposal a join client b on a.id_client = b.id_client WHERE a.user_id='$user_id'");
    if($result->num_rows()>0){
      return $result->num_rows();
    }
    else
    {
      return 0;
    }
  }

  public function getProposalByTeam($team_id){
    $result=$this->db->query("SELECT a.* FROM proposal a join client b on a.id_client = b.id_client WHERE a.user_id in (select user_id from user where team_id = ".$team_id.") and a.id_client > 1");
    if($result->num_rows()>0){
      return $result->num_rows();
    }
    else
    {
      return 0;
    }
  }

  public function cekDuplicate($nama_projek){
    $this->db->select('MAX(info_projek) as nama_projek');
    $this->db->from('projek');
    $this->db->where('info_projek LIKE', '%'.$nama_projek.'%');
    $hasil = $this->db->get();
    return $hasil->result();
  }

  public function checkClient($user_id,$id_client){
    $query= "SELECT * FROM proposal WHERE user_id = '$user_id' AND id_client='$id_client'";
    $r = $this->db->query($query);
    if($r->num_rows()>0){
      return "true";
    }else{
      return "false";
    }
  }

  function getProposalByStatus($user_id, $status){
    $this->db->select('*');
    $this->db->from('proposal');
    $this->db->where('user_id',$user_id);
    $this->db->where('status',$status);
    $this->db->where('id_client > 1');
    $hasil = $this->db->get();
    return $hasil;
  }

  function getProposalTeamByStatus($status){
        $iduser = $this->session->userdata('user_id');
        $idteam = $this->session->userdata('team_id');

        $result = $this->db->query('select * from
        (
            (select p.id, p.status, p.arsip
            from proposal p
            where p.user_id='.$iduser.')
            union
            (select p.id, p.status, p.arsip
            from proposal p
            where p.user_id in (select user_id from user where team_id = '.$idteam.') and share_team=1)
        ) t_union
        where arsip=0 and status='.$status.'');
        return $result->result();
  }

  public function signing($id)
    {
      $data = array(
        'status' => 2
    );
      //$this->db->from('proposal');
      $this->db->where('id',$id);
      $this->db->update('proposal', $data);
    }

  public function insert_new_section($data_section){
      $this->db->insert('section_tambahan', $data_section);
      $section_id = $this->db->insert_id();
      return $section_id;
  }

  public function insert_section_detail($data_section_detail){
    return $this->db->insert('section_detail', $data_section_detail);
  }

  public function additional_section($proposal_id){
      $this->db->select('*');
      $this->db->from('section_tambahan');
      $this->db->where('proposal_id',$proposal_id);
      if($hasil = $this->db->get())
      {
        if($hasil && $hasil->num_rows() > 0)
        {
          $add_section = $hasil->result();
          $hasil_baru = array();
          foreach ($add_section as $as) 
          {
            $as->detail = array();
            $this->db->select('section_detail_id,section_detail_content,section_id');
            $this->db->from('section_detail');
            $this->db->where('section_id',$as->section_id);
            $hasil2 = $this->db->get();
            if($hasil2 && $hasil2->num_rows() > 0)
            {
              $detail = $hasil2->result();
              foreach ($detail as $dt) 
              {
                array_push($as->detail, $dt);
              }
            }
            array_push($hasil_baru, $as);
          }
          return $hasil_baru;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
  }

  public function update_section_aboutme($proposal_id,$data_projek){
      $this->db->where('id',$proposal_id);
      $this->db->where('user_id',$this->session->userdata('user_id'));
      return $this->db->update('proposal', $data_projek);
    }

  public function update_additional_section($proposal_id,$section_id,$title,$content)
  {
      $result_update = $this->db->query('UPDATE section_detail a
                      INNER JOIN section_tambahan b ON b.section_id = a.section_id
                      SET a.section_detail_content = "'.$content.'", b.section_name = "'.$title.'"
                      WHERE a.section_id = '.$section_id.' and b.section_type="text" and  b.proposal_id = '.$proposal_id
                    );
      return $result_update;
  }

  public function update_project_title($proposal_id,$project_title)
  {
    $result_update_title = $this->db->query('UPDATE projek set info_projek = "'.$project_title.'" where idproposal ='.$proposal_id);
    return $result_update_title;
  }

  public function update_contact_email($proposal_id,$contact_email)
  {
    $result_update_email = $this->db->query('UPDATE proposal set user_email = "'.$contact_email.'" where id ='.$proposal_id);
    return $result_update_email;
  }

  public function update_project_client($proposal_id,$client_id)
  {
    $result_update_title = $this->db->query('UPDATE proposal set id_client = '.$client_id.' where id ='.$proposal_id);
    return $result_update_title;
  }

  public function insert_new_tracking($data_tracking,$detail_tracking)
  {
    $this->db->insert('proposal_tracking', $data_tracking);
    $tracking_id = $this->db->insert_id();

    foreach ($detail_tracking as $key => $value)
    {
      foreach ($value as $ky => $vl) {
        $this->db->insert('proposal_tracking_detail', array(
          'section'=>$ky,
          'screentime'=>$vl,
          'tracking_id'=>$tracking_id,
          'proposal_id'=>$data_tracking['proposal_id']
        ));
      }
    }

    return true;
  }

  public function update_tracking($tracking_id,$section,$screentime,$proposal_id){
    $this->db->set('screentime', $screentime);
    $this->db->where('tracking_id', $tracking_id);
    $this->db->where('section', $section);
    $this->db->where('proposal_id', $proposal_id);
    $updatetime = $this->db->update('proposal_tracking_detail');
    if ($updatetime){
      return 1;
    }else{
      return 0;
    }
  }

  public function get_tracking_id($proposal_id)
  {
    $this->db->select('tracking_id');
    $this->db->from('proposal_tracking');
    $this->db->where('proposal_id', $proposal_id);
    $this->db->order_by('tracking_id desc');
    $hasil = $this->db->get();
    return $hasil->row_array();
  }

  public function get_all_detail_tracking($tracking_id,$proposal_id)
  {
    $this->db->select('section');
    $this->db->from('proposal_tracking_detail');
    $this->db->where('tracking_id', $tracking_id);
    $this->db->where('proposal_id', $proposal_id);
    $hasil = $this->db->get();
    return $hasil->result();
  }

  function prop_per_page($number,$offset,$query){
       $this->db->select('*');
       $this->db->from('proposal');
       $this->db->join('projek' , 'projek.idproposal = proposal.id');
       $this->db->join('client' , 'client.id_client = proposal.id_client');
       $this->db->join('user' , 'user.user_id = proposal.user_id');
       $this->db->where('(info_projek LIKE "%'.$query.'%")', NULL, FALSE);
       $this->db->limit($number, $offset);
       $isi = $this->db->get();
       return $isi;
       // $query = $this->db->get('client',$number,$offset)->result();
  }

  function getProposalUserAndClient($proposal_id)
  {
    $this->db->select('b.*,c.nama_pic');
    $this->db->from('proposal a');
    $this->db->join('user b','a.user_id = b.user_id');
    $this->db->join('client c','a.id_client = c.id_client');
    $this->db->where('a.id', $proposal_id);
    $hasil = $this->db->get();
    return $hasil->row_array(); 
  }

  function update_header_proposal($data)
  {
    return $this->db->query("UPDATE proposal_style SET headerbgcolor = '".$data['headerbgcolor']."',headertextcolor = '".$data['headertextcolor']."' WHERE idproposal = ".$data['idproposal']);
  }

  function update_proposal_layout($data)
  {
    $this->db->where('idproposal',$data['idproposal']);
    $qy = $this->db->get('proposal_style');
    if($qy && $qy -> num_rows() >= 1)
    {
      return $this->db->query("UPDATE proposal_style SET section_layout = ".$data['section_layout']." WHERE idproposal = ".$data['idproposal']);
    }
    else
    {
      return $this->db->insert('proposal_style', array('idproposal'=>$data['idproposal'],'headerbgcolor'=>'#656464','headertextcolor'=>'#ffffff','headerimageurl'=>NULL,'section_layout'=>$data['section_layout'],'fontstyle'=>'Avenir'));
    }
  }

  function update_proposal_fontstyle($data)
  {
    $this->db->where('idproposal',$data['idproposal']);
    $qy = $this->db->get('proposal_style');
    if($qy && $qy -> num_rows() >= 1)
    {
      return $this->db->query("UPDATE proposal_style SET fontstyle = '".$data['fontstyle']."' WHERE idproposal = ".$data['idproposal']);
    }
    else
    {
      return $this->db->insert('proposal_style', array('idproposal'=>$data['idproposal'],'headerbgcolor'=>'#656464','headertextcolor'=>'#ffffff','headerimageurl'=>NULL,'section_layout'=>0,'fontstyle'=>$data['fontstyle']));
    }
  }

  function update_header_image_proposal($proposal_id,$imgurl)
  {
    $this->db->where('idproposal',$proposal_id);
    $qy = $this->db->get('proposal_style');
    if($qy && $qy -> num_rows() >= 1)
    {
      return $this->db->query("UPDATE proposal_style SET headerimageurl = '".$imgurl."' WHERE idproposal = ".$proposal_id);
    }
    else
    {
      return $this->db->insert('proposal_style', array('idproposal'=>$proposal_id,'headerbgcolor'=>'#656464','headertextcolor'=>'#ffffff','headerimageurl'=>$imgurl,'logoimageurl'=>NULL,'section_layout'=>0,'fontstyle'=>'Avenir'));
    }
  }

  function update_logo_image_proposal($proposal_id,$imgurl)
  {
    $this->db->where('idproposal',$proposal_id);
    $qy = $this->db->get('proposal_style');
    if($qy && $qy -> num_rows() >= 1)
    {
      return $this->db->query("UPDATE proposal_style SET logoimageurl = '".$imgurl."' WHERE idproposal = ".$proposal_id);
    }
    else
    {
      return $this->db->insert('proposal_style', array('idproposal'=>$proposal_id,'headerbgcolor'=>'#656464','headertextcolor'=>'#ffffff','headerimageurl'=>NULL,'logoimageurl'=>$imgurl,'section_layout'=>0,'fontstyle'=>'Avenir'));
    }
  }

  function insert_header_proposal($data){
    return $this->db->insert('proposal_style',$data);
  }

  function remove_overview($proposal_id)
  {
   return $this->db->query("UPDATE projek SET project_desc ='' WHERE idproposal =".$proposal_id);
  }

  function remove_aboutme($proposal_id)
  {
   return $this->db->query("UPDATE proposal SET aboutyou ='' WHERE id =".$proposal_id);
  }

  function remove_section($tabel,$proposal_id)
  {
    $this -> db -> where('idproposal', $proposal_id);
    return $this->db->update($tabel,array('deleted'=>1)); 
  }

  function remove_nextstep($proposal_id)
  {
    return $this->db->query("UPDATE projek SET nextstep ='' WHERE idproposal =".$proposal_id);
  }

  function remove_additional_section($section,$proposal_id)
  {
    $query =  $this->db->query('delete from section_detail where section_id in (select section_id from section_tambahan where section_name = "'.$section.'" and proposal_id = '.$proposal_id.')');
    if($query)
    {
      $this -> db -> where('proposal_id', $proposal_id);
      $query2 = $this->db->delete('section_tambahan');
      if($query)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }

  public function delete_background($idproposal)
  {
    return $this->db->query("UPDATE proposal_style set headerimageurl = NULL where idproposal = ".$idproposal);
  }

  public function delete_logo($idproposal)
  {
    return $this->db->query("UPDATE proposal_style set logoimageurl = NULL where idproposal = ".$idproposal);
  }

  public function delete_logo_proposal($idproposal)
  {
    return $this->db->query("UPDATE proposal set logo_url = NULL where id = ".$idproposal);
  }

  public function getProposalSummary($proposal_id){
    $this->db->select('b.info_projek, b.project_desc');
    $this->db->from('proposal a');
    $this->db->join('projek b', 'a.id = b.idproposal');
    $this->db->where('a.id', $proposal_id);
    $hasildetail = $this->db->get();
    return $hasildetail->row();
  }

  public function cariProposal($query){
        $iduser = $this->session->userdata('user_id');
        $idteam = $this->session->userdata('team_id');

        $result = $this->db->query('select * from
        (
            (select p.judul, p.id, p.status, p.tgl_buat, p.user_id, p.tgl_kirim, c.nama_pic, c.email, p.arsip, p.created_at, u.user_name, u.team_id
            from proposal p
            join client c on c.id_client=p.id_client
            join user u on u.user_id=p.user_id
            where p.user_id='.$iduser.' and (p.judul LIKE "%'.$query.'%" OR c.nama_pic LIKE "%'.$query.'%"))
            union
            (select p.judul, p.id, p.status, p.tgl_buat, p.user_id, p.tgl_kirim, c.nama_pic, c.email, p.arsip, p.created_at, u.user_name, u.team_id
            from proposal p
            join client c on c.id_client=p.id_client
            join user u on u.user_id=p.user_id
            where p.user_id in (select user_id from user where team_id = '.$idteam.') and share_team=1 and (p.judul LIKE "%'.$query.'%" OR c.nama_pic LIKE "%'.$query.'%"))
        ) t_union
        where arsip=0
        order by created_at desc');
        return $result;
  }

  public function getAllProposal(){
      $iduser = $this->session->userdata('user_id');
    $this->db->select('client.nama_pic,client.email,client.id_client,proposal.judul,proposal.id,proposal.status,proposal.tgl_buat,proposal.user_id,proposal.tgl_kirim');
    $this->db->from('proposal');
    $this->db->join('client', 'proposal.id_client = client.id_client');
    // $this->db->join('user', 'proposal.user_id = user.user_id');
    $this->db->where('proposal.user_id', $iduser);
    $this->db->where('arsip', 0);
    $hasildetail = $this->db->get();
    return $hasildetail->result();
  }

    function proposalByTeam()
    {
        $iduser = $this->session->userdata('user_id');
        $idteam = $this->session->userdata('team_id');

        $result = $this->db->query('select * from
        (
            (select p.judul, p.id, p.status, p.tgl_buat, p.user_id, p.tgl_kirim, c.nama_pic, c.email, p.arsip, p.created_at, u.user_name, u.team_id
            from proposal p
            join client c on c.id_client=p.id_client
            join user u on u.user_id=p.user_id
            where p.user_id='.$iduser.')
            union
            (select p.judul, p.id, p.status, p.tgl_buat, p.user_id, p.tgl_kirim, c.nama_pic, c.email, p.arsip, p.created_at, u.user_name, u.team_id
            from proposal p
            join client c on c.id_client=p.id_client
            join user u on u.user_id=p.user_id
            where p.user_id in (select user_id from user where team_id = '.$idteam.') and share_team=1)
        ) t_union
        where arsip=0
        order by created_at desc');
        return $result->result();
    }

  public function newproposaltoDB($data){
    $this->db->query('INSERT INTO proposal (user_id,judul,id_client,tgl_buat) VALUES ("'.$data['user_id'].'","'.$data['projek'].'","'.$data['client_id'].'","'.$data['tgl_buat'].'")');
    $idPropBaru = $this->db->insert_id();
    if( $idPropBaru !== 0){
      $insertToSection = $this->db->query('INSERT INTO section_baru (sec_urutan,sec_judul,sec_isi,proposal_id,deleted) VALUES ("'.$data['urutan'].'","'.$data['judul_sec'].'","'.$data['overview'].'","'.$idPropBaru.'","0")');
      if($insertToSection){
        return $idPropBaru;
      }else{
        return false;
      }
    }else{
      echo 'Gagal Menambahkan Data';
    }
  }
  public function detailBaruProposal($id){
    $this->db->select('c.nama_pic, c.perusahaan, c.email as client_email, u.user_name, u.user_email, u.team_id, p.judul, p.id, p.logo_url, p.tgl_buat, p.template, p.share_team, p.user_id, p.signature, p.id_client');
    $this->db->from('proposal p');
    $this->db->join('client c', 'p.id_client = c.id_client');
    $this->db->join('user u', 'p.user_id = u.user_id');
    $this->db->where('p.id', $id);
    $hasildetail = $this->db->get();
    return $hasildetail->row();
  }

  public function detailSection($id){
    $this->db->select('sec_id, sec_judul, sec_isi, sec_urutan, sec_tipe');
    $this->db->from('section_baru');
    $this->db->where('proposal_id', $id);
    $this->db->where('deleted', 0);
    $hasil = $this->db->get();
    return $hasil->result();
  }

  public function detailSecTable($secid,$propid){
    $this->db->select('item_id, item, biaya');
    $this->db->from('section_table');
    $this->db->where('sec_id', $secid);
    $this->db->where('proposal_id', $propid);
    $this->db->where('deleted', 0);
    $hasil = $this->db->get();
    return $hasil->result();
  }

  public function detailSecImage($sec,$prop){
    $this->db->select('image_id, image_dir');
    $this->db->from('section_image');
    $this->db->where('sec_id', $sec);
    $this->db->where('prop_id', $prop);
    $this->db->where('deleted', 0);
    $hasil = $this->db->get();
    return $hasil->result();
  }

  public function getImage($id){
    $this->db->select('image_dir');
    $this->db->from('section_image');
    $this->db->where('image_id', $id);
    $hasil = $this->db->get();
    return $hasil->result();
  }

  public function deleteSection($propId,$secId){
    $this->db->set('deleted', '1');
    $this->db->where('proposal_id', $propId);
    $this->db->where('sec_id', $secId);
    $deleted = $this->db->update('section_baru');
    if ($deleted){
      return 1;
    }else{
      return 0;
    }
  }
  
  public function deleteImage($img_id){
    $this->db->set('deleted', '1');
    $this->db->where('image_id', $img_id);
    $deleted = $this->db->update('section_image');
    if ($deleted){
      return 1;
    }else{
      return 0;
    }
  }

  public function addSectionText($prop_id,$urutan,$judul,$isi){
    $data = array(
      'proposal_id' => $prop_id,
      'sec_urutan' => $urutan,
      'sec_judul' => $judul,
      'sec_isi' => $isi,
    );
      $insert = $this->db->insert('section_baru', $data);
    if($insert){
      return 1;
    }else{
      return 0;
    }
  }

  public function addSectionTable($prop_id,$urutan,$judul,$isi,$tipe,$item,$biaya){
    $data = array(
      'proposal_id' => $prop_id,
      'sec_urutan' => $urutan,
      'sec_judul' => $judul,
      'sec_isi' => $isi,
      'sec_tipe' => $tipe
    );
      $insert = $this->db->insert('section_baru', $data);
      if($insert){
        $newSecId = $this->db->insert_id();
        $itemBaru = array(
          'item' => $item,
          'biaya' => $biaya,
          'proposal_id' => $prop_id,
          'sec_id'=> $newSecId
        );
        $insertItem = $this->db->insert('section_table', $itemBaru);
        if($insertItem){
          return 1;
        }else{
          return 0;
        }
      }else{
        return 'Gagal tambah section';
      }
  }

  public function addItemTable($data){
    $insertItem = $this->db->insert('section_table', $data);
    if($insertItem){
      return 1;
    }else{
      return 0;
    }
  }

  public function addSectionImage($prop_id,$urutan,$isi,$judul,$tipe){
    $data = array(
      'proposal_id' => $prop_id,
      'sec_urutan' => $urutan,
      'sec_isi' => $isi,
      'sec_judul' => $judul,
      'sec_tipe' => $tipe
    );
    $insert = $this->db->insert('section_baru', $data);
    if($insert){
      return 1;
    }else{
      return 0;
    }
  }

  public function uploadLogoHeader($data){
    $image = array(
      'logo_url' => $data['image_dir']
    );
    $this->db->where('id',$data['prop_id']);
    $update = $this->db->update('proposal',$image);
    if($update){
      return 1;
    }else{
      return 0;
    }
  }

  // public function updateLogoWithOwnLogo($team, $prop){
  //   $this->db->set('logo_url', 'select team_logo from team where team_id='.$team.'');
  //   $this->db->where('id', $prop);
  //   $update = $this->db->update('proposal');
  //   if($update){
  //     return 1;
  //   }else{
  //     return 0;
  //   }
  // }

  public function uploadImageSection($data){
    $insertImage = $this->db->insert('section_image', $data);
    if($insertImage){
      return 1;
    }else{
      return 0;
    }
  }

  public function updateDetailSection($data,$id){
    $this->db->where('sec_id',$id);
    $update = $this->db->update('section_baru',$data);
    if($update){
      return 1;
    }else{
      return 0;
    }
  }

  public function updateDetailItem($data, $id){
    $this->db->where('item_id',$id);
    $update = $this->db->update('section_table',$data);
    if($update){
      return 1;
    }else{
      return 0;
    }
  }

  public function deleteItem($itemid){
    $this->db->set('deleted', '1');
    $this->db->where('item_id', $itemid);
    $deleted = $this->db->update('section_table');
    if ($deleted){
      return 1;
    }else{
      return 0;
    }
  }

  public function updateTglKirim($tgl,$propid){
    $this->db->set('tgl_kirim', $tgl);
    $this->db->where('id', $propid);
    $update = $this->db->update('proposal');
    if($update){
      echo 1;
    }else{
      echo 0;
    }
  }

  public function getProposalStyle($prop_id){
    $this->db->select('headerbgcolor,headertextcolor,fontstyle');
    $this->db->from('proposal_style');
    $this->db->where('idproposal',$prop_id);
    $hasil = $this->db->get();
    return $hasil->row();
  }

  public function insertProposalStyle($data){
    $insertStyle = $this->db->insert('proposal_style', $data);
    if($insertStyle){
      return 1;
    }else{
      return 0;
    }
  }

  public function updateProposalStyle($data,$id_prop){
    $this->db->where('idproposal', $id_prop);
    $update = $this->db->update('proposal_style', $data);
    if($update){
      return 1;
    }else{
      return 0;
    }
  }

    function updateTemplate($template,$id_prop)
    {
        $update = $this->db->query("UPDATE proposal SET template='$template' WHERE id='$id_prop'");
        if($update){
            return 1;
        }else{
            return 0;
        }
    }

    function updateShareDocument($share,$id)
    {
        $update = $this->db->query("UPDATE proposal set share_team='$share' WHERE id='$id'");
        if($update){
            return 1;
        }else{
            return 0;
        }
    }

    function updateSignature($data, $id)
    {
        $update = $this->db->query("UPDATE proposal set signature='$data' WHERE id='$id'");
        if($update){
            return 1;
        }else{
            return 0;
        }
    }

    function updateJudul($data, $id)
    {
        $update = $this->db->query("UPDATE proposal set judul='$data' WHERE id='$id'");
        if($update){
            return 1;
        }else{
            return 0;
        }
    }

    function updateClientProposal($client, $id)
    {
        $update = $this->db->query("UPDATE proposal set id_client='$client' WHERE id='$id'");
        if($update){
            return 1;
        }else{
            return 0;
        }
    }

    function getTemplate()
    {
        $this->db->select('name,img');
        $this->db->from('template_proposal_layout');
        $this->db->where('status',1);
        $this->db->order_by('id','asc');
        $result = $this->db->get();

        if($result->num_rows()>=1){
            return $result->result();
        } else{
            return 0;
        }
    }

    function getKontenProposal($id)
    {
        $this->db->select('p.id, p.judul, p.share_team, p.user_id, u.team_id');
        $this->db->from('proposal p');
        $this->db->join('user u', 'u.user_id = p.user_id');
        $this->db->where('p.id',$id);
        $hasil = $this->db->get();

        if($hasil->num_rows()>0){
            return $hasil->row();
        } else{
            return false;
        }
    }

    function getKontenBaca($id)
    {
        $result = $this->db->query("
            SELECT b.sec_judul as section, avg(t.screentime) as lama
            FROM proposal_tracking_detail t
            INNER JOIN section_baru b ON b.sec_id = t.section
            WHERE t.proposal_id='$id' and b.deleted=0
            GROUP BY t.section ASC
            HAVING COUNT(*)>0");

        if($result->num_rows()>=1){
            return $result->result();
        } else{
            return 0;
        }
    }

    function getTotalBaca($id)
    {
        $result = $this->db->query("select count(proposal_id) as total from proposal_tracking where proposal_id='$id'");
        if($result->num_rows()>=1){
            return $result->row();
        } else{
            return 0;
        }
    }

    function getBestHourRead($id)
    {
        $result = $this->db->query("SELECT hour(tracking_time) as hour
            FROM proposal_tracking
            WHERE proposal_id='$id'
            GROUP BY hour(tracking_time)
            ORDER BY count(hour(tracking_time)) desc
            limit 1");
        if($result->num_rows()>=1){
            return $result->row();
        } else{
            return 0;
        }
    }

    function getBestMinuteRead($id)
    {
        $result = $this->db->query("SELECT minute(tracking_time) as minute
            FROM proposal_tracking
            WHERE proposal_id='$id'
            GROUP BY minute(tracking_time)
            ORDER BY count(minute(tracking_time)) desc
            limit 1");
        if($result->num_rows()>=1){
            return $result->row();
        } else{
            return 0;
        }
    }
}
