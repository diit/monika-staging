<?php

class Plan_model extends CI_model
{
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function getPlanNow()
    {
        $iduser = $this->session->userdata('user_id');
        $idteam = $this->session->userdata('team_id');

        $this->db->select('*');
        $this->db->from('upgrade');
        $this->db->where('status',1);
        $this->db->order_by('upgrade_time','desc');
        $this->db->limit(1);
        $result = $this->db->get();

        if($result->num_rows() >0){
            return $result->row();
        }
        else{
            return 0;
        }
    }

    // Temporary promo
    function check_promo($kode){
        $this->db->select('id');
        $this->db->from('upgrade_promo');
        $this->db->where('promo_code',$kode);
        $this->db->where('expired_date >', date("Y-m-d h:i:s"));
        $result = $this->db->get();

        if($result->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }
    function check_status_promo($user_id){
        $this->db->select('promo');
        $this->db->from('user');
        $this->db->where('user_id',$user_id);
        $result = $this->db->get();
        return $result->result();
    }

    function update_status_promo($user_id, $data){
        $this->db->where('user_id', $user_id);
        $this->db->update('user', $data);
        return true;
    }
    // end of Temporary promo
}
