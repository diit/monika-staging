<?php
class Invoice_model extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function index(){
        $idteam = $this->session->userdata('team_id');
        $iduser = $this->session->userdata('user_id');
        $result = $this->db->query('select * from
        (
            (select i.id, i.number, i.judul, i.id_proposal, i.id_user, i.id_team, i.note, i.tax, i.status, i.send_invoice, i.due_invoice, i.created_at, c.nama_pic, i.deleted, u.user_name
            from invoice i
            join client c on c.id_client=i.id_client
            join user u on u.user_id=i.id_user
            where id_user='.$iduser.')
            union
            (select i.id, i.number, i.judul, i.id_proposal, i.id_user, i.id_team, i.note, i.tax, i.status, i.send_invoice, i.due_invoice, i.created_at, c.nama_pic, i.deleted, u.user_name
            from invoice i
            join client c on c.id_client=i.id_client
            join user u on u.user_id=i.id_user
            where id_team='.$idteam.' and share_team=1)
        ) t_union
        where deleted=0
        order by created_at desc');
        return $result;
    }

    public function insert_invoice_log($log)
    {
        return $this->db->insert('invoice_log' ,$log);
    }

    public function storeInvoice($data)
    {
        $this->db->insert('invoice', $data);
        return $this->db->insert_id();
    }

    public function totalItemPrice($id)
    {
        $this->db->select('*');
        $this->db->from('pemberian');
        $this->db->where('idproposal',$id);
        return $this->db->get();
    }

    public function cekFromProposal($id)
    {
        $cek = $this->db->query("SELECT id_proposal FROM invoice WHERE id_proposal='$id'");
        if($cek->num_rows()>0){
            return $cek->result();
        } else{
            return false;
        }
    }

    function cekKlien($perusahaan, $team)
    {
        $cek = $this->db->query("SELECT perusahaan FROM client WHERE perusahaan='$perusahaan' and team_id='$team'");
        if($cek->num_rows()>0){
            return $cek->result();
        } else{
            return false;
        }
    }

    public function create($number, $id_prop, $id_user, $note, $tax, $create_invoice, $due_invoice)
    {
        $this->db->query("INSERT INTO invoice (no_invoice, id_proposal, id_user, note, tax, create_invoice, due_invoice) VALUES ('$number', '$id_prop', '$note', '$tax', '$create_invoice', '$due_invoice'");
    }

    public function updateUnArchive($id)
    {
        $this->db->query("UPDATE proposal SET arsip = 0 WHERE id = '$id'");
    }

    function view($id)
    {
        $this->db->select('invoice.*, user.user_name, user.user_email, team.team_name, client.*');
        $this->db->from('invoice');
        $this->db->join('user', 'user.user_id = invoice.id_user');
        $this->db->join('team', 'user.team_id = team.team_id');
        $this->db->join('client', 'client.id_client = invoice.id_client');
        $this->db->where('invoice.deleted', 0);
        $this->db->where('invoice.id', $id);
        $query=$this->db->get();

        if($query->num_rows()>0)
        {
            $detail_invoice = $query->row();
            return $detail_invoice;
        }
        else
        {
            return false;
        }
    }

    function getStyle($id)
    {
        $this->db->select('*');
        $this->db->from('invoice_style');
        $this->db->where('id_invoice', $id);
        $query=$this->db->get();

        if($query->num_rows()>0)
        {
            $get_style = $query->row();
            return $get_style;
        }
        else
        {
            return false;
        }
    }

    function getItem($id)
    {
        $this->db->select('*');
        $this->db->from('invoice_item');
        $this->db->where('id_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function update($no, $id_client, $judul, $note, $tax, $send, $due, $share, $id)
    {
        return $this->db->query("UPDATE invoice SET number = '$no', id_client = '$id_client', judul = '$judul', note = '$note', tax = '$tax', send_invoice = '$send', due_invoice = '$due', share_team = '$share' WHERE id = '$id'");
    }

    function delete_item($id)
    {
      return $this->db->query("DELETE from invoice_item where id_invoice = ".$id);
    }

    function insert_item($item,$harga,$id){
      return $this->db->query("INSERT INTO invoice_item(item,harga,id_invoice) VALUES ('$item','$harga','$id')");
    }

    function archive($id)
    {
        return $this->db->query("UPDATE invoice SET deleted = 1, updated_at = now() WHERE id = '$id'");
    }

    public function updateStatusBaca($key,$val){
        return $this->db->query("UPDATE invoice SET status ='$val' WHERE id='$key' and status < '$val' ");
    }

    public function updateLastBaca($key,$val){
        return $this->db->query("UPDATE invoice SET last_baca = '$val' WHERE id='$key'");
    }

    public function updateCounterBaca($key){
       return $this->db->query("UPDATE invoice SET counter_baca = counter_baca+1 WHERE id='$key'");
    }

    public function updateCounterPreview($key){
       return $this->db->query("UPDATE invoice SET preview = preview+1 WHERE id='$key'");
    }

    public function getInvoiceByUser($user_id){
        $result=$this->db->query("SELECT a.id, a.judul, b.nama_pic FROM invoice a join client b on a.id_client = b.id_client WHERE a.id_user = '$user_id'");
        if($result->num_rows()>0){
          return $result->num_rows();
        }
        else
        {
          return 0;
        }
      }

    public function getInvoiceByTeam(){
        $idteam = $this->session->userdata('team_id');
        $iduser = $this->session->userdata('user_id');
        $result = $this->db->query('select * from
        (
            (select i.id, i.status, i.deleted
            from invoice i
            where id_user='.$iduser.')
            union
            (select i.id, i.status, i.deleted
            from invoice i
            where id_team='.$idteam.' and share_team=1)
        ) t_union
        where deleted=0');
        return $result;
    }

    function getInvoiceByStatus($status){
        $idteam = $this->session->userdata('team_id');
        $iduser = $this->session->userdata('user_id');
        $result = $this->db->query('select * from
        (
            (select i.id, i.status, i.deleted
            from invoice i
            where id_user='.$iduser.')
            union
            (select i.id, i.status, i.deleted
            from invoice i
            where id_team='.$idteam.' and share_team=1)
        ) t_union
        where deleted=0 and status='.$status.'');
        return $result->result();
      }

      function getInvoiceTeamByStatus($team_id, $status){
        $this->db->select('*');
        $this->db->from('invoice');
        $this->db->where('id_user in (select user_id from user where team_id = '.$team_id.') ');
        $this->db->where('status',$status);
        $hasil = $this->db->get();
        return $hasil;
      }

      function update_logo_image_invoice($id_invoice,$imgurl)
      {
        $this->db->where('id_invoice',$id_invoice);
        $qy = $this->db->get('invoice_style');
        if($qy && $qy -> num_rows() >= 1)
        {
          return $this->db->query("UPDATE invoice_style SET logoimageurl = '".$imgurl."' WHERE id_invoice = ".$id_invoice);
        }
        else
        {
          return $this->db->insert('invoice_style', array('id_invoice'=>$id_invoice,'bgcolor'=>'#ffffff','textcolor'=>'#000000','logoimageurl'=>$imgurl,'layout'=>0,'fontstyle'=>'Avenir'));
        }
      }

    function updateStatusPayment($status, $id)
    {
        return $this->db->query("UPDATE invoice SET status = '$status'  WHERE id = '$id'");
    }

    public function get_tracking_id($invoice_id)
      {
        $this->db->select('tracking_id');
        $this->db->from('invoice_tracking');
        $this->db->where('invoice_id', $invoice_id);
        $this->db->order_by('tracking_id desc');
        $hasil = $this->db->get();
        return $hasil->row_array();
      }

    public function insert_new_tracking($data_tracking,$detail_tracking)
      {
        $this->db->insert('invoice_tracking', $data_tracking);
        $tracking_id = $this->db->insert_id();

        foreach ($detail_tracking as $key => $value)
        {
          foreach ($value as $ky => $vl) {
            $this->db->insert('invoice_tracking_detail', array(
              'section'=>$ky,
              'screentime'=>$vl,
              'tracking_id'=>$tracking_id,
              'invoice_id'=>$data_tracking['invoice_id']
            ));
          }
        }

        return true;
      }

      public function update_tracking($tracking_id,$section,$screentime,$invoice_id){
        return $this->db->query("UPDATE invoice_tracking_detail SET screentime = ".$screentime." WHERE tracking_id = ".$tracking_id." and section = '".$section."' and invoice_id = ".$invoice_id);
      }

      public function cari_invoice($query){
        $idteam = $this->session->userdata('team_id');
        $iduser = $this->session->userdata('user_id');
        $result = $this->db->query('select * from
        (
            (select i.id, i.number, i.judul, i.id_proposal, i.id_user, i.id_team, i.note, i.tax, i.status, i.send_invoice, i.due_invoice, i.created_at, c.nama_pic, i.deleted, u.user_name
            from invoice i
            join client c on c.id_client=i.id_client
            join user u on u.user_id=i.id_user
            where id_user='.$iduser.' and (i.judul LIKE "%'.$query.'%" OR c.nama_pic LIKE "%'.$query.'%"))
            union
            (select i.id, i.number, i.judul, i.id_proposal, i.id_user, i.id_team, i.note, i.tax, i.status, i.send_invoice, i.due_invoice, i.created_at, c.nama_pic, i.deleted, u.user_name
            from invoice i
            join client c on c.id_client=i.id_client
            join user u on u.user_id=i.id_user
            where id_team='.$idteam.' and share_team=1 and (i.judul LIKE "%'.$query.'%" OR c.nama_pic LIKE "%'.$query.'%"))
        ) t_union
        where deleted=0
        order by created_at desc');
        return $result;
      }

    function updateTglKirim($tgl,$id)
    {
        $this->db->query("UPDATE invoice SET send_invoice='$tgl' where id='$id'");
    }

    function updateShareDocument($share,$id)
    {
        $update = $this->db->query("UPDATE invoice set share_team='$share' WHERE id='$id'");
        if($update){
            return 1;
        }else{
            return 0;
        }
    }
}
?>
