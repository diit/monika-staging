<?php
  class Client_model extends CI_Model{
    function __construct(){
      parent::__construct();
      $this->load->database();
    }

    function getCurrentAI(){
      $result = $this->db->query("SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'kurap-monika' AND TABLE_NAME = 'client'");
      return $result->row_array();
    }

    function getClient($id){
      $result=$this->db->query("SELECT * FROM client WHERE id_client='$id'");
      if($result->num_rows()>0){
        return $result->result();
      }else{
        return false;
      }
    }

      function getAllClientTeam($id)
      {
          $this->db->select('id_client, nama_pic');
          $this->db->from('client');
          $this->db->where('team_id',$id);
          $this->db->order_by('nama_pic','asc');
          $query = $this->db->get();
          return $query->result();
      }

    function tampil_where($table, $where){
		    return $this->db->get_where($table, $where);
	   }

     function klien_per_page($number,$offset, $team_id, $query){
          $this->db->select('*');
          $this->db->from('client');
          $this->db->where('team_id', $team_id);
          $this->db->where('(perusahaan LIKE "%'.$query.'%" OR nama_pic LIKE "%'.$query.'%")', NULL, FALSE);
          $this->db->where('deleted',0);
          $this->db->limit($number, $offset);
          $isi = $this->db->get();
		      return $isi;
          // $query = $this->db->get('client',$number,$offset)->result();
	   }

      function cariKlien($query){
          $idteam = $this->session->userdata('team_id');

          $this->db->select('*');
          $this->db->from('client');
          $this->db->where('team_id', $idteam);
          $this->db->where('(perusahaan LIKE "%'.$query.'%" OR nama_pic LIKE "%'.$query.'%")');
          return $this->db->get();
      }

    function getAllClient(){
      $result=$this->db->query("SELECT * FROM client");
      if($result->num_rows()>0){
        return $result->result();
      }else{
        return false;
      }
    }
    function checkClient($team_id){
      $this->db->select('nama_pic, perusahaan, email, id_client');
      $this->db->from('client');
      $this->db->where('team_id', $team_id);
      $this->db->order_by('id_client','desc');
      $this->db->LIMIT(1);
      $result = $this->db->get();
      return $result;
    }

    function setClient($pic,$perusahaan,$email,$telephone,$alamat,$kota,$user_id,$team_id){
      $this->db->query("INSERT INTO client (nama_pic, perusahaan, email, telephone, alamat_usaha, kota, user_id, team_id) VALUES ('$pic','$perusahaan','$email','$telephone','$alamat','$kota','$user_id','$team_id')");
        $id = $this->db->insert_id();
        return $id;
    }

    function setClientDuplicateProposal($data_new_client)
    {
      $this->db->insert('client',$data_new_client);
      return $this->db->insert_id();
    }

    function updateClient($pic,$perusahaan,$email,$telephone,$alamat,$kota,$id_client){
      return $this->db->query("UPDATE client SET nama_pic='$pic', perusahaan='$perusahaan', email='$email', telephone='$telephone', alamat_usaha='$alamat', kota='$kota' WHERE id_client='$id_client'");
    }

    function updateClientDataFromInvoice($alamat,$kota,$id){
      return $this->db->query("UPDATE client SET alamat_usaha='$alamat',kota='$kota' WHERE id_client='$id'");
    }

    function cekKlienInvoice($id_kl,$id_in){
      $this->db->select('judul');
      $this->db->from('invoice');
      $this->db->where('id', $id_in);
      $this->db->where('id_client', $id_kl);
      $result = $this->db->get();
      if ($result->num_rows()>0 ){
        return 1;
      }else{
        return 0;
      }
    }

    function updateClientInvoice($pic,$perusahaan,$email,$telephone,$alamat,$kota,$id_client){
      $this->db->select('*');
      $this->db->from('client');
      $this->db->where('id_client', $id_client);
      $this->db->where('email', $email);
      $this->db->order_by('id_client','desc');
      $result = $this->db->get();
      if($result->num_rows()>0)
      {
        if($result->row()->nama_pic != $pic)
        {
          return $this->db->query("UPDATE client SET nama_pic='$pic', perusahaan='$perusahaan', email='$email', telephone='$telephone', alamat_usaha='$alamat', kota='$kota' WHERE id_client='$id_client'");
        }
        return $id_client;
      }else{
        $klien = array(
            'nama_pic' => $pic,
            'perusahaan' => $perusahaan,
            'email'=>$email,
            'alamat_usaha' => $alamat,
            'kota' => $kota,
            'user_id' => $this->session->userdata('user_id'),
            'team_id' => $this->session->userdata('team_id'),
        );

        $this->db->insert('client', $klien);
        return $this->db->insert_id();
      }
    }

    function delete($id){
      return $this->db->query("UPDATE client set deleted=1 WHERE id_client='$id'");
    }

    function storeClient($data)
    {
      //check client first by email
      $this->db->select('*');
      $this->db->from('client');
      $this->db->where('team_id', $data['team_id']);
      $this->db->where('email', $data['email']);
      $this->db->order_by('id_client','desc');
      $result = $this->db->get();
      if($result->num_rows()>0)
      {
        if($result->row()->nama_pic != $data['nama_pic'])
        {
          $this->db->query("UPDATE client SET nama_pic='".$data['nama_pic']."', perusahaan='".$data['perusahaan']."' WHERE id_client=".$result->row()->id_client);
        }
        return $result->row()->id_client;
      }else{
        $this->db->insert('client', $data);
        return $this->db->insert_id();
      }
    }

    public function teamName($id){
      $this->db->select('team_name');
      $this->db->from('team');
      $this->db->where('team_id', $id);
      $hasil = $this->db->get();
      return $hasil->row();
    }
    
  }
 ?>
