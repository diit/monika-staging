<?php
class Questionnaire_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

	function getCurrentAI(){
      $result = $this->db->query("SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'kurap-monika' AND TABLE_NAME = 'questionnaires'");
      return $result->row_array();
    }

  function getAllQuest($user_id){
    $this->db->select('id,nama_projek, status');
    $this->db->from('questionnaires');
    $this->db->where('user_id', $user_id);
    $getData = $this->db->get();
    $result = $getData->result();
      return $result;
  }
	function insert_questionnaires_baru($user_id,$urutan,$status,$counter_baca,$tgl_buat){
			$data_questionnaires = array(
								'user_id'=>$user_id,
								'urutan'=>$urutan,
								'status'=>$status,
								'tgl_buat'=>$tgl_buat,
								'counter_baca'=>$counter_baca
							);
    	$this->db->insert('questionnaires', $data_questionnaires);
//      $id_questionnaires = $this->db->insert_id();
//      return $id_questionnaires;
	}

	function insert_detail_questionnaires_baru($judul_quest,$isi_quest,$data_id_quest,$questbaru_id){
			$data_questionnaires_detail = array(
								'judul'=>$judul_quest,
								'isi'=>$isi_quest,
								'data_id'=>$data_id_quest,
								'quest_id'=>$questbaru_id
							);
    	$this->db->insert('questionnaires_detail', $data_questionnaires_detail);

	}

	function get_detail_questionnaires($quest_id){
			$this->db->select('judul, isi, data_id');
			$this->db->from('questionnaires_detail');
			$this->db->where('quest_id', $quest_id);
			$detail_questionnaires = $this->db->get();
			$result = $detail_questionnaires->result();
			return $result;
	}

	function get_max_data_id($quest_id){
			$this->db->select('MAX(data_id) as maxid');
			$this->db->from('questionnaires_detail');
			$this->db->where('quest_id', $quest_id);
			$max_data_id = $this->db->get();
			$result = $max_data_id->result();
			return $result;
	}

	function update_urutan_pertanyaan($urutan,$quest_id,$user_id){
			$this->db->set('urutan', $urutan);
			$this->db->where('id', $quest_id);
			$this->db->where('user_id', $user_id);
			$this->db->update('questionnaires');
	}

	function hapus_pertanyaan($data_id_quest,$quest_id){
//      $id = $this->db->query('SELECT id FROM questionnaires_detail WHERE quest_id='.$quest_id.' AND data_id='.$data_id_quest.'');
//
////      $this->db->query('DELETE FROM questionnaires_detail WHERE quest_id='.$quest_id.' AND data_id='.$data_id_quest.'');
      $this->db->where('quest_id', $quest_id);
			$this->db->where('data_id', $data_id_quest);
			$this->db->delete('questionnaires_detail');
	}

  function update_judul_pertanyaan($quest_id,$data_id_quest,$updateJudul){
      $this->db->set('judul', $updateJudul);
			$this->db->where('quest_id', $quest_id);
			$this->db->where('data_id', $data_id_quest);
			$this->db->update('questionnaires_detail');
  }

  function update_isi_pertanyaan($quest_id,$data_id_quest,$updateIsi){
      $this->db->set('isi', $updateIsi);
			$this->db->where('quest_id', $quest_id);
			$this->db->where('data_id', $data_id_quest);
			$this->db->update('questionnaires_detail');
  }
    function update_nama_projek_quest($quest_id,$namaProjekUpdate,$user_id){
			$this->db->set('nama_projek', $namaProjekUpdate);
			$this->db->where('id', $quest_id);
			$this->db->where('user_id', $user_id);
			$this->db->update('questionnaires');
	}

}

?>
