<?php
  /**
   *
   */
  class Keuntungan_model extends CI_Model
  {

    function __construct()
    {
      parent:: __construct();
    }

    function getCurrentAI(){
      $result = $this->db->query("SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'kurap-monika' AND TABLE_NAME = 'keuntungan'");
      return $result->row_array();
    }

    function insert_keuntungan($keuntungan,$idproposal){
      $data_keuntungan = array(
                          'keuntungan'=>$keuntungan,
                          'idproposal'=>$idproposal
                          );
      $this->db->insert('keuntungan', $data_keuntungan);
      $idkeuntungan = $this->db->insert_id();
      return $idkeuntungan;
      //return $this->db->query("INSERT INTO keuntungan (keuntungan,idproposal) VALUES ('$keuntungan','$idproposal')");
    }

    function update($idprop,$keuntungan){
      return $this->db->query("UPDATE keuntungan SET keuntungan = '$keuntungan' WHERE idproposal = '$idprop'");
    }

    public function update_section_keuntungan($proposal_id,$data_projek){
      $this->db->where('idproposal',$proposal_id);
      return $this->db->update('keuntungan', $data_projek);
    }
  }

 ?>
