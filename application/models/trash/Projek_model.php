<?php
  class Projek_model extends CI_Model{
    function __construct(){
      parent::__construct();
    }

    public function insert_projek($info_projek,$projek_desc,$user_id,$nextstep,$idproposal){
      return $this->db->query("INSERT INTO `projek` (`info_projek`,`project_desc`,`user_id`,`nextstep`,`idproposal`) VALUES('$info_projek','$projek_desc','$user_id','$nextstep','$idproposal')");
    }

    public function cek_nama_projek($info)
  	{
  	  $this->db->where('info_projek',$info['info_projek']);
  	  $query = $this -> db -> get('projek');
  	  return $query -> num_rows();
  	}
    public function update_info($idproposal, $info){
      $query="UPDATE `projek` SET `info_projek`='$info' WHERE idproposal = '$idproposal'";
      return $this->db->query($query);
    }
    public function update_nextstep($idproposal, $nextstep){
      $query="UPDATE `projek` SET `nextstep`='$nextstep' WHERE idproposal = '$idproposal'";
      return $this->db->query($query);
    }

    public function update_section_projek($proposal_id,$data_projek){
      $this->db->where('idproposal',$proposal_id);
      $this->db->where('user_id',$this->session->userdata('user_id'));
      return $this->db->update('projek', $data_projek);
    }
  }

 ?>
