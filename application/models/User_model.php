<?php

use Mailgun\Mailgun;
class User_model extends CI_model{

public function register_team($team){
  $this->db->insert('team', $team);
  return $this->db->insert_id();
}

public function register_user($user){
  $this->db->insert('user', $user);
  return $this->db->insert_id();
}

function tampil_all($table){
		return $this->db->get($table);
	}


public function insertuser($data)
  {
    return $this->db->insert('user', $data);
  }

public function verifyemail($key)
  {
    $data = array('status' => 1);
        $this->db->where('md5(user_email)', $key);
        return $this->db->update('user', $data);
  }

  function update($id,$value,$modul){
    $this->db->where(array("user_id"=>$id));
    $this->db->update("user",array($modul=>$value));
  }

  function updateProfil($user_name,$user_corp,$user_mobile,$alamat,$kota,$user_id,$team_id){
    $this->db->where('user_id',$user_id);
    $data = array('user_name'=> $user_name,
                  'user_mobile'=> $user_mobile
                );
    $this->db->update("user",$data);

    $this->db->where('team_id',$team_id);
    $data_team = array('team_name'=> $user_corp,
                  'team_address'=> $alamat,
                  'team_city'=> $kota
                );
    $this->db->update("team",$data_team);
  }

  function invite_team($data)
  {
    $this->db->insert('team_invite', $data);
    return $this->db->insert_id();
  }

  function check_invitation($email)
  {
    $this->db->select('a.email,b.*');
    $this->db->from('team_invite a');
    $this->db->join('user b','a.user_id = b.user_id');
    $this->db->where('a.email',$email);
    $this->db->where('b.team_id > 0');
    $result = $this->db->get();
    if($result->num_rows()>0){
      return $result->row_array();
    }else{
      return false;
    }
  }

  function check_invitation_by_id($invite_id)
  {
    $this->db->select('a.email,b.*');
    $this->db->from('team_invite a');
    $this->db->join('user b','a.user_id = b.user_id');
    $this->db->where('a.id',$invite_id);
    $this->db->where('b.team_id > 0');
    $result = $this->db->get();
    if($result->num_rows()>0){
      return $result->row_array();
    }else{
      return false;
    }
  }

  function update_email_invitation($invite_id,$email)
    {
      $data = array('email' => $email);
      $this->db->where('id',$invite_id);
      return $this->db->update("team_invite",$data);
    }

  function join_team($invitor,$user_id)
  {
    $result_update = $this->db->query('
                      UPDATE user a
                      INNER JOIN team_invite b ON b.email = a.user_email
                      INNER JOIN user c on c.user_id = b.user_id
                      SET a.team_id = c.team_id, a.role = 2
                      WHERE c.user_id = '.$invitor.' and a.user_id = '.$user_id
                    );
    if($result_update)
    {
      $result=$this->db->query("SELECT * FROM user WHERE user_id = ".$user_id);
      if($result->num_rows()>0){
        return $result->row();
      }else{
        return false;
      }
    }
    else
    {
      return false;
    }

  }

  public function get_team_logo($team_id)
  {
    $this->db->select('team_logo');
    $this->db->where('team_id',$team_id);
    $query = $this -> db -> get('team');
    if($query && $query -> num_rows() >= 1)
    {
      return $query->row();
    }else{
      return false;
    }
  }

  function update_team_logo_image($team_id,$imgurl)
  {
    return $this->db->query("UPDATE team SET team_logo = '".$imgurl."' WHERE team_id = ".$team_id);
  }

  function get_team_member($team_id)
  {
    /*
    $this->db->select('a.user_name, a.role');
    $this->db->from('user a');
    $this->db->join('team b','a.team_id = b.team_id');
    $this->db->where('b.team_id',$team_id);
    $result = $this->db->get();
    */
    $result = $this->db->query('
                    SELECT a.user_name, a.role
                    FROM user a
                    JOIN team b ON a.team_id = b.team_id
                    WHERE b.team_id = '.$team_id.'
                    union 
                    SELECT a.email, "0" as role
                    FROM team_invite a
                    join user b on a.user_id = b.user_id
                    where b.team_id = '.$team_id.' and a.email not in (select user_email from user where team_id = b.team_id)
              ');//
    if($result->num_rows()>0){
      return $result->result();
    }else{
      return false;
    }
  }

  function getUser($id){
    $result=$this->db->query("SELECT a.user_id, a.user_name, a.user_email, a.user_password, a.user_mobile, a.status, a.tutorial, a.initial_proposal, a.kategori, a.about, a.team_id, b.team_name as user_corp,b.team_address as alamat,b.team_city as kota FROM user a join team b on a.team_id = b.team_id WHERE a.user_id=".$id);
    if($result->num_rows()>0){
      return $result->row();
    }else{
      return false;
    }
  }

  function insert_referral($data_referral)
  {
    return $this->db->insert('user_referral', $data_referral);
  }

  public function getReferralByUser($user_id){
    $result=$this->db->query("SELECT * FROM user_referral a join user b on a.referral_email = b.user_email WHERE a.user_id = ".$user_id);
    if($result->num_rows()>0){
      return $result->num_rows();
    }
  }

  function check_referral($refer_email)
  {
    $result=$this->db->query("SELECT * FROM user_referral WHERE referral_email = '".$refer_email."'");
    if($result->num_rows()>0){
      return $result->row();
    }else{
      return false;
    }
  }

  function getUserByEmailHash($kdemail){
    $result=$this->db->query("SELECT * FROM user WHERE md5(user_email) = '".$kdemail."'");
    if($result->num_rows()>0){
      return $result->row_array();
    }else{
      return false;
    }
  }

  public function aktifkan($user_id)
  	{
  		$data = array('status' => 1);
      $this->db->from('user');
  		$this->db->where('user_id',$user_id);
      $this->db->update('user', $data);
  	}

  public function nonaktif($user_id)
  	{
  		$data = array('status' => 2);
      $this->db->from('user');
  		$this->db->where('user_id',$user_id);
      $this->db->update('user', $data);
  	}

    function posts_save($id,$fields)
      {
            $this ->db
                  ->where('user_id',$id)
                  ->update('user',$fields);
      }


public function changepassword($key,$newpass)
  {
    $this->db->query("UPDATE user SET user_password = '$newpass' WHERE md5(user_email) = '$key'");
  }

public function login_user($email, $pass){

  $this->db->select('a.*,datediff(CURDATE(),a.create_date) as trial_day,b.team_name');
  $this->db->from('user a');
  $this->db->join('team b','a.team_id = b.team_id','LEFT');
  $this->db->where('user_email',$email);
  $this->db->where('user_password',$pass);

  if($query=$this->db->get())
  {
      return $query->row_array();
  }
  else{
    return false;
  }
}

//function get_skill_portfolio_team($team_id)
//{
//        $user_login['skill'] = array();
//        $this->db->select('a.skillname');
//        $this->db->from('userskill a');
//        $this->db->join('user b','b.user_id = a.user_id');
//        $this->db->join('team c','c.team_id = b.team_id');
//        $this->db->where('b.team_id',$team_id);
//        $this->db->where('role',1);
//        $query2=$this->db->get();
//        if($query2 && $query2->num_rows() > 0)
//        {
//          $skill = $query2->result_array();
//          foreach ($skill as $sk)
//          {
//            array_push($user_login['skill'], $sk['skillname']);
//          }
//        }
//
//        $user_login['portfolio'] = array();
//        $this->db->select('a.portfolio');
//        $this->db->from('userportfolio a');
//        $this->db->join('user b','b.user_id = a.user_id');
//        $this->db->join('team c','c.team_id = b.team_id');
//        $this->db->where('b.team_id',$team_id);
//        $this->db->where('role',1);
//        $query3=$this->db->get();
//        if($query3 && $query3 -> num_rows() > 0)
//        {
//          $portfolio = $query3->result_array();
//          foreach ($portfolio as $pt)
//          {
//            array_push($user_login['portfolio'], $pt['portfolio']);
//          }
//        }
//
//        return $user_login;
//}

public function check_account($email)
{
  $this->db->where('user_email',$email);
  $query = $this -> db -> get('user');
  if($query && $query -> num_rows() >= 1)
  {
    return $query->result();
  }else{
    return false;
  }
}

public function get_email($kdemail)
{
  $this->db->where('md5(user_email)',$kdemail);
  $query = $this -> db -> get('user');
  if($query && $query -> num_rows() >= 1)
  {
    return $query->row_array();
  }else{
    return false;
  }
}

public function verified($email, $pass){
  $this->db->select('*');
  $this->db->from('user');
  $this->db->where('user_email',$email);
  $this->db->where('user_password',$pass);
  $this->db->where('status >= 1');

  if($query=$this->db->get())
  {
      return $query->row_array();
  }
  else{
    return false;
  }
}
  public function login_admin($username, $pass){

    $this->db->select('*');
    $this->db->from('admin');
    $this->db->where('user_name',$username);
    $this->db->where('user_password',$pass);

    if($query=$this->db->get())
    {
        return $query->row_array();
    }
    else{
      return false;
    }
}

public function email_check($email){
  $explode = explode('@',$email);
  if($explode[1]=="gmail.com"){
    $query = "SELECT user_email FROM user WHERE REPLACE(user_email,'.','') = REPLACE('$email','.','')";
  }else{
    $query = "SELECT user_email FROM user WHERE user_email = '$email'";
  }
  $r = $this->db->query($query);
  if($r->num_rows()>0){
    return $r->result();
  }else{
    return false;
  }
}

public function phone_check($phone){
  $query = "SELECT user_mobile FROM user WHERE user_mobile = ".$phone;
  $r = $this->db->query($query);
  if($r->num_rows()>0)
  {
    return $r->result();
  }else{
    return false;
  }
}



public function getAllEmail()
{
  $this->db->select('user_email');
  $this->db->from('user');
  $query=$this->db->get();

  if($query->num_rows()>0){
    return $query->result();
  }else{
    return false;
  }
}

public function getUserByEmail($email){
  $result = $this->db->query("SELECT * FROM user WHERE user_email='$email'");
  if($result->num_rows()>0){
    return $result->row_array();
  }
}

//funtion to get email of user to send password
 public function ForgotPassword($email)
 {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_email', $email);
        $query=$this->db->get();
        return $query->row_array();
 }

 public function sendpassword($data)
 {
         date_default_timezone_set('GMT');
         $this->load->helper('string');
         $email = $data['user_email'];
         $query1=$this->db->query("SELECT *  from user where user_email = '".$email."' ");
         $row=$query1->result_array();
         if ($query1->num_rows()>0)

         {
             $passwordplain = "";
             $passwordplain = random_string('alnum', 16);
             $newpass['user_password'] = md5($passwordplain);
             $this->db->where('user_email', $email);
             $this->db->update('user', $newpass);

             $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
             $domain = "ml.monika.id";

             # Make the call to the client.
             $result = $mgClient->sendMessage($domain, array(
               'from'    => 'Monika <info@monika.id>',
               'to'      => $email,
               'subject' => 'Permintaan Reset Password',
               'html'    =>  'Thanks for contacting regarding to forgot password,
                            <br> Your new Password is <b>'.$passwordplain.'</b> <br> Please update your password <br>',
             ));

         } else {
            $this->session->set_flashdata('msg','Email not found try again!');
         }
           redirect('login');
         }


     public function kirim_password($email)
     {
           $saltid  = md5($email);
                 // Send email to ....
                 $url = base_url()."user/get_password/".$saltid;
                 $mgClient = new Mailgun('key-93127b80aa482b6b74c45ba849feaafb');
                 $domain = "ml.monika.id";
                 $url = site_url('user/resetform');
                 # Make the call to the client.
                 $result = $mgClient->sendMessage($domain, array(
                   'from'    => 'Monika <info@monika.id>',
                   'to'      => $email,
                   'subject' => 'Silahkan reset password anda',
                   'html'    => "<html><head><head></head><body><p>Hi,</p><p>Link untuk password telah tersedia <h3>Monika.id</h3>.</p><p>Silahkan klik link dibawah ini untuk ubah password kamu.</p><a href='".$url."/".$saltid."'>Klik Disini</a><br/><p>Salam kami,</p><p>Monika.id Team</p></body></html>",
                 ));


             }

      public function setTutorial($id,$val){
        return $this->db->query("UPDATE user SET tutorial=$val WHERE user_id='$id'");
      }

      public function setInitialProposal($email,$val){
        return $this->db->query("UPDATE user SET initial_proposal=$val WHERE user_email='$email'");
      }

//    public function getUserCategory()
//    {
//      $this -> db -> where('status', 1);
//      return $this -> db -> get('kategori') -> result();
//    }

    public function getTutorialStatus($user_id)
    {
      $this -> db -> select('tutorial');
      //$this -> db -> where('status', 1);
      $this -> db -> where('user_id', $user_id);
      $hasil = $this -> db -> get('user') -> row_array();
      return $hasil['tutorial'];
    }

    public function update_user_info($data,$user_id)
    {
      $this->db->where('user_id',$user_id);
      return $this->db->update("user",$data);
    }

//    public function delete_skill($user_id)
//    {
//      return $this->db->query("DELETE from userskill where user_id = ".$user_id);
//    }
//
//    public function delete_portfolio($user_id)
//    {
//      return $this->db->query("DELETE from userportfolio where user_id = ".$user_id);
//    }
//
//    public function insert_skill($skill,$user_id){
//      return $this->db->query("INSERT INTO userskill (skillname,user_id) VALUES ('".$skill."',".$user_id.")");
//    }
//
//    public function insert_portfolio($portfolio,$user_id){
//      return $this->db->query("INSERT INTO userportfolio (portfolio,user_id) VALUES ('".$portfolio."',".$user_id.")");
//    }

    function user_per_page($number,$offset,$query){
         $this->db->select('*');
         $this->db->from('user');
         $this->db->where('(user_name LIKE "%'.$query.'%")', NULL, FALSE);
         $this->db->limit($number, $offset);
         $isi = $this->db->get();
         return $isi;
         // $query = $this->db->get('client',$number,$offset)->result();
    }

    public function getcurrentplan()
    {
        $iduser = $this->session->userdata('user_id');

        $this->db->select('a.*,b.plan');
        $this->db->from('upgrade a');
        $this->db->join('plan b','a.plan_id = b.id');
        $this->db->where('a.user_id',$iduser);
        $this->db->order_by('upgrade_time asc');
        $this->db->limit(1);
        $plan = $this->db->get();
        return $plan->row();
    }

    function remainTrialDay()
    {
        $trial = $this->session->userdata('trial_day');

        if (($trial>=0)&&($trial < 14)){
            return 14-$trial;
        }
        else{
            return false;
        }
    }

    public function getplanhistory($team_id)
    {
      $this->db->select('a.*,b.plan');
         $this->db->from('upgrade a');
         $this->db->join('plan b','a.plan_id = b.id');
         $this->db->where('a.team_id',$team_id);
         $this->db->order_by('upgrade_time asc');
         $plan = $this->db->get();
         return $plan->result();
    }

    function insertPlanFromTrial()
    {
        $iduser = $this->session->userdata('user_id');

        if (empty(getcurrentplan())){
            return 0;
        }
        else{
            return 1;
        }
    }

    public function invoice_detail($invoice)
    {
      $this->db->select('a.*,b.plan,b.price,c.team_name,(select disc from upgrade_promo where promo_code = a.promo_code and expired_date > now() order by expired_date asc limit 1) as disc');
         $this->db->from('upgrade a');
         $this->db->join('plan b','a.plan_id = b.id');
         $this->db->join('team c','c.team_id = a.team_id');
         $this->db->where('a.invoice',$invoice);
         $plan = $this->db->get();
         return $plan->row();
    }

    public function insert_upgrade_data($data)
    {
      return $this->db->insert('upgrade', $data);
    }

    public function plan_paid($invoice, $team_id, $payment_method)
    {
      $this->db->where(array("invoice"=>$invoice,"team_id"=>$team_id,"status"=>0));
      return $this->db->update("upgrade",array("payment_method"=>$payment_method,"status"=>1,"payment_time"=>date("Y-m-d h:i:s"),"active_time"=>date("Y-m-d h:i:s")));
    }

    public function update_logo_transfer_receipt($team_id,$invoice,$receipt)
    {
      $this->db->where(array("invoice"=>$invoice,"team_id"=>$team_id,"status"=>0));
      return $this->db->update("upgrade",array("payment_method"=>1,"transfer_receipt"=>$receipt,"payment_time"=>date("Y-m-d h:i:s"))); 
    }

    function check_promo($promo)
    {
      $result = $this->db->query("SELECT * FROM upgrade_promo WHERE promo_code = '$promo' and expired_date > now()");
      if($result->num_rows()>0){
        return $result->row();
      }
      else
      {
        return false;
      }
    }

    function updateUsaha($name)
    {
        $team_id = $this->session->userdata('team_id');
        $this->db->query("UPDATE team SET team_name = '$name' WHERE team_id = '$team_id'");
    }

    function updateAbout($desc)
    {
        $user_id = $this->session->userdata('user_id');
        $this->db->query("UPDATE user SET about = '$desc' WHERE user_id = '$user_id'");
    }

    function getEmailInvitation($id)
    {
        $this->db->select('*');
        $this->db->from('team_invite');
        $this->db->where('id',$id);
        return $this->db->get()->row();
    }

}


?>
