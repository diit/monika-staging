<?php
/**
 * Class proposal untuk mengatur semua kelengkapan proposal
 */
class Laporan_model extends CI_model
{
//  public function proposal(){
//    $this->db->select('client.nama_pic, client.email,projek.info_projek, proposal.id, proposal.tgl_buat, proposal.status, proposal.duplicate_from');
//    $this->db->from('proposal');
//    $this->db->join('projek' , 'projek.idproposal = proposal.id');
//    $this->db->join('client' , 'client.id_client = proposal.id_client');
//    $this->db->where('proposal.user_id', $this->session->userdata('user_id'));
//    $this->db->where('proposal.arsip', 0);
//    $this->db->order_by('proposal.id desc');
//    $hasil = $this->db->get();
//    return $obj_proposal = $hasil->result();
//  }

//  function get_read_time_range()
//  {
//    if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
//    {
//      if($this->monikalib->getUserPlan()->plan_id == 1 || $this->monikalib->getUserPlan()->plan_id == 2)
//      {
//        $hasil = $this->db->query('
//                                  select count(HOUR(last_baca)) as count, HOUR(last_baca) as hour
//                                  from proposal
//                                  where counter_baca > 0 and user_id = '.$this->session->userdata('user_id').' and id_client > 1
//                                  group by HOUR(last_baca)
//                                  order by hour asc
//                                ');
//      }
//      else if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
//      {
//        $hasil = $this->db->query('
//                          select count(HOUR(last_baca)) as count, HOUR(last_baca) as hour
//                          from proposal
//                          where counter_baca > 0 and user_id in (select user_id from user where team_id = '.$this->session->userdata('team_id').') and id_client > 1
//                          group by HOUR(last_baca)
//                          order by hour asc
//                      ');
//      }
//    }
//    else
//    {
//      $hasil = $this->db->query('
//                                select count(HOUR(last_baca)) as count, HOUR(last_baca) as hour
//                                  from proposal
//                                  where counter_baca > 0 and user_id = '.$this->session->userdata('user_id').' and id_client > 1
//                                  group by HOUR(last_baca)
//                                  order by hour asc
//                              ');
//    }
//
//      return $hasil->result();
//  }

//  function get_top_proposal()
//  {
//    if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
//    {
//      if($this->monikalib->getUserPlan()->plan_id == 1 || $this->monikalib->getUserPlan()->plan_id == 2)
//      {
//        $hasil = $this->db->query('
//                            SELECT * from
//                            (
//                              SELECT a.proposal_id,count(a.proposal_id) as total, concat(d.nama_pic," - ", c.info_projek) as detail
//                              FROM proposal_tracking a
//                              JOIN proposal b ON a.proposal_id = b.id
//                              JOIN projek c ON b.id = c.idproposal
//                              JOIN client d ON d.id_client = b.id_client
//                              JOIN user d on d.user_id = b.user_id
//                              WHERE b.user_id = '.$this->session->userdata('user_id').' AND b.id_client > 1
//                              GROUP BY a.proposal_id
//                            )x order by x.total desc limit 5
//                            ');
//      }
//      else if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
//      {
//        $hasil = $this->db->query('
//                            SELECT * from
//                            (
//                              SELECT a.proposal_id,count(a.proposal_id) as total, concat(d.nama_pic," - ", c.info_projek) as detail
//                              FROM proposal_tracking a
//                              JOIN proposal b ON a.proposal_id = b.id
//                              JOIN projek c ON b.id = c.idproposal
//                              JOIN client d ON d.id_client = b.id_client
//                              JOIN user e on e.user_id = b.user_id
//                              WHERE b.user_id = '.$this->session->userdata('user_id').' AND b.id_client > 1
//                              GROUP BY a.proposal_id
//                              UNION
//                              SELECT a.proposal_id,count(a.proposal_id) as total, concat(d.nama_pic," - ", c.info_projek) as detail
//                              FROM proposal_tracking a
//                              JOIN proposal b ON a.proposal_id = b.id
//                              JOIN projek c ON c.idproposal = b.id
//                              JOIN client d ON d.id_client = b.id_client
//                              JOIN user e on e.user_id = b.user_id
//                              WHERE b.user_id in (select user_id from user where team_id = '.$this->session->userdata('team_id').') AND b.id_client > 1
//                              GROUP BY a.proposal_id
//                            )x order by x.total desc limit 5
//                          ');
//      }
//    }
//    else
//    {
//      $hasil = $this->db->query('
//                            SELECT * from
//                            (
//                              SELECT a.proposal_id,count(a.proposal_id) as total, concat(d.nama_pic," - ", c.info_projek) as detail
//                              FROM proposal_tracking a
//                              JOIN proposal b ON a.proposal_id = b.id
//                              JOIN projek c ON c.idproposal = b.id
//                              JOIN client d ON d.id_client = b.id_client
//                              JOIN user e on e.user_id = b.user_id
//                              WHERE b.user_id = '.$this->session->userdata('user_id').' AND b.id_client > 1
//                              GROUP BY a.proposal_id
//                            )x order by x.total desc limit 5
//                            ');
//    }

//    $this->db->select('proposal.id, proposal.judul, client.nama_pic');
//    $this->db->from('proposal');
//    $this->db->join('client','proposal.id_client = client.id_client');
//    $this->db->where('proposal.user_id', $this->session->userdata('user_id'));
//    $this->db->order_by('proposal.tgl_buat','desc');
//    $this->db->limit('5');
//    $hasil = $this->db->get();
//    return $hasil->result();
//  }

//  function get_top_invoice()
//  {
//    $this->db->select('invoice.id, invoice.judul, client.nama_pic');
//    $this->db->from('invoice');
//    $this->db->join('client','invoice.id_client = client.id_client');
//    $this->db->where('invoice.id_user', $this->session->userdata('user_id'));
//    $this->db->order_by('invoice.created_at','desc');
//    $this->db->limit('5');
//    $hasil = $this->db->get();
//    return $hasil->result();
//  }

//  function get_avg_readtime()
//  {
//    if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
//    {
//      if($this->monikalib->getUserPlan()->plan_id == 1 || $this->monikalib->getUserPlan()->plan_id == 2)
//      {
//        $hasil = $this->db->query('
//                            SELECT * from
//                            (
//                              SELECT avg(a.screentime) as rata, a.proposal_id, a.section, c.sec_judul
//                              FROM proposal_tracking_detail a
//                              JOIN proposal b ON a.proposal_id = b.id
//                              JOIN section_baru c ON a.section = c.sec_id
//                              WHERE b.user_id = '.$this->session->userdata('user_id').' AND b.id_client > 1
//                              GROUP BY c.sec_judul
//                              HAVING avg(a.screentime) > 0
//                            )x order by x.rata desc limit 5
//                            ');
//      }
//      else if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
//      {
//        $hasil = $this->db->query('
//                            SELECT * from
//                            (
//                              SELECT avg(a.screentime) as rata, a.proposal_id, a.section, c.sec_judul
//                              FROM proposal_tracking_detail a
//                              JOIN proposal b ON a.proposal_id = b.id
//                              JOIN section_baru c ON a.section = c.sec_id
//                              WHERE b.user_id = '.$this->session->userdata('user_id').' AND b.id_client > 1
//                              GROUP BY c.sec_judul
//                              HAVING avg(a.screentime) > 0
//                              UNION
//                              SELECT a.section,avg(a.screentime) as rata, a.proposal_id, c.sec_judul
//                              FROM proposal_tracking_detail a
//                              JOIN proposal b ON a.proposal_id = b.id
//                              JOIN section_baru c ON a.section = c.sec_id
//                              WHERE b.user_id in (select user_id from user where team_id = '.$this->session->userdata('team_id').') AND b.id_client > 1
//                              GROUP BY c.sec_judul
//                              HAVING avg(a.screentime) > 0
//                            )x order by x.rata desc limit 5
//                          ');
//      }
//    }
//    else
//    {
//      $hasil = $this->db->query('
//                            SELECT * from
//                            (
//                              SELECT avg(a.screentime) as rata, a.proposal_id, a.section, c.sec_judul
//                              FROM proposal_tracking_detail a
//                              JOIN proposal b ON a.proposal_id = b.id
//                              JOIN section_baru c ON a.section = c.sec_id
//                              WHERE b.user_id = '.$this->session->userdata('user_id').' AND b.id_client > 1
//                              GROUP BY c.sec_judul
//                              HAVING avg(a.screentime) > 0
//                            )x order by x.rata desc limit 5
//                            ');
//    }
//
//      return $hasil->result();
//  }

//  function get_avg_readtime_by_proposal_id($proposal_id)
//  {
//    if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
//    {
//      if($this->monikalib->getUserPlan()->plan_id == 1 || $this->monikalib->getUserPlan()->plan_id == 2)
//      {
//        $hasil = $this->db->query('
//                            SELECT * from
//                            (
//                              SELECT a.section,avg(a.screentime) as rata, a.proposal_id, c.sec_judul
//                              FROM proposal_tracking_detail a
//                              JOIN proposal b ON a.proposal_id = b.id
//                              JOIN section_baru c ON a.section = c.sec_id
//                              WHERE b.user_id = '.$this->session->userdata('user_id').' AND b.id_client > 1 AND a.proposal_id = '.$proposal_id.'
//                              GROUP BY c.sec_judul
//                              HAVING avg(a.screentime) > 0
//                            )x order by x.rata desc limit 5
//                            ');
//      }
//      else if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
//      {
//        $hasil = $this->db->query('
//                            SELECT * from
//                            (
//                              SELECT a.section,avg(a.screentime) as rata, a.proposal_id, c.sec_judul
//                              FROM proposal_tracking_detail a
//                              JOIN proposal b ON a.proposal_id = b.id
//                              JOIN section_baru c ON a.section = c.sec_id
//                              WHERE b.user_id = '.$this->session->userdata('user_id').' AND b.id_client > 1 AND a.proposal_id = '.$proposal_id.'
//                              GROUP BY c.sec_judul
//                              HAVING avg(a.screentime) > 0
//                              UNION
//                              SELECT a.section,avg(a.screentime) as rata, a.proposal_id, c.sec_judul
//                              FROM proposal_tracking_detail a
//                              JOIN proposal b ON a.proposal_id = b.id
//                              JOIN section_baru c ON a.section = c.sec_id
//                              WHERE b.user_id in (select user_id from user where team_id = '.$this->session->userdata('team_id').') AND b.id_client > 1 AND a.proposal_id = '.$proposal_id.'
//                              GROUP BY c.sec_judul
//                              HAVING avg(a.screentime) > 0
//                            )x order by x.rata desc limit 5
//                          ');
//      }
//    }
//    else
//    {
//      $hasil = $this->db->query('
//                            SELECT * from
//                            (
//                              SELECT a.section,avg(a.screentime) as rata, a.proposal_id, c.sec_judul
//                              FROM proposal_tracking_detail a
//                              JOIN proposal b ON a.proposal_id = b.id
//                              JOIN section_baru c ON a.section = c.sec_id
//                              WHERE b.user_id = '.$this->session->userdata('user_id').' AND b.id_client > 1 AND a.proposal_id = '.$proposal_id.'
//                              GROUP BY c.sec_judul
//                              HAVING avg(a.screentime) > 0
//                            )x order by x.rata desc limit 5
//                            ');
//    }
//
//      return $hasil->result();
//  }
//
//  function get_avg_read()
//  {
//    if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
//    {
//      if($this->monikalib->getUserPlan()->plan_id == 1 || $this->monikalib->getUserPlan()->plan_id == 2)
//      {
//        $hasil = $this->db->query("select avg(counter_baca) rata from proposal where counter_baca > 0 and user_id =".$this->session->userdata('user_id')." and id_client > 1");
//      }
//      else if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
//      {
//        $hasil = $this->db->query("select avg(counter_baca) rata from proposal where counter_baca > 0 and user_id in (select user_id from user where team_id = ".$this->session->userdata('team_id').") and id_client > 1");
//      }
//    }
//    else
//    {
//      $hasil = $this->db->query("select avg(counter_baca) rata from proposal where counter_baca > 0 and user_id =".$this->session->userdata('user_id')." and id_client > 1");
//    }
//
//    if($hasil->num_rows()>0)
//    {
//      return $hasil->row()->rata;
//    }
//    else
//    {
//      return false;
//    }
//  }
//
//  function get_best_time_read()
//  {
//    if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
//    {
//      if($this->monikalib->getUserPlan()->plan_id == 1 || $this->monikalib->getUserPlan()->plan_id == 2)
//      {
//        $hasil = $this->db->query("select COUNT(HOUR(last_baca)) as count, HOUR(last_baca) as hour from proposal where counter_baca > 0 and user_id =".$this->session->userdata('user_id')." and id_client > 1 group by HOUR(last_baca) order by count desc limit 1");
//      }
//      else if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
//      {
//        $hasil = $this->db->query("select COUNT(HOUR(last_baca)) as count, HOUR(last_baca) as hour from proposal where counter_baca > 0 and user_id in (select user_id from user where team_id = ".$this->session->userdata('team_id').") and id_client > 1 group by HOUR(last_baca) order by count desc limit 1");
//      }
//    }
//    else
//    {
//      $hasil = $this->db->query("select COUNT(HOUR(last_baca)) as count, HOUR(last_baca) as hour from proposal where counter_baca > 0 and user_id =".$this->session->userdata('user_id')." and id_client > 1 group by HOUR(last_baca) order by count desc limit 1");
//    }
//    if($hasil->num_rows()>0)
//    {
//      return $hasil->row()->hour;
//    }
//    else
//    {
//      return false;
//    }
//  }
//
//  function get_best_time_read_by_proposal_id($proposal_id)
//  {
//    $hasil = $this->db->query("SELECT COUNT(HOUR(a.tracking_time)) as count, HOUR(a.tracking_time) as hour FROM proposal_tracking a join proposal b on a.proposal_id = b.id where b.id_client > 1 and a.proposal_id = ".$proposal_id." group by HOUR(a.tracking_time) order by count desc");
//    if($hasil->num_rows()>0)
//    {
//      return $hasil->row()->hour;
//    }
//    else
//    {
//      return false;
//    }
//  }
//
//  function get_detail_proposal_stat_by_id($proposal_id)
//  {
//      $this->db->select('id,judul,counter_baca');
//      $this->db->from('proposal');
//      $this->db->where('id',$proposal_id);
//      $hasil = $this->db->get();
//
//      if($hasil->num_rows()>0){
//          return $hasil->row();
//      } else{
//          return false;
//      }
//  }
//
//  function get_estimated_revenue()
//  {
//    if($this->monikalib->getUserPlan() != NULL && $this->monikalib->getUserPlan()->status == 1)
//    {
//      if($this->monikalib->getUserPlan()->plan_id == 1 || $this->monikalib->getUserPlan()->plan_id == 2)
//      {
//        $hasil = $this->db->query('SELECT sum(e.harga) as project_fee
//                              FROM proposal a
//                              JOIN projek b ON b.idproposal = a.id
//                              JOIN client c ON c.id_client = a.id_client
//                              JOIN user d on d.user_id = a.user_id
//                              JOIN pemberian e on e.idproposal = a.id
//                              WHERE a.user_id = '.$this->session->userdata('user_id').' AND a.id_client > 1 AND a.arsip = 0');
//      }
//      else if($this->monikalib->getUserPlan()->plan_id == 3 || $this->monikalib->getUserPlan()->plan_id == 4)
//      {
//        $hasil = $this->db->query('
//                            SELECT sum(project_fee) as project_fee from
//                            (
//                              SELECT sum(e.harga) as project_fee
//                              FROM proposal a
//                              JOIN projek b ON b.idproposal = a.id
//                              JOIN client c ON c.id_client = a.id_client
//                              JOIN user d on d.user_id = a.user_id
//                              JOIN pemberian e on e.idproposal = a.id
//                              WHERE a.user_id = '.$this->session->userdata('user_id').' AND a.id_client > 1 AND a.arsip = 0
//                              UNION
//                              SELECT sum(e.harga) as project_fee FROM proposal a
//                              JOIN projek b ON b.idproposal = a.id
//                              JOIN client c ON c.id_client = a.id_client
//                              JOIN user d on d.user_id = a.user_id
//                              JOIN pemberian e on e.idproposal = a.id
//                              WHERE a.user_id in (select user_id from user where team_id = '.$this->session->userdata('team_id').') AND c.id_client > 1 AND a.id_client > 1 AND a.arsip = 0
//                            )x ');
//      }
//    }
//    else
//    {
//      $hasil = $this->db->query('SELECT sum(e.harga) as project_fee FROM proposal a
//                              JOIN projek b ON b.idproposal = a.id
//                              JOIN client c ON c.id_client = a.id_client
//                              JOIN user d on d.user_id = a.user_id
//                              JOIN pemberian e on e.idproposal = a.id
//                              WHERE a.user_id = '.$this->session->userdata('user_id').' AND a.id_client > 1 AND a.arsip = 0');
//    }
//
//    if($hasil->num_rows()>0)
//    {
//      return $hasil->row()->project_fee;
//    }
//    else
//    {
//      return false;
//    }
//  }

//    public function getProposalLogByTeam($team)
//    {
//        $this->db->select('a.*, b.user_name, c.info_projek, d.judul');
//        $this->db->from('proposal_log a');
//        $this->db->join('user b', 'a.user_id = b.user_id');
//        $this->db->join('projek c', 'a.proposal_id = c.idproposal');
//        $this->db->join('proposal d', 'a.proposal_id = d.id');
//        $this->db->where('a.user_id in (select user_id from user where team_id = '.$team.') and id_client > 1');
//        $this->db->order_by('a.logtime desc');
//        $result = $this->db->get();
//        if($result->num_rows()>0)
//        {
//          return $result->result();
//        }
//        else
//        {
//          return false;
//        }
//    }
//
//    function getProposalLog()
//    {
//        $this->db->select('a.*, b.judul');
//        $this->db->from('proposal_log a');
//        $this->db->join('proposal b','a.proposal_id = b.id');
//        $this->db->where('a.user_id',$this->session->userdata('user_id'));
//        $this->db->order_by('a.logtime','desc');
//        $this->db->limit(20);
//        $result = $this->db->get();
//        if($result->num_rows()>0)
//        {
//          return $result->result();
//        }
//        else
//        {
//          return false;
//        }
//    }

    // new model by wage
//    function get_all_avg_readtime(){
//      $this->db->select('proposal_tracking_detail.section, avg(proposal_tracking_detail.screentime) as rata_rata, section_baru.sec_judul');
//      $this->db->from('proposal_tracking_detail');
//      $this->db->join('section_baru', 'proposal_tracking_detail.section = section_baru.sec_id');
//      $this->db->join('proposal', 'proposal_tracking_detail.proposal_id = proposal.id');
//      $this->db->where('proposal.user_id ='.$this->session->userdata('user_id').' AND proposal.id_client > 1');
//      $this->db->group_by('section_baru.sec_judul');
//      $this->db->having('rata_rata > 0');
//      $this->db->order_by('rata_rata','DESC');
//      $this->db->limit(5);
//      $result = $this->db->get();
//      if($result->num_rows()>0)
//      {
//        return $result->result();
//      }
//      else
//      {
//        return false;
//      }
//    }
    // end of new model by wage

    function getLog()
    {
        $id_user = $this->session->userdata('user_id');

        $query = $this->db->query('
            select * from
            (
                (select a.proposal_id as id, a.section, a.action, a.user_id, a.logtime, a1.judul
                from proposal_log a
                join proposal a1 on a1.id=a.proposal_id)
                union all
                (select b.id_invoice as id, b.section, b.action, b.user_id, b.logtime, b1.judul
                from invoice_log b
                join invoice b1 on b1.id=b.id_invoice)
            ) t_union
            where user_id='.$id_user.'
            order by logtime desc
            limit 30
        ');
        return $query->result();
    }

    function getTopReadProposal()
    {
        $iduser = $this->session->userdata('user_id');
        $idteam = $this->session->userdata('team_id');

        $result = $this->db->query('select * from
        (
            (select p.id, p.judul, c.nama_pic, p.counter_baca as jml, p.arsip
            from proposal p
            join client c on c.id_client=p.id_client
            where p.user_id='.$iduser.' and p.counter_baca>0 )
            union
            (select p.id, p.judul, c.nama_pic, p.counter_baca as jml, p.arsip
            from proposal p
            join client c on c.id_client=p.id_client
            where p.user_id in (select user_id from user where team_id = '.$idteam.') and p.share_team=1 and p.counter_baca>0 )
        ) t_union
        where arsip=0
        order by jml desc
        limit 5
        ');
        return $result->result();
    }

    function getTopFiveProposal()
    {
        $iduser = $this->session->userdata('user_id');

        $this->db->select('p.id, p.judul, c.nama_pic, p.counter_baca');
        $this->db->from('proposal p');
        $this->db->join('client c', 'c.id_client=p.id_client');
        $this->db->where('p.user_id',$iduser);
        $this->db->where('p.arsip',0);
        $this->db->order_by('p.counter_baca', 'desc');
        $this->db->limit(5);
        $result = $this->db->get();
        return $result->result();
    }

    function getTopFiveInvoice()
    {
        $iduser = $this->session->userdata('user_id');

        $this->db->select('i.id, i.judul, c.nama_pic');
        $this->db->from('invoice i');
        $this->db->join('client c', 'c.id_client=i.id_client');
        $this->db->where('i.id_user',$iduser);
        $this->db->where('i.deleted',0);
        $this->db->order_by('i.created_at','desc');
        $this->db->limit(5);
        $result = $this->db->get();
        return $result->result();
    }

    function averageKontenBaca()
    {
        $iduser = $this->session->userdata('user_id');

        if($this->monikalib->currentUserPlan()==0){
            $result = $this->db->query("
                select s.sec_judul, round(avg(td.screentime)) as lama
                from proposal_tracking_detail td
                inner join proposal p on p.id=td.proposal_id
                inner join section_baru s on s.sec_id=td.section
                where p.user_id='$iduser'
                group by s.sec_judul
                order by lama desc
                limit 2
            ");
        }
        else{
            $result = $this->db->query("
                select s.sec_judul, round(avg(td.screentime)) as lama
                from proposal_tracking_detail td
                inner join proposal p on p.id=td.proposal_id
                inner join section_baru s on s.sec_id=td.section
                where p.user_id='$iduser'
                group by s.sec_judul
                order by lama desc
                limit 7
            ");
        }
        return $result->result();
    }

    function bestHourRead()
    {
        $iduser = $this->session->userdata('user_id');
        $idteam = $this->session->userdata('team_id');

        $result = $this->db->query("
            select * from(
                (SELECT hour(tracking_time) as hour, count(hour(tracking_time)) as jml
                FROM proposal_tracking pt
                JOIN proposal p ON p.id=pt.proposal_id
                WHERE p.user_id='$iduser'
                GROUP BY hour
                ORDER BY jml desc
                )
                union
                (SELECT hour(tracking_time) as hour, count(hour(tracking_time)) as jml
                FROM proposal_tracking pt
                JOIN proposal p ON p.id=pt.proposal_id
                where p.user_id in (select user_id from user where team_id ='$idteam') and share_team=1
                GROUP BY hour
                ORDER BY jml desc
                )
            )t_union
            ORDER BY jml desc
            limit 1
        ");
        return $result->row();
    }

    function bestMinuteRead()
    {
        $iduser = $this->session->userdata('user_id');
        $idteam = $this->session->userdata('team_id');

        $result = $this->db->query("
            select * from(
                (SELECT minute(tracking_time) as minute, count(minute(tracking_time)) as jml
                FROM proposal_tracking pt
                JOIN proposal p ON p.id=pt.proposal_id
                WHERE p.user_id='$iduser'
                GROUP BY minute
                ORDER BY jml desc
                )
                union
                (SELECT minute(tracking_time) as minute, count(minute(tracking_time)) as jml
                FROM proposal_tracking pt
                JOIN proposal p ON p.id=pt.proposal_id
                where p.user_id in (select user_id from user where team_id ='$idteam') and share_team=1
                GROUP BY minute
                ORDER BY jml desc
                )
            )t_union
            ORDER BY jml desc
            limit 1
        ");
        return $result->row();
    }

    function avgProposalRead()
    {
        $iduser = $this->session->userdata('user_id');
        $idteam = $this->session->userdata('team_id');

        $result = $this->db->query("
            select round(avg(jml)) as rerata from(
                (SELECT hour(tracking_time) as hour, count(hour(tracking_time)) as jml
                FROM proposal_tracking pt
                JOIN proposal p ON p.id=pt.proposal_id
                WHERE p.user_id='$iduser'
                GROUP BY hour
                ORDER BY jml desc
                )
                union
                (SELECT hour(tracking_time) as hour, count(hour(tracking_time)) as jml
                FROM proposal_tracking pt
                JOIN proposal p ON p.id=pt.proposal_id
                where p.user_id in (select user_id from user where team_id ='$idteam') and share_team=1
                GROUP BY hour
                ORDER BY jml desc
                )
            )t_union
            ORDER BY jml desc
        ");
        return $result->row();
    }

    function readTimeProposal()
    {
        $iduser = $this->session->userdata('user_id');
        $idteam = $this->session->userdata('team_id');

        $result = $this->db->query("
            select * from
            (
                select * from(
                (SELECT hour(tracking_time) as hour, count(hour(tracking_time)) as jml
                FROM proposal_tracking pt
                JOIN proposal p ON p.id=pt.proposal_id
                WHERE p.user_id='$iduser' and p.counter_baca>0
                GROUP BY hour
                ORDER BY jml desc
                )
                union
                (SELECT hour(tracking_time) as hour, count(hour(tracking_time)) as jml
                FROM proposal_tracking pt
                JOIN proposal p ON p.id=pt.proposal_id
                where p.user_id in (select user_id from user where team_id =23) and share_team='$idteam' and p.counter_baca>0
                GROUP BY hour
                ORDER BY jml desc
                )
            )t_union
            ORDER BY jml desc
            limit 6
            )t_read
            order by hour asc
        ");
        return $result->result();
    }
}
