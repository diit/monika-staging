<?php
/**
 * Class proposal untuk mengatur semua kelengkapan proposal
 */
class Template_model extends CI_model
{
  public function template_proposal(){
    $this->db->select('template_client.nama_pic, template_client.email,template_projek.info_projek, template_proposal.id, template_proposal.tgl_buat, template_proposal.id_client');
    $this->db->from('template_proposal');
    $this->db->join('template_projek' , 'template_projek.idproposal = template_proposal.id');
    $this->db->join('template_client' , 'template_client.id_client = template_proposal.id_client');
    $this->db->order_by('template_proposal.id desc');
    $hasil = $this->db->get();
    return $obj_proposal = $hasil->result();
  }

  public function lihat_prop($kategori,$user_id){
    $this->db->select('template_proposal.*');
    $this->db->from('template_proposal');
    $this->db->join('template_projek' , 'template_proposal.id = template_projek.idproposal');
    $this->db->join('template_client', 'template_proposal.id_client = template_client.id_client');
    $this->db->where('template_proposal.category_id in ('.$kategori.',0)');
    //$hasildetail = $this->db->get();
    //return $hasildetail->result();

    if($query=$this->db->get())
    {
      if($query && $query -> num_rows() >= 1)
      {
        $prop_detail =  $query->result();
        $new_prop_detail = array();
        foreach ($prop_detail as $key => $pd) {
          $pd->pengalaman = array();
          $this->db->select('pengalaman');
          $this->db->from('template_pengalaman');
          $this->db->where('idproposal',$pd->id);
          $query2=$this->db->get();
          if($query2 && $query2->num_rows() > 0)
          {
            $pengalaman = $query2->result_array();
            //var_dump($pengalaman);
            $pd->pengalaman = $pengalaman;
          }
          array_push($new_prop_detail, $pd);
        }

        return $new_prop_detail;
      }
      else
      {
        return false;
      }
    }
    else{
      return false;
    }

  }

  public function lihat_template($id_template,$user_id){
    $this->db->select('*');
    $this->db->from('template_proposal');
    $this->db->join('template_projek' , 'template_proposal.id = template_projek.idproposal');
    $this->db->join('template_client', 'template_proposal.id_client = template_client.id_client');
    $this->db->join('template_keuntungan', 'template_keuntungan.idproposal = template_proposal.id');
    $this->db->where('template_proposal.id', $id_template);
    //$hasildetail = $this->db->get();
    //return $hasildetail->result();

    if($query=$this->db->get())
    {
      if($query && $query -> num_rows() >= 1)
      {
        $prop_detail =  $query->row();
        $prop_detail->pengalaman = array();
        $this->db->select('pengalaman');
        $this->db->from('template_pengalaman');
        $this->db->where('idproposal',$id_template);
        $query2=$this->db->get();
        if($query2 && $query2->num_rows() > 0)
        {
          $pengalaman = $query2->result_array();
          //var_dump($pengalaman);
          $prop_detail->pengalaman = $pengalaman;
        }

        return $prop_detail;
      }
      else
      {
        return false;
      }
    }
    else{
      return false;
    }

  }

  public function insert_proposal($aboutyou,$id_client,$tglbuat){
    /*return $this->db->query("INSERT INTO proposal (user_email,aboutyou,id_client,status,tgl_buat) VALUES ('$useremail','$aboutyou','$id_client','$statusbaca','$tglbuat')");
    */
    $data_proposal = array(
                  'aboutyou'=>$aboutyou,
                  'id_client'=>$id_client,
                  'tgl_buat'=>$tglbuat
                );
    $this->db->insert('template_proposal', $data_proposal);
      $id_proposal = $this->db->insert_id();
      return $id_proposal;
  }

  public function insert_projek($info_projek,$projek_desc,$nextstep,$idproposal){
    return $this->db->query("INSERT INTO `template_projek` (`info_projek`,`project_desc`,`nextstep`,`idproposal`) VALUES('$info_projek','$projek_desc','$nextstep','$idproposal')");
  }

  public function insert_kemampuan($skill,$idproposal){
    return $this->db->query("INSERT INTO template_kemampuan (skill,idproposal) VALUES ('$skill','$idproposal')");
  }

  public function insert_pengalaman($pengalaman,$idproposal){
    return $this->db->query("INSERT INTO template_pengalaman (pengalaman,idproposal) VALUES('$pengalaman','$idproposal')");
  }

  public function insert_tahapan($tahapan,$tanggal,$idproposal){
    return $this->db->query("INSERT INTO template_tahapan (tahapan,tanggal,idproposal) VALUES('$tahapan','$tanggal','$idproposal')");
  }

  function insert_keuntungan($keuntungan,$idproposal){
    $data_keuntungan = array(
                        'keuntungan'=>$keuntungan,
                        'idproposal'=>$idproposal
                        );
    $this->db->insert('template_keuntungan', $data_keuntungan);
    $idkeuntungan = $this->db->insert_id();
    return $idkeuntungan;
    //return $this->db->query("INSERT INTO keuntungan (keuntungan,idproposal) VALUES ('$keuntungan','$idproposal')");
  }

  function insert_pemberian($pemberian,$harga,$idproposal){
    return $this->db->query("INSERT INTO template_pemberian(pemberian,harga,idproposal) VALUES ('$pemberian','$harga','$idproposal')");
  }

  function insert_syarat($syarat,$idproposal){
    return $this->db->query("INSERT INTO template_syarat (syaratketentuan,idproposal) VALUES ('$syarat','$idproposal')");
  }


  public function projek($id_template){
    $this->db->select('*');
    $this->db->from('template_proposal');
    $this->db->join('template_projek' , 'template_proposal.id = template_projek.idproposal');
    $this->db->where('template_proposal.id', $id_template);
    $hasildetail = $this->db->get();
    return $hasildetail->row_array();
  }

  public function syarat($id_template){
    $this->db->select('*');
    $this->db->from('template_proposal');
    $this->db->join('template_syarat' , 'template_proposal.id = template_syarat.idproposal');
    $this->db->where('template_proposal.id', $id_template);
    $hasildetail = $this->db->get();
    return $hasildetail->result();
  }
  public function tahapan($id_template){
    $this->db->select('*');
    $this->db->from('template_proposal');
    $this->db->join('template_tahapan', 'template_proposal.id = template_tahapan.idproposal');
    $this->db->where('template_proposal.id', $id_template);
    $hasildetail = $this->db->get();
    return $hasildetail->result();
  }
  public function kemampuan($id_template){
    $this->db->select('*');
    $this->db->from('template_kemampuan');
    $this->db->where('idproposal', $id_template);
    $hasildetail = $this->db->get();
    return $hasildetail->result();
  }
  public function pengalaman($id_template){
    $this->db->select('*');
    $this->db->from('template_pengalaman');
    $this->db->where('idproposal', $id_template);
    $hasildetail = $this->db->get();
    return $hasildetail->result();
  }
  public function pemberian($id_template){
    $this->db->select('*');
    $this->db->from('template_pemberian');
    //$this->db->join('pemberian', 'keuntungan.id= pemberian.idkeuntungan');
    $this->db->where('idproposal', $id_template);
    $hasildetail = $this->db->get();
    return $hasildetail->result();
  }

  public function keuntungan($id_template){
    $this->db->select('*');
    $this->db->from('template_keuntungan');
    $this->db->where('idproposal', $id_template);
    $hasildetail = $this->db->get();
    return $hasildetail->row();
  }


//Admin Template Function
  public function additional_section($id_template){
      $this->db->select('*');
      $this->db->from('template_section_tambahan');
      $this->db->where('proposal_id',$id_template);
      if($hasil = $this->db->get())
      {
        if($hasil && $hasil->num_rows() > 0)
        {
          $add_section = $hasil->result();
          $hasil_baru = array();
          foreach ($add_section as $as)
          {
            $as->detail = array();
            $this->db->select('section_detail_id,section_detail_content,section_id');
            $this->db->from('template_section_detail');
            $this->db->where('section_id',$as->section_id);
            $hasil2 = $this->db->get();
            if($hasil2 && $hasil2->num_rows() > 0)
            {
              $detail = $hasil2->result();
              foreach ($detail as $dt)
              {
                array_push($as->detail, $dt);
              }
            }
            array_push($hasil_baru, $as);
          }
          return $hasil_baru;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
  }

  public function update_project_title($proposal_id,$project_title)
  {
    $result_update_title = $this->db->query('UPDATE template_projek set info_projek = "'.$project_title.'" where idproposal ='.$proposal_id);
    return $result_update_title;
  }

  function remove_section($tabel,$proposal_id)
  {
    $this -> db -> where('idproposal', $proposal_id);
    return $this->db->delete($tabel);
  }

  function remove_overview($proposal_id)
  {
   return $this->db->query("UPDATE template_projek SET project_desc ='' WHERE idproposal =".$proposal_id);
  }

  function remove_nextstep($proposal_id)
  {
    return $this->db->query("UPDATE template_projek SET nextstep ='' WHERE idproposal =".$proposal_id);
  }

  function remove_additional_section($section,$proposal_id)
  {
    $query =  $this->db->query('delete from section_detail where section_id in (select section_id from template_section_tambahan where section_name = "'.$section.'" and proposal_id = '.$proposal_id.')');
    if($query)
    {
      $this -> db -> where('proposal_id', $proposal_id);
      $query2 = $this->db->delete('template_section_tambahan');
      if($query)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }

  public function insert_new_section($data_section){
      $this->db->insert('template_section_tambahan', $data_section);
      $section_id = $this->db->insert_id();
      return $section_id;
  }

  public function insert_section_detail($data_section_detail){
    return $this->db->insert('template_section_detail', $data_section_detail);
  }

  public function update_section_projek($proposal_id,$data_projek){
    $this->db->where('idproposal',$proposal_id);
    // $this->db->where('user_id',$this->session->userdata('user_id'));
    return $this->db->update('template_projek', $data_projek);
  }

  public function update_section_keuntungan($proposal_id,$data_projek){
    $this->db->where('idproposal',$proposal_id);
    return $this->db->update('template_keuntungan', $data_projek);
  }

  function update_keuntungan($idprop,$keuntungan){
    return $this->db->query("UPDATE keuntungan SET keuntungan = '$keuntungan' WHERE idproposal = '$idprop'");
  }

  public function delete_portfolio($idproposal)
  {
    return $this->db->query("DELETE from template_pengalaman where idproposal = ".$idproposal);
  }

  public function update_pengalaman($idproposal, $pengalaman){
    return $this->db->query("UPDATE template_pengalaman SET pengalaman='$pengalaman' WHERE idproposal='$idproposal'");
  }

  public function delete_kemampuan($idproposal)
  {
    return $this->db->query("DELETE from template_kemampuan where idproposal = ".$idproposal);
  }

  public function update_kemampuan($id,$skill){
    return $this->db->query("UPDATE template_kemampuan SET skill ='$skill' WHERE id='$id'");
  }

  public function delete_tahapan($idproposal)
  {
    return $this->db->query("DELETE from template_tahapan where idproposal = ".$idproposal);
  }

  public function update_tahapan($id,$tahapan,$tgl){
    return $this->db->query("UPDATE template_tahapan SET tahapan='$tahapan', tanggal='$tgl' WHERE id='$id'");
  }

  public function delete_syarat($idproposal)
  {
    return $this->db->query("DELETE from template_syarat where idproposal = ".$idproposal);
  }

  function update_syarat($id,$syarat){
    return $this->db->query("UPDATE template_syarat SET syaratketentuan='$syarat' WHERE id='$id'");
  }

  public function delete_pemberian($idproposal)
  {
    return $this->db->query("DELETE from template_pemberian where idproposal = ".$idproposal);
  }

  function update_pemberian($idproposal, $pemberian,$harga){
    return $this->db->query("UPDATE template_pemberian SET pemberian='".$pemberian."', harga=".$harga." WHERE idproposal=".$idproposal);
  }

  public function update_additional_section($proposal_id,$section_id,$content)
  {
      $result_update = $this->db->query('UPDATE template_section_detail a
                      INNER JOIN template_section_tambahan b ON b.section_id = a.section_id
                      SET a.section_detail_content = "'.$content.'"
                      WHERE a.section_id = '.$section_id.' and b.section_type="text" and  b.proposal_id = '.$proposal_id
                    );
      return $result_update;
  }

  public function update_info($idproposal, $info){
    $query="UPDATE `template_projek` SET `info_projek`='$info' WHERE idproposal = '$idproposal'";
    return $this->db->query($query);
  }
  public function update_nextstep($idproposal, $nextstep){
    $query="UPDATE `template_projek` SET `nextstep`='$nextstep' WHERE idproposal = '$idproposal'";
    return $this->db->query($query);
  }


}
