<?php
class Arsip_model extends CI_Model{
  function __construct(){
    parent::__construct();
    $this->load->database();
  }

  public function arsip(){
    $this->db->select('client.nama_pic, client.email, proposal.id, proposal.tgl_buat, proposal.status, proposal.arsip, proposal.judul, proposal.tgl_kirim');
    $this->db->from('proposal');
    $this->db->join('client' , 'client.id_client = proposal.id_client');
    $this->db->where('proposal.user_id', $this->session->userdata('user_id'));
    $this->db->where('proposal.arsip', 1);
    $hasil = $this->db->get();
    return $obj_proposal = $hasil->result();
  }
  public function updateArchive($id){
    $this->db->query("UPDATE proposal SET arsip = 1 WHERE id = '$id'");
  }
  public function updateUnArchive($id){
    $this->db->query("UPDATE proposal SET arsip = 0 WHERE id = '$id'");
  }

    function arsipInvoice()
    {
        $idteam = $this->session->userdata('team_id');
        $this->db->select('invoice.*, client.nama_pic');
        $this->db->from('invoice');
        $this->db->join('client', 'client.id_client = invoice.id_client');
        $this->db->where('invoice.id_team', $idteam);
        $this->db->where('invoice.deleted', 1);
        $this->db->order_by('invoice.id desc');
        return $this->db->get()->result();
    }

    function updateUnArchiveInvoice($id){
        $this->db->query("UPDATE invoice SET deleted = 0 WHERE id = '$id'");
      }

    function cariProposalArsip($query){
        $iduser = $this->session->userdata('user_id');
        $idteam = $this->session->userdata('team_id');

        $result = $this->db->query('
            select p.judul, p.id, p.status, p.tgl_buat, p.user_id, p.tgl_kirim, c.nama_pic, c.email, p.arsip, p.created_at, u.user_name, u.team_id
            from proposal p
            join client c on c.id_client=p.id_client
            join user u on u.user_id=p.user_id
            where p.user_id='.$iduser.' and (p.judul LIKE "%'.$query.'%" OR c.nama_pic LIKE "%'.$query.'%") and p.arsip=1
            order by p.tgl_buat desc');
        return $result;
    }

     function cariInvoiceArsip($query){
        $idteam = $this->session->userdata('team_id');
        $iduser = $this->session->userdata('user_id');
        $result = $this->db->query('
            select i.id, i.number, i.judul, i.id_proposal, i.id_user, i.id_team, i.note, i.tax, i.status, i.send_invoice, i.due_invoice, i.created_at, c.nama_pic, i.deleted, u.user_name
            from invoice i
            join client c on c.id_client=i.id_client
            join user u on u.user_id=i.id_user
            where id_user='.$iduser.' and (i.judul LIKE "%'.$query.'%" OR c.nama_pic LIKE "%'.$query.'%") and i.deleted=1
            order by i.created_at desc');
        return $result;
    }
}

 ?>
